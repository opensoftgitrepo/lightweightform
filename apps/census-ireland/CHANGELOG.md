# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.13.4"></a>

## [4.13.4](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.13.3&sourceBranch=refs%2Ftags%2Fv4.13.4) (2022-05-12)

**Note:** Version bump only for package @lightweightform-app/census-ireland

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.13.3"></a>

## [4.13.3](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.13.2&sourceBranch=refs%2Ftags%2Fv4.13.3) (2022-03-16)

**Note:** Version bump only for package @lightweightform-app/census-ireland

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.13.2"></a>

## [4.13.2](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.13.1&sourceBranch=refs%2Ftags%2Fv4.13.2) (2022-02-14)

**Note:** Version bump only for package @lightweightform-app/census-ireland

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.13.1"></a>

## [4.13.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.13.0&sourceBranch=refs%2Ftags%2Fv4.13.1) (2022-02-03)

**Note:** Version bump only for package @lightweightform-app/census-ireland

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.13.0"></a>

# [4.13.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.12.2&sourceBranch=refs%2Ftags%2Fv4.13.0) (2022-01-27)

### Features

- update dependencies
  ([f921338](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/f921338))

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.12.2"></a>

## [4.12.2](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.12.1&sourceBranch=refs%2Ftags%2Fv4.12.2) (2021-10-27)

**Note:** Version bump only for package @lightweightform-app/census-ireland

<a name="4.12.1"></a>

## [4.12.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.12.0&sourceBranch=refs%2Ftags%2Fv4.12.1) (2021-10-27)

**Note:** Version bump only for package @lightweightform-app/census-ireland

<a name="4.12.0"></a>

# [4.12.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.11.2&sourceBranch=refs%2Ftags%2Fv4.12.0) (2021-10-26)

**Note:** Version bump only for package @lightweightform-app/census-ireland

<a name="4.11.2"></a>

## [4.11.2](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.11.1&sourceBranch=refs%2Ftags%2Fv4.11.2) (2021-10-25)

**Note:** Version bump only for package @lightweightform-app/census-ireland

<a name="4.11.1"></a>

## [4.11.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.11.0&sourceBranch=refs%2Ftags%2Fv4.11.1) (2021-10-22)

**Note:** Version bump only for package @lightweightform-app/census-ireland

<a name="4.11.0"></a>

# [4.11.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.10.2&sourceBranch=refs%2Ftags%2Fv4.11.0) (2021-10-20)

**Note:** Version bump only for package @lightweightform-app/census-ireland

<a name="4.10.2"></a>

## [4.10.2](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.10.1&sourceBranch=refs%2Ftags%2Fv4.10.2) (2021-09-13)

**Note:** Version bump only for package @lightweightform-app/census-ireland

<a name="4.10.1"></a>

## [4.10.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.10.0&sourceBranch=refs%2Ftags%2Fv4.10.1) (2021-08-22)

**Note:** Version bump only for package @lightweightform-app/census-ireland

<a name="4.10.0"></a>

# [4.10.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.9.2&sourceBranch=refs%2Ftags%2Fv4.10.0) (2021-07-14)

**Note:** Version bump only for package @lightweightform-app/census-ireland

<a name="4.9.2"></a>

## [4.9.2](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.9.1&sourceBranch=refs%2Ftags%2Fv4.9.2) (2021-07-11)

**Note:** Version bump only for package @lightweightform-app/census-ireland

<a name="4.9.1"></a>

## [4.9.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.9.0&sourceBranch=refs%2Ftags%2Fv4.9.1) (2021-07-11)

**Note:** Version bump only for package @lightweightform-app/census-ireland

<a name="4.9.0"></a>

# [4.9.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.8.3&sourceBranch=refs%2Ftags%2Fv4.9.0) (2021-07-05)

**Note:** Version bump only for package @lightweightform-app/census-ireland

<a name="4.8.3"></a>

## [4.8.3](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.8.2&sourceBranch=refs%2Ftags%2Fv4.8.3) (2021-06-22)

**Note:** Version bump only for package @lightweightform-app/census-ireland

<a name="4.8.2"></a>

## [4.8.2](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.8.1&sourceBranch=refs%2Ftags%2Fv4.8.2) (2021-06-22)

**Note:** Version bump only for package @lightweightform-app/census-ireland

<a name="4.8.1"></a>

## [4.8.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.8.0&sourceBranch=refs%2Ftags%2Fv4.8.1) (2021-06-21)

**Note:** Version bump only for package @lightweightform-app/census-ireland

<a name="4.8.0"></a>

# [4.8.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.7.5&sourceBranch=refs%2Ftags%2Fv4.8.0) (2021-06-19)

### Features

- async actions + detect load cancel
  ([34998c6](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/34998c6))

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.7.5"></a>

## [4.7.5](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.7.4&sourceBranch=refs%2Ftags%2Fv4.7.5) (2021-03-10)

### Bug Fixes

- **theme-common:** make addNgModuleImport receive opt importExpression
  ([589f54e](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/589f54e)),
  closes #52

<a name="4.7.4"></a>

## [4.7.4](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.7.3&sourceBranch=refs%2Ftags%2Fv4.7.4) (2021-02-06)

**Note:** Version bump only for package @lightweightform-app/census-ireland

<a name="4.7.3"></a>

## [4.7.3](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.7.2&sourceBranch=refs%2Ftags%2Fv4.7.3) (2020-12-16)

**Note:** Version bump only for package @lightweightform-app/census-ireland

<a name="4.7.2"></a>

## [4.7.2](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.7.1&sourceBranch=refs%2Ftags%2Fv4.7.2) (2020-12-15)

**Note:** Version bump only for package @lightweightform-app/census-ireland

<a name="4.7.1"></a>

## [4.7.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.7.0&sourceBranch=refs%2Ftags%2Fv4.7.1) (2020-12-15)

**Note:** Version bump only for package @lightweightform-app/census-ireland

<a name="4.7.0"></a>

# [4.7.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.6.1&sourceBranch=refs%2Ftags%2Fv4.7.0) (2020-12-15)

**Note:** Version bump only for package @lightweightform-app/census-ireland

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.6.1"></a>

## [4.6.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.6.0&sourceBranch=refs%2Ftags%2Fv4.6.1) (2020-11-09)

**Note:** Version bump only for package @lightweightform-app/census-ireland

<a name="4.6.0"></a>

# [4.6.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.5.4&sourceBranch=refs%2Ftags%2Fv4.6.0) (2020-11-09)

**Note:** Version bump only for package @lightweightform-app/census-ireland

<a name="4.5.4"></a>

## [4.5.4](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.5.3&sourceBranch=refs%2Ftags%2Fv4.5.4) (2020-10-28)

### Bug Fixes

- **core:** prevent base path in LF route path
  ([f950425](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/f950425)),
  closes #51

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.5.3"></a>

## [4.5.3](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.5.2&sourceBranch=refs%2Ftags%2Fv4.5.3) (2020-06-09)

**Note:** Version bump only for package @lightweightform-app/census-ireland

<a name="4.5.2"></a>

## [4.5.2](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.5.1&sourceBranch=refs%2Ftags%2Fv4.5.2) (2020-02-19)

**Note:** Version bump only for package @lightweightform-app/census-ireland

<a name="4.5.1"></a>

## [4.5.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.5.0&sourceBranch=refs%2Ftags%2Fv4.5.1) (2020-02-19)

**Note:** Version bump only for package @lightweightform-app/census-ireland

<a name="4.5.0"></a>

# [4.5.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.4.6&sourceBranch=refs%2Ftags%2Fv4.5.0) (2020-02-19)

### Features

- support Angular 9
  ([54a7154](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/54a7154))

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.4.6"></a>

## [4.4.6](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.4.5&sourceBranch=refs%2Ftags%2Fv4.4.6) (2020-02-13)

**Note:** Version bump only for package @lightweightform-app/census-ireland

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.4.5"></a>

## [4.4.5](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.4.4&sourceBranch=refs%2Ftags%2Fv4.4.5) (2020-01-23)

**Note:** Version bump only for package @lightweightform-app/census-ireland

<a name="4.4.4"></a>

## [4.4.4](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.4.3&sourceBranch=refs%2Ftags%2Fv4.4.4) (2020-01-22)

### Bug Fixes

- fix lint on apps
  ([f48ddd3](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/f48ddd3))

<a name="4.4.3"></a>

## [4.4.3](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.4.2&sourceBranch=refs%2Ftags%2Fv4.4.3) (2020-01-19)

**Note:** Version bump only for package @lightweightform-app/census-ireland

<a name="4.4.2"></a>

## [4.4.2](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.4.1&sourceBranch=refs%2Ftags%2Fv4.4.2) (2020-01-18)

**Note:** Version bump only for package @lightweightform-app/census-ireland

<a name="4.4.1"></a>

## [4.4.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.4.0&sourceBranch=refs%2Ftags%2Fv4.4.1) (2020-01-14)

### Bug Fixes

- repo-wide spellchecks + small improvements
  ([90545fb](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/90545fb))

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.4.0"></a>

# [4.4.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.3.0&sourceBranch=refs%2Ftags%2Fv4.4.0) (2020-01-06)

**Note:** Version bump only for package @lightweightform-app/census-ireland

<a name="4.3.0"></a>

# [4.3.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.2.0&sourceBranch=refs%2Ftags%2Fv4.3.0) (2019-12-26)

### Features

- **numeric-input:** introduce numeric-input
  ([7ee28ff](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/7ee28ff)),
  closes #44

<a name="4.2.0"></a>

# [4.2.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.1.0&sourceBranch=refs%2Ftags%2Fv4.2.0) (2019-12-08)

### Bug Fixes

- force MobX version 5.14.2
  ([68817b5](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/68817b5))

### Features

- support AoT compilation
  ([bd9e886](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/bd9e886)),
  closes #43

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.1.0"></a>

# [4.1.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.0.1&sourceBranch=refs%2Ftags%2Fv4.1.0) (2019-11-10)

### Bug Fixes

- **bootstrap-theme:** fix schematics on Windows
  ([c7f3575](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/c7f3575))

### Features

- **core:** support custom encoding when saving
  ([3c95169](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/3c95169))

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.0.1"></a>

## [4.0.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.0.0&sourceBranch=refs%2Ftags%2Fv4.0.1) (2019-10-22)

**Note:** Version bump only for package @lightweightform-app/census-ireland

<a name="4.0.0"></a>

# [4.0.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.0.0-beta.19&sourceBranch=refs%2Ftags%2Fv4.0.0) (2019-10-19)

Initial LF 4 release.
