# Lightweightform/census-ireland

This package contains a Lightweightform example application built from the
original
[Census of Population of Ireland paper form](https://lightweightform.io/wp-content/uploads/2017/11/65995_English_Household_2016_New_Version_Do_Not_Complete.pdf)
with all the fields, groups, behaviours, validations, and instructions.

## Try it

This example application is running online at:
https://lightweightform.io/static/census-ireland.

## Launch

Running `npm run dev` at the top-level will start a local server accessible at:
https://localhost:3001.

To only launch the `census-ireland` application run
`npm run dev -- --scope @lightweightform-app/census-ireland` instead.
