import { I18nPaths } from '@lightweightform/core';

import { createOptions } from '../utils/options-creator';

export const YES_NO_OPTIONS = createOptions(['Sim', 'Não']);
export const UNNAMED_PERSON = '[Sem nome]';

export const APP_I18N_PT_PT: I18nPaths = {
  '*': {
    documentTitle: 'Censo Populacional da Irlanda',
    appTitleLg: 'Censo Populacional da Irlanda',
    appTitleSm: 'Censo Irlanda',
    appSchemaModalTitle: 'Esquema',
    appValueModalTitle: 'Valor',
    actionsMenu: {
      save: 'Guardar',
      load: 'Carregar',
      validate: 'Validar',
      language: 'Idioma',
    },
    actionsMenuOptions: {
      language: {
        'en-US': 'Inglês',
        'pt-PT': 'Português',
      },
      debug: {
        schema: 'Mostrar esquema',
        value: 'Mostrar valor',
      },
    },
  },
};
