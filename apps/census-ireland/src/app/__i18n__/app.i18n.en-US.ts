import { I18nPaths } from '@lightweightform/core';

import { createOptions } from '../utils/options-creator';

export const YES_NO_OPTIONS = createOptions(['Yes', 'No']);
export const UNNAMED_PERSON = '[No name]';

export const APP_I18N_EN_US: I18nPaths = {
  '*': {
    documentTitle: 'Census of Population of Ireland',
    appTitleLg: 'Census of Population of Ireland',
    appTitleSm: 'Census Ireland',
    appSchemaModalTitle: 'Schema',
    appValueModalTitle: 'Value',
    actionsMenu: {
      save: 'Save',
      load: 'Load',
      validate: 'Validate',
      language: 'Language',
    },
    actionsMenuOptions: {
      save: { json: 'JSON', xml: 'XML' },
      load: { json: 'JSON', xml: 'XML' },
      language: {
        'en-US': 'English',
        'pt-PT': 'Portuguese',
      },
      debug: {
        schema: 'Show schema',
        value: 'Show value',
      },
    },
  },
};
