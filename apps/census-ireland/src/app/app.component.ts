import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  isDevMode,
  OnInit,
  ViewChild,
} from '@angular/core';
import {
  ActionsMenuAction,
  AppComponent as LfAppComponent,
  ModalComponent,
} from '@lightweightform/bootstrap-theme';
import {
  LfFileStorage,
  LfI18n,
  LfSerializer,
  LfStorage,
  LfUnloadAlert,
} from '@lightweightform/core';
import { getRowId } from '@lightweightform/storage';

import { CensusSerializer } from './services/serializer.service';

@Component({
  selector: 'census-app',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
  public actions: ActionsMenuAction[] = [
    {
      id: 'save',
      style: 'outline-secondary',
      icon: 'save',
      options: [
        { id: 'json', value: 'json' },
        { id: 'xml', value: 'xml' },
      ],
      callback: async (format: 'json' | 'xml') => {
        this.valueSerializer.useFormat(format);
        try {
          const dateStr = new Date().toISOString().replace(/[T:.]/g, '-');
          const fileName = `census-${dateStr}`;
          await this.lfFileStorage.saveToFile('/', fileName);
          console.log('Value saved successfully');
          this.lfStorage.setPristine('/');
        } catch (err) {
          console.error('Error saving file:', err);
        }
      },
    },
    {
      id: 'load',
      style: 'outline-secondary',
      icon: 'folder-open',
      isDisabled: () => !this.lfFileStorage.loadIsSupported,
      options: [
        { id: 'json', value: 'json' },
        { id: 'xml', value: 'xml' },
      ],
      callback: async (format: 'json' | 'xml') => {
        this.valueSerializer.useFormat(format);
        try {
          if (await this.lfFileStorage.loadFromFile('/')) {
            this.afterLoad();
            console.log('Value loaded successfully');
            this.lfStorage.setTouched('/', true);
          }
        } catch (err) {
          console.error('Error loading file:', err);
        }
      },
    },
    {
      id: 'language',
      style: 'outline-success',
      icon: 'language',
      options: this.lfI18n.languages.map((lang) => ({
        id: lang,
        value: lang,
        isActive: () => this.lfI18n.currentLanguage === lang,
      })),
      callback: (lang) => this.lfI18n.setCurrentLanguage(lang),
    },
    {
      id: 'validate',
      style: 'outline-danger',
      icon: 'check-square-o',
      callback: () => this.lfApp.validate(),
    },
    // Debug
    ...(isDevMode()
      ? ([
          {
            id: 'debug',
            style: 'outline-danger',
            icon: 'bug',
            callback: (show) =>
              show === 'schema'
                ? this.schemaModal.show()
                : this.valueModal.show(),
            options: [
              { id: 'schema', value: 'schema' },
              { id: 'value', value: 'value' },
            ],
          },
        ] as ActionsMenuAction[])
      : []),
  ];

  @ViewChild(LfAppComponent) private lfApp: LfAppComponent;
  // Schema and value modals
  @ViewChild('schemaModal')
  private schemaModal: ModalComponent;
  @ViewChild('valueModal') private valueModal: ModalComponent;

  constructor(
    public lfStorage: LfStorage,
    public lfI18n: LfI18n,
    private lfFileStorage: LfFileStorage,
    @Inject(LfSerializer) private valueSerializer: CensusSerializer,
    private lfUnloadAlert: LfUnloadAlert
  ) {}

  public ngOnInit() {
    if (!isDevMode()) {
      this.lfUnloadAlert.enable();
    }
  }

  /**
   * Should run after loading a saved file. Makes the people in the
   * relationships table reference the "row id" of the household people instead
   * of their index.
   */
  private afterLoad(): void {
    const household = this.lfStorage.get('/household');
    this.lfStorage.set(
      '/relationships/relationshipsTable',
      this.lfStorage
        .get('/relationships/relationshipsTable')
        .map(({ personOne, personTwo, relationship }) => {
          return {
            personOne:
              personOne === null ? null : getRowId(household[personOne]),
            personTwo:
              personTwo === null ? null : getRowId(household[personTwo]),
            relationship,
          };
        })
    );
  }
}
