import { RecordSchema, recordSchema } from '@lightweightform/storage';

import { accommodationSchema } from './components/accommodation/accommodation.schema';
import { householdSchema } from './components/household/household.schema';
import { relationshipsSchema } from './components/relationships/relationships.schema';

/**
 * Schema of the Census application.
 */
export const appSchema: RecordSchema = recordSchema(
  {
    accommodation: accommodationSchema,
    household: householdSchema,
    relationships: relationshipsSchema,
  },
  { xmlElementName: 'censusIreland' }
);
