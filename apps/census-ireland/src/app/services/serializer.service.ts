import { Inject, Injectable, Optional } from '@angular/core';
import {
  LfJsonSerializer,
  LfJsonSerializerOptions,
  LfSerializer,
  LfStorage,
  LF_JSON_SERIALIZER_OPTIONS,
} from '@lightweightform/core';
import { getRowId, Schema } from '@lightweightform/storage';
import {
  LfXmlSerializer,
  LfXmlSerializerOptions,
  LF_XML_SERIALIZER_OPTIONS,
} from '@lightweightform/xml-serializer';

/**
 * Custom serialiser which allows usage of both the JSON serialiser and the XML
 * serialiser by first specifying which serialiser to use. Further fixes
 * references to the household in the relationships table.
 */
@Injectable()
export class CensusSerializer extends LfSerializer {
  private jsonSerializer: LfJsonSerializer;
  private xmlSerializer: LfXmlSerializer;
  private serializer: LfSerializer;

  constructor(
    private lfStorage: LfStorage,
    @Optional()
    @Inject(LF_JSON_SERIALIZER_OPTIONS)
    jsonOptions: LfJsonSerializerOptions | null,
    @Optional()
    @Inject(LF_XML_SERIALIZER_OPTIONS)
    xmlOptions: LfXmlSerializerOptions | null
  ) {
    super();
    this.jsonSerializer = new LfJsonSerializer(jsonOptions);
    this.xmlSerializer = new LfXmlSerializer(xmlOptions, lfStorage);
    this.serializer = this.jsonSerializer;
  }

  // Obtain from the serializer currently in use
  public get charset(): string {
    return this.serializer.charset;
  }
  public get mimeType(): string {
    return this.serializer.mimeType;
  }
  public get accept(): string {
    return this.serializer.accept;
  }
  public get extension(): string {
    return this.serializer.extension;
  }
  public get autoBOM(): boolean {
    return this.serializer.autoBOM;
  }
  public get encode(): ((content: string) => BlobPart[]) | undefined {
    return this.serializer.encode;
  }

  /**
   * Specify which format should be handled: JSON or XML.
   * @param format Format to be handled by serialiser.
   */
  public useFormat(format: 'json' | 'xml'): void {
    this.serializer =
      format === 'json' ? this.jsonSerializer : this.xmlSerializer;
  }

  public serialize(js: any, schema: Schema, path: string): string {
    if (path === '/') {
      // Save relationship references with the index instead of the row id
      const household = this.lfStorage.get('/household');
      js.relationships.relationshipsTable.forEach((relationship) => {
        if (relationship.personOne !== null) {
          relationship.personOne = household.findIndex(
            (person) => getRowId(person) === relationship.personOne
          );
        }
        if (relationship.personTwo !== null) {
          relationship.personTwo = household.findIndex(
            (person) => getRowId(person) === relationship.personTwo
          );
        }
      });
    }
    return this.serializer.serialize(js, schema, path);
  }

  public deserialize(text: string, schema: Schema, path: string): any {
    return this.serializer.deserialize(text, schema, path);
  }
}
