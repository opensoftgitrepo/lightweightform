import { I18N_EN_US, I18N_PT_PT } from '@lightweightform/bootstrap-theme';
import { I18nLanguages, LfI18n } from '@lightweightform/core';

import { ACCOMMODATION_I18N_EN_US } from './components/accommodation/__i18n__/accommodation.i18n.en-US';
import { ACCOMMODATION_I18N_PT_PT } from './components/accommodation/__i18n__/accommodation.i18n.pt-PT';
import { EDUCATION_WORK_I18N_EN_US } from './components/household/person/education-work/__i18n__/education-work.i18n.en-US';
import { EDUCATION_WORK_I18N_PT_PT } from './components/household/person/education-work/__i18n__/education-work.i18n.pt-PT';
import { HEALTH_I18N_EN_US } from './components/household/person/health/__i18n__/health.i18n.en-US';
import { HEALTH_I18N_PT_PT } from './components/household/person/health/__i18n__/health.i18n.pt-PT';
import { LANGUAGE_CULTURE_I18N_EN_US } from './components/household/person/language-culture/__i18n__/language-culture.i18n.en-US';
import { LANGUAGE_CULTURE_I18N_PT_PT } from './components/household/person/language-culture/__i18n__/language-culture.i18n.pt-PT';
import { PERSON_I18N_EN_US } from './components/household/person/__i18n__/person.i18n.en-US';
import { PERSON_I18N_PT_PT } from './components/household/person/__i18n__/person.i18n.pt-PT';
import { HOUSEHOLD_I18N_EN_US } from './components/household/__i18n__/household.i18n.en-US';
import { HOUSEHOLD_I18N_PT_PT } from './components/household/__i18n__/household.i18n.pt-PT';
import { RELATIONSHIPS_I18N_EN_US } from './components/relationships/__i18n__/relationships.i18n.en-US';
import { RELATIONSHIPS_I18N_PT_PT } from './components/relationships/__i18n__/relationships.i18n.pt-PT';
import { APP_I18N_EN_US } from './__i18n__/app.i18n.en-US';
import { APP_I18N_PT_PT } from './__i18n__/app.i18n.pt-PT';

export const appI18n: I18nLanguages = {
  'en-US': LfI18n.mergeTranslations(
    I18N_EN_US,
    APP_I18N_EN_US,
    ACCOMMODATION_I18N_EN_US,
    HOUSEHOLD_I18N_EN_US,
    PERSON_I18N_EN_US,
    EDUCATION_WORK_I18N_EN_US,
    HEALTH_I18N_EN_US,
    LANGUAGE_CULTURE_I18N_EN_US,
    RELATIONSHIPS_I18N_EN_US
  ),
  'pt-PT': LfI18n.mergeTranslations(
    I18N_PT_PT,
    APP_I18N_PT_PT,
    ACCOMMODATION_I18N_PT_PT,
    HOUSEHOLD_I18N_PT_PT,
    PERSON_I18N_PT_PT,
    EDUCATION_WORK_I18N_PT_PT,
    HEALTH_I18N_PT_PT,
    LANGUAGE_CULTURE_I18N_PT_PT,
    RELATIONSHIPS_I18N_PT_PT
  ),
};
