/**
 * Creates i18n options given the labels of each option.
 * @param labels List of option labels.
 * @returns Options.
 */
export function createOptions(labels) {
  return labels.map((label, i) => {
    let value;
    let code;
    value = code = `${i + 1}`;
    return { value, code, label };
  });
}
