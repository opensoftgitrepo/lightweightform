/**
 * This app's options always have the same structure. Since the label of the
 * options comes from the i18n files, we use this function to simplify the
 * creation of field options.
 * @param nOptions Number of options to create.
 * @param nullOption Whether the first option should be a `null` value (empty
 * option).
 * @returns List of field options.
 */
export function createAllowedValues(nOptions: number): string[] {
  const options: string[] = [];
  for (let i = 1; i <= nOptions; ++i) {
    options.push(`${i}`);
  }
  return options;
}
