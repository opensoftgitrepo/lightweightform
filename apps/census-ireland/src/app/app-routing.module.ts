import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {
  LfRouter,
  LfRoutes,
  LF_I18N_LANGUAGE_QUERY_PARAM,
  LF_ROUTER_BASE_PATH,
} from '@lightweightform/core';

import { AccommodationComponent } from './components/accommodation/accommodation.component';
import { HouseholdComponent } from './components/household/household.component';
import { EducationWorkComponent } from './components/household/person/education-work/education-work.component';
import { HealthComponent } from './components/household/person/health/health.component';
import { LanguageCultureComponent } from './components/household/person/language-culture/language-culture.component';
import { PersonComponent } from './components/household/person/person.component';
import { RelationshipsComponent } from './components/relationships/relationships.component';

/**
 * Application routes.
 */
const routes: LfRoutes = [
  { path: '', redirectTo: 'accommodation', pathMatch: 'full' },
  { path: 'accommodation', component: AccommodationComponent },
  { path: 'household', component: HouseholdComponent },
  { path: 'household/:personId', component: PersonComponent },
  {
    path: 'household/:personId/languageCulture',
    component: LanguageCultureComponent,
  },
  { path: 'household/:personId/health', component: HealthComponent },
  {
    path: 'household/:personId/educationWork',
    component: EducationWorkComponent,
  },
  { path: 'relationships', component: RelationshipsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    LfRouter,
    { provide: LF_I18N_LANGUAGE_QUERY_PARAM, useValue: 'l' },
    { provide: LF_ROUTER_BASE_PATH, useValue: '/' },
  ],
})
export class AppRoutingModule {}
