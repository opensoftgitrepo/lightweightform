import {
  numberSchema,
  RecordSchema,
  recordSchema,
  Storage,
  stringSchema,
} from '@lightweightform/storage';

import { createAllowedValues } from '../../utils/allowed-values-creator';

/**
 * Schema of the accommodation form.
 */
export const accommodationSchema: RecordSchema = recordSchema({
  h1Select: stringSchema({
    isNullable: true,
    isRequired: true,
    allowedValues: createAllowedValues(7),
  }),
  h2Select: stringSchema({
    isNullable: true,
    isRequired: true,
    allowedValues: createAllowedValues(9),
  }),
  h3Select: stringSchema({
    isNullable: true,
    isRequired: true,
    allowedValues: createAllowedValues(4),
  }),
  h3OptionalSelect: stringSchema({
    isNullable: true,
    isRequired: buildingIsRented,
    allowedValues: createAllowedValues(3),
  }),
  h4Number: numberSchema({
    isInteger: true,
    isNullable: true,
    isRequired: buildingIsRented,
    min: 0,
  }),
  h4Select: stringSchema({
    isNullable: true,
    isRequired: buildingIsRented,
    allowedValues: createAllowedValues(3),
  }),
  h5Number: numberSchema({
    isInteger: true,
    isNullable: true,
    isRequired: true,
    min: 0,
    max: 99,
  }),
  h6Select: stringSchema({
    isNullable: true,
    isRequired: true,
    allowedValues: createAllowedValues(9),
  }),
  h7Select: stringSchema({
    isNullable: true,
    isRequired: true,
    allowedValues: createAllowedValues(5),
  }),
  h8Select: stringSchema({
    isNullable: true,
    isRequired: true,
    allowedValues: createAllowedValues(5),
  }),
  h9Select: stringSchema({
    isNullable: true,
    isRequired: true,
    allowedValues: createAllowedValues(5),
  }),
  h10Radio: stringSchema({
    isNullable: true,
    isRequired: true,
    allowedValues: createAllowedValues(2),
  }),
  h11Select: stringSchema({
    isNullable: true,
    isRequired: true,
    allowedValues: createAllowedValues(3),
  }),
});

/**
 * Whether the building is rented.
 * @param ctx Storage with relative path set to the accommodation form.
 * @returns Whether building is rented.
 */
function buildingIsRented(ctx: Storage): boolean {
  return ctx.get('h3Select') === '3';
}
