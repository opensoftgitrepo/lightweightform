import { I18nPaths } from '@lightweightform/core';

import { createOptions } from '../../../utils/options-creator';
import { YES_NO_OPTIONS } from '../../../__i18n__/app.i18n.en-US';

export const ACCOMMODATION_I18N_EN_US: I18nPaths = {
  '/accommodation': {
    documentTitle: 'Accommodation | Census of Population of Ireland',
    label: 'Accommodation',
    helpMessage: `
      <h5>Who should complete the Census Form?</h5>

      <p>
        The householder or any adult member of the household present should
        complete this form. A separate household Form should be completed for
        every household.

      <p>
        A household is:
        <ul>
          <li>one person living alone, or</li>
          <li>
            a group of related or unrelated people living at the same address
            with common housekeeping arrangements, meaning they share at least
            one meal a day or share a living or sitting room.
          </li>
        </ul>
  `,
  },
  '/accommodation/h1Select': {
    code: 'H1',
    label: 'What type of accommodation does your household occupy?',
    options: createOptions([
      'Detached',
      'Semi-detached',
      'Terraced (including end of terrace)',
      'In a purpose-built block',
      'Part of a converted house or commercial building',
      'Bed-sit (with some shared facilities e.g. toilet)',
      'A caravan or other mobile or temporary structure',
    ]),
  },
  '/accommodation/h2Select': {
    code: 'H2',
    label: 'When was your house, flat, or apartment first built?',
    helpMessage:
      'Specify the year in which the building was first built even if it was ' +
      'subsequently converted, extended, or renovated.',
    options: createOptions([
      'Before 1919',
      '1919–1945 inclusive',
      '1946–1960 inclusive',
      '1961–1970 inclusive',
      '1971–1980 inclusive',
      '1981–1990 inclusive',
      '1991–2000 inclusive',
      '2001–2010 inclusive',
      '2011 or later',
    ]),
  },
  '/accommodation/h3Select': {
    code: 'H3',
    label: 'Does your household own or rent your accommodation?',
    options: createOptions([
      'Own with mortgage or loan',
      'Own outright',
      'Rent',
      'Live rent-free',
    ]),
  },
  '/accommodation/h3OptionalSelect': {
    label: 'If renting, who is your landlord?',
    options: createOptions([
      'Private landlord',
      'Local authority',
      'Voluntary/co-operative housing body',
    ]),
  },
  '/accommodation/h4Number': {
    code: 'H4',
    suffix: ' €',
    label:
      'If your accommodation is rented, how much rent does your household pay?',
    legend: 'Enter amount to the nearest Euro',
    thousandsSeparator: ',',
  },
  '/accommodation/h4Select': {
    label: 'Rent payment is per:',
    options: createOptions(['Per week', 'Per month', 'Per year']),
  },
  '/accommodation/h5Number': {
    code: 'H5',
    label: 'How many rooms do you have for use by your household exclusively?',
    helpMessage:
      'Do NOT count bathrooms, toilets, kitchenettes, utility rooms, ' +
      'consulting rooms, offices, shops, halls or landings, or rooms that ' +
      'can only be used for storage such as cupboards. Do count all other ' +
      'rooms such as kitchens, living rooms, bedrooms, conservatories that ' +
      'you can sit in, and studies. If two rooms have been converted into ' +
      'one, count them as one room.',
  },
  '/accommodation/h6Select': {
    code: 'H6',
    label:
      'What is the main type of fuel used by the central heating in your ' +
      'accommodation?',
    options: createOptions([
      'No central heating',
      'Oil',
      'Natural gas',
      'Electricity',
      'Coal (including anthracite)',
      'Peat (including turf)',
      'Liquid petroleum gas (LPG)',
      'Wood (including wood pellets)',
      'Other',
    ]),
  },
  '/accommodation/h7Select': {
    code: 'H7',
    label: 'What type of piped water supply does your accommodation have?',
    options: createOptions([
      'Connection to a public main',
      'Connection to a group water scheme with a public source of supply',
      'Connection to a group water scheme with a private source of supply ' +
        '(e.g. borehole, lake, etc.)',
      'Connection to other private source (e.g. well, lake, rainwater tank, ' +
        'etc.)',
      'No piped water supply',
    ]),
  },
  '/accommodation/h8Select': {
    code: 'H8',
    label: 'What type of sewerage facility does your accommodation have?',
    options: createOptions([
      'Public sewerage scheme',
      'Individual septic tank',
      'Individual treatment system other than a septic tank',
      'Other sewerage facility',
      'No sewerage facility',
    ]),
  },
  '/accommodation/h9Select': {
    code: 'H9',
    label:
      'How many cars or vans are owned or are available for use by one or ' +
      'more members of the household?',
    helpMessage: 'Include any company car or van if available for private use.',
    options: createOptions(['One', 'Two', 'Three', 'Four or more', 'None']),
  },
  '/accommodation/h10Radio': {
    code: 'H10',
    label: 'Does your household have a personal computer (PC)?',
    options: YES_NO_OPTIONS,
  },
  '/accommodation/h11Select': {
    code: 'H11',
    label: 'Does your household have access to the Internet?',
    options: createOptions([
      'Yes, Broadband connection',
      'Yes, other connection',
      'No',
    ]),
  },
};
