import { I18nPaths } from '@lightweightform/core';

import { createOptions } from '../../../utils/options-creator';
import { YES_NO_OPTIONS } from '../../../__i18n__/app.i18n.pt-PT';

export const ACCOMMODATION_I18N_PT_PT: I18nPaths = {
  '/accommodation': {
    documentTitle: 'Alojamento | Censo Populacional da Irlanda',
    label: 'Alojamento',
    validations: {
      EMPTY_ACCOMMODATION:
        'Por favor confirme que respondeu a todas as questões relativas ao ' +
        'seu alojamento.',
    },
    helpMessage: `
    <h5>Quem é que deve responder ao Census?</h5>

    <p>
      O proprietário do alojamento ou outro adulto presente que pertença ao
      agregado deve completar o formulário. Um formulário separado deve ser
      completado para cada agregado.

    <p>
      Um agregado é:
      <ul>
        <li>um indivíduo que viva sozinho ou</li>
        <li>
          um grupo de indivíduos relacionados ou não relacionados que morem no
          mesmo endereço com tarefas domiciliárias comuns, o que significa que
          partilham uma refeição por dia ou partilham uma sala de estar.
        </li>
      </ul>
`,
  },
  '/accommodation/h1Select': {
    label: 'Qual o tipo de alojamento ocupado pelo seu agregado?',
    options: createOptions([
      'Isolado',
      'Semi-isolado',
      'Casa geminada (incluindo a casa inicial/final)',
      'Apartamento construído como tal',
      'Parte de uma casa convertida ou edifício comercial',
      'Quarto conjugado (com instalações partilhadas e.g. quarto de banho)',
      'Uma caravana ou outro alojamento móvel ou estrutura temporária',
    ]),
  },
  '/accommodation/h2Select': {
    label: 'Quando é que o seu edifício foi inicialmente contruído?',
    helpMessage:
      'Especifique o ano em que o edifício foi inicialmente construído ' +
      'mesmo que este tenha sido posteriormente convertido, estendido ou ' +
      'renovado.',
    options: createOptions([
      'Antes de 1919',
      '1919–1945 inclusive',
      '1946–1960 inclusive',
      '1961–1970 inclusive',
      '1971–1980 inclusive',
      '1981–1990 inclusive',
      '1991–2000 inclusive',
      '2001–2010 inclusive',
      '2011 ou depois',
    ]),
  },
  '/accommodation/h3Select': {
    label: 'O seu alojamento é comprado ou arrendado pelo agregado?',
    options: createOptions([
      'Comprado (com hipotéca ou empréstimo em curso)',
      'Comprado (sem dívidas associadas)',
      'Arrendado',
      'Vive sem pagar renda',
    ]),
  },
  '/accommodation/h3OptionalSelect': {
    label: 'Se está a arrendar, quem é o seu senhorio?',
    options: createOptions([
      'Senhorio privado',
      'Autoridade local',
      'Entidade de alojamento voluntária/cooperativa',
    ]),
  },
  '/accommodation/h4Number': {
    label:
      'Se o alojamento é arrendado, qual o valor da renda paga pelo agregado?',
    legend: 'Introduza o valor aproximado ao Euro',
    thousandsSeparator: ' ',
  },
  '/accommodation/h4Select': {
    label: 'Pagamento da renda por:',
    options: createOptions(['Por semana', 'Por mês', 'Por ano']),
  },
  '/accommodation/h5Number': {
    label: 'Quantos quartos existem para uso exclusivo pelo agregado?',
    helpMessage:
      'Não conte quartos de banho, kitchenettes, despensas, consultórios, ' +
      'escritórios empresariais, lojas, corredores ou quartos que apenas ' +
      'podem ser utilizados para arrumação. Conte todos os outros quartos ' +
      'como cozinhas, salas de jantar, quartos de dormir ou escritórios ' +
      'pessoais. Se dois quartos tiverem sido convertidos em um só, conte-os ' +
      'como um só quarto.',
  },
  '/accommodation/h6Select': {
    label:
      'Qual o principal tipo de combustível utilizado pelo aquecimento ' +
      'central do seu alojamento?',
    options: createOptions([
      'Não possui aquecimento central',
      'Petróleo',
      'Gás natural',
      'Eletricidade',
      'Carvão (incluindo antracite)',
      'Turfa',
      'Gás liquefeito de petróleo (GLP)',
      'Madeira (incluindo paletes de madeira)',
      'Outro',
    ]),
  },
  '/accommodation/h7Select': {
    label: 'De que tipo de água canalizada é que o seu alojamento dispõe?',
    options: createOptions([
      'Conectada à rede pública',
      'Conectada a um esquema em grupo com uma fonte de água pública',
      'Conectada a um esquema em grupo com uma fonte de água privada (e.g. ' +
        'furo, lago, etc.)',
      'Conectada a outra fonte de água privada (e.g. poço, lago, tanque, ' +
        'etc.)',
      'Não possui água canalizada',
    ]),
  },
  '/accommodation/h8Select': {
    label:
      'De que tipo de instalação de esgotos é que o seu alojamento dispõe?',
    options: createOptions([
      'Serviço de esgotos público',
      'Tanque séptico individual',
      'Sistema de tratamento individual que não um tanque séptico',
      'Outro tipo de instalação',
      'Nenhuma instalação',
    ]),
  },
  '/accommodation/h9Select': {
    label:
      'Quantos carros ou carrinhas é que pertencem ou estão disponíveis a um ' +
      'ou mais membros do agregado?',
    helpMessage:
      'Inclua qualquer carro ou carrinha empresarial se disponível para uso ' +
      'privado.',
    options: createOptions(['Um', 'Dois', 'Três', 'Quatro ou mais', 'Nenhum']),
  },
  '/accommodation/h10Radio': {
    label: 'O seu agregado dispõe de um computador pessoal (PC)?',
    options: YES_NO_OPTIONS,
  },
  '/accommodation/h11Select': {
    label: 'O seu agregado dispõe de acesso à Internet?',
    options: createOptions([
      'Sim, conexão banda larga',
      'Sim, outro tipo de conexão',
      'Não',
    ]),
  },
};
