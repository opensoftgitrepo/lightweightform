import {
  numberSchema,
  RecordSchema,
  recordSchema,
  Storage,
  stringSchema,
  tableSchema,
} from '@lightweightform/storage';

import { createAllowedValues } from '../../utils/allowed-values-creator';

export const relationshipsSchema: RecordSchema = recordSchema({
  relationshipsTable: tableSchema(
    recordSchema({
      personOne: numberSchema({
        isInteger: true,
        isNullable: true,
        isRequired: true,
        allowedValues: (ctx) => ctx.childrenIds('/household') as number[],
      }),
      personTwo: numberSchema({
        isInteger: true,
        isNullable: true,
        isRequired: true,
        allowedValues: (ctx) => ctx.childrenIds('/household') as number[],
      }),
      relationship: stringSchema({
        isNullable: true,
        isRequired: true,
        allowedValues: createAllowedValues(11),
      }),
    }),
    { maxSize: maxNumberOfRelationships }
  ),
});

function maxNumberOfRelationships(ctx: Storage): number {
  const household = ctx.get('/household');
  return (household.length * (household.length - 1)) / 2;
}
