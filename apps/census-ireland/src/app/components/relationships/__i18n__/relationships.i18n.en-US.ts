import { I18nPaths, LfStorage } from '@lightweightform/core';
import { getRowId } from '@lightweightform/storage';

import { createOptions } from '../../../utils/options-creator';
import { UNNAMED_PERSON } from '../../../__i18n__/app.i18n.en-US';

export const RELATIONSHIPS_I18N_EN_US: I18nPaths = {
  '/relationships': {
    documentTitle: 'Relationships | Census of Population of Ireland',
    label: 'Relationships',
  },
  '/relationships/relationshipsTable': {
    code: 'P4',
    label: 'Household relationships',
    columnLabels: {
      personOne: 'Person 1',
      personTwo: 'Person 2',
      relationship: 'Relationship of person 2 to person 1',
    },
    addRowActionText: 'Add relationship',
    removeRowsActionText: 'Remove relationships',
    noRowsText: 'No registered relationships.',
  },
  '/relationships/relationshipsTable/?/personOne': {
    options: personOptions,
  },
  '/relationships/relationshipsTable/?/personTwo': {
    options: personOptions,
  },
  '/relationships/relationshipsTable/?/relationship': {
    options: createOptions([
      'Husband or wife',
      'Partner (incl. same-sex partner)',
      'Son or daughter',
      'Step-child',
      'Brother or sister',
      'Mother or father',
      'Grandparent',
      'Step-mother/-father',
      'Son-/daughter-in-law',
      'Grandchild',
      'Other',
    ]),
  },
};

function personOptions(ctx: LfStorage) {
  return ctx.get('/household').map((person) => ({
    value: getRowId(person),
    label: person.p1Text || UNNAMED_PERSON,
  }));
}
