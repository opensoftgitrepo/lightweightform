import { I18nPaths, LfStorage } from '@lightweightform/core';
import { getRowId } from '@lightweightform/storage';

import { createOptions } from '../../../utils/options-creator';
import { UNNAMED_PERSON } from '../../../__i18n__/app.i18n.pt-PT';

export const RELATIONSHIPS_I18N_PT_PT: I18nPaths = {
  '/relationships': {
    documentTitle: 'Relacionamentos | Censo Populacional da Irlanda',
    label: 'Relações',
  },
  '/relationships/relationshipsTable': {
    label: 'Relações entre o agregado',
    columnLabels: {
      personOne: 'Pessoa 1',
      personTwo: 'Pessoa 2',
      relationship: 'Relação da pessoa 2 para a pessoa 1',
    },
    addRowActionText: 'Adicionar relação',
    removeRowsActionText: 'Remover relações',
    noRowsText: 'Nenhuma relação introduzida.',
  },
  '/relationships/relationshipsTable/?/personOne': {
    options: personOptions,
  },
  '/relationships/relationshipsTable/?/personTwo': {
    options: personOptions,
  },
  '/relationships/relationshipsTable/?/relationship': {
    options: createOptions([
      'Marido ou mulher',
      'Parceiro/a (incl. do mesmo sexo)',
      'Filho/a',
      'Enteado/a',
      'Irmão ou irmã',
      'Mãe ou pai',
      'Avô/ó',
      'Madastra ou padastro',
      'Genro ou nora',
      'Neto/a',
      'Outro',
    ]),
  },
};

function personOptions(ctx: LfStorage) {
  return ctx.get('/household').map((person) => ({
    value: getRowId(person),
    label: person.p1Text || UNNAMED_PERSON,
  }));
}
