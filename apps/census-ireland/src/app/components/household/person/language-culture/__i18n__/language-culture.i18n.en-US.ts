import { I18nPaths } from '@lightweightform/core';

import { createOptions } from '../../../../../utils/options-creator';
import { YES_NO_OPTIONS } from '../../../../../__i18n__/app.i18n.en-US';

export const LANGUAGE_CULTURE_I18N_EN_US: I18nPaths = {
  '/household/?/languageCulture': {
    documentTitle: (ctx) =>
      `Language and Culture | ${
        ctx.get('../p1Text') || 'Person'
      } | Census of Population of Ireland`,
    label: 'Language and Culture',
  },
  '/household/?/languageCulture/p10CheckboxGroup': {
    code: 'P10',
    label: 'Nationality',
    helpMessage:
      'If you have more than one nationality please declare all of them',
    options: createOptions(['Irish', 'Other nationality', 'No nationality']),
    validations: {
      INVALID_P10:
        'Cannot select "no nationality" when nationalities are selected.',
    },
  },
  '/household/?/languageCulture/p10Text': {
    label: 'Other nationality',
  },
  '/household/?/languageCulture/p11Select': {
    code: 'P11',
    label: 'Ethnic or cultural background',
    options: createOptions([
      // A. White
      'Irish',
      'Irish traveller',
      'Any other White background',
      // B. Black or Black Irish
      'African',
      'Any other Black background',
      // C. Asian or Asian Irish
      'Chinese',
      'Any other Asian background',
      // D. Other, including mixed background
      'Other',
    ]),
  },
  '/household/?/languageCulture/p11Text': {
    label: 'Other',
  },
  '/household/?/languageCulture/p12Select': {
    code: 'P12',
    label: 'Religion',
    options: createOptions([
      'Roman Catholic',
      'Church of Ireland',
      'Islamic',
      'Presbyterian',
      'Orthodox',
      'Other',
      'No religion',
    ]),
  },
  '/household/?/languageCulture/p12Text': {
    label: 'Other',
  },
  '/household/?/languageCulture/p13Number': {
    code: 'P13',
    label: 'How many children has the person given birth to?',
    helpMessage: 'Answer in number of children born',
    legend: 'This question is for women only',
  },
  '/household/?/languageCulture/p14Radio': {
    code: 'P14',
    label: 'Can the person speak Irish?',
    options: YES_NO_OPTIONS,
  },
  '/household/?/languageCulture/p14Select': {
    label: 'How often does the person speak Irish?',
    options: createOptions([
      'Daily, within the education system',
      'Daily, outside of the education system',
      'Weekly',
      'Less often',
      'Never',
    ]),
  },
  '/household/?/languageCulture/p15Radio': {
    code: 'P15',
    label:
      'Does the person speak a language other than English or Irish at home?',
    options: YES_NO_OPTIONS,
  },
  '/household/?/languageCulture/p15Text': {
    label: 'What language?',
  },
  '/household/?/languageCulture/p15Select': {
    label: 'How well does the person speak English?',
    options: createOptions([
      'Very well',
      'Well',
      'Not well',
      'Does not speak English at all',
    ]),
  },
};
