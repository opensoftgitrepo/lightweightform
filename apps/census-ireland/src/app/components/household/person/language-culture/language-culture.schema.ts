import {
  listSchema,
  numberSchema,
  RecordSchema,
  recordSchema,
  Storage,
  stringSchema,
  ValidationIssue,
} from '@lightweightform/storage';
import { isAfter, subYears } from 'date-fns/esm';

import { createAllowedValues } from '../../../../utils/allowed-values-creator';

export const languageCultureSchema: RecordSchema = recordSchema({
  p10CheckboxGroup: listSchema(
    stringSchema({
      validate: p10IsValid,
      allowedValues: createAllowedValues(3),
    }),
    { minSize: 1, validate: p10IsValid }
  ),
  p10Text: stringSchema({ minLength: (ctx) => +hasOtherNationality(ctx) }),
  p11Select: stringSchema({
    isNullable: true,
    isRequired: true,
    allowedValues: createAllowedValues(8),
  }),
  p11Text: stringSchema({
    minLength: (ctx) => +hasOtherCulturalBackground(ctx),
  }),
  p12Select: stringSchema({
    isNullable: true,
    isRequired: true,
    allowedValues: createAllowedValues(7),
  }),
  p12Text: stringSchema({ minLength: (ctx) => +hasOtherReligion(ctx) }),
  p13Number: numberSchema({
    isInteger: true,
    isNullable: true,
    isRequired: isFemale,
    min: 0,
    max: 99,
  }),
  p14Radio: stringSchema({
    isNullable: true,
    isRequired: is3YearsOrOlder,
    allowedValues: createAllowedValues(2),
  }),
  p14Select: stringSchema({
    isNullable: true,
    isRequired: speaksIrish,
    allowedValues: createAllowedValues(5),
  }),
  p15Radio: stringSchema({
    isNullable: true,
    isRequired: true,
    allowedValues: createAllowedValues(2),
  }),
  p15Text: stringSchema({ minLength: (ctx) => +speaksOtherLanguage(ctx) }),
  p15Select: stringSchema({
    isNullable: true,
    isRequired: speaksOtherLanguage,
    allowedValues: createAllowedValues(4),
  }),
});

function p10IsValid(ctx: Storage): ValidationIssue | undefined {
  const p10 = ctx.get();
  if (
    p10.indexOf('3') !== -1 &&
    (p10.indexOf('2') !== -1 || p10.indexOf('1') !== -1)
  ) {
    return { code: 'INVALID_P10' };
  }
}

export function hasOtherNationality(ctx: Storage): boolean {
  return ctx.get('p10CheckboxGroup').indexOf('2') !== -1;
}

function isFemale(ctx: Storage): boolean {
  return ctx.get('../p2Radio') === '2';
}

function is3YearsOrOlder(ctx: Storage): boolean {
  return !isAfter(ctx.get('../p3Date'), subYears(Date.now(), 3));
}

function hasOtherCulturalBackground(ctx: Storage): boolean {
  return ctx.get('p11Select') === '8';
}

export function hasOtherReligion(ctx: Storage): boolean {
  return ctx.get('p12Select') === '6';
}

function speaksIrish(ctx: Storage): boolean {
  return ctx.get('p14Radio') === '1';
}

function speaksOtherLanguage(ctx: Storage): boolean {
  return ctx.get('p15Radio') === '1';
}
