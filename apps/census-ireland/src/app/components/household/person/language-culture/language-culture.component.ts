import { Component } from '@angular/core';
import { LfRouter } from '@lightweightform/core';
import { makeObservable } from 'mobx';
import { computed } from 'mobx-angular';

@Component({
  selector: 'census-language-culture',
  templateUrl: './language-culture.component.html',
})
export class LanguageCultureComponent {
  constructor(private lfRouter: LfRouter) {
    makeObservable(this);
  }

  @computed
  public get path() {
    const { personId } = this.lfRouter.params;
    return `/household/${personId}/languageCulture`;
  }
}
