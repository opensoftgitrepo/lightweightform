import { I18nPaths } from '@lightweightform/core';

import { createOptions } from '../../../../../utils/options-creator';
import { YES_NO_OPTIONS } from '../../../../../__i18n__/app.i18n.pt-PT';

export const LANGUAGE_CULTURE_I18N_PT_PT: I18nPaths = {
  '/household/?/languageCulture': {
    documentTitle: (ctx) =>
      `Língua e Cultura | ${
        ctx.get('../p1Text') || 'Person'
      } | Censo Populacional da Irlanda`,
    label: 'Língua e Cultura',
  },
  '/household/?/languageCulture/p10CheckboxGroup': {
    label: 'Nacionalidade',
    helpMessage:
      'Se tem mais do que uma nacionalidade, por favor declare-as todas',
    options: createOptions([
      'Irlandesa',
      'Outra nacionalidade',
      'Nenhuma nacionalidade',
    ]),
    validations: {
      INVALID_P10:
        'Não pode selecionar "nenhuma nacionalidade" quando selecionou ' +
        'outras nacionalidades.',
    },
  },
  '/household/?/languageCulture/p10Text': {
    label: 'Outra nacionalidade',
  },
  '/household/?/languageCulture/p11Select': {
    label: 'Origem étnica ou cultural',
    options: createOptions([
      // A. Caucasiana
      'Irlandesa',
      'Viajante irlandesa',
      'Qualquer outra origem caucasiana',
      // B. Negra ou negra-irlandesa
      'Africana',
      'Qualquer outra origem negra',
      // C. Asiática ou ásio-irlandesa
      'Chinesa',
      'Qualquer outra origem asiática',
      // D. Other, including mixed background
      'Outra',
    ]),
  },
  '/household/?/languageCulture/p11Text': {
    label: 'Outra',
  },
  '/household/?/languageCulture/p12Select': {
    label: 'Religião',
    options: createOptions([
      'Católico Romana',
      'Igreja da Irlanda',
      'Islâmica',
      'Presbiteriana',
      'Ortodoxa',
      'Outra',
      'Nenhuma religião',
    ]),
  },
  '/household/?/languageCulture/p12Text': {
    label: 'Outra',
  },
  '/household/?/languageCulture/p13Number': {
    label: 'A quantas crianças é que a pessoa já deu à luz?',
    helpMessage: 'Responda em número de crianças nascidas',
    legend: 'Esta questão aplica-se apenas a mulheres',
  },
  '/household/?/languageCulture/p14Radio': {
    label: 'A pessoa fala Irlandês?',
    options: YES_NO_OPTIONS,
  },
  '/household/?/languageCulture/p14Select': {
    label: 'Com que frequência é que a pessoa fala Irlandês?',
    options: createOptions([
      'Diariamente, dentro do sistema de educação',
      'Diariamente, fora do sistema de educação',
      'Semanalmente',
      'Com menos frequência',
      'Nunca',
    ]),
  },
  '/household/?/languageCulture/p15Radio': {
    label: 'A pessoa fala alguma língua que não Inglês ou Irlandês em casa?',
    options: YES_NO_OPTIONS,
  },
  '/household/?/languageCulture/p15Text': {
    label: 'Que língua?',
  },
  '/household/?/languageCulture/p15Select': {
    label: 'Quão bem é que a pessoa fala Inglês?',
    options: createOptions([
      'Muito bem',
      'Bem',
      'Mal',
      'Não fala Inglês de todo',
    ]),
  },
};
