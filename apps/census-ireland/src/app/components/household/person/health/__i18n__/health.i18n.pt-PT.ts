import { I18nPaths } from '@lightweightform/core';

import { createOptions } from '../../../../../utils/options-creator';
import { YES_NO_OPTIONS } from '../../../../../__i18n__/app.i18n.pt-PT';

export const HEALTH_I18N_PT_PT: I18nPaths = {
  '/household/?/health': {
    documentTitle: (ctx) =>
      `Saúde | ${
        ctx.get('../p1Text') || 'Person'
      } | Censo Populacional da Irlanda`,
    label: 'Saúde',
    P16_HEADER:
      'P16 - O indivíduo sofre de alguma das seguintes condições ou ' +
      'dificuldades de longa duração?',
    P17_HEADER:
      'P17 - Se respondeu "Sim" a alguma das categorias especificadas em ' +
      'P16, o indivíduo tem alguma dificuldade em realizar algo do que se ' +
      'segue?',
    P18_HEADER: 'P18 - Como é que é, em geral, a saúde do indivíduo?',
  },
  '/household/?/health/p16RadioA': {
    label: '(a) Cegueira ou deficiência grave na visão',
    options: YES_NO_OPTIONS,
  },
  '/household/?/health/p16RadioB': {
    label: '(b) Surdez ou deficiência auditiva grave',
    options: YES_NO_OPTIONS,
  },
  '/household/?/health/p16RadioC': {
    label:
      '(c) Uma dificuldade com atividades físicas básicas como andar, subir ' +
      'escadas, alcançar, levantar ou carregar',
    options: YES_NO_OPTIONS,
  },
  '/household/?/health/p16RadioD': {
    label: '(d) Uma deficiência intelectual',
    options: YES_NO_OPTIONS,
  },
  '/household/?/health/p16RadioE': {
    label: '(e) Uma dificuldade em aprender, lembrar ou concentrar',
    options: YES_NO_OPTIONS,
  },
  '/household/?/health/p16RadioF': {
    label: '(f) Uma condição psicológica ou emocional',
    options: YES_NO_OPTIONS,
  },
  '/household/?/health/p16RadioG': {
    label:
      '(g) Uma dificuldade com dor, respiração ou qualquer outra doença ou ' +
      'condição crónica',
    options: YES_NO_OPTIONS,
  },
  '/household/?/health/p17RadioA': {
    label: '(a) Vestir-se, tomar banho ou movimentar-se dentro de casa',
    options: YES_NO_OPTIONS,
  },
  '/household/?/health/p17RadioB': {
    label: '(b) Sair de casa sozinho para ir às compras ou visitar o médico',
    options: YES_NO_OPTIONS,
  },
  '/household/?/health/p17RadioC': {
    label:
      '(C) Trabalhar num emprego ou negócio ou frequentar escola ou faculdade',
    options: YES_NO_OPTIONS,
  },
  '/household/?/health/p17RadioD': {
    label:
      '(D) Participar em outras atividades como lazer ou utilizar transportes',
    options: YES_NO_OPTIONS,
  },
  '/household/?/health/p18Radio': {
    label: 'Como é que é, em geral, a saúde do indivíduo?',
    options: createOptions(['Muito boa', 'Boa', 'Razoável', 'Má', 'Muito má']),
  },
};
