import {
  RecordSchema,
  recordSchema,
  Storage,
  stringSchema,
} from '@lightweightform/storage';

import { createAllowedValues } from '../../../../utils/allowed-values-creator';

export const healthSchema: RecordSchema = recordSchema({
  p16RadioA: stringSchema({
    isNullable: true,
    isRequired: true,
    allowedValues: createAllowedValues(2),
  }),
  p16RadioB: stringSchema({
    isNullable: true,
    isRequired: true,
    allowedValues: createAllowedValues(2),
  }),
  p16RadioC: stringSchema({
    isNullable: true,
    isRequired: true,
    allowedValues: createAllowedValues(2),
  }),
  p16RadioD: stringSchema({
    isNullable: true,
    isRequired: true,
    allowedValues: createAllowedValues(2),
  }),
  p16RadioE: stringSchema({
    isNullable: true,
    isRequired: true,
    allowedValues: createAllowedValues(2),
  }),
  p16RadioF: stringSchema({
    isNullable: true,
    isRequired: true,
    allowedValues: createAllowedValues(2),
  }),
  p16RadioG: stringSchema({
    isNullable: true,
    isRequired: true,
    allowedValues: createAllowedValues(2),
  }),
  p17RadioA: stringSchema({
    isNullable: true,
    isRequired: hasAnyCondition,
    allowedValues: createAllowedValues(2),
  }),
  p17RadioB: stringSchema({
    isNullable: true,
    isRequired: hasAnyCondition,
    allowedValues: createAllowedValues(2),
  }),
  p17RadioC: stringSchema({
    isNullable: true,
    isRequired: hasAnyCondition,
    allowedValues: createAllowedValues(2),
  }),
  p17RadioD: stringSchema({
    isNullable: true,
    isRequired: hasAnyCondition,
    allowedValues: createAllowedValues(2),
  }),
  p18Radio: stringSchema({
    isNullable: true,
    isRequired: true,
    allowedValues: createAllowedValues(5),
  }),
});

function hasAnyCondition(ctx: Storage): boolean {
  return (
    ctx.get('p16RadioA') === '1' ||
    ctx.get('p16RadioB') === '1' ||
    ctx.get('p16RadioC') === '1' ||
    ctx.get('p16RadioD') === '1' ||
    ctx.get('p16RadioE') === '1' ||
    ctx.get('p16RadioF') === '1' ||
    ctx.get('p16RadioG') === '1'
  );
}
