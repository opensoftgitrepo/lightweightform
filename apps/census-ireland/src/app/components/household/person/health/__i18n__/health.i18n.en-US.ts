import { I18nPaths } from '@lightweightform/core';

import { createOptions } from '../../../../../utils/options-creator';
import { YES_NO_OPTIONS } from '../../../../../__i18n__/app.i18n.en-US';

export const HEALTH_I18N_EN_US: I18nPaths = {
  '/household/?/health': {
    documentTitle: (ctx) =>
      `Health | ${
        ctx.get('../p1Text') || 'Person'
      } | Census of Population of Ireland`,
    label: 'Health',
    P16_HEADER:
      'P16 - Does the person have any of the following long-lasting ' +
      'conditions or difficulties?',
    P17_HEADER:
      'P17 - If "Yes" to any of the categories specified in P16, does the ' +
      'person have any difficulty in doing any of the following?',
    P18_HEADER: 'P18 - How is your health in general?',
  },
  '/household/?/health/p16RadioA': {
    label: '(a) Blindness or a serious vision impairment',
    options: YES_NO_OPTIONS,
  },
  '/household/?/health/p16RadioB': {
    label: '(b) Deafness or a serious hearing impairment',
    options: YES_NO_OPTIONS,
  },
  '/household/?/health/p16RadioC': {
    label:
      '(c) A difficulty with basic physical activities such as walking, ' +
      'climbing stairs, reaching, lifting, or carrying',
    options: YES_NO_OPTIONS,
  },
  '/household/?/health/p16RadioD': {
    label: '(d) An intellectual disability',
    options: YES_NO_OPTIONS,
  },
  '/household/?/health/p16RadioE': {
    label: '(e) A difficulty with learning, remembering, or concentrating',
    options: YES_NO_OPTIONS,
  },
  '/household/?/health/p16RadioF': {
    label: '(f) A psychological or emotional condition',
    options: YES_NO_OPTIONS,
  },
  '/household/?/health/p16RadioG': {
    label:
      '(g) A difficulty with pain, breathing, or any other chronic illness ' +
      'or condition',
    options: YES_NO_OPTIONS,
  },
  '/household/?/health/p17RadioA': {
    label: '(a) Dressing, bathing, or getting around inside the home',
    options: YES_NO_OPTIONS,
  },
  '/household/?/health/p17RadioB': {
    label: '(b) Going outside the home alone to shop or visit a doctor',
    options: YES_NO_OPTIONS,
  },
  '/household/?/health/p17RadioC': {
    label: '(c) Working at a job or business or attending school or college',
    options: YES_NO_OPTIONS,
  },
  '/household/?/health/p17RadioD': {
    label:
      '(d) Participating in other activities, for example leisure or using ' +
      'transport',
    options: YES_NO_OPTIONS,
  },
  '/household/?/health/p18Radio': {
    label: "How is the person's health in general?",
    options: createOptions(['Very Good', 'Good', 'Fair', 'Bad', 'Very bad']),
  },
};
