import { Component } from '@angular/core';
import { LfRouter } from '@lightweightform/core';
import { makeObservable } from 'mobx';
import { computed } from 'mobx-angular';

@Component({
  selector: 'census-person-health',
  templateUrl: './health.component.html',
})
export class HealthComponent {
  constructor(private lfRouter: LfRouter) {
    makeObservable(this);
  }

  @computed
  public get path() {
    const { personId } = this.lfRouter.params;
    return `/household/${personId}/health`;
  }
}
