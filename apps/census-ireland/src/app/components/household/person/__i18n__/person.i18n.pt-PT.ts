import { I18nPaths } from '@lightweightform/core';

import { createOptions } from '../../../../utils/options-creator';
import {
  UNNAMED_PERSON,
  YES_NO_OPTIONS,
} from '../../../../__i18n__/app.i18n.pt-PT';

export const PERSON_I18N_PT_PT: I18nPaths = {
  '/household/?': {
    documentTitle: (ctx) =>
      `${ctx.get('p1Text') || UNNAMED_PERSON} | Censo Populacional da Irlanda`,
    label: (ctx) => ctx.get('p1Text') || UNNAMED_PERSON,
  },
  '/household/?/p1Text': {
    label: 'Nome completo',
    helpMessage: 'Escreva o seu nome completo',
  },
  '/household/?/p2Radio': {
    label: 'Sexo',
    options: createOptions(['Masculino', 'Feminino']),
  },
  '/household/?/p3Date': {
    label: 'Data de nascimento',
  },
  '/household/?/p5Select': {
    label: 'Estado civil',
    legend: 'Responda se o indivíduo tem 15 ou mais anos',
    options: createOptions([
      'Solteiro (nunca casado e nunca numa parceria civil entre pessoas do ' +
        'mesmo sexo)',
      'Casado (primeiro casamento)',
      'Casado (sem ser o primeiro casamento)',
      'Numa parceria civil entre pessoas do mesmo sexo',
      'Separado',
      'Divorciado',
      'Viúvo',
    ]),
  },
  '/household/?/p6Text': {
    label: 'Local de nascimento',
    helpMessage:
      'Se na Irlanda (incluindo Irlanda do Norte), escreva o município. Se ' +
      'no estrangeiro, escreva o país.',
    legend:
      'Escreva o local em que a mãe do indivíduo vivia no momento do nascimento',
  },
  '/household/?/p7Select': {
    label: 'Onde é que o indivíduo normalmente vive?',
    legend: 'Responda se tem 15 ou mais anos',
    options: createOptions([
      'AQUI nesta morada',
      'Em outro local na IRLANDA (incluindo Irlanda do Norte)',
      'Noutro local no ESTRANGEIRO',
    ]),
  },
  '/household/?/p7Text': {
    label: 'Morada completa corrente',
  },
  '/household/?/p8Select': {
    label: 'Onde é que o indivíduo normalmente vivia há um ano atrás?',
    legend: 'Responda se tem 1 ou mais anos',
    options: createOptions([
      'AQUI nesta morada',
      'Em outro local na IRLANDA (incluindo Irlanda do Norte)',
      'Noutro local no ESTRANGEIRO',
    ]),
  },
  '/household/?/p8Text': {
    label: 'Morada completa corrente',
  },
  '/household/?/p9Radio': {
    label:
      'O indivíduo viveu fora da República da Irlanda por um período ' +
      'contínuo de um ou mais anos?',
    legend: 'Responda se tem 1 ou mais anos e vive na Irlanda',
    options: YES_NO_OPTIONS,
  },
  '/household/?/p9OptionalNumber': {
    label:
      'Ano em que pela última vez retomou residência na República da Irlanda',
  },
  '/household/?/p9OptionalText': {
    label: 'País da última residência prévia',
  },
};
