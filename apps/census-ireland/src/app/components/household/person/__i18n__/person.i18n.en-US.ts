import { I18nPaths } from '@lightweightform/core';

import { createOptions } from '../../../../utils/options-creator';
import {
  UNNAMED_PERSON,
  YES_NO_OPTIONS,
} from '../../../../__i18n__/app.i18n.en-US';

export const PERSON_I18N_EN_US: I18nPaths = {
  '/household/?': {
    documentTitle: (ctx) =>
      `${
        ctx.get('p1Text') || UNNAMED_PERSON
      } | Census of Population of Ireland`,
    label: (ctx) => ctx.get('p1Text') || UNNAMED_PERSON,
  },
  '/household/?/p1Text': {
    code: 'P1',
    label: 'Full name',
    helpMessage: 'Write your complete name',
  },
  '/household/?/p2Radio': {
    code: 'P2',
    label: 'Sex',
    options: createOptions(['Male', 'Female']),
  },
  '/household/?/p3Date': {
    code: 'P3',
    label: 'Date of birth',
  },
  '/household/?/p5Select': {
    code: 'P5',
    label: 'Marital status',
    legend: 'Answer if aged 15 or over',
    options: createOptions([
      'Single (never married and never in a same-sex civil partnership)',
      'Married (first marriage)',
      'Re-married',
      'In a registered same-sex civil partnership',
      'Separated',
      'Divorced',
      'Widowed',
    ]),
  },
  '/household/?/p6Text': {
    code: 'P6',
    label: 'Place of birth',
    helpMessage:
      'If Ireland (including Northern Ireland), write in the county. If ' +
      'elsewhere abroad, write in the country.',
    legend:
      "Give the place where the person's mother lived at the time of the " +
      'birth',
  },
  '/household/?/p7Select': {
    code: 'P7',
    label: 'Where does the person usually live?',
    legend: 'Answer if aged 15 or over',
    options: createOptions([
      'HERE at this address',
      'Elsewhere in IRELAND (including Northern Ireland)',
      'Elsewhere ABROAD',
    ]),
  },
  '/household/?/p7Text': {
    label: 'Current full address',
  },
  '/household/?/p8Select': {
    code: 'P8',
    label: 'Where did the person usually live one year ago?',
    legend: 'Answer if aged 1 year or over',
    options: createOptions([
      'HERE at this address',
      'Elsewhere in IRELAND (including Northern Ireland)',
      'Elsewhere ABROAD',
    ]),
  },
  '/household/?/p8Text': {
    label: 'Current full address',
  },
  '/household/?/p9Radio': {
    code: 'P9',
    label:
      'Has the person lived outside of the Republic of Ireland for a ' +
      'continuous period of one year or more?',
    legend: 'Answer if aged 1 year or over and living in Ireland',
    options: YES_NO_OPTIONS,
  },
  '/household/?/p9OptionalNumber': {
    label: 'Year of last taking up residence in the Republic of Ireland',
  },
  '/household/?/p9OptionalText': {
    label: 'Country of last previous residence',
  },
};
