import { Component } from '@angular/core';
import { LfRouter } from '@lightweightform/core';
import { makeObservable } from 'mobx';
import { computed } from 'mobx-angular';

@Component({
  selector: 'census-person',
  templateUrl: './person.component.html',
})
export class PersonComponent {
  constructor(private lfRouter: LfRouter) {
    makeObservable(this);
  }

  @computed
  public get path(): string {
    const { personId } = this.lfRouter.params;
    return `/household/${personId}`;
  }
}
