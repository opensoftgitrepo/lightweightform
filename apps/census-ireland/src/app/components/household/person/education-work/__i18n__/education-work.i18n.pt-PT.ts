import { I18nPaths } from '@lightweightform/core';

import { createOptions } from '../../../../../utils/options-creator';
import { YES_NO_OPTIONS } from '../../../../../__i18n__/app.i18n.pt-PT';

export const EDUCATION_WORK_I18N_PT_PT: I18nPaths = {
  '/household/?/educationWork': {
    documentTitle: (ctx) =>
      `Educação e Trabalho | ${
        ctx.get('../p1Text') || 'Person'
      } | Censo Populacional da Irlanda`,
    label: 'Educação e Trabalho',
    P34_HEADER:
      'P34 - Qual é o nome e morada completos do local de trabalho, escola ' +
      'ou faculdade do indivíduo?',
  },
  '/household/?/educationWork/p19Select': {
    label:
      'Como é que o indivíduo costuma viajar para o trabalho, escola ou ' +
      'faculdade?',
    helpMessage:
      'Escolha uma opção para a parte mais longa, em distância, do trajeto ' +
      'habitual do indivíduo para o trabalho, escola ou faculdade',
    options: createOptions([
      'Não trabalha e não frequenta uma escola ou faculdade',
      'A pé',
      'Bicicleta',
      'Autocarro',
      'Comboio, DART ou LUAS',
      'Motocicleta ou scooter',
      'Conduzindo um carro',
      'Como passageiro de um carro',
      'Carrinha',
      'Outro, incluíndo camião',
      'Trabalha principalmente a partir de casa',
    ]),
  },
  '/household/?/educationWork/p20Select': {
    label:
      'A que horas é que o indivíduo geralmente sai de casa com destino ao ' +
      'trabalho, escola ou faculdade?',
    options: createOptions([
      'Não trabalha e não frequenta uma escola ou faculdade',
      'Antes das 06:30',
      '06:30–07:00',
      '07:01–07:30',
      '07:31–08:00',
      '08:01–08:30',
      '08:31–09:00',
      '09:01–09:30',
      'Depois das 09:30',
    ]),
  },
  '/household/?/educationWork/p21Number': {
    label:
      'Qual é geralmente a duração da viagem do indivíduo para o trabalho, ' +
      'escola ou faculdade?',
    legend: 'Escreva em minutos',
  },
  '/household/?/educationWork/p22Radio': {
    label:
      'O indivíduo fornece ajuda pessoal regular e não remunerada a um amigo ' +
      'ou familiar com uma doença prolongada, problema de saúde ou ' +
      'incapacidade?',
    helpMessage:
      'Ajuda pessoal inclui ajuda em tarefas básicas como alimentar-se ou ' +
      'vestir-se',
    legend: 'Inclua problemas causados pela velhice',
    options: YES_NO_OPTIONS,
  },
  '/household/?/educationWork/p22Number': {
    label: 'Durante quantas horas por semana?',
    legend: 'Escreva em horas',
  },
  '/household/?/educationWork/p24Radio': {
    label: 'O indivíduo deixou a educação em tempo integral?',
    options: YES_NO_OPTIONS,
  },
  '/household/?/educationWork/p24Number': {
    label: 'Escreva a idade em que deixou',
  },
  '/household/?/educationWork/p25Select': {
    label:
      'Qual é o nível mais elevado de educação/formação (a tempo inteiro ou ' +
      'a tempo parcial) que a pessoa concluiu até à data?',
    options: createOptions([
      'Nenhuma educação/formação formal',
      'Educação primária. NFQ Níveis 1 ou 2. Cert. FETAC Nível 1 ou 2 ou ' +
        'equivalente',
      'Secundário inferior. NFQ Nível 3. Certificado Junior/Inter/Grupo, ' +
        'Cert. FETAC Nível 3, Habilidades Introdutórias FÁS, NCVA Foundation ' +
        'Cert. ou equivalente',
      'Secondário superior. NFQ Níveis 4 ou 5. Cert. de saída (incluindo ' +
        'programas Aplicados e Vocacionais) ou equivalente',
      'Técnico ou Vocacional. NFQ Níveis 4 ou 5. Cert. FETAC Nível 4/5, ' +
        'NCVA Nível 1/2, FÁS Competências Específicas, Cert. Teagasc em ' +
        'Agricultura, Cert. CERT Craft ou equivalente',
      'Certificado Avançado/Aprendizagem Completa. NFQ Nível 6. ' +
        'Cert. FETAC Avançado, NCVA Nível 3, Cert. FÁS Nacional de ' +
        'Artesanato, Cert. Teagasc Agrícola, Cert. CERT Culinária ' +
        'Profissional ou equivalente',
      'Certificado Superior. NFQ Nível 6. Cert. NCEA/HETAC Nacional ou ' +
        'equivalente',
      'Bacharelado comum ou diploma nacional. NFQ Nível 7',
      'Bacharelado com Distinção/Qualificação Profissional ou ambos. NFQ ' +
        'Nível 8',
      'Diploma ou gráu de pós-graduado. NFQ Nível 9. Diploma de ' +
        'pós-graduação, mestrado ou equivalente',
      'Doutoramento (Ph.D) ou superior. NFQ Nível 10',
    ]),
  },
  '/household/?/educationWork/p26Text': {
    label:
      'Qual é o principal campo de estudo da mais alta qualificação que o ' +
      'indivíduo completou até há data?',
    helpMessage:
      'E.g. contabilidade, terapia de beleza, agricultura, canalização, etc.',
    legend: 'Exclua qualificações de escola Secundária',
  },
  '/household/?/educationWork/p27Select': {
    label: 'Como é que o indivíduo descreveria o seu status principal atual?',
    options: createOptions([
      'A trabalhar para vencimento ou lucro',
      'À procura do primeiro emprego regular',
      'Desempregado',
      'Estudante',
      'A cuidar da casa/família',
      'Reformado',
      'Incapaz de trabalhar devido a doença ou incapacidade permanente',
      'Outro',
    ]),
  },
  '/household/?/educationWork/p27Text': {
    label: 'Outro',
  },
  '/household/?/educationWork/p29Select': {
    label:
      'O indivíduo trabalha (trabalhou) como empregado ou é (foi) + ' +
      'trabalhador por conta própria no seu trabalho principal?',
    helpMessage:
      'O trabalho principal é aquele em que o indivíduo trabalha (trabalhou)' +
      'durante mais horas',
    options: createOptions([
      'Empregado',
      'Trabalhador por conta própria, com empregados pagos',
      'Trabalhador por conta própria, sem empregados pagos',
      'A ajudar um relativo (sem receber salário fixo)',
    ]),
  },
  '/household/?/educationWork/p30Textarea': {
    label: 'Qual é (era) a ocupação do indivíduo no seu trabalho principal?',
    helpMessage:
      'Funcionários públicos e funcionários do governo local devem declarar ' +
      'seu grau, e.g. Diretor Administrativo Sênior. Membros do Gardaí ou ' +
      'Exército devem declarar a sua posição. Professores devem indicar o ' +
      'ramo de ensino, e.g. professor Primário. Clero e ordens religiosas ' +
      'devem dar descrição completa, e.g. Freira, Enfermeira Geral Registrada.',
    legend:
      'Em todos os casos descreva a ocupação completa e precisamente, dando ' +
      'o cargo completo',
  },
  '/household/?/educationWork/p30Text': {
    label:
      'Se agricultor, escreva o tamanho da área cultivada apróximada ao ' +
      'hectare',
    suffix: ' Hectares',
  },
  '/household/?/educationWork/p32Textarea': {
    label:
      'Qual é (era) o negócio do empregador do indivíduo no local onde o ' +
      'indivíduo trabalha (trabalhou) no seu trabalho principal?',
    helpMessage:
      'E.g. construção de computadores, reparamento de carros, educação ' +
      'Secundária, alimentos por atacado, fabricação de produtos ' +
      'farmacêuticos, limpeza por contrato, desenvolvimento de software, ' +
      'suporte',
    legend:
      'Se o indivíduo é (era) trabalhador por conta própria, resposda em ' +
      'relação ao seu próprio negócio. Descreva o principal produto ou ' +
      'serviço fornecido pelo empregador do indivíduo',
  },
  '/household/?/educationWork/p34NameTextarea': {
    legend: 'Nome completo',
  },
  '/household/?/educationWork/p34AddressTextarea': {
    legend: 'Morada',
  },
  '/household/?/educationWork/p34Radio': {
    options: createOptions([
      'Trabalha principalmente a partir de casa',
      'Não possui local fixo de trabalho',
    ]),
  },
};
