import { I18nPaths } from '@lightweightform/core';

import { createOptions } from '../../../../../utils/options-creator';
import { YES_NO_OPTIONS } from '../../../../../__i18n__/app.i18n.en-US';

export const EDUCATION_WORK_I18N_EN_US: I18nPaths = {
  '/household/?/educationWork': {
    documentTitle: (ctx) =>
      `Education and Work | ${
        ctx.get('../p1Text') || 'Person'
      } | Census of Population of Ireland`,
    label: 'Education and Work',
    P34_HEADER:
      "P34 - What is the full name and address of the person's place of " +
      'work, school, or college?',
  },
  '/household/?/educationWork/p19Select': {
    code: 'P19',
    label: 'How does the person usually travel to work, school, or college?',
    helpMessage:
      "Choose one option for the longest part, by distance, of the person's " +
      'usual journey to work, school, or college',
    options: createOptions([
      'Not at work, school or college',
      'On foot',
      'Bicycle',
      'Bus, minibus, or coach',
      'Train, DART or LUAS',
      'Motor cycle or scooter',
      'Driving a car',
      'Passenger in a car',
      'Van',
      'Other, including lorry',
      'Work mainly at or from home',
    ]),
  },
  '/household/?/educationWork/p20Select': {
    code: 'P20',
    label:
      'What time does the person usually leave home to go to work, school, ' +
      'or college?',
    options: createOptions([
      'Not at work, school, or college',
      'Before 06:30',
      '06:30–07:00',
      '07:01–07:30',
      '07:31–08:00',
      '08:01–08:30',
      '08:31–09:00',
      '09:01–09:30',
      'After 09:30',
    ]),
  },
  '/household/?/educationWork/p21Number': {
    code: 'P21',
    label:
      "How long does the person's journey to work, school, or college " +
      'usually take?',
    legend: 'Write in minutes',
  },
  '/household/?/educationWork/p22Radio': {
    code: 'P22',
    label:
      'Does the person provide regular unpaid personal help for a friend or ' +
      'family member with a long-term illness, health problem, or disability?',
    helpMessage:
      'Personal help includes help with basic tasks such as feeding or ' +
      'dressing',
    legend: 'Include problems which are due to old age',
    options: YES_NO_OPTIONS,
  },
  '/household/?/educationWork/p22Number': {
    label: 'For how many hours per week?',
    legend: 'Write in hours',
  },
  '/household/?/educationWork/p24Radio': {
    code: 'P24',
    label: 'Has the person ceased full-time education?',
    options: YES_NO_OPTIONS,
  },
  '/household/?/educationWork/p24Number': {
    label: 'Write in the age at which it ceased',
  },
  '/household/?/educationWork/p25Select': {
    code: 'P25',
    label:
      'What is the highest level of education/training (full-time or ' +
      'part-time) which the person has completed to date?',
    options: createOptions([
      'No formal education/training',
      'Primary education. NFQ Levels 1 or 2. FETAC Level 1 or 2 Cert. or ' +
        'equivalent',
      'Lower Secondary. NFQ Level 3. Junior/Inter/Group Cert., FETAC Level 3 ' +
        'Cert., FÁS Introductory Skills, NCVA Foundation Cert. or equivalent',
      'Upper Secondary. NFQ Levels 4 or 5. Leaving Cert. (including Applied ' +
        'and Vocational programmes) or equivalent',
      'Technical or Vocational. NFQ Levels 4 or 5. FETAC Level 4/5 Cert. ' +
        'NCVA Level 1/2, FÁS Specific Skills, Teagasc Cert. in  Agriculture, ' +
        'CERT Craft Cert. or equivalent',
      'Advanced Certificate/Completed Apprenticeship. NFQ Level 6. FETAC ' +
        'Advanced Cert., NCVA Level 3, FÁS National Craft Cert., Teagasc ' +
        'Farming Cert., CERT Professional Cookery Cert. or equivalent',
      'Higher Certificate. NFQ Level 6. NCEA/HETAC National Cert. or ' +
        'equivalent',
      'Ordinary Bachelor Degree or National Diploma. NFQ Level 7',
      'Honours Bachelor Degree/Professional qualification or both. NFQ Level 8',
      'Postgraduate Diploma or Degree. NFQ Level 9. Postgraduate Diploma, ' +
        'Masters Degree or equivalent',
      'Doctorate (Ph.D) or higher. NFQ Level 10',
    ]),
  },
  '/household/?/educationWork/p26Text': {
    code: 'P26',
    label:
      'What is the main field of study of the highest qualification the ' +
      'person has completed to date?',
    helpMessage: 'E.g. accountancy, beauty therapy, farming, plumbing, etc.',
    legend: 'Exclude Secondary school qualifications',
  },
  '/household/?/educationWork/p27Select': {
    code: 'P27',
    label: 'How would the person describe their present principal status?',
    options: createOptions([
      'Working for payment or profit',
      'Looking for first regular job',
      'Unemployed',
      'Student or pupil',
      'Looking after home/family',
      'Retired from employment',
      'Unable to work due to permanent sickness or disability',
      'Other',
    ]),
  },
  '/household/?/educationWork/p27Text': {
    label: 'Other',
  },
  '/household/?/educationWork/p29Select': {
    code: 'P29',
    label:
      'Does (did) the person work as an employee or is (was) the person ' +
      'self-employed in their main job?',
    helpMessage:
      'The main job is the job in which the person works (worked) the most ' +
      'hours',
    options: createOptions([
      'Employee',
      'Self-employed, with paid employees',
      'Self-employed, without paid employees',
      'Assisting relative (not receiving a fixed wage or salary)',
    ]),
  },
  '/household/?/educationWork/p30Textarea': {
    code: 'P30',
    label: "What is (was) the person's occupation in their main job?",
    helpMessage:
      'Civil servants and local government employees should state their ' +
      'grade e.g. Senior Administrative Officer. Members of the Gardaí or ' +
      'Army should state their rank. Teachers should state the branch of ' +
      'teaching e.g. Primary Teacher. Clergy and religious orders should ' +
      'give full description e.g. Nun, Registered General Nurse.',
    legend:
      'In all cases describe the occupation fully and precisely giving the ' +
      'full job title',
  },
  '/household/?/educationWork/p30Text': {
    label:
      'If a farmer, write in the size of the area farmed to the nearest ' +
      'hectare',
    thousandsSeparator: ',',
    suffix: ' Hectares',
  },
  '/household/?/educationWork/p32Textarea': {
    code: 'P32',
    label:
      "What is (was) the business of the person's employer at the place " +
      'where the person works (worked) in their main job?',
    helpMessage:
      'E.g. making computers, repairing cars, Secondary education, ' +
      'food wholesale, making pharmaceuticals, contract cleaning, software ' +
      'development, support',
    legend:
      'If the person is (was) self-employed, answer in respect of their own ' +
      'business. Describe the main product or service provided by the ' +
      "person's employer.",
  },
  '/household/?/educationWork/p34NameTextarea': {
    legend: 'Full name',
  },
  '/household/?/educationWork/p34AddressTextarea': {
    legend: 'Address',
  },
  '/household/?/educationWork/p34Radio': {
    options: createOptions([
      'Works mainly at or from home',
      'No fixed place of work',
    ]),
  },
};
