import {
  numberSchema,
  RecordSchema,
  recordSchema,
  Storage,
  stringSchema,
} from '@lightweightform/storage';
import { getYear, isAfter, subYears } from 'date-fns/esm';

import { createAllowedValues } from '../../../../utils/allowed-values-creator';

export const educationWorkSchema: RecordSchema = recordSchema({
  p19Select: stringSchema({
    isNullable: true,
    isRequired: true,
    allowedValues: createAllowedValues(11),
  }),
  p20Select: stringSchema({
    isNullable: true,
    isRequired: worksButNotFromHome,
    allowedValues: createAllowedValues(9),
  }),
  p21Number: numberSchema({
    isInteger: true,
    isNullable: true,
    isRequired: worksButNotFromHome,
    min: 0,
    max: 999,
  }),
  p22Radio: stringSchema({
    isNullable: true,
    isRequired: true,
    allowedValues: createAllowedValues(2),
  }),
  p22Number: numberSchema({
    isNullable: true,
    isRequired: providesUnpaidHelp,
    min: 0,
    max: 24 * 7,
  }),
  p24Radio: stringSchema({
    isNullable: true,
    isRequired: is15YearsOrOlder,
    allowedValues: createAllowedValues(2),
  }),
  p24Number: numberSchema({
    isInteger: true,
    isNullable: true,
    isRequired: hasCeasedEducation,
    min: 15,
    max: ageInYears,
  }),
  p25Select: stringSchema({
    isNullable: true,
    isRequired: is15YearsOrOlder,
    allowedValues: createAllowedValues(11),
  }),
  p26Text: stringSchema({ minLength: (ctx) => +is15YearsOrOlder(ctx) }),
  p27Select: stringSchema({
    isNullable: true,
    isRequired: is15YearsOrOlder,
    allowedValues: createAllowedValues(8),
  }),
  p27Text: stringSchema({ minLength: (ctx) => +hasOtherPrincipalStatus(ctx) }),
  p29Select: stringSchema({
    isNullable: true,
    isRequired: isOrWasEmployed,
    allowedValues: createAllowedValues(4),
  }),
  p30Textarea: stringSchema({ minLength: (ctx) => +isOrWasEmployed(ctx) }),
  p30Text: numberSchema({
    isInteger: true,
    isNullable: true,
    min: 0,
    max: 9999,
  }),
  p32Textarea: stringSchema({
    minLength: (ctx) => +isEmployedOrUnemployedAndNotRetired(ctx),
  }),
  p34NameTextarea: stringSchema({
    minLength: (ctx) => +hasFixedPlaceOfWork(ctx),
  }),
  p34AddressTextarea: stringSchema({
    minLength: (ctx) => +hasFixedPlaceOfWork(ctx),
  }),
  p34Radio: stringSchema({
    isNullable: true,
    isRequired: isEmployedOrStudent,
    allowedValues: createAllowedValues(2),
  }),
});

function worksButNotFromHome(ctx: Storage): boolean {
  return ['1', '11'].indexOf(ctx.get('p19Select')) === -1;
}

function providesUnpaidHelp(ctx: Storage): boolean {
  return ctx.get('p22Radio') === '1';
}

function is15YearsOrOlder(ctx: Storage): boolean {
  return !isAfter(ctx.get('../p3Date'), subYears(Date.now(), 15));
}

function hasCeasedEducation(ctx: Storage): boolean {
  return (
    !isAfter(ctx.get('../p3Date'), subYears(Date.now(), 15)) &&
    ctx.get('p24Radio') === '1'
  );
}

function ageInYears(ctx: Storage): number {
  return getYear(Date.now()) - getYear(ctx.get('../p3Date'));
}

function hasOtherPrincipalStatus(ctx: Storage): boolean {
  return (
    !isAfter(ctx.get('../p3Date'), subYears(Date.now(), 15)) &&
    ctx.get('p27Select') === '8'
  );
}

function isOrWasEmployed(ctx: Storage): boolean {
  return (
    !isAfter(ctx.get('../p3Date'), subYears(Date.now(), 15)) &&
    ['1', '3', '6'].indexOf(ctx.get('p27Select')) > -1
  );
}

function isEmployedOrUnemployedAndNotRetired(ctx: Storage): boolean {
  return (
    !isAfter(ctx.get('../p3Date'), subYears(Date.now(), 15)) &&
    ['1', '3'].indexOf(ctx.get('p27Select')) > -1
  );
}

function hasFixedPlaceOfWork(ctx: Storage): boolean {
  return (
    !isAfter(ctx.get('../p3Date'), subYears(Date.now(), 15)) &&
    ['1', '4'].indexOf(ctx.get('p27Select')) > -1 &&
    ctx.get('p34Radio') !== '2'
  );
}

function isEmployedOrStudent(ctx: Storage): boolean {
  return (
    !isAfter(ctx.get('../p3Date'), subYears(Date.now(), 15)) &&
    ['1', '4'].indexOf(ctx.get('p27Select')) > -1 &&
    ctx.get('p34NameTextarea') === null &&
    ctx.get('p34AddressTextarea') === null
  );
}
