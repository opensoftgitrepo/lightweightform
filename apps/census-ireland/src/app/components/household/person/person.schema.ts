import {
  dateSchema,
  numberSchema,
  RecordSchema,
  recordSchema,
  Storage,
  stringSchema,
} from '@lightweightform/storage';
import { getYear, isAfter, subYears } from 'date-fns/esm';

import { createAllowedValues } from '../../../utils/allowed-values-creator';

import { educationWorkSchema } from './education-work/education-work.schema';
import { healthSchema } from './health/health.schema';
import { languageCultureSchema } from './language-culture/language-culture.schema';

/**
 * Schema for a single person.
 */
export const personSchema: RecordSchema = recordSchema(
  {
    p1Text: stringSchema({ minLength: 1 }),
    p2Radio: stringSchema({
      isNullable: true,
      isRequired: true,
      allowedValues: createAllowedValues(2),
    }),
    p3Date: dateSchema({
      isNullable: true,
      isRequired: true,
      maxDate: new Date(),
    }),
    p5Select: stringSchema({
      isNullable: true,
      isRequired: xYearsOrOlder(15),
      allowedValues: createAllowedValues(7),
    }),
    p6Text: stringSchema({ minLength: 1 }),
    p7Select: stringSchema({
      isNullable: true,
      isRequired: true,
      allowedValues: createAllowedValues(3),
    }),
    p7Text: stringSchema({ minLength: 1 }),
    p8Select: stringSchema({
      isNullable: true,
      isRequired: xYearsOrOlder(1),
      allowedValues: createAllowedValues(3),
    }),
    p8Text: stringSchema({ minLength: (ctx) => +hasntLivedHereForAYear(ctx) }),
    p9Radio: stringSchema({
      isNullable: true,
      isRequired: livesInIrelandAndIsOneYearOrOlder,
      allowedValues: createAllowedValues(2),
    }),
    p9OptionalNumber: numberSchema({
      isInteger: true,
      isNullable: true,
      isRequired: livedOutsideIrelandForMoreThanOneYear,
      min: birthDateYear,
      max: getYear(Date.now()),
    }),
    p9OptionalText: stringSchema({
      minLength: (ctx) => +livedOutsideIrelandForMoreThanOneYear(ctx),
    }),
    languageCulture: languageCultureSchema,
    health: healthSchema,
    educationWork: educationWorkSchema,
  },
  { xmlElementName: 'person' }
);

function xYearsOrOlder(xYears: number): (ctx: Storage) => boolean {
  return (ctx: Storage) =>
    !isAfter(ctx.get('p3Date'), subYears(Date.now(), xYears));
}

function hasntLivedHereForAYear(ctx: Storage): boolean {
  return (
    !isAfter(ctx.get('p3Date'), subYears(Date.now(), 1)) &&
    !(ctx.get('p7Select') === '1' && ctx.get('p8Select') === '1')
  );
}

function livesInIrelandAndIsOneYearOrOlder(ctx: Storage): boolean {
  return (
    ['1', '2'].indexOf(ctx.get('p7Select')) !== -1 &&
    !isAfter(ctx.get('p3Date'), subYears(Date.now(), 1))
  );
}

function livedOutsideIrelandForMoreThanOneYear(ctx: Storage): boolean {
  return ctx.get('p9Radio') === '1';
}

function birthDateYear(ctx: Storage): number {
  return getYear(ctx.get('p3Date'));
}
