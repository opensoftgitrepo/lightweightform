import {
  Storage,
  TableSchema,
  tableSchema,
  ValidationIssue,
} from '@lightweightform/storage';

import { personSchema } from './person/person.schema';

/**
 * Schema of the household form.
 */
export const householdSchema: TableSchema = tableSchema(personSchema, {
  minSize: 1,
  validate: checkDuplicatedPersons,
});

/**
 * Emit a warning if there are two people with the same name.
 * @param ctx Storage with relative path set to the household list.
 * @returns A warning if there are duplicated persons.
 */
function checkDuplicatedPersons(ctx: Storage): ValidationIssue | undefined {
  // Ignore empty names (they're errors anyway)
  const household = ctx.get().filter(({ p1Text }) => p1Text !== '');
  const nDifferent = household.reduce((set: Set<string>, { p1Text }) => {
    set.add(p1Text);
    return set;
  }, new Set()).size;
  if (household.length !== nDifferent) {
    return {
      code: 'PERSONS_DUPLICATED',
      isWarning: true,
    };
  }
}
