import { I18nPaths } from '@lightweightform/core';

export const HOUSEHOLD_I18N_EN_US: I18nPaths = {
  '/household': {
    documentTitle: 'Household | Census of Population of Ireland',
    label: 'Household',
    addValueToListButtonText: 'Add person',
    addRowActionText: 'Add person',
    removeRowsActionText: 'Remove persons',
    noRowsText: 'Empty household.',
    columnLabels: {
      p1Text: 'Full name',
    },
    goToFormSm: 'Go',
    goToFormLg: 'Go to form',
    validations: {
      PERSONS_DUPLICATED:
        'We detect that you have input two persons with the same name. ' +
        'Please ensure that there is no duplicated information in the form.',
    },
  },
};
