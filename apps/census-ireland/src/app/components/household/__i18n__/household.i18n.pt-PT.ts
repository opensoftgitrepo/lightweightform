import { I18nPaths } from '@lightweightform/core';

export const HOUSEHOLD_I18N_PT_PT: I18nPaths = {
  '/household': {
    documentTitle: 'Agregado | Censo Populacional da Irlanda',
    label: 'Agregado',
    addValueToListButtonText: 'Adicionar pessoa',
    addRowActionText: 'Adicionar pessoa',
    removeRowsActionText: 'Remover pessoas',
    noRowsText: 'Nenhuma pessoa no agregado.',
    columnLabels: {
      p1Text: 'Nome completo',
    },
    goToFormSm: 'Navegar',
    goToFormLg: 'Navegar para formulário',
    validations: {
      PERSONS_DUPLICATED:
        'Detectamos que existem duas pessoas com o mesmo nome. Por favor ' +
        'verifique que não existe informação duplicada no formulário.',
    },
  },
};
