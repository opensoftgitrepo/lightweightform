import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { LfBootstrapThemeModule } from '@lightweightform/bootstrap-theme';
import {
  LfFileStorage,
  LfI18n,
  LfSerializer,
  LfStorage,
  LfUnloadAlert,
  LF_APP_I18N,
  LF_APP_SCHEMA,
} from '@lightweightform/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { appI18n } from './app.i18n';
import { appSchema } from './app.schema';
import { AccommodationComponent } from './components/accommodation/accommodation.component';
import { HouseholdComponent } from './components/household/household.component';
import { EducationWorkComponent } from './components/household/person/education-work/education-work.component';
import { HealthComponent } from './components/household/person/health/health.component';
import { LanguageCultureComponent } from './components/household/person/language-culture/language-culture.component';
import { PersonComponent } from './components/household/person/person.component';
import { RelationshipsComponent } from './components/relationships/relationships.component';
import { CensusSerializer } from './services/serializer.service';

@NgModule({
  imports: [BrowserModule, LfBootstrapThemeModule, AppRoutingModule],
  declarations: [
    AppComponent,
    AccommodationComponent,
    HouseholdComponent,
    PersonComponent,
    EducationWorkComponent,
    LanguageCultureComponent,
    HealthComponent,
    RelationshipsComponent,
  ],
  providers: [
    LfStorage,
    LfI18n,
    LfFileStorage,
    LfUnloadAlert,
    { provide: LF_APP_SCHEMA, useValue: appSchema },
    { provide: LF_APP_I18N, useValue: appI18n },
    { provide: LfSerializer, useClass: CensusSerializer },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
