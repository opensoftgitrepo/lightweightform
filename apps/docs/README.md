# Lightweightform/docs

This package contains the documentation for using Lightweightform, itself built
as a Lightweightform application.

## Launch

Running `npm run dev` at the top-level will start a local server accessible at:
https://localhost:3000.

To only launch the `docs` application run
`npm run dev -- --scope @lightweightform-app/docs` instead.
