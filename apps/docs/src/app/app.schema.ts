import { RecordSchema, recordSchema } from '@lightweightform/storage';

import { checkboxGroupSchema } from './components/form-elements/checkbox-group/checkbox-group.schema';
import { checkboxSchema } from './components/form-elements/checkbox/checkbox.schema';
import { dateRangeSchema } from './components/form-elements/date-range/date-range.schema';
import { dateSchema } from './components/form-elements/date/date.schema';
import { fileSchema } from './components/form-elements/file/file.schema';
import { numberSchema } from './components/form-elements/number/number.schema';
import { radioSchema } from './components/form-elements/radio/radio.schema';
import { selectSchema } from './components/form-elements/select/select.schema';
import { tableSchema } from './components/form-elements/table/table.schema';
import { textSchema } from './components/form-elements/text/text.schema';
import { textareaSchema } from './components/form-elements/textarea/textarea.schema';
import { introductionSchema } from './components/introduction/introduction.schema';
import { tutorialSchema } from './components/tutorial/tutorial.schema';
import { buttonSchema } from './components/ui/button/button.schema';
import { cardSchema } from './components/ui/card/card.schema';
import { modalSchema } from './components/ui/modal/modal.schema';

/**
 * Schema of the "docs" LF application.
 */
export const appSchema: RecordSchema = recordSchema({
  introduction: introductionSchema,
  tutorial: tutorialSchema,
  coreConcepts: recordSchema({
    storageSchema: recordSchema({}),
    dataValidation: recordSchema({}),
    themeUsage: recordSchema({}),
    i18n: recordSchema({}),
    routing: recordSchema({}),
  }),
  schematics: recordSchema({}),
  components: recordSchema({
    formElements: recordSchema({
      checkboxGroup: checkboxGroupSchema,
      checkbox: checkboxSchema,
      dateRange: dateRangeSchema,
      date: dateSchema,
      file: fileSchema,
      number: numberSchema,
      radio: radioSchema,
      select: selectSchema,
      table: tableSchema,
      text: textSchema,
      textarea: textareaSchema,
    }),
    ui: recordSchema({
      button: buttonSchema,
      card: cardSchema,
      modal: modalSchema,
    }),
  }),
  api: recordSchema({}),
});
