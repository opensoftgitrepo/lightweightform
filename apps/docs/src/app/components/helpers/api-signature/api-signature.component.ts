import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { makeObservable } from 'mobx';
import { computed, observable } from 'mobx-angular';

import { Entry } from '../../../services/api.service';
import { HighlighterService } from '../../../services/highlighter.service';
import { PrettierService } from '../../../services/prettier.service';

@Component({
  selector: 'docs-api-signature',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './api-signature.component.html',
})
export class ApiSignatureComponent {
  @observable.ref
  @Input()
  public signature: Entry;

  @observable
  @Input()
  public displayDefault = false;

  @observable
  @Input()
  public addLinks = true;

  constructor(
    private highlighter: HighlighterService,
    private prettier: PrettierService
  ) {
    makeObservable(this);
  }

  @computed
  public get isTopLevelExport(): boolean {
    return !this.signature.parentEntry;
  }

  public uniqueImplemented(implemented) {
    const set = new Set();
    return implemented
      .reduce((uniq, impl) => {
        if (!set.has(impl.name)) {
          set.add(impl.name);
          uniq.push(impl);
        }
        return uniq;
      }, [])
      .sort((imp1, imp2) => imp1.name.localeCompare(imp2.name));
  }

  public formatValue(value): string {
    if (value === '...') {
      return '<span class="token undefined">…</span>';
    }

    let prettyValue = this.prettier.prettify(`(${value})`, 'typescript');
    if (prettyValue.endsWith(';\n')) {
      prettyValue = prettyValue.slice(0, -2);
    }
    if (prettyValue.startsWith('(') && prettyValue.endsWith(')')) {
      prettyValue = prettyValue.slice(1, -1);
    }
    return this.highlighter.highlight(prettyValue, 'typescript');
  }
}
