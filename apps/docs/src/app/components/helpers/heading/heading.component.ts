import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  HostBinding,
  Input,
  OnDestroy,
} from '@angular/core';
import { LfRouter } from '@lightweightform/core';
import { asap } from '@lightweightform/theme-common';
import { makeObservable, observe } from 'mobx';
import { computed, observable } from 'mobx-angular';

@Component({
  selector:
    // tslint:disable-next-line: component-selector
    'h1[docs-heading], h2[docs-heading], h3[docs-heading], h4[docs-heading], h5[docs-heading], h6[docs-heading]',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './heading.component.html',
})
export class HeadingComponent implements AfterViewInit, OnDestroy {
  @observable
  @Input()
  public ref: string;

  private disposeObserveFragment: () => void;

  constructor(private elementRef: ElementRef, private lfRouter: LfRouter) {
    makeObservable(this);
  }

  @computed
  @HostBinding('attr.id')
  public get _attrId(): string {
    return this.ref || '';
  }

  public ngAfterViewInit() {
    // Scroll to the element when the URL fragment matches the `ref`
    this.disposeObserveFragment = observe(
      this.lfRouter,
      'fragment',
      ({ newValue }) => {
        if (newValue === this.ref) {
          asap(() => this.elementRef.nativeElement.scrollIntoView());
        }
      },
      true
    );
  }

  public ngOnDestroy() {
    this.disposeObserveFragment && this.disposeObserveFragment();
  }
}
