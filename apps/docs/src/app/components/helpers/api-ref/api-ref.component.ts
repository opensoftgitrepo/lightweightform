import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { makeObservable } from 'mobx';
import { computed, observable } from 'mobx-angular';

import { ApiService, Entry } from '../../../services/api.service';

@Component({
  selector: 'docs-api-ref',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './api-ref.component.html',
})
export class ApiRefComponent {
  @observable @Input() public pkg: string;
  @observable @Input() public name?: string;

  constructor(private apiService: ApiService) {
    makeObservable(this);
  }

  @computed
  public get entry(): Entry {
    return this.apiService.getEntryByPackagePlusName(
      `${this.pkg}/${this.name}`
    );
  }

  @computed
  public get refName(): string {
    return this.name
      ? this.entry.name
      : `${this.apiService.packagesGroupName}/${this.pkg}`;
  }

  @computed
  public get entryUrl(): string {
    return this.apiService.entryUrl(this.entry);
  }

  @computed
  public get url(): string {
    return this.name ? this.entryUrl.split('#')[0] : `/api/${this.pkg}`;
  }

  @computed
  public get fragment(): string {
    return this.name ? this.entryUrl.split('#')[1] : '';
  }
}
