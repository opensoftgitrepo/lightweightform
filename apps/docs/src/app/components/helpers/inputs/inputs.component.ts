import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Optional,
} from '@angular/core';
import { LfI18n, LfStorage, PathBased } from '@lightweightform/core';
import {
  appendToPath,
  isNullableSchema,
  isRecordSchema,
  pathError,
  Schema,
} from '@lightweightform/storage';

@Component({
  selector: 'docs-inputs',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './inputs.component.html',
})
export class InputsComponent extends PathBased {
  constructor(
    @Optional()
    @Inject(PathBased)
    parentPathBasedComponent: PathBased | null,
    lfStorage: LfStorage,
    lfI18n: LfI18n
  ) {
    super(parentPathBasedComponent, lfStorage, lfI18n);
  }

  public recordFieldsPaths(path: string): string[] {
    const schema = this.relativeStorage.schema(path);
    return isRecordSchema(schema)
      ? Object.keys(schema.fieldsSchemas).map((field) =>
          appendToPath(path, field)
        )
      : [];
  }

  public schemaOf(path: string): Schema {
    return this.relativeStorage.schema(path);
  }

  public canShow(path: string): boolean {
    const schema = this.schemaOf(path);
    const hideIf = (schema.docsOptions || {}).hideIf;
    return (
      hideIf === undefined ||
      !hideIf(this.relativeStorage.relativeStorage(path))
    );
  }

  protected validatePath(): void {
    const schema = this.relativeStorage.schema();
    if (!isRecordSchema(schema) || isNullableSchema(schema)) {
      throw pathError(
        this.path,
        'Schema must be of the "record" type and not "nullable"'
      );
    }
  }
}
