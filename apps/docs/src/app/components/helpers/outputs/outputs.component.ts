import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Optional,
} from '@angular/core';
import { LfI18n, LfStorage, PathBased } from '@lightweightform/core';
import {
  appendToPath,
  isBooleanSchema,
  isNullableSchema,
  isRecordSchema,
  pathError,
  RecordSchema,
} from '@lightweightform/storage';
import { makeObservable } from 'mobx';
import { action, computed, observable } from 'mobx-angular';

@Component({
  selector: 'docs-outputs',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './outputs.component.html',
})
export class OutputsComponent extends PathBased {
  @observable public events: Array<{ name: string; emitted: any }> = [];

  constructor(
    @Optional()
    @Inject(PathBased)
    parentPathBasedComponent: PathBased | null,
    lfStorage: LfStorage,
    lfI18n: LfI18n
  ) {
    super(parentPathBasedComponent, lfStorage, lfI18n);
    makeObservable(this);
  }

  @computed
  public get outputs(): string[] {
    return Object.keys(
      (this.relativeStorage.schema() as RecordSchema).fieldsSchemas
    );
  }

  @action
  public emitEvent(name: string, emitted: any): void {
    if (this.relativeStorage.get(name)) {
      emitted =
        emitted instanceof Event
          ? emitted.constructor.name
          : JSON.stringify(emitted);
      this.events.push({ name, emitted });
    }
  }

  @action
  public clearEvents(): void {
    this.events.length = 0;
  }

  protected validatePath(): void {
    const schema = this.relativeStorage.schema();
    if (!isRecordSchema(schema) || isNullableSchema(schema)) {
      throw pathError(
        this.path,
        'Schema must be of the "record" type and not "nullable"'
      );
    }
    Object.keys(schema.fieldsSchemas).forEach((field) => {
      const fieldSchema = schema.fieldsSchemas[field];
      if (!isBooleanSchema(fieldSchema) || isNullableSchema(fieldSchema)) {
        throw pathError(
          appendToPath(this.path, field),
          'Schema must be of the "boolean" type and not "nullable"'
        );
      }
    });
  }
}
