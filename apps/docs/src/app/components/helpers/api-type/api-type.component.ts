import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { makeObservable } from 'mobx';
import { computed, observable } from 'mobx-angular';

import { ApiService } from '../../../services/api.service';
import { HighlighterService } from '../../../services/highlighter.service';

@Component({
  selector: 'docs-api-type',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './api-type.component.html',
})
export class ApiTypeComponent {
  @observable.ref
  @Input()
  public type: any;

  @observable
  @Input()
  public addLinks = true;

  constructor(
    private apiService: ApiService,
    private highlighter: HighlighterService
  ) {
    makeObservable(this);
  }

  @computed
  public get typeUrl(): string {
    return this.apiService.entryUrl(this.apiService.getEntryById(this.type.id));
  }

  public highlight(type): string {
    return this.highlighter.highlight(type, 'typescript');
  }

  public typeOf(value) {
    return typeof value;
  }
}
