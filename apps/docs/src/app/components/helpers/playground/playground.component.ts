import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Input,
  Optional,
  ViewChild,
} from '@angular/core';
import { LfI18n, LfStorage, PathBased } from '@lightweightform/core';
import { makeObservable } from 'mobx';
import { observable } from 'mobx-angular';

import { OutputsComponent } from '../outputs/outputs.component';

@Component({
  selector: 'docs-playground',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './playground.component.html',
})
export class PlaygroundComponent extends PathBased {
  @ViewChild('outputs')
  public outputsComponent: OutputsComponent;

  @observable
  @Input()
  public schemaCode?: string;

  @Input()
  public schemaInputPaths: string[] = [];

  @observable
  @Input()
  public htmlCode?: string;

  @Input()
  public htmlInputPaths: string[] = [];

  @Input()
  public htmlOutputPaths: string[] = [];

  @observable
  @Input()
  public i18nCode?: string;

  @Input()
  public i18nInputPaths: string[] = [];

  @observable.struct
  @Input()
  public state?: Array<Record<string, any>>;

  constructor(
    @Optional()
    @Inject(PathBased)
    parentPathBasedComponent: PathBased | null,
    lfStorage: LfStorage,
    lfI18n: LfI18n
  ) {
    super(parentPathBasedComponent, lfStorage, lfI18n);
    makeObservable(this);
  }

  public emitEvent(type: string, evt: any) {
    this.outputsComponent.emitEvent(type, evt);
  }

  public pathLabel(path) {
    return this.lfI18n.getFromPath(
      this.relativeStorage.resolvePath(path),
      'label'
    );
  }
}
