import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { makeObservable } from 'mobx';
import { computed, observable } from 'mobx-angular';

import { ApiService, Entry } from '../../../services/api.service';
import { MarkdownService } from '../../../services/markdown.service';

@Component({
  selector: 'docs-api-entry',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './api-entry.component.html',
})
export class ApiEntryComponent {
  @observable.struct
  @Input()
  public entry: Entry;

  constructor(
    private apiService: ApiService,
    private markdownService: MarkdownService
  ) {
    makeObservable(this);
  }

  @computed
  public get isTopLevelExport() {
    return !this.entry.parentEntry;
  }

  @computed
  public get entryRemoteRepositoryUrl(): string {
    return this.apiService.entryRemoteRepositoryUrl(this.entry);
  }

  @computed
  public get entryPackage(): string {
    return this.apiService.packageOf(this.entry);
  }

  @computed
  public get entryPackageWithGroupName(): string {
    return `${this.apiService.packagesGroupName}/${this.entryPackage}`;
  }

  public getEntryById(id: number): any {
    return this.apiService.getEntryById(id);
  }

  public entryName(entry: Entry): string {
    return entry.kindString !== 'Index signature'
      ? entry.inputName || entry.outputName || entry.name
      : entry.parameters[0].name;
  }

  public entryFragment(entry: Entry): string {
    return entry.kindString !== 'Index signature'
      ? entry.inputName
        ? `>${entry.inputName}`
        : entry.outputName
        ? `<${entry.outputName}`
        : entry.name
      : `+${entry.parameters[0].name}`;
  }

  public groupFragment(group: any): string {
    return `@${group.title}`;
  }

  public md(text: string): string {
    return this.markdownService.render(text);
  }

  public isObjectAttribute(entry: Entry) {
    return (
      entry.parentEntry && entry.parentEntry.kindString === 'Object literal'
    );
  }
}
