import { Clipboard } from '@angular/cdk/clipboard';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { makeObservable } from 'mobx';
import { action, computed, observable } from 'mobx-angular';
// `json5` depends on `json`
import 'prismjs/components/prism-json';
import 'prismjs/components/prism-json5';
import 'prismjs/components/prism-scss';
import 'prismjs/components/prism-typescript';

import { HighlighterService } from '../../../services/highlighter.service';
import { PrettierService } from '../../../services/prettier.service';

export interface CodeOptions {
  /**
   * Whether a single value is to be shown. For TypeScript only.
   */
  isValue?: boolean;
}

@Component({
  selector: 'docs-code',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './code.component.html',
})
export class CodeComponent {
  @observable
  @Input()
  public code?: string;

  @observable
  @Input()
  public language?: 'json' | 'html' | 'scss' | 'typescript';

  @observable
  @Input()
  public printWidth = 80;

  @observable
  @Input()
  public prettify = true;

  @observable
  @Input()
  public highlight = true;

  @observable
  @Input()
  public options: CodeOptions = {};

  @observable private copied = false;

  constructor(
    private highlighter: HighlighterService,
    private prettier: PrettierService,
    private clipboard: Clipboard
  ) {
    makeObservable(this);
  }

  @computed
  public get prettyCode(): string | undefined {
    if (!this.code || !this.language || !this.prettify) {
      return this.code;
    }
    try {
      const isTsValue = this.language === 'typescript' && this.options.isValue;
      let code = this.code;
      if (isTsValue) {
        code = `_ = ${code}`;
      }
      code = this.prettier.prettify(code, this.language, {
        printWidth: this.printWidth,
      });
      if (isTsValue) {
        code = code.slice(4).replace(/;\n?$/, '');
      }
      return code;
    } catch (err) {}
  }

  @computed
  public get hasError(): boolean {
    return this.prettyCode === undefined;
  }

  @computed
  public get highlightedCode(): string {
    if (!this.hasError) {
      if (!this.language || !this.highlight) {
        return this.prettyCode!;
      }
      return this.highlighter.highlight(this.prettyCode!, this.language);
    } else {
      return 'Error rendering the code.';
    }
  }

  @computed
  public get buttonText(): string {
    return this.copied ? 'Copied!' : 'Copy';
  }

  @action
  public copyCode(): void {
    if (this.clipboard.copy(this.prettyCode!)) {
      this.copied = true;
    }
  }

  @action
  public clearCopied(): void {
    this.copied = false;
  }
}
