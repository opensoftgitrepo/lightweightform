import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { AppComponent } from '@lightweightform/bootstrap-theme';
import { LfRouter } from '@lightweightform/core';
import { makeObservable, reaction } from 'mobx';
import { computed } from 'mobx-angular';

import { ApiService, Entry } from '../../services/api.service';

@Component({
  selector: 'docs-api',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './api.component.html',
})
export class ApiComponent implements AfterViewInit, OnDestroy {
  @ViewChild('apiContent')
  public _apiContent: ElementRef;

  private disposeOnPageChanges: () => void;

  constructor(
    private lfRouter: LfRouter,
    private apiService: ApiService,
    private appComponent: AppComponent
  ) {
    makeObservable(this);
  }

  @computed
  public get package(): string | undefined {
    return this.lfRouter.params.package;
  }

  @computed
  public get packageWithGroup(): string | undefined {
    return (
      this.package && `${this.apiService.packagesGroupName}/${this.package}`
    );
  }

  @computed
  public get entryName(): string | undefined {
    return this.lfRouter.params.entryName;
  }

  @computed
  public get entry(): Entry | undefined {
    const entry =
      this.entryName &&
      this.apiService.getEntryByPackagePlusName(
        `${this.package}/${this.entryName}`
      );
    return entry;
  }

  public get packages() {
    return Object.keys(this.apiService.packagesGroups);
  }

  public ngAfterViewInit() {
    // Scroll to the top of the viewport at init or when the page changes
    this.disposeOnPageChanges = reaction(
      () => [this.package, this.entryName],
      () => this.scrollToTop()
    );
    this.scrollToTop();
  }

  public ngOnDestroy() {
    this.disposeOnPageChanges && this.disposeOnPageChanges();
  }

  public entryById(id: number): Entry {
    return this.apiService.getEntryById(id);
  }

  public entryUrl(id: number): string {
    return this.apiService.entryUrl(this.entryById(id));
  }

  public packageName(pkg: string): string {
    return `${this.apiService.packagesGroupName}/${pkg}`;
  }

  public groupsOfPackage(pkg: string): any[] {
    return this.apiService.packagesGroups[pkg];
  }

  private scrollToTop() {
    this.appComponent.scrollTo({ top: 0, left: 0 });
  }
}
