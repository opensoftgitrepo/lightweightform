import { Component } from '@angular/core';

@Component({
  selector: 'docs-part3',
  templateUrl: './part3.component.html',
})
export class Part3Component {
  public emailValidationFnCode = `
    function emailValidator(ctx: Storage): ValidationIssue | undefined {
      const email = ctx.get();
      if (!/^.+@.+$/.test(email)) {
        return {code: 'INVALID_EMAIL'};
      }
    }
  `;

  public emailValidationI18nCode = `
    {
      // ...
      validations: {
        INVALID_EMAIL: 'The e-mail is invalid.'
      }
    }
  `;

  public i18nGuestLabelCode = `
    {
      '/guests/?': {
        label: (ctx: Storage) => ctx.get('name') || '[No name]'
      },
      // ...
    }
  `;
}
