<lf-form *mobxAutorun path="tutorial/part3">
  <section>
    <p>
      In this part of the tutorial we will be adding validations to our hotel
      reservation form. LF allows validations to be specified directly within
      the schema of your form and will automatically validate your fields as
      needed whenever your form's data changes.
    </p>

    <p>
      If you are following the tutorial online and have lost your progress, the
      following link will provide you the base for this part of the tutorial:
      <a
        href="https://stackblitz.com/github/opensoft-sa/hotel-reservation/tree/fields"
        target="_blank"
        rel="noopener noreferrer"
        >StackBlitz [Fields]</a
      >.
    </p>
  </section>

  <section>
    <h2 docs-heading ref="addSimpleValidations">Add simple validations</h2>

    <p>
      We call "simple validations" those that you can add to a field without
      actually writing validation code. As an example, adding
      <code>isRequired: true</code> to the schema options of a "nullable" schema
      will make it invalid for the user to leave a value of that schema as
      <code>null</code>. You may go ahead and add this property to the
      <code>check-in-out</code> field of the reservation details.
    </p>

    <p>
      You may notice that the element in the screen will now have a "<lf-icon
        icon="asterisk"
        style="font-size: 0.65rem; vertical-align: middle; color: var(--orange)"
      ></lf-icon
      >" representing that the field is mandatory and that pressing "Validate"
      whilst the field is empty will prompt an error in the validation menu.
    </p>

    <p>
      For fields that are not nullable such as those used in
      <code>&lt;lf-text&gt;</code> elements, you can add a different property to
      their schemas to make them "required": for example, for non-nullable
      string schemas you can set <code>minLength</code> to <code>1</code> or
      more; for table schemas you can specify a <code>minSize</code> of
      <code>1</code> or more.
    </p>

    <p>
      Take a look at
      <a
        routerLink="/coreConcepts/dataValidation"
        fragment="validationProperties"
        ><lf-i18n path="/coreConcepts" key="label"></lf-i18n>&#10141;<lf-i18n
          path="/coreConcepts/dataValidation"
          key="label"
        ></lf-i18n
        >&#10141;Validation properties</a
      >
      for a list of all supported validation properties and add them to your
      application so that the following is verified:
    </p>

    <ul>
      <li>
        The name, e-mail, and check in/out dates in the reservation details must
        be filled in.
      </li>
      <li>The check-in date must not be before today.</li>
      <li>When provided, the check-in hour must be between 16h and 23h.</li>
      <li>
        A minimum of one room must be reserved and all added rooms must have
        their type set to either single, double, or twin.
      </li>
      <li>A minimum of one guest must be added.</li>
      <li>
        The name, document type, document number, and birth dates of added
        guests are all required information and the document type must be either
        a passport or an ID card.
      </li>
      <li>The birth date of a guest cannot be after the current date.</li>
    </ul>
  </section>

  <section>
    <h2 docs-heading ref="addCustomValidations">Add custom validations</h2>

    <p>
      LF allows users to specify their own validation functions in a way that
      makes it easy to access other parts of the form data when necessary. This
      is typically done via the <code>validate</code> field of a schema.
    </p>

    <p>
      As an example, the following is a validation function that verifies that
      an e-mail address follows a certain format (at least one characted;
      followed by "@"; followed by at least another character):
    </p>

    <docs-code language="typescript" [code]="emailValidationFnCode"></docs-code>

    <p>
      You can add that function to your application and use it in e-mail field
      schemas via <code>validate: emailValidator</code> (note that both
      <code>Storage</code> and <code>ValidationIssue</code> types should be
      imported from <code>@lightweightform/storage</code>).
    </p>

    <p>
      There are two main things to understand in the previous code snippet: what
      the parameter <code>ctx</code> of the function is; and the meaning of the
      object being returned.
    </p>

    <p>
      The argument received by the function is an instance of a
      <code>Storage</code>: this is where the data of your form is kept. For the
      purposes of this tutorial, you should only need to know that the
      <code>get</code> method of the storage allows you to query the value of
      any part of your form by providing it the path to said value. The above
      example is calling <code>ctx.get()</code> which is equivalent to
      <code>ctx.get('.')</code> (give me the value in the "current working path"
      of the storage instance; which, in the context of the validation function,
      is the path of the value being validated). As an example, we could call
      <code>ctx.get('/reservation-details/name')</code> in order to obtain the
      name of the person under which a reservation is being made.
    </p>

    <p>
      As for the return of the validation function: if nothing is returned, then
      the value being validated has no issues; otherwise, we should return a
      "validation issue" object (or a list of them). This object should have a
      <code>code</code> and may optionally have some <code>data</code>. You may
      also optionally set <code>isWarning: true</code> to specify that the issue
      in question is just a warning and not a blocking error (<em>i.e.</em> the
      form should still be submitted if only warnings were found).
    </p>

    <p>
      The code and data of the validation issue are used to specify a message to
      be shown to the user filling in the form. As per usual, this can be
      specified in the corresponding i18n file, in the path of the field being
      validated. To set the validation message of an e-mail field you should
      thus add the following to its corresponding i18n section:
    </p>

    <docs-code
      language="typescript"
      [code]="emailValidationI18nCode"
      [options]="{ isValue: true }"
    ></docs-code>

    <p>
      The value associated with the code (in this case the string
      <code>'The e-mail is invalid.'</code>) can also be a function which
      receives a storage instance and the validation issue's
      <code>data</code> as arguments.
    </p>

    <p>
      Similarly, and although unrelated to validations, all i18n values can also
      be functions which receive a storage instance as argument. As an example,
      we can use the following code in our application to set the
      <code>label</code> of a guest to be the guest's name in
      <code>src/app/components/guests/guest/__i18n__/guest.i18n.en-US.ts</code>:
    </p>

    <docs-code
      language="typescript"
      [code]="i18nGuestLabelCode"
      [options]="{ isValue: true }"
    ></docs-code>

    <p>
      One final note: all other validation properties such as
      <code>minLength</code>, <code>maxDate</code>, <em>etc.</em> also support
      receiving a function that expects a storage instance as argument and
      returns a value of the expected type (<code>e.g.</code> a number for
      <code>minLength</code>). For example, writing <code>minLength: 6</code> is
      equivalent to writing <code>minLength: (ctx: Storage) => 6</code>. The
      only caveat is that the storage instance that they receive has its
      "current working path" set to the parent of the value being validated (so
      <code>ctx.get()</code>> will return the value of the parent).
    </p>

    <p>
      With these explanations out of the way, you should now be able to
      implement the following validations (and others if you think of some):
    </p>

    <ul>
      <li>
        E-mails should follow an e-mail-like format such as the one shown above
        when provided, but guest e-mails are optional.
      </li>
      <li>
        The check-in and check-out cannot happen during the same day (at least
        one night must be spent at the hotel). For date related computations we
        advise you to add a dependency such as <code>date-fns</code> to make
        math simpler.
      </li>
      <li>
        The number of guests must not exceed the capacity of the requested
        rooms. As an example: if the reservation is for two rooms, a single and
        a double, then at most 3 guests are allowed.
      </li>
      <li>
        There must be at least one adult guest at the time of the check-in.
      </li>
      <li>
        A warning should be shown if a guest name has less than 3 characters as
        it is unlikely that the name is correct.
      </li>
    </ul>

    <p>
      After adding all validations, your application should be similar to the
      one found at:
      <a
        href="https://stackblitz.com/github/opensoft-sa/hotel-reservation/tree/validations"
        target="_blank"
        rel="noopener noreferrer"
        >StackBlitz [Validations]</a
      >.
    </p>

    <p>
      For more information on LF validations, feel free to read the whole of:
      <a routerLink="/coreConcepts/dataValidation"
        ><lf-i18n path="/coreConcepts" key="label"></lf-i18n>&#10141;<lf-i18n
          path="/coreConcepts/dataValidation"
          key="label"
        ></lf-i18n></a
      >.
    </p>
  </section>
</lf-form>
