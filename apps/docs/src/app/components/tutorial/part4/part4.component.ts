import { Component } from '@angular/core';

@Component({
  selector: 'docs-part4',
  templateUrl: './part4.component.html',
})
export class Part4Component {
  public roomsSchemaCode = `
    tableSchema(
      recordSchema({
        // ...
        price: numberSchema({isNullable: true, computedValue: roomPrice})
      }),
      // ...
    )
  `;

  public roomPriceFnCode = `
    function roomPrice(ctx: Storage): number | null {
      const roomType = ctx.get('type');
      return roomType === null ? null : roomType === 'single' ? 50 : 90;
    }
  `;

  public roomsHtmlCode = `
    <lf-table path="rooms">
      <lf-table-header>
        <!-- ... -->
        <lf-table-column id="price"></lf-table-column>
      </lf-table-header>

      <ng-template #lfTableRowTemplate let-id="id" let-i="index">
        <tr lf-table-row [path]="id">
          <!-- ... -->
          <td lf-table-cell><lf-number path="price" [scale]="2"></lf-number></td>
        </tr>
      </ng-template>
    </lf-table>
  `;

  public reservationDetailsI18nCode = `
    {
      // ...
      '/reservation-details/rooms': {
        // ...
        columnLabels: {
          // ...
          price: 'Price per night'
        },
        // ...
      },
      // ...
      '/reservation-details/rooms/?/price': {
        thousandsSeparator: ',',
        prefix: '€'
      }
    }
  `;
}
