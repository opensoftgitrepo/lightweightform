import { RecordSchema, recordSchema } from '@lightweightform/storage';

export const tutorialSchema: RecordSchema = recordSchema({
  part1: recordSchema({}),
  part2: recordSchema({}),
  part3: recordSchema({}),
  part4: recordSchema({}),
});
