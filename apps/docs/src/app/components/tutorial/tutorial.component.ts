import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'docs-tutorial',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './tutorial.component.html',
})
export class TutorialComponent {}
