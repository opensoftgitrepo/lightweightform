import { Component } from '@angular/core';

@Component({
  selector: 'docs-part1',
  templateUrl: './part1.component.html',
})
export class Part1Component {
  public newNgAppCommands = [
    'ng new hotel-reservation --routing --style scss',
    'cd hotel-reservation',
  ].join('\n');

  public addAndServeLfCommands = [
    'ng add @lightweightform/bootstrap-theme --locales en-US',
    'ng serve',
  ].join('\n');

  public generateLfFormsCommands = [
    'ng g @lightweightform/bootstrap-theme:form components/reservation-details',
    'ng g @lightweightform/bootstrap-theme:form --collection components/guests/guest',
  ].join('\n');

  public initialSchemaCode = `
    recordSchema({
      'reservation-details': recordSchema({}),
      guests: tableSchema(recordSchema({}))
    })
  `;

  public acceptedValueCode = `
    {
      'reservation-details': {},
      guests: [{}, {}]
    }
  `;

  public initialI18nCode = `
    {
      'en-US': {
        '*': {
          actionsMenu: {
            save: 'Save',
            load: 'Load',
            validate: 'Validate',
            submit: 'Submit'
          }
        },
        '/': {
          label: 'App'
        },
        '/reservation-details': {
          label: 'Reservation details'
        },
        '/guests': {
          label: 'Guests',
          columnLabels: {
            navigation: 'Navigation'
          },
          navigateButtonLabel: 'Go to form'
        },
        '/guests/?': {
          label: 'Guest'
        }
      }
    }
  `;
}
