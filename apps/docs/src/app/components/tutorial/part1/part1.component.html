<lf-form *mobxAutorun path="tutorial/part1">
  <section>
    <p>
      In this first part of the tutorial we will set up our "hotel reservation"
      application and go over the typical structure of Lightweightform
      applications.
    </p>

    <p>
      For those wanting to follow the tutorial online via StackBlitz, please
      continue reading until we present you with the link to launch it. We will
      provide you with a project initialised with the content that would be
      generated after running the commands that will be shortly presented.
    </p>
  </section>

  <section>
    <h2 docs-heading ref="createNgApp">Create a new Angular application</h2>

    <p>
      Start by creating a new Angular application via Angular CLI (with routing
      enabled and styles set to SCSS) and <code>cd</code> into it with:
    </p>

    <docs-code [code]="newNgAppCommands"></docs-code>
  </section>

  <section>
    <h2 docs-heading ref="addLf">Add Lightweightform</h2>

    <p>
      To use Lightweightform, you will first want to add it to your application.
      LF provides a custom schematic for Angular CLI to initialise an LF
      application. As such, to add LF, generate a minimal LF application, and
      serve it so it becomes locally accessible (at
      <a href="http://localhost:4200" target="_blank" rel="noopener noreferrer"
        >localhost:4200</a
      >), run the following commands:
    </p>

    <docs-code [code]="addAndServeLfCommands"></docs-code>

    <p>
      Lightweightform supports writing applications targeted to multiple
      locales. For the purpose of this tutorial we're specifying a single locale
      (<code>en-US</code>).
    </p>
  </section>

  <section>
    <h2 docs-heading ref="generatingForms">Generate the form pages</h2>

    <p>
      As previously mentioned, we are interested in having the following form
      pages in our application:
    </p>

    <ul>
      <li>
        <strong>Reservation details:</strong> a form page where customers will
        be able to fill in the details of their reservation.
      </li>
      <li>
        <strong>Guests:</strong> a collection of form pages, each to be filled
        with information about each guest.
      </li>
    </ul>

    <p>
      To simplify the creation of these pages, LF provides a custom schematic
      that generates a form page (or a collection of them): the
      <code>form</code> schematic. Running the following commands will thus give
      us the initial structure of our application:
    </p>

    <docs-code [code]="generateLfFormsCommands"></docs-code>

    <p>
      After this initial setup, your application should be similar to the one
      found here:
      <a
        href="https://stackblitz.com/github/opensoft-sa/hotel-reservation/tree/initial"
        target="_blank"
        rel="noopener noreferrer"
        >StackBlitz [Initial]</a
      >. For those wanting to follow this tutorial online, you may now proceed
      by making changes directly within the online IDE.
    </p>

    <p>
      Although unnecessary within the scope of this tutorial, for more
      information on the commands used to generate the initial LF application
      and form pages, please refer to:
      <a routerLink="/schematics"
        ><lf-i18n path="/schematics" key="label"></lf-i18n></a
      >.
    </p>
  </section>

  <section>
    <h2 docs-heading ref="understandingStructure">
      Understanding the structure of the application
    </h2>

    <p>
      Now that we have set up the structure of our application, let us go over
      the generated files to understand their purpose within an LF application.
    </p>

    <section>
      <h3 docs-heading ref="schemas">Schemas</h3>

      <p>
        LF operates with structured data: this means that the library needs to
        know the structure of your form's data. This structure is specified as a
        "schema".
      </p>

      <p>
        If you take a look at the project's files, you'll find
        <code>src/app/app.schema.ts</code>: this is where you can find the
        "root" schema of the whole application, which is imported by the root
        Angular module (<code>src/app/app.module.ts</code>).
      </p>

      <p>
        When using LF schematics to generate form pages (like we did), schema
        files are created within the folders that contain the Angular components
        representing each page. As such, you'll be able to find three other
        "schema files":
      </p>

      <ul>
        <li>
          <code
            >src/app/components/reservation-details/reservation-details.schema.ts</code
          >: the schema of the "reservation details" form page.
        </li>
        <li>
          <code>src/app/components/guests/guests.schema.ts</code>: the schema of
          the collection of guests itself (for which there is also a form page).
        </li>
        <li>
          <code>src/app/components/guests/guest/guest.schema.ts</code>: the
          schema of each guest form page.
        </li>
      </ul>

      <p>
        If we were to "flatten" the application's root schema by resolving all
        imports, the following definition would be the schema of the whole
        application:
      </p>

      <docs-code
        language="typescript"
        [code]="initialSchemaCode"
        [options]="{ isValue: true }"
      ></docs-code>

      <p>
        This means that our application expects the following as form data: a
        record (JavaScript object) with two fields,
        <code>reservation-details</code> and <code>guests</code>. The
        <code>reservation-details</code> field is a record, whilst
        <code>guests</code> is a collection (table in a database sense) of
        records.
      </p>

      <p>
        This entails that the following would be a value accepted by our
        application, representing a hotel reservation for two guests:
      </p>

      <docs-code
        language="typescript"
        [code]="acceptedValueCode"
        [options]="{ isValue: true }"
      ></docs-code>

      <p>
        To know the "current state" of the application, you may press "Save" in
        the top bar button of the served application. The saved document should
        contain a JSON resembling the above JavaScript value.
      </p>

      <p>
        In the next part of the tutorial we will be adding fields to each form
        page. This will involve editing these schemas to reflect the type of
        expected data. Furthermore, as we will see in yet another part of the
        tutorial, the schemas are where we will be defining how data should be
        validated. For more information on the inner workings of LF and how
        schemas are used, feel free to read:
        <a routerLink="/coreConcepts/storageSchema"
          ><lf-i18n path="/coreConcepts" key="label"></lf-i18n>&#10141;<lf-i18n
            path="/coreConcepts/storageSchema"
            key="label"
          ></lf-i18n></a
        >.
      </p>

      <p>
        You may have noticed the presence of some files ending in
        <code>.schema.spec.ts</code>. These, similarly to other
        <code>.spec.ts</code> files, are meant for unit testing: in this case
        for testing the schema. They can be safely ignored for the purposes of
        the tutorial.
      </p>
    </section>

    <section>
      <h3 docs-heading ref="components">Components</h3>

      <p>
        If you are currently serving the application, you most likely noticed
        that the LF schematics generated a working&mdash;although
        basic&mdash;application that, among other things, allows you to save and
        load the state of the application, validate it, navigate between form
        pages, and add/remove guests.
      </p>

      <p>
        The basic UI of the LF application is defined within
        <code>src/app/app.component.html</code>: this includes the main
        application frame, the top bar, side bar, and router outlet (where the
        content of routes is rendered). The code responsible for the actions in
        the top bar can be found at <code>src/app/app.component.ts</code>.
      </p>

      <p>
        The side bar's entries that allow navigation between form pages are
        automatically populated from reading the application's schema together
        with the available routes of the application (automatically added to
        <code>src/app/app-routing.module.ts</code> by the
        <code>form</code> schematic).
      </p>

      <p>
        If we browse the code used to render each form page (for example, the
        HTML in
        <code
          >src/app/components/reservation-details/reservation-details.component.html</code
        >), we can see that a <code>path</code> is being provided to an
        <code>&lt;lf-form&gt;</code> component.
        <strong>Paths are an important concept in LF:</strong> they are
        "pointers" to form data. As an example, the path
        <code>/</code> represents the "root value" of the form data, while
        <code>/guests/13</code> would represent the guest with id
        <code>13</code> within the collection of guests. As you'll most likely
        realise, LF uses paths extensively to associate LF components with form
        data or to be able to simply talk about data that is associated with a
        certain schema or value of a certain schema.
      </p>
    </section>

    <section>
      <h3 docs-heading ref="i18n">I18n</h3>

      <p>
        Lightweightform uses internationalisation (i18n) files to manage mostly
        everything that may change due to locales. In
        <code>src/app/app.i18n.ts</code> we define the application's "root" i18n
        object, imported by the root Angular module.
      </p>

      <p>
        For each component, you should find a <code>__i18n__</code> folder which
        should contain the i18n files that correspond to it. Because for the
        purposes of this tutorial we are only interested in the
        <code>en-US</code> locale, you will only find a single file in these
        folders.
      </p>

      <p>
        Similarly to what we did for the schemas, if we were to "flatten" the
        root i18n object by resolving all imports, we would obtain an object not
        unlike the following:
      </p>

      <docs-code
        language="typescript"
        [code]="initialI18nCode"
        [options]="{ isValue: true }"
      ></docs-code>

      <p>
        By looking at this object, we are able to understand how translations
        are organised in LF: simply put, they are a record where each key
        represents a locale, and for each locale we have a map from paths to
        key/value pairs of translations. When LF wants to know a certain
        translation, it will "ask" for the value associated with a certain key
        for a certain path within a certain locale. As an example, the
        <code>label</code> key allows us to provide a "label" to paths: used,
        for example, in the titles of form pages and in the application's side
        bar.
      </p>

      <p>
        The <code>*</code> "path" represents a fallback, meaning that if a
        certain key isn't provided for a certain path, then that same key will
        be looked up in the fallback path.
      </p>

      <p>
        The <code>?</code> within <code>/guests/?</code> represents "any
        identifier" of the collection (it will match against
        <code>/guests/4</code> or <code>/guests/200</code>, for example).
      </p>
    </section>

    <section>
      <h3 docs-heading ref="otherChanges">Other changes</h3>

      <p>
        A few other changes were made by the schematics: within
        <code>src/styles.scss</code> the LF styles are imported (which come with
        Bootstrap bundled together, so you can use Bootstrap classes as
        appropriate); we further generate a basic way of managing the collection
        of guests within
        <code>src/app/components/guests/guests.component.html</code>.
      </p>
    </section>
  </section>
</lf-form>
