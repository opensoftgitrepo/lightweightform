<lf-form *mobxAutorun path="tutorial/part2">
  <section>
    <p>
      Whilst in the first part of the tutorial we simply generated our "starter"
      application and understood the structure of the project, for this second
      part we will be making actual changes to our application by adding fields
      to each of the form pages.
    </p>

    <p>
      If you are following the tutorial online and have lost your progress, the
      following link will provide you the base for this part of the tutorial:
      <a
        href="https://stackblitz.com/github/opensoft-sa/hotel-reservation/tree/initial"
        target="_blank"
        rel="noopener noreferrer"
        >StackBlitz [Initial]</a
      >.
    </p>
  </section>

  <section>
    <h2 docs-heading ref="addFirstField">Add your first field</h2>

    <p>
      For the first field, we are interested in the name of the person under
      which the reservation should be made. Adding a field in LF generally
      entails following three steps:
    </p>

    <ol>
      <li>Adding the field of the correct type to the schema.</li>
      <li>
        Adding to the form page the component that will allow the field to be
        edited (linking it to the respective schema).
      </li>
      <li>
        Editing its "properties that can be translated" within the i18n file.
      </li>
    </ol>

    <p>
      LF's supported schema types are very similar to the types and objects
      supported by JavaScript. For a list of all supported schema types, take a
      quick look at:
      <a routerLink="/coreConcepts/storageSchema" fragment="schemaCreation"
        ><lf-i18n path="/coreConcepts" key="label"></lf-i18n>&#10141;<lf-i18n
          path="/coreConcepts/storageSchema"
          key="label"
        ></lf-i18n
        >&#10141;Schema creation</a
      >.
    </p>

    <p>
      For our case, because we are interested in a person's name, we will want
      to use a string schema. As such, let us edit
      <code
        >src/app/components/reservation-details/reservation-details.schema.ts</code
      >
      so that the reservation details form page has the following schema (note
      that the <code>stringSchema</code> function should be imported from the
      <code>@lightweightform/storage</code> package):
    </p>

    <docs-code
      language="typescript"
      [code]="reservationDetailsSchemaCode"
      [options]="{ isValue: true }"
    ></docs-code>

    <p>
      With the first step out of the way, we can now add a component that allows
      us to edit this string. In LF, components representing fields specify the
      type of data they can work with: as an example, the
      <code>&lt;lf-text&gt;</code> component accepts values that have a string
      schema, while the <code>&lt;lf-number&gt;</code> component expects values
      with nullable number schemas (<code>isNullable</code> property set to
      <code>true</code> in the schema options argument).
    </p>

    <p>
      Knowing this, we can now add the following element within the
      <code>lf-form</code> at
      <code
        >src/app/components/reservation-details/reservation-details.component.html</code
      >:
    </p>

    <docs-code language="html" [code]="lfStringNameCode"></docs-code>

    <p>
      Note that the added <code>path</code> is effectively pointing to
      <code>/reservation-details/name</code>, but because the
      <code>&lt;lf-text&gt;</code> is within the <code>&lt;lf-form&gt;</code>,
      we can write the path relatively to its parent. As such, the following
      versions of <code>path</code> would all be equivalent in this context:
      <code>name</code>, <code>./name</code>,
      <code>../reservation-details/name</code>.
    </p>

    <p>
      You should now have a working LF field in your application. If you now
      edit the value of the field and press "Save" in the application's top bar
      you should notice that the edited value is present in the data of the
      form.
    </p>

    <p>
      As an aside: LF allows multiple references to the same value. As an
      example, if you duplicate the above HTML element, you will notice that
      when the first field is edited, the second one will automatically update
      (because they both point to the same value).
    </p>

    <p>
      The third step involves editing the "translatable" parts of the created
      field. As such, we can edit
      <code
        >src/app/components/reservation-details/__i18n__/reservation-details.i18n.en-US.ts</code
      >
      and add the following property to the i18n object responsible for the
      reservation details page:
    </p>

    <docs-code language="typescript" [code]="nameI18nCode"></docs-code>

    <p>
      And that's it: our first LF field has been fully incorporated into our
      form.
    </p>
  </section>

  <section>
    <h2 docs-heading ref="addMoreFields">Add more fields</h2>

    <p>
      As you may guess, besides the <code>&lt;lf-text&gt;</code> element, LF
      supports a bunch of other elements: each meant for different types of
      schemas. For this section, we expect you to browse through LF's supported
      elements (<em>e.g.</em>
      <a routerLink="/components/formElements/number"
        ><lf-i18n path="/components" key="label"></lf-i18n>&#10141;<lf-i18n
          path="/components/formElements"
          key="label"
        ></lf-i18n
        >&#10141;<lf-i18n
          path="/components/formElements/number"
          key="label"
        ></lf-i18n></a
      >) and try and add the following fields (and any others if you feel like
      it) to the reservation details form page:
    </p>

    <ul>
      <li>
        <code>email</code>: E-mail of the person under which the reservation is
        being made.
      </li>
      <li><code>check-in-out</code>: Check-in and check-out dates.</li>
      <li>
        <code>check-in-hour</code>: Integer representing the hour at which the
        guests expect to check-in.
      </li>
      <li>
        <code>rooms</code>: A collection of rooms to be reserved, each room
        containing the following information:

        <ul>
          <li>
            <code>type</code>: Type of the room (single, double, or twin).
          </li>
          <li>
            <code>smoking-room</code>: Whether guests should be allowed to smoke
            in the room.
          </li>
        </ul>
      </li>
    </ul>

    <p>And the following fields in each guest form page:</p>

    <ul>
      <li><code>name</code>: Name of the guest.</li>
      <li>
        <code>document-type</code>: Type of the document that the guest will
        present at check-in (either a passport or a national ID card).
      </li>
      <li><code>document-number</code>: Number of the document.</li>
      <li><code>birth-date</code>: Birth date of the guest.</li>
      <li><code>email</code>: E-mail of the guest.</li>
    </ul>

    <p>
      Furthermore, you might want to change the title in the top bar of your
      application and add the name of the guest to each row of the table in the
      Guests page.
    </p>

    <p>
      After all the changes, your application should look similar to the one
      found at:
      <a
        href="https://stackblitz.com/github/opensoft-sa/hotel-reservation/tree/fields"
        target="_blank"
        rel="noopener noreferrer"
        >StackBlitz [Fields]</a
      >.
    </p>
  </section>
</lf-form>
