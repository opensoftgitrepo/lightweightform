import { Component } from '@angular/core';

@Component({
  selector: 'docs-part2',
  templateUrl: './part2.component.html',
})
export class Part2Component {
  public reservationDetailsSchemaCode = `
    recordSchema({
      name: stringSchema()
    })
`;

  public lfStringNameCode = '<lf-text path="name"></lf-text>';

  public nameI18nCode = `
    export const reservationDetailsI18nEnUS: Record<string, any> = {
      // ...
      '/reservation-details/name': {
        label: 'Name',
        helpMessage: 'The reservation will be made in this name'
      }
    }
  `;
}
