import {
  booleanSchema,
  numberSchema as numSchema,
  RecordSchema,
  recordSchema,
  stringSchema,
} from '@lightweightform/storage';

import { customValidate } from '../../../utils/schema-utils';

export const numberSchema: RecordSchema = recordSchema({
  number: numSchema({
    isNullable: true,
    isRequired: (ctx) => ctx.get('schemaOptions/isRequired'),
    min: (ctx) => {
      const min = ctx.get('schemaOptions/min');
      return min === null ? undefined : min;
    },
    max: (ctx) => {
      const max = ctx.get('schemaOptions/max');
      return max === null ? undefined : max;
    },
    validate: customValidate,
  }),
  schemaOptions: recordSchema(
    {
      isNullable: booleanSchema({ docsOptions: { isReadOnly: true } }),
      isRequired: booleanSchema(),
      min: numSchema({ isNullable: true }),
      max: numSchema({ isNullable: true }),
      customError: booleanSchema({ docsOptions: { hideOptionIf: () => true } }),
      customWarning: booleanSchema({
        docsOptions: { hideOptionIf: () => true },
      }),
    },
    { initialValue: { isNullable: true } }
  ),
  inputs: recordSchema(
    {
      path: stringSchema(),
      scale: numSchema({ isNullable: true, initialValue: 0, min: 0, max: 100 }),
      leadingZeroes: numSchema({ isNullable: true, initialValue: 1, min: 1 }),
      isReadOnly: booleanSchema(),
    },
    { initialValue: { path: '/number' } }
  ),
  outputs: recordSchema({
    change: booleanSchema(),
  }),
  i18n: recordSchema(
    {
      label: stringSchema(),
      code: stringSchema(),
      helpMessage: stringSchema(),
      legend: stringSchema(),
      prefix: stringSchema(),
      suffix: stringSchema(),
      decimalSeparator: stringSchema(),
      thousandsSeparator: stringSchema(),
    },
    { initialValue: { label: 'Number', code: 'N' } }
  ),
});
