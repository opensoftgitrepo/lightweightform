import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { FormComponent } from '@lightweightform/bootstrap-theme';
import { makeObservable } from 'mobx';
import { computed, observable } from 'mobx-angular';

import { formatI18nProps } from '../../../utils/format-i18n-props';
import {
  formatInputs,
  formatOutputs,
} from '../../../utils/format-inputs-outputs';
import { formatSchemaOptions } from '../../../utils/format-schema-options';

@Component({
  selector: 'docs-number',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './number.component.html',
})
export class NumberComponent {
  @observable
  @ViewChild(FormComponent)
  public form: FormComponent;

  constructor() {
    makeObservable(this);
  }

  @computed
  public get schemaCode(): string | undefined {
    if (this.form) {
      return `
        numberSchema(${formatSchemaOptions(
          this.form.relativeStorage,
          'schemaOptions'
        )})
      `;
    }
  }

  @computed
  public get htmlCode(): string | undefined {
    if (this.form) {
      return `
        <lf-number
          ${formatInputs(this.form.relativeStorage, 'inputs')}
          ${formatOutputs(this.form.relativeStorage, 'outputs')}
        ></lf-number>
      `;
    }
  }

  @computed
  public get i18nCode(): string | undefined {
    if (this.form) {
      return `{
        '${
          this.form.relativeStorage.get('inputs/path') || '*'
        }': {${formatI18nProps(this.form.relativeStorage, 'i18n')}}
      }`;
    }
  }

  @computed
  public get state(): Array<Record<string, any>> | undefined {
    if (this.form) {
      return [
        {
          name: 'value',
          state: this.form.relativeStorage.get('number'),
        },
        {
          name: 'isTouched',
          state:
            this.form.relativeStorage.getStateProperty('number', 'isTouched') ||
            false,
        },
        {
          name: 'isDirty',
          state:
            this.form.relativeStorage.getStateProperty('number', 'isDirty') ||
            false,
        },
      ];
    }
  }
}
