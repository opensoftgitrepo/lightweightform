import { dateRangeSchema as dateRSchema } from '@lightweightform/bootstrap-theme';
import {
  booleanSchema,
  dateSchema,
  RecordSchema,
  recordSchema,
  stringSchema,
} from '@lightweightform/storage';

import { customValidate } from '../../../utils/schema-utils';

export const dateRangeSchema: RecordSchema = recordSchema({
  dateRange: dateRSchema({
    isNullable: true,
    isRequired: (ctx) => ctx.get('schemaOptions/isRequired'),
    minDate: (ctx) => {
      const minDate = ctx.get('schemaOptions/minDate');
      return minDate === null ? undefined : minDate;
    },
    maxDate: (ctx) => {
      const maxDate = ctx.get('schemaOptions/maxDate');
      return maxDate === null ? undefined : maxDate;
    },
    validate: customValidate,
  }),
  schemaOptions: recordSchema(
    {
      isNullable: booleanSchema({ docsOptions: { isReadOnly: true } }),
      isRequired: booleanSchema(),
      minDate: dateSchema({ isNullable: true }),
      maxDate: dateSchema({ isNullable: true }),
      customError: booleanSchema({ docsOptions: { hideOptionIf: () => true } }),
      customWarning: booleanSchema({
        docsOptions: { hideOptionIf: () => true },
      }),
    },
    { initialValue: { isNullable: true } }
  ),
  inputs: recordSchema(
    {
      path: stringSchema(),
      isReadOnly: booleanSchema(),
    },
    { initialValue: { path: '/dates' } }
  ),
  outputs: recordSchema({
    change: booleanSchema(),
  }),
  i18n: recordSchema(
    {
      label: stringSchema(),
      code: stringSchema(),
      helpMessage: stringSchema(),
      legend: stringSchema(),
      format: stringSchema(),
      separator: stringSchema(),
      placeholder: stringSchema(),
    },
    {
      initialValue: {
        label: 'Date range',
        code: 'D',
        format: 'YYYY-MM-DD',
        separator: ' - ',
        placeholder: 'YYYY-MM-DD',
      },
    }
  ),
});
