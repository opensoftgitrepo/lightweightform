import {
  booleanSchema,
  listSchema,
  RecordSchema,
  recordSchema,
  stringSchema,
  tableSchema,
} from '@lightweightform/storage';

import { customValidate } from '../../../utils/schema-utils';

export const selectSchema: RecordSchema = recordSchema({
  select: stringSchema({
    isNullable: true,
    isRequired: (ctx) => ctx.get('schemaOptions/isRequired'),
    allowedValues: (ctx) => ctx.get('schemaOptions/allowedValues'),
    validate: customValidate,
    docsOptions: {
      displayNullOption: true,
    },
  }),
  schemaOptions: recordSchema(
    {
      type: stringSchema({
        initialValue: 'string',
        allowedValues: ['string'],
        docsOptions: { hideOptionIf: () => true, isReadOnly: true },
      }),
      isNullable: booleanSchema({ docsOptions: { isReadOnly: true } }),
      isRequired: booleanSchema(),
      allowedValues: listSchema(stringSchema(), {
        computedValue: (ctx) =>
          ctx.get('../i18n/options').map(({ value }) => value),
      }),
      customError: booleanSchema({ docsOptions: { hideOptionIf: () => true } }),
      customWarning: booleanSchema({
        docsOptions: { hideOptionIf: () => true },
      }),
    },
    { initialValue: { isNullable: true } }
  ),
  inputs: recordSchema(
    {
      path: stringSchema(),
      isReadOnly: booleanSchema(),
    },
    { initialValue: { path: '/path' } }
  ),
  outputs: recordSchema({
    change: booleanSchema(),
  }),
  i18n: recordSchema(
    {
      label: stringSchema(),
      code: stringSchema(),
      helpMessage: stringSchema(),
      legend: stringSchema(),
      options: tableSchema(
        recordSchema({
          value: stringSchema(),
          label: stringSchema(),
          code: stringSchema(),
        })
      ),
    },
    {
      initialValue: {
        label: 'Select',
        code: 'S',
        options: [
          { value: 'a', label: 'Option A' },
          { value: 'b', label: 'Option B' },
          { value: 'c', label: 'Option C' },
        ] as any,
      },
    }
  ),
});
