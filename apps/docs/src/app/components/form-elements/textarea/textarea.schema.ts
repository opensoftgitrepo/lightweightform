import {
  booleanSchema,
  numberSchema,
  RecordSchema,
  recordSchema,
  stringSchema,
} from '@lightweightform/storage';

import { customValidate } from '../../../utils/schema-utils';

export const textareaSchema: RecordSchema = recordSchema({
  textarea: stringSchema({
    minLength: (ctx) => {
      const minLength = ctx.get('schemaOptions/minLength');
      return minLength === null ? undefined : minLength;
    },
    maxLength: (ctx) => {
      const maxLength = ctx.get('schemaOptions/maxLength');
      return maxLength === null ? undefined : maxLength;
    },
    validate: customValidate,
  }),
  schemaOptions: recordSchema({
    minLength: numberSchema({ isNullable: true, min: 0 }),
    maxLength: numberSchema({ isNullable: true, min: 0 }),
    customError: booleanSchema({ docsOptions: { hideOptionIf: () => true } }),
    customWarning: booleanSchema({ docsOptions: { hideOptionIf: () => true } }),
  }),
  inputs: recordSchema(
    {
      path: stringSchema(),
      initialHeight: stringSchema({ initialValue: '6rem' }),
      isReadOnly: booleanSchema(),
    },
    { initialValue: { path: '/string' } }
  ),
  outputs: recordSchema({
    change: booleanSchema(),
  }),
  i18n: recordSchema(
    {
      label: stringSchema(),
      code: stringSchema(),
      helpMessage: stringSchema(),
      legend: stringSchema(),
    },
    { initialValue: { label: 'Textarea', code: 'T' } }
  ),
});
