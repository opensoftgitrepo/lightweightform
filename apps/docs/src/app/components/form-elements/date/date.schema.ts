import {
  booleanSchema,
  dateSchema as datSchema,
  RecordSchema,
  recordSchema,
  stringSchema,
} from '@lightweightform/storage';

import { customValidate } from '../../../utils/schema-utils';

export const dateSchema: RecordSchema = recordSchema({
  date: datSchema({
    isNullable: true,
    isRequired: (ctx) => ctx.get('schemaOptions/isRequired'),
    minDate: (ctx) => {
      const minDate = ctx.get('schemaOptions/minDate');
      return minDate === null ? undefined : minDate;
    },
    maxDate: (ctx) => {
      const maxDate = ctx.get('schemaOptions/maxDate');
      return maxDate === null ? undefined : maxDate;
    },
    validate: customValidate,
  }),
  schemaOptions: recordSchema(
    {
      isNullable: booleanSchema({ docsOptions: { isReadOnly: true } }),
      isRequired: booleanSchema(),
      minDate: datSchema({ isNullable: true }),
      maxDate: datSchema({ isNullable: true }),
      customError: booleanSchema({ docsOptions: { hideOptionIf: () => true } }),
      customWarning: booleanSchema({
        docsOptions: { hideOptionIf: () => true },
      }),
    },
    { initialValue: { isNullable: true } }
  ),
  inputs: recordSchema(
    {
      path: stringSchema(),
      isReadOnly: booleanSchema(),
    },
    { initialValue: { path: '/date' } }
  ),
  outputs: recordSchema({
    change: booleanSchema(),
  }),
  i18n: recordSchema(
    {
      label: stringSchema(),
      code: stringSchema(),
      helpMessage: stringSchema(),
      legend: stringSchema(),
      format: stringSchema(),
      placeholder: stringSchema(),
    },
    {
      initialValue: {
        label: 'Date',
        code: 'D',
        format: 'YYYY-MM-DD',
        placeholder: 'YYYY-MM-DD',
      },
    }
  ),
});
