import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { FormComponent } from '@lightweightform/bootstrap-theme';
import { makeObservable } from 'mobx';
import { computed, observable } from 'mobx-angular';

import { formatI18nProps } from '../../../utils/format-i18n-props';
import {
  formatInputs,
  formatOutputs,
} from '../../../utils/format-inputs-outputs';
import { formatSchemaOptions } from '../../../utils/format-schema-options';

@Component({
  selector: 'docs-date',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './date.component.html',
})
export class DateComponent {
  @observable
  @ViewChild(FormComponent)
  public form: FormComponent;

  constructor() {
    makeObservable(this);
  }

  @computed
  public get schemaCode(): string | undefined {
    if (this.form) {
      return `
        dateSchema(${formatSchemaOptions(
          this.form.relativeStorage,
          'schemaOptions'
        )})
      `;
    }
  }

  @computed
  public get htmlCode(): string | undefined {
    if (this.form) {
      return `
        <lf-date
          ${formatInputs(this.form.relativeStorage, 'inputs')}
          ${formatOutputs(this.form.relativeStorage, 'outputs')}
        ></lf-date>
      `;
    }
  }

  @computed
  public get i18nCode(): string | undefined {
    if (this.form) {
      return `{
        '${
          this.form.relativeStorage.get('inputs/path') || '*'
        }': {${formatI18nProps(this.form.relativeStorage, 'i18n')}}
      }`;
    }
  }

  @computed
  public get state(): Array<Record<string, any>> | undefined {
    if (this.form) {
      return [
        {
          name: 'value',
          state: this.form.relativeStorage.get('date'),
        },
        {
          name: 'isTouched',
          state:
            this.form.relativeStorage.getStateProperty('date', 'isTouched') ||
            false,
        },
        {
          name: 'isDirty',
          state:
            this.form.relativeStorage.getStateProperty('date', 'isDirty') ||
            false,
        },
      ];
    }
  }
}
