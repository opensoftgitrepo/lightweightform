import {
  booleanSchema,
  numberSchema,
  RecordSchema,
  recordSchema,
  stringSchema,
} from '@lightweightform/storage';

import { customValidate } from '../../../utils/schema-utils';

export const fileSchema: RecordSchema = recordSchema({
  file: stringSchema({
    isNullable: true,
    isRequired: (ctx) => ctx.get('schemaOptions/isRequired'),
    validate: customValidate,
  }),
  schemaOptions: recordSchema(
    {
      isNullable: booleanSchema({ docsOptions: { isReadOnly: true } }),
      isRequired: booleanSchema(),
      customError: booleanSchema({ docsOptions: { hideOptionIf: () => true } }),
      customWarning: booleanSchema({
        docsOptions: { hideOptionIf: () => true },
      }),
    },
    { initialValue: { isNullable: true } }
  ),
  inputs: recordSchema(
    {
      path: stringSchema(),
      isReadOnly: booleanSchema(),
      accept: stringSchema(),
      minSize: numberSchema({ isNullable: true }),
      maxSize: numberSchema({ isNullable: true }),
    },
    { initialValue: { path: '/file' } }
  ),
  outputs: recordSchema({
    change: booleanSchema(),
  }),
  i18n: recordSchema(
    {
      label: stringSchema(),
      code: stringSchema(),
      helpMessage: stringSchema(),
      legend: stringSchema(),
    },
    { initialValue: { label: 'File', code: 'F' } }
  ),
});
