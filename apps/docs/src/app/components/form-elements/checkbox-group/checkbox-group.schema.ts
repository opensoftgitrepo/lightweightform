import {
  booleanSchema,
  listSchema,
  numberSchema,
  RecordSchema,
  recordSchema,
  stringSchema,
  tableSchema,
} from '@lightweightform/storage';

import { customValidate } from '../../../utils/schema-utils';

export const checkboxGroupSchema: RecordSchema = recordSchema({
  checkboxGroup: listSchema(
    stringSchema({
      allowedValues: (ctx) => ctx.get('../elementsSchemaOptions/allowedValues'),
    }),
    {
      minSize: (ctx) => {
        const minSize = ctx.get('schemaOptions/minSize');
        return minSize === null ? undefined : minSize;
      },
      maxSize: (ctx) => {
        const maxSize = ctx.get('schemaOptions/maxSize');
        return maxSize === null ? undefined : maxSize;
      },
      validate: customValidate,
    }
  ),
  schemaOptions: recordSchema({
    minSize: numberSchema({ isNullable: true, min: 0 }),
    maxSize: numberSchema({ isNullable: true, min: 0 }),
    customError: booleanSchema({ docsOptions: { hideOptionIf: () => true } }),
    customWarning: booleanSchema({ docsOptions: { hideOptionIf: () => true } }),
  }),
  elementsSchemaOptions: recordSchema({
    type: stringSchema({
      initialValue: 'string',
      allowedValues: ['string'],
      docsOptions: { hideOptionIf: () => true, isReadOnly: true },
    }),
    allowedValues: listSchema(stringSchema(), {
      computedValue: (ctx) =>
        ctx.get('../i18n/options').map(({ value }) => value),
    }),
  }),
  inputs: recordSchema(
    {
      path: stringSchema(),
      displayInline: booleanSchema(),
      isReadOnly: booleanSchema(),
    },
    { initialValue: { path: '/list' } }
  ),
  outputs: recordSchema({
    change: booleanSchema(),
  }),
  i18n: recordSchema(
    {
      label: stringSchema(),
      code: stringSchema(),
      helpMessage: stringSchema(),
      legend: stringSchema(),
      options: tableSchema(
        recordSchema({
          value: stringSchema(),
          label: stringSchema(),
          code: stringSchema(),
        })
      ),
    },
    {
      initialValue: {
        label: 'Checkbox group',
        code: 'C',
        options: [
          { value: 'a', label: 'Option A' },
          { value: 'b', label: 'Option B' },
          { value: 'c', label: 'Option C' },
        ] as any,
      },
    }
  ),
});
