import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { FormComponent } from '@lightweightform/bootstrap-theme';
import { makeObservable } from 'mobx';
import { computed, observable } from 'mobx-angular';

import { formatI18nProps } from '../../../utils/format-i18n-props';
import {
  formatInputs,
  formatOutputs,
} from '../../../utils/format-inputs-outputs';
import { formatSchemaOptions } from '../../../utils/format-schema-options';

@Component({
  selector: 'docs-checkbox',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './checkbox.component.html',
})
export class CheckboxComponent {
  @observable
  @ViewChild(FormComponent)
  public form: FormComponent;

  constructor() {
    makeObservable(this);
  }

  @computed
  public get schemaCode(): string | undefined {
    if (this.form) {
      return `booleanSchema(${formatSchemaOptions(
        this.form.relativeStorage,
        'schemaOptions'
      )})`;
    }
  }

  @computed
  public get htmlCode(): string | undefined {
    if (this.form) {
      return `
        <lf-checkbox
          ${formatInputs(this.form.relativeStorage, 'inputs')}
          ${formatOutputs(this.form.relativeStorage, 'outputs')}
        ></lf-checkbox>
      `;
    }
  }

  @computed
  public get i18nCode(): string | undefined {
    if (this.form) {
      return `{
        '${
          this.form.relativeStorage.get('inputs/path') || '*'
        }': {${formatI18nProps(this.form.relativeStorage, 'i18n')}}
      }`;
    }
  }

  @computed
  public get state(): Array<Record<string, any>> | undefined {
    if (this.form) {
      return [
        { name: 'value', state: this.form.relativeStorage.get('checkbox') },
        {
          name: 'isTouched',
          state:
            this.form.relativeStorage.getStateProperty(
              'checkbox',
              'isTouched'
            ) || false,
        },
        {
          name: 'isDirty',
          state:
            this.form.relativeStorage.getStateProperty('checkbox', 'isDirty') ||
            false,
        },
      ];
    }
  }
}
