import {
  booleanSchema,
  RecordSchema,
  recordSchema,
  stringSchema,
} from '@lightweightform/storage';

import { customValidate } from '../../../utils/schema-utils';

export const checkboxSchema: RecordSchema = recordSchema({
  checkbox: booleanSchema({
    allowedValues: (ctx) => {
      const allowed = ctx.get('schemaOptions/allowedValues');
      return allowed === null ? undefined : [allowed];
    },
    validate: customValidate,
  }),
  schemaOptions: recordSchema({
    allowedValues: booleanSchema({
      isNullable: true,
      allowedValues: [true, false],
    }),
    customError: booleanSchema({ docsOptions: { hideOptionIf: () => true } }),
    customWarning: booleanSchema({ docsOptions: { hideOptionIf: () => true } }),
  }),
  inputs: recordSchema(
    {
      path: stringSchema(),
      isReadOnly: booleanSchema(),
    },
    { initialValue: { path: '/boolean' } }
  ),
  outputs: recordSchema({
    change: booleanSchema(),
  }),
  i18n: recordSchema(
    {
      label: stringSchema(),
      code: stringSchema(),
      helpMessage: stringSchema(),
      legend: stringSchema(),
    },
    { initialValue: { label: 'Checkbox', code: 'C' } }
  ),
});
