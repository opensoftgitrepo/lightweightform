import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { FormComponent } from '@lightweightform/bootstrap-theme';
import { makeObservable } from 'mobx';
import { computed, observable } from 'mobx-angular';

import { formatI18nProps } from '../../../utils/format-i18n-props';
import {
  formatInputs,
  formatOutputs,
} from '../../../utils/format-inputs-outputs';
import { formatSchemaOptions } from '../../../utils/format-schema-options';

@Component({
  selector: 'docs-table',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './table.component.html',
})
export class TableComponent {
  @observable
  @ViewChild(FormComponent)
  public form: FormComponent;

  constructor() {
    makeObservable(this);
  }

  @computed
  public get schemaCode(): string | undefined {
    if (this.form) {
      return `
        tableSchema(recordSchema({
          firstName: stringSchema({minLength: 1}),
          lastName: stringSchema(),
          age: numberSchema({
            isNullable: true,
            min: 0,
            validate: ctx => {
              if (ctx.get() > 130) {
                return {code: 'UNLIKELY_AGE', isWarning: true};
              }
            },
          }),
          married: booleanSchema(),
          profession: stringSchema({
            isNullable: true,
            allowedValues: ['singer', 'dancer'],
            computedValue: ctx => {
              const age = ctx.get('age');
              return age === null ? null : age > 30 ? 'singer' : 'dancer';
            },
          }),
        }), ${formatSchemaOptions(this.form.relativeStorage, 'schemaOptions')})
      `;
    }
  }

  @computed
  public get htmlCode(): string | undefined {
    if (this.form) {
      return `
        <lf-table
          ${formatInputs(this.form.relativeStorage, 'inputs')}
          ${formatOutputs(this.form.relativeStorage, 'outputs')}
        >
          <lf-table-header>
            <lf-table-column id="fullName">
              <lf-table-column id="firstName"></lf-table-column>
              <lf-table-column id="lastName"></lf-table-column>
            </lf-table-column>
            <lf-table-column id="age"></lf-table-column>
            <lf-table-column id="married"></lf-table-column>
            <lf-table-column id="profession"></lf-table-column>
          </lf-table-header>

          <ng-template #lfTableRowTemplate let-id="id">
            <tr lf-table-row [path]="id">
              <td lf-table-cell><lf-text path="firstName"></lf-text></td>
              <td lf-table-cell><lf-text path="lastName"></lf-text></td>
              <td lf-table-cell><lf-number path="age"></lf-number></td>
              <td lf-table-cell><lf-checkbox path="married"></lf-checkbox></td>
              <td lf-table-cell><lf-select path="profession"></lf-select></td>
            </tr>
          </ng-template>
        </lf-table>
      `;
    }
  }

  @computed
  public get i18nCode(): string | undefined {
    if (this.form) {
      const columnLabels = this.form.relativeStorage.get('i18n/columnLabels');
      return `{
        '${
          this.form.relativeStorage.get('inputs/path') || '*'
        }': {${formatI18nProps(this.form.relativeStorage, 'i18n').replace(
        JSON.stringify(columnLabels),
        columnLabels
      )}}
      }`;
    }
  }

  @computed
  public get state(): Array<Record<string, any>> | undefined {
    if (this.form) {
      return [
        {
          name: 'value',
          state: this.form.relativeStorage.getAsJS('table'),
        },
        {
          name: 'isTouched',
          state:
            this.form.relativeStorage.getStateProperty('table', 'isTouched') ||
            false,
        },
        {
          name: 'isDirty',
          state:
            this.form.relativeStorage.getStateProperty('table', 'isDirty') ||
            false,
        },
      ];
    }
  }
}
