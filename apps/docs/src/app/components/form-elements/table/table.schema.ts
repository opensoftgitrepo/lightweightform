import {
  booleanSchema,
  numberSchema,
  RecordSchema,
  recordSchema,
  stringSchema,
  tableSchema as tabSchema,
} from '@lightweightform/storage';

import { customValidate } from '../../../utils/schema-utils';

export const tableSchema: RecordSchema = recordSchema({
  table: tabSchema(
    recordSchema({
      firstName: stringSchema({ minLength: 1 }),
      lastName: stringSchema(),
      age: numberSchema({
        isNullable: true,
        min: 0,
        validate: (ctx) => {
          if (ctx.get() > 130) {
            return { code: 'UNLIKELY_AGE', isWarning: true };
          }
        },
      }),
      married: booleanSchema(),
      profession: stringSchema({
        isNullable: true,
        allowedValues: ['singer', 'dancer'],
        isClientOnly: false,
        computedValue: (ctx) => {
          const age = ctx.get('age');
          return age === null ? null : age > 30 ? 'singer' : 'dancer';
        },
      }),
    }),
    {
      initialValue: [
        { firstName: 'John', lastName: 'Smith', age: 35, married: true },
        { firstName: 'Sarah', lastName: 'Jones', age: 24, married: false },
        { firstName: 'Joe', lastName: 'Moore', age: 44, married: true },
      ],
      minSize: (ctx) => {
        const minSize = ctx.get('schemaOptions/minSize');
        return minSize === null ? undefined : minSize;
      },
      maxSize: (ctx) => {
        const maxSize = ctx.get('schemaOptions/maxSize');
        return maxSize === null ? undefined : maxSize;
      },
      validate: customValidate,
    }
  ),
  schemaOptions: recordSchema({
    minSize: numberSchema({ isNullable: true, min: 0 }),
    maxSize: numberSchema({ isNullable: true, min: 0 }),
    customError: booleanSchema({ docsOptions: { hideOptionIf: () => true } }),
    customWarning: booleanSchema({ docsOptions: { hideOptionIf: () => true } }),
  }),
  inputs: recordSchema(
    {
      path: stringSchema(),
      rowHeight: numberSchema({
        isNullable: true,
        initialValue: 42,
        min: 0,
        docsOptions: { isReadOnly: true },
      }),
      maxVisibleRows: numberSchema({
        isNullable: true,
        initialValue: 10,
        min: 1,
      }),
      isReadOnly: booleanSchema(),
    },
    { initialValue: { path: '/table' } }
  ),
  outputs: recordSchema({
    change: booleanSchema(),
  }),
  i18n: recordSchema(
    {
      label: stringSchema(),
      code: stringSchema(),
      helpMessage: stringSchema(),
      legend: stringSchema(),
      columnLabels: stringSchema({
        docsOptions: { isReadOnly: true },
      }),
      addRowActionText: stringSchema({ initialValue: 'Add row' }),
      removeRowsActionText: stringSchema({ initialValue: 'Remove rows' }),
      noRowsText: stringSchema({ initialValue: 'No rows to display.' }),
    },
    {
      initialValue: {
        label: 'Table',
        code: 'T',
        columnLabels:
          "{fullName: 'Full name', firstName: 'First name', lastName: 'Last name', age: 'Age', married: 'Married?', profession: 'Profession'}",
        addRowActionText: 'Add person',
        removeRowsActionText: 'Remove people',
        noRowsText: 'No people have been added.',
      },
    }
  ),
});
