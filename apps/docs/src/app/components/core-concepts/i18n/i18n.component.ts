import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'docs-i18n',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './i18n.component.html',
})
export class I18nComponent {
  public i18nFile1Code = `
    const enUs = {
      '*': {
        key1: 'Some string',
        key2: 'Another string',
      },
      '/some/path': {
        key1: 'Some string',
      },
    };
  `;
}
