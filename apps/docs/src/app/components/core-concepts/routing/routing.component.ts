import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'docs-routing',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './routing.component.html',
})
export class RoutingComponent {}
