import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'docs-storage-schema',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './storage-schema.component.html',
})
export class StorageSchemaComponent {
  public personSchema1Code = `
    const personSchema = recordSchema({
      name: stringSchema(),
      dateOfBirth: dateSchema({ isNullable: true }),
      phoneNumbers: listSchema(stringSchema()),
      isMarried: booleanSchema(),
      numberOfChildren: numberSchema({ isNullable: true, isInteger: true }),
    });
  `;

  public personSchema2Code = `
    const personSchema = {
      type: 'record',
      fieldsSchemas: {
        name: { type: 'string' },
        dateOfBirth: { type: 'date', isNullable: true },
        phoneNumbers: { type: 'list', elementsSchema: { type: 'string' } },
        isMarried: { type: 'boolean' },
        numberOfChildren: { type: 'number', isNullable: true, isInteger: true },
      },
    };
  `;

  public aliceCode = `
    const alice = {
      name: 'Alice Smith',
      dateOfBirth: new Date('1990-01-01'),
      phoneNumbers: ['+1-202-555-0198'],
      isMarried: false,
      numberOfChildren: null,
    };
  `;

  public storageUsageCode = `
    storage.set('/', alice);
    storage.get('/name'); // 'Alice Smith'
    storage.get('/phoneNumbers/0'); // '+1-202-555-0198'
    storage.set('/numberOfChildren', 0);
    storage.push('/phoneNumbers', '+44 01694-488820');
  `;

  public textComponentCode = '<lf-text path="/name"></lf-text>';

  public relativeStorageCode = `
    storage.currentPath; // '/'

    // \`name\` is equivalent to \`./name\` or, in this case, \`/name\`
    const relativeStorage = storage.relativeStorage('name');
    relativeStorage.currentPath; // '/name'
    relativeStorage.get(); // 'Alice Smith'
    relativeStorage.get('../numberOfChildren'); // 0
    relativeStorage.set('../isMarried', true);

    // "Absolute" paths can still be used
    relativeStorage.get('/isMarried'); // true
  `;

  public computedValueCode = `
    const personSchema = recordSchema({
      dateOfBirth: dateSchema({ isNullable: true }),
      age: numberSchema({
        isNullable: true,
        isInteger: true,
        computedValue: ctx => {
          const dateOfBirth = ctx.get('/dateOfBirth');
          if (dateOfBirth === null) {
            return null;
          }
          return Math.floor((new Date() - dateOfBirth.getTime()) / 3.15576e+10);
        },
      })
    });
  `;

  public computedValueStorageCode = `
    // Assume that the current time is \`2019-01-01\`
    storage.set('/dateOfBirth', new Date('2000-06-01'));
    storage.get('/age'); // 18
    storage.set('/dateOfBirth', new Date('2015-06-01'));
    storage.get('/age'); // 3

    storage.set('/age', 25); // Throws error (computed values cannot be set)
  `;

  public tableSchemaCode = `
    const appSchema = recordSchema({
      people: tableSchema(personSchema),
    });
  `;
}
