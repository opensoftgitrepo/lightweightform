<lf-form *mobxAutorun path="coreConcepts/storageSchema">
  <section>
    <p>
      LF stores all form data within an instance of a storage (<docs-api-ref
        pkg="core"
        name="LfStorage"
      ></docs-api-ref>
      service). This storage is responsible for performing data validation and
      provides an API to access and manipulate said data.
    </p>

    <p>
      Data inserted in the storage must respect a predefined structure (<em
        >i.e.</em
      >
      it must be properly typed). This structure is defined by instantiating the
      storage with a schema: which defines the type of data allowed in each
      field of the form and how each field should be validated.
    </p>

    <p>
      The storage performs two kinds of validation: type validation, in which a
      value is type checked against its expected schema type; and
      <a routerLink="/coreConcepts/dataValidation">data validation</a>, where
      the "business logic" determines whether a certain value is valid. Values
      stored in the storage are always valid in respect to their type, as
      attempting to store an incorrectly typed value will result in an error
      being thrown.
    </p>
  </section>

  <section>
    <h2 docs-heading ref="schemaCreation">Schema creation</h2>

    <p>
      There exists a schema type for each basic JavaScript value type:
      <docs-api-ref pkg="storage" name="BooleanSchema"></docs-api-ref>,
      <docs-api-ref pkg="storage" name="DateSchema"></docs-api-ref>,
      <docs-api-ref pkg="storage" name="NumberSchema"></docs-api-ref>, and
      <docs-api-ref pkg="storage" name="StringSchema"></docs-api-ref>; as well
      as for some complex structures:
      <docs-api-ref pkg="storage" name="ListSchema"></docs-api-ref>,
      <docs-api-ref pkg="storage" name="MapSchema"></docs-api-ref>,
      <docs-api-ref pkg="storage" name="RecordSchema"></docs-api-ref>,
      <docs-api-ref pkg="storage" name="TableSchema"></docs-api-ref>, and
      <docs-api-ref pkg="storage" name="TupleSchema"></docs-api-ref>. Most LF
      field components expect that the storage values that they represent
      conform to specific schemas; <em>e.g.</em> the
      <docs-api-ref pkg="bootstrap-theme" name="TextComponent"
        ><code>&lt;lf-text&gt;</code></docs-api-ref
      >
      component must represent string values (with a string schema).
    </p>

    <p>
      As an example, the schema for a form intending to gather the data of a
      person could be defined as follows:
    </p>

    <docs-code language="typescript" [code]="personSchema1Code"></docs-code>

    <p>
      The above functions are provided by the
      <docs-api-ref pkg="storage"></docs-api-ref> module (there is one function
      for each schema type) and the resulting schema is equivalent to the
      following object:
    </p>

    <docs-code language="typescript" [code]="personSchema2Code"></docs-code>

    <p>
      The
      <docs-api-ref pkg="storage" name="Schema.isNullable"></docs-api-ref>
      property stipulates whether the field may contain the value
      <code>null</code> (in terms of type, meaning that attempting to store the
      value <code>null</code> when the respecting schema is non-nullable results
      in a thrown error). Similarly, the
      <docs-api-ref pkg="storage" name="NumberSchema.isInteger"></docs-api-ref>
      property (supported only by
      <code>number</code>
      schemas) validates (in terms of type) numeric values by making sure that
      they are integers.
    </p>

    <p>
      The following is a value that conforms to the defined schema in terms of
      types:
    </p>

    <docs-code language="typescript" [code]="aliceCode"></docs-code>
  </section>

  <section>
    <h2 docs-heading ref="initialValues">Initial values</h2>

    <p>
      Values stored in the storage are never <code>undefined</code>, this means
      that when a storage is initialised with a given schema, all values are
      initialised with a default value. The default values for each schema type
      are as follows:
    </p>

    <ul>
      <li>
        Values of nullable schemas are initialised with the value
        <code>null</code>;
      </li>
      <li>Boolean values are initialised as <code>false</code>;</li>
      <li>Date values are initialised with <code>new Date(0)</code>;</li>
      <li>Number values are initialised with <code>0</code>;</li>
      <li>String values receive an empty string <code>''</code>;</li>
      <li>All collections are initialised as an empty collection;</li>
      <li>
        Record and tuple values are initialised with all of its fields set to
        their respective initial values.
      </li>
    </ul>

    <p>
      Initial values may be changed by specifying the
      <docs-api-ref pkg="storage" name="Schema.initialValue"></docs-api-ref>
      property of their respective schemas. Furthermore, the storage API
      provides a <docs-api-ref pkg="core" name="LfStorage.reset"></docs-api-ref>
      method to reset a given value to its initial value.
    </p>
  </section>

  <section>
    <h2 docs-heading ref="storageUsage">Getting and setting data</h2>

    <p>
      All data in the storage has an associated "path" (a similar concept to
      file paths in a typical file system). For instance, the name of the person
      in the previous example can be accessed via the path <code>/name</code>;
      their first phone number would be accessed via
      <code>/phoneNumbers/0</code>; the person object itself is represented by
      the root path <code>/</code>.
    </p>

    <p>
      To access and manipulate a certain field of a form, we must use the
      storage API and reference said field by its path. As an example, if we
      have a storage instance <code>storage</code> initialised with the above
      <code>personSchema</code>, then we can access and edit the form's data in
      the following fashion:
    </p>

    <docs-code language="typescript" [code]="storageUsageCode"></docs-code>

    <p>
      For the full storage API, see:
      <docs-api-ref pkg="core" name="LfStorage"></docs-api-ref>.
    </p>

    <p>
      LF uses storage paths extensively in order to associate Angular components
      to the form data values that they represent. For instance, the following
      LF component can be used to display a text field that manipulates the
      person's name:
    </p>

    <docs-code language="html" [code]="textComponentCode"></docs-code>
  </section>

  <section>
    <h2 docs-heading ref="relativeStorageUsage">Relative storages and paths</h2>

    <p>
      When a storage instance is initially created, an internal property is
      created that sets its "current working path" to the root path
      <code>/</code>. Relative storages are instances of a storage that share
      the schema and data of the initial storage but that may have a different
      current working path.
    </p>

    <p>
      All paths provided to methods of the storage API are relative to the
      instance's current working path. Similarly to file system paths, storage
      paths may thus contain the special names <code>.</code> and
      <code>..</code> to represent the current and parent path respectively;
      furthermore, the path <code>x</code> is equivalent to <code>./x</code>.
      All storage API methods that receive a path default their path to
      <code>'.'</code>. The following continuation of the previous example shows
      a valid usage of relative storages and paths:
    </p>

    <docs-code language="typescript" [code]="relativeStorageCode"></docs-code>

    <p>
      Relative storages may further be created in "read-only" mode: meaning that
      no data manipulation is allowed from such instances.
    </p>
  </section>

  <section>
    <h2 docs-heading ref="computedValues">Computed values</h2>

    <p>
      LF supports the notion of "computed values": which behave similarly to
      cells in your typical spreadsheet whose value depends on other cells.
      Computed values are defined in schemas via the
      <docs-api-ref pkg="storage" name="Schema.computedValue"></docs-api-ref>
      property and can be used as follows:
    </p>

    <docs-code language="typescript" [code]="computedValueCode"></docs-code>

    <p>
      Example storage interaction with computed values where
      <code>storage</code> is a storage instance initialised with the above
      schema:
    </p>

    <docs-code
      language="typescript"
      [code]="computedValueStorageCode"
    ></docs-code>
  </section>

  <section>
    <h2 docs-heading ref="tableSchemas">Table schemas</h2>

    <p>
      Table schemas are schemas meant to represent (possibly large) collections.
      They differ from lists in that their elements <strong>must</strong> be
      records and each element (dubbed a "row") is identified by an
      auto-generated identifier that does not change over time (<em>i.e.</em>
      they are not accessed by their index).
    </p>

    <p>
      Tables are the preferred way of representing collections in LF. The
      following example shows the schema of a form that collects information
      about a collection of people:
    </p>

    <docs-code language="typescript" [code]="tableSchemaCode"></docs-code>
  </section>
</lf-form>
