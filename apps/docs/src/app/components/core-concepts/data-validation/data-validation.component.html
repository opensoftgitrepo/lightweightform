<lf-form *mobxAutorun path="coreConcepts/dataValidation">
  <section>
    <p>
      Form data is validated by the storage according to the schemas associated
      with said data.
    </p>
  </section>

  <section>
    <h2 docs-heading ref="validationProperties">Validation properties</h2>

    <p>
      Different schema types may support different "native" validation
      properties: as an example, schemas of type <code>number</code> may contain
      <code>min</code> and <code>max</code> properties to respectively specify
      the minimum and maximum allowed value. On the other hand, some properties
      are available for all schema types, such as <code>allowedValues</code> and
      the more generic <code>validate</code>. When querying the storage API to
      check whether a form is invalid, the API will return
      <docs-api-ref pkg="storage" name="ValidationIssue"
        >validation issues</docs-api-ref
      >.
    </p>

    <p>The following is a list of all supported validation properties:</p>

    <ul>
      <li>
        <docs-api-ref pkg="storage" name="Schema.allowedValues"></docs-api-ref>:
        supported by all schema types, expects a list of "valid" values. Form
        entries with non-allowed values will contain a validation issue with
        code
        <docs-api-ref pkg="storage" name="DISALLOWED_VALUE_CODE"
          ><code>LF_DISALLOWED_VALUE</code></docs-api-ref
        >.
      </li>
      <li>
        <docs-api-ref pkg="storage" name="Schema.isRequired"></docs-api-ref>:
        supported by nullable schemas to specify whether <code>null</code> is an
        allowed value. Nullable form entries that are required and have a value
        of <code>null</code> will contain a validation issue with code
        <docs-api-ref pkg="storage" name="IS_REQUIRED_CODE"
          ><code>LF_IS_REQUIRED</code></docs-api-ref
        >.
      </li>
      <li>
        <docs-api-ref pkg="storage" name="NumberSchema.min"></docs-api-ref
        >/<docs-api-ref pkg="storage" name="NumberSchema.max"></docs-api-ref>:
        supported by <code>number</code> schemas to specify the mininum and
        maximum allowed value, respectively. Out of bounds numbers will contain
        a validation issue with code
        <docs-api-ref pkg="storage" name="NUMBER_OUT_OF_BOUNDS_CODE"
          ><code>LF_NUMBER_OUT_OF_BOUNDS</code></docs-api-ref
        >.
      </li>
      <li>
        <docs-api-ref pkg="storage" name="DateSchema.minDate"></docs-api-ref
        >/<docs-api-ref pkg="storage" name="DateSchema.maxDate"></docs-api-ref>:
        supported by <code>date</code> schemas to specify the minimum and
        maximum allowed date, respectively. Out of bounds dates will contain a
        validation issue with code
        <docs-api-ref pkg="storage" name="DATE_OUT_OF_BOUNDS_CODE"
          ><code>LF_DATE_OUT_OF_BOUNDS</code></docs-api-ref
        >.
      </li>
      <li>
        <docs-api-ref pkg="storage" name="StringSchema.minLength"></docs-api-ref
        >/<docs-api-ref
          pkg="storage"
          name="StringSchema.maxLength"
        ></docs-api-ref
        >: supported by <code>string</code> schemas to specify the minimum and
        maximum number of characters allowed, respectively. Strings with out of
        bound lengths will contain a validation issue with code
        <docs-api-ref pkg="storage" name="LENGTH_OUT_OF_BOUNDS_CODE"
          ><code>LF_LENGTH_OUT_OF_BOUNDS</code></docs-api-ref
        >.
      </li>
      <li>
        <docs-api-ref
          pkg="storage"
          name="CollectionSchema.minSize"
        ></docs-api-ref
        >/<docs-api-ref
          pkg="storage"
          name="CollectionSchema.maxSize"
        ></docs-api-ref
        >: supported by collection schemas (<code>list</code>, <code>map</code>,
        and <code>table</code>) to specify the minimum and maximum allowed
        collection size, respectively. Collections with sizes out of bounds will
        contain a validation issue with code
        <docs-api-ref pkg="storage" name="SIZE_OUT_OF_BOUNDS_CODE"
          ><code>LF_SIZE_OUT_OF_BOUNDS</code></docs-api-ref
        >.
      </li>
      <li>
        <docs-api-ref pkg="storage" name="Schema.validate"></docs-api-ref>:
        supported by all schema types to specify custom validation functions;
        does not run when the value is <code>null</code> (for nullable schemas).
        It accepts a single validation function or an array of them. Each
        validation function may return one or more validation issues (as an
        array).
      </li>
    </ul>
  </section>

  <section>
    <h2 docs-heading ref="errorsWarnings">Errors and warnings</h2>

    <p>
      LF allows a distinction between errors and warnings when validating data.
      When emitting a validation issue from a validation function, one may add
      an <code>isWarning</code> property with the value <code>true</code> to
      identify said issue as a warning. Non-warning validation issues are seen
      by LF as errors.
    </p>

    <p>
      The storage API provides methods to list issues, errors, and warnings: see
      <docs-api-ref pkg="core" name="LfStorage.validationIssues"></docs-api-ref
      >,
      <docs-api-ref pkg="core" name="LfStorage.validationErrors"></docs-api-ref
      >,
      <docs-api-ref
        pkg="core"
        name="LfStorage.validationWarnings"
      ></docs-api-ref
      >, and other variants (such as
      <docs-api-ref
        pkg="core"
        name="LfStorage.localValidationIssues"
      ></docs-api-ref
      >).
    </p>
  </section>

  <section>
    <h2 docs-heading ref="example">Example</h2>

    <p>
      The following adaptation of the schema used in the previous page adds some
      validations to the "person form":
    </p>

    <docs-code language="typescript" [code]="validationsCode"></docs-code>

    <p>The added validations do the following:</p>

    <ul>
      <li>Make sure that the person's name has at least 1 character;</li>
      <li>
        The date of birth cannot be <code>null</code> and must be less than the
        current date;
      </li>
      <li>
        If the person is over 18 years old, then at least 1 phone number is
        required;
      </li>
      <li>
        Phone numbers must have at least 1 character and start with a
        <code>+</code>;
      </li>
      <li>
        The number of children cannot be <code>null</code> and must be between 0
        and 99.
      </li>
    </ul>

    <p>
      As seen in the example, schema validations accept custom functions. These
      functions receive as an argument relative storages with their current
      working path set either to the path of the value being validated (<code
        >validate</code
      >
      function) or to the path of their parent value (all other functions).
    </p>

    <p>
      The storage offers an API to list all
      <docs-api-ref pkg="storage" name="ValidationIssuesMap"
        >validation issues</docs-api-ref
      >
      associated with a given path. See:
      <docs-api-ref pkg="core" name="LfStorage.validationIssues"></docs-api-ref>
      and
      <docs-api-ref
        pkg="core"
        name="LfStorage.localValidationIssues"
      ></docs-api-ref
      >. The following example shows an interaction with a storage instance
      <code>storage</code> initialized with the above schema:
    </p>

    <docs-code language="typescript" [code]="storageApiCode"></docs-code>

    <p>
      Behind the scenes, LF uses this API to display the issues associated with
      each field of the form. The <code>data</code> property of the validation
      issues is used to help display proper error messages to the user of the
      form.
    </p>
  </section>

  <section>
    <h2 docs-heading ref="asyncValidations">Asynchronous validations</h2>

    <p>
      All validation functions can be asynchronous if they return a promise.
      However, there is a caveat in how data should be accessed within the
      asynchronous validation function: other form values should be accessed
      within the synchronous context of the function, <em>i.e.</em> before the
      first <code>await</code> (when using <code>async</code>/<code>await</code>
      syntax). Consider the following example with an asynchronous validation:
    </p>

    <docs-code language="typescript" [code]="asyncValidation1Code"></docs-code>

    <p>
      Due to how the tracking of dependencies in LF works (MobX is being used),
      changes to the value with path <code>/nationality</code> will not cause
      the validation function to be reevaluated. To work around this issue, we
      should rewrite the above validation function as:
    </p>

    <docs-code language="typescript" [code]="asyncValidation2Code"></docs-code>

    Rejected validation functions will cause a
    <docs-api-ref pkg="storage" name="ValidationIssue"
      >validation issue</docs-api-ref
    >
    with code
    <docs-api-ref pkg="storage" name="IS_REJECTED_CODE"
      ><code>LF_IS_REJECTED</code></docs-api-ref
    >.
  </section>

  <section>
    <h2 docs-heading ref="touchedDirtyStates">Touched and dirty states</h2>

    <p>
      LF keeps track of whether field values have been touched (the user has
      blurred a field) or are dirty (a field's value has been changed due to
      user interaction). To prevent forms from displaying errors on fields as
      soon as they are initialised, LF only shows validation issues on "touched"
      fields.
    </p>

    <p>
      Although LF marks fields as touched and dirty when appropriate, it is
      still possible to manually set them. This is important when setting values
      on the storage programmatically and can be done through the storage API:
      see
      <docs-api-ref pkg="core" name="LfStorage.setTouched"></docs-api-ref> and
      <docs-api-ref pkg="core" name="LfStorage.setDirty"></docs-api-ref>.
    </p>
  </section>
</lf-form>
