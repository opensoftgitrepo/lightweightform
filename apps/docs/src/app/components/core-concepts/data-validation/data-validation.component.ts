import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'docs-data-validation',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './data-validation.component.html',
})
export class DataValidationComponent {
  public validationsCode = `
    const personSchema = recordSchema({
      name: stringSchema({minLength: 1}),
      dateOfBirth: dateSchema({
        isNullable: true,
        isRequired: true,
        maxDate: new Date(),
      }),
      phoneNumbers: listSchema(
        stringSchema({
          minLength: 1,
          validate: ctx => {
            const phoneNumber = ctx.get();
            if (phoneNumber && !phoneNumber.startsWith('+')) {
              return { code: 'NO_COUNTRY_CODE', isWarning: true };
            }
          },
        }),
        // Assume that \`isOver18\` is a function that returns whether a given
        // birth date makes a person over 18 years of age
        {minSize: ctx => (isOver18(ctx.get('dateOfBirth')) ? 1 : 0)},
      ),
      isMarried: booleanSchema(),
      numberOfChildren: numberSchema({
        isNullable: true,
        isInteger: true,
        isRequired: true,
        min: 0,
        max: 99,
      }),
    });
  `;

  public storageApiCode = `
    storage.set('/', {
      name: '',
      dateOfBirth: new Date('1990-01-01'),
      phoneNumbers: [],
      isMarried: false,
      numberOfChildren: -1
    });
    storage.validationIssues('/');
    // {
    //   '/name': [{
    //     code: 'LF_LENGTH_OUT_OF_BOUNDS',
    //     data: { length: 0, minLength: 1 },
    //   }],
    //   '/phoneNumbers': [{
    //     code: 'LF_SIZE_OUT_OF_BOUNDS',
    //     data: { size: 0, minSize: 1 },
    //   }],
    //   '/numberOfChildren': [{
    //     code: 'LF_NUMBER_OUT_OF_BOUNDS',
    //     data: { value: -1, min: 0, max: 99 },
    //   }],
    // }

    storage.push('/phoneNumbers', '123456789');
    storage.localValidationIssues('/phoneNumbers/0');
    // [{ code: 'NO_COUNTRY_CODE', isWarning: true }]
  `;

  public asyncValidation1Code = `
  const personSchema = recordSchema({
    nationality: stringSchema({
      validate: async ctx => {
        const availableNationalities = await fetchNationalities();
        if (!availableNationalities.includes(ctx.get())) {
          return { code: 'INVALID_NATIONALITY' };
        }
      }
    }),
  });
  `;

  public asyncValidation2Code = `
    async ctx => {
      const nationality = ctx.get();
      const availableNationalities = await fetchNationalities();
      if (!availableNationalities.includes(nationality)) {
        return { code: 'INVALID_NATIONALITY' };
      }
    }
  `;
}
