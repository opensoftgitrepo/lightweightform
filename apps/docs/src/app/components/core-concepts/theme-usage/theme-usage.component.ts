import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'docs-theme-usage',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './theme-usage.component.html',
})
export class ThemeUsageComponent {}
