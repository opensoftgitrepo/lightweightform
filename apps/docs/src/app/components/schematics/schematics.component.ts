import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'docs-schematics',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './schematics.component.html',
})
export class SchematicsComponent {
  public initExample = `\
ng add @lightweightform/bootstrap-theme -m mod -l en-US,pt-PT\
`;

  public formExample = `\
ng g @lightweightform/bootstrap-theme:form my-form
`;

  public collectionExample = `\
ng g @lightweightform/bootstrap-theme:form -c my-form/my-collection/another-form
`;
}
