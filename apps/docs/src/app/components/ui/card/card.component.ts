import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  ViewChild,
} from '@angular/core';
import { FormComponent } from '@lightweightform/bootstrap-theme';
import { getRowId } from '@lightweightform/storage';
import { makeObservable } from 'mobx';
import { computed, observable } from 'mobx-angular';

import { BOOTSTRAP_DOCS_URL } from '../../../injection-tokens';
import { formatInputs } from '../../../utils/format-inputs-outputs';

@Component({
  selector: 'docs-card',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './card.component.html',
})
export class CardComponent {
  @observable
  @ViewChild(FormComponent)
  public form: FormComponent;

  constructor(@Inject(BOOTSTRAP_DOCS_URL) public bootstrapDocsURL: string) {
    makeObservable(this);
  }

  @computed
  public get htmlCode(): string | undefined {
    if (this.form) {
      return `
      <lf-card ${formatInputs(this.form.relativeStorage, 'inputs')}>
        ${this.form.relativeStorage
          .get('inputs/content')
          .map(
            (content) => `
            <lf-card-content ${formatInputs(
              this.form.relativeStorage,
              `inputs/content/${getRowId(content)}`
            )}>
              ${content.ngContent}
            </lf-card-content>`
          )
          .join('')}
      </lf-card>
    `;
    }
  }

  public replaceNgContent(ngContent: string): string {
    return ngContent
      .replace(/ lf-card-content/g, ' ')
      .replace(/<(\/)?lf-card-content/g, '<$1div')
      .replace(
        /contentType="([^"]*)"|\[contentType\]="'([^"]*)'"/g,
        'class="card-$1$2"'
      );
  }
}
