import {
  RecordSchema,
  recordSchema,
  stringSchema,
  tableSchema,
} from '@lightweightform/storage';

export const cardSchema: RecordSchema = recordSchema({
  inputs: recordSchema(
    {
      path: stringSchema(),
      class: stringSchema(),
      content: tableSchema(
        recordSchema(
          {
            contentType: stringSchema({
              allowedValues: ['header', 'body', 'footer'],
            }),
            ngContent: stringSchema({
              docsOptions: { hideInputIf: () => true },
            }),
          },
          { initialValue: { contentType: 'body' } as any }
        ),
        {
          initialValue: [
            { contentType: 'header', ngContent: 'Header' },
            {
              contentType: 'body',
              ngContent:
                '<h5 lf-card-content contentType="title">Title</h5> Body',
            },
          ],
          docsOptions: {
            hideInputIf: () => true,
          },
        }
      ),
    },
    { initialValue: { class: 'bg-primary text-white' } }
  ),
});
