import {
  booleanSchema,
  RecordSchema,
  recordSchema,
  stringSchema,
} from '@lightweightform/storage';

export const modalSchema: RecordSchema = recordSchema({
  modal: recordSchema({
    autoFocusTest: stringSchema(),
  }),
  inputs: recordSchema({
    path: stringSchema(),
    startVisible: booleanSchema(),
    size: stringSchema({
      isNullable: true,
      allowedValues: ['sm', 'lg'],
    }),
    isCentered: booleanSchema(),
    animate: booleanSchema({ initialValue: true }),
    closeWithEsc: booleanSchema({ initialValue: true }),
    showBackdrop: booleanSchema({ initialValue: true }),
    ignoreBackdropClick: booleanSchema(),
    autoFocus: booleanSchema(),
  }),
  outputs: recordSchema({
    show: booleanSchema(),
    hide: booleanSchema(),
    shown: booleanSchema(),
    hidden: booleanSchema(),
  }),
  headerInputs: recordSchema({
    showCloseButton: booleanSchema({ initialValue: true }),
    ngContent: stringSchema({
      initialValue: 'Header',
      docsOptions: {
        useTextarea: true,
        hideInputIf: () => true,
      },
    }),
  }),
  bodyInputs: recordSchema({
    ngContent: stringSchema({
      initialValue: 'Hello world!',
      docsOptions: {
        useTextarea: true,
        hideInputIf: () => true,
      },
    }),
  }),
  footerInputs: recordSchema({
    ngContent: stringSchema({
      initialValue:
        '<button lf-button [style]="\'secondary\'" (click)="modal.hide()">' +
        'Close</button>',
      docsOptions: {
        useTextarea: true,
        initialHeight: '1rem',
        isReadOnly: true,
        hideInputIf: () => true,
      },
    }),
  }),
  i18n: recordSchema(
    {
      closeButtonTooltip: stringSchema(),
    },
    { initialValue: { closeButtonTooltip: 'Close' } }
  ),
});
