import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  ViewChild,
} from '@angular/core';
import {
  FormComponent,
  ModalComponent as LfModalComponent,
} from '@lightweightform/bootstrap-theme';
import { makeObservable } from 'mobx';
import { computed, observable } from 'mobx-angular';

import { BOOTSTRAP_DOCS_URL } from '../../../injection-tokens';
import { formatI18nProps } from '../../../utils/format-i18n-props';
import {
  formatInputs,
  formatOutputs,
} from '../../../utils/format-inputs-outputs';

@Component({
  selector: 'docs-modal',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './modal.component.html',
})
export class ModalComponent {
  @observable
  @ViewChild(FormComponent)
  public form: FormComponent;

  @observable
  @ViewChild('modal')
  public modal: LfModalComponent;

  constructor(@Inject(BOOTSTRAP_DOCS_URL) public bootstrapDocsURL: string) {
    makeObservable(this);
  }

  @computed
  public get htmlCode(): string | undefined {
    if (this.form) {
      return `
      <lf-modal
        #modal
        ${formatInputs(this.form.relativeStorage, 'inputs')}
        ${formatOutputs(this.form.relativeStorage, 'outputs')}
      >
        ${
          this.form.relativeStorage.get('headerInputs/showCloseButton') ||
          this.form.relativeStorage.get('headerInputs/ngContent')
            ? `<lf-modal-header ${formatInputs(
                this.form.relativeStorage,
                'headerInputs'
              )}>
                 ${this.form.relativeStorage.get('headerInputs/ngContent')}
              </lf-modal-header>`
            : ''
        }
        <lf-modal-body ${formatInputs(this.form.relativeStorage, 'bodyInputs')}>
          ${this.form.relativeStorage.get('bodyInputs/ngContent')}
        </lf-modal-body>
        <lf-modal-footer ${formatInputs(
          this.form.relativeStorage,
          'footerInputs'
        )}>
          ${this.form.relativeStorage.get('footerInputs/ngContent')}
        </lf-modal-footer>
      </lf-modal>
    `;
    }
  }

  @computed
  public get i18nCode(): string | undefined {
    if (this.form) {
      return `{
        '${
          this.form.relativeStorage.get('inputs/path') || '*'
        }': {${formatI18nProps(this.form.relativeStorage, 'i18n')}}
      }`;
    }
  }

  @computed
  public get state(): Array<Record<string, any>> | undefined {
    if (this.modal) {
      return [{ name: 'isVisible', state: this.modal.isVisible }];
    }
  }
}
