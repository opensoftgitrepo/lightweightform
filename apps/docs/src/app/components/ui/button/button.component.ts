import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  ViewChild,
} from '@angular/core';
import { FormComponent } from '@lightweightform/bootstrap-theme';
import { makeObservable } from 'mobx';
import { computed, observable } from 'mobx-angular';

import { BOOTSTRAP_DOCS_URL } from '../../../injection-tokens';
import {
  formatInputs,
  formatOutputs,
} from '../../../utils/format-inputs-outputs';

@Component({
  selector: 'docs-button',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './button.component.html',
})
export class ButtonComponent {
  @observable
  @ViewChild(FormComponent)
  public form: FormComponent;

  constructor(@Inject(BOOTSTRAP_DOCS_URL) public bootstrapDocsURL: string) {
    makeObservable(this);
  }

  @computed
  public get htmlCode(): string | undefined {
    if (this.form) {
      const tag = this.form.relativeStorage.get('inputs/element').slice(1, -1);
      return `
      <${tag}
        lf-button
        ${formatInputs(this.form.relativeStorage, 'inputs')}
        ${formatOutputs(this.form.relativeStorage, 'outputs')}
      ${
        tag !== 'input'
          ? `>${
              this.form.relativeStorage.get('inputs/dropdownToggleSplit')
                ? ''
                : this.form.relativeStorage.get('inputs/ngContent')
            }</${tag}>`
          : '>'
      }
    `;
    }
  }
}
