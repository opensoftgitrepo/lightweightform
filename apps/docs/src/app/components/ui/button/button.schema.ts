import {
  booleanSchema,
  RecordSchema,
  recordSchema,
  stringSchema,
} from '@lightweightform/storage';

export const buttonSchema: RecordSchema = recordSchema({
  inputs: recordSchema(
    {
      element: stringSchema({
        allowedValues: ['<button>', '<input>', '<a>'],
        initialValue: '<button>',
        docsOptions: {
          hideInputIf: () => true,
        },
      }),
      routerLink: stringSchema({
        docsOptions: { hideIf: (ctx) => ctx.get('../element') !== '<a>' },
      }),
      style: stringSchema({
        isNullable: true,
        allowedValues: [
          'primary',
          'secondary',
          'success',
          'danger',
          'warning',
          'info',
          'light',
          'dark',
          'link',
          'outline-primary',
          'outline-secondary',
          'outline-success',
          'outline-danger',
          'outline-warning',
          'outline-info',
          'outline-light',
          'outline-dark',
        ],
      }),
      size: stringSchema({
        isNullable: true,
        allowedValues: ['sm', 'lg'],
      }),
      type: stringSchema({
        allowedValues: ['button', 'submit', 'reset'],
        initialValue: 'button',
        docsOptions: { hideIf: (ctx) => ctx.get('../element') === '<a>' },
      }),
      block: booleanSchema(),
      dropdownToggle: booleanSchema({
        docsOptions: {
          hideIf: (ctx) =>
            ctx.get('../element') === '<input>' ||
            ctx.get('../dropdownToggleSplit'),
        },
      }),
      dropdownToggleSplit: booleanSchema({
        docsOptions: {
          hideIf: (ctx) =>
            ctx.get('../element') === '<input>' || ctx.get('../dropdownToggle'),
        },
      }),
      isDisabled: booleanSchema({
        docsOptions: { hideIf: (ctx) => ctx.get('../element') === '<a>' },
      }),
      isActive: booleanSchema(),
      ngContent: stringSchema({
        initialValue: 'Button',
        docsOptions: {
          hideIf: (ctx) =>
            ctx.get('../element') === '<input>' ||
            ctx.get('../dropdownToggleSplit'),
          hideInputIf: () => true,
        },
      }),
      value: stringSchema({
        docsOptions: { hideIf: (ctx) => ctx.get('../element') !== '<input>' },
      }),
    },
    {
      initialValue: { routerLink: '/', style: 'primary', value: 'Button' },
    }
  ),
  outputs: recordSchema({
    click: booleanSchema({ initialValue: true }),
  }),
});
