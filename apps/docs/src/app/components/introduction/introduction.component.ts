import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'docs-introduction',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './introduction.component.html',
})
export class IntroductionComponent {}
