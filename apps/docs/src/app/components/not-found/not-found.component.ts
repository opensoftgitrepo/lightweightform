import { Component } from '@angular/core';

@Component({
  selector: 'docs-not-found',
  templateUrl: './not-found.component.html',
})
export class NotFoundComponent {}
