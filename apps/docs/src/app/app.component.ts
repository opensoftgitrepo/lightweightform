import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ViewChild,
} from '@angular/core';
import {
  ActionsMenuAction,
  AppComponent as LfAppComponent,
} from '@lightweightform/bootstrap-theme';
import { LfRouter } from '@lightweightform/core';
import { autorun, makeObservable } from 'mobx';
import { computed } from 'mobx-angular';

import { RemoteRepository } from './services/remote-repository.service';

@Component({
  selector: 'docs-app',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './app.component.html',
})
export class AppComponent implements AfterViewInit {
  @computed
  public get actions(): ActionsMenuAction[] {
    const actions: ActionsMenuAction[] = [];

    // Show "validate" button when browsing the form elements
    if (this.lfRouter.isActive('/components/formElements')) {
      actions.push({
        id: 'validate',
        style: 'outline-danger',
        icon: 'check-square-o',
        callback: () => this.lfApp.validate(),
      });
    }

    actions.push(
      {
        icon: 'home',
        style: 'outline-primary',
        href: 'https://lightweightform.io',
      },
      {
        icon: 'bitbucket',
        style: 'outline-primary',
        href: this.remoteRepo.remoteUrl,
      }
    );
    return actions;
  }

  @ViewChild(LfAppComponent) private lfApp: LfAppComponent;

  constructor(
    private lfRouter: LfRouter,
    private remoteRepo: RemoteRepository
  ) {
    makeObservable(this);
  }

  public ngAfterViewInit() {
    // Hide the "validation panel" when not browsing the form elements
    autorun(() => {
      if (!this.lfRouter.isActive('/components/formElements')) {
        this.lfApp.validationPanel.isVisible = false;
      }
    });
  }
}
