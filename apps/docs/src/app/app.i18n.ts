import { I18N_EN_US } from '@lightweightform/bootstrap-theme';
import { I18nLanguages, LfI18n } from '@lightweightform/core';

import { appI18nEnUS } from './__i18n__/app.i18n.en-US';

export const appI18n: I18nLanguages = {
  'en-US': LfI18n.mergeTranslations(I18N_EN_US, appI18nEnUS),
};
