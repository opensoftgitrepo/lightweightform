import { Injectable } from '@angular/core';
import { marked } from 'marked';

import { HighlighterService } from './highlighter.service';

/**
 * Service used to render text as Markdown.
 */
@Injectable({ providedIn: 'root' })
export class MarkdownService {
  constructor(highlighter: HighlighterService) {
    marked.setOptions({
      highlight(code, language) {
        // Language defaults to TypeScript
        return highlighter.highlight(code, (language as any) || 'typescript');
      },
      smartLists: true,
    });
  }

  /**
   * Render a given Markdown string as HTML.
   * @param text Markdown to render as HTML.
   * @returns HTML representation of provided Markdown.
   */
  public render(text: string): string {
    return marked(text);
  }
}
