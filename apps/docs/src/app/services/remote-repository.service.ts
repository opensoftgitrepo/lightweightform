import { Injectable } from '@angular/core';

/**
 * Service used to query information on the remote repository.
 */
@Injectable({ providedIn: 'root' })
export class RemoteRepository {
  /**
   * URL of the remote repository.
   */
  public remoteUrl =
    'https://bitbucket.org/opensoftgitrepo/lightweightform/src/master';

  /**
   * URL to a specific file (possibly with line information).
   * @param file File to get URL to (relative to the packages directory).
   * @param line Line to highlight in file.
   * @returns URL to specified file.
   */
  public fileUrl(file: string, line?: number) {
    let fileUrl = this.remoteUrl + `/${file}`;
    if (line !== undefined) {
      fileUrl += `#lines-${line}`;
    }
    return fileUrl;
  }
}
