import { Injectable } from '@angular/core';
import { highlight, languages as prismLanguages } from 'prismjs';
// `json5` depends on `json`
import 'prismjs/components/prism-json';
import 'prismjs/components/prism-json5';
import 'prismjs/components/prism-scss';
import 'prismjs/components/prism-typescript';

/**
 * Service used to highlight code.
 */
@Injectable({ providedIn: 'root' })
export class HighlighterService {
  /**
   * Highlights the provided code.
   * @param code Code to highlight.
   * @param language Language of code to highlight.
   * @returns HTML representation of highlighted code.
   */
  public highlight(
    code: string,
    language: 'json' | 'html' | 'scss' | 'ts' | 'typescript'
  ): string {
    const prismLang =
      language === 'json'
        ? 'json5'
        : language === 'ts'
        ? 'typescript'
        : language;
    // Highlight `undefined` as well
    return highlight(code, prismLanguages[prismLang], prismLang).replace(
      /\bundefined\b/g,
      '<span class="token undefined">undefined</span>'
    );
  }
}
