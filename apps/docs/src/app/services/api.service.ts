import { Injectable } from '@angular/core';

import api from '../../docs-api.json';

import { RemoteRepository } from './remote-repository.service';

/**
 * Type of an entry of the JSON API.
 */
export type Entry = any;

/**
 * Service used to parse and query API information.
 */
@Injectable({ providedIn: 'root' })
export class ApiService {
  private idMap: Map<number, Entry> = new Map();
  private pkgPlusNameMap: Map<string, Entry> = new Map();

  constructor(private remoteRepo: RemoteRepository) {
    for (const child of api.children) {
      this.parseJsonApi(child);
    }
    this.arrangeTopLevelGroups(api);
  }

  /**
   * Name of the group to which all packages belong.
   */
  public get packagesGroupName(): string {
    // Root node's name has the name of the package's group
    return api.name;
  }

  /**
   * Mapping from packages to their groups of exported elements.
   */
  public get packagesGroups(): Record<string, any[]> {
    return api.groups as any;
  }

  /**
   * Get an entry given its id.
   * @param id Id of entry to get.
   * @returns Entry with specified id if found.
   */
  public getEntryById(id: number): Entry | undefined {
    return this.idMap.get(id);
  }

  /**
   * URL for a given entry.
   * @param entry Entry from which to get URL.
   * @returns URL to provided entry.
   */
  public entryUrl(entry: Entry): string {
    const entryNameParts = entry.fullName.split('.');
    return (
      `/api/${entry.package}/${entryNameParts[0]}` +
      (entryNameParts.length > 1 ? `#${entryNameParts.slice(1).join('.')}` : '')
    );
  }

  /**
   * Gets and entry given its package + full name.
   * @param pkgPlusName Package "slash" full name of the entry.
   * @returns Entry with provided name if found.
   */
  public getEntryByPackagePlusName(pkgPlusName: string): Entry | undefined {
    return this.pkgPlusNameMap.get(pkgPlusName);
  }

  /**
   * Search entries by name.
   * @param search Search query.
   * @returns List of entries matching the provided search string.
   */
  public searchByName(search: string): Entry[] {
    const matches: Entry[] = [];
    for (const [name, entry] of Array.from(this.pkgPlusNameMap.entries())) {
      const nameMinusPkg = name.slice(name.indexOf('/') + 1);
      if (nameMinusPkg.includes(search)) {
        matches.push(entry);
      }
    }
    return matches;
  }

  /**
   * Package to which a given entry belong based on the source file it was
   * defined in.
   * @param entry Entry for which to get the originating package.
   * @returns Package to which a given entry belongs.
   */
  public packageOf(entry: Entry): string {
    if (entry.parentEntry) {
      return entry.parentEntry.package;
    }
    const fileName = entry.sources[0].fileName;
    return fileName.slice(0, fileName.indexOf('/'));
  }

  /**
   * Remote repository URL for the given entry.
   * @param entry Entry for which to get the remote repository URL.
   * @returns Remote repository URL for given entry.
   */
  public entryRemoteRepositoryUrl(entry: Entry): string {
    const { fileName, line } = entry.sources[0];
    return this.remoteRepo.fileUrl(`libs/${fileName}`, line);
  }

  /**
   * Recursively builds indices from the JSON API file in order to access
   * entries by their id, name, etc. Further adds convenient properties to
   * entries such as their package, parent entries, etc.
   * @param entry Entry for which to build an index.
   */
  private parseJsonApi(entry: Entry): void {
    // Add all entries to ID map even if not public
    this.idMap.set(entry.id, entry);

    // Set additional properties
    this.setAdditionalFlags(entry);
    entry.package = this.packageOf(entry);
    entry.fullName = entry.parentEntry
      ? `${entry.parentEntry.fullName}.${entry.name}`
      : entry.name;

    // Do not index non-public APIs by name
    if (this.isPublicApi(entry)) {
      this.pkgPlusNameMap.set(`${entry.package}/${entry.fullName}`, entry);
    }

    // Parse children
    const possibleChildren = [
      'children',
      'signatures',
      'parameters',
      'typeParameter',
      'indexSignature',
    ];
    for (const children of possibleChildren) {
      if (entry[children]) {
        for (const child of Array.isArray(entry[children])
          ? entry[children]
          : [entry[children]]) {
          child.parentEntry = entry;
          this.parseJsonApi(child);
        }
      }
    }

    // Parse type
    if (entry.type) {
      this.parseJsonApiType(entry, entry.type);
    }

    // Arrange the groups
    this.arrangeGroups(entry);
  }

  /**
   * Extension to `parseJsonApi` to deal with "reflection" types.
   * @param entry Entry for which to build an index.
   * @param type Type of the entry or a child of its type.
   */
  private parseJsonApiType(entry: Entry, type: any): void {
    if (type.type === 'array') {
      this.parseJsonApiType(entry, type.elementType);
    } else if (type.type === 'union' || type.type === 'intersection') {
      for (const childType of type.types) {
        this.parseJsonApiType(entry, childType);
      }
    } else if (type.type === 'tuple') {
      for (const childType of type.elements) {
        this.parseJsonApiType(entry, childType);
      }
    } else if (type.type === 'reflection') {
      const child = type.declaration;
      child.parentEntry = entry;
      this.parseJsonApi(child);
    }
  }

  /**
   * Set additional flags on entries.
   * @param entry Entry on which to set flags.
   */
  private setAdditionalFlags(entry: Entry): void {
    if (entry.decorators) {
      if (entry.kindString === 'Class') {
        for (const dec of entry.decorators) {
          if (dec.name === 'NgModule') {
            entry.flags.isModule = true;
          } else if (dec.name === 'Injectable') {
            entry.flags.isService = true;
            entry.decoratorArgument = dec.arguments.options || '';
          } else if (dec.name === 'Component') {
            entry.flags.isComponent = true;
          } else if (dec.name === 'Directive') {
            entry.flags.isDirective = true;
          }
          if (dec.name === 'Component' || dec.name === 'Directive') {
            const selector =
              dec.arguments.obj &&
              dec.arguments.obj.match(/selector:\s*'(.+?)'/)[1];
            entry.decoratorArgument = `{selector: '${selector}'}`;
          }
        }
      } else if (
        entry.kindString === 'Property' ||
        entry.kindString === 'Accessor'
      ) {
        for (const dec of entry.decorators) {
          if (dec.name === 'Input') {
            entry.flags.isInput = true;
            const binding = dec.arguments.bindingPropertyName;
            entry.inputName =
              (binding && binding.slice(1, binding.length - 1)) || entry.name;
          } else if (dec.name === 'Output') {
            entry.flags.isOutput = true;
            const binding = dec.arguments.bindingPropertyName;
            entry.outputName =
              (binding && binding.slice(1, binding.length - 1)) || entry.name;
          }
        }
      }
    }
    if (entry.kindString === 'Variable') {
      if (
        entry.type.type === 'reference' &&
        entry.type.name === 'InjectionToken'
      ) {
        entry.flags.isInjectionToken = true;
      }
    }
  }

  /**
   * Arranges the groups of an entry.
   * @param entry Entry with groups to arrange.
   */
  private arrangeGroups(entry: Entry): void {
    let groups = entry.groups;
    if (!groups || groups.length === 0) {
      return;
    }

    // Remove non-public API entries
    for (const group of groups) {
      group.children = group.children.filter((id) =>
        this.isPublicApi(this.getEntryById(id))
      );
      entry.groups = groups = groups.filter((g) => g.children.length > 0);
    }

    // Rename "Enumeration members" to "Members" (the former is a mouthfull)
    const membersGroup = groups.find((g) => g.title === 'Enumeration members');
    membersGroup && (membersGroup.title = 'Members');

    // Don't differentiate between properties and accessors
    this.mergeGroups(groups, 'Properties', 'Accessors');

    // Split properties into inputs and outputs
    this.splitGroup(
      groups,
      'Properties',
      ['Inputs', 'Outputs', 'Properties'],
      ({ flags }) =>
        flags.isInput ? 'Inputs' : flags.isOutput ? 'Outputs' : 'Properties'
    );
    // Sort inputs/outputs by their input/output names
    const inputsGroup = groups.find((g) => g.title === 'Inputs');
    const outputsGroup = groups.find((g) => g.title === 'Outputs');
    inputsGroup &&
      inputsGroup.children.sort((id1, id2) =>
        this.getEntryById(id1).inputName.localeCompare(
          this.getEntryById(id2).inputName
        )
      );
    outputsGroup &&
      outputsGroup.children.sort((id1, id2) =>
        this.getEntryById(id1).outputName.localeCompare(
          this.getEntryById(id2).outputName
        )
      );

    // Don't differentiate between properties and index signatures
    if (entry.indexSignature) {
      const sigId = entry.indexSignature.id;
      const propertiesIdx = groups.findIndex((g) => g.title === 'Properties');
      if (propertiesIdx === -1) {
        const propsGroup = { title: 'Properties', children: [sigId] };
        const methodsIdx = groups.findIndex((g) => g.title === 'Methods');
        if (methodsIdx === -1) {
          groups.push(propsGroup);
        } else {
          groups.splice(methodsIdx, 0, propsGroup);
        }
      } else {
        groups[propertiesIdx].children.push(sigId);
      }
    }
  }

  /**
   * Arranges the top-level groups.
   * @param entry Top-level entry.
   */
  private arrangeTopLevelGroups(entry: Entry) {
    const groups = entry.groups;
    if (!groups || groups.length === 0) {
      return;
    }

    // Rename "Enumerations" to "Enums" (the former is a mouthfull)
    const membersGroup = groups.find((g) => g.title === 'Enumerations');
    membersGroup && (membersGroup.title = 'Enums');

    // Split classes in components, directives, services, or normal classes
    this.splitGroup(
      groups,
      'Classes',
      ['Modules', 'Components', 'Directives', 'Services', 'Classes'],
      ({ flags }) =>
        flags.isModule
          ? 'Modules'
          : flags.isComponent
          ? 'Components'
          : flags.isDirective
          ? 'Directives'
          : flags.isService
          ? 'Services'
          : 'Classes'
    );

    // Split variables from injection tokens
    this.splitGroup(
      groups,
      'Variables',
      ['Injection tokens', 'Variables'],
      ({ flags }) => (flags.isInjectionToken ? 'Injection tokens' : 'Variables')
    );

    // Merge variables and object literals
    this.mergeGroups(groups, 'Variables', 'Object literals');

    // Split exports per package
    const packages = {};
    for (const group of groups) {
      for (const id of group.children) {
        const pkg = this.packageOf(this.getEntryById(id));
        const packageGroups = packages[pkg] || (packages[pkg] = []);
        let packageGroup = packageGroups[packageGroups.length - 1];
        if (!packageGroup || packageGroup.title !== group.title) {
          packageGroups.push(
            (packageGroup = { title: group.title, children: [] })
          );
        }
        packageGroup.children.push(id);
      }
    }
    entry.groups = packages;
  }

  /**
   * Merges two groups into one.
   * @param groups List of groups.
   * @param intoGroup Title of the group the entries should belong to.
   * @param fromGroup Title of the group the entries should be moved out of.
   */
  private mergeGroups(groups: any[], intoGroup: string, fromGroup: string) {
    const fromIdx = groups.findIndex((g) => g.title === fromGroup);
    if (fromIdx !== -1) {
      const intoIdx = groups.findIndex((g) => g.title === intoGroup);
      if (intoIdx === -1) {
        groups[fromIdx].title = intoGroup;
      } else {
        groups[intoIdx].children.push(...groups[fromIdx].children);
        groups[intoIdx].children.sort((id1, id2) =>
          this.getEntryById(id1).name.localeCompare(this.getEntryById(id2).name)
        );
        groups.splice(fromIdx, 1);
      }
    }
  }

  /**
   * Splits a group into multiple groups given a function that maps an entry to
   * the group it should belong to.
   * @param groups List of groups.
   * @param groupTitle Title of group to split.
   * @param groupOf Function mapping an entry to the title of the group it
   * should belong to.
   */
  private splitGroup(
    groups: any[],
    groupTitle: string,
    splitGroupTitles: string[],
    groupOf: (entry: Entry) => string
  ) {
    const groupIdx = groups.findIndex((g) => g.title === groupTitle);
    if (groupIdx !== -1) {
      const group = groups[groupIdx];
      const splits = group.children.reduce((obj, id) => {
        const entryGroup = groupOf(this.getEntryById(id));
        obj[entryGroup] || (obj[entryGroup] = []);
        obj[entryGroup].push(id);
        return obj;
      }, {});
      splitGroupTitles.forEach((t, i) => (splitGroupTitles[t] = i));
      const newGroups = Object.keys(splits)
        .sort((t1, t2) => splitGroupTitles[t1] - splitGroupTitles[t2])
        .map((title) => ({ title, children: splits[title] }));
      groups.splice(groupIdx, 1, ...newGroups);
    }
  }

  /**
   * Whether a given entry is part of the public API.
   * @param entry Entry to check if public.
   * @returns Whether this entry is public.
   */
  private isPublicApi(entry: Entry): boolean {
    return (
      (!entry.inputName && !entry.outputName && !entry.name.startsWith('_')) ||
      (entry.inputName && !entry.inputName.startsWith('_')) ||
      (entry.outputName && !entry.outputName.startsWith('_'))
    );
  }
}
