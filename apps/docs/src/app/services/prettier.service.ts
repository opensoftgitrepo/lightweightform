import { Injectable } from '@angular/core';
import * as parserBabel from 'prettier/parser-babel';
import * as parserHTML from 'prettier/parser-html';
import * as parserPostCSS from 'prettier/parser-postcss';
import * as parserTypeScript from 'prettier/parser-typescript';
import { format } from 'prettier/standalone';

const prettierPlugins = [
  parserBabel,
  parserHTML,
  parserPostCSS,
  parserTypeScript,
];

/**
 * Service used to highlight code.
 */
@Injectable({ providedIn: 'root' })
export class PrettierService {
  /**
   * Prettifies the provided code.
   * @param code Code to prettify.
   * @param language Language of code to prettify.
   * @param prettierOptions Prettier options.
   * @returns Prettified code.
   */
  public prettify(
    code: string,
    language: 'json' | 'html' | 'scss' | 'ts' | 'typescript',
    prettierOptions: Record<string, any> = {}
  ): string {
    const parser =
      language === 'html'
        ? 'angular'
        : language === 'ts'
        ? 'typescript'
        : language;
    return format(code, {
      parser,
      plugins: prettierPlugins,
      printWidth: 80,
      singleQuote: true,
      ...prettierOptions,
    });
  }
}
