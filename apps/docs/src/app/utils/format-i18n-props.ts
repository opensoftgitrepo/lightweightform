import { LfStorage } from '@lightweightform/core';
import { initialValue } from '@lightweightform/storage';
import { comparer } from 'mobx';

/**
 * Formats `i18n` to be shown in a block of TS code, they are shown unless their
 * value is the empty string or unless specified via `hideIf` or `hideI18nIf`
 * within the documentation options.
 * @param ctx LF storage context.
 * @param i18nPath Path for the i18n, relative to the storage instance.
 * @returns String with the i18n being declared.
 */
export function formatI18nProps(ctx: LfStorage, i18nPath) {
  return Object.keys(ctx.schema(i18nPath).fieldsSchemas)
    .map((field) => formatI18nProp(ctx.relativeStorage(i18nPath), field))
    .filter((entry) => entry !== '')
    .join(',');
}
function formatI18nProp(ctx: LfStorage, inputPath: string): string {
  const schema = ctx.schema(inputPath);
  const options = schema.docsOptions;
  const relCtx = ctx.relativeStorage(inputPath);
  const hide = options && options.hideIf && options.hideIf(relCtx);
  const hideI18n =
    hide || (options && options.hideI18nIf && options.hideI18nIf(relCtx));
  const attr = ctx.resolvePathToArray(inputPath).slice(-1)[0];
  const val = ctx.getAsJS(inputPath);
  return hideI18n ||
    (!schema.computedValue &&
      // Don't show prop when it's the same as its default value
      comparer.structural(val, initialValue(ctx, schema, ctx.currentPath)))
    ? ''
    : `${attr}: ${JSON.stringify(cleanUpVal(val))}`;
}

/**
 * Deeply remove empty strings from the value.
 * @param val Value from which to remove empty strings.
 * @returns Value without empty strings.
 */
function cleanUpVal(val: any): any {
  if (Array.isArray(val)) {
    val.forEach((el) => cleanUpVal(el));
  } else if (typeof val === 'object' && val !== null) {
    Object.keys(val).forEach((k) => {
      if (val[k] === '') {
        delete val[k];
      } else {
        cleanUpVal(val[k]);
      }
    });
  }
  return val;
}
