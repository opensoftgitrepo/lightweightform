import { LfStorage } from '@lightweightform/core';
import { initialValue } from '@lightweightform/storage';
import { comparer } from 'mobx';

/**
 * Formats `input`s to be shown in an HTML string, they are shown unless their
 * value is the same as their "initial value" or unless specified via `hideIf`
 * or `hideInputIf` within the documentation options.
 * @param ctx LF storage context.
 * @param inputsPath Path for the inputs, relative to the storage instance.
 * @returns String with the inputs being declared.
 */
export function formatInputs(ctx: LfStorage, inputsPath: string) {
  return Object.keys(ctx.schema(inputsPath).fieldsSchemas)
    .map((field) => {
      return formatInput(ctx.relativeStorage(inputsPath), field);
    })
    .join(' ');
}
function formatInput(ctx: LfStorage, inputPath: string): string {
  const schema = ctx.schema(inputPath);
  const options = schema.docsOptions;
  const relCtx = ctx.relativeStorage(inputPath);
  const hide = options && options.hideIf && options.hideIf(relCtx);
  const hideInput =
    hide || (options && options.hideInputIf && options.hideInputIf(relCtx));
  const attr = ctx.resolvePathToArray(inputPath).slice(-1)[0];
  const val = ctx.get(inputPath);
  return hideInput ||
    (!schema.computedValue &&
      // Don't show input when it's the same as its default value
      comparer.structural(val, initialValue(ctx, schema, ctx.currentPath)))
    ? ''
    : typeof val === 'string'
    ? attr === 'style'
      ? `[style]="'${val}'"`
      : `${attr}="${val}"`
    : `[${attr}]="${val}"`;
}

/**
 * Formats `output`s to be shown in an HTML string, they are shown when their
 * associated boolean value is `true`.
 * @param ctx LF storage context.
 * @param outputsPath Path of the outputs, relative to the storage instance.
 * @returns String with the outputs being declared.
 */
export function formatOutputs(ctx: LfStorage, outputsPath: string) {
  return Object.keys(ctx.schema(outputsPath).fieldsSchemas)
    .map((field) => {
      return formatOutput(ctx.relativeStorage(outputsPath), field);
    })
    .join(' ');
}
function formatOutput(ctx: LfStorage, outputPath: string): string {
  const attr = `${ctx.resolvePathToArray(outputPath).slice(-1)[0]}`;
  const attrMethod = 'on' + attr[0].toUpperCase() + attr.slice(1);
  const val = ctx.get(outputPath);
  return !val ? '' : `(${attr})="${attrMethod}($event)"`;
}
