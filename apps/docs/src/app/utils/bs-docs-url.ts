import packageJson from 'bootstrap/package.json';

/**
 * Base URL for the Bootstrap docs (with the same version as the one used by
 * the LF theme).
 */
export const BOOTSTRAP_DOCS_BASE_URL =
  'https://getbootstrap.com/docs/' + packageJson.version.match(/^\d+\.\d+/)![0];
