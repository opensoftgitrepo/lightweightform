import { LfStorage } from '@lightweightform/core';
import {
  appendToPath,
  initialValue,
  isBooleanSchema,
  isDateSchema,
} from '@lightweightform/storage';
import { comparer } from 'mobx';

/**
 * Formats schema options to be shown in a schema example, they are shown unless
 * their value is the same as their "initial value" or unless specified via
 * `hideIf` or `hideOptionIf` within the documentation options.
 * @param ctx LF storage context.
 * @param optionsPath Path for the schema options, relative to the storage
 * instance.
 * @returns String with the schema options being declared.
 */
export function formatSchemaOptions(ctx: LfStorage, optionsPath: string) {
  const options = Object.keys(ctx.schema(optionsPath).fieldsSchemas)
    .map((field) => {
      return formatSchemaOption(ctx.relativeStorage(optionsPath), field);
    })
    .filter((option) => option);
  if (ctx.hasPath(appendToPath(optionsPath, 'customError'))) {
    const customError = ctx.get(appendToPath(optionsPath, 'customError'));
    const customWarning = ctx.get(appendToPath(optionsPath, 'customWarning'));
    if (customError || customWarning) {
      options.push(`
      validate: (ctx: LfStorage) => {
        ${customError && !customWarning ? `return {code: 'CUSTOM_ERROR'};` : ''}
        ${
          !customError && customWarning
            ? `return {code: 'CUSTOM_WARNING', isWarning: true};`
            : ''
        }
        ${
          customError && customWarning
            ? `return [
                 {code: 'CUSTOM_ERROR'},
                 {code: 'CUSTOM_WARNING', isWarning: true},
               ];`
            : ''
        }

      }
    `);
    }
  }
  return options.length > 0 ? `{${options.join(',')}}` : '';
}
function formatSchemaOption(ctx: LfStorage, inputPath: string): string {
  const schema = ctx.schema(inputPath);
  const options = schema.docsOptions;
  const relCtx = ctx.relativeStorage(inputPath);
  const hide = options && options.hideIf && options.hideIf(relCtx);
  const hideOption =
    hide || (options && options.hideOptionIf && options.hideOptionIf(relCtx));
  const option = ctx.resolvePathToArray(inputPath).slice(-1)[0];
  const val = ctx.get(inputPath);
  const tsVal =
    val && isDateSchema(schema)
      ? `new Date('${val.toISOString().slice(0, 10)}')`
      : option === 'allowedValues' && isBooleanSchema(schema)
      ? `[${JSON.stringify(val)}]` // Allowed values on checkboxes
      : JSON.stringify(val);
  return hideOption ||
    (!schema.computedValue &&
      // Don't show option when it's the same as its default value
      comparer.structural(val, initialValue(ctx, schema, ctx.currentPath)))
    ? ''
    : `${option}: ${tsVal}`;
}
