import { LfStorage } from '@lightweightform/core';
import { ValidationIssues } from '@lightweightform/storage';

/**
 * Custom validation function for docs values.
 * @param ctx Context of the value.
 * @returns Custom validation issues.
 */
export function customValidate(ctx: LfStorage): ValidationIssues {
  const issues: any[] = [];
  if (ctx.get('../schemaOptions/customError')) {
    issues.push({ code: 'CUSTOM_ERROR' });
  }
  if (ctx.get('../schemaOptions/customWarning')) {
    issues.push({ code: 'CUSTOM_WARNING', isWarning: true });
  }
  return issues;
}
