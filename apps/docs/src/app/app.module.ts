import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { LfBootstrapThemeModule } from '@lightweightform/bootstrap-theme';
import {
  LfFileHolder,
  LfI18n,
  LfStorage,
  LF_APP_I18N,
  LF_APP_SCHEMA,
} from '@lightweightform/core';
import { TabsModule } from 'ngx-bootstrap/tabs';
// XXX: This import is required for the production build to work and it
// (somehow) won't work if added within the highlighter service
import 'prismjs';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { appI18n } from './app.i18n';
import { appSchema } from './app.schema';
import { ApiComponent } from './components/api/api.component';
import { DataValidationComponent } from './components/core-concepts/data-validation/data-validation.component';
import { I18nComponent } from './components/core-concepts/i18n/i18n.component';
import { RoutingComponent } from './components/core-concepts/routing/routing.component';
import { StorageSchemaComponent } from './components/core-concepts/storage-schema/storage-schema.component';
import { ThemeUsageComponent } from './components/core-concepts/theme-usage/theme-usage.component';
import { CheckboxGroupComponent } from './components/form-elements/checkbox-group/checkbox-group.component';
import { CheckboxComponent } from './components/form-elements/checkbox/checkbox.component';
import { DateRangeComponent } from './components/form-elements/date-range/date-range.component';
import { DateComponent } from './components/form-elements/date/date.component';
import { FileComponent } from './components/form-elements/file/file.component';
import { NumberComponent } from './components/form-elements/number/number.component';
import { RadioComponent } from './components/form-elements/radio/radio.component';
import { SelectComponent } from './components/form-elements/select/select.component';
import { TableComponent } from './components/form-elements/table/table.component';
import { TextComponent } from './components/form-elements/text/text.component';
import { TextareaComponent } from './components/form-elements/textarea/textarea.component';
import { ApiEntryComponent } from './components/helpers/api-entry/api-entry.component';
import { ApiRefComponent } from './components/helpers/api-ref/api-ref.component';
import { ApiSignatureComponent } from './components/helpers/api-signature/api-signature.component';
import { ApiTypeComponent } from './components/helpers/api-type/api-type.component';
import { CodeComponent } from './components/helpers/code/code.component';
import { HeadingComponent } from './components/helpers/heading/heading.component';
import { InputsComponent } from './components/helpers/inputs/inputs.component';
import { OutputsComponent } from './components/helpers/outputs/outputs.component';
import { PlaygroundComponent } from './components/helpers/playground/playground.component';
import { IntroductionComponent } from './components/introduction/introduction.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { SchematicsComponent } from './components/schematics/schematics.component';
import { Part1Component } from './components/tutorial/part1/part1.component';
import { Part2Component } from './components/tutorial/part2/part2.component';
import { Part3Component } from './components/tutorial/part3/part3.component';
import { Part4Component } from './components/tutorial/part4/part4.component';
import { TutorialComponent } from './components/tutorial/tutorial.component';
import { ButtonComponent } from './components/ui/button/button.component';
import { CardComponent } from './components/ui/card/card.component';
import { ModalComponent } from './components/ui/modal/modal.component';
import { BOOTSTRAP_DOCS_URL } from './injection-tokens';
import { BOOTSTRAP_DOCS_BASE_URL } from './utils/bs-docs-url';

@NgModule({
  imports: [
    BrowserModule,
    LfBootstrapThemeModule,
    AppRoutingModule,
    TabsModule.forRoot(),
  ],
  declarations: [
    // Application
    AppComponent,
    // Not found
    NotFoundComponent,
    // Helpers
    ApiEntryComponent,
    ApiRefComponent,
    ApiSignatureComponent,
    ApiTypeComponent,
    CodeComponent,
    HeadingComponent,
    InputsComponent,
    OutputsComponent,
    PlaygroundComponent,
    // Introduction
    IntroductionComponent,
    // Tutorial
    TutorialComponent,
    Part1Component,
    Part2Component,
    Part3Component,
    Part4Component,
    // Core concepts
    StorageSchemaComponent,
    DataValidationComponent,
    ThemeUsageComponent,
    I18nComponent,
    RoutingComponent,
    // Schematics
    SchematicsComponent,
    // Form element components
    CheckboxComponent,
    CheckboxGroupComponent,
    DateComponent,
    DateRangeComponent,
    FileComponent,
    NumberComponent,
    RadioComponent,
    SelectComponent,
    TableComponent,
    TextareaComponent,
    TextComponent,
    // UI components
    ButtonComponent,
    CardComponent,
    ModalComponent,
    // API
    ApiComponent,
  ],
  providers: [
    LfStorage,
    LfI18n,
    LfFileHolder,
    { provide: LF_APP_SCHEMA, useValue: appSchema },
    { provide: LF_APP_I18N, useValue: appI18n },
    { provide: BOOTSTRAP_DOCS_URL, useValue: BOOTSTRAP_DOCS_BASE_URL },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
