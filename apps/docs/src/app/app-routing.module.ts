import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LfRouter, LfRoutes, LF_ROUTER_BASE_PATH } from '@lightweightform/core';

import { ApiComponent } from './components/api/api.component';
import { DataValidationComponent } from './components/core-concepts/data-validation/data-validation.component';
import { StorageSchemaComponent } from './components/core-concepts/storage-schema/storage-schema.component';
import { CheckboxGroupComponent } from './components/form-elements/checkbox-group/checkbox-group.component';
import { CheckboxComponent } from './components/form-elements/checkbox/checkbox.component';
import { DateRangeComponent } from './components/form-elements/date-range/date-range.component';
import { DateComponent } from './components/form-elements/date/date.component';
import { FileComponent } from './components/form-elements/file/file.component';
import { NumberComponent } from './components/form-elements/number/number.component';
import { RadioComponent } from './components/form-elements/radio/radio.component';
import { SelectComponent } from './components/form-elements/select/select.component';
import { TableComponent } from './components/form-elements/table/table.component';
import { TextComponent } from './components/form-elements/text/text.component';
import { TextareaComponent } from './components/form-elements/textarea/textarea.component';
import { IntroductionComponent } from './components/introduction/introduction.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { SchematicsComponent } from './components/schematics/schematics.component';
import { Part1Component } from './components/tutorial/part1/part1.component';
import { Part2Component } from './components/tutorial/part2/part2.component';
import { Part3Component } from './components/tutorial/part3/part3.component';
import { Part4Component } from './components/tutorial/part4/part4.component';
import { TutorialComponent } from './components/tutorial/tutorial.component';
import { ButtonComponent } from './components/ui/button/button.component';
import { CardComponent } from './components/ui/card/card.component';
import { ModalComponent } from './components/ui/modal/modal.component';

// import {AppSchematicComponent} from './components/cli/app-schematic/app-schematic.component';
// import {CliComponent} from './components/cli/cli.component';
// import {ComponentSchematicComponent} from './components/cli/component-schematic/component-schematic.component';
// import {LanguageSchematicComponent} from './components/cli/language-schematic/language-schematic.component';
// import {I18nComponent} from './components/core-concepts/i18n/i18n.component';
// import {RoutingComponent} from './components/core-concepts/routing/routing.component';
// import {ThemeUsageComponent} from './components/core-concepts/theme-usage/theme-usage.component';

/**
 * Application routes.
 */
const routes: LfRoutes = [
  { path: '', redirectTo: '/introduction', pathMatch: 'full' },
  { path: 'introduction', component: IntroductionComponent },
  { path: 'tutorial', component: TutorialComponent },
  { path: 'tutorial/part1', component: Part1Component },
  { path: 'tutorial/part2', component: Part2Component },
  { path: 'tutorial/part3', component: Part3Component },
  { path: 'tutorial/part4', component: Part4Component },
  { path: 'coreConcepts/storageSchema', component: StorageSchemaComponent },
  { path: 'coreConcepts/dataValidation', component: DataValidationComponent },
  // {path: 'coreConcepts/themeUsage', component: ThemeUsageComponent},
  // {path: 'coreConcepts/i18n', component: I18nComponent},
  // {path: 'coreConcepts/routing', component: RoutingComponent},
  { path: 'schematics', component: SchematicsComponent },
  {
    path: 'components/formElements/checkboxGroup',
    component: CheckboxGroupComponent,
  },
  { path: 'components/formElements/checkbox', component: CheckboxComponent },
  { path: 'components/formElements/date', component: DateComponent },
  { path: 'components/formElements/dateRange', component: DateRangeComponent },
  { path: 'components/formElements/file', component: FileComponent },
  { path: 'components/formElements/number', component: NumberComponent },
  { path: 'components/formElements/radio', component: RadioComponent },
  { path: 'components/formElements/select', component: SelectComponent },
  { path: 'components/formElements/table', component: TableComponent },
  { path: 'components/formElements/text', component: TextComponent },
  { path: 'components/formElements/textarea', component: TextareaComponent },
  { path: 'components/ui/button', component: ButtonComponent },
  { path: 'components/ui/card', component: CardComponent },
  { path: 'components/ui/modal', component: ModalComponent },
  { path: 'api', component: ApiComponent },
  { path: 'api/:package', component: ApiComponent, lfPath: '/api' },
  { path: 'api/:package/:entryName', component: ApiComponent, lfPath: '/api' },
  { path: '**', component: NotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [LfRouter, { provide: LF_ROUTER_BASE_PATH, useValue: '/' }],
})
export class AppRoutingModule {}
