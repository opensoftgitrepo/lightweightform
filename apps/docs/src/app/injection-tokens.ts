import { InjectionToken } from '@angular/core';

/**
 * Injection token used to specify the prefix for the Bootstrap docs URL.
 */
export const BOOTSTRAP_DOCS_URL = new InjectionToken<string>(
  'BOOTSTRAP_DOCS_URL'
);
