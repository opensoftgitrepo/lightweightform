import { I18nPaths, LfStorage } from '@lightweightform/core';
import { isListSchema, isRecordSchema } from '@lightweightform/storage';

// Helpers variables for the generic i18n functions
const BASE_COMPONENTS_PATH = '/components/componentType/componentName';
const baseComponentsPathLen = BASE_COMPONENTS_PATH.split('/').length;
function isCard(id: string): boolean {
  // `id` is either one of the titles or its name end with it
  const cardTitles = ['inputs', 'outputs', 'schemaOptions', 'i18n'];
  return cardTitles.some(
    (t) => id === t || id.endsWith(t[0].toUpperCase() + t.slice(1))
  );
}

function uppercase(str: string): string {
  return str[0].toUpperCase() + str.slice(1);
}

function uncamelize(str: string, separator: string = ' '): string {
  return str.replace(/[A-Z]/g, `${separator}$&`).toLowerCase();
}

function fromI18nRecord(
  key: string,
  dft?: string
): (ctx: LfStorage) => string | undefined {
  return (ctx: LfStorage) => {
    const pathInfo = ctx.pathInfo();
    if (pathInfo.length === baseComponentsPathLen + 1) {
      const { id } = pathInfo[baseComponentsPathLen];
      if (!isCard(id!.toString())) {
        const { path } = pathInfo[baseComponentsPathLen - 1];
        const i18nPath = `${path}/i18n/${key}`;
        if (ctx.hasPath(i18nPath)) {
          return ctx.get(i18nPath);
        }
      }
    }
    return dft;
  };
}

export const appI18nEnUS: I18nPaths = {
  '*': {
    appTitleLg: 'Lightweightform documentation',
    appTitleSm: 'LF docs',
    actionsMenu: {
      validate: 'Validate',
    },
    label: (ctx: LfStorage) => {
      const pathInfo = ctx.pathInfo();
      // Page labels
      if (pathInfo.length <= baseComponentsPathLen) {
        const { id } = pathInfo[pathInfo.length - 1];
        return uppercase(uncamelize(id!.toString()));
      }
      if (pathInfo.length > baseComponentsPathLen) {
        const { id } = pathInfo[baseComponentsPathLen];
        // Input cards
        if (isCard(id!.toString())) {
          // Card title
          if (pathInfo.length === baseComponentsPathLen + 1) {
            return uppercase(uncamelize(id!.toString()));
          }
          // Card input
          else {
            const fieldId = pathInfo[pathInfo.length - 1].id!.toString();
            return fieldId === 'ngContent' ? '<ng-content>' : fieldId;
          }
        }
        // Non-cards
        else {
          const { path } = pathInfo[baseComponentsPathLen - 1];
          const i18nPath = `${path}/i18n/label`;
          if (ctx.hasPath(i18nPath)) {
            return ctx.get(i18nPath);
          }
        }
      }
    },
    options: (ctx: LfStorage) => {
      const pathInfo = ctx.pathInfo();
      if (pathInfo.length > baseComponentsPathLen) {
        const options: any[] = [];
        const { id } = pathInfo[baseComponentsPathLen];
        const schema = pathInfo[pathInfo.length - 1].schema;
        // Input cards
        if (isCard(id!.toString())) {
          if (schema.isNullable) {
            options.push({ value: null });
          }
          return options.concat(
            schema.allowedValues &&
              (schema.allowedValues as any[]).map((value) => ({
                value,
                label: value.toString(),
              }))
          );
        }
        // Non-cards
        else {
          const { path } = pathInfo[baseComponentsPathLen - 1];
          const i18nPath = `${path}/i18n/options`;
          if ((schema.docsOptions || {}).displayNullOption) {
            options.push({ value: null });
          }
          if (ctx.hasPath(i18nPath)) {
            return options.concat(Array.from(ctx.get(i18nPath)));
          }
        }
      }
    },
    columnLabels: (ctx: LfStorage) => {
      const pathInfo = ctx.pathInfo();
      if (pathInfo.length > baseComponentsPathLen) {
        const { id } = pathInfo[baseComponentsPathLen];
        // Input cards
        if (isCard(id!.toString())) {
          const schema = pathInfo[pathInfo.length - 1].schema;
          const childrenSchema = isListSchema(schema)
            ? schema.elementsSchema
            : schema.rowsSchema;
          // Tables of records (obtain column names from records fields)
          if (isRecordSchema(childrenSchema)) {
            const columns: string[] = Object.keys(childrenSchema.fieldsSchemas);
            return columns.reduce((obj, column) => {
              obj[column] = column === 'ngContent' ? '<ng-content>' : column;
              return obj;
            }, {});
          }
          // Tables of values (single column "value")
          else {
            return { value: 'value' };
          }
        }
      }
    },
    code: fromI18nRecord('code'),
    helpMessage: fromI18nRecord('helpMessage'),
    legend: fromI18nRecord('legend'),
    prefix: fromI18nRecord('prefix'),
    suffix: fromI18nRecord('suffix'),
    decimalSeparator: fromI18nRecord('decimalSeparator', '.'),
    thousandsSeparator: fromI18nRecord('thousandsSeparator'),
    closeButtonTooltip: fromI18nRecord('closeButtonTooltip', 'Close'),
    format: fromI18nRecord('format', 'YYYY-MM-DD'),
    placeholder: fromI18nRecord('placeholder', 'YYYY-MM-DD'),
    separator: fromI18nRecord('separator', ' - '),
    addRowActionText: fromI18nRecord('addRowActionText', 'Add row'),
    removeRowsActionText: fromI18nRecord('removeRowsActionText', 'Remove rows'),
    noRowsText: fromI18nRecord('noRowsText', 'No rows to display.'),
  },
  '/components/ui': {
    label: 'UI',
  },
  '/components/formElements/table/table': {
    columnLabels: {
      fullName: 'Full name',
      firstName: 'First name',
      lastName: 'Last name',
      age: 'Age',
      married: 'Married?',
      profession: 'Profession',
    },
  },
  '/components/formElements/table/table/?/profession': {
    options: [
      { value: 'dancer', label: 'Dancer' },
      { value: 'singer', label: 'Singer' },
    ],
  },
  '/tutorial': {
    label: 'Tutorial (hotel reservation)',
  },
  '/tutorial/part1': {
    label: '1. Setup',
  },
  '/tutorial/part2': {
    label: '2. Form fields',
  },
  '/tutorial/part3': {
    label: '3. Validations',
  },
  '/tutorial/part4': {
    label: '4. Computed fields',
  },
  '/coreConcepts/storageSchema': {
    label: 'Storage and schema',
  },
  '/coreConcepts/themeUsage': {
    label: 'Using themes',
  },
  '/cli': {
    label: 'CLI',
  },
  '/coreConcepts/i18n': {
    label: 'Internationalisation',
  },
  '/api': {
    label: 'API',
  },
};
