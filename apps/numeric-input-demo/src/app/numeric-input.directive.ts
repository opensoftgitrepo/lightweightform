import {
  ChangeDetectorRef,
  Directive,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  Output,
  SimpleChanges,
} from '@angular/core';
import {
  NumericInput,
  NumericInputOptions,
  NumericRepresentation,
} from '@lightweightform/numeric-input';

export interface ValueChange {
  stringValue: string;
  numberValue: number | null;
}

@Directive({
  selector: 'input[demoNumericInput]',
  exportAs: 'demoNumericInput',
})
export class NumericInputDirective implements OnChanges, OnDestroy {
  @Input() public numericRepresentation?: NumericRepresentation;
  @Input() public scale?: number;
  @Input() public leadingZeroes?: number;
  @Input() public min?: number;
  @Input() public max?: number;
  @Input() public prefix?: string;
  @Input() public suffix?: string;
  @Input() public decimalSeparator?: string;
  @Input() public thousandsSeparator?: string;

  @Output() public valueChange: EventEmitter<ValueChange> =
    new EventEmitter<ValueChange>();

  public optionsError: Error | null = null;

  private numericInput: NumericInput;

  constructor(private elementRef: ElementRef, private cd: ChangeDetectorRef) {
    this.numericInput = new NumericInput(this.elementRef.nativeElement, {
      onUpdate: (stringValue, numberValue) => {
        this.valueChange.emit({ stringValue, numberValue });
        this.cd.detectChanges();
      },
    });
  }

  public get options(): NumericInputOptions {
    return {
      numericRepresentation: this.numericRepresentation || 'float',
      scale: this.scale || 0,
      leadingZeroes: this.leadingZeroes == null ? 1 : this.leadingZeroes,
      min: this.min == null ? -Infinity : this.min,
      max: this.max == null ? Infinity : this.max,
      prefix: this.prefix || '',
      suffix: this.suffix || '',
      decimalSeparator: this.decimalSeparator || '.',
      thousandsSeparator: this.thousandsSeparator || '',
    };
  }

  public get optionsInEffect(): NumericInputOptions {
    return {
      numericRepresentation: this.numericInput.numericRepresentation,
      scale: this.numericInput.scale,
      leadingZeroes: this.numericInput.leadingZeroes,
      min: this.numericInput.min,
      max: this.numericInput.max,
      prefix: this.numericInput.prefix,
      suffix: this.numericInput.suffix,
      decimalSeparator: this.numericInput.decimalSeparator,
      thousandsSeparator: this.numericInput.thousandsSeparator,
    };
  }

  public ngOnChanges(changes: SimpleChanges) {
    if (
      changes.numericRepresentation ||
      changes.scale ||
      changes.leadingZeroes ||
      changes.min ||
      changes.max ||
      changes.prefix ||
      changes.suffix ||
      changes.decimalSeparator ||
      changes.thousandsSeparator
    ) {
      try {
        this.numericInput.setOptions(this.options);
        this.optionsError = null;
      } catch (err) {
        this.optionsError = err;
      }
      this.cd.detectChanges();
    }
  }

  public ngOnDestroy() {
    this.numericInput.destroy();
  }

  public update(value: string | number | null): void {
    this.numericInput.update(value);
  }

  public toString(): string {
    return this.numericInput.toString();
  }

  public toNumber(): number | null {
    return this.numericInput.toNumber();
  }

  public isValid(): boolean {
    return this.numericInput.isValid();
  }

  public validOptions(options: NumericInputOptions): boolean {
    try {
      NumericInput.validateOptions(options);
      return true;
    } catch (err) {
      return false;
    }
  }
}
