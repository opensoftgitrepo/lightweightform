import { Component, OnInit } from '@angular/core';
import { NumericInputOptions } from '@lightweightform/numeric-input';

import { ValueChange } from './numeric-input.directive';

@Component({
  selector: 'demo-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  // Initial numeric input value
  public value = '';
  // Default options
  public options: NumericInputOptions = {
    numericRepresentation: 'float',
    scale: 2,
    prefix: '$',
    thousandsSeparator: ',',
  };
  // Value used for the manual update
  public updateValue = '';

  public get numericUpdateValue(): number | null {
    return !this.updateValue ? null : +this.updateValue;
  }

  public ngOnInit() {
    const value = localStorage.getItem('value');
    if (value !== null) {
      this.value = value;
    }
    const options = localStorage.getItem('options');
    if (options !== null) {
      this.options = JSON.parse(options);
    }
    const updateValue = localStorage.getItem('updateValue');
    if (updateValue !== null) {
      this.updateValue = updateValue;
    }
  }

  public onValueChange(evt: ValueChange) {
    localStorage.setItem('value', evt.stringValue);
  }

  public onOptionChange() {
    localStorage.setItem('options', JSON.stringify(this.options));
  }

  public onUpdateValueChange() {
    localStorage.setItem('updateValue', this.updateValue);
  }

  public numberToString(num: number | null): string {
    return String(num);
  }

  public optionsToString(options: NumericInputOptions = {}): string {
    return `{\n  ${Object.keys(options)
      .map(
        (k) =>
          `${k}: ${
            typeof options[k] === 'string'
              ? JSON.stringify(options[k])
              : String(options[k])
          }`
      )
      .join(',\n  ')}\n}`;
  }
}
