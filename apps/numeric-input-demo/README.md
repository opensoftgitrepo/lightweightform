# Lightweightform/numeric-input-demo

This package contains a demo application to play around with the numeric input.

## Launch

Running `npm run dev` at the top-level will start a local server accessible at:
https://localhost:3002.

To only launch the `numeric-input-demo` application run
`npm run dev -- --scope @lightweightform-app/numeric-input-demo` instead.
