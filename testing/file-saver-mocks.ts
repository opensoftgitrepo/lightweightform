// tslint:disable:deprecation

import { saveAs } from 'file-saver';

// Mock the `saveAs` function so that calling it saves the passed Blob in a
// local variable
jest.mock('file-saver');

let savedBlob: Blob;
(saveAs as any).mockImplementation((blob: Blob) => {
  savedBlob = blob;
  return true; // Save successful (e.g. when calling `msSaveOrOpenBlob`)
});

// Override `document.createElement` to mock clicks on `<input type="file">`
// elements by triggering a change of their value by setting its list of files
// as the previously saved `Blob` (last call to `saveAs`)
// tslint:disable-line:deprecation
const createElem = document.createElement;
document.createElement = function () {
  const elem = createElem.apply(this, arguments);
  if (elem.tagName === 'INPUT') {
    elem.addEventListener('click', (evt) => {
      if (elem.type === 'file') {
        Object.defineProperty(evt.target, 'files', { value: [savedBlob] });
        evt.target.dispatchEvent(new Event('change'));
      }
    });
  }
  return elem;
};
