import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Input,
  Optional,
} from '@angular/core';
import { LfI18n, LfRouter, LfStorage, PathBased } from '@lightweightform/core';
import {
  isNullableSchema,
  isStringSchema,
  pathError,
  ValidationMode,
} from '@lightweightform/storage';
import { makeObservable } from 'mobx';
import { action, computed } from 'mobx-angular';

import { InlineValue } from '../../abstract/inline-value';

/**
 * Component used for editing a "multiple lines" LF string.
 */
@Component({
  selector: 'lf-textarea',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './textarea.component.html',
})
export class TextareaComponent extends InlineValue {
  /**
   * Initial height of the textarea (CSS measurement string).
   */
  @Input() public initialHeight = '6rem';

  constructor(
    @Optional()
    @Inject(PathBased)
    parentPathBasedComponent: PathBased | null,
    lfStorage: LfStorage,
    lfI18n: LfI18n,
    @Optional()
    @Inject(LfRouter)
    lfRouter: LfRouter | null
  ) {
    super(parentPathBasedComponent, lfStorage, lfI18n, lfRouter);
    makeObservable(this);
  }

  /**
   * Whether to show the "is required" icon on the component.
   */
  @computed
  public get showIsRequired(): boolean {
    return this.minLength !== undefined && this.minLength > 0;
  }

  /**
   * Minimum length allowed.
   */
  @computed
  public get minLength(): number | undefined {
    const minLength =
      this.relativeStorage.validationMode === ValidationMode.AUTO
        ? this.relativeStorage.getStateProperty('.', 'minLength')
        : this.relativeStorage.schema().minLength;
    return typeof minLength === 'number' ? minLength : undefined;
  }

  /**
   * Maximum length allowed.
   */
  @computed
  public get maxLength(): number | undefined {
    const maxLength =
      this.relativeStorage.validationMode === ValidationMode.AUTO
        ? this.relativeStorage.getStateProperty('.', 'maxLength')
        : this.relativeStorage.schema().maxLength;
    return typeof maxLength === 'number' ? maxLength : undefined;
  }

  /**
   * Action to run on every `input` event of the textarea input.
   * @param evt `input` event.
   */
  @action
  public _onInput(evt): void {
    this.relativeStorage.set('.', evt.target.value);
    this.relativeStorage.setDirty();
  }

  protected validatePath(): void {
    const schema = this.relativeStorage.schema();
    if (!isStringSchema(schema) || isNullableSchema(schema)) {
      throw pathError(
        this.path,
        'Schema must be of the "string" type and not "nullable"'
      );
    }
  }
}
