import {
  ComponentFixture,
  ComponentFixtureAutoDetect,
  TestBed,
} from '@angular/core/testing';
import { MobxAngularModule } from 'mobx-angular';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

import { TooltipComponent } from '../tooltip.component';

describe('Tooltip component', () => {
  let fixture: ComponentFixture<TooltipComponent>;
  let component: TooltipComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MobxAngularModule, TooltipModule],
      declarations: [TooltipComponent],
      providers: [{ provide: ComponentFixtureAutoDetect, useValue: true }],
    });
    fixture = TestBed.createComponent(TooltipComponent);
    component = fixture.componentInstance;
  });

  it('is created', () => {
    expect(component).toBeDefined();
  });
});
