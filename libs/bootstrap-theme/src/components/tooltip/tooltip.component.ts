import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

/**
 * Lf tooltip component.
 */
@Component({
  selector: 'lf-tooltip',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './tooltip.component.html',
})
export class TooltipComponent {
  @Input() public help?: string;
  @Input() public placement: 'top' | 'bottom' | 'left' | 'right' = 'top';
  @Input() public delay = 0;
}
