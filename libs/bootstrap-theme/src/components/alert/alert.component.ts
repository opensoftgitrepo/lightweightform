import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Input,
  Optional,
  SkipSelf,
} from '@angular/core';
import { LfI18n, LfStorage, PathBased } from '@lightweightform/core';
import { makeObservable } from 'mobx';
import { computed, observable } from 'mobx-angular';

import { Toggleable } from '../../abstract/toggleable';
import { I18N_CLOSE_BUTTON_TOOLTIP_KEY } from '../../i18n/keys';

/**
 * LF alert component.
 */
@Component({
  selector: 'lf-alert',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{ provide: PathBased, useExisting: AlertComponent }],
  templateUrl: './alert.component.html',
})
export class AlertComponent extends Toggleable {
  @observable
  @Input()
  public style: string;
  @observable
  @Input()
  public showCloseButton: boolean;

  constructor(
    @Optional()
    @SkipSelf()
    @Inject(PathBased)
    parentPathBasedComponent: PathBased | null,
    lfStorage: LfStorage,
    lfI18n: LfI18n
  ) {
    super(parentPathBasedComponent, lfStorage, lfI18n);
    makeObservable(this);
  }

  @computed
  public get closeButtonTooltip(): string {
    return this.translate(I18N_CLOSE_BUTTON_TOOLTIP_KEY);
  }
}
