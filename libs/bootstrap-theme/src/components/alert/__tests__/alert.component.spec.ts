import {
  ComponentFixture,
  ComponentFixtureAutoDetect,
  TestBed,
} from '@angular/core/testing';
import {
  LfI18n,
  LfStorage,
  LF_APP_I18N,
  LF_APP_SCHEMA,
} from '@lightweightform/core';
import { recordSchema, Schema } from '@lightweightform/storage';
import { MobxAngularModule } from 'mobx-angular';

import { AlertComponent } from '../alert.component';

describe('Alert component', () => {
  const schema: Schema = recordSchema({});
  let fixture: ComponentFixture<AlertComponent>;
  let component: AlertComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MobxAngularModule],
      declarations: [AlertComponent],
      providers: [
        { provide: ComponentFixtureAutoDetect, useValue: true },
        { provide: LF_APP_SCHEMA, useValue: schema },
        { provide: LF_APP_I18N, useValue: {} },
        LfStorage,
        LfI18n,
      ],
    });
    fixture = TestBed.createComponent(AlertComponent);
    component = fixture.componentInstance;
  });

  it('is created', () => {
    expect(component).toBeDefined();
  });
});
