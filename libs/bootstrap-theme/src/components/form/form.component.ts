import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Optional,
  SkipSelf,
  ViewChild,
} from '@angular/core';
import { LfI18n, LfStorage, PathBased } from '@lightweightform/core';
import { makeObservable } from 'mobx';
import { action, computed } from 'mobx-angular';

import { Value } from '../../abstract/value';
import {
  I18N_FORM_CANCEL_REMOVE_BUTTON_TEXT_KEY,
  I18N_FORM_CONFIRM_REMOVE_BUTTON_TEXT_KEY,
  I18N_FORM_ENSURE_REMOVE_TEXT_KEY,
  I18N_FORM_HELP_ACTION_TEXT_KEY,
  I18N_FORM_REMOVE_ACTION_TEXT_KEY,
} from '../../i18n/keys';
import { ActionsMenuAction } from '../actions-menu/actions-menu.component';
import { ModalComponent } from '../modal/modal.component';

/**
 * LF form component.
 */
@Component({
  selector: 'lf-form',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{ provide: PathBased, useExisting: FormComponent }],
  templateUrl: './form.component.html',
})
export class FormComponent extends Value {
  /**
   * Modal used to show the form's help message.
   */
  @ViewChild('helpModal') public _helpModal: ModalComponent;
  /**
   * Modal used to ask for confirmation when attempting to remove the form.
   */
  @ViewChild('removeModal')
  public _removeModal: ModalComponent;

  constructor(
    @Optional()
    @SkipSelf()
    @Inject(PathBased)
    parentPathBasedComponent: PathBased | null,
    lfStorage: LfStorage,
    lfI18n: LfI18n
  ) {
    super(parentPathBasedComponent, lfStorage, lfI18n);
    makeObservable(this);
  }

  /**
   * Text used for the "help" action.
   */
  @computed
  public get helpActionText(): string {
    return this.translate(I18N_FORM_HELP_ACTION_TEXT_KEY);
  }
  /**
   * Text used for the "remove" action.
   */
  @computed
  public get removeActionText(): string {
    return this.translate(I18N_FORM_REMOVE_ACTION_TEXT_KEY);
  }
  /**
   * Message displayed to the user to confirm removal of the form.
   */
  @computed
  public get ensureRemoveText(): string {
    return this.translate(I18N_FORM_ENSURE_REMOVE_TEXT_KEY);
  }
  /**
   * Text for the "cancel" button in the removal modal.
   */
  @computed
  public get cancelRemoveButtonText(): string {
    return this.translate(I18N_FORM_CANCEL_REMOVE_BUTTON_TEXT_KEY);
  }
  /**
   * Text for the "confirm" button in the removal modal.
   */
  @computed
  public get confirmRemoveButtonText(): string {
    return this.translate(I18N_FORM_CONFIRM_REMOVE_BUTTON_TEXT_KEY);
  }

  /**
   * Whether this form can be removed (if it is `nullable` or its parent is a
   * list or map).
   */
  @computed
  public get isRemovable(): boolean {
    return (
      this.relativeStorage.isNullable() || this.relativeStorage.isRemovable()
    );
  }

  /**
   * Form-related actions shown on the top of the form.
   */
  @computed
  public get _formActions(): ActionsMenuAction[] {
    const actions: ActionsMenuAction[] = [];
    // Help action
    if (this.helpMessage !== undefined) {
      actions.push({
        text: this.helpActionText,
        class: 'help',
        style: 'outline-primary',
        icon: 'question-circle',
        callback: () => this.showHelpModal(),
      });
    }
    // Remove form action
    if (this.isRemovable) {
      actions.push({
        text: this.removeActionText,
        class: 'remove-form',
        style: 'outline-danger',
        icon: 'trash',
        callback: () => this.showRemoveModal(),
      });
    }
    return actions;
  }

  /**
   * Removes this form by either setting it to `null` (when it is `nullable` or
   * removing it from its parent list or map).
   */
  @action
  public remove(): void {
    if (this.relativeStorage.isNullable()) {
      this.relativeStorage.set('.', null);
    } else {
      this.relativeStorage.remove();
    }
  }

  public _cancelRemove(): void {
    this._removeModal.hide();
  }

  public async _confirmRemove(): Promise<void> {
    await this._removeModal.hide();
    this.remove();
  }

  private showHelpModal(): void {
    this._helpModal.show();
  }

  private showRemoveModal(): void {
    this._removeModal.show();
  }
}
