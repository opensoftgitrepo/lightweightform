import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'lf-help-tooltip',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './help-tooltip.component.html',
})
export class HelpTooltipComponent {
  @Input() public help: string;
}
