import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  ComponentFixture,
  ComponentFixtureAutoDetect,
  TestBed,
} from '@angular/core/testing';
import { MobxAngularModule } from 'mobx-angular';

import { HelpTooltipComponent } from '../help-tooltip.component';

describe('Help-tooltip component', () => {
  let fixture: ComponentFixture<HelpTooltipComponent>;
  let component: HelpTooltipComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MobxAngularModule],
      declarations: [HelpTooltipComponent],
      providers: [{ provide: ComponentFixtureAutoDetect, useValue: true }],
      schemas: [NO_ERRORS_SCHEMA],
    });
    fixture = TestBed.createComponent(HelpTooltipComponent);
    component = fixture.componentInstance;
  });

  it('is created', () => {
    expect(component).toBeDefined();
  });
});
