import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Input,
  Optional,
} from '@angular/core';
import { LfI18n, LfRouter, LfStorage, PathBased } from '@lightweightform/core';
import {
  isListSchema,
  isNullableSchema,
  pathError,
  ValidationMode,
} from '@lightweightform/storage';
import { comparer, makeObservable } from 'mobx';
import { action, computed, observable } from 'mobx-angular';

import { Option, SelectionValue } from '../../abstract/selection-value';

/**
 * Component used for selecting a list of options by using inputs of type
 * `checkbox`.
 */
@Component({
  selector: 'lf-checkbox-group',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './checkbox-group.component.html',
})
export class CheckboxGroupComponent extends SelectionValue {
  /**
   * Whether the list of checkbox inputs should be displayed inline. The
   * component will still display in "block" mode in small screens.
   */
  @observable
  @Input()
  public displayInline = false;

  constructor(
    @Optional()
    @Inject(PathBased)
    parentPathBasedComponent: PathBased | null,
    lfStorage: LfStorage,
    lfI18n: LfI18n,
    @Optional()
    @Inject(LfRouter)
    lfRouter: LfRouter | null
  ) {
    super(parentPathBasedComponent, lfStorage, lfI18n, lfRouter);
    makeObservable(this);
  }

  /**
   * Whether to show the "is required" icon on the component.
   */
  @computed
  public get showIsRequired(): boolean {
    const minSize =
      this.relativeStorage.validationMode === ValidationMode.AUTO
        ? this.relativeStorage.getStateProperty('.', 'minSize')
        : this.relativeStorage.schema().minSize;
    return (typeof minSize === 'number' ? minSize : 0) > 0;
  }

  /**
   * Whether a given option is selected.
   * @param option Option to check if it is selected.
   */
  public isSelected(option: Option) {
    return this.relativeStorage
      .get()
      .some((el) => comparer.structural(el, option.value));
  }

  /**
   * Action to run whenever a checkbox `input` changes.
   * @param option Option that changed.
   */
  @action
  public _onOptionChange(option: Option, evt) {
    const checked = !!evt.target.checked;
    const isSelected = this.isSelected(option);
    if (isSelected && !checked) {
      this.relativeStorage.filter(
        '.',
        (el) => !comparer.structural(el, option.value)
      );
    } else if (!isSelected && checked) {
      this.relativeStorage.push('.', option.value);
    }
    this.relativeStorage.setDirty();
  }

  protected validatePath(): void {
    const schema = this.relativeStorage.schema();
    if (!isListSchema(schema) || isNullableSchema(schema)) {
      throw pathError(
        this.path,
        'Schema must be of the "list" type and not "nullable"'
      );
    }
  }
}
