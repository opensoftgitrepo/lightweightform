import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  Inject,
  Input,
  OnDestroy,
  Optional,
} from '@angular/core';
import { LfI18n, LfRouter, LfStorage, PathBased } from '@lightweightform/core';
import { LfFileHolder } from '@lightweightform/core';
import {
  isNullableSchema,
  isStringSchema,
  pathError,
  ValidationMode,
} from '@lightweightform/storage';
import { makeObservable, observe, reaction } from 'mobx';
import { action, computed, observable } from 'mobx-angular';

import { InlineValue } from '../../abstract/inline-value';
import {
  I18N_FILE_BROWSE_TEXT_KEY,
  I18N_FILE_CHOOSE_PLACEHOLDER_KEY,
  I18N_FILE_CLEAR_BUTTON_TOOLTIP_KEY,
  I18N_FILE_NONE_CHOSEN_KEY,
} from '../../i18n/keys';

/**
 * Component used for selecting files.
 */
@Component({
  selector: 'lf-file',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './file.component.html',
})
export class FileComponent
  extends InlineValue
  implements AfterViewInit, OnDestroy
{
  private static idCounter = 0;

  /**
   * One or more unique file type specifiers describing file types to allow.
   */
  @observable
  @Input()
  public accept = '';

  /**
   * Minimum allowed file size in bytes.
   */
  @observable
  @Input()
  public minSize?: number;

  /**
   * Maximum allowed file size in bytes.
   */
  @observable
  @Input()
  public maxSize?: number;

  // Unique id per component, used for the label to target the file input
  public _id = `lf-file-input-${FileComponent.idCounter++}`;

  @observable
  public _selectedFile: File | null = null;

  // Disposers
  private disposeObserveValue: () => void;
  private disposeOnMinMaxSizeChange: () => void;

  constructor(
    @Optional()
    @Inject(PathBased)
    parentPathBasedComponent: PathBased | null,
    lfStorage: LfStorage,
    lfI18n: LfI18n,
    @Optional()
    @Inject(LfRouter)
    lfRouter: LfRouter | null,
    private lfFileHolder: LfFileHolder
  ) {
    super(parentPathBasedComponent, lfStorage, lfI18n, lfRouter);
    makeObservable(this);
  }

  /**
   * "Choose file" placeholder text.
   */
  @computed
  public get choosePlaceholder(): string {
    return this.translate(I18N_FILE_CHOOSE_PLACEHOLDER_KEY);
  }
  /**
   * "Browse" text.
   */
  @computed
  public get browseText(): string {
    return this.translate(I18N_FILE_BROWSE_TEXT_KEY);
  }
  /**
   * "No file chosen" text.
   */
  @computed
  public get noneChosenText(): string {
    return this.translate(I18N_FILE_NONE_CHOSEN_KEY);
  }
  /**
   * "Clear" button tooltip.
   */
  @computed
  public get clearButtonTooltip(): string {
    return this.translate(I18N_FILE_CLEAR_BUTTON_TOOLTIP_KEY);
  }

  /**
   * Whether to show the "is required" icon on the component.
   */
  @computed
  public get showIsRequired(): boolean {
    return (
      (this.relativeStorage.validationMode === ValidationMode.AUTO
        ? this.relativeStorage.getStateProperty('.', 'isRequired')
        : this.relativeStorage.schema().isRequired) === true
    );
  }

  @computed
  public get value() {
    return this.relativeStorage.has() ? this.relativeStorage.get() : undefined;
  }

  public ngAfterViewInit() {
    super.ngAfterViewInit();

    // Update the selected file when the value changes. If the file is in the
    // file holder then we switch to it, otherwise we set the file as `null`,
    // since it doesn't exist
    this.disposeObserveValue = observe(
      this,
      'value',
      ({ newValue }) => {
        const file = this.lfFileHolder.get(this.relativeStorage.currentPath);
        if (file && newValue === file.name) {
          this._selectedFile = file;
        } else {
          this.setFile(null);
        }
      },
      true
    );

    // Remove selected file when a file was selected but the `minSize`/`maxSize`
    // changed and made the selected file invalid
    this.disposeOnMinMaxSizeChange = reaction(
      () => [this.value, this.minSize, this.maxSize],
      () => {
        const file = this.lfFileHolder.get(this.relativeStorage.currentPath);
        if (file && !this.hasValidSize(file)) {
          this.setFile(null);
        }
      },
      { name: 'fileReactionOnMinMaxSizeChange' }
    );
  }

  public ngOnDestroy() {
    this.disposeObserveValue && this.disposeObserveValue();
    this.disposeOnMinMaxSizeChange && this.disposeOnMinMaxSizeChange();
    super.ngOnDestroy();
  }

  /**
   * Action to run on every `input` event of the file input.
   * @param evt `input` event.
   */
  @action
  public _onInput(evt): void {
    this.setFile(evt.target.files[0] || null);
    this.relativeStorage.setDirty();
  }

  public _onClickClear(evt): void {
    evt.preventDefault();
    this.setFile(null);
  }

  protected validatePath(): void {
    const schema = this.relativeStorage.schema();
    if (!isStringSchema(schema) || !isNullableSchema(schema)) {
      throw pathError(
        this.path,
        'Schema must be of the "string" type and "nullable"'
      );
    }
  }

  @action
  private setFile(file: File | null) {
    const fileInput = this.targets.first.nativeElement;
    // Disallow files with wrong sizes
    if (!this.hasValidSize(file)) {
      // Access file input via `#focusTarget`
      fileInput.value = '';
      file = null;
    }
    this._selectedFile = file;

    // Set file in file holder service
    if (file != null) {
      this.lfFileHolder.set(this.relativeStorage.currentPath, file);
    } else {
      this.lfFileHolder.delete(this.relativeStorage.currentPath);
      fileInput.value = '';
    }

    this.relativeStorage.set('.', file?.name || null);
  }

  private hasValidSize(file: File | null) {
    return (
      !file ||
      (file.size >= (this.minSize ?? 0) &&
        file.size <= (this.maxSize ?? Infinity))
    );
  }
}
