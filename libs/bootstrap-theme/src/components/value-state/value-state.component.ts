import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Optional,
} from '@angular/core';
import { LfI18n, LfStorage, PathBased } from '@lightweightform/core';
import {
  getStateProperty,
  isArrayLike,
  isReservedStateProp,
} from '@lightweightform/storage';
import { isObservable, isObservableMap, keys, makeObservable } from 'mobx';
import { computed } from 'mobx-angular';
import { isPromiseBasedObservable } from 'mobx-utils';

/**
 * Component used to debug information about the state of a value.
 */
@Component({
  selector: 'lf-value-state',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './value-state.component.html',
})
export class ValueStateComponent extends PathBased {
  /**
   * Whether the application is in production mode.
   */
  public inProduction: boolean = process.env.NODE_ENV === 'production';

  constructor(
    @Optional()
    @Inject(PathBased)
    parentPathBasedComponent: PathBased | null,
    lfStorage: LfStorage,
    lfI18n: LfI18n
  ) {
    super(parentPathBasedComponent, lfStorage, lfI18n);
    makeObservable(this);
  }

  /**
   * State properties where each "promise" is transformed into an object for
   * easier rendering as a JSON. Also hides the `$mobx` property.
   */
  @computed
  public get stateProperties(): Record<string, any> {
    const schema = this.relativeStorage.schema();
    const state = this.relativeStorage.getState();
    const value = this.relativeStorage.get();
    return Array.from(keys(state.props)).reduce((obj, prop: string) => {
      let propValue: any = null;
      // When the value is `null`, other validations shouldn't be executed
      if (
        value !== null ||
        !isReservedStateProp(schema, prop) ||
        prop === 'isRequired' ||
        prop === 'validationIssues'
      ) {
        propValue = getStateProperty(schema, state, prop);
        if (isPromiseBasedObservable(propValue)) {
          const res = (propValue as any).value;
          propValue = {
            isPromise: true,
            state: propValue.state,
            value: res instanceof Error ? `Error(${res.message})` : res,
          };
        }
      }
      obj[prop] = this.toJS(propValue);
      return obj;
    }, {});
  }

  /**
   * Whether the state properties contain at least one async property.
   */
  @computed
  public get hasAsyncProperties(): boolean {
    const props = this.stateProperties;
    return Object.keys(props).some((prop) => {
      const val = props[prop];
      return typeof val === 'object' && val && val.isPromise;
    });
  }

  /**
   * Recursively transforms MobX values into their vanilla JS representation.
   * Differs from MobX's `toJS` implementation by recursing even inside non-MobX
   * values.
   * @param value Value to transform.
   * @returns Transformed value.
   */
  private toJS(value: any) {
    if (isArrayLike(value)) {
      return value.map((el) => this.toJS(el));
    }
    if (value instanceof Map || isObservableMap(value)) {
      return new Map(
        Array.from(value.entries()).map(
          ([key, val]) => [key, this.toJS(val)] as [string, any]
        )
      );
    }
    if (value !== null && typeof value === 'object') {
      const keysFn: any = isObservable(value) ? keys : Object.keys;
      return keysFn(value).reduce((obj, prop: any) => {
        obj[prop] = this.toJS(value[prop]);
        return obj;
      }, {});
    }
    return value;
  }
}
