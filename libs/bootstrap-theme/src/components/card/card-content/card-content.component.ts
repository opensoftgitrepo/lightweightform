import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Inject,
  Input,
  Optional,
  SkipSelf,
} from '@angular/core';
import { LfI18n, LfStorage, PathBased } from '@lightweightform/core';
import { makeObservable } from 'mobx';
import { computed, observable } from 'mobx-angular';

/**
 * Available types of card content.
 */
export type CardContentType =
  | 'header'
  | 'body'
  | 'footer'
  | 'title'
  | 'subtitle'
  | 'text'
  | 'link'
  | 'img'
  | 'img-overlay'
  | 'img-top'
  | 'img-bottom';

/**
 * Content of an `lf-card`.
 */
@Component({
  selector: 'lf-card-content, [lf-card-content]',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{ provide: PathBased, useExisting: CardContentComponent }],
  template:
    '<ng-container *mobxAutorun><ng-content></ng-content></ng-container>',
})
export class CardContentComponent extends PathBased {
  /**
   * Type of the content.
   */
  @observable
  @Input()
  public contentType: CardContentType;

  constructor(
    @Optional()
    @SkipSelf()
    @Inject(PathBased)
    parentPathBasedComponent: PathBased | null,
    lfStorage: LfStorage,
    lfI18n: LfI18n
  ) {
    super(parentPathBasedComponent, lfStorage, lfI18n);
    makeObservable(this);
  }

  // Classes for each type of card content
  @computed
  @HostBinding('class.card-header')
  public get _hostClassCardHeader() {
    return this.contentType === 'header';
  }
  @computed
  @HostBinding('class.card-body')
  public get _hostClassCardBody() {
    return this.contentType === 'body';
  }
  @computed
  @HostBinding('class.card-footer')
  public get _hostClassCardFooter() {
    return this.contentType === 'footer';
  }
  @computed
  @HostBinding('class.card-title')
  public get _hostClassCardTitle() {
    return this.contentType === 'title';
  }
  @computed
  @HostBinding('class.card-subtitle')
  public get _hostClassCardSubtitle() {
    return this.contentType === 'subtitle';
  }
  @computed
  @HostBinding('class.card-text')
  public get _hostClassCardText() {
    return this.contentType === 'text';
  }
  @computed
  @HostBinding('class.card-link')
  public get _hostClassCardLink() {
    return this.contentType === 'link';
  }
  @computed
  @HostBinding('class.card-img')
  public get _hostClassCardImg() {
    return this.contentType === 'img';
  }
  @computed
  @HostBinding('class.card-img-overlay')
  public get _hostClassCardImgOverlay() {
    return this.contentType === 'img-overlay';
  }
  @computed
  @HostBinding('class.card-img-top')
  public get _hostClassCardImgTop() {
    return this.contentType === 'img-top';
  }
  @computed
  @HostBinding('class.card-img-bottom')
  public get _hostClassCardImgBottom() {
    return this.contentType === 'img-bottom';
  }
}
