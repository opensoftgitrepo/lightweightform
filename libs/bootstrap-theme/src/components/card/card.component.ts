import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Inject,
  Optional,
  SkipSelf,
} from '@angular/core';
import { LfI18n, LfStorage, PathBased } from '@lightweightform/core';

/**
 * Bootstrap card component, see options on how to further customise the card
 * with background and border utilities at [Bootstrap documentation](
 * https://getbootstrap.com/docs/4.1/components/card/#card-styles).
 */
@Component({
  selector: 'lf-card, [lf-card]',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{ provide: PathBased, useExisting: CardComponent }],
  template:
    '<ng-container *mobxAutorun><ng-content></ng-content></ng-container>',
})
export class CardComponent extends PathBased {
  /**
   * Always add the class `card` to LF cards.
   */
  @HostBinding('class.card') public _hostClassCard = true;

  constructor(
    @Optional()
    @SkipSelf()
    @Inject(PathBased)
    parentPathBasedComponent: PathBased | null,
    lfStorage: LfStorage,
    lfI18n: LfI18n
  ) {
    super(parentPathBasedComponent, lfStorage, lfI18n);
  }
}
