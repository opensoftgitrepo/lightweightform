import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  ComponentFixture,
  ComponentFixtureAutoDetect,
  TestBed,
} from '@angular/core/testing';
import {
  LfI18n,
  LfStorage,
  LF_APP_I18N,
  LF_APP_SCHEMA,
} from '@lightweightform/core';
import { StringSchema, stringSchema } from '@lightweightform/storage';
import { MobxAngularModule } from 'mobx-angular';

import { TextComponent } from '../text.component';

describe('Text component', () => {
  const schema: StringSchema = stringSchema();
  let fixture: ComponentFixture<TextComponent>;
  let component: TextComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MobxAngularModule],
      declarations: [TextComponent],
      providers: [
        { provide: ComponentFixtureAutoDetect, useValue: true },
        { provide: LF_APP_SCHEMA, useValue: schema },
        { provide: LF_APP_I18N, useValue: {} },
        LfStorage,
        LfI18n,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });
    fixture = TestBed.createComponent(TextComponent);
    component = fixture.componentInstance;
  });

  it('is created', () => {
    expect(component).toBeDefined();
  });
});
