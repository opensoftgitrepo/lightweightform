import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { makeObservable } from 'mobx';
import { computed, observable } from 'mobx-angular';

/**
 * LF icon component.
 */
@Component({
  selector: 'lf-icon',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './icon.component.html',
})
export class IconComponent {
  @observable
  @Input()
  public icon: string;
  @observable
  @Input()
  public size?: string;
  @observable
  @Input()
  public fixedWidth = false;
  @observable
  @Input()
  public spin = false;
  @observable
  @Input()
  public pulse = false;
  @observable
  @Input()
  public ariaHidden = true;

  constructor() {
    makeObservable(this);
  }

  @computed
  public get classes(): Record<string, boolean> {
    const classes = {
      fa: true,
      ['fa-' + this.icon]: true,
      'fa-fw': this.fixedWidth,
      'fa-spin': this.spin,
      'fa-pulse': this.pulse,
    };
    if (this.size) {
      classes['fa-' + this.size] = true;
    }
    return classes;
  }
}
