import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Input,
} from '@angular/core';
import { LfI18n } from '@lightweightform/core';
import { makeObservable, runInAction } from 'mobx';
import { action, computed, observable } from 'mobx-angular';

import {
  I18N_ACTIONS_MENU_BUTTON_TEXT_KEY,
  I18N_ACTIONS_MENU_KEY,
  I18N_ACTIONS_MENU_OPTIONS_KEY,
} from '../../i18n/keys';
import { ButtonSize, ButtonStyle } from '../button/button.component';

/**
 * Action of an actions-menu.
 */
export interface ActionsMenuAction {
  /**
   * Identifier of the action (used to identify the action in the i18n file).
   */
  id?: string;
  /**
   * Text of the action (which may also be specified via the i18n file).
   */
  text?: string;
  /**
   * Class name to add to the action.
   */
  class?: string;
  /**
   * Style of the action (relevant when the actions aren't collapsable).
   */
  style?: ButtonStyle;
  /**
   * (Font Awesome) icon of the action.
   */
  icon?: string;
  /**
   * Whether the action should show as active.
   */
  isActive?: () => boolean;
  /**
   * Whether the action should show as disabled.
   */
  isDisabled?: () => boolean;
  /**
   * Action to execute when the action is pressed (can be asynchronous, in which
   * case the action will be disabled during the duration of the action and a
   * loading animation will appear).
   * @param value Value of the selected option, when the action has options.
   */
  callback?: (value?: any) => void | Promise<any>;
  /**
   * Use the option as an external link.
   */
  href?: string;
  /**
   * Options of the action (when defined, the action's button will become the
   * toggle for a dropdown menu containing the options as items).
   */
  options?: ActionsMenuActionOption[];
  /**
   * Whether the dropdown menu should be aligned to the right of the dropdown
   * toggle. Relevant only when `options` is declared.
   */
  alignRight?: boolean;
}

/**
 * Option of an action of an actions-menu.
 */
export interface ActionsMenuActionOption {
  /**
   * Identifier of the option (used to identify the option in the i18n file).
   */
  id?: string;
  /**
   * Text of the option (which may also be specified via the i18n file).
   */
  text?: string;
  /**
   * Class name to add to the option.
   */
  class?: string;
  /**
   * (Font Awesome) icon of the action.
   */
  icon?: string;
  /**
   * Value of the option (passed to the callback of the action when the option
   * is selected).
   */
  value: any;
  /**
   * Whether to display the option as active.
   */
  isActive?: () => boolean;
  /**
   * Whether to display the option as disabled.
   */
  isDisabled?: () => boolean;
  /**
   * Action to execute when the option is pressed. Will be executed before the
   * action's callback, when defined (can be asynchronous, in which case the
   * action will be disabled during the duration of the action and a loading
   * animation will appear).
   */
  callback?: () => void | Promise<any>;
  /**
   * Use the option as an external link.
   */
  href?: string;
}

/**
 * Available sizes on which to collapse the actions-menu.
 */
export type ActionsMenuCollapseSize =
  | 'none'
  | 'sm'
  | 'md'
  | 'lg'
  | 'xl'
  | 'every';

/**
 * Component that responsively displays a menu of actions.
 */
@Component({
  selector: 'lf-actions-menu',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './actions-menu.component.html',
})
export class ActionsMenuComponent {
  /**
   * Actions of the actions-menu.
   */
  @observable.struct
  @Input()
  public actions: ActionsMenuAction[];
  /**
   * Size of the buttons of the actions-menu.
   */
  @observable
  @Input()
  public buttonsSize: ButtonSize;
  /**
   * Screen size at which the actions-menu should collapse.
   */
  @observable
  @Input()
  public collapseSize: ActionsMenuCollapseSize = 'md';
  /**
   * Screen size at which the actions-menu button should collapse.
   */
  @observable
  @Input()
  public actionsLabelCollapseSize: ActionsMenuCollapseSize = 'sm';
  /**
   * Whether the actions-menu (when collapsed) should align its menu to the
   * right.
   */
  @observable
  @Input()
  public alignMenusRight = false;

  /**
   * Actions currently executing.
   */
  @observable
  private runningActions = new Set<string>();

  constructor(@Inject(LfI18n) protected lfI18n: LfI18n) {
    makeObservable(this);
  }

  @computed
  public get actionsLabel(): string {
    return this.lfI18n.get(I18N_ACTIONS_MENU_BUTTON_TEXT_KEY);
  }

  public actionText(act: ActionsMenuAction): string {
    return act.text != null
      ? act.text
      : act.id != null
      ? this.lfI18n.get(`${I18N_ACTIONS_MENU_KEY}.${act.id}`)
      : '';
  }

  public actionOptionText(
    act: ActionsMenuAction,
    option: ActionsMenuActionOption
  ): string {
    return option.text != null
      ? option.text
      : act.id != null && option.id != null
      ? this.lfI18n.get(
          `${I18N_ACTIONS_MENU_OPTIONS_KEY}.${act.id}.${option.id}`
        )
      : '';
  }

  @action
  public async _runAction(act: ActionsMenuAction, value?: any) {
    const res = act.callback && act.callback(value);
    if (res && typeof res.then === 'function') {
      const actionId = this.actionId(act);
      try {
        this.runningActions.add(actionId);
        await res;
      } finally {
        runInAction(() => this.runningActions.delete(actionId));
      }
    }
  }

  @action
  public async _runActionOption(
    act: ActionsMenuAction,
    option: ActionsMenuActionOption
  ) {
    const optionId = this.actionId(act, option);
    const optionRes = option.callback && option.callback();
    this.runningActions.add(optionId);
    try {
      if (optionRes && typeof optionRes === 'function') {
        await optionRes;
      }
      await this._runAction(act, option.value);
    } finally {
      runInAction(() => this.runningActions.delete(optionId));
    }
  }

  @computed
  public get _isAnyActionRunning(): boolean {
    return this.runningActions.size > 0;
  }

  public _isActionRunning(act: ActionsMenuAction): boolean {
    return this.runningActions.has(this.actionId(act));
  }

  public _isChildOptionRunning(act: ActionsMenuAction): boolean {
    for (const id of this.runningActions) {
      if (id.startsWith(`${this.actionId(act)}.`)) {
        return true;
      }
    }
    return false;
  }

  public _isActionOptionRunning(
    act: ActionsMenuAction,
    option: ActionsMenuActionOption
  ): boolean {
    return this.runningActions.has(this.actionId(act, option));
  }

  private actionId(
    act: ActionsMenuAction,
    option?: ActionsMenuActionOption
  ): string {
    const actionId = act.id || this.actionText(act);
    const optionId =
      option && (option.id || this.actionOptionText(act, option));
    return optionId ? `${actionId}.${optionId}` : `${actionId}`;
  }
}
