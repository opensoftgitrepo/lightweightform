import { Component, HostBinding } from '@angular/core';

/**
 * Component that represents the footer of a Bootstrap modal.
 */
@Component({
  selector: 'lf-modal-footer, [lf-modal-footer]',
  template: '<ng-content></ng-content>',
})
export class ModalFooterComponent {
  @HostBinding('class.modal-footer') public _hostClassModalFooter = true;
}
