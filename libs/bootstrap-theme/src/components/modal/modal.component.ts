import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  Output,
  SkipSelf,
  ViewChild,
} from '@angular/core';
import { LfI18n, LfStorage, PathBased } from '@lightweightform/core';
import 'bootstrap/js/dist/modal.js';
import $ from 'jquery';
import { makeObservable, reaction } from 'mobx';
import { observable } from 'mobx-angular';
import { Subscription } from 'rxjs';

import { Toggleable } from '../../abstract/toggleable';

/**
 * Available modal sizes.
 */
export type BsModalSize = 'sm' | 'lg';

/**
 * LF modal component.
 */
@Component({
  selector: 'lf-modal',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{ provide: PathBased, useExisting: ModalComponent }],
  templateUrl: './modal.component.html',
})
export class ModalComponent
  extends Toggleable
  implements OnInit, AfterViewInit, OnDestroy
{
  /**
   * Size of the modal component. See available sizes at
   * [Bootstrap documentation](
   * https://getbootstrap.com/docs/4.0/components/modal/#optional-sizes).
   */
  @observable
  @Input()
  public size: BsModalSize;
  /**
   * Whether to center the modal vertically on screen.
   */
  @observable
  @Input()
  public isCentered = false;
  /**
   * Whether to animate the modal component.
   */
  @observable
  @Input()
  public animate = true;
  /**
   * Whether to enable closing the modal by pressing the `Esc` key.
   */
  @observable
  @Input()
  public closeWithEsc = true;
  /**
   * Whether to show a dark background underneath the modal when it appears.
   */
  @observable
  @Input()
  public showBackdrop = true;
  /**
   * Whether to disable closing the modal by clicking on the backdrop.
   */
  @observable
  @Input()
  public ignoreBackdropClick = false;
  /**
   * If the modal contains `<input>`, `<select>`, or `<textarea>` elements
   * within, focus the first found element after the modal opens.
   */
  @observable
  @Input()
  public autoFocus = false;

  /**
   * Event emitter which emits an event whenever the modal finishes showing.
   */
  @Output()
  public shown: EventEmitter<void> = new EventEmitter<void>();
  /**
   * Event emitter which emits an event whenever the modal finishes hiding.
   */
  @Output()
  public hidden: EventEmitter<void> = new EventEmitter<void>();

  @ViewChild('modal') public _modal: ElementRef;

  private $modal: JQuery;
  // Whether the modal is animating
  private isAnimating: boolean;

  // Subscriptions
  private disposeOnOptionsChange: () => void;
  private onShowSubscription: Subscription;
  private onHideSubscription: Subscription;

  constructor(
    @Optional()
    @SkipSelf()
    @Inject(PathBased)
    parentPathBasedComponent: PathBased | null,
    lfStorage: LfStorage,
    lfI18n: LfI18n
  ) {
    super(parentPathBasedComponent, lfStorage, lfI18n);
    makeObservable(this);
  }

  public ngAfterViewInit() {
    this.createModal();
  }

  public ngOnDestroy() {
    this.destroyModal();
    super.ngOnDestroy();
  }

  /**
   * Method to call on long modals when their content changes, which forces
   * their height to also change.
   */
  public refresh(): void {
    this.$modal.modal('handleUpdate');
  }

  public show(): Promise<void> {
    return new Promise((resolve: () => void) => {
      super.show();
      if (this.isAnimating) {
        this.$modal.one('shown.bs.modal', resolve);
      } else {
        resolve();
      }
    });
  }

  public hide(): Promise<void> {
    return new Promise((resolve: () => void) => {
      super.hide();
      if (this.isAnimating) {
        this.$modal.one('hidden.bs.modal', resolve);
      } else {
        resolve();
      }
    });
  }

  public toggle(): Promise<void> {
    return this.isVisible ? this.hide() : this.show();
  }

  /**
   * Creates a new modal and starts listening to events.
   */
  private createModal() {
    this.$modal = $(this._modal.nativeElement).modal({
      backdrop:
        this.showBackdrop && this.ignoreBackdropClick
          ? 'static'
          : this.showBackdrop,
      keyboard: this.closeWithEsc,
      show: this.isVisible,
    });

    // There seems to be no way of updating these settings, so the most we can
    // do is destroy and reconstruct the modal whenever they change. TODO:
    // Figure out a better approach for this, perhaps implement this
    // functionality ourselves
    this.disposeOnOptionsChange = reaction(
      () => [this.closeWithEsc, this.showBackdrop, this.ignoreBackdropClick],
      () => this.destroyModal().then(() => this.createModal()),
      { name: 'modalReactionOnOptionsChange' }
    );

    // Set the component's state if the modal is shown unconventionally
    // (i.e. without calling `show()`)
    this.$modal.on('show.bs.modal', () => {
      this.isAnimating = true;
      if (!this.isVisible) {
        this.show();
      }
    });

    // Set the component's state if the modal is hidden unconventionally
    // (i.e. without calling `hide()`), this happens, for instance, when
    // pressing `Esc` or when pressing the area outside the modal
    this.$modal.on('hide.bs.modal', () => {
      this.isAnimating = true;
      if (this.isVisible) {
        this.hide();
      }
    });

    // Runs when the showing animation ends
    this.$modal.on('shown.bs.modal', () => {
      this.isAnimating = false;
      this.shown.emit();
      // `isVisible` may change faster than the animations occur, force the UI
      // state to match `isVisible`
      if (!this.isVisible) {
        this.hide();
      }
      // Focus the first input of the modal's body once it opens
      if (this.autoFocus) {
        const firstInput = $(
          '.modal-body input, .modal-body select, .modal-body textarea',
          this.$modal
        ).get(0);
        if (firstInput) {
          firstInput.focus();
        }
      }
    });

    // Runs when the hiding animation ends
    this.$modal.on('hidden.bs.modal', () => {
      this.isAnimating = false;
      this.hidden.emit();
      // `isVisible` may change faster than the animations occur, force the UI
      // state to match `isVisible`
      if (this.isVisible) {
        this.show();
      }
    });

    // Open the modal when `show` is called
    this.onShowSubscription = this._show.subscribe(() => {
      if (!this.isAnimating) {
        this.$modal.modal('show');
      }
    });

    // Close the modal when `hide` is called
    this.onHideSubscription = this._hide.subscribe(() => {
      if (!this.isAnimating) {
        this.$modal.modal('hide');
      }
    });
  }

  /**
   * Unsubscribes from all events and destroys the modal (returns a promise that
   * resolves after the modal has been destroyed).
   * @returns Promise that resolves when the modal has been destroyed.
   */
  // FIXME: This returns a promise because we are forced to hide the modal prior
  // to removing it; otherwise a leftover backdrop will stay within the document
  // (this is probably an upstream Bootstrap bug)
  private destroyModal(): Promise<void> {
    this.disposeOnOptionsChange && this.disposeOnOptionsChange();
    if (this.$modal) {
      this.$modal.off('show.bs.modal');
      this.$modal.off('hide.bs.modal');
      this.$modal.off('shown.bs.modal');
      this.$modal.off('hidden.bs.modal');
    }
    this.onShowSubscription && this.onShowSubscription.unsubscribe();
    this.onHideSubscription && this.onHideSubscription.unsubscribe();
    if (this.isVisible) {
      return new Promise((resolve) => {
        // If we don't hide the modal prior to disposing of it, then a leftover
        // backdrop will stay in the document
        this.hide();
        this.$modal.one('hidden.bs.modal', () => {
          $(this).modal('dispose');
          resolve();
        });
      });
    } else {
      this.$modal.modal('dispose');
      return Promise.resolve();
    }
  }
}
