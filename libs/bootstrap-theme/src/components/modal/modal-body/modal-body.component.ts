import { Component, HostBinding } from '@angular/core';

/**
 * Component that represents the body of a Bootstrap modal.
 */
@Component({
  selector: 'lf-modal-body, [lf-modal-body]',
  template: '<ng-content></ng-content>',
})
export class ModalBodyComponent {
  @HostBinding('class.modal-body') public _hostClassModalBody = true;
}
