import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Input,
} from '@angular/core';
import { makeObservable } from 'mobx';
import { computed, observable } from 'mobx-angular';

import { I18N_CLOSE_BUTTON_TOOLTIP_KEY } from '../../../i18n/keys';
import { ModalComponent } from '../modal.component';

/**
 * Component that represents the header of a Bootstrap modal.
 */
@Component({
  selector: 'lf-modal-header, [lf-modal-header]',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './modal-header.component.html',
})
export class ModalHeaderComponent {
  /**
   * Whether to display a button to close the modal.
   */
  @observable
  @Input()
  public showCloseButton = true;

  // The modal header must always have the `modal-header` class.
  @HostBinding('class.modal-header') public _hostClassModalHeader = true;

  constructor(private modalComponent: ModalComponent) {
    makeObservable(this);
  }

  /**
   * Tooltip text used by the modal's close button.
   */
  @computed
  public get closeButtonTooltip() {
    return this.modalComponent.translate(I18N_CLOSE_BUTTON_TOOLTIP_KEY);
  }

  /**
   * Hides the modal.
   */
  public hide() {
    this.modalComponent.hide();
  }
}
