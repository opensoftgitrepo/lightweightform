import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Optional,
} from '@angular/core';
import { LfI18n, LfRouter, LfStorage, PathBased } from '@lightweightform/core';
import {
  isArrayLike,
  isBooleanSchema,
  pathError,
  ValidationMode,
} from '@lightweightform/storage';
import { makeObservable } from 'mobx';
import { action, computed } from 'mobx-angular';

import { InlineValue } from '../../abstract/inline-value';

/**
 * LF checkbox component.
 */
@Component({
  selector: 'lf-checkbox',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './checkbox.component.html',
})
export class CheckboxComponent extends InlineValue {
  constructor(
    @Optional()
    @Inject(PathBased)
    parentPathBasedComponent: PathBased | null,
    lfStorage: LfStorage,
    lfI18n: LfI18n,
    @Optional()
    @Inject(LfRouter)
    lfRouter: LfRouter | null
  ) {
    super(parentPathBasedComponent, lfStorage, lfI18n, lfRouter);
    makeObservable(this);
  }

  /**
   * Whether to show the "is required" icon on the component.
   */
  @computed
  public get showIsRequired(): boolean {
    if (!this.relativeStorage.isNullable()) {
      const allowedValues =
        this.relativeStorage.validationMode === ValidationMode.AUTO
          ? this.relativeStorage.getStateProperty('.', 'allowedValues')
          : this.relativeStorage.schema().allowedValues;
      return (
        isArrayLike(allowedValues) &&
        allowedValues.length === 1 &&
        allowedValues[0] === true
      );
    }
    return (
      (this.relativeStorage.validationMode === ValidationMode.AUTO
        ? this.relativeStorage.getStateProperty('.', 'isRequired')
        : this.relativeStorage.schema().isRequired) === true
    );
  }

  /**
   * Action to run whenever the value of the checkbox changes.
   * @param evt `change` event.
   */
  @action
  public _onCheckboxChange(evt) {
    this.relativeStorage.set('.', !!evt.target.checked);
    this.relativeStorage.setDirty();
  }

  protected validatePath(): void {
    const schema = this.relativeStorage.schema();
    if (!isBooleanSchema(schema)) {
      throw pathError(this.path, 'Schema must be of the "boolean" type');
    }
  }
}
