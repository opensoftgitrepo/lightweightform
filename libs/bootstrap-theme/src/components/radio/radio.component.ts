import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Input,
  Optional,
} from '@angular/core';
import { LfI18n, LfRouter, LfStorage, PathBased } from '@lightweightform/core';
import { makeObservable } from 'mobx';
import { action, observable } from 'mobx-angular';

import { SingleSelectionValue } from '../../abstract/single-selection-value';

/**
 * Component used for selecting from a list of options by using inputs of type
 * `radio`.
 */
@Component({
  selector: 'lf-radio',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './radio.component.html',
})
export class RadioComponent extends SingleSelectionValue {
  /**
   * Whether the list of radio inputs should be displayed inline. The component
   * will still display in "block" mode in small screens.
   */
  @observable
  @Input()
  public displayInline = false;

  constructor(
    @Optional()
    @Inject(PathBased)
    parentPathBasedComponent: PathBased | null,
    lfStorage: LfStorage,
    lfI18n: LfI18n,
    @Optional()
    @Inject(LfRouter)
    lfRouter: LfRouter | null
  ) {
    super(parentPathBasedComponent, lfStorage, lfI18n, lfRouter);
    makeObservable(this);
  }

  /**
   * Action to run whenever the value of the `radio` input changes.
   * @param evt `change` event.
   */
  @action
  public _onRadioChange(evt) {
    this.relativeStorage.set('.', this.options[evt.target.value].value);
    this.relativeStorage.setDirty();
  }
}
