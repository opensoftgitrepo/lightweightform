import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
  Optional,
  SkipSelf,
} from '@angular/core';
import { LfI18n, LfRouter, LfStorage, PathBased } from '@lightweightform/core';
import { makeObservable } from 'mobx';
import { computed } from 'mobx-angular';

import { AppComponent } from '../app/app.component';

/**
 * Component used to show a tree to navigate between values.
 */
@Component({
  selector: 'lf-tree-nav',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{ provide: PathBased, useExisting: TreeNavComponent }],
  templateUrl: './tree-nav.component.html',
})
export class TreeNavComponent extends PathBased implements OnInit {
  constructor(
    @Optional()
    @SkipSelf()
    @Inject(PathBased)
    parentPathBasedComponent: PathBased | null,
    lfStorage: LfStorage,
    lfI18n: LfI18n,
    private lfRouter: LfRouter
  ) {
    super(parentPathBasedComponent, lfStorage, lfI18n);
    makeObservable(this);
  }

  @computed
  public get _hasRoutes(): boolean {
    return (
      this.lfRouter.matchingRoutes(this.path, { navigateToParent: false })
        .length > 0
    );
  }

  /**
   * Hide the tree-nav.
   */
  public hideTreeNav() {
    (this.parentPathBasedComponent as AppComponent).hideTreeNav();
  }
}
