import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  SkipSelf,
} from '@angular/core';
import {
  LfI18n,
  LfMatchingRoute,
  LfRouter,
  LfStorage,
  PathBased,
} from '@lightweightform/core';
import {
  appendToPath,
  getRowId,
  Identifier,
  isComplexSchema,
  isListSchema,
  isTableSchema,
  pathError,
  ValidationMode,
} from '@lightweightform/storage';
import { makeObservable, observe } from 'mobx';
import { action, computed, observable } from 'mobx-angular';

import {
  I18N_TREE_NAV_ADD_OPTIONAL_VALUE_BUTTON_TEXT_KEY,
  I18N_TREE_NAV_ADD_VALUE_TO_LIST_BUTTON_TEXT_KEY,
  I18N_VALUE_LABEL_KEY,
  I18N_VALUE_RETRY_VALIDATE_TEXT_KEY,
} from '../../../i18n/keys';

/**
 * Entry for an item of the tree-nav.
 */
@Component({
  selector: 'lf-tree-entry',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{ provide: PathBased, useExisting: TreeEntryComponent }],
  templateUrl: './tree-entry.component.html',
})
export class TreeEntryComponent extends PathBased implements OnInit, OnDestroy {
  /**
   * Whether this tree-entry is to be displayed as the root one (in which case
   * its label isn't shown anywhere and its children are displayed one after the
   * other). The entry can be displayed as root only when its schema is complex.
   * A button is displayed at the end of the list when there are optional values
   * to be added or to add a new element in case the schema is a list.
   */
  @observable
  @Input()
  public displayAsRoot = true;

  /**
   * Internal input used to make a tree only show entries that render in the
   * same outlet (values that should render inside other components shouldn't be
   * part of the same tree).
   */
  @Input()
  public _componentLevel: number;

  private disposeOnDisplayAsRootChanges: () => void;

  constructor(
    @Optional()
    @SkipSelf()
    @Inject(PathBased)
    parentPathBasedComponent: PathBased | null,
    lfStorage: LfStorage,
    lfI18n: LfI18n,
    private lfRouter: LfRouter
  ) {
    super(parentPathBasedComponent, lfStorage, lfI18n);
    makeObservable(this);
  }

  /**
   * Label associated with this value.
   */
  @computed
  public get label(): string {
    const label = this.translate(I18N_VALUE_LABEL_KEY);
    return label === undefined
      ? this.relativeStorage.currentPath === '/'
        ? '/'
        : this.relativeStorage.currentId
      : label;
  }
  /**
   * Message used to ask the user to retry validating the value.
   */
  @computed
  public get retryValidateText(): string {
    return this.translate(I18N_VALUE_RETRY_VALIDATE_TEXT_KEY);
  }
  @computed
  public get addOptionalValueButtonText(): string {
    return this.translate(I18N_TREE_NAV_ADD_OPTIONAL_VALUE_BUTTON_TEXT_KEY);
  }
  @computed
  public get addValueToListButtonText(): string {
    return this.translate(I18N_TREE_NAV_ADD_VALUE_TO_LIST_BUTTON_TEXT_KEY);
  }

  @computed
  public get maxSize(): number | undefined {
    const maxSize =
      this.relativeStorage.validationMode === ValidationMode.AUTO
        ? this.relativeStorage.getStateProperty('.', 'maxSize')
        : this.relativeStorage.schema().maxSize;
    return typeof maxSize === 'number' ? maxSize : undefined;
  }

  @computed
  public get matchingRoutes(): LfMatchingRoute[] {
    return this.lfRouter.matchingRoutes(this.path, { navigateToParent: false });
  }

  @computed
  public get navigateableRoute(): LfMatchingRoute | null {
    return this.lfRouter.navigateableRouteFromMatchingRoutes(
      this.matchingRoutes
    );
  }

  @computed
  public get url(): string | null {
    return (
      this.navigateableRoute &&
      this.lfRouter.createUrlFromNavigateableRoute(this.navigateableRoute)
    );
  }

  @computed
  public get isActive(): boolean {
    return this.lfRouter.isActive(this.path);
  }

  @computed
  public get isList(): boolean {
    const schema = this.relativeStorage.schema();
    return isListSchema(schema) || isTableSchema(schema);
  }

  @computed
  public get childrenIds(): Identifier[] {
    return this.lfStorage.childrenIds(this.path).filter((id: string) => {
      const path = appendToPath(this.path, id);
      const matchingRoutes = this.lfRouter
        .matchingRoutes(path, { navigateToParent: false })
        .filter(
          (matchingRoute) =>
            this.matchingRouteComponentLevel(matchingRoute) ===
            this._componentLevel
        );
      return matchingRoutes.length > 0;
    });
  }

  @computed
  public get nonNullChildrenIds(): Identifier[] {
    return this.childrenIds.filter(
      (id) => this.relativeStorage.get(id.toString()) !== null
    );
  }

  @computed
  public get nullChildrenIds(): Identifier[] {
    return this.childrenIds.filter(
      (id) => this.relativeStorage.get(id.toString()) === null
    );
  }

  public ngOnInit() {
    super.ngOnInit();
    // Validate the path whenever `displayAsRoot` changes
    this.disposeOnDisplayAsRootChanges = observe(this, 'displayAsRoot', () => {
      this.validatePath();
    });
    // Set `componentLevel` when not defined
    if (this._componentLevel === undefined) {
      this._componentLevel = this.matchingRouteComponentLevel(
        this.matchingRoutes[0]
      );
    }
  }

  public ngOnDestroy() {
    super.ngOnDestroy();
    this.disposeOnDisplayAsRootChanges();
  }

  public childLabel(id: Identifier): string {
    const childPath = appendToPath(this.path, id);
    return this.lfI18n.getFromPath(childPath, I18N_VALUE_LABEL_KEY) || id;
  }

  @action
  public addToList(): void {
    const { schema, value } = this.lfStorage
      .pathInfo(this.path, { fetchValues: true })
      .slice(-1)[0];
    this.lfStorage.push(this.path, undefined);
    this.lfRouter.navigate(
      appendToPath(
        this.path,
        isListSchema(schema)
          ? value.length - 1
          : getRowId(value[value.length - 1])
      ),
      { queryParamsHandling: 'merge' }
    );
  }

  @action
  public initializeChild(id: Identifier): void {
    this.relativeStorage.initialize(id.toString());
    this.lfRouter.navigate(appendToPath(this.path, id), {
      queryParamsHandling: 'merge',
    });
  }

  protected validatePath(): void {
    if (this.matchingRoutes.length === 0) {
      throw pathError(
        this.path,
        'The path or one of its children must have an associated route'
      );
    }
    const schema = this.relativeStorage.schema();
    if (this.displayAsRoot && !isComplexSchema(schema)) {
      throw pathError(
        this.path,
        'Schema must be complex to display the entry as "root"'
      );
    }
  }

  /**
   * The "component" level of the provided matching route (basically the number
   * of outlets used by the route).
   * @param matchingRoute Matching of a route with a provided path.
   * @returns Component level for the matching route.
   */
  private matchingRouteComponentLevel(matchingRoute: LfMatchingRoute): number {
    return matchingRoute.routes.reduce(
      (level, route) =>
        level + (route.component || route.redirectTo !== undefined ? 1 : 0),
      0
    );
  }
}
