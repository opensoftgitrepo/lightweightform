import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Input,
} from '@angular/core';
import { makeObservable } from 'mobx';
import { observable } from 'mobx-angular';

@Component({
  selector: 'lf-dropdown-menu, [lf-dropdown-menu]',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template:
    '<ng-container *mobxAutorun><ng-content></ng-content></ng-container>',
})
export class DropdownMenuComponent {
  @observable
  @Input()
  @HostBinding('class.dropdown-menu-right')
  public alignRight = false;

  @HostBinding('class.dropdown-menu') public _hostClassDropdownMenu = true;

  constructor() {
    makeObservable(this);
  }
}
