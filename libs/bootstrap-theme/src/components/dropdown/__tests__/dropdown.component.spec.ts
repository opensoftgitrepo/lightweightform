import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  ComponentFixture,
  ComponentFixtureAutoDetect,
  TestBed,
} from '@angular/core/testing';
import {
  LfI18n,
  LfStorage,
  LF_APP_I18N,
  LF_APP_SCHEMA,
} from '@lightweightform/core';
import { recordSchema, Schema } from '@lightweightform/storage';
import { MobxAngularModule } from 'mobx-angular';

import { DropdownItemComponent } from '../dropdown-item/dropdown-item.component';
import { DropdownMenuComponent } from '../dropdown-menu/dropdown-menu.component';
import { DropdownComponent } from '../dropdown.component';

describe('Dropdown component', () => {
  const schema: Schema = recordSchema({});
  let fixture: ComponentFixture<DropdownComponent>;
  let component: DropdownComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MobxAngularModule],
      declarations: [
        DropdownComponent,
        DropdownMenuComponent,
        DropdownItemComponent,
      ],
      providers: [
        { provide: ComponentFixtureAutoDetect, useValue: true },
        { provide: LF_APP_SCHEMA, useValue: schema },
        { provide: LF_APP_I18N, useValue: {} },
        LfStorage,
        LfI18n,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });
    fixture = TestBed.createComponent(DropdownComponent);
    component = fixture.componentInstance;
  });

  it('is created', () => {
    expect(component).toBeDefined();
  });
});
