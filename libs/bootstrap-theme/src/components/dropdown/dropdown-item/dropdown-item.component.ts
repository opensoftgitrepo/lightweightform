import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  HostBinding,
  Input,
} from '@angular/core';
import { makeObservable } from 'mobx';
import { computed, observable } from 'mobx-angular';

/**
 * Available types of dropdown item.
 */
export type DropdownItemType = 'item' | 'divider' | 'text' | 'header';

@Component({
  selector: 'lf-dropdown-item, [lf-dropdown-item]',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template:
    '<ng-container *mobxAutorun><ng-content></ng-content></ng-container>',
})
export class DropdownItemComponent {
  @observable
  @Input()
  public itemType: DropdownItemType = 'item';
  @observable
  @Input()
  @HostBinding('class.active')
  public isActive = false;
  @observable
  @Input()
  public isDisabled = false;

  protected _tagName: string;

  constructor(_host: ElementRef) {
    this._tagName = _host.nativeElement.tagName.toLowerCase();
    makeObservable(this);
  }

  // Classes for each type of dropdown item
  @computed
  @HostBinding('class.dropdown-item')
  public get _hostClassDropdownItem() {
    return this.itemType === 'item';
  }
  @computed
  @HostBinding('class.dropdown-divider')
  public get _hostClassDropdownDivider() {
    return this.itemType === 'divider';
  }
  @computed
  @HostBinding('class.dropdown-item-text')
  public get _hostClassDropdownItemText() {
    return this.itemType === 'text';
  }
  @computed
  @HostBinding('class.dropdown-header')
  public get _hostClassDropdownHeader() {
    return this.itemType === 'header';
  }

  // Set disable (attribute for buttons, class for others)
  @computed
  @HostBinding('class.disabled')
  public get _hostClassDisabled() {
    return (
      this.itemType === 'item' && this._tagName !== 'button' && this.isDisabled
    );
  }
  @computed
  @HostBinding('attr.disabled')
  public get _hostDisabled() {
    return this.itemType === 'item' &&
      this._tagName === 'button' &&
      this.isDisabled
      ? ''
      : undefined;
  }
}
