import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  ElementRef,
  EventEmitter,
  HostBinding,
  Inject,
  Input,
  OnDestroy,
  Optional,
  Output,
  SkipSelf,
} from '@angular/core';
import { LfI18n, LfStorage, PathBased } from '@lightweightform/core';
import 'bootstrap/js/dist/dropdown.js';
import $ from 'jquery';
import { makeObservable, reaction } from 'mobx';
import { computed, observable } from 'mobx-angular';
import { Subscription } from 'rxjs';

import { Toggleable } from '../../abstract/toggleable';

export type DropdownPosition = 'left' | 'right' | 'up' | 'down';
export type DropdownBoundary =
  | 'viewport'
  | 'window'
  | 'scrollParent'
  | ElementRef;
export type DropdownReference = 'toggle' | 'parent' | ElementRef;
export type DropdownDisplay = 'dynamic' | 'static';

/**
 * LF dropdown component.
 */
@Component({
  selector: 'lf-dropdown, [lf-dropdown]',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{ provide: PathBased, useExisting: DropdownComponent }],
  template:
    '<ng-container *mobxAutorun><ng-content></ng-content></ng-container>',
})
export class DropdownComponent
  extends Toggleable
  implements AfterViewInit, OnDestroy
{
  @observable
  @Input()
  public position: DropdownPosition = 'down';
  @observable
  @Input()
  public offset: string | number = 0;
  @observable
  @Input()
  public flip = true;
  @observable
  @Input()
  public boundary: DropdownBoundary = 'scrollParent';
  @observable
  @Input()
  public reference: DropdownReference = 'toggle';
  @observable
  @Input()
  public display: DropdownDisplay = 'dynamic';

  @Output() public shown: EventEmitter<void> = new EventEmitter<void>();
  @Output() public hidden: EventEmitter<void> = new EventEmitter<void>();

  @HostBinding('class.btn-group') public _hostClassBtnGroup = true;

  // The toggle of the dropdown is, by default, the first child of the
  // dropdown's content. Alternatively a `dropdownToggle` reference may be set
  // on the desired element
  @ContentChild('dropdownToggle', { read: ElementRef })
  public _toggle: ElementRef;

  private $host: JQuery;
  private $toggle: JQuery;

  // Whether the dropdown is animating
  private isAnimating: boolean;

  // Subscriptions
  private disposeOnOptionsChange: () => void;
  private showSubscription: Subscription;
  private hideSubscription: Subscription;

  constructor(
    @Optional()
    @SkipSelf()
    @Inject(PathBased)
    parentPathBasedComponent: PathBased | null,
    lfStorage: LfStorage,
    lfI18n: LfI18n,
    private host: ElementRef
  ) {
    super(parentPathBasedComponent, lfStorage, lfI18n);
    makeObservable(this);
  }

  @computed
  @HostBinding('class.dropleft')
  public get _hostClassDropleft() {
    return this.position === 'left';
  }
  @computed
  @HostBinding('class.dropright')
  public get _hostClassDropright() {
    return this.position === 'right';
  }
  @computed
  @HostBinding('class.dropup')
  public get _hostClassDropup() {
    return this.position === 'up';
  }

  public ngAfterViewInit() {
    this.createDropdown();
  }

  public ngOnDestroy() {
    this.destroyDropdown();
    super.ngOnDestroy();
  }

  /**
   * Creates a new dropdown and starts listening to events.
   */
  private createDropdown() {
    const target = this._toggle
      ? this._toggle.nativeElement
      : this.host.nativeElement.children[0];
    this.$toggle = $(target)
      .attr('data-toggle', 'dropdown')
      .attr('aria-haspopup', 'true')
      .attr('aria-expanded', 'false')
      .dropdown({
        offset: this.offset,
        flip: this.flip,
        boundary:
          this.boundary instanceof ElementRef
            ? this.boundary.nativeElement
            : this.boundary,
        reference:
          this.reference instanceof ElementRef
            ? this.reference.nativeElement
            : this.reference,
        display: this.display,
      });
    this.$host = $(this.host.nativeElement);

    // Make UI match state at startup
    if (this.isVisible) {
      this.$toggle.dropdown('toggle');
    }

    // There seems to be no way of updating these settings, so the most we can
    // do is destroy and reconstruct the modal whenever they change. TODO:
    // Figure out a better approach for this
    this.disposeOnOptionsChange = reaction(
      () => [
        this.offset,
        this.flip,
        this.boundary,
        this.reference,
        this.display,
      ],
      () => {
        this.destroyDropdown();
        this.createDropdown();
      },
      { name: 'dropdownReactionOnOptionsChange' }
    );

    // Set the component's state if the dropdown is shown without calling
    // `show()`
    this.$host.on('show.bs.dropdown', () => {
      this.isAnimating = true;
      if (!this.isVisible) {
        this.show();
      }
    });

    // Set the component's state if the dropdown is hidden without calling
    // `hide()`
    this.$host.on('hide.bs.dropdown', () => {
      this.isAnimating = true;
      if (this.isVisible) {
        this.hide();
      }
    });

    // Runs when the showing animation ends
    this.$host.on('shown.bs.dropdown', () => {
      this.isAnimating = false;
      this.shown.emit();
      // `isVisible` may change faster than the animations occur, force the UI
      // state to match `isVisible`
      if (!this.isVisible) {
        this.hide();
      }
    });

    // Runs when the hiding animation ends
    this.$host.on('hidden.bs.dropdown', () => {
      this.isAnimating = false;
      this.hidden.emit();
      // `isVisible` may change faster than the animations occur, force the UI
      // state to match `isVisible`
      if (this.isVisible) {
        this.show();
      }
    });

    // Open the dropdown when `show` is called
    this.showSubscription = this._show.subscribe(() => {
      if (!this.isAnimating && !this.$host.hasClass('show')) {
        this.$toggle.dropdown('toggle');
      }
    });

    // Close the dropdown when `hide` is called
    this.hideSubscription = this._hide.subscribe(() => {
      if (!this.isAnimating && this.$host.hasClass('show')) {
        this.$toggle.dropdown('toggle');
      }
    });
  }

  /**
   * Unsubscribes from all events and destroys the dropdown.
   */
  private destroyDropdown() {
    this.disposeOnOptionsChange && this.disposeOnOptionsChange();
    if (this.$host) {
      this.$host.off('show.bs.dropdown');
      this.$host.off('hide.bs.dropdown');
      this.$host.off('shown.bs.dropdown');
      this.$host.off('hidden.bs.dropdown');
    }
    this.showSubscription && this.showSubscription.unsubscribe();
    this.hideSubscription && this.hideSubscription.unsubscribe();
    this.$toggle &&
      this.$toggle
        .removeAttr('data-toggle')
        .removeAttr('aria-haspopup')
        .removeAttr('aria-expanded')
        .dropdown('dispose');
  }
}
