import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  Inject,
  Input,
  OnDestroy,
  Optional,
} from '@angular/core';
import { LfI18n, LfRouter, LfStorage, PathBased } from '@lightweightform/core';
import {
  NumericInput,
  NumericInputOptions,
} from '@lightweightform/numeric-input';
import {
  isNullableSchema,
  isNumberSchema,
  pathError,
  ValidationMode,
} from '@lightweightform/storage';
import { makeObservable, observe } from 'mobx';
import { action, computed, observable } from 'mobx-angular';
import { Subscription } from 'rxjs';

import { InlineValue } from '../../abstract/inline-value';
import {
  I18N_NUMBER_DECIMAL_SEPARATOR_KEY,
  I18N_NUMBER_PREFIX_KEY,
  I18N_NUMBER_SUFFIX_KEY,
  I18N_NUMBER_THOUSANDS_SEPARATOR_KEY,
} from '../../i18n/keys';

/**
 * LF number component.
 */
@Component({
  selector: 'lf-number',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './number.component.html',
})
export class NumberComponent
  extends InlineValue
  implements AfterViewInit, OnDestroy
{
  /**
   * Number of decimal digits to show.
   */
  @observable
  @Input()
  public scale = 0;
  /**
   * Number of digits to show on the left side of the number.
   */
  @observable
  @Input()
  public leadingZeroes = 1;

  /**
   * Numeric input instance associated with the component.
   */
  private numericInput?: NumericInput;

  // Disposers
  private targetsChangeSubscription: Subscription;
  private disposeObserveOptions: () => void;
  private disposeObserveValue: () => void;

  constructor(
    @Optional()
    @Inject(PathBased)
    parentPathBasedComponent: PathBased | null,
    lfStorage: LfStorage,
    lfI18n: LfI18n,
    @Optional()
    @Inject(LfRouter)
    lfRouter: LfRouter | null
  ) {
    super(parentPathBasedComponent, lfStorage, lfI18n, lfRouter);
    makeObservable(this);
  }

  /**
   * Number's prefix.
   */
  @computed
  public get prefix(): string {
    return this.translate(I18N_NUMBER_PREFIX_KEY);
  }

  /**
   * Number's suffix.
   */
  @computed
  public get suffix(): string {
    return this.translate(I18N_NUMBER_SUFFIX_KEY);
  }

  /**
   * Number's decimal separator.
   */
  @computed
  public get decimalSeparator(): string {
    return this.translate(I18N_NUMBER_DECIMAL_SEPARATOR_KEY);
  }

  /**
   * Number's thousands separator.
   */
  @computed
  public get thousandsSeparator(): string {
    return this.translate(I18N_NUMBER_THOUSANDS_SEPARATOR_KEY);
  }

  @computed
  public get value(): number | null | undefined {
    return this.relativeStorage.has() ? this.relativeStorage.get() : undefined;
  }

  @computed
  public get formattedValue(): string {
    return (
      this.numericInput?.format(this.value === undefined ? null : this.value) ??
      ''
    );
  }

  /**
   * Whether to show the "is required" icon on the component.
   */
  @computed
  public get showIsRequired(): boolean {
    return (
      (this.relativeStorage.validationMode === ValidationMode.AUTO
        ? this.relativeStorage.getStateProperty('.', 'isRequired')
        : this.relativeStorage.schema().isRequired) === true
    );
  }

  @computed
  public get min(): number | undefined {
    if (!this.relativeStorage.has()) {
      return undefined;
    }
    const min =
      this.relativeStorage.validationMode === ValidationMode.AUTO
        ? this.relativeStorage.getStateProperty('.', 'min')
        : this.relativeStorage.schema().min;
    return typeof min === 'number' ? min : undefined;
  }

  @computed
  public get max(): number | undefined {
    if (!this.relativeStorage.has()) {
      return undefined;
    }
    const max =
      this.relativeStorage.validationMode === ValidationMode.AUTO
        ? this.relativeStorage.getStateProperty('.', 'max')
        : this.relativeStorage.schema().max;
    return typeof max === 'number' ? max : undefined;
  }

  /**
   * Numeric input options.
   */
  @computed
  private get numericInputOptions(): NumericInputOptions {
    const isInt = !!this.relativeStorage.schema().isInteger;
    const min = this.min == null ? -Infinity : this.min;
    const max = this.max == null ? Infinity : this.max;
    return {
      numericRepresentation: isInt ? 'int' : 'float',
      scale: this.scale || 0,
      leadingZeroes: this.leadingZeroes || 1,
      min: isInt ? Math.max(min, Number.MIN_SAFE_INTEGER) : min,
      max: isInt ? Math.min(max, Number.MAX_SAFE_INTEGER) : max,
      prefix: this.prefix || '',
      suffix: this.suffix || '',
      decimalSeparator: this.decimalSeparator || '.',
      thousandsSeparator: this.thousandsSeparator || '',
    };
  }

  public ngAfterViewInit() {
    super.ngAfterViewInit();

    // Create a new numeric input instance and recreate it whenever the
    // `<input>` element is recreated
    this.numericInput = this.newNumericInput();
    this.targetsChangeSubscription = this.targets.changes.subscribe(() => {
      this.numericInput?.destroy();
      this.numericInput = this.newNumericInput();
    });

    // Update the numeric input's options whenever they change
    this.disposeObserveOptions = observe(
      this,
      'numericInputOptions' as any,
      ({ newValue }) =>
        this.numericInput?.setOptions(newValue as any, this.value)
    );

    // Update the numeric input whenever the storage value changes
    this.disposeObserveValue = observe(
      this,
      'value',
      ({ newValue }) => {
        if (this.numericInput && newValue !== this.numericInput.toNumber()) {
          this.numericInput.update(newValue == null ? null : (newValue as any));
        }
      },
      true
    );
  }

  public ngOnDestroy() {
    this.targetsChangeSubscription &&
      this.targetsChangeSubscription.unsubscribe();
    this.disposeObserveOptions && this.disposeObserveOptions();
    this.disposeObserveValue && this.disposeObserveValue();
    this.numericInput && this.numericInput.destroy();
    super.ngOnDestroy();
  }

  /**
   * Action to run on every `input` event of the text input.
   */
  @action
  public _onInput(): void {
    this.relativeStorage.setDirty();
  }

  protected validatePath(): void {
    const schema = this.relativeStorage.schema();
    if (!isNumberSchema(schema) || !isNullableSchema(schema)) {
      throw pathError(
        this.path,
        'Schema must be of the "number" type and "nullable"'
      );
    }
  }

  private newNumericInput(): NumericInput | undefined {
    if (this.targets.length > 0) {
      return new NumericInput(this.targets.first.nativeElement, {
        ...this.numericInputOptions,
        onUpdate: (_, numberValue) =>
          this.relativeStorage.has() &&
          !this.relativeStorage.isComputed() &&
          this.relativeStorage.set('.', numberValue),
      });
    }
  }
}
