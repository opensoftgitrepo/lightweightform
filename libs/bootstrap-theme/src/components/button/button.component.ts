import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  HostBinding,
  Input,
} from '@angular/core';
import { makeObservable } from 'mobx';
import { computed, observable } from 'mobx-angular';

/**
 * Available styles for an `lf-button`.
 */
export type ButtonStyle =
  | 'primary'
  | 'secondary'
  | 'success'
  | 'danger'
  | 'warning'
  | 'info'
  | 'light'
  | 'dark'
  | 'link'
  | 'outline-primary'
  | 'outline-secondary'
  | 'outline-success'
  | 'outline-danger'
  | 'outline-warning'
  | 'outline-info'
  | 'outline-light'
  | 'outline-dark';

/**
 * Available sizes for an `lf-button`.
 */
export type ButtonSize = 'sm' | 'lg';

/**
 * Bootstrap button component.
 */
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'button[lf-button], input[lf-button], a[lf-button]',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template:
    '<ng-container *mobxAutorun><ng-content></ng-content></ng-container>',
})
export class ButtonComponent {
  /**
   * Button style. See available styles at [Bootstrap documentation](
   * https://getbootstrap.com/docs/4.0/components/buttons/#examples).
   */
  @observable
  @Input()
  public style?: ButtonStyle;
  /**
   * Button size. See available sizes at [Bootstrap documentation](
   * https://getbootstrap.com/docs/4.0/components/buttons/#sizes).
   */
  @observable
  @Input()
  public size?: ButtonSize;
  /**
   * HTML button type.
   */
  @observable
  @Input()
  public type: 'button' | 'submit' | 'reset' = 'button';
  /**
   * Match the width of parent if `true`.
   */
  @observable
  @Input()
  @HostBinding('class.btn-block')
  public block = false;
  /**
   * Display a dropdown icon (this is automatic if `dropdownToggleSplit` is
   * set).
   */
  @observable
  @Input()
  public dropdownToggle = false;
  /**
   * Reduces the padding of the button so it can be used as the dropdown part of
   * a split dropdown.
   */
  @observable
  @Input()
  @HostBinding('class.dropdown-toggle-split')
  public dropdownToggleSplit = false;
  /**
   * Set the button as disabled.
   */
  @observable
  @Input()
  public isDisabled = false;
  /**
   * Set the button as active.
   */
  @observable
  @Input()
  @HostBinding('class.active')
  public isActive = false;

  /**
   * Always add the class `btn` to LF buttons.
   */
  @HostBinding('class.btn') public _hostClassBtn = true;

  /**
   * Tag of the element (`'button'` or `'a'`).
   */
  protected _tagName: string;

  constructor(_host: ElementRef) {
    makeObservable(this);
    this._tagName = _host.nativeElement.tagName.toLowerCase();
  }

  // Set the role as `'button'` when the element is an `a`.
  @HostBinding('attr.role')
  public get _role() {
    return this._tagName === 'a' ? 'button' : undefined;
  }

  // Set the corresponding style class depending on the button style (setting
  // individual classes instead of `class` itself makes it possible for the user
  // to still set `class` on the component without it being overridden)
  @computed
  @HostBinding('class.btn-primary')
  public get _hostClassBtnPrimary() {
    return this.style === 'primary';
  }
  @computed
  @HostBinding('class.btn-secondary')
  public get _hostClassBtnSecondary() {
    return this.style === 'secondary';
  }
  @computed
  @HostBinding('class.btn-success')
  public get _hostClassBtnSuccess() {
    return this.style === 'success';
  }
  @computed
  @HostBinding('class.btn-danger')
  public get _hostClassBtnDanger() {
    return this.style === 'danger';
  }
  @computed
  @HostBinding('class.btn-warning')
  public get _hostClassBtnWarning() {
    return this.style === 'warning';
  }
  @computed
  @HostBinding('class.btn-info')
  public get _hostClassBtnInfo() {
    return this.style === 'info';
  }
  @computed
  @HostBinding('class.btn-light')
  public get _hostClassBtnLight() {
    return this.style === 'light';
  }
  @computed
  @HostBinding('class.btn-dark')
  public get _hostClassBtnDark() {
    return this.style === 'dark';
  }
  @computed
  @HostBinding('class.btn-link')
  public get _hostClassBtnLink() {
    return this.style === 'link';
  }
  @computed
  @HostBinding('class.btn-outline-primary')
  public get _hostClassBtnOutlinePrimary() {
    return this.style === 'outline-primary';
  }
  @computed
  @HostBinding('class.btn-outline-secondary')
  public get _hostClassBtnOutlineSecondary() {
    return this.style === 'outline-secondary';
  }
  @computed
  @HostBinding('class.btn-outline-success')
  public get _hostClassBtnOutlineSuccess() {
    return this.style === 'outline-success';
  }
  @computed
  @HostBinding('class.btn-outline-danger')
  public get _hostClassBtnOutlineDanger() {
    return this.style === 'outline-danger';
  }
  @computed
  @HostBinding('class.btn-outline-warning')
  public get _hostClassBtnOutlineWarning() {
    return this.style === 'outline-warning';
  }
  @computed
  @HostBinding('class.btn-outline-info')
  public get _hostClassBtnOutlineInfo() {
    return this.style === 'outline-info';
  }
  @computed
  @HostBinding('class.btn-outline-light')
  public get _hostClassBtnOutlineLight() {
    return this.style === 'outline-light';
  }
  @computed
  @HostBinding('class.btn-outline-dark')
  public get _hostClassBtnOutlineDark() {
    return this.style === 'outline-dark';
  }

  // Set the corresponding style class
  @computed
  @HostBinding('class.btn-sm')
  public get _hostClassBtnSm() {
    return this.size === 'sm';
  }
  @computed
  @HostBinding('class.btn-lg')
  public get _hostClassBtnLg() {
    return this.size === 'lg';
  }

  // Set the dropdown toggle class when possible
  @computed
  @HostBinding('class.dropdown-toggle')
  public get _hostClassDropdownToggle() {
    return (
      this._tagName !== 'input' &&
      (this.dropdownToggle || this.dropdownToggleSplit)
    );
  }

  // Set type to `'button'` by default (on `button`s and `input`s)
  @computed
  @HostBinding('attr.type')
  public get _hostType() {
    return this._tagName !== 'a' ? this.type || 'button' : undefined;
  }

  @computed
  @HostBinding('attr.disabled')
  public get _hostDisabled() {
    return this._tagName !== 'a' && this.isDisabled ? '' : undefined;
  }
  @computed
  @HostBinding('attr.aria-disabled')
  public get _hostAriaDisabled() {
    return this._tagName !== 'a' && this.isDisabled ? 'true' : undefined;
  }

  @computed
  @HostBinding('attr.aria-pressed')
  public get _hostAriaPressed() {
    return this.isActive ? 'true' : undefined;
  }
}
