import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  Optional,
  ViewChild,
} from '@angular/core';
import { LfI18n, LfRouter, LfStorage, PathBased } from '@lightweightform/core';
import {
  isNullableSchema,
  pathError,
  ValidationMode,
} from '@lightweightform/storage';
import { asap } from '@lightweightform/theme-common';
import { comparer, makeObservable, observe } from 'mobx';
import { action, computed } from 'mobx-angular';
import {
  BsDatepickerConfig,
  BsDaterangepickerDirective,
} from 'ngx-bootstrap/datepicker';

import { InlineValue } from '../../abstract/inline-value';
import {
  I18N_DATE_FORMAT_KEY,
  I18N_DATE_PLACEHOLDER_KEY,
  I18N_DATE_RANGE_SEPARATOR_KEY,
} from '../../i18n/keys';
import { BsLocaleChanger } from '../../services/bs-locale-changer.service';
import { isDateRangeSchema } from '../../utils/date-range-schema';
import { isDate, localDateToUTC } from '../../utils/date-utils';

/**
 * LF date-range component.
 */
@Component({
  selector: 'lf-date-range',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './date-range.component.html',
})
export class DateRangeComponent
  extends InlineValue
  implements OnInit, OnDestroy
{
  /**
   * Date-range picker directive.
   */
  @ViewChild('drp')
  public _bsDateRangePicker: BsDaterangepickerDirective;

  // Disposers
  private disposeBsConfigObserve: () => void;

  constructor(
    @Optional()
    @Inject(PathBased)
    parentPathBasedComponent: PathBased | null,
    lfStorage: LfStorage,
    lfI18n: LfI18n,
    @Optional()
    @Inject(LfRouter)
    lfRouter: LfRouter | null,
    _bsLocaleChanger: BsLocaleChanger
  ) {
    super(parentPathBasedComponent, lfStorage, lfI18n, lfRouter);
    makeObservable(this);
  }

  /**
   * Format for a single date.
   */
  @computed
  public get dateFormat(): string {
    return this.translate(I18N_DATE_FORMAT_KEY);
  }

  /**
   * Separator used between the dates in the date-range.
   */
  @computed
  public get separator(): string {
    return this.translate(I18N_DATE_RANGE_SEPARATOR_KEY) || '-';
  }

  /**
   * Placeholder of the `input` field.
   */
  @computed
  public get placeholder(): string {
    const datePlaceholder = this.translate(I18N_DATE_PLACEHOLDER_KEY);
    return datePlaceholder
      ? `${datePlaceholder}${this.separator}${datePlaceholder}`
      : '';
  }

  /**
   * Whether to show the "is required" icon on the component.
   */
  @computed
  public get showIsRequired(): boolean {
    return (
      (this.relativeStorage.validationMode === ValidationMode.AUTO
        ? this.relativeStorage.getStateProperty('.', 'isRequired')
        : this.relativeStorage.schema().isRequired) === true
    );
  }

  /**
   * Minimum allowed date.
   */
  @computed
  public get minDate(): Date | undefined {
    const minDate =
      this.relativeStorage.validationMode === ValidationMode.AUTO
        ? this.relativeStorage.getStateProperty('.', 'minDate')
        : this.relativeStorage.schema().minDate;
    return isDate(minDate) ? minDate : undefined;
  }

  /**
   * Maximum allowed date.
   */
  @computed
  public get maxDate(): Date | undefined {
    const maxDate =
      this.relativeStorage.validationMode === ValidationMode.AUTO
        ? this.relativeStorage.getStateProperty('.', 'maxDate')
        : this.relativeStorage.schema().maxDate;
    return isDate(maxDate) ? maxDate : undefined;
  }

  /**
   * Configuration to use by Bootstrap datepicker.
   */
  @computed
  public get bsConfig(): Partial<BsDatepickerConfig> {
    return {
      containerClass: 'theme-default',
      rangeInputFormat: this.dateFormat,
      rangeSeparator: this.separator,
    };
  }

  public ngOnInit() {
    super.ngOnInit();

    this.disposeBsConfigObserve = observe(
      this,
      'bsConfig',
      () => {
        asap(() => {
          this._bsDateRangePicker.setConfig();
          this._bsDateRangePicker.bsValueChange.emit(
            this.relativeStorage.has() ? this.relativeStorage.get() : null
          );
        });
      },
      true
    );
  }

  public ngOnDestroy() {
    super.ngOnDestroy();
    this.disposeBsConfigObserve && this.disposeBsConfigObserve();
  }

  /**
   * Action to run whenever the date range is modified via the Bootstrap
   * datepicker UI.
   * @param dateRange Tuple representing the range.
   */
  @action
  public _onDateRangePickerChange(
    dateRange: Array<Date | undefined> | undefined
  ): void {
    // XXX: We convert the dates to UTC due to:
    // https://github.com/valor-software/ngx-bootstrap/issues/3609
    const value: [Date, Date] | null =
      dateRange && dateRange[0] && dateRange[1]
        ? [localDateToUTC(dateRange[0]), localDateToUTC(dateRange[1])]
        : null;

    // FIXME: This function is being called as soon as the component loads, thus
    // the comparison against the current date range (which shouldn't be
    // needed).
    const currentValue = this.relativeStorage.get();
    if (!comparer.structural(currentValue, value)) {
      this.relativeStorage.set('.', value);
      this.relativeStorage.setDirty('.', true);
    }
  }

  public _onFocus(evt: FocusEvent): void {
    super._onFocus(evt);
    this._bsDateRangePicker.show();
  }

  /**
   * Set touched when the date picker is hidden rather than on blur (it is
   * possible to change the value via the popup without actually focusing the
   * input; besides, otherwise the input may go invalid when the user selects
   * the first date of the range, but before the second).
   */
  @action
  public _onDateRangePickerHidden(): void {
    if (!this.relativeStorage.isComputed()) {
      this.relativeStorage.setTouched('.', true);
    }
  }

  protected validatePath(): void {
    const schema = this.relativeStorage.schema();
    if (!isDateRangeSchema(schema) || !isNullableSchema(schema)) {
      throw pathError(
        this.path,
        'Schema must be of the "date-range" type and "nullable"'
      );
    }
  }
}
