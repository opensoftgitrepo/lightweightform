import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  ComponentFixture,
  ComponentFixtureAutoDetect,
  TestBed,
} from '@angular/core/testing';
import {
  LfI18n,
  LfStorage,
  LF_APP_I18N,
  LF_APP_SCHEMA,
} from '@lightweightform/core';
import { MobxAngularModule } from 'mobx-angular';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import {
  DateRangeSchema,
  dateRangeSchema,
} from '../../../utils/date-range-schema';
import { DateRangeComponent } from '../date-range.component';

describe('Date-range component', () => {
  const schema: DateRangeSchema = dateRangeSchema({ isNullable: true });
  let fixture: ComponentFixture<DateRangeComponent>;
  let component: DateRangeComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MobxAngularModule, BsDatepickerModule.forRoot()],
      declarations: [DateRangeComponent],
      providers: [
        { provide: ComponentFixtureAutoDetect, useValue: true },
        { provide: LF_APP_SCHEMA, useValue: schema },
        { provide: LF_APP_I18N, useValue: {} },
        LfStorage,
        LfI18n,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });
    fixture = TestBed.createComponent(DateRangeComponent);
    component = fixture.componentInstance;
  });

  it('is created', () => {
    expect(component).toBeDefined();
  });
});
