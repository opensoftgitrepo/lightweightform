import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  ComponentFixture,
  ComponentFixtureAutoDetect,
  TestBed,
} from '@angular/core/testing';
import {
  LfI18n,
  LfStorage,
  LF_APP_I18N,
  LF_APP_SCHEMA,
} from '@lightweightform/core';
import { dateSchema, DateSchema } from '@lightweightform/storage';
import { MobxAngularModule } from 'mobx-angular';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { DateComponent } from '../date.component';

describe('Date component', () => {
  const schema: DateSchema = dateSchema({ isNullable: true });
  let fixture: ComponentFixture<DateComponent>;
  let component: DateComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MobxAngularModule, BsDatepickerModule.forRoot()],
      declarations: [DateComponent],
      providers: [
        { provide: ComponentFixtureAutoDetect, useValue: true },
        { provide: LF_APP_SCHEMA, useValue: schema },
        { provide: LF_APP_I18N, useValue: {} },
        LfStorage,
        LfI18n,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });
    fixture = TestBed.createComponent(DateComponent);
    component = fixture.componentInstance;
  });

  it('is created', () => {
    expect(component).toBeDefined();
  });
});
