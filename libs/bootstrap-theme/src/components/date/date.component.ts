import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  Optional,
  ViewChild,
} from '@angular/core';
import { LfI18n, LfRouter, LfStorage, PathBased } from '@lightweightform/core';
import {
  isDateSchema,
  isNullableSchema,
  pathError,
  ValidationMode,
} from '@lightweightform/storage';
import { asap } from '@lightweightform/theme-common';
import { comparer, makeObservable, observe } from 'mobx';
import { action, computed } from 'mobx-angular';
import {
  BsDatepickerConfig,
  BsDatepickerDirective,
} from 'ngx-bootstrap/datepicker';

import { InlineValue } from '../../abstract/inline-value';
import {
  I18N_DATE_FORMAT_KEY,
  I18N_DATE_PLACEHOLDER_KEY,
} from '../../i18n/keys';
import { BsLocaleChanger } from '../../services/bs-locale-changer.service';
import { isDate, localDateToUTC } from '../../utils/date-utils';

/**
 * LF date component.
 */
@Component({
  selector: 'lf-date',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './date.component.html',
})
export class DateComponent extends InlineValue implements OnInit, OnDestroy {
  /**
   * Datepicker directive.
   */
  @ViewChild('dp')
  public _bsDatePicker: BsDatepickerDirective;

  // Disposers
  private disposeBsConfigObserve: () => void;

  constructor(
    @Optional()
    @Inject(PathBased)
    parentPathBasedComponent: PathBased | null,
    lfStorage: LfStorage,
    lfI18n: LfI18n,
    @Optional()
    @Inject(LfRouter)
    lfRouter: LfRouter | null,
    _bsLocaleChanger: BsLocaleChanger
  ) {
    super(parentPathBasedComponent, lfStorage, lfI18n, lfRouter);
    makeObservable(this);
  }

  /**
   * Format of the date.
   */
  @computed
  public get dateFormat(): string {
    return this.translate(I18N_DATE_FORMAT_KEY) || 'YYYY-MM-DD';
  }

  /**
   * Placeholder of the `input` field.
   */
  @computed
  public get placeholder(): string {
    return this.translate(I18N_DATE_PLACEHOLDER_KEY) || '';
  }

  /**
   * Whether to show the "is required" icon on the component.
   */
  @computed
  public get showIsRequired(): boolean {
    return (
      (this.relativeStorage.validationMode === ValidationMode.AUTO
        ? this.relativeStorage.getStateProperty('.', 'isRequired')
        : this.relativeStorage.schema().isRequired) === true
    );
  }

  /**
   * Minimum allowed date.
   */
  @computed
  public get minDate(): Date | undefined {
    const minDate =
      this.relativeStorage.validationMode === ValidationMode.AUTO
        ? this.relativeStorage.getStateProperty('.', 'minDate')
        : this.relativeStorage.schema().minDate;
    return isDate(minDate) ? minDate : undefined;
  }

  /**
   * Maximum allowed date.
   */
  @computed
  public get maxDate(): Date | undefined {
    const maxDate =
      this.relativeStorage.validationMode === ValidationMode.AUTO
        ? this.relativeStorage.getStateProperty('.', 'maxDate')
        : this.relativeStorage.schema().maxDate;
    return isDate(maxDate) ? maxDate : undefined;
  }

  /**
   * Config for the `BsDatepicker`.
   */
  @computed
  public get bsConfig(): Partial<BsDatepickerConfig> {
    return {
      containerClass: 'theme-default',
      dateInputFormat: this.dateFormat,
    };
  }

  public ngOnInit(): void {
    super.ngOnInit();

    this.disposeBsConfigObserve = observe(
      this,
      'bsConfig',
      () => {
        asap(() => {
          this._bsDatePicker.setConfig();
          this._bsDatePicker.bsValueChange.emit(
            this.relativeStorage.has() ? this.relativeStorage.get() : null
          );
        });
      },
      true
    );
  }

  public ngOnDestroy(): void {
    super.ngOnDestroy();
    this.disposeBsConfigObserve && this.disposeBsConfigObserve();
  }

  /**
   * Action to run whenever the date `input` changes.
   * @param date Date to set.
   */
  @action
  public _onDatePickerChange(date: Date | null): void {
    // XXX: We convert the date to UTC due to:
    // https://github.com/valor-software/ngx-bootstrap/issues/3609
    date = !date || isNaN(+date) ? null : localDateToUTC(date);
    // FIXME: This function is being called as soon as the component loads, thus
    // the comparison against the current date (which shouldn't be needed).
    const currentDate = this.relativeStorage.get();
    if (!comparer.structural(currentDate, date)) {
      this.relativeStorage.set('.', date);
      this.relativeStorage.setDirty();
    }
  }

  public _onFocus(evt: FocusEvent): void {
    super._onFocus(evt);
    this._bsDatePicker.show();
  }

  /**
   * Set touched when the date picker is hidden rather than on blur (it is
   * possible to change the value via the popup without actually focusing the
   * input).
   */
  @action
  public _onDatePickerHidden(): void {
    if (!this.relativeStorage.isComputed()) {
      this.relativeStorage.setTouched();
    }
  }

  protected validatePath(): void {
    const schema = this.relativeStorage.schema();
    if (!isDateSchema(schema) || !isNullableSchema(schema)) {
      throw pathError(
        this.path,
        'Schema must be of the "date" type and "nullable"'
      );
    }
  }
}
