import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Optional,
} from '@angular/core';
import { LfI18n, LfRouter, LfStorage, PathBased } from '@lightweightform/core';
import { makeObservable } from 'mobx';
import { action } from 'mobx-angular';

import { SingleSelectionValue } from '../../abstract/single-selection-value';

/**
 * Component used for selecting from a list of options in a dropdown `<select>`
 * menu.
 */
@Component({
  selector: 'lf-select',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './select.component.html',
})
export class SelectComponent extends SingleSelectionValue {
  constructor(
    @Optional()
    @Inject(PathBased)
    parentPathBasedComponent: PathBased | null,
    lfStorage: LfStorage,
    lfI18n: LfI18n,
    @Optional()
    @Inject(LfRouter)
    lfRouter: LfRouter | null
  ) {
    super(parentPathBasedComponent, lfStorage, lfI18n, lfRouter);
    makeObservable(this);
  }

  /**
   * Action to run whenever the `<select>` values changes.
   * @param evt `change` event.
   */
  @action
  public _onSelectChange(evt) {
    this.relativeStorage.set('.', this.options[+evt.target.value].value);
    this.relativeStorage.setDirty();
  }
}
