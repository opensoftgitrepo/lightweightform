import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  ComponentFixture,
  ComponentFixtureAutoDetect,
  TestBed,
} from '@angular/core/testing';
import {
  LfI18n,
  LfStorage,
  LF_APP_I18N,
  LF_APP_SCHEMA,
} from '@lightweightform/core';
import { Schema, stringSchema } from '@lightweightform/storage';
import { MobxAngularModule } from 'mobx-angular';

import { SelectComponent } from '../select.component';

describe('Select component', () => {
  const schema: Schema = stringSchema();
  let fixture: ComponentFixture<SelectComponent>;
  let component: SelectComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MobxAngularModule],
      declarations: [SelectComponent],
      providers: [
        { provide: ComponentFixtureAutoDetect, useValue: true },
        { provide: LF_APP_SCHEMA, useValue: schema },
        { provide: LF_APP_I18N, useValue: {} },
        LfStorage,
        LfI18n,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });
    fixture = TestBed.createComponent(SelectComponent);
    component = fixture.componentInstance;
  });

  it('is created', () => {
    expect(component).toBeDefined();
  });
});
