import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Inject,
  OnDestroy,
  OnInit,
  Optional,
  SkipSelf,
  ViewChild,
} from '@angular/core';
import { LfI18n, LfRouter, LfStorage, PathBased } from '@lightweightform/core';
import {
  isNullableSchema,
  isRecordSchema,
  pathError,
  ValidationMode,
} from '@lightweightform/storage';
import { makeObservable, observe } from 'mobx';
import { action, computed, observable } from 'mobx-angular';

import { Value } from '../../abstract/value';
import { ValidationPanelComponent } from '../validation-panel/validation-panel.component';

/**
 * Lf app component.
 */
@Component({
  selector: 'lf-app',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{ provide: PathBased, useExisting: AppComponent }],
  templateUrl: './app.component.html',
})
export class AppComponent
  extends Value
  implements OnInit, AfterViewInit, OnDestroy
{
  @ViewChild(ValidationPanelComponent)
  public validationPanel: ValidationPanelComponent;
  @ViewChild('treeNavContainer')
  public _treeNavContainer: ElementRef;

  @ViewChild('mainContainer')
  public _mainContainer: ElementRef;
  @ViewChild('mainContent')
  public _mainContent: ElementRef;

  // State of the tree nav visibility
  @observable public _isTreeNavVisible = false;
  // Whether the backdrop element should be in visible or invisible state
  @observable public _isBackdropVisible = false;
  // Whether the backdrop element should exist
  @observable public _shouldBackdropExist = false;

  // If the state is toggled too fast then the `translateX` value won't differ
  // from the initial one, meaning that a transition never occurs and
  // `transitionend` is never called. We keep track of the `translateX` values
  // and run `_hidden` "manually" when necessary
  private lastTreeNavTranslateX: number;

  private disposeOnRouterPathChange: () => void;
  private onPressEsc: (evt: KeyboardEvent) => void;

  constructor(
    @Optional()
    @SkipSelf()
    @Inject(PathBased)
    parentPathBasedComponent: PathBased | null,
    lfStorage: LfStorage,
    lfI18n: LfI18n,
    @Optional()
    @Inject(LfRouter)
    private lfRouter: LfRouter | null
  ) {
    super(parentPathBasedComponent, lfStorage, lfI18n);
    makeObservable(this);
  }

  @computed
  public get isTreeNavVisible() {
    return this._isTreeNavVisible;
  }

  // Current `translateX` value of the tree nav
  private get treeNavTranslateX(): number {
    // Obtain `tx` from `'matrix(a, b, c, d, tx, ty)'`
    return +window
      .getComputedStyle(this._treeNavContainer.nativeElement)
      .getPropertyValue('transform')
      .split(',')[4];
  }

  public ngOnInit() {
    if (this._path === undefined) {
      this._path = '/';
    }
    super.ngOnInit();
  }

  public ngAfterViewInit() {
    // Hide tree nav when `Escape` is pressed
    this.onPressEsc = (evt) => {
      if (this.isTreeNavVisible && evt.key === 'Escape') {
        this.hideTreeNav();
      }
    };
    window.addEventListener('keyup', this.onPressEsc);

    // Scroll viewport to the top whenever the router path changes
    if (this.lfRouter !== null) {
      this.disposeOnRouterPathChange = observe(this.lfRouter, 'path', () => {
        this.scrollTo({ top: 0, left: 0 });
      });
    }
  }

  public ngOnDestroy() {
    this.onPressEsc && window.removeEventListener('keyup', this.onPressEsc);
    this.disposeOnRouterPathChange && this.disposeOnRouterPathChange();
  }

  @action
  public showTreeNav() {
    if (!this._isTreeNavVisible) {
      this._isTreeNavVisible = true;
      this._isBackdropVisible = true;
      this._shouldBackdropExist = true;
      this.lastTreeNavTranslateX = this.treeNavTranslateX;
    }
  }

  @action
  public hideTreeNav() {
    if (this._isTreeNavVisible) {
      this._isTreeNavVisible = false;
      // When there was no time for an animation and `transitionend` isn't called
      if (this.lastTreeNavTranslateX === this.treeNavTranslateX) {
        this._onTreeNavTransitionEnd();
      }
    }
  }

  @action
  public validate() {
    this.relativeStorage.setTouched('.', true);
    if (this.lfStorage.validationMode === ValidationMode.MANUAL) {
      this.lfStorage.validate();
    }
    this.validationPanel.show();
  }

  /**
   * Scroll the main content area.
   * @param options Options for the scrolling.
   */
  public scrollTo(options: ScrollToOptions): void {
    // Depending on whether we're in mobile or desktop mode, the scrollable
    // element differs
    this._mainContainer.nativeElement.scrollTo(options);
    this._mainContent.nativeElement.scrollTo(options);
  }

  // Action to run after the tree nav finishes hiding (hide backdrop)
  public _onTreeNavTransitionEnd() {
    if (!this.isTreeNavVisible && this._isBackdropVisible) {
      action('hideTreeNavBackdrop', () => (this._isBackdropVisible = false))();
    }
  }

  // Action to run after the backdrop finishes hiding (delete backdrop)
  public _onBackdropTransitionEnd() {
    action(
      'removeTreeNavBackdrop',
      () => (this._shouldBackdropExist = false)
    )();
  }

  protected validatePath(): void {
    const schema = this.relativeStorage.schema();
    if (!isRecordSchema(schema) || isNullableSchema(schema)) {
      throw pathError(
        this.path,
        'Schema must be of the "record" type and not "nullable"'
      );
    }
  }
}
