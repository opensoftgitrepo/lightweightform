import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Input,
  Optional,
} from '@angular/core';
import {
  LfFileStorage,
  LfI18n,
  LfRouter,
  LfStorage,
} from '@lightweightform/core';
import { makeObservable } from 'mobx';
import { action, computed, observable } from 'mobx-angular';

import { I18N_VALUE_RETRY_VALIDATE_TEXT_KEY } from '../../../i18n/keys';
import { ActionsMenuAction } from '../../actions-menu/actions-menu.component';
import { AppComponent } from '../app.component';

/**
 * LF component used to display the "top-bar" of an application.
 */
@Component({
  selector: 'lf-top-bar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './top-bar.component.html',
})
export class TopBarComponent {
  @observable.struct
  @Input()
  public actions: ActionsMenuAction[];

  constructor(
    private lfStorage: LfStorage,
    private lfRouter: LfRouter,
    @Inject(LfI18n) private lfI18n: LfI18n,
    @Optional() @Inject(LfFileStorage) private lfFileStorage: LfFileStorage,
    @Inject(AppComponent) protected _appComponent: AppComponent
  ) {
    makeObservable(this);
  }

  @computed
  public get retryValidationText(): string {
    return this.lfI18n.get(I18N_VALUE_RETRY_VALIDATE_TEXT_KEY);
  }

  @computed
  public get rootUrl(): string {
    try {
      return this.lfRouter.createUrl('/');
    } catch (err) {
      return '/';
    }
  }

  @computed
  public get appIsPending(): boolean {
    return (
      this.lfStorage.shouldShowPending() ||
      (this.lfFileStorage && this.lfFileStorage.isLoading)
    );
  }

  @computed
  public get shouldShowAppRecompute(): boolean {
    return !!this.lfStorage.shouldShowRejected();
  }

  @computed
  public get isTreeNavVisible(): boolean {
    return this._appComponent.isTreeNavVisible;
  }

  @action
  public recomputeAppStateProperties(): void {
    this.lfStorage.recomputeStateProperties();
  }

  @action
  public showTreeNav(): void {
    this._appComponent.showTreeNav();
  }
}
