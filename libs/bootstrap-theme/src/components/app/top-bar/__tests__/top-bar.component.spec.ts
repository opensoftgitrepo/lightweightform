import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  ComponentFixture,
  ComponentFixtureAutoDetect,
  TestBed,
} from '@angular/core/testing';
import {
  LfI18n,
  LfRouter,
  LfStorage,
  LF_APP_I18N,
  LF_APP_SCHEMA,
} from '@lightweightform/core';
import { recordSchema, Schema } from '@lightweightform/storage';
import { MobxAngularModule } from 'mobx-angular';

import { AppComponent } from '../../app.component';
import { TopBarComponent } from '../top-bar.component';

describe('Top-bar component', () => {
  const schema: Schema = recordSchema({});
  let fixture: ComponentFixture<TopBarComponent>;
  let component: TopBarComponent;

  const mockAppComponent = {
    // TODO
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MobxAngularModule],
      declarations: [TopBarComponent],
      providers: [
        { provide: ComponentFixtureAutoDetect, useValue: true },
        { provide: LF_APP_SCHEMA, useValue: schema },
        { provide: LF_APP_I18N, useValue: {} },
        LfStorage,
        LfI18n,
        { provide: LfRouter, useValue: { createUrl: () => '/' } },
        { provide: AppComponent, useValue: mockAppComponent },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });
    fixture = TestBed.createComponent(TopBarComponent);
    component = fixture.componentInstance;
  });

  it('is created', () => {
    expect(component).toBeDefined();
  });
});
