import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Inject,
  OnInit,
  Optional,
  SkipSelf,
} from '@angular/core';
import { LfI18n, LfStorage, PathBased } from '@lightweightform/core';
import { pathError } from '@lightweightform/storage';
import { makeObservable } from 'mobx';
import { computed } from 'mobx-angular';

import { Value } from '../../../abstract/value';
import { TableComponent } from '../table.component';

/**
 * LF table-row component.
 */
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'tr[lf-table-row]',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{ provide: PathBased, useExisting: TableRowComponent }],
  templateUrl: './table-row.component.html',
})
export class TableRowComponent extends Value implements OnInit {
  constructor(
    @Optional()
    @SkipSelf()
    @Inject(PathBased)
    protected parentPathBasedComponent: TableComponent, // Verified `OnInit`
    lfStorage: LfStorage,
    lfI18n: LfI18n
  ) {
    super(parentPathBasedComponent, lfStorage, lfI18n);
    makeObservable(this);
  }

  /**
   * Whether the row can be removed.
   */
  @computed
  public get allowsRemoval(): boolean {
    return this.parentPathBasedComponent.allowsRowRemoval;
  }

  /**
   * Whether the row is selected (adds an `is-selected` class to the row).
   */
  @computed
  @HostBinding('class.is-selected')
  public get isSelected(): boolean {
    return this.parentPathBasedComponent.getRowSelectedStatus(
      this.relativeStorage.currentId!
    );
  }

  /**
   * Class that should be added to the row when the row should show as having an
   * error.
   */
  @computed
  @HostBinding('class.is-invalid')
  public get _classIsInvalid(): boolean {
    return !!(
      this.relativeStorage.has() && this.relativeStorage.shouldShowError()
    );
  }

  /**
   * Class that should be added to the row when the row should show as having a
   * warning.
   */
  @computed
  @HostBinding('class.is-warning')
  public get _classIsWarning(): boolean {
    return !!(
      this.relativeStorage.has() && this.relativeStorage.shouldShowWarning()
    );
  }

  public ngOnInit() {
    super.ngOnInit();
    // Table rows must be children of tables
    if (!(this.parentPathBasedComponent instanceof TableComponent)) {
      throw pathError(
        this.path,
        'This component must be a child of a `TableComponent`'
      );
    }
  }

  /**
   * Sets the row's selection status.
   * @param selectedStatus `true` to set selected, `false` to set unselected.
   */
  public setSelectedStatus(selectedStatus: boolean): void {
    this.parentPathBasedComponent.setRowSelectedStatus(
      this.relativeStorage.currentId!,
      selectedStatus
    );
  }

  /**
   * Sets the row selection status of all rows from the last row that has had
   * its selection status set to this row.
   * @param selectedStatus `true` to set selected, `false` to set unselected.
   */
  public setSelectedStatusFromLastSelected(selectedStatus: boolean): void {
    this.parentPathBasedComponent.setRowSelectedStatusFromLastSelected(
      this.relativeStorage.currentId!,
      selectedStatus
    );
  }

  /**
   * Action to run when the selection checkbox label is clicked: added as a fix
   * for Firefox, where `shift+click` on the label won't trigger a click on the
   * `input`; as such we manually call the event.
   * @param event Click event.
   * @param checkboxElement Element on which the even should be called instead.
   */
  public _onLabelClick(event: any, checkboxElement: HTMLElement): void {
    this._onSelectionCheckboxClick(event);
    checkboxElement.focus();
    event.preventDefault();
  }

  /**
   * Action to run when the selection checkbox is clicked.
   * @param event Click event.
   */
  public _onSelectionCheckboxClick(event: any): void {
    if (event.shiftKey) {
      this.setSelectedStatusFromLastSelected(!this.isSelected);
    } else {
      this.setSelectedStatus(!this.isSelected);
    }
    event.stopPropagation(); // Don't call the label event
  }
}
