import { ScrollingModule } from '@angular/cdk/scrolling';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  ComponentFixture,
  ComponentFixtureAutoDetect,
  TestBed,
} from '@angular/core/testing';
import {
  LfI18n,
  LfStorage,
  LF_APP_I18N,
  LF_APP_SCHEMA,
} from '@lightweightform/core';
import { ListSchema, listSchema, stringSchema } from '@lightweightform/storage';
import { MobxAngularModule } from 'mobx-angular';

import { TableCellComponent } from '../table-cell/table-cell.component';
import { TableRowComponent } from '../table-row/table-row.component';
import { TableComponent } from '../table.component';

describe('Table component', () => {
  const schema: ListSchema = listSchema(stringSchema());
  let fixture: ComponentFixture<TableComponent>;
  let component: TableComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MobxAngularModule, ScrollingModule],
      declarations: [TableComponent, TableRowComponent, TableCellComponent],
      providers: [
        { provide: ComponentFixtureAutoDetect, useValue: true },
        { provide: LF_APP_SCHEMA, useValue: schema },
        { provide: LF_APP_I18N, useValue: {} },
        LfStorage,
        LfI18n,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });
    fixture = TestBed.createComponent(TableComponent);
    component = fixture.componentInstance;
  });

  it('is created', () => {
    expect(component).toBeDefined();
  });
});
