import { ChangeDetectionStrategy, Component, HostBinding } from '@angular/core';
import { makeObservable } from 'mobx';
import { computed } from 'mobx-angular';

import { TableComponent } from '../table.component';

/**
 * LF table-cell component.
 */
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'td[lf-table-cell], th[lf-table-cell]',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './table-cell.component.html',
})
export class TableCellComponent {
  constructor(private parentTableComponent: TableComponent) {
    makeObservable(this);
  }

  /**
   * Height of the cell.
   */
  @computed
  @HostBinding('style.height')
  public get _hostStyleHeight(): string {
    return this.parentTableComponent.rowHeight + 'px';
  }
}
