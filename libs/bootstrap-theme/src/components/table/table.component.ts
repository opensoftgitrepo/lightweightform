import {
  CdkVirtualScrollViewport,
  ViewportRuler,
} from '@angular/cdk/scrolling';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  ElementRef,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  Optional,
  SkipSelf,
  TemplateRef,
  TrackByFunction,
  ViewChild,
} from '@angular/core';
import { LfI18n, LfRouter, LfStorage, PathBased } from '@lightweightform/core';
import {
  appendToPath,
  getRowId,
  Identifier,
  isListSchema,
  isNullableSchema,
  isTableSchema,
  ListSchema,
  pathError,
  TableSchema,
  ValidationMode,
} from '@lightweightform/storage';
import {
  asap,
  ScrollbarUtils,
  TableHeaderDirective,
} from '@lightweightform/theme-common';
import { comparer, makeObservable, observe, reaction } from 'mobx';
import { action, computed, observable } from 'mobx-angular';
import { animationFrameScheduler, asapScheduler, Subscription } from 'rxjs';
import { auditTime } from 'rxjs/operators';

import { InlineValue } from '../../abstract/inline-value';
import {
  I18N_TABLE_ADD_ROW_ACTION_TEXT_KEY,
  I18N_TABLE_COLUMN_LABELS_KEY,
  I18N_TABLE_NO_ROWS_TEXT_KEY,
  I18N_TABLE_REMOVE_ROWS_ACTION_TEXT_KEY,
} from '../../i18n/keys';
import { ActionsMenuAction } from '../actions-menu/actions-menu.component';

/**
 * Width of the selection column (in `px`).
 */
export const SELECTION_COLUMN_WIDTH = 25.6;

/**
 * LF table component.
 *
 * Example usage:
 * ```html
 * <lf-table path="/path/to/table">
 *   <lf-table-header>
 *     <lf-table-column id="field1"></lf-table-column>
 *     <lf-table-column id="field2"></lf-table-column>
 *   </lf-table-header>
 *
 *   <ng-template #lfTableRowTemplate let-id="id">
 *     <tr lf-table-row [path]="id">
 *       <td lf-table-cell><lf-text path="field1"></lf-text></td>
 *       <td lf-table-cell><lf-number path="field2"></lf-number></td>
 *     </tr>
 *   </ng-template>
 * </lf-table>
 * ```
 */
@Component({
  selector: 'lf-table',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{ provide: PathBased, useExisting: TableComponent }],
  templateUrl: './table.component.html',
})
export class TableComponent
  extends InlineValue
  implements OnInit, AfterViewInit, OnDestroy
{
  /**
   * Height (in `px`) of a single row (virtual scroll settings). All rows must
   * have the same height.
   */
  @observable
  @Input()
  public rowHeight = 42;
  /**
   * Maximum number of visible rows at any given time.
   */
  @observable
  @Input()
  public maxVisibleRows = 10;
  /**
   * Size of the virtual scroll's template cache
   * (`cdkVirtualForTemplateCacheSize` input of Angular CDK's virtual for
   * directive).
   */
  @Input()
  public virtualScrollTemplateCacheSize = 20;

  /**
   * Whether to allow addition of new rows.
   */
  @observable
  @Input()
  public allowRowAddition = true;
  /**
   * Whether to allow removal of rows.
   */
  @observable
  @Input()
  public allowRowRemoval = true;

  // Table header
  @ContentChild(TableHeaderDirective)
  public _tableHeader: TableHeaderDirective;
  // Templates
  @ContentChild('lfTableRowTemplate')
  public _rowTemplate: TemplateRef<any>;
  @ContentChild('lfTableNoRowsTemplate')
  public _noRowsTemplate: TemplateRef<any>;

  // Scrollable elements
  @observable
  @ViewChild('scrollableHeader')
  public _scrollableHeader?: ElementRef;
  @ViewChild('scrollableBody')
  @observable
  public _scrollableBody?: CdkVirtualScrollViewport;

  // Whether the table's body has horizontal overflow
  @observable private hasHorizontalOverflow: boolean;

  // Selected rows
  @observable private selectedRows: Map<Identifier, boolean> = new Map();
  // Identifier of the row that was last selected/unselected; used to implement
  // selections while pressing shift (behaviour similar to how Gmail operates)
  private lastSelectedId: Identifier | null = null;
  // Position of the scrollable element after the latest scroll
  private lastScrollPosition: [number, number] = [0, 0];

  // Disposers/subscriptions
  private disposeObserveScrollables: () => void;
  private scrollSubscription: Subscription;
  private disposeObservePath: () => void;
  private disposeObserveValue: () => void;
  private disposeObserveBodyHeight: () => void;
  private viewportSubscription: Subscription;

  constructor(
    @Optional()
    @SkipSelf()
    @Inject(PathBased)
    parentPathBasedComponent: PathBased | null,
    lfStorage: LfStorage,
    lfI18n: LfI18n,
    @Optional()
    @Inject(LfRouter)
    lfRouter: LfRouter | null,
    private scrollbarUtils: ScrollbarUtils,
    private viewportRuler: ViewportRuler
  ) {
    super(parentPathBasedComponent, lfStorage, lfI18n, lfRouter);
    makeObservable(this);
  }

  @computed
  public get _addRowText(): string {
    return this.translate(I18N_TABLE_ADD_ROW_ACTION_TEXT_KEY);
  }

  @computed
  public get _removeRowsText(): string {
    return this.translate(I18N_TABLE_REMOVE_ROWS_ACTION_TEXT_KEY);
  }

  @computed
  public get _noRowsText(): string {
    return this.translate(I18N_TABLE_NO_ROWS_TEXT_KEY);
  }

  @computed
  public get _columnLabels(): string[] {
    return this.translate(I18N_TABLE_COLUMN_LABELS_KEY);
  }

  /**
   * Schema of this table's value.
   */
  @computed
  public get _schema(): ListSchema | TableSchema {
    return this.relativeStorage.schema() as ListSchema | TableSchema;
  }

  /**
   * Table's value.
   */
  @computed
  public get _value(): any[] {
    return this.relativeStorage.get().slice();
  }

  /**
   * Whether to show the "is required" icon on the component.
   */
  @computed
  public get _showIsRequired(): boolean {
    const minSize =
      this.relativeStorage.validationMode === ValidationMode.AUTO
        ? this.relativeStorage.getStateProperty('.', 'minSize')
        : this.relativeStorage.schema().minSize;
    return (typeof minSize === 'number' ? minSize : 0) > 0;
  }

  /**
   * Table actions.
   */
  @computed.struct
  public get _tableActions(): ActionsMenuAction[] {
    const actions: ActionsMenuAction[] = [];
    // Remove rows action
    if (this.allowsRowRemoval && !this.noRowsSelected) {
      actions.push({
        text: this._removeRowsText,
        class: 'remove-rows',
        style: 'outline-danger',
        icon: 'trash',
        callback: () => this._onClickRemoveRows(),
      });
    }
    // New row action
    const maxSize =
      this.relativeStorage.validationMode === ValidationMode.AUTO
        ? this.relativeStorage.getStateProperty('.', 'maxSize')
        : this.relativeStorage.schema().maxSize;
    if (
      this.allowsRowAddition &&
      (typeof maxSize !== 'number' || this._value.length < maxSize)
    ) {
      actions.push({
        text: this._addRowText,
        class: 'add-row',
        style: 'outline-primary',
        icon: 'plus-circle',
        callback: () => this._onClickNewRow(),
      });
    }
    return actions;
  }

  /**
   * Height (in px) that the scrollable body element should have.
   */
  @computed
  public get _scrollableBodyHeight(): number {
    return (
      Math.min(this._value.length, this.maxVisibleRows) * this.rowHeight +
      this.horizontalScrollbarHeight
    );
  }

  /**
   * Whether new rows can be added to the table (takes into consideration if the
   * table is in "read only" mode).
   */
  @computed
  public get allowsRowAddition(): boolean {
    return !this.isReadOnly && this.allowRowAddition;
  }

  /**
   * Whether rows can be removed from the table (takes into consideration if the
   * table is in "read only" mode).
   */
  @computed
  public get allowsRowRemoval(): boolean {
    return !this.isReadOnly && this.allowRowRemoval;
  }

  /**
   * Number of rows that have been selected.
   */
  @computed
  public get numberOfRowsSelected(): number {
    return this.selectedRows.size;
  }

  /**
   * Whether all rows are selected.
   */
  @computed
  public get allRowsSelected(): boolean {
    return this.selectedRows.size === this._value.length;
  }

  /**
   * Whether no rows are selected.
   */
  @computed
  public get noRowsSelected(): boolean {
    return this.selectedRows.size === 0;
  }

  /**
   * Track either by index or reference, depending on the type of value for list
   * schemas; or by row id, when dealing with table schemas.
   */
  @computed
  public get _trackBy(): TrackByFunction<any> {
    if (isListSchema(this._schema)) {
      const elementsType = this._schema.elementsSchema.type;
      return ['boolean', 'number', 'string'].indexOf(elementsType) === -1
        ? (_, el) => el
        : (i) => i;
    } else {
      return (_, row) => getRowId(row);
    }
  }

  /**
   * Width (in px) of the vertical scroll bar, when existent (`0` otherwise).
   */
  @computed
  private get verticalScrollbarWidth(): number {
    return this._value.length > this.maxVisibleRows
      ? this.scrollbarUtils.verticalScrollbarWidth
      : 0;
  }

  /**
   * Height (in px) of the horizontal scroll bar, when existent (`0` otherwise).
   */
  @computed
  private get horizontalScrollbarHeight(): number {
    return this.hasHorizontalOverflow
      ? this.scrollbarUtils.horizontalScrollbarHeight
      : 0;
  }

  public ngAfterViewInit() {
    super.ngAfterViewInit();

    this.disposeObserveScrollables = reaction(
      () => [this._scrollableBody, this._scrollableHeader] as const,
      ([scrollableBody, scrollableHeader]) => {
        this.scrollSubscription?.unsubscribe();
        if (scrollableBody && scrollableHeader) {
          // React to scrolling on the scrollable body. Adapted from:
          // https://github.com/angular/components/blob/master/src/cdk/scrolling/virtual-scroll-viewport.ts
          const scrollableBodyEl = scrollableBody.elementRef.nativeElement;
          const scrollableHeaderEl = scrollableHeader?.nativeElement;
          this.scrollSubscription = scrollableBody
            .elementScrolled()
            .pipe(
              auditTime(
                0,
                typeof requestAnimationFrame !== 'undefined'
                  ? animationFrameScheduler
                  : asapScheduler
              )
            )
            .subscribe(() => {
              // Save last scroll position to restore scroll position when
              // needed
              this.lastScrollPosition = [
                scrollableBodyEl.scrollTop,
                scrollableBodyEl.scrollLeft,
              ];
              // Sync header scroll with body scroll
              scrollableHeaderEl &&
                (scrollableHeaderEl.scrollLeft = scrollableBodyEl.scrollLeft);
            });
        }
      }
    );

    // Set the table's scroll position whenever the path changes (and when the
    // component is first initialised)
    this.disposeObservePath = observe(
      this,
      'path',
      ({ oldValue, newValue }: any) => {
        // Save old scroll value
        if (oldValue) {
          this.lfStorage.setStateProperty(
            oldValue,
            this.scrollPositionStatePropName(oldValue),
            this.lastScrollPosition
          );
        }
        // Use previous scroll values
        const savedScrollPosition = this.lfStorage.getStateProperty(
          newValue,
          this.scrollPositionStatePropName(newValue)
        ) || [0, 0];
        if (!comparer.shallow(this.lastScrollPosition, savedScrollPosition)) {
          this.lastScrollPosition = savedScrollPosition;
          asap(() =>
            this._scrollableBody?.scrollTo({
              top: savedScrollPosition[0],
              left: savedScrollPosition[1],
            })
          );
        }
      },
      true
    );

    // Remove "selected rows" of non-existent rows (because they were removed
    // by an outside operation, such as loading from a file)
    this.disposeObserveValue = observe(this, '_value', () => {
      for (const id of Array.from(this.selectedRows.keys())) {
        if (!this.relativeStorage.has(`${id}`)) {
          this.selectedRows.delete(id);
        }
      }
    });

    // Refresh the virtual scroll viewport whenever its height changes
    this.disposeObserveBodyHeight = observe(
      this,
      '_scrollableBodyHeight',
      () => {
        this._scrollableBody?.checkViewportSize();
        this.checkHasHorizontalOverflow();
      }
    );

    // Update the status of the horizontal overflow whenever the window viewport
    // size changes
    this.viewportSubscription = this.viewportRuler.change().subscribe(() => {
      this.checkHasHorizontalOverflow();
    });
    asap(() => this.checkHasHorizontalOverflow()); // Update on start-up
  }

  public ngOnDestroy() {
    super.ngOnDestroy();
    // Save scroll value
    this.lfStorage.setStateProperty(
      this.path,
      this.scrollPositionStatePropName(this.path),
      this.lastScrollPosition
    );
    this.disposeObserveScrollables?.();
    this.scrollSubscription?.unsubscribe();
    this.disposeObservePath?.();
    this.disposeObserveValue?.();
    this.disposeObserveBodyHeight?.();
    this.viewportSubscription?.unsubscribe();
  }

  public _columnLabel(columnId: string): string {
    return this._columnLabels && this._columnLabels[columnId];
  }

  /**
   * Minimum width (in px) that the table should have.
   * @param includeScrollbarColumn Whether the scrollbar column should be
   * included in the computation (should be set to `true` for the header table).
   * @returns Minimum width that the table should have.
   */
  public _tableMinWidth(includeScrollbarColumn: boolean): number {
    return (
      this._tableHeader.tableMinWidth +
      (this.allowsRowRemoval ? SELECTION_COLUMN_WIDTH : 0) +
      (includeScrollbarColumn ? this.verticalScrollbarWidth : 0)
    );
  }

  /**
   * Maximum width (in px) that the table should have.
   * @param includeScrollbarColumn Whether the scrollbar column should be
   * included in the computation (should be set to `true` for the header table).
   * @returns Maximum width that the table should have.
   */
  public _tableMaxWidth(includeScrollbarColumn: boolean): number {
    return (
      this._tableHeader.tableMaxWidth +
      (this.allowsRowRemoval ? SELECTION_COLUMN_WIDTH : 0) +
      (includeScrollbarColumn ? this.verticalScrollbarWidth : 0)
    );
  }

  /**
   * Widths (as strings with correct units) of each column of the table.
   * @param includeScrollbarColumn Whether the scrollbar column should be
   * included (should be set to `true` for the header table).
   * @returns Widths of each column of the table.
   */
  public _columnWidths(includeScrollbarColumn: boolean): string[] {
    return (
      this.allowsRowRemoval ? [SELECTION_COLUMN_WIDTH + 'px'] : []
    ).concat(
      this._tableHeader.columnWidths.concat(
        includeScrollbarColumn ? [this.verticalScrollbarWidth + 'px'] : []
      )
    );
  }

  /**
   * Adds a new row to the table.
   */
  @action
  public addNewRow(): void {
    this.relativeStorage.push('.', undefined);
    // Scroll to bottom (shows newly inserted row)
    asap(() => this.scrollToIndex(this._value.length - 1));
  }

  /**
   * Gets the selection status of the row with the provided id.
   * @param id Id of the row for which to check selection status.
   * @returns Whether the row with the provided id is selected.
   */
  public getRowSelectedStatus(id: Identifier): boolean {
    return this.allowsRowRemoval && this.selectedRows.has(id);
  }

  /**
   * Sets the selection status of the row with the provided id.
   * @param id Id of the row on which to set selection status.
   * @param selectedStatus `true` to set selected, `false` to set unselected.
   */
  @action
  public setRowSelectedStatus(id: Identifier, selectedStatus: boolean): void {
    if (!this.relativeStorage.has(`${id}`)) {
      throw pathError(this.path, `Invalid id: ${id}`);
    }
    if (selectedStatus) {
      this.selectedRows.set(id, true);
    } else {
      this.selectedRows.delete(id);
    }
    this.lastSelectedId = id;
  }

  /**
   * Sets the selection status of all rows from the last row that has had its
   * selection status set to the row with the provided id.
   * @param id Id of the row until which to set selection status.
   * @param selectedStatus `true` to set selected, `false` to set unselected.
   */
  @action
  public setRowSelectedStatusFromLastSelected(
    id: Identifier,
    selectedStatus: boolean
  ): void {
    if (!this.relativeStorage.has(`${id}`)) {
      throw pathError(this.path, `Invalid id: ${id}`);
    }
    const lastSelectedId =
      this.lastSelectedId === null ||
      !this.relativeStorage.has(`${this.lastSelectedId}`)
        ? id
        : this.lastSelectedId;
    let idx: number;
    let lastSelectedIdx: number;
    const isTable = isTableSchema(this._schema);
    if (isTable) {
      idx = this._value.findIndex((row) => getRowId(row) === id);
      lastSelectedIdx =
        id === lastSelectedId
          ? idx
          : this._value.findIndex((row) => getRowId(row) === lastSelectedId);
    } else {
      idx = id as number;
      lastSelectedIdx = lastSelectedId as number;
    }
    for (
      let i = Math.min(lastSelectedIdx, idx),
        l = Math.max(lastSelectedIdx, idx);
      i <= l;
      ++i
    ) {
      const iId = isTable ? getRowId(this._value[i]) : i;
      if (selectedStatus) {
        this.selectedRows.set(iId, true);
      } else {
        this.selectedRows.delete(iId);
      }
    }
    this.lastSelectedId = id;
  }

  /**
   * Unselects all rows.
   */
  @action
  public unselectAllRows(): void {
    // Doing this seems to be way faster than calling `selectedRows.clear()`
    // with, for example, 200000 rows
    this.selectedRows = new Map();
  }

  /**
   * Sets the selection status of all rows.
   * @param selectedStatus `true` to set selected, `false` to set unselected.
   */
  @action
  public setAllRowsSelectedStatus(selectedStatus: boolean): void {
    if (selectedStatus) {
      const isTable = isTableSchema(this._schema);
      this._value.forEach((el, i) => {
        const id = isTable ? getRowId(el) : i;
        this.selectedRows.set(id, true);
      });
    } else {
      this.unselectAllRows();
    }
  }

  /**
   * Toggles the status of all rows (if they are all selected, then they all
   * become unselected, otherwise they all become selected).
   */
  @action
  public toggleAllRowsSelectedStatus(): void {
    this.setAllRowsSelectedStatus(!this.allRowsSelected);
  }

  /**
   * Removes all selected rows.
   */
  @action
  public removeSelectedRows(): void {
    const isTable = isTableSchema(this._schema);
    this.relativeStorage.set(
      '.',
      this._value.filter((el, i) => {
        const id = isTable ? getRowId(el) : i;
        return !this.selectedRows.has(id);
      })
    );
    this.unselectAllRows();
    this.lastSelectedId = null;
  }

  /**
   * Scrolls to the row with the provided id.
   * @param id Identifier of the row to scroll to.
   */
  public scrollToId(id: Identifier): void {
    if (!this.relativeStorage.has(`${id}`)) {
      throw pathError(this.path, `Invalid id: ${id}`);
    }
    const index = isListSchema(this._schema)
      ? (id as number)
      : this._value.findIndex((row) => getRowId(row) === id);
    this.scrollToIndex(index);
  }

  /**
   * Scrolls to row with the provided index.
   * @param index Index of the row to scroll to.
   */
  public scrollToIndex(index: number): void {
    this._scrollableBody?.scrollToIndex(index);
  }

  /**
   * Identifier of the given element with the given index.
   * @param el Element for which to get identifier.
   * @param index Index of the element in the value.
   * @returns Identifier for the given element at the given index.
   */
  public _id(el: any, index: number): Identifier {
    return isListSchema(this._schema) ? index : getRowId(el);
  }

  /**
   * Action to run when the "remove rows" button is clicked.
   */
  @action
  public _onClickRemoveRows(): void {
    this.removeSelectedRows();
    this.relativeStorage.setDirty();
  }

  /**
   * Action to run when the "new row" button is clicked.
   */
  @action
  public _onClickNewRow(): void {
    this.addNewRow();
    this.relativeStorage.setDirty();
  }

  protected validatePath(): void {
    const schema = this.relativeStorage.schema();
    if (
      (!isListSchema(schema) && !isTableSchema(schema)) ||
      isNullableSchema(schema)
    ) {
      throw pathError(
        this.path,
        'Schema must be of the "list" or "table" type and not "nullable"'
      );
    }
  }

  // Scroll to the activated row when one is activated.
  @action
  protected onActivate(activatedPath: string): void {
    const relevantPart = this.lfStorage
      .pathInfo(activatedPath)
      .find(({ path }) => path.length > this.path.length);
    const id = relevantPart && relevantPart.id!;
    if (id !== undefined && this.relativeStorage.has(`${id}`)) {
      this.scrollToId(id);
    }
  }

  /**
   * State property name used to save the position of the scroll within the
   * table.
   * @param path Path of table.
   * @returns State property name.
   */
  private scrollPositionStatePropName(path: string): string {
    // Add the path to the name of the property when the value is a child of a
    // computed value (since children states are shared with the parent computed
    // value)
    return `lfTableScrollPosition${
      this.relativeStorage.isComputed(appendToPath(path, '..'))
        ? `[${path}]`
        : ''
    }`;
  }

  /**
   * Update the status of the "horizontal overflow".
   */
  @action
  private checkHasHorizontalOverflow() {
    this.hasHorizontalOverflow =
      !!this._scrollableBody &&
      this.scrollbarUtils.hasHorizontalOverflow(
        this._scrollableBody.elementRef.nativeElement
      );
  }
}
