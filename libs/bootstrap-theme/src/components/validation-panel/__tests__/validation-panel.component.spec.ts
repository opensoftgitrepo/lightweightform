import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  ComponentFixture,
  ComponentFixtureAutoDetect,
  TestBed,
} from '@angular/core/testing';
import {
  LfI18n,
  LfRouter,
  LfStorage,
  LF_APP_I18N,
  LF_APP_SCHEMA,
} from '@lightweightform/core';
import { numberSchema, Schema } from '@lightweightform/storage';
import { MobxAngularModule } from 'mobx-angular';

import { ValidationPanelComponent } from '../validation-panel.component';

describe('Validation panel component', () => {
  const schema: Schema = numberSchema();
  let fixture: ComponentFixture<ValidationPanelComponent>;
  let component: ValidationPanelComponent;

  const mockLfRouter = {
    // TODO
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MobxAngularModule],
      declarations: [ValidationPanelComponent],
      providers: [
        { provide: ComponentFixtureAutoDetect, useValue: true },
        { provide: LF_APP_SCHEMA, useValue: schema },
        { provide: LF_APP_I18N, useValue: {} },
        LfStorage,
        LfI18n,
        { provide: LfRouter, useValue: mockLfRouter },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });
    fixture = TestBed.createComponent(ValidationPanelComponent);
    component = fixture.componentInstance;
  });

  it('is created', () => {
    expect(component).toBeDefined();
  });
});
