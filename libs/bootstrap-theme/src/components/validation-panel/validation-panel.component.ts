import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
  Optional,
} from '@angular/core';
import { LfI18n, LfRouter, LfStorage, PathBased } from '@lightweightform/core';
import {
  ValidationIssue,
  ValidationIssuesMap,
  ValidationMode,
} from '@lightweightform/storage';
import { makeObservable, override } from 'mobx';
import { action, computed, observable } from 'mobx-angular';
import { PENDING } from 'mobx-utils';

import { Toggleable } from '../../abstract/toggleable';
import {
  I18N_CLOSE_BUTTON_TOOLTIP_KEY,
  I18N_VALIDATION_PANEL_ERRORS_TEXT_KEY,
  I18N_VALIDATION_PANEL_FOCUS_TEXT_KEY,
  I18N_VALIDATION_PANEL_NEXT_BUTTON_TOOLTIP_KEY,
  I18N_VALIDATION_PANEL_NO_ISSUES_TEXT_KEY,
  I18N_VALIDATION_PANEL_PREV_BUTTON_TOOLTIP_KEY,
  I18N_VALIDATION_PANEL_WARNINGS_TEXT_KEY,
  I18N_VALUE_LABEL_KEY,
} from '../../i18n/keys';

/**
 * Validation panel component.
 */
@Component({
  selector: 'lf-validation-panel',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './validation-panel.component.html',
})
export class ValidationPanelComponent extends Toggleable implements OnInit {
  /**
   * Path currently being viewed.
   */
  @observable private _currentPath: string;

  constructor(
    @Optional()
    @Inject(PathBased)
    parentPathBasedComponent: PathBased | null,
    lfStorage: LfStorage,
    lfI18n: LfI18n,
    private lfRouter: LfRouter
  ) {
    super(parentPathBasedComponent, lfStorage, lfI18n);
    makeObservable(this);
  }

  @computed
  public get errorsText(): string {
    return this.translate(I18N_VALIDATION_PANEL_ERRORS_TEXT_KEY);
  }

  @computed
  public get warningsText(): string {
    return this.translate(I18N_VALIDATION_PANEL_WARNINGS_TEXT_KEY);
  }

  @computed
  public get noIssuesText(): string {
    return this.translate(I18N_VALIDATION_PANEL_NO_ISSUES_TEXT_KEY);
  }

  @computed
  public get focusButtonTooltip() {
    return this.translate(I18N_VALIDATION_PANEL_FOCUS_TEXT_KEY);
  }

  @computed
  public get nextButtonTooltip() {
    return this.translate(I18N_VALIDATION_PANEL_NEXT_BUTTON_TOOLTIP_KEY);
  }

  @computed
  public get prevButtonTooltip() {
    return this.translate(I18N_VALIDATION_PANEL_PREV_BUTTON_TOOLTIP_KEY);
  }

  @computed
  public get closeButtonTooltip() {
    return this.translate(I18N_CLOSE_BUTTON_TOOLTIP_KEY);
  }

  @computed
  public get isPending(): boolean {
    return this.relativeStorage.validationStatus() === PENDING;
  }

  @computed
  public get currentPath() {
    return this._currentPath && this._validationIssues[this._currentPath]
      ? this._currentPath
      : this._pages[0];
  }

  @computed
  public get currentLabel() {
    return this.currentPath && this.labelOf(this.currentPath);
  }

  @computed
  public get hasNext() {
    return this.nPages > 1;
  }

  @computed
  public get hasPrevious() {
    return this.nPages > 1;
  }

  @computed
  public get errors(): ValidationIssue[] {
    return (
      (this._validationIssues[this.currentPath] || []).filter(
        (issue) => !issue.isWarning
      ) || []
    );
  }

  @computed
  public get warnings(): ValidationIssue[] {
    return (
      (this._validationIssues[this.currentPath] || []).filter(
        (issue) => issue.isWarning
      ) || []
    );
  }

  @computed
  public get hasNoIssues() {
    return this.nPages === 0;
  }

  @computed
  public get panelType() {
    return this.isPending
      ? 'secondary'
      : this.errors.length > 0
      ? 'danger'
      : this.warnings.length > 0
      ? 'warning'
      : 'success';
  }

  @computed
  public get shouldDisableFocusButton(): boolean {
    return (
      this.lfStorage.validationMode === ValidationMode.MANUAL &&
      !this.lfStorage.has(this.currentPath)
    );
  }

  @computed
  private get _pages() {
    return Object.keys(this._validationIssues);
  }

  @computed
  public get nPages() {
    return this._pages.length;
  }

  @computed
  public get currentPathIndex(): number {
    return this._pages.indexOf(this.currentPath);
  }

  @computed
  private get _validationIssues(): ValidationIssuesMap {
    return this.relativeStorage.validationIssues() || {};
  }

  public show(): void {
    super.show();
    this.navigateToCurrentPath();
  }

  @action
  public triggerFocus() {
    this.navigateToCurrentPath();
  }

  @action
  public next() {
    if (this.hasNext) {
      this._currentPath =
        this._pages[(this.currentPathIndex + 1) % this.nPages];
      this.navigateToCurrentPath();
    }
  }

  @action
  public previous() {
    if (this.hasPrevious) {
      this._currentPath =
        this._pages[(this.currentPathIndex - 1 + this.nPages) % this.nPages];
      this.navigateToCurrentPath();
    }
  }

  /**
   * Label associated with the given path.
   * @param path Path from which to fetch label.
   * @returns Label of given path.
   */
  private labelOf(path: string): string {
    return this.lfI18n.getFromPath(path, I18N_VALUE_LABEL_KEY) || path;
  }

  private navigateToCurrentPath() {
    if (this.currentPath) {
      // Check if the first route that matches the path is navigateable, if not
      // don't navigate
      const firstMatching = this.lfRouter.matchingRoutes(this.currentPath, {
        navigateToParent: true,
      })[0];
      const navigateable =
        firstMatching &&
        this.lfRouter.navigateableRouteFromMatchingRoutes([firstMatching]);
      if (navigateable) {
        this.lfRouter.navigateFromNavigateableRoute(navigateable, {
          queryParamsHandling: 'merge',
        });
      } else {
        // Remove fragment TODO: Find a better way of doing it
        this.lfRouter.navigate('.', { queryParamsHandling: 'merge' });
      }
    }
  }
}
