import { ScrollingModule } from '@angular/cdk/scrolling';
import { CommonModule } from '@angular/common';
import { NgModule, Provider } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { LfCoreModule } from '@lightweightform/core';
import { LfTableHeaderModule } from '@lightweightform/theme-common';
import { MobxAngularModule } from 'mobx-angular';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { MarkdownModule } from 'ngx-markdown';

import { ActionsMenuComponent } from './components/actions-menu/actions-menu.component';
import { AlertComponent } from './components/alert/alert.component';
import { AppComponent } from './components/app/app.component';
import { TopBarComponent } from './components/app/top-bar/top-bar.component';
import { ButtonComponent } from './components/button/button.component';
import { CardContentComponent } from './components/card/card-content/card-content.component';
import { CardComponent } from './components/card/card.component';
import { CheckboxGroupComponent } from './components/checkbox-group/checkbox-group.component';
import { CheckboxComponent } from './components/checkbox/checkbox.component';
import { DateRangeComponent } from './components/date-range/date-range.component';
import { DateComponent } from './components/date/date.component';
import { DropdownItemComponent } from './components/dropdown/dropdown-item/dropdown-item.component';
import { DropdownMenuComponent } from './components/dropdown/dropdown-menu/dropdown-menu.component';
import { DropdownComponent } from './components/dropdown/dropdown.component';
import { FileComponent } from './components/file/file.component';
import { FormComponent } from './components/form/form.component';
import { HelpTooltipComponent } from './components/help-tooltip/help-tooltip.component';
import { IconComponent } from './components/icon/icon.component';
import { ModalBodyComponent } from './components/modal/modal-body/modal-body.component';
import { ModalFooterComponent } from './components/modal/modal-footer/modal-footer.component';
import { ModalHeaderComponent } from './components/modal/modal-header/modal-header.component';
import { ModalComponent } from './components/modal/modal.component';
import { NumberComponent } from './components/number/number.component';
import { RadioComponent } from './components/radio/radio.component';
import { SelectComponent } from './components/select/select.component';
import { TableCellComponent } from './components/table/table-cell/table-cell.component';
import { TableRowComponent } from './components/table/table-row/table-row.component';
import { TableComponent } from './components/table/table.component';
import { TextComponent } from './components/text/text.component';
import { TextareaComponent } from './components/textarea/textarea.component';
import { TooltipComponent } from './components/tooltip/tooltip.component';
import { TreeEntryComponent } from './components/tree-nav/tree-entry/tree-entry.component';
import { TreeNavComponent } from './components/tree-nav/tree-nav.component';
import { ValidationPanelComponent } from './components/validation-panel/validation-panel.component';
import { ValueStateComponent } from './components/value-state/value-state.component';
import { ValidationIssueDirective } from './directives/validation-issue/validation-issue.directive';

/**
 * Exported/declared components and directives.
 */
const DECLARATIONS = [
  ActionsMenuComponent,
  AlertComponent,
  AppComponent,
  ButtonComponent,
  CardComponent,
  CardContentComponent,
  CheckboxComponent,
  CheckboxGroupComponent,
  DateComponent,
  DateRangeComponent,
  DropdownComponent,
  DropdownItemComponent,
  DropdownMenuComponent,
  FileComponent,
  FormComponent,
  HelpTooltipComponent,
  IconComponent,
  ModalBodyComponent,
  ModalComponent,
  ModalFooterComponent,
  ModalHeaderComponent,
  NumberComponent,
  RadioComponent,
  SelectComponent,
  TableCellComponent,
  TableComponent,
  TableRowComponent,
  TextareaComponent,
  TextComponent,
  TooltipComponent,
  TopBarComponent,
  TreeEntryComponent,
  TreeNavComponent,
  ValidationIssueDirective,
  ValidationPanelComponent,
  ValueStateComponent,
];

/**
 * LF Bootstrap theme module.
 */
@NgModule({
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    RouterModule.forChild([]),
    BsDatepickerModule.forRoot(),
    LfCoreModule,
    LfTableHeaderModule,
    MarkdownModule.forRoot(),
    MobxAngularModule,
    ScrollingModule,
    TooltipModule.forRoot(),
  ],
  declarations: DECLARATIONS,
  exports: [
    LfCoreModule,
    LfTableHeaderModule,
    MobxAngularModule,
    ...DECLARATIONS,
  ],
})
export class LfBootstrapThemeModule {}

/**
 * Theme services that should be provided for each provided `LF_APP_SCHEMA`.
 * Currently empty, reserved for future use.
 */
export const LF_BOOTSTRAP_THEME_SERVICES: Provider = [];
