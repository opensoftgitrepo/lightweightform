import { Directive } from '@angular/core';
import { LfI18n, LfRouter, LfStorage, PathBased } from '@lightweightform/core';
import { makeObservable } from 'mobx';
import { computed } from 'mobx-angular';
import { PromiseState } from 'mobx-utils';

import {
  I18N_SELECTION_NO_OPTIONS_TEXT_KEY,
  I18N_SELECTION_OPTIONS_KEY,
} from '../i18n/keys';

import { InlineValue } from './inline-value';

/**
 * List of options supported by components which present a list of options from
 * which to select.
 */
export type Options<T = any> = Array<Option<T>>;

/**
 * Type of an option supported by components which present a list of options
 * from which to select.
 */
export interface Option<T = any> {
  /**
   * Value of the option.
   */
  value: T;
  /**
   * Human-readable string to represent the option.
   */
  label?: string;
  /**
   * Code of the option.
   */
  code?: string;
  /**
   * Help message associated with the option.
   */
  helpMessage?: string;
  /**
   * Whether the option should show up as disabled.
   */
  isDisabled?: boolean;
}

/**
 * Class which represents a component whose value may be restricted by a set of
 * options.
 */
@Directive()
// tslint:disable-next-line: directive-class-suffix
export abstract class SelectionValue extends InlineValue {
  constructor(
    parentPathBasedComponent: PathBased | null,
    lfStorage: LfStorage,
    lfI18n: LfI18n,
    lfRouter: LfRouter | null
  ) {
    super(parentPathBasedComponent, lfStorage, lfI18n, lfRouter);
    makeObservable(this);
  }

  /**
   * Selectable options (possibly fetched asynchronously).
   */
  // Not `computed` due to `useState` (cannot be used within a `computed`)
  public get options(): Options {
    return this.translate(I18N_SELECTION_OPTIONS_KEY, { useState: true }) || [];
  }

  /**
   * Text to be shown when there are no options available.
   */
  @computed
  public get noOptionsText(): string | undefined {
    return this.translate(I18N_SELECTION_NO_OPTIONS_TEXT_KEY);
  }

  /**
   * Promise-status of the options.
   */
  @computed
  public get optionsStatus(): PromiseState {
    return this.translationStatus(I18N_SELECTION_OPTIONS_KEY);
  }
}
