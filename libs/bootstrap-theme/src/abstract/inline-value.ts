import {
  AfterViewInit,
  Directive,
  ElementRef,
  EventEmitter,
  HostBinding,
  Input,
  OnDestroy,
  OnInit,
  Output,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { LfI18n, LfRouter, LfStorage, PathBased } from '@lightweightform/core';
import { ValidationMode } from '@lightweightform/storage';
import { asap } from '@lightweightform/theme-common';
import { autorun, makeObservable } from 'mobx';
import { computed, observable } from 'mobx-angular';

import { Value } from './value';

/**
 * Abstract class used to represent typical "inline-able" values.
 */
@Directive()
// tslint:disable-next-line: directive-class-suffix
export abstract class InlineValue
  extends Value
  implements OnInit, AfterViewInit, OnDestroy
{
  /**
   * Whether the component should be hidden when its value isn't required.
   * **NOT READY FOR USE**
   */
  // TODO: What about components that aren't nullable? Example: tables, text, we
  // should use minSize/minLength instead. Allow each component to implement its
  // own "isRequired" functionality?
  @observable
  @Input()
  public _hideWhenNotRequired = false;

  /**
   * Event emitter which emits an event whenever the component's DOM input is
   * focused.
   */
  @Output()
  public focus: EventEmitter<FocusEvent> = new EventEmitter<FocusEvent>();
  /**
   * Event emitter which emits an event whenever the component's DOM input is
   * blurred.
   */
  @Output()
  public blur: EventEmitter<FocusEvent> = new EventEmitter<FocusEvent>();

  /**
   * Add an `lf-inline-value` class to components.
   */
  @HostBinding('class.lf-inline-value') public _classLfInlineValue = true;

  /**
   * Target elements that should be the target of focus/blur events; the first
   * element of que query list is the one that will be focused on route change
   * or when demanded via `focus()`.
   */
  @ViewChildren('focusTarget') protected targets: QueryList<ElementRef>;

  /**
   * Disposer for the "auto-focus" of the field when it becomes active.
   */
  private disposeOnActive: () => void;

  constructor(
    parentPathBasedComponent: PathBased | null,
    lfStorage: LfStorage,
    lfI18n: LfI18n,
    protected lfRouter: LfRouter | null
  ) {
    super(parentPathBasedComponent, lfStorage, lfI18n);
    makeObservable(this);
  }

  /**
   * Whether this component should be displayed.
   */
  @computed
  public get shouldDisplay(): boolean {
    return (
      this.relativeStorage.has() &&
      (!this._hideWhenNotRequired ||
        (this.relativeStorage.validationMode === ValidationMode.AUTO
          ? this.relativeStorage.getStateProperty('.', 'isRequired')
          : this.relativeStorage.schema().isRequired) === true)
    );
  }

  public ngOnInit() {
    super.ngOnInit();
  }

  public ngAfterViewInit() {
    if (this.lfRouter !== null) {
      // Auto-focus the field when this component's path is activated
      this.disposeOnActive = autorun(
        () => {
          if (this.lfRouter!.isActiveWithFragment(this.path)) {
            const res =
              this.onActivate &&
              this.onActivate(this.lfRouter!.pathWithFragment!);
            // Trigger the `focus` only when path matches exactly
            if (
              res !== false &&
              this.lfRouter!.isActiveWithFragment(this.path, true)
            ) {
              asap(() => this.triggerFocus());
            }
          }
        },
        { name: 'fieldAutorunFocusOnActive' }
      );
    }
  }

  public ngOnDestroy() {
    super.ngOnDestroy();
    this.disposeOnActive && this.disposeOnActive();
  }

  /**
   * Scrolls this component into the center of the viewport and focuses it.
   */
  public triggerFocus(): void {
    if (this.targets && this.targets.length > 0) {
      const target: HTMLElement = this.targets.first.nativeElement;
      target.scrollIntoView({ block: 'center' });
      target.focus();
    }
  }

  /**
   * Action to run whenever a focusable part of the field is focused.
   * @param evt `focus` event.
   */
  public _onFocus(evt: FocusEvent): void {
    this.focus.emit(evt);
  }

  /**
   * Action to run whenever a focusable part of the field is blurred.
   * @param evt `blur` event.
   * @param setTouched Whether to try and set the value as "touched". Defaults
   * to `true`.
   */
  public _onBlur(evt: FocusEvent, setTouched = true): void {
    if (setTouched && !this.relativeStorage.isComputed()) {
      this.relativeStorage.setTouched();
    }
    // Remove the fragment when the path exactly matches the URL
    if (this.lfRouter?.isActiveWithFragment(this.path, true)) {
      this.lfRouter.navigate('.', { queryParamsHandling: 'merge' });
    }
    this.blur.emit(evt);
  }

  /**
   * Method that directives can implement which is called whenever the
   * directive's path becomes active. Returning `false` prevents the default
   * behaviour of focusing this directive's target element.
   * @param activatedPath Full activated path (may be a child of the directive's
   * path).
   * @returns Nothing or `false` to prevent the default focus behaviour.
   */
  protected onActivate?(activatedPath: string): false | void;
}
