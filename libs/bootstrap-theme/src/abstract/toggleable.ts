import { Directive, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { LfI18n, LfStorage, PathBased } from '@lightweightform/core';
import { makeObservable } from 'mobx';
import { action, computed, observable } from 'mobx-angular';

/**
 * Abstract class for components that are toggleable and path based.
 */
@Directive()
// tslint:disable-next-line: directive-class-suffix
export abstract class Toggleable extends PathBased implements OnInit {
  /**
   * Whether the toggleable element should start visible.
   */
  @Input() public startVisible = false;

  /**
   * Event emitter which emits an event whenever the toggleable is set to be
   * shown.
   */
  // tslint:disable-next-line:no-output-rename
  @Output('show') public _show: EventEmitter<void> = new EventEmitter<void>();
  /**
   * Event emitter which emits an event whenever the toggleable is set to be
   * hidden.
   */
  // tslint:disable-next-line:no-output-rename
  @Output('hide') public _hide: EventEmitter<void> = new EventEmitter<void>();

  @observable protected _isVisible: boolean;

  constructor(
    parentPathBasedComponent: PathBased | null,
    lfStorage: LfStorage,
    lfI18n: LfI18n
  ) {
    super(parentPathBasedComponent, lfStorage, lfI18n);
    makeObservable(this);
  }

  /**
   * Whether the toggleable is visible.
   */
  @computed
  public get isVisible(): boolean {
    return this._isVisible;
  }
  public set isVisible(setVisible: boolean) {
    if (!this._isVisible && setVisible) {
      this._isVisible = true;
      this._show.emit();
    } else if (this._isVisible && !setVisible) {
      this._isVisible = false;
      this._hide.emit();
    }
  }

  public ngOnInit() {
    this._isVisible = this.startVisible;
  }

  /**
   * Show the toggleable element.
   */
  @action
  public show(): void {
    this.isVisible = true;
  }

  /**
   * Hide the toggleable element.
   */
  @action
  public hide(): void {
    this.isVisible = false;
  }

  /**
   * Toggle the visibility of the toggleable element.
   */
  @action
  public toggle(): void {
    this.isVisible = !this.isVisible;
  }
}
