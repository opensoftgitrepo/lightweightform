import { Directive } from '@angular/core';
import { LfI18n, LfRouter, LfStorage, PathBased } from '@lightweightform/core';
import { ValidationMode } from '@lightweightform/storage';
import { comparer, makeObservable } from 'mobx';
import { computed } from 'mobx-angular';

import { SelectionValue } from './selection-value';

/**
 * Abstract class used to represent components where a single option is selected
 * from a set of options.
 */
@Directive()
// tslint:disable-next-line: directive-class-suffix
export abstract class SingleSelectionValue extends SelectionValue {
  constructor(
    parentPathBasedComponent: PathBased | null,
    lfStorage: LfStorage,
    lfI18n: LfI18n,
    lfRouter: LfRouter | null
  ) {
    super(parentPathBasedComponent, lfStorage, lfI18n, lfRouter);
    makeObservable(this);
  }

  /**
   * Whether to show the "is required" icon on the component.
   */
  @computed
  public get showIsRequired(): boolean {
    return (
      this.relativeStorage.isNullable() &&
      (this.relativeStorage.validationMode === ValidationMode.AUTO
        ? this.relativeStorage.getStateProperty('.', 'isRequired')
        : this.relativeStorage.schema().isRequired) === true
    );
  }

  /**
   * Index of the selected option; `-1` when none of the available options is
   * selected.
   */
  // Not `computed` since `options` cannot be accessed within a `computed` value
  public get selectedOptionIndex(): number {
    const val = this.relativeStorage.get();
    return this.options.findIndex((opt) => comparer.structural(val, opt.value));
  }
}
