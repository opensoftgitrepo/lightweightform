import { Directive, HostBinding, Input } from '@angular/core';
import { LfI18n, LfStorage, PathBased } from '@lightweightform/core';
import { makeObservable } from 'mobx';
import { computed, observable } from 'mobx-angular';

import {
  I18N_VALUE_CODE_KEY,
  I18N_VALUE_HELP_MESSAGE_KEY,
  I18N_VALUE_LABEL_KEY,
  I18N_VALUE_LEGEND_KEY,
  I18N_VALUE_RETRY_VALIDATE_TEXT_KEY,
} from '../i18n/keys';

/**
 * Abstract class that all components which "encapsulate" a storage value should
 * inherit.
 */
@Directive()
// tslint:disable-next-line: directive-class-suffix
export abstract class Value extends PathBased {
  /**
   * Whether the component should render in "read-only" mode (this has no effect
   * if this component is within an already "read-only" component).
   */
  // tslint:disable-next-line:no-input-rename
  @observable
  @Input('isReadOnly')
  public _isReadOnly = false;

  /**
   * Add an `lf-value` class to all value components.
   */
  @HostBinding('class.lf-value') public _classLfValue = true;

  constructor(
    parentPathBasedComponent: PathBased | null,
    lfStorage: LfStorage,
    lfI18n: LfI18n
  ) {
    super(parentPathBasedComponent, lfStorage, lfI18n);
    makeObservable(this);
  }

  /**
   * Label associated with this value.
   */
  @computed
  public get label(): string | undefined {
    return this.translate(I18N_VALUE_LABEL_KEY);
  }
  /**
   * Code associated with this value.
   */
  @computed
  public get code(): string | undefined {
    return this.translate(I18N_VALUE_CODE_KEY);
  }
  /**
   * Help message associated with this value.
   */
  @computed
  public get helpMessage(): string | undefined {
    return this.translate(I18N_VALUE_HELP_MESSAGE_KEY);
  }
  /**
   * Legend associated with this value.
   */
  @computed
  public get legend(): string | undefined {
    return this.translate(I18N_VALUE_LEGEND_KEY);
  }
  /**
   * Message used to ask the user to retry validating the value.
   */
  @computed
  public get retryValidateText(): string | undefined {
    return this.translate(I18N_VALUE_RETRY_VALIDATE_TEXT_KEY);
  }

  /**
   * Whether the component should render in "read-only" mode. Inherits the
   * "read-only" state of parent components (meaning that it's not possible to
   * have an "editable" component within a "read-only" one). Furthermore, if the
   * component's value is computed, the component will always be in "read-only"
   * mode.
   */
  @computed
  public get isReadOnly(): boolean {
    return (
      (this.parentPathBasedComponent !== null &&
        (this.parentPathBasedComponent as Value).isReadOnly) ||
      this.relativeStorage.isComputed() ||
      this._isReadOnly
    );
  }
}
