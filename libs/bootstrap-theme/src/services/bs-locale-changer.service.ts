import { Injectable } from '@angular/core';
import { LfI18n } from '@lightweightform/core';
import { observe } from 'mobx';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';

import { I18N_BS_LOCALE_KEY } from '../i18n/keys';

/**
 * Automatically change `ngx-bootstrap` locale based on current language.
 */
@Injectable({ providedIn: 'root' })
export class BsLocaleChanger {
  constructor(
    private bsLocaleService: BsLocaleService,
    private lfI18n: LfI18n
  ) {
    observe(
      this.lfI18n,
      'currentLanguage',
      () => {
        this.bsLocaleService.use(this.lfI18n.get(I18N_BS_LOCALE_KEY) || 'en');
      },
      true
    );
  }
}
