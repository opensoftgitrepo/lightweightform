/**
 * Converts a date representing a certain day in local time to a date that
 * represents that same day in UTC. Used to fix:
 * https://github.com/valor-software/ngx-bootstrap/issues/3609
 * @param date Date to convert.
 * @returns Date in UTC.
 */
export function localDateToUTC(date: Date): Date {
  return new Date(
    Date.UTC(date.getFullYear(), date.getMonth(), date.getDate())
  );
}

/**
 * Whether the provided value is an instance of `Date`.
 * @param value Value to check if it is a date.
 * @returns Whether the provided value is a date.
 */
export function isDate(value: Date): value is Date {
  return (
    value instanceof Date ||
    (typeof value === 'object' &&
      Object.prototype.toString.call(value) === '[object Date]')
  );
}
