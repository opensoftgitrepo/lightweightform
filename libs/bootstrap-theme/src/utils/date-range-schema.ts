import {
  compareDates,
  DateSchema,
  dateSchema,
  DATE_OUT_OF_BOUNDS_CODE,
  DATE_OUT_OF_BOUNDS_TYPE,
  isArrayLike,
  isDateSchema,
  isTupleSchema,
  KnownKeys,
  ParentContextFunction,
  Schema,
  Storage,
  tupleSchema,
  TupleSchema,
  ValidationIssue,
} from '@lightweightform/storage';

/**
 * Default issue code emitted for invalid date-ranges (where the start date is
 * after the end date).
 */
export const INVALID_DATE_RANGE_CODE = 'LF_INVALID_DATE_RANGE';
/**
 * Type of issue emitted for invalid date-ranges.
 */
export const INVALID_DATE_RANGE_TYPE = 'invalidDateRange';

/**
 * Date-range schema.
 */
export interface DateRangeSchema
  extends TupleSchema<[Date, Date], [DateSchema, DateSchema]> {
  /**
   * Skip adding validations (needed when using a date-range within a computed
   * value).
   */
  skipValidations?: boolean;
  /**
   * Issue code to indicate that the date-range is invalid (where the start date
   * is after the end date).
   */
  invalidDateRangeCode?: string;
  /**
   * Minimum date that the date-range represented by this schema is allowed to
   * have.
   */
  minDate?: Date | ParentContextFunction<Date>;
  /**
   * String to use as issue code when the date-range's start date is less than
   * the minimum allowed date. Defaults to `DATE_OUT_OF_BOUNDS_CODE`.
   */
  minDateCode?: string;
  /**
   * Maximum date that the date-range represented by this schema is allowed to
   * have.
   */
  maxDate?: Date | ParentContextFunction<Date>;
  /**
   * String to use as issue code when the date-range's end date is greater than
   * the maximum allowed date. Defaults to `DATE_OUT_OF_BOUNDS_CODE`.
   */
  maxDateCode?: string;
  /**
   * Options to construct the start date schema with.
   */
  startDateOptions?: Pick<
    DateSchema,
    Exclude<
      KnownKeys<DateSchema>,
      'type' | 'minDate' | 'minDateCode' | 'maxDate' | 'maxDateCode'
    >
  > & { [extraProperties: string]: any };
  /**
   * Options to construct the end date schema with.
   */
  endDateOptions?: Pick<
    DateSchema,
    Exclude<
      KnownKeys<DateSchema>,
      'type' | 'minDate' | 'minDateCode' | 'maxDate' | 'maxDateCode'
    >
  > & { [extraProperties: string]: any };
}

/**
 * Options used to create a new date-range schema.
 */
export type DateRangeSchemaOptions = Pick<
  DateRangeSchema,
  Exclude<KnownKeys<DateRangeSchema>, 'type' | 'elementsSchemas'>
> & { [extraProperties: string]: any };

/**
 * Creates a new date-range schema object.
 * @param options Options used to create the schema.
 * @returns Date-range schema.
 */
export function dateRangeSchema(
  options: DateRangeSchemaOptions = {}
): DateRangeSchema {
  const initialState = options.initialState || {};

  // Date-range validation function
  function validateDateRange(ctx: Storage): ValidationIssue | undefined {
    const value: [Date, Date] = ctx.get();
    if (compareDates(value[0], value[1]) > 0) {
      return {
        code: options.invalidDateRangeCode ?? INVALID_DATE_RANGE_CODE,
        data: { type: INVALID_DATE_RANGE_TYPE },
      };
    }

    const minDate: Date | undefined = ctx.getStateProperty('.', 'minDate');
    const maxDate: Date | undefined = ctx.getStateProperty('.', 'maxDate');
    const code =
      minDate != null && compareDates(value[0], minDate) < 0
        ? options.minDateCode ?? DATE_OUT_OF_BOUNDS_CODE
        : maxDate != null && compareDates(value[1], maxDate) > 0
        ? options.maxDateCode ?? DATE_OUT_OF_BOUNDS_CODE
        : null;
    if (code != null) {
      return {
        code,
        data: { type: DATE_OUT_OF_BOUNDS_TYPE, value, minDate, maxDate },
      };
    }
  }

  // Skip adding validations when requested (because e.g. the date-range is
  // computed)
  if (!options.skipValidations) {
    // Set `minDate` and `maxDate` as data properties in the date-range.
    if (options.minDate) {
      initialState.minDate =
        typeof options.minDate === 'function'
          ? (ctx: Storage) =>
              (options.minDate as any)(ctx.relativeStorage('..'))
          : options.minDate;
    }
    if (options.maxDate) {
      initialState.maxDate =
        typeof options.maxDate === 'function'
          ? (ctx: Storage) =>
              (options.maxDate as any)(ctx.relativeStorage('..'))
          : options.maxDate;
    }

    // Add date-range validation
    if (isArrayLike(options.validate)) {
      options.validate.unshift(validateDateRange);
    } else if (options.validate !== undefined) {
      options.validate = [validateDateRange, options.validate];
    } else {
      options.validate = validateDateRange;
    }
  }

  return tupleSchema(
    [dateSchema(options.startDateOptions), dateSchema(options.endDateOptions)],
    { ...options, initialState }
  ) as DateRangeSchema;
}

/**
 * Returns whether a given schema is a date-range schema.
 * @param schema Schema to check.
 * @returns `true` when the provided schema is a date-range schema.
 */
export function isDateRangeSchema(schema: Schema): schema is DateRangeSchema {
  return (
    isTupleSchema(schema) &&
    schema.elementsSchemas.length === 2 &&
    isDateSchema(schema.elementsSchemas[0]) &&
    isDateSchema(schema.elementsSchemas[1])
  );
}
