/**
 * Options for the `form` schematic.
 */
export interface FormOptions {
  /**
   * Form name (possibly a path with the module as root).
   */
  name: string;
  /**
   * Name of the project.
   */
  project: string;
  /**
   * Module on which to create form.
   */
  module?: string;
  /**
   * Locales to support for i18n (comma separated).
   */
  locales: string;
  /**
   * Name of the folder where i18n translations are kept.
   */
  i18nFolderName: string;
  /**
   * Whether to create a collection of forms.
   */
  collection: boolean;
  /**
   * Parent storage path on which to insert form (or collection of forms).
   */
  parentPath?: string;
  /**
   * Whether to skip the creation of a component for the collection itself.
   */
  skipCollectionComponent: boolean;
  /**
   * Whether the form's style should be inlined.
   */
  inlineStyle: boolean;
  /**
   * Whether the form's template should be inlined.
   */
  inlineTemplate: boolean;
  /**
   * Component's view encapsulation.
   */
  viewEncapsulation?: 'Emulated' | 'Native' | 'None' | 'ShadowDom';
  /**
   * Component's change detection.
   */
  changeDetection: 'Default' | 'OnPush';
  /**
   * Form's prefix.
   */
  prefix: string;
  /**
   * Style file extension.
   */
  style: 'css' | 'scss' | 'sass' | 'less' | 'styl';
  /**
   * Whether to skip generation of test files.
   */
  skipTests: boolean;
  /**
   * Whether to flatten component creation.
   */
  flat: boolean;
  /**
   * Whether to skip import in parent `NgModule`.
   */
  skipImport: boolean;
  /**
   * Component's selector.
   */
  selector?: string;
  /**
   * Whether to skip generation of component's selector.
   */
  skipSelector: boolean;
  /**
   * Whether to export the form in parent `NgModule`.
   */
  export: boolean;
  /**
   * Whether to run linter after creating the form.
   */
  lintFix: boolean;
}
