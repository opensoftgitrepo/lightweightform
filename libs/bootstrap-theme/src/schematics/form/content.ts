import { strings } from '@angular-devkit/core';
import {
  stringAsProperty,
  validTsIdRegExp,
} from '@lightweightform/theme-common/schematics';

/**
 * HTML content for the form's component.
 * @param storagePath Form's storage path.
 * @returns HTML content for the form's component.
 */
export function htmlFormComponentContent(
  storagePath: string,
  routePlaceholders: string[]
): string {
  const pathAssignment =
    routePlaceholders.length === 0 ? `path="${storagePath}"` : '[path]="path"';
  return `\
<lf-form ${pathAssignment}>
</lf-form>`;
}

/**
 * HTML content for the collection's component.
 * @param storagePath Collection's storage path.
 * @returns HTML content for the collection's component.
 */
export function htmlCollectionComponentContent(
  storagePath: string,
  routePlaceholders: string[]
): string {
  const pathAssignment =
    routePlaceholders.length === 0 ? `path="${storagePath}"` : '[path]="path"';
  return `\
<lf-form ${pathAssignment}>
  <lf-table path=".">
    <lf-table-header>
      <lf-table-column id="navigation"></lf-table-column>
    </lf-table-header>

    <ng-template #lfTableRowTemplate let-id="id">
      <tr lf-table-row [path]="id">
        <td lf-table-cell>
          <a
            lf-button
            size="sm"
            [style]="'outline-success'"
            [block]="true"
            [routerLink]="id"
            queryParamsHandling="merge"
          >
            <lf-icon icon="share-square-o"></lf-icon>
            <lf-i18n path=".." key="navigateButtonLabel"></lf-i18n>
          </a>
        </td>
      </tr>
    </ng-template>
  </lf-table>
</lf-form>`;
}

/**
 * Content for the getter used to get a component's storage path.
 * @param storagePath Component's storage path.
 * @param routeParams Param names for the component's route.
 * @returns `path` getter.
 */
export function tsComponentPathGetterContent(
  storagePath: string,
  routeParams: string[]
): string {
  const paramsAsIds = routeParams.map((p) =>
    validTsIdRegExp.test(p) ? p : strings.camelize(p)
  );
  const paramsAsProps = routeParams.map((p, i) =>
    p === paramsAsIds[i] ? p : `${stringAsProperty(p)}: ${paramsAsIds[i]}`
  );
  let j = 0;
  const pathWithParams = storagePath
    .split('/')
    .map((id) => (id === '?' ? `\${${paramsAsIds[j++]}}` : id))
    .join('/');
  return `\
{
  const { ${paramsAsProps.join(', ')} } = this.lfRouter.params;
  return \`${pathWithParams}\`;
}
`;
}
