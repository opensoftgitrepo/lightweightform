import { basename, dirname, join, Path, strings } from '@angular-devkit/core';
import {
  apply,
  applyTemplates,
  chain,
  externalSchematic,
  filter,
  mergeWith,
  move,
  noop,
  Rule,
  SchematicsException,
  Source,
  Tree,
  url,
} from '@angular-devkit/schematics';
import {
  addFormLocaleToRootI18n,
  addLfRoute,
  addSchemaToParentSchema,
  addTsComponentConstructorParameter,
  addTsComponentGetter,
  getLfRouteInfo,
  getModulePath,
  getParentStoragePath,
  getProject,
  getRootI18nPath,
  hasRoutes,
  relativeTsImportPath,
  SchemaExplorer,
  setTsComponentInlineTemplate,
  TsImport,
} from '@lightweightform/theme-common/schematics';
import { applyLintFix } from '@schematics/angular/utility/lint-fix';

import { getLocalesArrayFromString } from '../utils/locales';

import {
  htmlCollectionComponentContent,
  htmlFormComponentContent,
  tsComponentPathGetterContent,
} from './content';
import { FormOptions } from './form.options';

/**
 * `form` schematic function.
 * @param options Schematic options.
 * @returns Schematic rule.
 */
export function form(options: FormOptions): Rule {
  return async (host: Tree) => {
    const project = await getProject(host, options.project);
    const moduleDir = dirname(getModulePath(host, project, options.module));
    let formPath = join(moduleDir, dirname(options.name as Path));
    const formName = basename(options.name as Path);

    (options as any).formName = formName;

    let collectionPath: Path | undefined;
    let collectionName: string | undefined;
    if (options.collection) {
      collectionPath = dirname(formPath);
      if (options.flat) {
        formPath = collectionPath;
      }
      collectionName = basename(dirname(options.name as Path));

      if (collectionName === '.') {
        throw new SchematicsException(
          'The provided name does not include the collection name. A name ' +
            'is expected in the format: "collection-name/form-name", ' +
            'possibly prepended by the path where the collection should be ' +
            'created.'
        );
      }
    }

    let formStoragePathId = strings.dasherize(formName);
    const formSchemaPath = join(
      formPath,
      options.flat ? '' : strings.dasherize(formName),
      strings.dasherize(formName) + '.schema.ts'
    );
    let collectionStoragePathId: string | undefined;
    let collectionSchemaPath: Path | undefined;
    if (options.collection) {
      formStoragePathId = '?';
      collectionStoragePathId = strings.dasherize(collectionName!);
      collectionSchemaPath = join(
        collectionPath!,
        options.flat ? '' : strings.dasherize(collectionName!),
        strings.dasherize(collectionName!) + '.schema.ts'
      );
    }

    const schemaExplorer = new SchemaExplorer(host, moduleDir);
    const parentStoragePath =
      options.parentPath ||
      getParentStoragePath(
        schemaExplorer,
        options.collection ? collectionSchemaPath! : formSchemaPath
      );

    const routerModulePath = getModulePath(host, project, options.module, true);
    const parentRouteInfo = getLfRouteInfo(
      host,
      routerModulePath,
      parentStoragePath
    );
    const routeParams = parentRouteInfo
      ? parentRouteInfo.params
      : // Default params from parent storage path
        parentStoragePath
          .split('/')
          .filter((id) => id === '?')
          .map((_, i) => `param-${i + 1}`);

    return chain([
      options.collection
        ? createCollection(
            options,
            schemaExplorer,
            moduleDir,
            routerModulePath,
            formName,
            collectionPath!,
            collectionName!,
            collectionSchemaPath!,
            formSchemaPath,
            collectionStoragePathId!,
            parentStoragePath,
            routeParams
          )
        : noop(),
      createForm(
        options,
        schemaExplorer,
        moduleDir,
        routerModulePath,
        formPath,
        formName,
        formSchemaPath,
        formStoragePathId,
        options.collection
          ? join(parentStoragePath as Path, collectionStoragePathId!)
          : parentStoragePath,
        options.collection
          ? [...routeParams, `${strings.dasherize(formName)}-id`]
          : routeParams
      ),
    ]);
  };
}

/**
 * Creates the form collection.
 * @param options Schematic options.
 * @param schemaExplorer `SchemaExplorer` instance.
 * @param moduleDir Module directory where to find the root schema.
 * @param routerModulePath Path of the router module.
 * @param formName Form name.
 * @param path Path where the collection is being created (independently of
 * whether it is flat).
 * @param name Collection name.
 * @param schemaPath Path of the collection's schema.
 * @param formSchemaPath Path of the form's schema.
 * @param storagePathId Id of the collection in the storage path.
 * @param parentStoragePath Storage path of the collection's parent.
 * @param routeParams Param names for the collection's route.
 * @returns Schematic rule.
 */
export function createCollection(
  options: FormOptions,
  schemaExplorer: SchemaExplorer,
  moduleDir: Path,
  routerModulePath: Path | undefined,
  formName: string,
  path: Path,
  name: string,
  schemaPath: Path,
  formSchemaPath: Path,
  storagePathId: string,
  parentStoragePath: string,
  routeParams: string[]
): Rule {
  return () => {
    const formSchemaImportPath = relativeTsImportPath(
      schemaPath,
      formSchemaPath
    );
    const storagePath = join(parentStoragePath as Path, storagePathId);

    return chain([
      options.skipCollectionComponent
        ? noop()
        : externalSchematic('@schematics/angular', 'component', {
            ...options,
            path,
            name,
            lintFix: false, // Prevent lint from running twice
          }),
      addSchema(
        options,
        schemaExplorer,
        path,
        name,
        schemaPath,
        storagePathId,
        storagePath,
        parentStoragePath,
        'collection-files',
        { formName, formSchemaImportPath }
      ),
      addI18n(
        options,
        moduleDir,
        path,
        name,
        storagePath,
        'collection-per-locale-files'
      ),
      options.skipCollectionComponent
        ? noop()
        : setHtml(
            options,
            path,
            name,
            htmlCollectionComponentContent(storagePath, routeParams)
          ),
      options.skipCollectionComponent
        ? noop()
        : editComponentTs(options, path, name, storagePath, routeParams),
      options.skipCollectionComponent
        ? noop()
        : addRoute(
            options,
            routerModulePath,
            path,
            name,
            storagePath,
            routeParams
          ),
      options.lintFix ? applyLintFix() : noop(),
    ]);
  };
}

/**
 * Creates the form.
 * @param options Schematic options.
 * @param schemaExplorer `SchemaExplorer` instance.
 * @param moduleDir Module directory where to find the root schema.
 * @param routerModulePath Path of the router module.
 * @param path Path where the form is being created (independently of whether it
 * is flat).
 * @param name Form name.
 * @param schemaPath Path of the form's schema.
 * @param storagePathId Id of the form in the storage path.
 * @param parentStoragePath Storage path of the form's parent.
 * @param routeParams Param names for the form's route.
 * @returns Schematic rule.
 */
export function createForm(
  options: FormOptions,
  schemaExplorer: SchemaExplorer,
  moduleDir: Path,
  routerModulePath: Path | undefined,
  path: Path,
  name: string,
  schemaPath: Path,
  storagePathId: string,
  parentStoragePath: string,
  routeParams: string[]
): Rule {
  return () => {
    const storagePath = join(parentStoragePath as Path, storagePathId);

    return chain([
      externalSchematic('@schematics/angular', 'component', {
        ...options,
        path,
        name,
        lintFix: false, // Prevent lint from running twice
      }),
      addSchema(
        options,
        schemaExplorer,
        path,
        name,
        schemaPath,
        storagePathId,
        storagePath,
        parentStoragePath,
        'form-files',
        {},
        !options.collection
      ),
      addI18n(
        options,
        moduleDir,
        path,
        name,
        storagePath,
        'form-per-locale-files'
      ),
      setHtml(
        options,
        path,
        name,
        htmlFormComponentContent(storagePath, routeParams)
      ),
      editComponentTs(options, path, name, storagePath, routeParams),
      addRoute(options, routerModulePath, path, name, storagePath, routeParams),
      options.lintFix ? applyLintFix() : noop(),
    ]);
  };
}

/**
 * Add needed schema files.
 * @param options Schematic options.
 * @param schemaExplorer `SchemaExplorer` instance.
 * @param path Path where the component is being created (independently of
 * whether it is flat).
 * @param name Component name.
 * @param schemaPath Path of the component's schema to add.
 * @param storagePathId Identifier of the component's schema in the storage
 * path.
 * @param storagePath Path of the component's schema in the storage.
 * @param parentStoragePath Parent path of the component's schema in the
 * storage.
 * @param filesDir Directory containing the files to be created.
 * @param addToParent Whether to add the component's schema to the parent
 * schema.
 * @returns Schematic rule.
 */
function addSchema(
  options: FormOptions,
  schemaExplorer: SchemaExplorer,
  path: Path,
  name: string,
  schemaPath: Path,
  storagePathId: string,
  storagePath: string,
  parentStoragePath: string,
  filesDir: string,
  extraTemplateBindings: Record<string, any> = {},
  addToParent: boolean = true
): Rule {
  return (host: Tree) => {
    const tsSchemaName = `${strings.camelize(name)}Schema`;
    const rootSchemaInfo = schemaExplorer.getSchemaInfoFromStoragePath('/')!;
    const rootSchemaImportPath = relativeTsImportPath(
      schemaPath,
      rootSchemaInfo.fileInfo.filePath
    );

    const templateSource = apply(url(`form/${filesDir}`), [
      options.skipTests
        ? filter((p) => !p.endsWith('.spec.ts.template'))
        : noop(),
      applyTemplates({
        ...strings,
        ...options,
        'if-flat': (str: string) => (options.flat ? '' : str),
        name,
        storagePath,
        rootTsSchemaName: schemaExplorer.rootSchemaName,
        rootSchemaImportPath,
        ...extraTemplateBindings,
      }),
      move(path),
    ]);

    if (addToParent) {
      addSchemaToParentSchema(
        host,
        schemaExplorer,
        schemaPath,
        tsSchemaName,
        storagePathId,
        parentStoragePath
      );
    }

    return chain([mergeWith(templateSource)]);
  };
}

/**
 * Add component's i18n objects.
 * @param options Schematic options.
 * @param moduleDir Module directory where to find the root i18n file.
 * @param path Path where the component is being created (independently of
 * whether it is flat).
 * @param name Component name.
 * @param storagePath Path of the component's schema in the storage.
 * @param filesDir Directory where to find the "per-locale" i18n files.
 * @returns Schematic rule.
 */
function addI18n(
  options: FormOptions,
  moduleDir: Path,
  path: Path,
  name: string,
  storagePath: string,
  filesDir: string
): Rule {
  return (host: Tree) => {
    const localesArr = getLocalesArrayFromString(options.locales);
    const rootI18nPath = getRootI18nPath(host, moduleDir);
    const i18nDirPath = join(
      path,
      options.flat ? '' : strings.dasherize(name),
      options.i18nFolderName
    );

    // Per language sources
    const languageTemplateSources: Source[] = [];
    for (const locale of localesArr) {
      languageTemplateSources.push(
        apply(url(`form/${filesDir}`), [
          applyTemplates({
            ...strings,
            ...options,
            name,
            locale,
            storagePath,
          }),
          move(i18nDirPath),
        ])
      );

      // Add locale to the root i18n
      const localePath = join(
        i18nDirPath,
        `${strings.dasherize(name)}.i18n.${locale}.ts`
      );
      const localeTsName = `${strings.camelize(name)}I18n${strings.classify(
        locale
      )}`;
      addFormLocaleToRootI18n(
        host,
        rootI18nPath,
        locale,
        localePath,
        localeTsName
      );
    }

    return chain([
      ...languageTemplateSources.map((source) => mergeWith(source)),
    ]);
  };
}

/**
 * Sets the component's HTML content.
 * @param options Schematic options.
 * @param path Path where the component is being created (independently of
 * whether it is flat).
 * @param name Component name.
 * @param htmlContent HTML content to set.
 * @returns Schematic rule.
 */
function setHtml(
  options: FormOptions,
  path: Path,
  name: string,
  htmlContent: string
): Rule {
  return (host: Tree) => {
    if (!options.inlineTemplate) {
      const componentHtmlPath = join(
        path,
        options.flat ? '' : strings.dasherize(name),
        strings.dasherize(name) + '.component.html'
      );
      host.overwrite(componentHtmlPath, htmlContent);
    } else {
      const componentTsPath = join(
        path,
        options.flat ? '' : strings.dasherize(name),
        strings.dasherize(name) + '.component.ts'
      );
      setTsComponentInlineTemplate(host, componentTsPath, htmlContent);
    }
  };
}

/**
 * Adds a way of accessing the component's path when needed.
 * @param options Schematic options.
 * @param path Path where the component is being created (independently of
 * whether it is flat).
 */
function editComponentTs(
  options: FormOptions,
  path: Path,
  name: string,
  storagePath: string,
  routeParams: string[]
): Rule {
  return (host: Tree) => {
    if (routeParams.length > 0) {
      const componentTsPath = join(
        path,
        options.flat ? '' : strings.dasherize(name),
        strings.dasherize(name) + '.component.ts'
      );

      // Add `lfRouter` to constructor
      addTsComponentConstructorParameter(
        host,
        componentTsPath,
        'lfRouter',
        'LfRouter',
        ['private'],
        [new TsImport('LfRouter', '@lightweightform/core')]
      );

      // Set `path` getter
      addTsComponentGetter(
        host,
        componentTsPath,
        'path',
        'string',
        tsComponentPathGetterContent(storagePath, routeParams),
        ['@computed'],
        [new TsImport('computed', 'mobx-angular')]
      );
    }
  };
}

/**
 * Add a route to the form.
 * @param options Schematic options.
 * @param routerModulePath Path of the router module.
 * @param name Form name.
 * @param project Project where the form is being created.
 * @param storagePath Path of the form in the storage.
 * @returns Schematic rule.
 */
function addRoute(
  options: FormOptions,
  routerModulePath: Path | undefined,
  path: Path,
  name: string,
  storagePath: string,
  routeParams: string[]
): Rule {
  return (host: Tree) => {
    if (!routerModulePath) {
      return console.warn(
        'Routing module not found. Skipping insertion of component route.'
      );
    }

    let i = 0;
    const routePath = storagePath
      .slice(1)
      .split('/')
      .map((id) => (id === '?' ? `:${routeParams[i++]}` : id))
      .join('/');

    // Add a redirect to the route when there are no routes
    if (!hasRoutes(host, routerModulePath)) {
      addLfRoute(host, routerModulePath, {
        routePath: '',
        storagePath: '/',
        redirectTo: routePath,
        pathMatch: 'full',
      });
    }

    const componentTsName = `${strings.classify(name)}Component`;
    const componentFilePath = join(
      path,
      options.flat ? '' : strings.dasherize(name),
      strings.dasherize(name) + '.component.ts'
    );
    addLfRoute(host, routerModulePath, {
      routePath,
      storagePath,
      componentTsName,
      componentFilePath,
    });
  };
}
