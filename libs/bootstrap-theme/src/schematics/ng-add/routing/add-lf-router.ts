import { Path } from '@angular-devkit/core';
import { Rule, Tree } from '@angular-devkit/schematics';
import {
  addNgModuleProvider,
  addNgModuleValueProvider,
  changeRoutesToLfRoutes,
  getModulePath,
  getProject,
} from '@lightweightform/theme-common/schematics';

import { getLocalesArrayFromString } from '../../utils/locales';
import { NgAddOptions } from '../ng-add.options';

/**
 * Adds the application schema to the project.
 * @param options Schematic options.
 * @returns Schematic rule.
 */
export function addLfRouter(options: NgAddOptions): Rule {
  return async (host: Tree) => {
    const project = await getProject(host, options.project);
    let modulePath: Path;
    try {
      modulePath = getModulePath(host, project, options.module, true);
    } catch (err) {
      return console.warn(
        'Routing module not found. Skipping incorporation of `LfRouter`.'
      );
    }

    addNgModuleProvider(host, modulePath, 'LfRouter', '@lightweightform/core');
    addNgModuleValueProvider(
      host,
      modulePath,
      'LF_ROUTER_BASE_PATH',
      '@lightweightform/core',
      `'${options.routerBasePath}'`
    );
    const localesArr = getLocalesArrayFromString(options.locales);
    if (localesArr.length > 1) {
      addNgModuleValueProvider(
        host,
        modulePath,
        'LF_I18N_LANGUAGE_QUERY_PARAM',
        '@lightweightform/core',
        `'l'`
      );
    }

    changeRoutesToLfRoutes(host, modulePath);
  };
}
