/**
 * Bootstrap version in `X.Y.Z` format (replaced by Rollup in compile time).
 */
declare const __bootstrapVersion__: string;

/**
 * Content for the file that imports LF's Bootstrap-theme styles.
 * @param fileType Type of file where the content will be inserted.
 * @returns File content to import the theme.
 */
export function themeStylesContent(fileType: 'scss' | 'sass'): string {
  const semicolon = fileType === 'scss' ? ';' : '';
  const bsVersion = __bootstrapVersion__;
  const bsVersionMajMin = bsVersion.match(/^\d+\.\d+/)![0];
  // TODO: Get Bootstrap version from dependencies
  return `\
// Upstream Bootstrap variables and theme variables may be overridden here. See:
// https://getbootstrap.com/docs/${bsVersionMajMin}/getting-started/theming/#variable-defaults
// and https://github.com/twbs/bootstrap/blob/v${bsVersion}/scss/_variables.scss to
// override Bootstrap variables. LF theme variables names can be found at:
// https://bitbucket.org/opensoftgitrepo/lightweightform/src/master/libs/bootstrap-theme/src/scss-utils/_variables.scss

@import '~@lightweightform/bootstrap-theme/scss/bootstrap-theme'${semicolon}`;
}

/**
 * Whether an existing file content already imports the theme's styles.
 * @param content Current file content.
 * @returns Whether the current file current already imports the theme's styles.
 */
export function containsImport(content: string): boolean {
  const importRegExp =
    /@import\s+('|")~@lightweightform\/bootstrap-theme\/scss\/bootstrap-theme\1/;
  return content.match(importRegExp) !== null;
}
