import { join, normalize, Path, workspaces } from '@angular-devkit/core';
import { Rule, Tree } from '@angular-devkit/schematics';
import {
  addStylePathToProjectTarget,
  getProject,
  getProjectStyleFilePath,
} from '@lightweightform/theme-common/schematics';
import { InsertChange } from '@schematics/angular/utility/change';

import { NgAddOptions } from '../ng-add.options';

import { containsImport, themeStylesContent } from './theme-styles-content';

/**
 * File used to import the Bootstrap-theme styles.
 */
export const themeStylesFilename = 'lf-bootstrap-theme.scss';

/**
 * Imports the Bootstrap-theme styles to the project.
 * @param options Schematic options.
 * @returns Schematic rule.
 */
export function addThemeStyles(options: NgAddOptions): Rule {
  const projectName = options.project;
  return async (host: Tree) => {
    const project = await getProject(host, projectName);
    const stylesPath =
      getProjectStyleFilePath(project, 'scss') ||
      getProjectStyleFilePath(project, 'sass');

    const themeStylesPath = themeStylesFilePath(project);
    if (!stylesPath || stylesPath === themeStylesPath) {
      if (host.exists(themeStylesPath)) {
        return console.warn(
          `File "${themeStylesPath}" already exists. Skipping import of ` +
            'styles.'
        );
      }

      host.create(themeStylesPath, themeStylesContent('scss'));
      addStylePathToProjectTarget(host, projectName, 'build', themeStylesPath);
      addStylePathToProjectTarget(host, projectName, 'test', themeStylesPath);
      return;
    }

    // Add theme styles only if they haven't already been imported
    if (!containsImport(host.read(stylesPath)!.toString('utf-8'))) {
      const ext = stylesPath.slice(-4) as 'scss' | 'sass';
      const insertion = new InsertChange(
        stylesPath,
        0,
        `${themeStylesContent(ext)}\n\n`
      );
      const recorder = host.beginUpdate(stylesPath);

      recorder.insertLeft(insertion.pos, insertion.toAdd);
      host.commitUpdate(recorder);
    }
  };
}

/**
 * File path of the file used to import the Bootstrap-theme styles.
 * @param project Project on which the styles are being added.
 * @returns File path.
 */
export function themeStylesFilePath(
  project: workspaces.ProjectDefinition
): Path {
  return normalize(
    join(
      (project.sourceRoot as Path) || join(project.root as Path, 'src'),
      themeStylesFilename
    )
  );
}
