import { Rule, Tree } from '@angular-devkit/schematics';
import {
  getIndentation,
  getProject,
  getProjectStyleFilePath,
} from '@lightweightform/theme-common/schematics';

import { NgAddOptions } from '../ng-add.options';

import { themeStylesFilePath } from './add-theme-styles';

/**
 * Adds application specific styles to the project.
 * @param options Schematic options.
 * @returns Schematic rule.
 */
export function addAppStyles(options: NgAddOptions): Rule {
  return async (host: Tree) => {
    const project = await getProject(host, options.project);
    const styleFilePath = getProjectStyleFilePath(project);

    // Verify that the obtained path is not the path used by the schematics to
    // imports the theme's styles
    if (!styleFilePath || styleFilePath === themeStylesFilePath(project)) {
      return console.error(
        'Could not find the default style file for the project.'
      );
    }
    const buffer = host.read(styleFilePath);
    if (!buffer) {
      return console.error(`Could not read "${styleFilePath}" in the project.`);
    }
    const content = buffer.toString('utf-8');

    const indent = getIndentation(content);
    const toInsert =
      '\n' +
      (styleFilePath.endsWith('sass')
        ? `html, body\n${indent}height: 100%\n`
        : `html, body {\n${indent}height: 100%;\n}\n`);

    if (content.includes(toInsert)) {
      return;
    }

    const recorder = host.beginUpdate(styleFilePath);
    recorder.insertLeft(content.length, toInsert);
    host.commitUpdate(recorder);
  };
}
