/**
 * Options for the `ng-add` schematic.
 */
export interface NgAddOptions {
  /**
   * Name of the project.
   */
  project: string;
  /**
   * Module on which to set up LF.
   */
  module?: string;
  /**
   * Locales to support for i18n (comma separated).
   */
  locales: string;
  /**
   * Base path to set for the LF routing.
   */
  routerBasePath: string;
  /**
   * Name of the folder where i18n translations are kept.
   */
  i18nFolderName: string;
  /**
   * Whether to skip generation of test files.
   */
  skipTests: boolean;
  /**
   * Whether to run linter after setting up LF.
   */
  lintFix: boolean;
}
