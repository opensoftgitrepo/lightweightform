/**
 * Content for the HTML file that should contain an `<lf-app>`.
 * @param setRouterOutlet Whether to set the `<router-outlet>`.
 * @returns File content containing an `<lf-app>`.
 */
export function htmlComponentContent(setRouterOutlet: boolean = true): string {
  return `\
<lf-app>
  <!-- Top-bar of the application -->
  <lf-top-bar [actions]="actions">
    <!-- Title -->
    <lf-i18n key="label"></lf-i18n>
  </lf-top-bar>

  <!-- Tree-nav -->
  <lf-tree-nav></lf-tree-nav>

  <!-- Content -->
  ${
    setRouterOutlet
      ? '<router-outlet></router-outlet>'
      : '<!-- <router-outlet></router-outlet> -->'
  }
</lf-app>`;
}

/**
 * Actions to set as a property in the component's TS file.
 * @param localesArr Array of locales the application should support.
 * @returns Actions to set in the component's TS file.
 */
export function tsComponentActions(localesArr: string[]): string {
  return `\
[
  {
    id: 'save',
    style: 'outline-secondary',
    icon: 'save',
    callback: () => this.save()
  },
  {
    id: 'load',
    style: 'outline-secondary',
    icon: 'folder-open',
    isDisabled: () => !this.lfFileStorage.loadIsSupported,
    callback: () => this.load()
  },${
    localesArr.length > 1
      ? `
  {
    id: 'language',
    style: 'outline-primary',
    icon: 'language',
    options: this.lfI18n.languages.map(lang => ({
      id: lang,
      value: lang,
      isActive: () => this.lfI18n.currentLanguage === lang
    })),
    callback: lang => this.lfI18n.setCurrentLanguage(lang)
  },`
      : ''
  }
  {
    id: 'validate',
    style: 'outline-danger',
    icon: 'check-square-o',
    callback: () => this.lfApp.validate()
  },
  {
    id: 'submit',
    style: 'outline-success',
    icon: 'send',
    callback: () => this.submit()
  }
]`;
}

/**
 * Method signature and body to set as the `ngOnInit` method of the component's
 * TS file.
 * @returns Method signature and body to set in the component's TS file.
 */
export function tsComponentNgOnInit(): string {
  return `\
() {
  if (!isDevMode()) {
    this.lfUnloadAlert.enable();
  }
}`;
}

/**
 * Method signature and body to set as the `save` method of the component's TS
 * file.
 * @returns Method signature and body to set in the component's TS file.
 */
export function tsComponentSave(moduleName: string): string {
  return `\
(): Promise<void> {
  try {
    const dateStr = new Date().toISOString().replace(/[T:.]/g, '-');
    const fileName = \`${moduleName}-\${dateStr}\`;
    await this.lfFileStorage.saveToFile('/', fileName);
    this.lfStorage.setPristine();
  } catch (err) {
    console.error('Error saving file:', err);
  }
}`;
}

/**
 * Method signature and body to set as the `load` method of the component's TS
 * file.
 * @returns Method signature and body to set in the component's TS file.
 */
export function tsComponentLoad(): string {
  return `\
(): Promise<void> {
  try {
    await this.lfFileStorage.loadFromFile('/');
    this.lfStorage.setTouched('/', true);
  } catch (err) {
    console.error('Error loading file:', err);
  }
}`;
}

/**
 * Method signature and body to set as the `submit` method of the component's TS
 * file.
 * @returns Method signature and body to set in the component's TS file.
 */
export function tsComponentSubmit(): string {
  return `\
(): void {
  if (!this.lfStorage.hasErrors()) {
    const appJSON = JSON.stringify(this.lfStorage.getAsJS());
    alert(appJSON); // Do something with the JSON
  } else {
    this.lfApp.validate();
  }
}`;
}
