import { dirname, join, Path } from '@angular-devkit/core';
import {
  chain,
  externalSchematic,
  Rule,
  Tree,
} from '@angular-devkit/schematics';
import {
  addTsComponentConstructorParameter,
  addTsComponentInterface,
  addTsComponentMethod,
  addTsComponentProperty,
  getModuleNameFromPath,
  getModulePath,
  getProject,
  htmlContainsTag,
  isUnmodifiedHtmlAppComponent,
  isUnmodifiedHtmlModuleComponent,
  TsImport,
} from '@lightweightform/theme-common/schematics';

import { getLocalesArrayFromString } from '../../utils/locales';
import { NgAddOptions } from '../ng-add.options';

import {
  htmlComponentContent,
  tsComponentActions,
  tsComponentLoad,
  tsComponentNgOnInit,
  tsComponentSave,
  tsComponentSubmit,
} from './lf-app-content';

/**
 * Adds an `<lf-app>` to the project.
 * @param options Schematic options.
 * @returns Schematic rule.
 */
export function addLfApp(options: NgAddOptions): Rule {
  return async (host: Tree) => {
    const project = await getProject(host, options.project);
    const modulePath = getModulePath(host, project, options.module);
    const moduleName = getModuleNameFromPath(modulePath);
    const moduleDir = dirname(modulePath);
    const componentTsPath = join(moduleDir, `${moduleName}.component.ts`);
    const componentHtmlPath = join(moduleDir, `${moduleName}.component.html`);
    const localesArr = getLocalesArrayFromString(options.locales);

    if (!host.exists(componentTsPath)) {
      return chain([
        externalSchematic('@schematics/angular', 'component', {
          ...options,
          flat: true,
          path: moduleDir,
          name: moduleName,
        }),
        (h: Tree) => h.overwrite(componentHtmlPath, htmlComponentContent()),
        editComponentTs(moduleName, localesArr, componentTsPath),
      ]);
    }

    return chain([
      appendLfAppHtml(
        moduleName,
        localesArr,
        componentTsPath,
        componentHtmlPath
      ),
    ]);
  };
}

/**
 * Append the `<lf-app>` to the given HTML file and set the default `actions` in
 * the TS file.
 * @param componentTsPath Component's TS file path.
 * @param componentHtmlPath Component's HTML file path.
 * @returns Schematic rule.
 */
function appendLfAppHtml(
  moduleName: string,
  localesArr: string[],
  componentTsPath: Path,
  componentHtmlPath: Path
): Rule {
  return (host: Tree) => {
    if (!host.exists(componentHtmlPath)) {
      return chain([
        (h: Tree) => h.create(componentHtmlPath, htmlComponentContent()),
        editComponentTs(moduleName, localesArr, componentTsPath),
      ]);
    }
    const currentHtml = host.read(componentHtmlPath)!.toString('utf-8');
    if (!htmlContainsTag(currentHtml, 'lf-app')) {
      return chain([
        (h: Tree) => {
          let newHtml: string;
          if (
            (moduleName === 'app' &&
              isUnmodifiedHtmlAppComponent(currentHtml)) ||
            (moduleName !== 'app' &&
              isUnmodifiedHtmlModuleComponent(currentHtml, moduleName))
          ) {
            newHtml = htmlComponentContent();
          } else {
            console.warn(
              `Prepended \`<lf-app>\` in "${componentHtmlPath.slice(1)}" ` +
                'since we cannot safely replace the existing content. Make ' +
                'sure to adapt the file to your needs.'
            );
            newHtml =
              htmlComponentContent(
                !htmlContainsTag(currentHtml, 'router-outlet')
              ) + `\n\n${currentHtml}`;
          }
          h.overwrite(componentHtmlPath, newHtml);
        },
        editComponentTs(moduleName, localesArr, componentTsPath),
      ]);
    }
  };
}

/**
 * Sets the default `actions` in the component's TS file and the `ngOnInit`.
 * @param moduleName Name of the module we are working on.
 * @param componentTsPath Component's TS file path.
 * @returns Schematic rule.
 */
function editComponentTs(
  moduleName: string,
  localesArr: string[],
  componentTsPath: Path
): Rule {
  return (host: Tree) => {
    // Set actions property
    addTsComponentProperty(
      host,
      componentTsPath,
      'actions',
      'ActionsMenuAction[]',
      tsComponentActions(localesArr),
      undefined,
      [new TsImport('ActionsMenuAction', '@lightweightform/bootstrap-theme')]
    );

    // Add a property for referencing the `<lf-app>` (to call `validate` on)
    const importName = moduleName === 'app' ? 'LfAppComponent' : 'AppComponent';
    addTsComponentProperty(
      host,
      componentTsPath,
      'lfApp',
      importName,
      undefined,
      [`@ViewChild(${importName})`],
      [
        new TsImport(
          'AppComponent',
          '@lightweightform/bootstrap-theme',
          importName
        ),
        new TsImport('ViewChild', '@angular/core'),
      ]
    );

    // Inject `LfStorage`, `LfFileStorage`, `LfI18n`, and `LfUnloadAlert` when
    // appropriate
    addTsComponentConstructorParameter(
      host,
      componentTsPath,
      'lfStorage',
      'LfStorage',
      ['public'],
      [new TsImport('LfStorage', '@lightweightform/core')]
    );
    addTsComponentConstructorParameter(
      host,
      componentTsPath,
      'lfFileStorage',
      'LfFileStorage',
      ['public'],
      [new TsImport('LfFileStorage', '@lightweightform/core')]
    );
    if (localesArr.length > 1) {
      addTsComponentConstructorParameter(
        host,
        componentTsPath,
        'lfI18n',
        'LfI18n',
        ['public'],
        [new TsImport('LfI18n', '@lightweightform/core')]
      );
    }
    addTsComponentConstructorParameter(
      host,
      componentTsPath,
      'lfUnloadAlert',
      'LfUnloadAlert',
      ['public'],
      [new TsImport('LfUnloadAlert', '@lightweightform/core')]
    );

    // Add mock ngOnInit, save, load, and submit methods
    addTsComponentInterface(
      host,
      componentTsPath,
      'OnInit',
      new TsImport('OnInit', '@angular/core')
    );
    addTsComponentMethod(
      host,
      componentTsPath,
      'ngOnInit',
      tsComponentNgOnInit(),
      undefined,
      [new TsImport('isDevMode', '@angular/core')]
    );
    addTsComponentMethod(
      host,
      componentTsPath,
      'save',
      tsComponentSave(moduleName),
      ['async']
    );
    addTsComponentMethod(host, componentTsPath, 'load', tsComponentLoad(), [
      'async',
    ]);
    addTsComponentMethod(host, componentTsPath, 'submit', tsComponentSubmit());
  };
}
