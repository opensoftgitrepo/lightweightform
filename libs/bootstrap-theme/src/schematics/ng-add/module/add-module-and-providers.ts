import { Rule, Tree } from '@angular-devkit/schematics';
import {
  addNgModuleClassProvider,
  addNgModuleImport,
  addNgModuleProvider,
  getModulePath,
  getProject,
} from '@lightweightform/theme-common/schematics';

import { NgAddOptions } from '../ng-add.options';

/**
 * Adds the theme module and required services to the root `NgModule`.
 * @param options Schematic options.
 * @returns Schematic rule.
 */
export function addThemeModuleAndProviders(options: NgAddOptions): Rule {
  return async (host: Tree) => {
    const project = await getProject(host, options.project);
    const modulePath = getModulePath(host, project, options.module);

    // Add `LfBootstrapThemeModule`
    addNgModuleImport(
      host,
      modulePath,
      'LfBootstrapThemeModule',
      '@lightweightform/bootstrap-theme'
    );

    // Add `LfStorage`, `LfFileStorage`, `LfI18n`, and `LfUnloadAlert` services
    addNgModuleProvider(host, modulePath, 'LfStorage', '@lightweightform/core');
    addNgModuleProvider(
      host,
      modulePath,
      'LfFileStorage',
      '@lightweightform/core'
    );
    addNgModuleProvider(host, modulePath, 'LfI18n', '@lightweightform/core');
    addNgModuleProvider(
      host,
      modulePath,
      'LfUnloadAlert',
      '@lightweightform/core'
    );

    // Add the JSON serialiser `LfJsonSerializer` as the file storage serialiser
    addNgModuleClassProvider(
      host,
      modulePath,
      'LfSerializer',
      '@lightweightform/core',
      'LfJsonSerializer',
      '@lightweightform/core'
    );
  };
}
