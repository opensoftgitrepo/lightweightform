import { dirname, strings } from '@angular-devkit/core';
import {
  apply,
  applyTemplates,
  chain,
  FileEntry,
  forEach,
  mergeWith,
  move,
  Rule,
  Source,
  Tree,
  url,
} from '@angular-devkit/schematics';
import {
  addNgModuleValueProvider,
  getModuleNameFromPath,
  getModulePath,
  getProject,
  setProjectSchematicOptions,
} from '@lightweightform/theme-common/schematics';

import { getLocalesArrayFromString } from '../../utils/locales';
import { NgAddOptions } from '../ng-add.options';

/**
 * Default i18n folder name.
 */
// tslint:disable-next-line:no-var-requires
export const defaultI18nFolderName = require('./ng-add/ng-add.schema.json')
  .properties.i18nFolderName.default;

/**
 * Locales supported by the Bootstrap theme.
 */
export const themeSupportedLocales = {
  'en-US': 'I18N_EN_US',
  'pt-PT': 'I18N_PT_PT',
};

/**
 * Adds the application i18n to the project.
 * @param options Schematic options.
 * @returns Schematic rule.
 */
export function addI18n(options: NgAddOptions): Rule {
  return async (host: Tree) => {
    const project = await getProject(host, options.project);
    const modulePath = getModulePath(host, project, options.module);
    const moduleDir = dirname(modulePath);
    const moduleName = getModuleNameFromPath(modulePath);
    const localesArr = getLocalesArrayFromString(options.locales);

    // Theme supported locales filtered by the ones specified by the user
    const commonLocales = Object.keys(themeSupportedLocales).reduce(
      (obj, locale) => {
        if (localesArr.indexOf(locale) !== -1) {
          obj[locale] = themeSupportedLocales[locale];
        }
        return obj;
      },
      {}
    );

    let skipI18n = false;
    const templateSource = apply(url('ng-add/i18n/files'), [
      applyTemplates({
        ...strings,
        ...options,
        moduleName,
        commonLocales,
        localesArr,
      }),
      move(moduleDir),
      forEach((entry: FileEntry) => {
        const path = entry.path;
        if (host.exists(path)) {
          if (path.endsWith('.i18n.ts')) {
            console.warn(
              `File "${path.slice(1)}" already exists. Skipping i18n ` +
                'generation.'
            );
            skipI18n = true;
          }
          return null;
        }
        return entry;
      }),
    ]);

    // Per language sources
    const localeTemplateSources: Source[] = [];
    if (!skipI18n) {
      for (const locale of localesArr) {
        localeTemplateSources.push(
          apply(url('ng-add/i18n/per-locale-files'), [
            applyTemplates({
              ...strings,
              ...options,
              moduleName,
              localesArr,
              locale,
            }),
            move(`${moduleDir}/${options.i18nFolderName}`),
            forEach((entry: FileEntry) =>
              host.exists(entry.path) ? null : entry
            ),
          ])
        );
      }
    }

    return chain([
      saveI18nOptions(options),
      ...(skipI18n
        ? []
        : [
            mergeWith(templateSource),
            ...localeTemplateSources.map((source) => mergeWith(source)),
            importAppI18n(options),
          ]),
    ]);
  };
}

/**
 * Imports the i18n object in the module.
 * @param options Schematic options.
 * @returns Schematic rule.
 */
function importAppI18n(options: NgAddOptions): Rule {
  return async (host: Tree) => {
    const project = await getProject(host, options.project);
    const modulePath = getModulePath(host, project, options.module);
    const moduleName = getModuleNameFromPath(modulePath);

    addNgModuleValueProvider(
      host,
      modulePath,
      'LF_APP_I18N',
      '@lightweightform/core',
      `${strings.camelize(moduleName)}I18n`,
      `./${strings.dasherize(moduleName)}.i18n`
    );
  };
}

/**
 * Saves the app locales and name of the i18n folder within `angular.json` when
 * necessary.
 * @param options Schematic options.
 * @returns Schematic rule.
 */
function saveI18nOptions(options: NgAddOptions): Rule {
  return (host: Tree) => {
    setProjectSchematicOptions(
      host,
      options.project,
      '@lightweightform/bootstrap-theme:form',
      { locales: options.locales }
    );

    if (options.i18nFolderName !== defaultI18nFolderName) {
      setProjectSchematicOptions(
        host,
        options.project,
        '@lightweightform/bootstrap-theme:form',
        { i18nFolderName: options.i18nFolderName }
      );
    }
  };
}
