import { chain, noop, Rule, Tree } from '@angular-devkit/schematics';
import {
  getProjectSchematicOptions,
  setProjectSchematicOptions,
} from '@lightweightform/theme-common/schematics';
import { applyLintFix } from '@schematics/angular/utility/lint-fix';

import { addI18n } from './i18n/add-i18n';
import { addLfApp } from './lf-app/add-lf-app';
import { addThemeModuleAndProviders } from './module/add-module-and-providers';
import { NgAddOptions } from './ng-add.options';
import { addLfRouter } from './routing/add-lf-router';
import { addAppSchema } from './schema/add-app-schema';
import { addAppStyles } from './styles/add-app-styles';
import { addThemeStyles } from './styles/add-theme-styles';

/**
 * `ng-add-setup` schematic function.
 * @param options Schematic options.
 * @returns Schematic rule.
 */
export function ngAddSetup(options: NgAddOptions): Rule {
  return chain([
    copyAngularComponentSchematicOptions(options),
    addThemeModuleAndProviders(options),
    addThemeStyles(options),
    addAppStyles(options),
    addAppSchema(options),
    addLfRouter(options),
    addLfApp(options),
    addI18n(options),
    options.lintFix ? applyLintFix() : noop(),
  ]);
}

/**
 * Copies the Angular component schematic options to our form schematic options
 * so that both schematics use the same defaults.
 * @param options Schematic options.
 * @returns Schematic rule.
 */
function copyAngularComponentSchematicOptions(options: NgAddOptions): Rule {
  return (host: Tree) => {
    const ngComponentOptions = getProjectSchematicOptions(
      host,
      options.project,
      '@schematics/angular:component'
    );

    if (ngComponentOptions) {
      setProjectSchematicOptions(
        host,
        options.project,
        '@lightweightform/bootstrap-theme:form',
        ngComponentOptions
      );
    }
  };
}
