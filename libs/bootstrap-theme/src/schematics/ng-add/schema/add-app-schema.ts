import { dirname, strings } from '@angular-devkit/core';
import {
  apply,
  applyTemplates,
  chain,
  FileEntry,
  filter,
  forEach,
  mergeWith,
  move,
  noop,
  Rule,
  Tree,
  url,
} from '@angular-devkit/schematics';
import {
  addNgModuleValueProvider,
  getModuleNameFromPath,
  getModulePath,
  getProject,
} from '@lightweightform/theme-common/schematics';

import { NgAddOptions } from '../ng-add.options';

/**
 * Adds the application schema to the project.
 * @param options Schematic options.
 * @returns Schematic rule.
 */
export function addAppSchema(options: NgAddOptions): Rule {
  return async (host: Tree) => {
    const project = await getProject(host, options.project);
    const modulePath = getModulePath(host, project, options.module);
    const moduleDir = dirname(modulePath);
    const moduleName = getModuleNameFromPath(modulePath);

    let skipSchema = false;
    const templateSource = apply(url('ng-add/schema/files'), [
      options.skipTests
        ? filter((p) => !p.endsWith('.spec.ts.template'))
        : noop(),
      applyTemplates({ ...strings, ...options, moduleName }),
      move(moduleDir),
      forEach((entry: FileEntry) => {
        const path = entry.path;
        if (host.exists(path)) {
          if (path.endsWith('.schema.ts')) {
            console.warn(
              `File "${path.slice(1)}" already exists. Skipping schema ` +
                'generation.'
            );
            skipSchema = true;
          }
          return null;
        }
        return entry;
      }),
    ]);

    return skipSchema
      ? noop()
      : chain([mergeWith(templateSource), importAppSchema(options)]);
  };
}

/**
 * Imports the schema in the module.
 * @param options Schematic options.
 * @returns Schematic rule.
 */
function importAppSchema(options: NgAddOptions): Rule {
  return async (host: Tree) => {
    const project = await getProject(host, options.project);
    const modulePath = getModulePath(host, project, options.module);
    const moduleName = getModuleNameFromPath(modulePath);

    addNgModuleValueProvider(
      host,
      modulePath,
      'LF_APP_SCHEMA',
      '@lightweightform/core',
      `${strings.camelize(moduleName)}Schema`,
      `./${strings.dasherize(moduleName)}.schema`
    );
  };
}
