import {
  Rule,
  SchematicContext,
  TaskId,
  Tree,
} from '@angular-devkit/schematics';
import {
  NodePackageInstallTask,
  RunSchematicTask,
} from '@angular-devkit/schematics/tasks';
import {
  addPackageJsonDependency,
  getPackageJsonDependency,
  NodeDependencyType,
} from '@schematics/angular/utility/dependencies';

import { NgAddOptions } from './ng-add.options';

/**
 * A record containing the map of the theme's peer dependencies to their version
 * (replaced by Rollup in compile time).
 */
declare const __themePeerDependencies__: Record<string, string>;

/**
 * `ng-add` schematic function.
 * @param options Schematic options.
 * @returns Schematic rule.
 */
export function ngAdd(options: NgAddOptions): Rule {
  return (host: Tree, context: SchematicContext) => {
    const themePeerDependencies = __themePeerDependencies__;

    for (const dep of Object.keys(themePeerDependencies)) {
      // Add dependencies only if they don't exist to prevent recording changes
      // on `package.json`
      if (getPackageJsonDependency(host, dep) === null) {
        addPackageJsonDependency(host, {
          type: NodeDependencyType.Default,
          name: dep,
          version: themePeerDependencies[dep],
        });
      }
    }

    const taskDeps: TaskId[] = [];
    // Install missing peer dependencies since we depend on the `theme-common`
    // schematics utilities and only then can we set up the project
    // XXX: Don't install dependencies in development mode since `npm install`
    // overrides `npm link`s (see: https://github.com/npm/npm/issues/17287),
    // thus messing up local projects testing the schematics
    if (process.env.NODE_ENV === 'production') {
      taskDeps.push(context.addTask(new NodePackageInstallTask()));
    }
    context.addTask(new RunSchematicTask('ng-add-setup', options), taskDeps);
  };
}
