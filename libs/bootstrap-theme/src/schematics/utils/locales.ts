/**
 * Returns the list of locales given a string representing them.
 * @param localesStr String representing the locales.
 * @returns List of locales.
 */
export function getLocalesArrayFromString(localesStr: string): string[] {
  return localesStr.split(',').map((l) => l.trim());
}
