import {
  Directive,
  HostBinding,
  HostListener,
  Inject,
  Input,
  OnInit,
  Optional,
} from '@angular/core';
import { LfI18n, LfRouter, LfStorage, PathBased } from '@lightweightform/core';
import {
  DATE_OUT_OF_BOUNDS_CODE,
  DATE_OUT_OF_BOUNDS_TYPE,
  DISALLOWED_VALUE_CODE,
  DISALLOWED_VALUE_TYPE,
  IS_REQUIRED_CODE,
  IS_REQUIRED_TYPE,
  LENGTH_OUT_OF_BOUNDS_CODE,
  LENGTH_OUT_OF_BOUNDS_TYPE,
  NUMBER_OUT_OF_BOUNDS_CODE,
  NUMBER_OUT_OF_BOUNDS_TYPE,
  resolvePath,
  SIZE_OUT_OF_BOUNDS_CODE,
  SIZE_OUT_OF_BOUNDS_TYPE,
  ValidationIssue,
} from '@lightweightform/storage';
import { makeObservable } from 'mobx';
import { computed, observable } from 'mobx-angular';
import { MarkdownService, MarkedRenderer } from 'ngx-markdown';

import { I18N_VALIDATIONS_KEY, I18N_VALUE_LABEL_KEY } from '../../i18n/keys';
import {
  INVALID_DATE_RANGE_CODE,
  INVALID_DATE_RANGE_TYPE,
} from '../../utils/date-range-schema';

/**
 * Protocol prefix used for LF links.
 */
export const LF_URL_PROTOCOL = 'lf:';

// This is deprecated and `LF_URL_PROTOCOL` should be used instead.
const LF_URL_PROTOCOL_OLD = 'lf://';

// Prefix used in the class of anchors linking to LF fields (e.g.
// `<a class="lfAnchor-/field" href="/field">`)
const LF_ANCHOR_CLASS_PREFIX = 'lfAnchor-';

/**
 * Validation message directive.
 */
@Directive({
  selector: 'lf-validation-issue, [lfValidationIssue]',
  exportAs: 'lfValidationIssue',
})
export class ValidationIssueDirective extends PathBased implements OnInit {
  constructor(
    @Optional()
    @Inject(PathBased)
    parentPathBasedComponent: PathBased | null,
    lfStorage: LfStorage,
    lfI18n: LfI18n,
    private lfRouter: LfRouter,
    private markdownService: MarkdownService
  ) {
    super(parentPathBasedComponent, lfStorage, lfI18n);
    makeObservable(this);
  }

  /**
   * HTML string with the issue message to display.
   */
  @computed
  @HostBinding('innerHTML')
  public get issueMessageHTML(): string {
    // Value as obtained from the i18n file
    const i18nValue =
      this.lfI18n.getFromPath(
        this.path,
        `${I18N_VALIDATIONS_KEY}.${this.issue.code}`,
        { callFunctionWithContext: false }
      ) ??
      // If there is no i18n value for the code but the issue has a well known
      // type, then we use the message associated with the default code of said
      // type
      (this.issue.data?.type &&
        this.lfI18n.getFromPath(
          this.path,
          `${I18N_VALIDATIONS_KEY}.${ValidationIssueDirective.defaultCodeForType(
            this.issue.data.type
          )}`,
          { callFunctionWithContext: false }
        ));

    // Obtain markdown message from the value
    return this.markdownService.compile(
      typeof i18nValue === 'function'
        ? i18nValue(
            this.lfStorage.relativeStorage(this.path),
            this.issue.data,
            this.lfI18n
          )
        : i18nValue != null
        ? i18nValue
        : this.issue.code,
      undefined,
      undefined,
      { renderer: this.lfLinksRenderer }
    );
  }

  /**
   * Renderer capable of handling `lf://` links.
   */
  @computed
  private get lfLinksRenderer() {
    const renderer = new MarkedRenderer();

    const originalLink = renderer.link;
    renderer.link = (href, title, text) => {
      // Non-LF link
      if (!href || !href.startsWith(LF_URL_PROTOCOL)) {
        return originalLink.call(renderer, href, title, text);
      }
      // Support old protocol (`lf://`)
      if (href.startsWith(LF_URL_PROTOCOL_OLD)) {
        href = LF_URL_PROTOCOL + href.slice(LF_URL_PROTOCOL_OLD.length);
      }
      // Replace links with the "LF protocol" with the appropriate paths
      const path = resolvePath(this.path, href.slice(LF_URL_PROTOCOL.length));
      const route = this.lfRouter.navigateableRoute(path);
      if (href === text) {
        text = this.labelOf(path);
      }
      href = route! && this.lfRouter.createUrlFromNavigateableRoute(route!);
      const anchor = originalLink.call(renderer, href, title, text);
      return anchor.replace(
        /^<a/,
        `<a class="${LF_ANCHOR_CLASS_PREFIX}${path}"`
      );
    };

    return renderer;
  }

  /**
   * Issue with message to display.
   */
  @observable
  @Input()
  public issue: ValidationIssue;

  /**
   * Default issue code associated with issues of a certain type.
   * @param type Type of the issue.
   * @returns Default issue code.
   */
  private static defaultCodeForType(type: string): string | null {
    switch (type) {
      case IS_REQUIRED_TYPE:
        return IS_REQUIRED_CODE;
      case DISALLOWED_VALUE_TYPE:
        return DISALLOWED_VALUE_CODE;
      case DATE_OUT_OF_BOUNDS_TYPE:
        return DATE_OUT_OF_BOUNDS_CODE;
      case SIZE_OUT_OF_BOUNDS_TYPE:
        return SIZE_OUT_OF_BOUNDS_CODE;
      case NUMBER_OUT_OF_BOUNDS_TYPE:
        return NUMBER_OUT_OF_BOUNDS_CODE;
      case LENGTH_OUT_OF_BOUNDS_TYPE:
        return LENGTH_OUT_OF_BOUNDS_CODE;
      case INVALID_DATE_RANGE_TYPE:
        return INVALID_DATE_RANGE_CODE;
      default:
        return null;
    }
  }

  // Intercept clicks on links with class `lfPathLink` to use the router.
  // Adapted from Angular's `routerLink` implementation.
  @HostListener('click', [
    '$event.target',
    '$event.button',
    '$event.ctrlKey',
    '$event.metaKey',
    '$event.shiftKey',
  ])
  public _onClick(
    target: HTMLElement,
    button: number,
    ctrlKey: boolean,
    metaKey: boolean,
    shiftKey: boolean
  ): boolean {
    const lfAnchorClass = Array.from(target.classList).find((cl) =>
      cl.startsWith(LF_ANCHOR_CLASS_PREFIX)
    );
    if (button !== 0 || ctrlKey || metaKey || shiftKey || !lfAnchorClass) {
      return true;
    }
    this.lfRouter.navigate(lfAnchorClass.slice(LF_ANCHOR_CLASS_PREFIX.length), {
      queryParamsHandling: 'merge',
    });
    return false;
  }

  /**
   * Label associated with the given path.
   * @param path Path from which to fetch label.
   * @returns Label of given path.
   */
  private labelOf(path: string): string {
    return this.lfI18n.getFromPath(path, I18N_VALUE_LABEL_KEY) || path;
  }
}
