import {
  I18nPaths,
  I18N_UNLOAD_ALERT_MESSAGE_KEY,
  LfStorage,
} from '@lightweightform/core';
import {
  DATE_OUT_OF_BOUNDS_CODE,
  DISALLOWED_VALUE_CODE,
  isBooleanSchema,
  IS_REJECTED_CODE,
  IS_REQUIRED_CODE,
  LENGTH_OUT_OF_BOUNDS_CODE,
  NUMBER_OUT_OF_BOUNDS_CODE,
  SIZE_OUT_OF_BOUNDS_CODE,
} from '@lightweightform/storage';

import {
  INVALID_DATE_RANGE_CODE,
  isDateRangeSchema,
} from '../utils/date-range-schema';

import {
  I18N_ACTIONS_MENU_BUTTON_TEXT_KEY,
  I18N_BS_LOCALE_KEY,
  I18N_CLOSE_BUTTON_TOOLTIP_KEY,
  I18N_DATE_FORMAT_KEY,
  I18N_DATE_PLACEHOLDER_KEY,
  I18N_DATE_RANGE_SEPARATOR_KEY,
  I18N_FILE_BROWSE_TEXT_KEY,
  I18N_FILE_CHOOSE_PLACEHOLDER_KEY,
  I18N_FILE_CLEAR_BUTTON_TOOLTIP_KEY,
  I18N_FILE_NONE_CHOSEN_KEY,
  I18N_FORM_CANCEL_REMOVE_BUTTON_TEXT_KEY,
  I18N_FORM_CONFIRM_REMOVE_BUTTON_TEXT_KEY,
  I18N_FORM_ENSURE_REMOVE_TEXT_KEY,
  I18N_FORM_HELP_ACTION_TEXT_KEY,
  I18N_FORM_REMOVE_ACTION_TEXT_KEY,
  I18N_NUMBER_DECIMAL_SEPARATOR_KEY,
  I18N_NUMBER_THOUSANDS_SEPARATOR_KEY,
  I18N_SELECTION_NO_OPTIONS_TEXT_KEY,
  I18N_SELECTION_OPTIONS_KEY,
  I18N_TABLE_ADD_ROW_ACTION_TEXT_KEY,
  I18N_TABLE_NO_ROWS_TEXT_KEY,
  I18N_TABLE_REMOVE_ROWS_ACTION_TEXT_KEY,
  I18N_TREE_NAV_ADD_OPTIONAL_VALUE_BUTTON_TEXT_KEY,
  I18N_TREE_NAV_ADD_VALUE_TO_LIST_BUTTON_TEXT_KEY,
  I18N_VALIDATIONS_KEY,
  I18N_VALIDATION_PANEL_ERRORS_TEXT_KEY,
  I18N_VALIDATION_PANEL_FOCUS_TEXT_KEY,
  I18N_VALIDATION_PANEL_NEXT_BUTTON_TOOLTIP_KEY,
  I18N_VALIDATION_PANEL_NO_ISSUES_TEXT_KEY,
  I18N_VALIDATION_PANEL_PREV_BUTTON_TOOLTIP_KEY,
  I18N_VALIDATION_PANEL_WARNINGS_TEXT_KEY,
  I18N_VALUE_RETRY_VALIDATE_TEXT_KEY,
} from './keys';

/**
 * Default english translations for Bootstrap theme strings.
 */
export const I18N_EN_US: I18nPaths = {
  '*': {
    // App
    [I18N_BS_LOCALE_KEY]: 'en',
    // Unload alert service
    [I18N_UNLOAD_ALERT_MESSAGE_KEY]:
      'Are you sure you want to leave? Changes that you made may not be saved.',
    // Value
    [I18N_VALUE_RETRY_VALIDATE_TEXT_KEY]: 'Retry validation',
    // Tree-nav
    [I18N_TREE_NAV_ADD_OPTIONAL_VALUE_BUTTON_TEXT_KEY]: 'Add',
    [I18N_TREE_NAV_ADD_VALUE_TO_LIST_BUTTON_TEXT_KEY]: 'Add',
    // Closeable
    [I18N_CLOSE_BUTTON_TOOLTIP_KEY]: 'Close',
    // Actions-menu
    [I18N_ACTIONS_MENU_BUTTON_TEXT_KEY]: 'Actions',
    // Validation panel
    [I18N_VALIDATION_PANEL_ERRORS_TEXT_KEY]: 'Errors:',
    [I18N_VALIDATION_PANEL_WARNINGS_TEXT_KEY]: 'Warnings:',
    [I18N_VALIDATION_PANEL_NO_ISSUES_TEXT_KEY]: 'Everything looks great!',
    [I18N_VALIDATION_PANEL_FOCUS_TEXT_KEY]: 'Focus field',
    [I18N_VALIDATION_PANEL_NEXT_BUTTON_TOOLTIP_KEY]: 'Next field',
    [I18N_VALIDATION_PANEL_PREV_BUTTON_TOOLTIP_KEY]: 'Previous field',
    // Form
    [I18N_FORM_HELP_ACTION_TEXT_KEY]: 'Help',
    [I18N_FORM_REMOVE_ACTION_TEXT_KEY]: 'Remove',
    [I18N_FORM_ENSURE_REMOVE_TEXT_KEY]:
      'Please confirm that you wish to remove this form.',
    [I18N_FORM_CANCEL_REMOVE_BUTTON_TEXT_KEY]: 'Cancel',
    [I18N_FORM_CONFIRM_REMOVE_BUTTON_TEXT_KEY]: 'Confirm',
    // Date
    [I18N_DATE_FORMAT_KEY]: 'M/D/YYYY',
    [I18N_DATE_PLACEHOLDER_KEY]: 'm/d/yyyy',
    // Date-range
    [I18N_DATE_RANGE_SEPARATOR_KEY]: ' - ',
    // File
    [I18N_FILE_CHOOSE_PLACEHOLDER_KEY]: 'Choose file',
    [I18N_FILE_BROWSE_TEXT_KEY]: 'Browse',
    [I18N_FILE_NONE_CHOSEN_KEY]: 'No file chosen',
    [I18N_FILE_CLEAR_BUTTON_TOOLTIP_KEY]: 'Clear',
    // Number
    [I18N_NUMBER_DECIMAL_SEPARATOR_KEY]: '.',
    [I18N_NUMBER_THOUSANDS_SEPARATOR_KEY]: '',
    // Table
    [I18N_TABLE_ADD_ROW_ACTION_TEXT_KEY]: 'Add row',
    [I18N_TABLE_REMOVE_ROWS_ACTION_TEXT_KEY]: 'Remove rows',
    [I18N_TABLE_NO_ROWS_TEXT_KEY]: 'No rows to display.',
    // Selection
    [I18N_SELECTION_NO_OPTIONS_TEXT_KEY]: 'No options available.',
    // Validations
    [I18N_VALIDATIONS_KEY]: {
      [IS_REJECTED_CODE]: 'An error occurred validating this field.',
      [IS_REQUIRED_CODE]: 'The field is required.',
      [DISALLOWED_VALUE_CODE]: (ctx, { allowedValues }) => {
        if (isBooleanSchema(ctx.schema()) && allowedValues.length === 1) {
          return `The field must${
            allowedValues[0] === true ? '' : ' not'
          } be ticked.`;
        }
        return `The field's value must be one of: ${allowedValues.join(', ')}.`;
      },
      [INVALID_DATE_RANGE_CODE]:
        'The range is invalid. The start date must not be after the end date.',
      [DATE_OUT_OF_BOUNDS_CODE]: (ctx, { minDate, maxDate }) => {
        // TODO: Format dates properly
        if (isDateRangeSchema(ctx.schema())) {
          return maxDate === undefined
            ? `The start date mustn't be before ${minDate}.`
            : minDate === undefined
            ? `The end date mustn't be after ${maxDate}.`
            : `The range must be between ${minDate} and ${maxDate}.`;
        }
        return maxDate === undefined
          ? `The date mustn't be before ${minDate}.`
          : minDate === undefined
          ? `The date mustn't be after ${maxDate}.`
          : `The date must be between ${minDate} and ${maxDate}.`;
      },
      [SIZE_OUT_OF_BOUNDS_CODE]: (
        ctx: LfStorage,
        { size, minSize, maxSize }
      ) => {
        if (size === 0 && minSize > 0) {
          return 'The field is required.';
        }
        // Checkbox group
        if (ctx.hasStateProperty('.', `i18n(${I18N_SELECTION_OPTIONS_KEY})`)) {
          return maxSize === undefined
            ? `A minimum of ${+minSize!} option${
                +minSize === 1 ? '' : 's'
              } must be selected.`
            : minSize === undefined
            ? `A maximum of ${+maxSize} option${
                +maxSize === 1 ? '' : 's'
              } must be selected.`
            : maxSize === minSize
            ? `${+maxSize} option${+maxSize === 1 ? '' : 's'} must be selected.`
            : `Between ${+minSize} and ${+maxSize} options must be selected.`;
        }
        // Tables and form-lists
        return maxSize === undefined
          ? `The collection must have at least ${+minSize!} element${
              +minSize === 1 ? '' : 's'
            }.`
          : minSize === undefined
          ? `The collection must have no more than ${+maxSize} element${
              +maxSize === 1 ? '' : 's'
            }.`
          : maxSize === minSize
          ? `The collection must have ${+maxSize} element${
              +maxSize === 1 ? '' : 's'
            }.`
          : `The collection must have between ${+minSize} and ${+maxSize} ` +
            'elements.';
      },
      [NUMBER_OUT_OF_BOUNDS_CODE]: (_, { min, max }) => {
        return max === undefined
          ? `The value mustn't be lesser than ${+min!}.`
          : min === undefined
          ? `The value mustn't be greater than ${+max}.`
          : `The value must be between ${+min} and ${+max}.`;
      },
      [LENGTH_OUT_OF_BOUNDS_CODE]: (_, { length, minLength, maxLength }) => {
        if (length === 0 && minLength > 0) {
          return 'The field is required.';
        }
        return maxLength === undefined
          ? `The value must have at least ${+minLength!} character${
              +minLength! === 1 ? '' : 's'
            }.`
          : minLength === undefined
          ? `The value must have at most ${+maxLength} character${
              +maxLength === 1 ? '' : 's'
            }.`
          : maxLength === minLength
          ? `The value must have ${+maxLength} character${
              +maxLength === 1 ? '' : 's'
            }.`
          : `The value must have ${+minLength} to ${+maxLength} characters.`;
      },
    },
  },
};
