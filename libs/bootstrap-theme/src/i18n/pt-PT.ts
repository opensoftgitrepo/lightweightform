import {
  I18nPaths,
  I18N_UNLOAD_ALERT_MESSAGE_KEY,
  LfStorage,
} from '@lightweightform/core';
import {
  DATE_OUT_OF_BOUNDS_CODE,
  DISALLOWED_VALUE_CODE,
  isBooleanSchema,
  IS_REJECTED_CODE,
  IS_REQUIRED_CODE,
  LENGTH_OUT_OF_BOUNDS_CODE,
  NUMBER_OUT_OF_BOUNDS_CODE,
  SIZE_OUT_OF_BOUNDS_CODE,
} from '@lightweightform/storage';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { ptBrLocale } from 'ngx-bootstrap/locale';

import {
  INVALID_DATE_RANGE_CODE,
  isDateRangeSchema,
} from '../utils/date-range-schema';

import {
  I18N_ACTIONS_MENU_BUTTON_TEXT_KEY,
  I18N_BS_LOCALE_KEY,
  I18N_CLOSE_BUTTON_TOOLTIP_KEY,
  I18N_DATE_FORMAT_KEY,
  I18N_DATE_PLACEHOLDER_KEY,
  I18N_DATE_RANGE_SEPARATOR_KEY,
  I18N_FILE_BROWSE_TEXT_KEY,
  I18N_FILE_CHOOSE_PLACEHOLDER_KEY,
  I18N_FILE_CLEAR_BUTTON_TOOLTIP_KEY,
  I18N_FILE_NONE_CHOSEN_KEY,
  I18N_FORM_CANCEL_REMOVE_BUTTON_TEXT_KEY,
  I18N_FORM_CONFIRM_REMOVE_BUTTON_TEXT_KEY,
  I18N_FORM_ENSURE_REMOVE_TEXT_KEY,
  I18N_FORM_HELP_ACTION_TEXT_KEY,
  I18N_FORM_REMOVE_ACTION_TEXT_KEY,
  I18N_NUMBER_DECIMAL_SEPARATOR_KEY,
  I18N_NUMBER_THOUSANDS_SEPARATOR_KEY,
  I18N_SELECTION_NO_OPTIONS_TEXT_KEY,
  I18N_SELECTION_OPTIONS_KEY,
  I18N_TABLE_ADD_ROW_ACTION_TEXT_KEY,
  I18N_TABLE_NO_ROWS_TEXT_KEY,
  I18N_TABLE_REMOVE_ROWS_ACTION_TEXT_KEY,
  I18N_TREE_NAV_ADD_OPTIONAL_VALUE_BUTTON_TEXT_KEY,
  I18N_TREE_NAV_ADD_VALUE_TO_LIST_BUTTON_TEXT_KEY,
  I18N_VALIDATIONS_KEY,
  I18N_VALIDATION_PANEL_ERRORS_TEXT_KEY,
  I18N_VALIDATION_PANEL_FOCUS_TEXT_KEY,
  I18N_VALIDATION_PANEL_NEXT_BUTTON_TOOLTIP_KEY,
  I18N_VALIDATION_PANEL_NO_ISSUES_TEXT_KEY,
  I18N_VALIDATION_PANEL_PREV_BUTTON_TOOLTIP_KEY,
  I18N_VALIDATION_PANEL_WARNINGS_TEXT_KEY,
  I18N_VALUE_RETRY_VALIDATE_TEXT_KEY,
} from './keys';

// Define `pt` locale
defineLocale('pt', ptBrLocale);

/**
 * Default portuguese translations for Bootstrap theme strings.
 */
export const I18N_PT_PT: I18nPaths = {
  '*': {
    // App
    [I18N_BS_LOCALE_KEY]: 'pt',
    // Unload alert service
    [I18N_UNLOAD_ALERT_MESSAGE_KEY]:
      'Tem a certeza que pretende sair? Alterações feitas podem não estar guardadas.',
    // Value
    [I18N_VALUE_RETRY_VALIDATE_TEXT_KEY]: 'Voltar a tentar validar',
    // Tree-nav
    [I18N_TREE_NAV_ADD_OPTIONAL_VALUE_BUTTON_TEXT_KEY]: 'Adicionar',
    [I18N_TREE_NAV_ADD_VALUE_TO_LIST_BUTTON_TEXT_KEY]: 'Adicionar',
    // Actions-menu
    [I18N_ACTIONS_MENU_BUTTON_TEXT_KEY]: 'Ações',
    // Closeable
    [I18N_CLOSE_BUTTON_TOOLTIP_KEY]: 'Fechar',
    // Validation panel
    [I18N_VALIDATION_PANEL_ERRORS_TEXT_KEY]: 'Erros:',
    [I18N_VALIDATION_PANEL_WARNINGS_TEXT_KEY]: 'Avisos:',
    [I18N_VALIDATION_PANEL_NO_ISSUES_TEXT_KEY]: 'Nenhum problema a reportar!',
    [I18N_VALIDATION_PANEL_FOCUS_TEXT_KEY]: 'Selecionar campo',
    [I18N_VALIDATION_PANEL_NEXT_BUTTON_TOOLTIP_KEY]: 'Campo seguinte',
    [I18N_VALIDATION_PANEL_PREV_BUTTON_TOOLTIP_KEY]: 'Campo anterior',
    // Form
    [I18N_FORM_HELP_ACTION_TEXT_KEY]: 'Ajuda',
    [I18N_FORM_REMOVE_ACTION_TEXT_KEY]: 'Remover',
    [I18N_FORM_ENSURE_REMOVE_TEXT_KEY]:
      'Por favor confirme que pretende remover este formulário.',
    [I18N_FORM_CANCEL_REMOVE_BUTTON_TEXT_KEY]: 'Cancelar',
    [I18N_FORM_CONFIRM_REMOVE_BUTTON_TEXT_KEY]: 'Confirmar',
    // Date
    [I18N_DATE_FORMAT_KEY]: 'DD/MM/YYYY',
    [I18N_DATE_PLACEHOLDER_KEY]: 'dd/mm/aaaa',
    // Date-range
    [I18N_DATE_RANGE_SEPARATOR_KEY]: ' - ',
    // File
    [I18N_FILE_CHOOSE_PLACEHOLDER_KEY]: 'Escolher ficheiro',
    [I18N_FILE_BROWSE_TEXT_KEY]: 'Procurar',
    [I18N_FILE_NONE_CHOSEN_KEY]: 'Nenhum ficheiro escolhido',
    [I18N_FILE_CLEAR_BUTTON_TOOLTIP_KEY]: 'Limpar',
    // Number
    [I18N_NUMBER_DECIMAL_SEPARATOR_KEY]: ',',
    [I18N_NUMBER_THOUSANDS_SEPARATOR_KEY]: '',
    // Table
    [I18N_TABLE_ADD_ROW_ACTION_TEXT_KEY]: 'Adicionar linha',
    [I18N_TABLE_REMOVE_ROWS_ACTION_TEXT_KEY]: 'Remover linhas',
    [I18N_TABLE_NO_ROWS_TEXT_KEY]: 'Nenhuma linha a mostrar.',
    // Selection
    [I18N_SELECTION_NO_OPTIONS_TEXT_KEY]: 'Nenhuma opção disponível.',
    // Validations
    [I18N_VALIDATIONS_KEY]: {
      [IS_REJECTED_CODE]: 'Ocorreu um erro ao validar este valor.',
      [IS_REQUIRED_CODE]: 'Campo de preenchimento obrigatório.',
      [DISALLOWED_VALUE_CODE]: (ctx, { allowedValues }) => {
        if (isBooleanSchema(ctx.schema()) && allowedValues.length === 1) {
          return `O campo ${
            allowedValues[0] === true ? 'tem de ser' : 'não pode ser'
          } marcado.`;
        }
        return `O valor do campo tem de ser um de: ${allowedValues.join(
          ', '
        )}.`;
      },
      [INVALID_DATE_RANGE_CODE]:
        'O intervalo não é válido. A data de início não pode ser posterior à ' +
        'data de fim.',
      [DATE_OUT_OF_BOUNDS_CODE]: (ctx, { minDate, maxDate }) => {
        // TODO: Format dates properly
        if (isDateRangeSchema(ctx.schema())) {
          return maxDate === undefined
            ? `A data de início não pode ser anterior a ${minDate}.`
            : minDate === undefined
            ? `A data de fim não pode ser posterior a ${maxDate}.`
            : `O intervalo tem de estar entre ${minDate} e ${maxDate}.`;
        }
        return maxDate === undefined
          ? `A data não pode ser anterior a ${minDate}.`
          : minDate === undefined
          ? `A data não pode ser posterior a ${maxDate}.`
          : `A data tem de estar entre ${minDate} e ${maxDate}.`;
      },
      [SIZE_OUT_OF_BOUNDS_CODE]: (
        ctx: LfStorage,
        { size, minSize, maxSize }
      ) => {
        if (size === 0 && minSize > 0) {
          return 'Campo de preenchimento obrigatório.';
        }
        // Checkbox group
        if (ctx.hasStateProperty('.', `i18n(${I18N_SELECTION_OPTIONS_KEY})`)) {
          const numOptions = (n) => (n === 1 ? 'uma' : n === 2 ? 'duas' : n);
          return maxSize === undefined
            ? `Tem de selecionar no mínimo ${numOptions(+minSize!)} ${
                +minSize === 1 ? 'opção' : 'opções'
              }.`
            : minSize === undefined
            ? `Tem de selecionar no máximo ${numOptions(+maxSize!)} ${
                +minSize === 1 ? 'opção' : 'opções'
              }.`
            : maxSize === minSize
            ? `Tem de selecionar ${numOptions(+maxSize!)} ${
                +maxSize === 1 ? 'opção' : 'opções'
              }.`
            : `Entre ${numOptions(+minSize)} e ${numOptions(
                +maxSize
              )} opções têm de ser selecionadas.`;
        }
        // Tables and form-lists
        return maxSize === undefined
          ? `A coleção tem de ter no mínimo ${+minSize!} elemento${
              +minSize === 1 ? '' : 's'
            }.`
          : minSize === undefined
          ? `A coleção tem de ter no máximo ${+maxSize} elemento${
              +maxSize === 1 ? '' : 's'
            }.`
          : maxSize === minSize
          ? `A coleção tem de ter ${+maxSize} elemento${
              +maxSize === 1 ? '' : 's'
            }.`
          : `A coleção tem de ter entre ${+minSize} e ${+maxSize} elementos.`;
      },
      [NUMBER_OUT_OF_BOUNDS_CODE]: (_, { min, max }) => {
        return max === undefined
          ? `O valor não pode ser menor do que ${+min!}.`
          : min === undefined
          ? `O valor não pode ser maior do que ${+max}.`
          : `O valor tem de estar entre ${+min} e ${+max}.`;
      },
      [LENGTH_OUT_OF_BOUNDS_CODE]: (_, { length, minLength, maxLength }) => {
        if (length === 0 && minLength > 0) {
          return 'Campo de preenchimento obrigatório.';
        }
        return maxLength === undefined
          ? `O valor tem de ter no mínimo ${+minLength!} ${
              +minLength! === 1 ? 'carácter' : 'caracteres'
            }.`
          : minLength === undefined
          ? `O valor tem de ter no máximo ${+maxLength} ${
              +maxLength === 1 ? 'carácter' : 'caracteres'
            }.`
          : maxLength === minLength
          ? `O valor tem de ter ${+maxLength} ${
              +maxLength === 1 ? 'carácter' : 'caracteres'
            }.`
          : `O valor tem de ter entre ${+minLength} e ${+maxLength} ` +
            'caracteres.';
      },
    },
  },
};
