// App =========================================================================

/**
 * I18n key used to set the locale used by `ngx-bootstrap`.
 */
export const I18N_BS_LOCALE_KEY = 'bsLocale';

// Value =======================================================================

/**
 * I18n key used to associate a label with a value.
 */
export const I18N_VALUE_LABEL_KEY = 'label';
/**
 * I18n key used to associate a code with a value.
 */
export const I18N_VALUE_CODE_KEY = 'code';
/**
 * I18n key used to associate a help message with a value.
 */
export const I18N_VALUE_HELP_MESSAGE_KEY = 'helpMessage';
/**
 * I18n key used to associate a legend with a value.
 */
export const I18N_VALUE_LEGEND_KEY = 'legend';
/**
 * I18n key used to associate messages with validation errors.
 */
export const I18N_VALIDATIONS_KEY = 'validations';
/**
 * I18n key used to ask the user to retry validating the value.
 */
export const I18N_VALUE_RETRY_VALIDATE_TEXT_KEY = 'retryValidateText';

// Tree-nav ====================================================================

/**
 * I18n key used to set the text of the dropdown button used to add optional
 * forms.
 */
export const I18N_TREE_NAV_ADD_OPTIONAL_VALUE_BUTTON_TEXT_KEY =
  'addOptionalValueButtonText';
/**
 * I18n key used to set the text of the button used to add forms to a list of
 * forms.
 */
export const I18N_TREE_NAV_ADD_VALUE_TO_LIST_BUTTON_TEXT_KEY =
  'addValueToListButtonText';

// Closeable ===================================================================

/**
 * I18n key used to set the text of the tooltip of the "close" button in
 * components.
 */
export const I18N_CLOSE_BUTTON_TOOLTIP_KEY = 'closeButtonTooltip';

// Actions-menu ================================================================

/**
 * I18n key used to set the text of the actions-menu button when it is in
 * "mobile" mode.
 */
export const I18N_ACTIONS_MENU_BUTTON_TEXT_KEY = 'actionsButtonText';
/**
 * I18n key used to translate the text of the buttons.
 */
export const I18N_ACTIONS_MENU_KEY = 'actionsMenu';
/**
 * I18n key used to translate the text of the options of the buttons.
 */
export const I18N_ACTIONS_MENU_OPTIONS_KEY = 'actionsMenuOptions';

// Validation panel ============================================================

/**
 * I18n key used to set the "errors" title of validation panel.
 */
export const I18N_VALIDATION_PANEL_ERRORS_TEXT_KEY = 'errorsText';
/**
 * I18n key used to set the "warnings" title of validation panel.
 */
export const I18N_VALIDATION_PANEL_WARNINGS_TEXT_KEY = 'warningsText';
/**
 * I18n key used to set the text that shows up in the validation panel when
 * there are no errors or warnings.
 */
export const I18N_VALIDATION_PANEL_NO_ISSUES_TEXT_KEY = 'noIssuesText';
/**
 * I18n key used to set the text of the tooltip of the "focus" button in the
 * validation panel.
 */
export const I18N_VALIDATION_PANEL_FOCUS_TEXT_KEY = 'focusButtonTooltip';
/**
 * I18n key used to set the text of the tooltip of the "next" button in the
 * validation panel.
 */
export const I18N_VALIDATION_PANEL_NEXT_BUTTON_TOOLTIP_KEY =
  'nextButtonTooltip';
/**
 * I18n key used to set the text of the tooltip of the "previous" button in the
 * validation panel.
 */
export const I18N_VALIDATION_PANEL_PREV_BUTTON_TOOLTIP_KEY =
  'prevButtonTooltip';

// Form ========================================================================

/**
 * I18n key used to set the text of a form component's help action.
 */
export const I18N_FORM_HELP_ACTION_TEXT_KEY = 'helpActionText';
/**
 * I18n key used to set the text of a form component's remove action.
 */
export const I18N_FORM_REMOVE_ACTION_TEXT_KEY = 'removeActionText';
/**
 * I18n key used to set the message displayed to the user in a form component to
 * confirm removal of a form.
 */
export const I18N_FORM_ENSURE_REMOVE_TEXT_KEY = 'ensureRemoveText';
/**
 * I18n key used to set the text of a form component's "cancel" button in the
 * removal modal.
 */
export const I18N_FORM_CANCEL_REMOVE_BUTTON_TEXT_KEY = 'cancelRemoveButtonText';
/**
 * I18n key used to set the text of a form component's "confirm" button in the
 * removal modal.
 */
export const I18N_FORM_CONFIRM_REMOVE_BUTTON_TEXT_KEY =
  'confirmRemoveButtonText';

// Date ========================================================================

/**
 * I18n key used to set the format of a date component.
 */
export const I18N_DATE_FORMAT_KEY = 'format';
/**
 * I18n key used to set the placeholder of a date component's input field.
 */
export const I18N_DATE_PLACEHOLDER_KEY = 'placeholder';

// Date range ==================================================================

/**
 * I18n keys used to set the separator of the date range.
 */
export const I18N_DATE_RANGE_SEPARATOR_KEY = 'separator';

// File ========================================================================

/**
 * I18n key used to set the "Choose file" placeholder of the file input.
 */
export const I18N_FILE_CHOOSE_PLACEHOLDER_KEY = 'choosePlaceholder';

/**
 * I18n key used to set the "Browse" text of the file input.
 */
export const I18N_FILE_BROWSE_TEXT_KEY = 'browseText';

/**
 * I18n key used to set the "No file chosen" text of the file input.
 */
export const I18N_FILE_NONE_CHOSEN_KEY = 'noneChosenText';

/**
 * I18n key used to set the "Clear" button tooltip text.
 */
export const I18N_FILE_CLEAR_BUTTON_TOOLTIP_KEY = 'clearButtonTooltip';

// Number ======================================================================

/**
 * I18n key used to set the prefix of a number component.
 */
export const I18N_NUMBER_PREFIX_KEY = 'prefix';
/**
 * I18n key used to set the suffix of a number component.
 */
export const I18N_NUMBER_SUFFIX_KEY = 'suffix';
/**
 * I18n key used to set the decimal separator of a number component.
 */
export const I18N_NUMBER_DECIMAL_SEPARATOR_KEY = 'decimalSeparator';
/**
 * I18n key used to set the "thousands separator" of a number component.
 */
export const I18N_NUMBER_THOUSANDS_SEPARATOR_KEY = 'thousandsSeparator';

// Table =======================================================================

/**
 * I18n key used to translate the "add row" text of the table component.
 */
export const I18N_TABLE_ADD_ROW_ACTION_TEXT_KEY = 'addRowActionText';
/**
 * I18n key used to translate the "remove rows" text of the table component.
 */
export const I18N_TABLE_REMOVE_ROWS_ACTION_TEXT_KEY = 'removeRowsActionText';
/**
 * I18n key used to translate the "no rows" text of the table component.
 */
export const I18N_TABLE_NO_ROWS_TEXT_KEY = 'noRowsText';
/**
 * I18n key used to set the labels of the table columns in the table component.
 */
export const I18N_TABLE_COLUMN_LABELS_KEY = 'columnLabels';

// Selection ===================================================================

/**
 * I18n key used to specify the list of options.
 */
export const I18N_SELECTION_OPTIONS_KEY = 'options';
/**
 * I18n key used to translate the "no options" message on components that
 * require options.
 */
export const I18N_SELECTION_NO_OPTIONS_TEXT_KEY = 'noOptionsText';
