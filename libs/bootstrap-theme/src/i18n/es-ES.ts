import {
  I18nPaths,
  I18N_UNLOAD_ALERT_MESSAGE_KEY,
  LfStorage,
} from '@lightweightform/core';
import {
  DATE_OUT_OF_BOUNDS_CODE,
  DISALLOWED_VALUE_CODE,
  isBooleanSchema,
  IS_REJECTED_CODE,
  IS_REQUIRED_CODE,
  LENGTH_OUT_OF_BOUNDS_CODE,
  NUMBER_OUT_OF_BOUNDS_CODE,
  SIZE_OUT_OF_BOUNDS_CODE,
} from '@lightweightform/storage';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { esLocale } from 'ngx-bootstrap/locale';

import {
  INVALID_DATE_RANGE_CODE,
  isDateRangeSchema,
} from '../utils/date-range-schema';

import {
  I18N_ACTIONS_MENU_BUTTON_TEXT_KEY,
  I18N_BS_LOCALE_KEY,
  I18N_CLOSE_BUTTON_TOOLTIP_KEY,
  I18N_DATE_FORMAT_KEY,
  I18N_DATE_PLACEHOLDER_KEY,
  I18N_DATE_RANGE_SEPARATOR_KEY,
  I18N_FILE_BROWSE_TEXT_KEY,
  I18N_FILE_CHOOSE_PLACEHOLDER_KEY,
  I18N_FILE_CLEAR_BUTTON_TOOLTIP_KEY,
  I18N_FILE_NONE_CHOSEN_KEY,
  I18N_FORM_CANCEL_REMOVE_BUTTON_TEXT_KEY,
  I18N_FORM_CONFIRM_REMOVE_BUTTON_TEXT_KEY,
  I18N_FORM_ENSURE_REMOVE_TEXT_KEY,
  I18N_FORM_HELP_ACTION_TEXT_KEY,
  I18N_FORM_REMOVE_ACTION_TEXT_KEY,
  I18N_NUMBER_DECIMAL_SEPARATOR_KEY,
  I18N_NUMBER_THOUSANDS_SEPARATOR_KEY,
  I18N_SELECTION_NO_OPTIONS_TEXT_KEY,
  I18N_SELECTION_OPTIONS_KEY,
  I18N_TABLE_ADD_ROW_ACTION_TEXT_KEY,
  I18N_TABLE_NO_ROWS_TEXT_KEY,
  I18N_TABLE_REMOVE_ROWS_ACTION_TEXT_KEY,
  I18N_TREE_NAV_ADD_OPTIONAL_VALUE_BUTTON_TEXT_KEY,
  I18N_TREE_NAV_ADD_VALUE_TO_LIST_BUTTON_TEXT_KEY,
  I18N_VALIDATIONS_KEY,
  I18N_VALIDATION_PANEL_ERRORS_TEXT_KEY,
  I18N_VALIDATION_PANEL_FOCUS_TEXT_KEY,
  I18N_VALIDATION_PANEL_NEXT_BUTTON_TOOLTIP_KEY,
  I18N_VALIDATION_PANEL_NO_ISSUES_TEXT_KEY,
  I18N_VALIDATION_PANEL_PREV_BUTTON_TOOLTIP_KEY,
  I18N_VALIDATION_PANEL_WARNINGS_TEXT_KEY,
  I18N_VALUE_RETRY_VALIDATE_TEXT_KEY,
} from './keys';

// Define `es` locale
defineLocale('es', esLocale);

/**
 * Default spanish translations for Bootstrap theme strings.
 */
export const I18N_ES_ES: I18nPaths = {
  '*': {
    // App
    [I18N_BS_LOCALE_KEY]: 'es',
    // Unload alert service
    [I18N_UNLOAD_ALERT_MESSAGE_KEY]:
      'Estás seguro que quieres irte? Es posible que los cambios realizados no se guarden.',
    // Value
    [I18N_VALUE_RETRY_VALIDATE_TEXT_KEY]: 'Reintentar la validación',
    // Tree-nav
    [I18N_TREE_NAV_ADD_OPTIONAL_VALUE_BUTTON_TEXT_KEY]: 'Añadir',
    [I18N_TREE_NAV_ADD_VALUE_TO_LIST_BUTTON_TEXT_KEY]: 'Añadir',
    // Actions-menu
    [I18N_ACTIONS_MENU_BUTTON_TEXT_KEY]: 'Comportamiento',
    // Closeable
    [I18N_CLOSE_BUTTON_TOOLTIP_KEY]: 'Cerrar',
    // Validation panel
    [I18N_VALIDATION_PANEL_ERRORS_TEXT_KEY]: 'Errores:',
    [I18N_VALIDATION_PANEL_WARNINGS_TEXT_KEY]: 'Advertencias:',
    [I18N_VALIDATION_PANEL_NO_ISSUES_TEXT_KEY]: '¡Todo parece genial!',
    [I18N_VALIDATION_PANEL_FOCUS_TEXT_KEY]: 'Seleccionar campo',
    [I18N_VALIDATION_PANEL_NEXT_BUTTON_TOOLTIP_KEY]: 'Siguiente campo',
    [I18N_VALIDATION_PANEL_PREV_BUTTON_TOOLTIP_KEY]: 'Campo anterior',
    // Form
    [I18N_FORM_HELP_ACTION_TEXT_KEY]: 'Ayuda',
    [I18N_FORM_REMOVE_ACTION_TEXT_KEY]: 'Eliminar',
    [I18N_FORM_ENSURE_REMOVE_TEXT_KEY]:
      'Confirme que desea eliminar este formulario.',
    [I18N_FORM_CANCEL_REMOVE_BUTTON_TEXT_KEY]: 'Cancelar',
    [I18N_FORM_CONFIRM_REMOVE_BUTTON_TEXT_KEY]: 'Confirmar',
    // Date
    [I18N_DATE_FORMAT_KEY]: 'DD/MM/YYYY',
    [I18N_DATE_PLACEHOLDER_KEY]: 'dd/mm/aaaa',
    // Date-range
    [I18N_DATE_RANGE_SEPARATOR_KEY]: ' - ',
    // File
    [I18N_FILE_CHOOSE_PLACEHOLDER_KEY]: 'Elija el archivo',
    [I18N_FILE_BROWSE_TEXT_KEY]: 'Buscar',
    [I18N_FILE_NONE_CHOSEN_KEY]: 'Ningún archivo elegido',
    [I18N_FILE_CLEAR_BUTTON_TOOLTIP_KEY]: 'Limpiar',
    // Number
    [I18N_NUMBER_DECIMAL_SEPARATOR_KEY]: ',',
    [I18N_NUMBER_THOUSANDS_SEPARATOR_KEY]: '',
    // Table
    [I18N_TABLE_ADD_ROW_ACTION_TEXT_KEY]: 'Añadir fila',
    [I18N_TABLE_REMOVE_ROWS_ACTION_TEXT_KEY]: 'Eliminar filas',
    [I18N_TABLE_NO_ROWS_TEXT_KEY]: 'No hay filas para mostrar.',
    // Selection
    [I18N_SELECTION_NO_OPTIONS_TEXT_KEY]: 'No hay opciones disponibles.',
    // Validations
    [I18N_VALIDATIONS_KEY]: {
      [IS_REJECTED_CODE]: 'Se produjo un error al validar este campo.',
      [IS_REQUIRED_CODE]: 'El campo es obligatorio.',
      [DISALLOWED_VALUE_CODE]: (ctx, { allowedValues }) => {
        if (isBooleanSchema(ctx.schema()) && allowedValues.length === 1) {
          return `El campo${
            allowedValues[0] === true ? '' : ' no'
          } debe estar marcado.`;
        }
        return `El valor del campo debe ser uno de: ${allowedValues.join(
          ', '
        )}.`;
      },
      [INVALID_DATE_RANGE_CODE]:
        'El rango no es válido. La fecha de inicio no debe ser posterior a la fecha de fin.',
      [DATE_OUT_OF_BOUNDS_CODE]: (ctx, { minDate, maxDate }) => {
        // TODO: Format dates properly
        if (isDateRangeSchema(ctx.schema())) {
          return maxDate === undefined
            ? `La fecha de inicio no puede ser anterior al ${minDate}.`
            : minDate === undefined
            ? `La fecha de fin no puede ser posterior al ${maxDate}.`
            : `El rango debe estar entre el ${minDate} y el ${maxDate}.`;
        }
        return maxDate === undefined
          ? `La fecha no puede ser anterior al ${minDate}.`
          : minDate === undefined
          ? `La fecha no puede ser posterior al ${maxDate}.`
          : `La fecha debe estar entre el ${minDate} y el ${maxDate}.`;
      },
      [SIZE_OUT_OF_BOUNDS_CODE]: (
        ctx: LfStorage,
        { size, minSize, maxSize }
      ) => {
        if (size === 0 && minSize > 0) {
          return 'El campo es obligatorio.';
        }
        // Checkbox group
        if (ctx.hasStateProperty('.', `i18n(${I18N_SELECTION_OPTIONS_KEY})`)) {
          const numOptions = (n) => (n === 1 ? 'una' : n === 2 ? 'dos' : n);
          return maxSize === undefined
            ? `Debes seleccionar al menos ${numOptions(+minSize!)} ${
                +minSize === 1 ? 'opción' : 'opciones'
              }.`
            : minSize === undefined
            ? `Debes seleccionar un máximo de ${numOptions(+maxSize!)} ${
                +minSize === 1 ? 'opción' : 'opciones'
              }.`
            : maxSize === minSize
            ? `Debes seleccionar ${numOptions(+maxSize!)} ${
                +maxSize === 1 ? 'opción' : 'opciones'
              }.`
            : `Debes seleccionar entre ${numOptions(+minSize)} y ${numOptions(
                +maxSize
              )} opciones.`;
        }
        // Tables and form-lists
        return maxSize === undefined
          ? `La colección debe tener al menos ${+minSize!} elemento${
              +minSize === 1 ? '' : 's'
            }.`
          : minSize === undefined
          ? `La colección debe tener un máximo de ${+maxSize} elemento${
              +maxSize === 1 ? '' : 's'
            }.`
          : maxSize === minSize
          ? `La colección debe tener ${+maxSize} elemento${
              +maxSize === 1 ? '' : 's'
            }.`
          : `La colección debe tener entre ${+minSize} y ${+maxSize} elementos.`;
      },
      [NUMBER_OUT_OF_BOUNDS_CODE]: (_, { min, max }) => {
        return max === undefined
          ? `El valor no puede ser inferior a ${+min!}.`
          : min === undefined
          ? `El valor no puede ser superior a ${+max}.`
          : `El valor debe estar entre ${+min} y ${+max}.`;
      },
      [LENGTH_OUT_OF_BOUNDS_CODE]: (_, { length, minLength, maxLength }) => {
        if (length === 0 && minLength > 0) {
          return 'El campo es obligatorio.';
        }
        return maxLength === undefined
          ? `El valor debe tener al menos ${+minLength!} ${
              +minLength! === 1 ? 'carácter' : 'caracteres'
            }.`
          : minLength === undefined
          ? `El valor debe tener un máximo de ${+maxLength} ${
              +maxLength === 1 ? 'carácter' : 'caracteres'
            }.`
          : maxLength === minLength
          ? `El valor debe tener ${+maxLength} ${
              +maxLength === 1 ? 'carácter' : 'caracteres'
            }.`
          : `El valor debe tener entre ${+minLength} y ${+maxLength} ` +
            'caracteres.';
      },
    },
  },
};
