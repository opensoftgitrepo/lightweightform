import nodeResolve from '@rollup/plugin-node-resolve';
import replace from '@rollup/plugin-replace';
import typescript from 'rollup-plugin-typescript2';
import { version as bsVersion } from 'bootstrap/package.json';

import * as themePkgJson from './package.json';
import * as rootPkgJson from '../../package.json';

// Catch inner packages as well, e.g. `@schematics/angular/utility/config`
const external = (id) => {
  const externals = [
    '@angular-devkit/core',
    '@angular-devkit/schematics',
    '@lightweightform/theme-common/schematics',
    '@schematics/angular',
  ];
  return (
    externals.find(
      (e) => id.startsWith(e) && (!id[e.length] || id[e.length] === '/')
    ) !== undefined
  );
};

// Get peer dependencies versions from the versions used within the repository
const peerDependencies = Object.keys(themePkgJson.peerDependencies);
const peerDependenciesVersions = peerDependencies.reduce((obj, dep) => {
  if (
    !themePkgJson.peerDependenciesMeta[dep] ||
    !themePkgJson.peerDependenciesMeta[dep].optional
  ) {
    obj[dep] =
      themePkgJson.devDependencies[dep] ||
      rootPkgJson.dependencies[dep] ||
      rootPkgJson.devDependencies[dep];
  }
  return obj;
}, {});

function plugins() {
  return [
    nodeResolve(),
    typescript({
      tsconfig: '../../tsconfig.schematics.json',
      tsconfigOverride: { include: [`${__dirname}/src/schematics/**/*`] },
    }),
    // Must replace only after TS compilation
    replace({
      'process.env.NODE_ENV': JSON.stringify(
        process.env.NODE_ENV || 'development'
      ),
      __themePeerDependencies__: JSON.stringify(peerDependenciesVersions),
      __bootstrapVersion__: JSON.stringify(bsVersion),
    }),
  ];
}

export default ['before-install', 'index'].map((indexName) => ({
  input: `src/schematics/${indexName}.ts`,
  external,
  plugins: plugins(),
  output: {
    file: `dist/schematics/${indexName}.js`,
    format: 'cjs',
    sourcemap: true,
  },
}));
