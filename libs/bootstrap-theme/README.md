# Lightweightform/bootstrap-theme

This package provides a Lightweightform theme for building Lightweightform
applications.

This theme is built using [Bootstrap](https://getbootstrap.com) (v4) for the
style-guide and [Font Awesome](https://fontawesome.com/v4.7.0) for icons.

Other Lightweightform themes are advised to use this theme as their
implementation reference.

## Goals of Lightweightform's Bootstrap theme

- Configurability: styles may be easily overridden by changing the value of all
  SCSS variables (both those provided by the theme itself and all that are
  exposed by Bootstrap).
- Responsive by default (desktop, tablet, mobile).

## Documentation

All Lightweightform documentation is available at:
https://docs.lightweightform.io.

The `@lightweightform/bootstrap-theme` API is available at:
https://docs.lightweightform.io/api/bootstrap-theme.

## Browser support

- **Internet Explorer**: 11 (requires at least `Map.prototype.entries` and
  `Array.from` polyfills)
- **Other browsers**: Latest 2 versions
