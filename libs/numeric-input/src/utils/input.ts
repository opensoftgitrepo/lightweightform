/**
 * Whether the given input element is currently active.
 * @param inputElement Input element to check if is active.
 * @returns Whether the input element is active.
 */
export function isActive(inputElement: HTMLInputElement): boolean {
  return document.activeElement === inputElement;
}

/**
 * Sets the given input's caret in the provided position.
 * @param inputElement Input element on which to set the caret.
 * @param position Position on which to set the caret.
 */
export function setCaret(
  inputElement: HTMLInputElement,
  position: number
): void {
  if (isActive(inputElement)) {
    inputElement.setSelectionRange(position, position, 'none');
  }
}
