/**
 * Transforms a number which may be represented in exponential notation to a
 * string representation of the number without the exponential notation and with
 * a given scale.
 * @param num Number (possibly represented in exponential notation) to format
 * into a string.
 * @param scale Scale to use when representing the number as a string.
 * @returns String representation of the number without exponent.
 */
export function toStringWithoutExponent(num: number, scale: number): string {
  // `num.toFixed` returns exponential notation for (abs) numbers `>= 1e21`
  if (Math.abs(num) >= 1e21 && isFinite(num)) {
    const match = String(num).match(/^((-?)(\d+)(\.(\d+))?)e\+(\d+)$/);
    const [, , sign, deci, , frac = '', exp] = match!;
    const scaleStr = scale ? `.${'0'.repeat(scale)}` : '';
    return sign + deci + frac + '0'.repeat(+exp - frac.length) + scaleStr;
  }
  return num.toFixed(scale);
}

/**
 * Adds thousands separators to a string of digits. Adapted from:
 * http://stackoverflow.com/a/10899795/604296
 * @param digits String of digits.
 * @param separator Thousands separator.
 * @returns Digits separated by the provided separator by thousands.
 */
export function addThousandsSeparators(
  digits: string,
  separator: string
): string {
  return digits.replace(/\B(?=(\d{3})+\b)/g, separator);
}

/**
 * Removes all non-digit characters and leading zeroes from a string.
 * @param str String on which to perform changes.
 * @returns String without non-digit characters and leading zeroes.
 */
export function removeNonDigitsAndLeadingZeroes(str: string): string {
  return str.replace(/\D+/g, '').replace(/^0+([^0]|0$)/, '$1');
}

/**
 * Whether a given string representation of a number represents a negative
 * value. Note that negative `0` still counts as negative.
 * @param numStr String representation of number to check if negative.
 * @returns Whether the number string is negative.
 */
export function isNegative(numStr: string): boolean {
  return numStr[0] === '-';
}
