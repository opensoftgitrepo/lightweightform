import {
  NumericInputOptions,
  NumericRepresentation,
} from './numeric-input-options';
import { isActive, setCaret } from './utils/input';
import {
  addThousandsSeparators,
  isNegative,
  removeNonDigitsAndLeadingZeroes,
  toStringWithoutExponent,
} from './utils/number-string';

/**
 * Numeric input.
 */
export class NumericInput {
  /**
   * Property name of the input element that references an instance of this
   * class.
   */
  public static readonly NUMERIC_INPUT_REF = '$lfNumericInput';

  /**
   * Input element associated with the numeric input.
   */
  public readonly inputElement: HTMLInputElement;

  // Options with their default values
  private _numericRepresentation: NumericRepresentation = 'float';
  private _scale = 0;
  private _leadingZeroes = 1;
  private _min = -Infinity;
  private _max = Infinity;
  private _prefix = '';
  private _suffix = '';
  private _decimalSeparator = '.';
  private _thousandsSeparator = '';
  private onUpdate:
    | ((stringValue: string, numberValue: number | null) => void)
    | null = null;

  /**
   * String value represented by the numeric input.
   */
  private stringValue = '';
  /**
   * Number value represented by the numeric input.
   */
  private numberValue: number | null = null;

  /**
   * Turns a given input element (an `<input>` element) into a numeric input.
   * Inputs of type `text`, `search`, `tel`, and `password` are supported;
   * unfortunately, the `number` type is not supported. It is currently
   * recommended to use a type of `text` as the `tel` type forces a virtual
   * keyboard which, in some devices, does not contain a `-` key. In the future,
   * the recommended way of forcing a numeric virtual keyboard should be with
   * the attribute `inputmode` set to `"numeric"`, see: [Can I
   * use...](https://caniuse.com/#search=inputmode) for browser compatibility.
   * @param inputElement Element to set as a numeric input.
   * @param options Numeric input options.
   */
  constructor(inputElement: HTMLInputElement, options: NumericInputOptions) {
    this.inputElement = inputElement;
    // Add a reference to the instance from the input element (this is used by
    // event listeners to know whether the input element is a numeric input)
    this.inputElement[NumericInput.NUMERIC_INPUT_REF] = this;

    // Set options and initialise the numeric input with the element's value
    this.setOptions(options);

    // Set up events the first time an instance is successfully created
    NumericInput.setupEvents && NumericInput.setupEvents();
  }

  /**
   * Validates numeric input options.
   * @param options Options to validate.
   */
  public static validateOptions(options: NumericInputOptions): void {
    // Bypass validation when calling `NumericInput.format` or
    // `NumericInput.parse` with a numeric input instance
    if (options instanceof NumericInput) {
      return;
    }

    const {
      scale,
      leadingZeroes,
      min,
      max,
      decimalSeparator,
      thousandsSeparator,
      prefix,
      suffix,
    } = options;
    if (
      scale !== undefined &&
      (scale < 0 || scale > 100 || Math.floor(scale) !== scale)
    ) {
      throw new RangeError('"scale" must be an integer between 0 and 100');
    }
    if (
      leadingZeroes !== undefined &&
      (leadingZeroes < 1 ||
        !isFinite(leadingZeroes) ||
        Math.floor(leadingZeroes) !== leadingZeroes)
    ) {
      throw new RangeError('"leadingZeroes" must be an integer >= 1');
    }
    if (min !== undefined && max !== undefined && min > max) {
      throw new RangeError('"min" must be <= than "max"');
    }
    if (decimalSeparator !== undefined && decimalSeparator.length !== 1) {
      throw new Error('"decimalSeparator" must be a string of length 1');
    }
    if (thousandsSeparator !== undefined && thousandsSeparator.length > 1) {
      throw new Error('"thousandsSeparator" must be a string of length <= 1');
    }
    // We assume that no digits or `'-'` ever appear on the "non-numeric" parts
    // of the input, if this proves to be an issue, the code may be changed in
    // the future to allow digits/minus within the prefix/suffix
    // NOTE: The test below is fine even when the variables are `undefined`
    if (
      /[\d-]/.test(`${prefix}${suffix}${decimalSeparator}${thousandsSeparator}`)
    ) {
      throw new Error(
        '"prefix"/"suffix"/"decimalSeparator"/"thousandsSeparator" may not ' +
          'contain digits or "-"'
      );
    }
  }

  /**
   * Converts a value into a formatted string. The passed argument will first be
   * converted into a string, then formatted according to the provided options.
   * If a number was passed, it will be converted into a string taking into
   * consideration whether we're converting numbers as integers or as floats.
   * @param value Value to format.
   * @param options Options to use when formatting.
   * @returns Formatted representation of the provided value.
   */
  public static format(
    value: string | number | null,
    options: NumericInputOptions = {}
  ): string {
    NumericInput.validateOptions(options);
    const {
      numericRepresentation = 'float',
      scale = 0,
      leadingZeroes = 1,
      prefix = '',
      suffix = '',
      decimalSeparator = '.',
      thousandsSeparator = '',
    } = options;

    // Force `value` to be a string
    if (value == null || value !== value /*`NaN`*/) {
      return '';
    } else if (typeof value === 'number') {
      // Convert the number to a string taking into consideration whether
      // numbers represent floats or integers
      value =
        numericRepresentation === 'float'
          ? toStringWithoutExponent(value, scale)
          : toStringWithoutExponent(Math.round(value), 0);
    }

    const digits = removeNonDigitsAndLeadingZeroes(value);
    if (digits === '') {
      return '';
    }

    // Build the formatted value
    const hasDecimalPart = scale > 0;
    const sign = isNegative(value) ? '-' : '';
    let stringNumber: string;
    let intPart = hasDecimalPart ? digits.slice(0, -scale) : digits;
    if (intPart.length < leadingZeroes) {
      intPart = '0'.repeat(leadingZeroes - intPart.length) + intPart;
    }
    intPart = addThousandsSeparators(intPart, thousandsSeparator);
    if (hasDecimalPart) {
      const decimalPart =
        '0'.repeat(Math.max(scale - digits.length, 0)) + digits.slice(-scale);
      stringNumber = intPart + decimalSeparator + decimalPart;
    } else {
      stringNumber = intPart;
    }

    return sign + prefix + stringNumber + suffix;
  }

  /**
   * Converts a string value into a number, taking into consideration whether to
   * return it as a float or as an integer.
   * @param value String to convert to number.
   * @param options Options to use when parsing.
   * @return Numeric representation of the provided string.
   */
  public static parse(
    value: string,
    options: NumericInputOptions = {}
  ): number | null {
    NumericInput.validateOptions(options);
    const { numericRepresentation = 'float', scale = 0 } = options;

    const digits = removeNonDigitsAndLeadingZeroes(value || '');
    if (digits === '') {
      return null;
    }

    // Build the numeric value
    const hasDecimalPart = scale > 0;
    const sign = isNegative(value) ? '-' : '';
    let stringNumber: string;
    if (hasDecimalPart && numericRepresentation === 'float') {
      const intPart = digits.length <= scale ? '0' : digits.slice(0, -scale);
      const decimalPart =
        '0'.repeat(Math.max(scale - digits.length, 0)) + digits.slice(-scale);
      stringNumber = intPart + '.' + decimalPart;
    } else {
      stringNumber = digits;
    }

    return Number(sign + stringNumber);
  }

  /**
   * Method that sets up all numeric input events and then deletes itself (can
   * only be called once).
   */
  private static setupEvents?(): void {
    function addEventListener(
      type: string,
      listener: (numericInput: NumericInput, evt: Event) => void
    ): void {
      document.addEventListener(type, (evt) => {
        const numericInput =
          evt.target && evt.target[NumericInput.NUMERIC_INPUT_REF];
        numericInput && listener(numericInput, evt);
      });
    }
    addEventListener('input', (numericInput, evt) => {
      numericInput.onInput(evt);
    });
    addEventListener('keydown', (numericInput, evt: KeyboardEvent) => {
      numericInput.onKeyDown(evt);
    });
    addEventListener('focusin', (numericInput) => {
      numericInput.onFocusIn();
    });
    addEventListener('focusout', (numericInput) => {
      numericInput.onFocusOut();
    });
    // Remove the method after one use
    delete NumericInput.setupEvents;
  }

  /**
   * How the numeric input's number is represented.
   */
  public get numericRepresentation(): NumericRepresentation {
    return this._numericRepresentation;
  }

  /**
   * Number of decimal digits of the numeric input.
   */
  public get scale(): number {
    return this._scale;
  }

  /**
   * Number of zeroes shown on the integer part of the number.
   */
  public get leadingZeroes(): number {
    return this._leadingZeroes;
  }

  /**
   * Minimum number allowed.
   */
  public get min(): number {
    return this._min;
  }

  /**
   * Maximum number allowed.
   */
  public get max(): number {
    return this._max;
  }

  /**
   * Prefix added to the number.
   */
  public get prefix(): string {
    return this._prefix;
  }

  /**
   * Suffix added to the number.
   */
  public get suffix(): string {
    return this._suffix;
  }

  /**
   * Character used between the integer and decimal parts of a number.
   */
  public get decimalSeparator(): string {
    return this._decimalSeparator;
  }

  /**
   * Character used between groups of thousands in the integer part of a number.
   */
  public get thousandsSeparator(): string {
    return this._thousandsSeparator;
  }

  /**
   * Set new numeric input options and update the input element to reflect the
   * new options. A new value may be passed to update the input element with. If
   * no new value is provided, the numeric input will be updated with the input
   * element's current value.
   * @param options Numeric input options.
   * @param newValue New value to format
   */
  public setOptions(
    options: NumericInputOptions,
    newValue: string | number | null = this.inputElement.value
  ): void {
    const {
      numericRepresentation,
      scale,
      leadingZeroes,
      min,
      max,
      decimalSeparator,
      thousandsSeparator,
      prefix,
      suffix,
      onUpdate,
    } = options;

    // Validate options
    const relevantMin = min === undefined ? this._min : min;
    const relevantMax = max === undefined ? this._max : max;
    NumericInput.validateOptions({
      ...options,
      min: relevantMin,
      max: relevantMax,
    });

    // Update options
    numericRepresentation !== undefined &&
      (this._numericRepresentation = numericRepresentation);
    scale !== undefined && (this._scale = scale);
    leadingZeroes !== undefined && (this._leadingZeroes = leadingZeroes);
    min !== undefined && (this._min = min);
    max !== undefined && (this._max = max);
    decimalSeparator !== undefined &&
      (this._decimalSeparator = decimalSeparator);
    thousandsSeparator !== undefined &&
      (this._thousandsSeparator = thousandsSeparator);
    prefix !== undefined && (this._prefix = prefix);
    suffix !== undefined && (this._suffix = suffix);
    onUpdate !== undefined && (this.onUpdate = onUpdate);

    this.update(newValue);
  }

  /**
   * Programmatically update the numeric input's value while making sure that
   * the caret is kept in place. No `min`/`max` bounds will be checked when
   * making this update.
   * @param value New value to format and set on the input element.
   */
  public update(value: string | number | null): void {
    const newStringValue = this.format(value);
    this._update(newStringValue, this.parse(newStringValue));
  }

  /**
   * Numeric input's current (formatted) string value.
   * @returns String representation of the numeric input's value.
   */
  public toString(): string {
    return this.stringValue;
  }

  /**
   * Numeric input's current numeric value.
   * @returns Numeric representation of the numeric input's value.
   */
  public toNumber(): number | null {
    return this.numberValue;
  }

  /**
   * Formats a representation of a number into a formatted string. The passed
   * argument will first be converted into a string, then formatted according to
   * this instance's options. If a number was passed, it will be converted into
   * a string taking into consideration whether we're representing numbers as
   * integers or as floats.
   * @param value Value to format.
   * @returns Formatted representation of the provided value.
   */
  public format(value: string | number | null): string {
    return NumericInput.format(value, this as any);
  }

  /**
   * Parses a string value into a number using this instance's options (e.g.
   * whether the string represents an integer or float).
   * @param value String to parse into a number.
   * @return Numeric representation of the provided string.
   */
  public parse(value: string): number | null {
    return NumericInput.parse(value, this as any);
  }

  /**
   * Whether the current value is value in respect to the `min`/`max` bounds. A
   * `null` value is always considered valid.
   * @returns Whether the current value is valid.
   */
  public isValid(): boolean {
    return (
      this.numberValue === null ||
      (this.numberValue >= this.min && this.numberValue <= this.max)
    );
  }

  /**
   * Disassociates the input element with the numeric input. After calling
   * `destroy` the numeric input instance can no longer be used.
   */
  public destroy(): void {
    delete this.inputElement[NumericInput.NUMERIC_INPUT_REF];
  }

  /**
   * Method to update/format the input element's value while keeping the caret
   * in place.
   * @param newStringValue New string value.
   * @param newNumberValue Corresponding new number value.
   */
  private _update(newStringValue: string, newNumberValue: number | null): void {
    // Update state, save whether the value changed to know whether to call hook
    const changed =
      this.stringValue !== newStringValue ||
      this.numberValue !== newNumberValue;
    this.stringValue = newStringValue;
    this.numberValue = newNumberValue;

    // Compute the new caret position given the new string value
    const newCaretPosition = this.newCaretPosition();
    if (this.inputElement.value !== newStringValue) {
      this.inputElement.value = newStringValue;
    }
    // Update the caret (does nothing if the input element is not active)
    setCaret(this.inputElement, newCaretPosition!);

    if (changed && this.onUpdate) {
      this.onUpdate(newStringValue, newNumberValue);
    }
  }

  /**
   * Method to call on an `input` event. Updates the numeric input after the
   * input element has been changed by a user, possibly restricting what has
   * been typed due to `min`/`max` constraints.
   * @param evt Input event.
   * @param value Value being input (defaults to the value currently in the
   * input element).
   */
  private onInput(evt?: Event, value = this.inputElement.value): void {
    // XXX: Use a combination of keyboard and input events to reliably toggle
    // the sign across devices (some mobile keyboards don't trigger a key event)
    if (evt && (evt as any).data === '-') {
      const has2Minus = value.indexOf('-', value.indexOf('-') + 1) !== -1;
      value = has2Minus ? value.replace(/^-*/, '') : value.replace(/^-*/, '-');
    }

    // Allow the user to start typing a negative number by pressing `-`
    if (this.min < 0 && value === '-') {
      value = '-0';
    }

    // Force the number to be negative when it must be and it is being written
    // from scratch (so as to not force the user to type in a "-" beforehand)
    if (this.max < 0 && !isNegative(value) && this.numberValue === null) {
      value = `-${value}`;
    }

    let newStringValue = this.format(value);
    let newNumberValue = this.parse(newStringValue);

    // Allow invalid values when valid ones would be impossible to write
    // otherwise (e.g. if `min = 10` the user has to first enter a value `< 10`)
    if (
      newNumberValue !== null &&
      // Do not allow values out of "normal" bounds (`min < 0`/`max > 0`) unless
      // they were already invalid and are now "less invalid"
      ((this.min < 0 &&
        newNumberValue < this.min &&
        newNumberValue < this.numberValue!) ||
        (this.max > 0 &&
          newNumberValue > this.max &&
          newNumberValue > this.numberValue!) ||
        // Disallow positive/negative numbers when they are forbidden unless the
        // value is already invalid and is now "less invalid"
        (this.min >= 0 &&
          newNumberValue < 0 &&
          newNumberValue <= this.numberValue!) ||
        (this.max <= 0 &&
          newNumberValue > 0 &&
          newNumberValue >= this.numberValue!))
    ) {
      newStringValue = this.stringValue;
      newNumberValue = this.numberValue;
    }

    // Disallow a negative zero when the number can never be zero
    if (this.min >= 0 && newNumberValue === 0 && isNegative(newStringValue)) {
      newStringValue = newStringValue.slice(1);
      newNumberValue = 0;
    }

    this._update(newStringValue, newNumberValue);
  }

  /**
   * Method to call on a `keydown` event. Toggles the sign of the number when a
   * "-" is pressed; and handles deletions by setting the value to `null` when
   * deleting `0` and making the caret move to the left of a decimal or
   * thousands separator when trying to delete it with `Backspace`.
   * @param evt Keyboard event.
   */
  private onKeyDown(evt: KeyboardEvent): void {
    // Toggle input element's sign
    // XXX: Use a combination of keyboard and input events to reliably toggle
    // the sign across devices (some mobile keyboards don't trigger a key event)
    if (evt.key === '-') {
      this.onInput({ data: '-' } as any, `-${this.stringValue}`);
      return evt.preventDefault();
    }

    // Reset the input when deleting `0`
    if (
      (evt.key === 'Backspace' || evt.key === 'Delete') &&
      this.numberValue === 0
    ) {
      this.update('');
      return evt.preventDefault();
    }

    // Move caret to left of decimal/thousands separator
    if (evt.key === 'Backspace') {
      const { selectionStart, selectionEnd: caret } = this.inputElement;
      // When there is no selection of text
      if (caret !== null && selectionStart === caret) {
        // Get the char before the caret but first remove the `-`, the prefix,
        // and the suffix since we don't know if the prefix/suffix contains a
        // separator
        const value = this.toString();
        const signPrefixLength = this.prefix.length + +isNegative(value);
        const trimmedValue = value
          .slice(signPrefixLength)
          .slice(0, value.length - signPrefixLength - this.suffix.length);
        const trimmedCaret = caret - signPrefixLength;
        const char = trimmedValue.substring(trimmedCaret - 1, trimmedCaret);
        // Move the caret to the left when the char to the left is a separator
        if (
          char !== '' &&
          (char === this.thousandsSeparator || char === this.decimalSeparator)
        ) {
          setCaret(this.inputElement, caret - 1);
          return evt.preventDefault();
        }
      }
    }
  }

  /**
   * Method to call when the input element has been focused. Places the caret
   * after the minus/prefix and before the suffix.
   */
  private onFocusIn(): void {
    // Chrome/IE seem to require the placement of the caret to be deferred
    requestAnimationFrame(() =>
      setCaret(this.inputElement, this.newCaretPosition()!)
    );
  }

  /**
   * Method to call when the input element has been blurred. Disallows negative
   * zeroes to be shown.
   */
  private onFocusOut(): void {
    if (this.numberValue === 0 && isNegative(this.stringValue)) {
      this.update(0);
    }
  }

  /**
   * Computes the caret position for the current value of the numeric input
   * taking into consideration the current position of the caret in the element
   * (note that, at the time this function is called, the current value of the
   * numeric input may differ from the value in the input element).
   * @returns New caret position (when the input element is focused).
   */
  private newCaretPosition(): number | undefined {
    if (isActive(this.inputElement)) {
      // The caret must be placed after all leading zeroes (i.e. if we have
      // `|0.01`, the caret should move to `0.0|1`; if we have `|0.11`,
      // it should move to `0|.11`); however, when there are no leading zeroes,
      // the caret must be placed after the `-` and the prefix (i.e. if we have
      // `|-$1.00`, the caret should move to `-$|1.00`)
      const lastZeroMatch = this.stringValue.match(/^[^1-9]*0/);
      const minPosition =
        lastZeroMatch !== null
          ? lastZeroMatch.index! + lastZeroMatch[0].length
          : this.prefix.length + +isNegative(this.stringValue);
      // The caret must be placed before the suffix (i.e. if we have `0.00 €|`,
      // the caret should move to `0.00| €`)
      const maxPosition = this.stringValue.length - this.suffix.length;
      // We want to maintain the caret position in respect to the end of the
      // input
      const newCaretPosition =
        this.stringValue.length -
        (this.inputElement.value.length - this.inputElement.selectionEnd!);
      // However, the final result must be bound by the `min` and `max` caret
      // positions previously computed
      return Math.min(Math.max(newCaretPosition, minPosition), maxPosition);
    }
  }
}
