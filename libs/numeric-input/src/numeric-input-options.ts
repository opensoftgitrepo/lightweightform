/**
 * Supported ways of representing a numeric input as a number.
 */
export type NumericRepresentation = 'float' | 'int';

/**
 * Options to provide to the numeric input.
 */
export interface NumericInputOptions {
  /**
   * Whether the numeric input represents a float or integer. This affects how
   * the numeric input deals with numbers (e.g. if set to `'int'`, a numeric
   * input with value `'1,234.56'` is seen as representing the number `123456`
   * rather than `1234.56`). This affects `parse` and how numbers received by
   * `format` or `update` are seen by the numeric input. Defaults to `'float'`.
   */
  numericRepresentation?: NumericRepresentation;
  /**
   * Number of decimal digits of the numeric input (up to `100`). Defaults to
   * `0`.
   *
   * **NOTE:** We use
   * [`Number.prototype.toFixed(scale)`](
   * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/toFixed)
   * to help format numbers. Most browsers support `scale` up to `100` (maximum
   * value we allow), however, some browsers only support `scale` up to `20`, so
   * set `scale > 20` at your own risk (will throw an exception in unsupported
   * browsers).
   */
  scale?: number;
  /**
   * Number of zeroes to show on the integer part of the number. Defaults to
   * `1`.
   */
  leadingZeroes?: number;
  /**
   * Minimum number allowed. Defaults to `-Infinity`.
   */
  min?: number;
  /**
   * Maximum number allowed. Defaults to `Infinity`.
   */
  max?: number;
  /**
   * Adds a prefix to the numeric input (e.g. if set to `'$'`, a numeric input
   * representing the number `123.45` will be formatted as `'$123.45'`). The
   * prefix may not contain a digit or `'-'`. Defaults to `''`.
   */
  prefix?: string;
  /**
   * Adds a suffix to the numeric input (e.g. if set to `' €'`, a numeric input
   * representing the number `123.45` will be formatted as `'123.45 €'`). The
   * suffix may not contain a digit or `'-'`. Defaults to `''`.
   */
  suffix?: string;
  /**
   * Character used between the integer and decimal parts of a number (e.g. if
   * `decimalSeparator` is set as `','`, then the number `123.45` will be
   * formatted as `'123,45'`). The separator mustn't be a digit or
   * `'-'`. Defaults to `'.'`.
   */
  decimalSeparator?: string;
  /**
   * Character used between groups of thousands in the integer part of a number
   * (e.g. if `decimalSeparator` is set as `' '`, then the number `1234567` will
   * be formatted as `'1 234 567'`). The separator mustn't be a digit or `'-'`.
   * Defaults to `''`.
   */
  thousandsSeparator?: string;
  /**
   * Function to be called whenever the numeric input is updated or `null` when
   * no function should be called. Defaults to `null`.
   * @param stringValue Formatted string value of the numeric input.
   * @param numberValue Numeric value of the numeric input.
   */
  onUpdate?: ((stringValue: string, numberValue: number | null) => void) | null;
}
