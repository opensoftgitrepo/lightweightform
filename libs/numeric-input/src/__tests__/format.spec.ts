import { newNumericInput } from './test-util';

describe('Numeric input `format`', () => {
  it('returns an empty string when appropriate', () => {
    const { numericInput } = newNumericInput();
    // Signature doesn't support it, but we handle it anyway
    expect(numericInput.format(undefined as any)).toBe('');
    expect(numericInput.format(null)).toBe('');
    expect(numericInput.format('')).toBe('');
    expect(numericInput.format('-')).toBe('');
    expect(numericInput.format(NaN)).toBe('');
    expect(numericInput.format('xxx')).toBe('');
    // This might change in the future, but better to leave it specified
    expect(numericInput.format(Infinity)).toBe('');
    expect(numericInput.format(-Infinity)).toBe('');
  });

  it('allows negative zero when passed as string', () => {
    const { numericInput } = newNumericInput({ scale: 2 });
    expect(numericInput.format('-0')).toBe('-0.00');
  });

  it('disallows negative zero when passed as number', () => {
    const { numericInput } = newNumericInput({ scale: 2 });
    expect(numericInput.format(-0)).toBe('0.00');
  });

  it('strips all non-digit characters', () => {
    const { numericInput } = newNumericInput();
    expect(numericInput.format('x1y2')).toBe('12');
    expect(numericInput.format('-x1y2')).toBe('-12');
    expect(numericInput.format('123.45')).toBe('12345');
  });

  it('removes leading zeroes', () => {
    const { numericInput } = newNumericInput({ scale: 2 });
    expect(numericInput.format('01')).toBe('0.01');
    expect(numericInput.format('000123')).toBe('1.23');
    expect(numericInput.format('-0000')).toBe('-0.00');
  });

  it('handles scales properly', () => {
    const { numericInput: scale0 } = newNumericInput({ scale: 0 });
    const { numericInput: scale2 } = newNumericInput({ scale: 2 });
    const { numericInput: scale4 } = newNumericInput({ scale: 4 });
    expect(scale0.format('123')).toBe('123');
    expect(scale2.format('0')).toBe('0.00');
    expect(scale2.format('1')).toBe('0.01');
    expect(scale2.format('99')).toBe('0.99');
    expect(scale2.format('999')).toBe('9.99');
    expect(scale2.format('-1')).toBe('-0.01');
    expect(scale4.format('5')).toBe('0.0005');
    expect(scale4.format('-99')).toBe('-0.0099');
  });

  it('handles leading zeroes properly', () => {
    const { numericInput: zeroes2 } = newNumericInput({
      leadingZeroes: 2,
    });
    const { numericInput: zeroes4 } = newNumericInput({
      leadingZeroes: 4,
    });
    expect(zeroes2.format('0')).toBe('00');
    expect(zeroes2.format('1')).toBe('01');
    expect(zeroes2.format('99')).toBe('99');
    expect(zeroes2.format('999')).toBe('999');
    expect(zeroes2.format('-1')).toBe('-01');
    expect(zeroes4.format('5')).toBe('0005');
    expect(zeroes4.format('-99')).toBe('-0099');
  });

  it('supports prefixes', () => {
    const { numericInput: prefixUSD } = newNumericInput({ prefix: '$' });
    const { numericInput: prefixEUR } = newNumericInput({
      scale: 2,
      prefix: '€ ',
    });
    expect(prefixUSD.format('1')).toBe('$1');
    expect(prefixUSD.format('-99')).toBe('-$99');
    expect(prefixEUR.format('1')).toBe('€ 0.01');
    expect(prefixEUR.format('-999')).toBe('-€ 9.99');
  });

  it('supports suffixes', () => {
    const { numericInput: suffixUSD } = newNumericInput({ suffix: '$' });
    const { numericInput: suffixEUR } = newNumericInput({
      scale: 2,
      suffix: ' €',
    });
    expect(suffixUSD.format('1')).toBe('1$');
    expect(suffixUSD.format('-99')).toBe('-99$');
    expect(suffixEUR.format('1')).toBe('0.01 €');
    expect(suffixEUR.format('-999')).toBe('-9.99 €');
  });

  it('supports decimal separator', () => {
    const { numericInput } = newNumericInput({
      scale: 2,
      decimalSeparator: ',',
    });
    expect(numericInput.format('1')).toBe('0,01');
    expect(numericInput.format('-1')).toBe('-0,01');
    expect(numericInput.format('999')).toBe('9,99');
  });

  it('supports thousands separator', () => {
    const { numericInput: commaSeparator } = newNumericInput({
      thousandsSeparator: ',',
    });
    const { numericInput: spaceSeparator } = newNumericInput({
      scale: 2,
      thousandsSeparator: ' ',
    });
    expect(commaSeparator.format('1')).toBe('1');
    expect(commaSeparator.format('1234')).toBe('1,234');
    expect(commaSeparator.format('1234567')).toBe('1,234,567');
    expect(spaceSeparator.format('1')).toBe('0.01');
    expect(spaceSeparator.format('123456')).toBe('1 234.56');
    expect(spaceSeparator.format('123456789')).toBe('1 234 567.89');
  });

  it('handles numeric argument', () => {
    const { numericInput: float } = newNumericInput({
      numericRepresentation: 'float',
      scale: 2,
    });
    const { numericInput: int } = newNumericInput({
      numericRepresentation: 'int',
      scale: 2,
    });
    expect(float.format(1)).toBe('1.00');
    expect(float.format(0.05)).toBe('0.05');
    expect(float.format(22.5)).toBe('22.50');
    expect(float.format(1000)).toBe('1000.00');
    expect(int.format(1)).toBe('0.01');
    expect(int.format(0.05)).toBe('0.00');
    expect(int.format(22.5)).toBe('0.23');
    expect(int.format(1000)).toBe('10.00');
  });

  it('handles very large numbers represented in exponential notation', () => {
    const { numericInput: float } = newNumericInput({
      numericRepresentation: 'float',
      scale: 2,
    });
    const { numericInput: int } = newNumericInput({
      numericRepresentation: 'int',
      scale: 2,
    });
    expect(float.format(555e50)).toBe(`555${'0'.repeat(50)}.00`);
    expect(int.format(555e50)).toBe(`555${'0'.repeat(48)}.00`);
    expect(float.format(Number.MAX_VALUE)).toBe(
      `17976931348623157${'0'.repeat(292)}.00`
    );
    expect(int.format(Number.MAX_VALUE)).toBe(
      `17976931348623157${'0'.repeat(290)}.00`
    );
    expect(float.format(-Number.MAX_VALUE)).toBe(
      `-17976931348623157${'0'.repeat(292)}.00`
    );
    expect(int.format(-Number.MAX_VALUE)).toBe(
      `-17976931348623157${'0'.repeat(290)}.00`
    );
  });

  it('handles very small numbers represented in exponential notation', () => {
    const { numericInput: float } = newNumericInput({
      numericRepresentation: 'float',
      scale: 100,
    });
    const { numericInput: int } = newNumericInput({
      numericRepresentation: 'int',
      scale: 100,
    });
    // Floating point errors may occur when converting the number (apparently
    // the `toFixed` spec isn't clear and will depend on the JS engine)
    expect(float.format(555e-50)).toMatch(
      new RegExp(`^0\.${'0'.repeat(47)}555\\d{50}$`)
    );
    expect(float.format(Number.MIN_VALUE)).toBe(`0.${'0'.repeat(100)}`);
    expect(float.format(-Number.MIN_VALUE)).toBe(`-0.${'0'.repeat(100)}`);
    expect(int.format(555e-50)).toBe(`0.${'0'.repeat(100)}`);
    expect(int.format(-555e-50)).toBe(`0.${'0'.repeat(100)}`);
  });

  it('works well with multiple settings at the same time', () => {
    const { numericInput } = newNumericInput({
      scale: 3,
      leadingZeroes: 6,
      prefix: '$ ',
      suffix: ' €',
      decimalSeparator: ',',
      thousandsSeparator: '.',
    });
    expect(numericInput.format('1234567890')).toBe('$ 1.234.567,890 €');
    expect(numericInput.format('-1234567890')).toBe('-$ 1.234.567,890 €');
    expect(numericInput.format('-90')).toBe('-$ 000.000,090 €');
  });
});
