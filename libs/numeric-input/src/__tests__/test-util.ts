import { NumericInput } from '../numeric-input';
import { NumericInputOptions } from '../numeric-input-options';

/**
 * Creates a new possibly focused input element with an associated numeric input
 * and a given initial value and selections.
 * @param options Numeric input options.
 * @param inputElementOptions Options for the input element (initial value,
 * whether to start focused, and a symbol to represent the caret).
 * @returns An object containing the numeric input, the input element itself,
 * and a function to dispatch events on the input element (which takes the event
 * type and event data to dispatch and supports a "special" event data "value"
 * which sets the input element's value before dispatching the event).
 */
export function newNumericInput(
  options: NumericInputOptions = {},
  { value = '', focus = false, caretSymbol = '|' } = {}
): {
  numericInput: NumericInput;
  inputElement: HTMLInputElement;
  dispatchEvent: (type: string, data?: Record<string, any>) => void;
} {
  const inputElement = document.createElement('input');
  document.body.appendChild(inputElement);
  focus && inputElement.focus();
  setValueAndSelection(inputElement, value, caretSymbol);
  const numericInput = new NumericInput(inputElement, options);
  return {
    inputElement,
    numericInput,
    dispatchEvent(type: string, data: Record<string, any> = {}) {
      if (data.value !== undefined) {
        setValueAndSelection(inputElement, data.value, caretSymbol);
      }
      inputElement.dispatchEvent(
        new KeyboardEvent(type, {
          bubbles: true,
          cancelable: type !== 'input',
          ...data,
        })
      );
    },
  };
}

/**
 * Gets the value and selection positions of a value written with caret symbols.
 * E.g. `'0|.0|0 €'` returns:
 * `{value: '0.00 €', selectionStart: 1, selectionEnd: 3}`.
 * @param value Value with carets.
 * @param caretSymbol Symbol to use as caret.
 * @returns Object with the value without the carets and with the selection
 * positions.
 */
export function getValueAndSelection(
  value: string,
  caretSymbol = '|'
): { value: string; selectionStart: number; selectionEnd: number } {
  let selectionStart = value.indexOf(caretSymbol);
  selectionStart === -1 && (selectionStart = 0);
  value = value.replace(caretSymbol, '');
  let selectionEnd = value.indexOf(caretSymbol);
  value = value.replace(caretSymbol, '');
  selectionEnd === -1 && (selectionEnd = selectionStart);
  return { value, selectionStart, selectionEnd };
}

/**
 * Sets the value and selection positions on an input element.
 * @param inputElement Input element on which to set value and selections.
 * @param value Value with carets.
 * @param caretSymbol Symbol to use as caret.
 */
export function setValueAndSelection(
  inputElement: HTMLInputElement,
  value: string,
  caretSymbol = '|'
): void {
  const {
    value: valueWithoutSelection,
    selectionStart,
    selectionEnd,
  } = getValueAndSelection(value, caretSymbol);
  inputElement.value = valueWithoutSelection;
  if (document.activeElement === inputElement) {
    inputElement.setSelectionRange(selectionStart, selectionEnd, 'none');
  }
}

/**
 * Checks whether an input element has the same value and selection positions as
 * a given value (with carets)
 * @param inputElement Input element to check.
 * @param value Value with carets.
 * @param caretSymbol Symbol to use as caret.
 */
export function assertEqualInputElement(
  inputElement: HTMLInputElement,
  value: string,
  caretSymbol = '|'
): void {
  const {
    value: valueWithoutSelection,
    selectionStart,
    selectionEnd,
  } = getValueAndSelection(value, caretSymbol);

  if (document.activeElement === inputElement) {
    expect(inputElement.selectionStart).toBe(selectionStart);
    expect(inputElement.selectionEnd).toBe(selectionEnd);
  }
  expect(inputElement.value).toBe(valueWithoutSelection);
}
