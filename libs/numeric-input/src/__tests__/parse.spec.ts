import { newNumericInput } from './test-util';

describe('Numeric input `parse`', () => {
  it('returns `null` when appropriate', () => {
    const { numericInput } = newNumericInput();
    // Signature doesn't support it, but we handle it anyway
    expect(numericInput.parse(undefined as any)).toBeNull();
    expect(numericInput.parse('')).toBeNull();
    expect(numericInput.parse('-')).toBeNull();
    expect(numericInput.parse('xxx')).toBeNull();
  });

  it('returns a negative zero', () => {
    const { numericInput } = newNumericInput();
    expect(numericInput.parse('-0')).toBe(-0);
  });

  it('strips all non-digit characters', () => {
    const { numericInput } = newNumericInput();
    expect(numericInput.parse('x1y2')).toBe(12);
    expect(numericInput.parse('-x1y2')).toBe(-12);
    expect(numericInput.parse('123.45')).toBe(12345);
    expect(numericInput.parse('-$12345€')).toBe(-12345);
  });

  it('removes leading zeroes', () => {
    const { numericInput } = newNumericInput({ scale: 2 });
    expect(numericInput.parse('01')).toBe(0.01);
    expect(numericInput.parse('000123')).toBe(1.23);
    expect(numericInput.parse('-0000')).toBe(-0);
  });

  it('handles scales properly', () => {
    const { numericInput: scale0Float } = newNumericInput({
      numericRepresentation: 'float',
      scale: 0,
    });
    const { numericInput: scale2Float } = newNumericInput({
      numericRepresentation: 'float',
      scale: 2,
    });
    const { numericInput: scale4Float } = newNumericInput({
      numericRepresentation: 'float',
      scale: 4,
    });
    const { numericInput: scale2Int } = newNumericInput({
      numericRepresentation: 'int',
      scale: 2,
    });
    expect(scale0Float.parse('123')).toBe(123);
    expect(scale0Float.parse('1.23')).toBe(123);
    expect(scale2Float.parse('1')).toBe(0.01);
    expect(scale2Float.parse('123')).toBe(1.23);
    expect(scale4Float.parse('1')).toBe(0.0001);
    expect(scale4Float.parse('123456')).toBe(12.3456);
    expect(scale2Int.parse('1234.56')).toBe(123456);
  });
});
