import {
  assertEqualInputElement,
  newNumericInput,
  setValueAndSelection,
} from './test-util';

describe('Numeric input events', () => {
  it('shallowly checks `min`/`max` bounds when typing', () => {
    const { numericInput, dispatchEvent } = newNumericInput({
      min: 100,
      max: 200,
    });
    dispatchEvent('input', { value: '150' });
    expect(numericInput.toNumber()).toBe(150);
    dispatchEvent('input', { value: '300' });
    expect(numericInput.toNumber()).toBe(150);
    dispatchEvent('input', { value: '50' });
    expect(numericInput.toNumber()).toBe(50);

    numericInput.setOptions({ min: -200, max: -100 }, null);
    dispatchEvent('input', { value: '-150' });
    expect(numericInput.toNumber()).toBe(-150);
    dispatchEvent('input', { value: '-300' });
    expect(numericInput.toNumber()).toBe(-150);
    dispatchEvent('input', { value: '-50' });
    expect(numericInput.toNumber()).toBe(-50);
  });

  it('allows out of bounds values when they are "less invalid"', () => {
    const { numericInput, dispatchEvent } = newNumericInput(
      { min: -100, max: 100 },
      { value: '100000' }
    );
    dispatchEvent('input', { value: '100000000000' });
    expect(numericInput.toNumber()).toBe(100000);
    dispatchEvent('input', { value: '10000' });
    expect(numericInput.toNumber()).toBe(10000);
    dispatchEvent('input', { value: '100000' });
    expect(numericInput.toNumber()).toBe(10000);
    dispatchEvent('input', { value: '500' });
    expect(numericInput.toNumber()).toBe(500);
    dispatchEvent('input', { value: '1000' });
    expect(numericInput.toNumber()).toBe(500);
    dispatchEvent('input', { value: '-200' });
    expect(numericInput.toNumber()).toBe(500);
    dispatchEvent('input', { value: '101' });
    expect(numericInput.toNumber()).toBe(101);

    numericInput.update('-100000');
    dispatchEvent('input', { value: '-100000000000' });
    expect(numericInput.toNumber()).toBe(-100000);
    dispatchEvent('input', { value: '-10000' });
    expect(numericInput.toNumber()).toBe(-10000);
    dispatchEvent('input', { value: '-100000' });
    expect(numericInput.toNumber()).toBe(-10000);
    dispatchEvent('input', { value: '-500' });
    expect(numericInput.toNumber()).toBe(-500);
    dispatchEvent('input', { value: '-1000' });
    expect(numericInput.toNumber()).toBe(-500);
    dispatchEvent('input', { value: '200' });
    expect(numericInput.toNumber()).toBe(-500);
    dispatchEvent('input', { value: '-101' });
    expect(numericInput.toNumber()).toBe(-101);
  });

  it('allows a single `-` to be input when the value can be negative', () => {
    const { numericInput, dispatchEvent } = newNumericInput({});
    dispatchEvent('input', { value: '-' });
    expect(numericInput.toString()).toBe('-0');

    numericInput.setOptions({ min: 0 }, null);
    dispatchEvent('input', { value: '-' });
    expect(numericInput.toString()).toBe('');
  });

  it('forces the number to be negative when writing from empty', () => {
    const { numericInput, dispatchEvent } = newNumericInput({ max: -10 });
    // Numeric input was empty so the value is forced to be negative on input
    dispatchEvent('input', { value: '10' });
    expect(numericInput.toNumber()).toBe(-10);

    // The value won't automatically become negative when it was positive before
    // (we don't want to toggle a value's sign when the user is deleting it, for
    // example)
    numericInput.update('10');
    dispatchEvent('input', { value: '1' });
    expect(numericInput.toNumber()).toBe(1);
  });

  it('forbids negative zero when the number cannot be negative', () => {
    const { numericInput, dispatchEvent } = newNumericInput({ min: 0 });
    dispatchEvent('input', { value: '-0' });
    expect(numericInput.toString()).toBe('0');
  });

  it('allows toggling the sign by pressing down "-"', () => {
    const { numericInput, dispatchEvent } = newNumericInput(
      { scale: 2, thousandsSeparator: ',' },
      { focus: true, value: '5,55|5.55' }
    );
    dispatchEvent('keydown', { key: '-' });
    expect(numericInput.toString()).toBe('-5,555.55');
    dispatchEvent('keydown', { key: '-' });
    expect(numericInput.toString()).toBe('5,555.55');
  });

  it('resets the input when deleting 0', () => {
    const { numericInput, dispatchEvent } = newNumericInput(
      { scale: 2 },
      { focus: true, value: '0.|00' }
    );
    dispatchEvent('keydown', { key: 'Backspace' });
    expect(numericInput.toString()).toBe('');
    numericInput.update('-0');
    dispatchEvent('keydown', { key: 'Delete' });
    expect(numericInput.toString()).toBe('');
  });

  it('moves the caret when deleting a separator', () => {
    const { inputElement, dispatchEvent } = newNumericInput(
      { scale: 2, thousandsSeparator: ',' },
      { focus: true, value: '5,555.|55' }
    );
    dispatchEvent('keydown', { key: 'Backspace' });
    assertEqualInputElement(inputElement, '5,555|.55');

    setValueAndSelection(inputElement, '5,|555.55');
    dispatchEvent('keydown', { key: 'Backspace' });
    assertEqualInputElement(inputElement, '5|,555.55');

    // Event does nothing:
    setValueAndSelection(inputElement, '5,55|5.55');
    dispatchEvent('keydown', { key: 'Backspace' });
    assertEqualInputElement(inputElement, '5,55|5.55');
    setValueAndSelection(inputElement, '5,55|5.5|5');
    dispatchEvent('keydown', { key: 'Backspace' });
    assertEqualInputElement(inputElement, '5,55|5.5|5');
    dispatchEvent('keydown', { key: 'Delete' });
    assertEqualInputElement(inputElement, '5,55|5.5|5');
  });

  it('places the caret after prefix/before suffix when focusing', async () => {
    const { inputElement, dispatchEvent } = newNumericInput(
      { prefix: 'P ', suffix: ' S' },
      { focus: true, value: 'P 555 S' }
    );
    setValueAndSelection(inputElement, '|P 555 S');
    dispatchEvent('focusin');
    await new Promise((res) => requestAnimationFrame(res));
    assertEqualInputElement(inputElement, 'P |555 S');

    setValueAndSelection(inputElement, 'P 555 S|');
    dispatchEvent('focusin');
    await new Promise((res) => requestAnimationFrame(res));
    assertEqualInputElement(inputElement, 'P 555| S');

    setValueAndSelection(inputElement, 'P 5|55 S');
    dispatchEvent('focusin');
    await new Promise((res) => requestAnimationFrame(res));
    assertEqualInputElement(inputElement, 'P 5|55 S');
  });

  it('disallows negative zeroes when unfocusing', () => {
    const { numericInput, dispatchEvent } = newNumericInput(
      { scale: 2 },
      { focus: true, value: '-1.00' }
    );
    dispatchEvent('focusout');
    expect(numericInput.toString()).toBe('-1.00');
    numericInput.update('-0');
    expect(numericInput.toString()).toBe('-0.00');
    dispatchEvent('focusout');
    expect(numericInput.toString()).toBe('0.00');
  });

  it('allows destroying the numeric input', () => {
    const { numericInput, dispatchEvent } = newNumericInput({});
    numericInput.destroy();

    // Events no longer do anything
    dispatchEvent('input', { value: '-' });
    expect(numericInput.toString()).toBe('');
  });
});
