import { newNumericInput } from './test-util';

describe('Numeric input options', () => {
  it('validates `scale`', () => {
    expect(() => newNumericInput({ scale: -1 })).toThrow();
    expect(() => newNumericInput({ scale: 3.5 })).toThrow();
    expect(() => newNumericInput({ scale: Infinity })).toThrow();
    expect(() => newNumericInput({ scale: 101 })).toThrow();
    newNumericInput({ scale: 0 });
    const { numericInput } = newNumericInput({ scale: 100 });
    expect(() => numericInput.setOptions({ scale: -5 })).toThrow();
  });

  it('validates `leadingZeroes`', () => {
    expect(() => newNumericInput({ leadingZeroes: 0 })).toThrow();
    expect(() => newNumericInput({ leadingZeroes: 3.5 })).toThrow();
    expect(() => newNumericInput({ leadingZeroes: Infinity })).toThrow();
    newNumericInput({ leadingZeroes: 1 });
    const { numericInput } = newNumericInput({ leadingZeroes: 555 });
    expect(() => numericInput.setOptions({ leadingZeroes: -10 })).toThrow();
  });

  it('validates `min`/`max`', () => {
    expect(() => newNumericInput({ min: 20, max: 10 })).toThrow();
    newNumericInput({ min: 0, max: 0 });
    const { numericInput } = newNumericInput({ min: -123.45, max: 123.45 });
    expect(() => numericInput.setOptions({ min: 500 })).toThrow();
    expect(() => numericInput.setOptions({ max: -999 })).toThrow();
    numericInput.setOptions({ min: 10 });
    numericInput.setOptions({ min: 0, max: 9 });
  });

  it('validates `prefix`/`suffix`', () => {
    expect(() => newNumericInput({ prefix: '1' })).toThrow();
    expect(() => newNumericInput({ prefix: 'x-' })).toThrow();
    expect(() => newNumericInput({ suffix: '1' })).toThrow();
    expect(() => newNumericInput({ suffix: '-x' })).toThrow();
    newNumericInput({ prefix: '$ ' });
    const { numericInput } = newNumericInput({ suffix: ' EUR' });
    expect(() => numericInput.setOptions({ prefix: 'x0' })).toThrow();
    expect(() => numericInput.setOptions({ suffix: '9x' })).toThrow();
  });

  it('validates `decimalSeparator`/`thousandsSeparator`', () => {
    expect(() => newNumericInput({ decimalSeparator: '1' })).toThrow();
    expect(() => newNumericInput({ decimalSeparator: '-' })).toThrow();
    expect(() => newNumericInput({ decimalSeparator: '' })).toThrow();
    expect(() => newNumericInput({ decimalSeparator: 'xx' })).toThrow();
    expect(() => newNumericInput({ thousandsSeparator: '1' })).toThrow();
    expect(() => newNumericInput({ thousandsSeparator: '-' })).toThrow();
    expect(() => newNumericInput({ thousandsSeparator: 'xx' })).toThrow();
    newNumericInput({ decimalSeparator: ',' });
    newNumericInput({ thousandsSeparator: '' });
    const { numericInput } = newNumericInput({ thousandsSeparator: ' ' });
    expect(() => numericInput.setOptions({ decimalSeparator: '2' })).toThrow();
    expect(() =>
      numericInput.setOptions({ thousandsSeparator: '-' })
    ).toThrow();
  });

  it('calls `update` after setting options', () => {
    const { numericInput } = newNumericInput({ scale: 2 }, { value: '5000' });
    expect(numericInput.toString()).toBe('50.00');
    expect(numericInput.toNumber()).toBe(50);
    numericInput.setOptions({ decimalSeparator: ',' });
    expect(numericInput.toString()).toBe('50,00');
    // By default `update` is called with the input element's current value
    numericInput.setOptions({ numericRepresentation: 'int' });
    expect(numericInput.toString()).toBe('50,00');
    expect(numericInput.toNumber()).toBe(5000);
  });

  it('allows passing a new value when setting options', () => {
    const { numericInput } = newNumericInput({ scale: 2 }, { value: '5000' });
    expect(numericInput.toString()).toBe('50.00');
    expect(numericInput.toNumber()).toBe(50);
    numericInput.setOptions({ numericRepresentation: 'int' }, 50);
    expect(numericInput.toString()).toBe('0.50');
    expect(numericInput.toNumber()).toBe(50);
  });
});
