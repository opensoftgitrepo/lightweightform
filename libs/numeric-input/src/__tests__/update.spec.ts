import {
  assertEqualInputElement,
  newNumericInput,
  setValueAndSelection,
} from './test-util';

describe('Numeric input `update`', () => {
  it('is updated with the initial value of the input element', () => {
    const { numericInput } = newNumericInput({ scale: 2 }, { value: '0' });
    expect(numericInput.toString()).toBe('0.00');
    expect(numericInput.toNumber()).toBe(0);
  });

  it('allows negative zero to be displayed from the initial value', () => {
    const { numericInput } = newNumericInput({ scale: 2 }, { value: '-0' });
    expect(numericInput.toString()).toBe('-0.00');
  });

  it('allows negative zero to be displayed', () => {
    const { numericInput } = newNumericInput({ scale: 2 });
    numericInput.update('-0');
    expect(numericInput.toString()).toBe('-0.00');
  });

  it('ignores `min`/`max` bounds', () => {
    const { numericInput } = newNumericInput({ min: -20, max: 20 });
    numericInput.update(1000);
    expect(numericInput.isValid()).toBe(false);
    expect(numericInput.toString()).toBe('1000');
    numericInput.update(10);
    expect(numericInput.isValid()).toBe(true);
  });

  it('places caret after the `-` and prefix', () => {
    const { inputElement, numericInput } = newNumericInput(
      { scale: 2, prefix: '$' },
      { focus: true, value: '|$1.00' }
    );
    assertEqualInputElement(inputElement, '$|1.00');
    setValueAndSelection(inputElement, '|-$55.55');
    numericInput.update(inputElement.value);
    assertEqualInputElement(inputElement, '-$|55.55');
    setValueAndSelection(inputElement, '-|$999.99');
    numericInput.update(inputElement.value);
    assertEqualInputElement(inputElement, '-$|999.99');
  });

  it('places caret before the suffix', () => {
    const { inputElement, numericInput } = newNumericInput(
      { suffix: ' €' },
      { focus: true, value: '1 €|' }
    );
    assertEqualInputElement(inputElement, '1| €');
    setValueAndSelection(inputElement, '555 €|');
    numericInput.update(inputElement.value);
    assertEqualInputElement(inputElement, '555| €');
  });

  it('places caret after all leading zeroes', () => {
    const { inputElement, numericInput } = newNumericInput(
      { scale: 2, leadingZeroes: 2, prefix: '$' },
      { focus: true, value: '|$00.01' }
    );
    assertEqualInputElement(inputElement, '$00.0|1');
    setValueAndSelection(inputElement, '|-$00.55');
    numericInput.update(inputElement.value);
    assertEqualInputElement(inputElement, '-$00|.55');
    setValueAndSelection(inputElement, '$|0009.99');
    numericInput.update(inputElement.value);
    assertEqualInputElement(inputElement, '$0|9.99');
    setValueAndSelection(inputElement, '|$00.00');
    numericInput.update(inputElement.value);
    assertEqualInputElement(inputElement, '$00.00|');
  });

  it('keeps caret position whilst typing', () => {
    const { inputElement, numericInput } = newNumericInput(
      { scale: 2 },
      { focus: true, value: '0.0|1' }
    );
    assertEqualInputElement(inputElement, '0.0|1');
    setValueAndSelection(inputElement, '0.09|1');
    numericInput.update(inputElement.value);
    assertEqualInputElement(inputElement, '0.9|1');
    setValueAndSelection(inputElement, '0.98|1');
    numericInput.update(inputElement.value);
    assertEqualInputElement(inputElement, '9.8|1');
    setValueAndSelection(inputElement, '-9.8|1');
    numericInput.update(inputElement.value);
    assertEqualInputElement(inputElement, '-9.8|1');
  });

  it('calls `onUpdate` when appropriate', () => {
    let stringValue = '';
    let numberValue: number | null = null;
    let called = 0;
    const { numericInput } = newNumericInput({
      onUpdate: (str, num) => {
        stringValue = str;
        numberValue = num;
        called++;
      },
    });
    numericInput.update('1');
    expect(stringValue).toBe('1');
    expect(numberValue).toBe(1);
    expect(called).toBe(1);
    numericInput.update('1');
    expect(called).toBe(1);
    numericInput.update('2');
    expect(stringValue).toBe('2');
    expect(numberValue).toBe(2);
    expect(called).toBe(2);
  });
});
