# Lightweightform/storage

This package provides the means for defining the schema (data-type) of a
Lightweightform application. It further provides a storage class: used for
holding the schema and (MobX observable) value of the whole Lightweightform
application as well as arbitrary state information about each value. All value
manipulations should pass through an instance of said class in order to
guarantee correctness in terms of types and observable behaviour.

Whilst this package was made with the intent of being used by Lightweightform,
it should stand generic enough to be used by any application in need of a
MobX-compatible storage where each value may be bound by a set of constraints
defined in its schema.

## Example usage

```ts
import {
  listSchema,
  recordSchema,
  stringSchema,
  Storage,
} from '@lightweightform/storage';

// Create the application schema
const appSchema = recordSchema({
  person: recordSchema({
    name: stringSchema({ minLength: 1 }),
  }),
  pets: listSchema(
    recordSchema({
      name: stringSchema({ minLength: 1 }),
      species: stringSchema({ allowedValues: ['dog', 'cat', 'turtle'] }),
    }),
    { minSize: 1 }
  ),
});

// Create the storage instance
const storage = new Storage(appSchema);

// Operate on the storage instance
storage.set('/person/name', 'Alice');
storage.push('/pets', { name: 'Rex', species: 'dog' });
storage.hasIssues('/'); // Returns `false`
storage.set('/pets/0/species', 'hamster');
storage.hasIssues('/'); // Returns `true`
```

## Documentation and API

All Lightweightform documentation is available at:
https://docs.lightweightform.io.

The `@lightweightform/storage` API is available at:
https://docs.lightweightform.io/api/storage.

## Browser support

- **Internet Explorer**: 9+ (requires `Map` and `Array.from` polyfills)
- **Other browsers**: Latest 2 versions
