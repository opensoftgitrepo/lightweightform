# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.13.1"></a>

## [4.13.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.13.0&sourceBranch=refs%2Ftags%2Fv4.13.1) (2022-02-03)

### Bug Fixes

- default to "ifavailable" MobX proxy config
  ([fc16532](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/fc16532))

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.13.0"></a>

# [4.13.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.12.2&sourceBranch=refs%2Ftags%2Fv4.13.0) (2022-01-27)

### Features

- update dependencies
  ([f921338](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/f921338))

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.12.2"></a>

## [4.12.2](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.12.1&sourceBranch=refs%2Ftags%2Fv4.12.2) (2021-10-27)

### Bug Fixes

- **storage:** data type of 'is required' issue
  ([37e704e](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/37e704e))

<a name="4.12.1"></a>

## [4.12.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.12.0&sourceBranch=refs%2Ftags%2Fv4.12.1) (2021-10-27)

### Bug Fixes

- issue types + date-range
  ([cbab3a2](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/cbab3a2))

<a name="4.12.0"></a>

# [4.12.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.11.2&sourceBranch=refs%2Ftags%2Fv4.12.0) (2021-10-26)

### Features

- **storage:** support custom codes in standard issues
  ([09c6abf](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/09c6abf))

<a name="4.11.1"></a>

## [4.11.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.11.0&sourceBranch=refs%2Ftags%2Fv4.11.1) (2021-10-22)

### Bug Fixes

- **bootstrap-theme:** prevent access to state props in manual mode
  ([64588a8](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/64588a8))

<a name="4.11.0"></a>

# [4.11.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.10.2&sourceBranch=refs%2Ftags%2Fv4.11.0) (2021-10-20)

### Features

- support "manual" validation
  ([d2e1aff](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/d2e1aff))

<a name="4.9.1"></a>

## [4.9.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.9.0&sourceBranch=refs%2Ftags%2Fv4.9.1) (2021-07-11)

### Bug Fixes

- **storage:** relax type checking to allow any object-like
  ([ea558c0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/ea558c0))

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.7.0"></a>

# [4.7.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.6.1&sourceBranch=refs%2Ftags%2Fv4.7.0) (2020-12-15)

**Note:** Version bump only for package @lightweightform/storage

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.5.4"></a>

## [4.5.4](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.5.3&sourceBranch=refs%2Ftags%2Fv4.5.4) (2020-10-28)

**Note:** Version bump only for package @lightweightform/storage

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.5.0"></a>

# [4.5.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.4.6&sourceBranch=refs%2Ftags%2Fv4.5.0) (2020-02-19)

### Features

- support Angular 9
  ([54a7154](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/54a7154))

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.4.5"></a>

## [4.4.5](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.4.4&sourceBranch=refs%2Ftags%2Fv4.4.5) (2020-01-23)

**Note:** Version bump only for package @lightweightform/storage

<a name="4.4.4"></a>

## [4.4.4](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.4.3&sourceBranch=refs%2Ftags%2Fv4.4.4) (2020-01-22)

**Note:** Version bump only for package @lightweightform/storage

<a name="4.4.3"></a>

## [4.4.3](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.4.2&sourceBranch=refs%2Ftags%2Fv4.4.3) (2020-01-19)

**Note:** Version bump only for package @lightweightform/storage

<a name="4.4.2"></a>

## [4.4.2](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.4.1&sourceBranch=refs%2Ftags%2Fv4.4.2) (2020-01-18)

**Note:** Version bump only for package @lightweightform/storage

<a name="4.4.1"></a>

## [4.4.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.4.0&sourceBranch=refs%2Ftags%2Fv4.4.1) (2020-01-14)

### Bug Fixes

- repo-wide spellchecks + small improvements
  ([90545fb](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/90545fb))

<a name="4.2.0"></a>

# [4.2.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.1.0&sourceBranch=refs%2Ftags%2Fv4.2.0) (2019-12-08)

### Features

- support AoT compilation
  ([bd9e886](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/bd9e886)),
  closes #43

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.1.0"></a>

# [4.1.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.0.1&sourceBranch=refs%2Ftags%2Fv4.1.0) (2019-11-10)

**Note:** Version bump only for package @lightweightform/storage

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.0.0"></a>

# [4.0.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.0.0-beta.19&sourceBranch=refs%2Ftags%2Fv4.0.0) (2019-10-19)

Initial LF 4 release.
