import {
  BooleanSchema,
  DateSchema,
  ListSchema,
  MapSchema,
  NumberSchema,
  RecordSchema,
  Schema,
  StringSchema,
  TableSchema,
  TupleSchema,
} from '../schema-types';

import { KnownKeys } from './ts-type-utils';

/**
 * Options used to create a new boolean schema.
 */
export type BooleanSchemaOptions = Pick<
  BooleanSchema,
  Exclude<KnownKeys<BooleanSchema>, 'type'>
> & { [extraProperties: string]: any };

/**
 * Options used to create a new date schema.
 */
export type DateSchemaOptions = Pick<
  DateSchema,
  Exclude<KnownKeys<DateSchema>, 'type'>
> & { [extraProperties: string]: any };

/**
 * Options used to create a new list schema.
 * @param T Type of the list elements.
 * @param S Schema of the list elements.
 */
export type ListSchemaOptions<T = any, S extends Schema<T> = Schema<T>> = Pick<
  ListSchema<T, S>,
  Exclude<KnownKeys<ListSchema<T, S>>, 'type' | 'elementsSchema'>
> & { [extraProperties: string]: any };

/**
 * Options used to create a new map schema.
 * @param T Type of the map values.
 * @param S Schema of the map values.
 */
export type MapSchemaOptions<T = any, S extends Schema<T> = Schema<T>> = Pick<
  MapSchema<T, S>,
  Exclude<KnownKeys<MapSchema<T, S>>, 'type' | 'valuesSchema'>
> & { [extraProperties: string]: any };

/**
 * Options used to create a new number schema.
 */
export type NumberSchemaOptions = Pick<
  NumberSchema,
  Exclude<KnownKeys<NumberSchema>, 'type'>
> & { [extraProperties: string]: any };

/**
 * Options used to create a new record schema.
 * @param T Type of the record that the schema represents.
 * @param S Schemas of all record properties.
 */
export type RecordSchemaOptions<
  T extends Record<keyof T, any> = Record<any, any>,
  S extends { [F in keyof T]: Schema<T[F]> } = { [F in keyof T]: Schema<T[F]> }
> = Pick<
  RecordSchema<T, S>,
  Exclude<KnownKeys<RecordSchema<T, S>>, 'type' | 'fieldsSchemas'>
> & { [extraProperties: string]: any };

/**
 * Options used to create a new string schema.
 */
export type StringSchemaOptions = Pick<
  StringSchema,
  Exclude<KnownKeys<StringSchema>, 'type'>
> & { [extraProperties: string]: any };

/**
 * Options used to create a new table schema.
 * @param T Type of the table rows.
 * @param S Schema of the table rows.
 */
export type TableSchemaOptions<
  T extends Record<string, any> = Record<string, any>,
  S extends RecordSchema<T> = RecordSchema<T>
> = Pick<
  TableSchema<T, S>,
  Exclude<KnownKeys<TableSchema<T, S>>, 'type' | 'rowsSchema'>
> & { [extraProperties: string]: any };

/**
 * Options used to create a new tuple schema.
 * @param T Type of the tuple that the schema represents.
 * @param S Schemas of all tuple elements.
 */
export type TupleSchemaOptions<
  T extends any[] = any[],
  S extends Schema[] = Schema[]
> = Pick<
  TupleSchema<T, S>,
  Exclude<KnownKeys<TupleSchema<T, S>>, 'type' | 'elementsSchemas'>
> & { [extraProperties: string]: any };

/**
 * Creates a new boolean schema object.
 * @param options Options used to create the schema.
 * @returns Boolean schema.
 */
export function booleanSchema(
  options: BooleanSchemaOptions = {}
): BooleanSchema {
  return { type: 'boolean', ...options };
}

/**
 * Creates a new date schema object.
 * @param options Options used to create the schema.
 * @returns Date schema.
 */
export function dateSchema(options: DateSchemaOptions = {}): DateSchema {
  return { type: 'date', ...options };
}

/**
 * Creates a new list schema object.
 * @param elementsSchema Schema of the list's elements.
 * @param options Options used to create the schema.
 * @returns List schema.
 */
export function listSchema<T = any, S extends Schema<T> = Schema<T>>(
  elementsSchema: S,
  options: ListSchemaOptions<T, S> = {}
): ListSchema<T, S> {
  return { type: 'list', elementsSchema, ...options };
}

/**
 * Creates a new map schema object.
 * @param valuesSchema Schema of the map's elements.
 * @param options Options used to create the schema.
 * @returns Map schema.
 */
export function mapSchema<T = any, S extends Schema<T> = Schema<T>>(
  valuesSchema: S,
  options: MapSchemaOptions = {}
): MapSchema<T, S> {
  return { type: 'map', valuesSchema, ...options };
}

/**
 * Creates a new number schema object.
 * @param options Options used to create the schema.
 * @returns Number schema.
 */
export function numberSchema(options: NumberSchemaOptions = {}): NumberSchema {
  return { type: 'number', ...options };
}

/**
 * Creates a new record schema object.
 * @param fieldsSchemas Schemas of the record's fields.
 * @param options Options used to create the schema.
 * @returns Record schema.
 */
export function recordSchema<
  T extends Record<keyof T, any> = Record<any, any>,
  S extends { [F in keyof T]: Schema<T[F]> } = { [F in keyof T]: Schema<T[F]> }
>(
  fieldsSchemas: S & { [F in keyof T]: Schema<T[F]> },
  options: RecordSchemaOptions<T, S> = {}
): RecordSchema<T, S> {
  return { type: 'record', fieldsSchemas, ...options };
}

/**
 * Creates a new string schema object.
 * @param options Options used to create the schema.
 * @returns String schema.
 */
export function stringSchema(options: StringSchemaOptions = {}): StringSchema {
  return { type: 'string', ...options };
}

/**
 * Creates a new table schema object.
 * @param rowsSchema Schema of the table's rows.
 * @param options Options used to create the schema.
 * @returns Table schema.
 */
export function tableSchema<
  T extends Record<string, any> = Record<string, any>,
  S extends RecordSchema<T> = RecordSchema<T>
>(rowsSchema: S, options: TableSchemaOptions<T, S> = {}): TableSchema<T, S> {
  return { type: 'table', rowsSchema, ...options };
}

/**
 * Creates a new tuple schema object. Unfortunately, TypeScript does not allow
 * accessing element types of a tuple; as such, for better type checking, the
 * creation of tuple schemas requires the specification of its type arguments.
 * Example for a tuple with elements schemas `[StringSchema, NumberSchema]`:
 * ```typescript
 * tupleSchema<
 *   [string, number],
 *   [StringSchema, NumberSchema]
 * >([stringSchema(), numberSchema()])
 * ```
 * @param elementsSchemas Schemas of the tuple elements.
 * @param options Options used to create the schema.
 * @returns Tuple schema.
 */
// TODO: Confirm that it is still impossible with newer versions of TS.
export function tupleSchema<
  T extends any[] = any[],
  S extends Schema[] = Schema[]
>(
  elementsSchemas: S & Schema[],
  options: TupleSchemaOptions<T, S> = {}
): TupleSchema<T, S> {
  return { type: 'tuple', elementsSchemas, ...options };
}
