import { Schema } from '../schema-types';

import { pathError } from './error-utils';
import { appendToPath, isValidPathId, PATH_ID_PLACEHOLDER } from './path-utils';
import {
  isClientOnlySchema,
  isCollectionSchema,
  isComputedSchema,
  isDateSchema,
  isListSchema,
  isMapSchema,
  isNullableSchema,
  isNumberSchema,
  isRecordSchema,
  isStringSchema,
  isTableSchema,
  isTupleSchema,
} from './schema-checker';
import { $lfRowId } from './table-utils';

/**
 * Schema properties that the root schema cannot define as functions (since they
 * cannot depend on themselves).
 */
export const ROOT_FORBIDDEN_FUNCTIONS = [
  'initialValue',
  'isRequired',
  'allowedValues',
  'minDate',
  'maxDate',
  'minSize',
  'maxSize',
  'min',
  'max',
  'minLength',
  'maxLength',
];

/**
 * Schema properties that children of computed schemas cannot define.
 */
export const COMPUTED_CHILDREN_FORBIDDEN_PROPS = [
  'initialState',
  'isRequired',
  'allowedValues',
  'validate',
  'minDate',
  'maxDate',
  'minSize',
  'maxSize',
  'min',
  'max',
  'minLength',
  'maxLength',
];

/**
 * Validates the root schema of a storage against structural errors. For
 * example, schemas cannot have both an initial value and a computed value;
 * record schemas cannot have fields whose names contain forward slashes (since
 * they conflict with path definitions). This function throws an error if the
 * schema is invalid.
 * @param schema Schema to validate.
 * @param path The schema to validate must be the storage's root schema, this
 * means that this function should always be called with path set to `'/'` or,
 * preferably, with no path (since `'/'` is the default). This argument is used
 * internally to keep track of the path of the schema being validated.
 * @param ancestorIsComputed Argument used internally to know whether the
 * current schema being validates is a descendant of a computed schema.
 * @param parentIsMaybeSchema Argument used internally to know whether the
 * parent of the schema being currently validates is of type "maybe".
 */
export function validateSchema(
  schema: Schema,
  path: string = '/',
  ancestorIsComputed = false
): void {
  // Validate nullable schema
  if (!isNullableSchema(schema) && schema.isRequired !== undefined) {
    throw pathError(
      path,
      'Schemas can only define `isRequired` when they are nullable'
    );
  }
  const schemaIsComputed = isComputedSchema(schema);
  const isComputed = ancestorIsComputed || schemaIsComputed;
  // Validate root schema
  if (path === '/') {
    if (schemaIsComputed) {
      throw pathError(
        path,
        'The root schema cannot represent a computed value'
      );
    }
    if (schema.isClientOnly) {
      throw pathError(path, 'The root schema can never be client-only');
    }
    for (const attr of ROOT_FORBIDDEN_FUNCTIONS) {
      if (typeof schema[attr] === 'function') {
        throw pathError(
          path,
          `The root schema cannot define \`${attr}\` as a function`
        );
      }
    }
  }
  // Validate computed schema
  if (isComputed) {
    if (schemaIsComputed && ancestorIsComputed) {
      throw pathError(
        path,
        'Children of schemas that represent a computed value cannot ' +
          'themselves represent computed values'
      );
    }
    if (schema.initialValue !== undefined) {
      throw pathError(
        path,
        'Schemas that represent a computed value (or children of such ' +
          'schemas) cannot define an initial value'
      );
    }
    if (ancestorIsComputed) {
      for (const attr of COMPUTED_CHILDREN_FORBIDDEN_PROPS) {
        if (schema[attr] !== undefined) {
          throw pathError(
            path,
            'Children of schemas that represent a computed value cannot ' +
              `define \`${attr}\``
          );
        }
      }
    }
    if (isTableSchema(schema)) {
      throw pathError(
        path,
        'Table schemas may not be computed, use a list schema instead'
      );
    }
  }
  // Validate initial state properties
  if (schema.initialState !== undefined) {
    for (const prop of Object.keys(schema.initialState)) {
      if (isReservedStateProp(schema, prop)) {
        throw pathError(path, `State property "${prop}" is reserved`);
      }
    }
  }
  // Validate list/map/table schema
  if (isListSchema(schema) || isMapSchema(schema) || isTableSchema(schema)) {
    const childrenSchema = isListSchema(schema)
      ? schema.elementsSchema
      : isMapSchema(schema)
      ? schema.valuesSchema
      : schema.rowsSchema;
    const childrenPath = appendToPath(path, PATH_ID_PLACEHOLDER);
    // A limitation of using MobX is that we cannot have computed values as
    // direct children of lists or maps. It shouldn't make a lot of sense anyway
    // since then we might as well declare the list or map itself as computed
    if (isComputedSchema(childrenSchema)) {
      throw pathError(
        childrenPath,
        'Schemas that represent computed values cannot be direct children of ' +
          `${schema.type} schemas`
      );
    }
    // This could be allowed but makes no sense so might as well disallow it.
    // Instead the list, map, or table itself should be made client-only
    if (isClientOnlySchema(childrenSchema)) {
      throw pathError(
        childrenPath,
        'Schemas that represent client-only values cannot be direct children ' +
          `of ${schema.type} schemas`
      );
    }
    if (isTableSchema(schema)) {
      if (isNullableSchema(childrenSchema)) {
        throw pathError(childrenPath, 'Table rows cannot be nullable');
      }
      // Forbid usage of the property used to keep track of rows' keys
      if ($lfRowId in childrenSchema.fieldsSchemas) {
        throw pathError(childrenPath, `'${$lfRowId}' is a reserved field name`);
      }
    }
    validateSchema(childrenSchema, childrenPath, isComputed);
  }
  // Validate record schema
  if (isRecordSchema(schema)) {
    for (const field of Object.keys(schema.fieldsSchemas)) {
      if (!isValidPathId(field)) {
        throw pathError(path, `Invalid record field '${field}'`);
      }
      validateSchema(
        schema.fieldsSchemas[field],
        appendToPath(path, field),
        isComputed
      );
    }
  }
  // Validate tuple schema
  if (isTupleSchema(schema)) {
    schema.elementsSchemas.forEach((elementSchema, i) =>
      validateSchema(elementSchema, appendToPath(path, i), isComputed)
    );
  }
}

/**
 * Whether a given property name is reserved for a given schema.
 * @param schema Schema on which to check if the state property is reserved.
 * @param prop Property to check if is reserved.
 * @returns Whether the property is reserved in the given schema.
 */
export function isReservedStateProp(schema: Schema, prop: string): boolean {
  // Reserved independently of schema
  if (['allowedValues', 'validate', 'validationIssues'].indexOf(prop) !== -1) {
    return true;
  }
  // Reserved for nullable schemas
  if (prop === 'isRequired' && isNullableSchema(schema)) {
    return true;
  }
  // Schema-exclusive properties
  if (isDateSchema(schema)) {
    return prop === 'minDate' || prop === 'maxDate';
  }
  if (isCollectionSchema(schema)) {
    return prop === 'minSize' || prop === 'maxSize';
  }
  if (isNumberSchema(schema)) {
    return prop === 'min' || prop === 'max';
  }
  if (isStringSchema(schema)) {
    return prop === 'minLength' || prop === 'maxLength';
  }
  return false;
}
