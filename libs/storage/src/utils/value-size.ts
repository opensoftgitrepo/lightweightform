import { isObservableMap } from 'mobx';

import { isArrayLike } from './array-utils';

/**
 * Computed the size of a given value, where each simple value has size `1` and
 * all complex values have size `1` plus the size of all of its children.
 * @param value Value with size to count.
 * @returns Size of the value.
 */
export function valueSize(value: any): number {
  let size = 1;
  if (isArrayLike(value)) {
    for (const el of value) {
      size += valueSize(el);
    }
  } else if (value instanceof Map || isObservableMap(value)) {
    for (const val of value.values()) {
      size += valueSize(val);
    }
  } else if (value !== null && typeof value === 'object') {
    for (const key of Object.keys(value)) {
      size += valueSize(value[key]);
    }
  }
  return size;
}
