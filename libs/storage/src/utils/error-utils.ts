/**
 * Validation issue.
 */
export interface ValidationIssue {
  /**
   * Code which represents the issue.
   */
  code: string;
  /**
   * Extra data related to the issue.
   */
  data?: Record<string, any>;
  /**
   * Issues may have additional arbitrary properties.
   */
  [extraProperties: string]: any;
}

/**
 * List of validation issues.
 */
export type ValidationIssues = ValidationIssue[];

/**
 * Maps paths to the list of reasons for that path to be invalid.
 */
export type ValidationIssuesMap = Record<string, ValidationIssues>;

/**
 * Whether a passed value is a validation issue.
 * @param value Value to check if is a validation issue.
 * @returns Whether the passed value is a validation issue.
 */
export function isValidationIssue(value: any): value is ValidationIssue {
  return typeof value === 'object' && value.code !== undefined;
}

/**
 * Adds a new validation issue which occurred at a given path to the map of all
 * validation issues.
 * @param issues Map with all validation issues.
 * @param path Path on which the issue was found.
 * @param error Validation issue to add.
 */
export function addValidationIssue(
  issues: ValidationIssuesMap,
  path: string,
  error: ValidationIssue
): void {
  if (!issues[path]) {
    issues[path] = [];
  }
  issues[path].push(error);
}

/**
 * Returns an error instance given the path on which the error occurred and the
 * reason for the error. The error is not thrown. Example:
 * ```
 * throw pathError('/a', 'Reason');
 * ```
 * @param path Path where error occurred.
 * @param reason Reason for the error.
 * @param errorConstructor Constructor of the error to throw (`Error` by
 * default).
 */
export function pathError(
  path: string,
  reason: string,
  errorConstructor: any = Error
): Error & { path: string; reason: string } {
  const error: any = new errorConstructor(`At '${path}': ${reason}`);
  error.path = path;
  error.reason = reason;
  return error;
}

/**
 * Returns an error caused by an invalid schema type.
 * @param path Absolute path where the schema is of invalid type.
 * @param allowed Allowed schema types.
 * @param useAn Whether to use the indefinite article "an" instead of "a" before
 * enumerating the expected schema types.
 * @returns An error to be thrown due to an invalid schema.
 */
export function invalidSchemaTypeError(
  path: string,
  allowed: string[]
): Error & { path: string; reason: string; allowed: string[] } {
  const allowedStr =
    allowed.length <= 2
      ? allowed.join(' or ')
      : `${allowed.slice(0, -1).join(', ')}, or ${allowed.slice(-1)}`;
  const error: any = pathError(
    path,
    `Invalid schema type, expected: ${allowedStr} schema`
  );
  error.allowed = allowed;
  return error;
}
