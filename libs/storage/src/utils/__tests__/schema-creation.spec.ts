import {
  booleanSchema,
  dateSchema,
  listSchema,
  mapSchema,
  numberSchema,
  recordSchema,
  stringSchema,
  tupleSchema,
} from '../schema-creation';

// Example options
const options = { someProp: 'x' };
// Example child schema
const childSch = numberSchema();

describe('Schema creation', () => {
  it('creates a boolean schema', () => {
    expect(booleanSchema(options)).toEqual({ type: 'boolean', ...options });
  });

  it('creates a date schema', () => {
    expect(dateSchema(options)).toEqual({ type: 'date', ...options });
  });

  it('creates a list schema', () => {
    expect(listSchema(childSch, options)).toEqual({
      type: 'list',
      elementsSchema: childSch,
      ...options,
    });
  });

  it('creates a map schema', () => {
    expect(mapSchema(childSch, options)).toEqual({
      type: 'map',
      valuesSchema: childSch,
      ...options,
    });
  });

  it('creates a number schema', () => {
    expect(numberSchema(options)).toEqual({ type: 'number', ...options });
  });

  it('creates a record schema', () => {
    expect(recordSchema({ ch: childSch }, options)).toEqual({
      type: 'record',
      fieldsSchemas: { ch: childSch },
      ...options,
    });
  });

  it('creates a string schema', () => {
    expect(stringSchema(options)).toEqual({ type: 'string', ...options });
  });

  it('creates a tuple schema', () => {
    expect(tupleSchema([childSch], options)).toEqual({
      type: 'tuple',
      elementsSchemas: [childSch],
      ...options,
    });
  });
});
