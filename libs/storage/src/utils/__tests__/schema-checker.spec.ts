import * as schemaChecker from '../schema-checker';
import { numberSchema } from '../schema-creation';
const {
  isClientOnlySchema,
  isComplexSchema,
  isComputedSchema,
  isNullableSchema,
} = schemaChecker;

const schemaTypes = [
  'boolean',
  'date',
  'list',
  'map',
  'number',
  'record',
  'string',
  'tuple',
];

const complexTypes = ['list', 'map', 'record', 'tuple'];

describe('Schema checker', () => {
  it('detects schema objects', () => {
    for (const type of schemaTypes) {
      // e.g. `isTTTSchema({type: 'TTT'})` for each schema type
      const capitalized = type[0].toUpperCase() + type.slice(1);
      expect(schemaChecker[`is${capitalized}Schema`]({ type })).toBe(true);
      expect(schemaChecker[`is${capitalized}Schema`]({ type: null })).toBe(
        false
      );
    }
  });

  it('detects complex schemas', () => {
    for (const type of schemaTypes) {
      expect(isComplexSchema({ type })).toBe(complexTypes.indexOf(type) > -1);
    }
  });

  it('detects computed schemas', () => {
    expect(isComputedSchema(numberSchema({ computedValue: () => 0 }))).toBe(
      true
    );
    expect(isComputedSchema(numberSchema())).toBe(false);
  });

  it('detects client-only schemas', () => {
    expect(isClientOnlySchema(numberSchema({ isClientOnly: true }))).toBe(true);
    expect(isClientOnlySchema(numberSchema())).toBe(false);
    expect(isClientOnlySchema(numberSchema({ computedValue: () => 0 }))).toBe(
      true
    );
    expect(
      isClientOnlySchema(
        numberSchema({ computedValue: () => 0, isClientOnly: false })
      )
    ).toBe(false);
  });

  it('detects nullable schemas', () => {
    expect(isNullableSchema(numberSchema())).toBe(false);
    expect(isNullableSchema(numberSchema({ isNullable: true }))).toBe(true);
    expect(isNullableSchema(numberSchema({ isNullable: false }))).toBe(false);
  });
});
