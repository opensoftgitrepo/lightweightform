import { defineGetter } from '../object-utils';

describe('Object utilities', () => {
  it('defines a getter in an object', () => {
    // Check that the property descriptor of the getter is similar to the one
    // obtained from a native getter
    const customDescriptor = Object.getOwnPropertyDescriptor(
      defineGetter({}, 'prop', () => 1),
      'prop'
    )!;
    const nativeDescriptor = Object.getOwnPropertyDescriptor(
      {
        get prop() {
          return 1;
        },
      },
      'prop'
    )!;
    expect(customDescriptor.configurable).toBe(nativeDescriptor.configurable);
    expect(customDescriptor.enumerable).toBe(nativeDescriptor.enumerable);
    expect(customDescriptor.writable).toBe(nativeDescriptor.writable);
    expect(customDescriptor.value).toBe(nativeDescriptor.value);
    expect(customDescriptor.set).toBe(nativeDescriptor.set);
    expect(customDescriptor.get!()).toBe(nativeDescriptor.get!());
  });
});
