import {
  appendToPath,
  arrayToPath,
  ARRAY_MAX_LENGTH,
  isAbsolutePath,
  isSubpath,
  isValidPathId,
  isValidPathIndex,
  resolvePath,
  resolvePathToArray,
  splitLastId,
} from '../path-utils';

describe('Path utilities', () => {
  it('checks whether paths are absolute', () => {
    expect(isAbsolutePath('/')).toBe(true);
    expect(isAbsolutePath('/a/b')).toBe(true);
    expect(isAbsolutePath('a/b')).toBe(false);
    expect(isAbsolutePath('')).toBe(false);
  });

  it('appends identifiers to paths', () => {
    expect(appendToPath('/', 'a')).toBe('/a');
    expect(appendToPath('/a', 'b')).toBe('/a/b');
    expect(appendToPath('/a', 5)).toBe('/a/5');
    expect(appendToPath('/a/', 'b')).toBe('/a/b');
    expect(appendToPath('a', 'b')).toBe('a/b');
    expect(appendToPath('a', '.')).toBe('a/.');
    expect(appendToPath('a', '..')).toBe('a/..');
    expect(() => appendToPath('a', 'b/c')).toThrow();
  });

  it('resolves paths to arrays', () => {
    expect(resolvePathToArray('/a', 'b/c')).toEqual(['a', 'b', 'c']);
    expect(resolvePathToArray('/a', './b/../c')).toEqual(['a', 'c']);
    expect(resolvePathToArray('/a', '/b/')).toEqual(['b']);
    expect(resolvePathToArray('/a', 'b/../.././..')).toEqual([]);
    expect(resolvePathToArray('/a', 'b/../.././../d')).toEqual(['d']);
    expect(() => resolvePathToArray('a', '/b')).toThrow();
  });

  it('resolves paths', () => {
    expect(resolvePath('/a', 'b/c')).toBe('/a/b/c');
    expect(resolvePath('/a', './b/../c')).toBe('/a/c');
    expect(resolvePath('/a', '/b/')).toBe('/b');
    expect(resolvePath('/a', 'b/../.././..')).toBe('/');
    expect(resolvePath('/a', 'b/../.././../d')).toBe('/d');
    expect(() => resolvePath('a', '/b')).toThrow();
  });

  it('transforms an array path into a path', () => {
    expect(arrayToPath([])).toBe('/');
    expect(arrayToPath(['a', 'b'])).toBe('/a/b');
  });

  it('splits a path into its parent and last id', () => {
    expect(splitLastId('/a')).toEqual(['/', 'a']);
    expect(splitLastId('/a/b/c')).toEqual(['/a/b', 'c']);
    expect(() => splitLastId('/')).toThrow();
    expect(() => splitLastId('a')).toThrow();
  });

  it('checks whether a given string is a valid index of a path', () => {
    expect(isValidPathIndex(2)).toBe(true);
    expect(isValidPathIndex('2')).toBe(true);
    expect(isValidPathIndex('20')).toBe(true);
    expect(isValidPathIndex(ARRAY_MAX_LENGTH - 1)).toBe(true);
    expect(isValidPathIndex('x')).toBe(false);
    expect(isValidPathIndex('2x')).toBe(false);
    expect(isValidPathIndex('07')).toBe(false);
    expect(isValidPathIndex('-1')).toBe(false);
    expect(isValidPathIndex('4.5')).toBe(false);
    expect(isValidPathIndex('.')).toBe(false);
    expect(isValidPathIndex('..')).toBe(false);
    expect(isValidPathIndex('1/2')).toBe(false);
    expect(isValidPathIndex(ARRAY_MAX_LENGTH)).toBe(false);
    // Placeholders
    expect(isValidPathIndex('?', true)).toBe(true);
    expect(isValidPathIndex('?')).toBe(false);
  });

  it('checks whether a given identifier is a valid part of a path', () => {
    expect(isValidPathId(4)).toBe(true);
    expect(isValidPathId(ARRAY_MAX_LENGTH)).toBe(true);
    expect(isValidPathId('a-b')).toBe(true);
    expect(isValidPathId('.x')).toBe(true);
    expect(isValidPathId('..x')).toBe(true);
    expect(isValidPathId('a/b')).toBe(false);
    expect(isValidPathId('.')).toBe(false);
    expect(isValidPathId('..')).toBe(false);
  });

  it('checks that a path is a subpath of another', () => {
    expect(isSubpath('/', '/a')).toBe(true);
    expect(isSubpath('/a', '/a/b')).toBe(true);
    expect(isSubpath('/a', '/a')).toBe(false);
    expect(isSubpath('/a', '/b')).toBe(false);
    expect(isSubpath('/a', '/b/a')).toBe(false);
  });
});
