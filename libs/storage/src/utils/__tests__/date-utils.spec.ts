import { compareDates, isDate } from '../date-utils';

describe('Date utilities', () => {
  it('checks that a value is a date', () => {
    expect(isDate(new Date())).toBe(true);
    expect(isDate(new Date('invalid'))).toBe(true);
    expect(isDate(5)).toBe(false);
    expect(isDate({})).toBe(false);
  });

  it('compares two dates', () => {
    expect(compareDates(new Date(0), new Date(1))).toBeLessThan(0);
    expect(compareDates(new Date(0), new Date(0))).toBe(0);
    expect(compareDates(new Date(1), new Date(0))).toBeGreaterThan(0);
  });
});
