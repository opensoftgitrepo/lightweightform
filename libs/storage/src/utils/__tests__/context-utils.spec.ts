import { Storage } from '../../storage';
import { contextProperty, parentContextProperty } from '../context-utils';
import { numberSchema, recordSchema } from '../schema-creation';

// Example schema/storage
const numberSch = numberSchema({ x: 1, y: (ctx) => ctx.currentPath });
const storage = new Storage(recordSchema({ num: numberSch }));

describe('Context utilities', () => {
  it('calls a context property', () => {
    expect(contextProperty(storage, 'x', numberSch, '/num')).toBe(1);
    expect(contextProperty(storage, 'y', numberSch, '/num')).toBe('/num');
  });

  it('calls a parent context property', () => {
    expect(parentContextProperty(storage, 'x', numberSch, '/num')).toBe(1);
    expect(parentContextProperty(storage, 'y', numberSch, '/num')).toBe('/');
  });
});
