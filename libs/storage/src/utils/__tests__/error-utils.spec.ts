import {
  addValidationIssue,
  invalidSchemaTypeError,
  isValidationIssue,
  pathError,
} from '../error-utils';

describe('Error utilities', () => {
  it('determines whether a value is a validation issue', () => {
    expect(isValidationIssue(undefined)).toBe(false);
    expect(isValidationIssue(true)).toBe(false);
    expect(isValidationIssue({})).toBe(false);
    expect(isValidationIssue({ code: undefined })).toBe(false);
    expect(isValidationIssue({ code: 1 })).toBe(true);
    expect(isValidationIssue({ code: 'CODE' })).toBe(true);
  });

  it('adds validation errors', () => {
    const errors = {};
    addValidationIssue(errors, '/a', { code: 'A_ERROR' });
    addValidationIssue(errors, '/a', { code: 'A_ERROR_2' });
    expect(errors['/a'].length).toBe(2);
    addValidationIssue(errors, '/a/b', { code: 'A_B_ERROR' });
    expect(errors['/a/b'].length).toBe(1);
  });

  it('returns errors with a way of accessing the path and reason', () => {
    const error = pathError('/a/b', 'Some error');
    expect(error.path).toBe('/a/b');
    expect(error.reason).toBe('Some error');
  });

  it('returns errors related to invalid schema types', () => {
    const allowed = ['number', 'string'];
    const error = invalidSchemaTypeError('/a/b', allowed);
    expect(error.path).toBe('/a/b');
    expect(error.allowed).toBe(allowed);
  });
});
