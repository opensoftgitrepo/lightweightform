import { observable } from 'mobx';

import { Schema } from '../../schema-types';
import { ValidationIssue, ValidationIssuesMap } from '../error-utils';
import {
  booleanSchema,
  dateSchema,
  listSchema,
  mapSchema,
  numberSchema,
  recordSchema,
  stringSchema,
  tupleSchema,
} from '../schema-creation';
import {
  expectedType,
  INVALID_DATE_CODE,
  INVALID_INT_CODE,
  INVALID_MAP_KEY_CODE,
  INVALID_NUMBER_CODE,
  INVALID_RECORD_FIELD_CODE,
  INVALID_TYPE_CODE,
  MAX_SAFE_INTEGER,
  MISSING_RECORD_FIELD_CODE,
  SCHEMA_IS_COMPUTED_CODE,
  typeError,
  typeIsValid,
  TypeValidationOptions,
  UNSAFE_INT_CODE,
} from '../type-validation';

// Utility function used to check that a given value type is valid according to
// a schema
function expectValidType(
  value: any,
  schema: Schema,
  options?: TypeValidationOptions
) {
  expect(typeIsValid(value, schema, {}, '/', options)).toBe(true);
}

// Utility function used to check that a given value type is invalid according
// to a schema and that the first issue of a given path (defaults to `'/'`)
// partially matches a given issue
function expectInvalidType(
  value: any,
  schema: Schema,
  matchIssue: Partial<ValidationIssue>,
  issuePath: string = '/',
  options?: TypeValidationOptions
) {
  const issuesMap = {};
  expect(typeIsValid(value, schema, issuesMap, '/', options)).toBe(false);
  expect(issuesMap[issuePath][0]).toMatchObject(matchIssue);
}

describe('Type validation', () => {
  it('returns errors with a way of accessing all information', () => {
    const data = { someData: 'x' };
    const error = typeError('/a/b', {
      code: 'SOME_CODE',
      data,
    });
    expect(error.path).toBe('/a/b');
    expect(error.code).toBe('SOME_CODE');
    expect(error.data).toBe(data);
    // Data only added to the error when there is data
    expect('data' in typeError('/', { code: 'SOME_CODE' })).toBe(false);
  });

  it('returns correct expected types for schemas', () => {
    expect(expectedType(booleanSchema())).toBe('boolean');
    expect(expectedType(dateSchema())).toBe('Date');
    expect(expectedType(listSchema(dateSchema()))).toBe('Array<Date>');
    expect(expectedType(mapSchema(dateSchema()))).toBe('Map<string, Date>');
    expect(expectedType(dateSchema({ isNullable: true }))).toBe('Date | null');
    expect(expectedType(numberSchema())).toBe('number');
    expect(
      expectedType(
        recordSchema({
          num: numberSchema(),
          date: dateSchema(),
        })
      )
    ).toBe('{num: number, date: Date}');
    expect(expectedType(stringSchema())).toBe('string');
    expect(expectedType(tupleSchema([numberSchema(), dateSchema()]))).toBe(
      '[number, Date]'
    );
    expect(() => expectedType({ type: 'unknown' })).toThrow();
  });

  it('accepts `undefined` values unless the option is set', () => {
    const schema = numberSchema();
    expectValidType(undefined, schema);
    expectInvalidType(
      undefined,
      schema,
      { code: INVALID_TYPE_CODE, data: { expected: 'number' } },
      '/',
      { allowUndefined: false }
    );
  });

  it('accepts values of computed schemas unless the option is set', () => {
    const schema = numberSchema({ computedValue: () => 1 });
    expectValidType(undefined, schema);
    expectValidType(0, schema);
    expectValidType(undefined, schema, {
      allowValuesOfComputedSchemas: false,
    });
    // Invalid type
    expectInvalidType('0', schema, {
      code: INVALID_TYPE_CODE,
      data: { expected: 'number' },
    });
    // Disallowed value
    expectInvalidType(0, schema, { code: SCHEMA_IS_COMPUTED_CODE }, '/', {
      allowValuesOfComputedSchemas: false,
    });
  });

  it('does not allow `null` for non-nullable schemas', () => {
    const schema = booleanSchema();
    // Invalid type
    expectInvalidType(null, schema, {
      code: INVALID_TYPE_CODE,
      data: { expected: 'boolean' },
    });
  });

  it('type checks values against nullable schemas', () => {
    const schema = booleanSchema({ isNullable: true });
    expectValidType(null, schema);
    expectValidType(true, schema);
    // Invalid type
    expectInvalidType(0, schema, {
      code: INVALID_TYPE_CODE,
      data: { expected: 'boolean | null' },
    });
  });

  it('type checks values against boolean schemas', () => {
    const schema = booleanSchema();
    expectValidType(true, schema);
    expectValidType(false, schema);
    // Invalid type
    expectInvalidType(0, schema, {
      code: INVALID_TYPE_CODE,
      data: { expected: 'boolean' },
    });
  });

  it('type checks values against date schemas', () => {
    const schema = dateSchema();
    expectValidType(new Date(0), schema);
    // Invalid types
    expectInvalidType(0, schema, {
      code: INVALID_TYPE_CODE,
      data: { expected: 'Date' },
    });
    expectInvalidType('1970-01-01T00:00:00.000Z', schema, {
      code: INVALID_TYPE_CODE,
      data: { expected: 'Date' },
    });
    // Invalid date
    expectInvalidType(new Date('invalid'), schema, {
      code: INVALID_DATE_CODE,
    });
  });

  it('type checks values against list schemas', () => {
    const schema = listSchema(booleanSchema());
    expectValidType([], schema);
    expectValidType([true, false], schema);
    // MobX
    expectValidType(observable.array([]), schema);
    expectValidType(observable.array([true, false]), schema);
    // Invalid types
    expectInvalidType(
      [true, 0],
      schema,
      { code: INVALID_TYPE_CODE, data: { expected: 'boolean' } },
      '/1'
    );
    expectInvalidType({ 0: true, 1: false }, schema, {
      code: INVALID_TYPE_CODE,
      data: { expected: 'Array<boolean>' },
    });
    expectInvalidType(
      observable.array([true, 0]),
      schema,
      { code: INVALID_TYPE_CODE, data: { expected: 'boolean' } },
      '/1'
    );
  });

  it('type checks values against map schemas', () => {
    const schema = mapSchema(booleanSchema());
    expectValidType(new Map(), schema);
    expectValidType(
      new Map([
        ['a', true],
        ['b', false],
      ]),
      schema
    );
    // MobX
    expectValidType(observable.map({}), schema);
    expectValidType(observable.map({ a: true, b: false }), schema);
    // Invalid types
    expectInvalidType({ a: true, b: false }, schema, {
      code: INVALID_TYPE_CODE,
      data: { expected: 'Map<string, boolean>' },
    });
    expectInvalidType(
      new Map([
        ['a', true],
        ['b', 0 as any],
      ]),
      schema,
      { code: INVALID_TYPE_CODE, data: { expected: 'boolean' } },
      '/b'
    );
    expectInvalidType(
      observable.map({ a: true, b: 0 }),
      schema,
      { code: INVALID_TYPE_CODE, data: { expected: 'boolean' } },
      '/b'
    );
    // Invalid key
    expectInvalidType(new Map([['a/b', true]]), schema, {
      code: INVALID_MAP_KEY_CODE,
    });
    expectInvalidType(observable.map({ 'a/b': true }), schema, {
      code: INVALID_MAP_KEY_CODE,
    });
    expectInvalidType(observable.map({ '.': true }), schema, {
      code: INVALID_MAP_KEY_CODE,
    });
    expectInvalidType(observable.map({ '..': true }), schema, {
      code: INVALID_MAP_KEY_CODE,
    });
  });

  it('type checks values against number schemas', () => {
    const schema = numberSchema();
    const schemaInt = numberSchema({ isInteger: true });
    expectValidType(0, schema);
    expectValidType(10, schema);
    expectValidType(-10, schema);
    expectValidType(5.5, schema);
    expectValidType(1, schemaInt);
    expectValidType(-1, schemaInt);
    expectValidType(MAX_SAFE_INTEGER, schemaInt);
    expectValidType(-MAX_SAFE_INTEGER, schemaInt);
    // Invalid types
    expectInvalidType('0', schema, { data: { expected: 'number' } });
    expectInvalidType('0', schemaInt, { data: { expected: 'number' } });
    // Invalid values
    expectInvalidType(NaN, schema, {
      code: INVALID_NUMBER_CODE,
    });
    expectInvalidType(Infinity, schema, {
      code: INVALID_NUMBER_CODE,
    });
    expectInvalidType(-Infinity, schema, {
      code: INVALID_NUMBER_CODE,
    });
    // Invalid integer
    expectInvalidType(1.1, schemaInt, {
      code: INVALID_INT_CODE,
    });
    // Unsafe integer
    expectInvalidType(MAX_SAFE_INTEGER + 1, schemaInt, {
      code: UNSAFE_INT_CODE,
    });
    expectInvalidType(-MAX_SAFE_INTEGER - 1, schemaInt, {
      code: UNSAFE_INT_CODE,
    });
  });

  it('type checks values against record schemas', () => {
    const schema = recordSchema({ num: numberSchema() });
    const schemaWithComputed = recordSchema({
      num: numberSchema({ computedValue: () => 1 }),
    });
    expectValidType({ num: 2 }, schema);
    // Allow omitting by default
    expectValidType({}, schema);
    // MobX
    expectValidType(observable.object({ num: 2 }), schema);
    // Valid with extra fields
    expectValidType({ x: 'x' }, schema, { allowExtraRecordFields: true });
    // Computed fields may be omitted even without `allowUndefined`
    expectValidType({}, schemaWithComputed, { allowUndefined: false });
    // Invalid types
    expectInvalidType([], schema, { data: { field: 'num' } }, '/', {
      allowUndefined: false,
    });
    expectInvalidType(
      { num: false },
      schema,
      { data: { expected: 'number' } },
      '/num'
    );
    expectInvalidType(
      observable.object({ num: 'x' }),
      schema,
      { data: { expected: 'number' } },
      '/num'
    );
    // Invalid record field
    expectInvalidType({ x: 'x' }, schema, {
      code: INVALID_RECORD_FIELD_CODE,
      data: { field: 'x' },
    });
    // Missing record field
    expectInvalidType(
      {},
      schema,
      { code: MISSING_RECORD_FIELD_CODE, data: { field: 'num' } },
      '/',
      { allowUndefined: false }
    );
  });

  it('type checks values against string schemas', () => {
    const schema = stringSchema();
    expectValidType('', schema);
    expectValidType('abc', schema);
    // Invalid type
    expectInvalidType(0, schema, { data: { expected: 'string' } });
  });

  it('type checks values against tuple schemas', () => {
    const schema = tupleSchema([numberSchema()]);
    const schemaWithComputed = tupleSchema([
      numberSchema({ computedValue: () => 1 }),
    ]);
    expectValidType([], schema);
    expectValidType([2], schema);
    expectValidType([2], schemaWithComputed);
    // MobX
    expectValidType(observable.array([2]), schema);
    // Omitting computed value should be allowed even without `allowUndefined`
    expectValidType([], schemaWithComputed, { allowUndefined: false });
    // Invalid types
    expectInvalidType([2, 3], schema, { data: { expected: '[number]' } });
    expectInvalidType(['x'], schema, { data: { expected: 'number' } }, '/0');
    expectInvalidType(
      observable.array(['x']),
      schema,
      { data: { expected: 'number' } },
      '/0'
    );
    expectInvalidType([], schema, { data: { expected: 'number' } }, '/0', {
      allowUndefined: false,
    });
  });

  it('lists all type issues', () => {
    const schema = recordSchema({
      num: numberSchema(),
      numList: listSchema(numberSchema()),
      str: stringSchema(),
      strMap: mapSchema(stringSchema()),
      rec: recordSchema({
        bool: booleanSchema(),
      }),
    });
    let issuesMap: ValidationIssuesMap;
    expect(typeIsValid({ x: 'x' }, schema, (issuesMap = {}), '/')).toBe(false);
    expect(Object.keys(issuesMap).length).toBe(1);
    expect(issuesMap['/'][0]).toMatchObject({
      code: INVALID_RECORD_FIELD_CODE,
      data: { field: 'x' },
    });
    expect(
      typeIsValid(
        {
          num: 'x',
          numList: [1, true],
          z: 2,
          strMap: new Map([
            ['a', 3],
            ['b/', 'x' as any],
          ]),
          rec: { bool: 1 },
        },
        schema,
        (issuesMap = {}),
        '/',
        { listAllValidationIssues: true }
      )
    ).toBe(false);
    expect(Object.keys(issuesMap).length).toBe(6);
    expect(issuesMap['/num'][0]).toMatchObject({
      code: INVALID_TYPE_CODE,
      data: { expected: 'number' },
    });
    expect(issuesMap['/numList/1'][0]).toMatchObject({
      code: INVALID_TYPE_CODE,
      data: { expected: 'number' },
    });
    expect(issuesMap['/'][0]).toMatchObject({
      code: INVALID_RECORD_FIELD_CODE,
      data: { field: 'z' },
    });
    expect(issuesMap['/strMap/a'][0]).toMatchObject({
      code: INVALID_TYPE_CODE,
      data: { expected: 'string' },
    });
    expect(issuesMap['/strMap'][0]).toMatchObject({
      code: INVALID_MAP_KEY_CODE,
    });
    expect(issuesMap['/rec/bool'][0]).toMatchObject({
      code: INVALID_TYPE_CODE,
      data: { expected: 'boolean' },
    });
  });
});
