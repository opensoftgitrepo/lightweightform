import { autorun, observable, runInAction, spy } from 'mobx';
import { fromPromise, isPromiseBasedObservable } from 'mobx-utils';

import {
  fulfilledValue,
  invalidateablePromise,
  isPromise,
  promisify,
  resolveWhen,
} from '../promise-utils';

describe('Promise utilities', () => {
  it('checks that a value is a promise', () => {
    expect(isPromise(Promise.resolve(1))).toBe(true);
    expect(isPromise(fromPromise(Promise.resolve(1)))).toBe(true);
    expect(isPromise(invalidateablePromise(Promise.resolve(1)))).toBe(true);
    expect(isPromise({ then: () => 1 })).toBe(true);
    expect(isPromise(undefined)).toBe(false);
    expect(isPromise(null)).toBe(false);
    expect(isPromise({})).toBe(false);
  });

  it('"promisifies" values', () => {
    expect(isPromiseBasedObservable(promisify(1))).toBe(true);
    expect(isPromiseBasedObservable(promisify(Promise.resolve(1)))).toBe(true);
    // If the promise is already an observable promise, the returned promise
    // should be the same
    const obsPromise = fromPromise(Promise.resolve(1));
    expect(promisify(obsPromise)).toBe(obsPromise);
  });

  it('supports `resolveWhen` to resolve a promise when a predicate is `true`', async () => {
    const obs = observable.box(1);
    setTimeout(() => runInAction(() => obs.set(5)));
    await resolveWhen(() => obs.get() === 5);
    // Should resolve
  });

  it('returns the fulfilled value of promises', () => {
    expect(fulfilledValue(1)).toBe(1);
    expect(fulfilledValue(promisify(Promise.resolve(1)))).toBe(undefined);
    expect(fulfilledValue(promisify(Promise.resolve(1)), 'default')).toBe(
      'default'
    );
    expect(fulfilledValue(fromPromise.resolve(1))).toBe(1);
    expect(fulfilledValue(fromPromise.reject(1))).toBe(undefined);
    // `fulfilledValue` only gets the value of observable promises
    const promise = Promise.resolve(1);
    expect(fulfilledValue(promise)).toBe(promise);
  });

  it('allows computed values with promises to be invalidated', async () => {
    let count = 0;
    const obj = observable.object({
      get comp() {
        // Computed values should not change state, but for testing purposes
        // we're interested in how many times the computation is executed
        count++;
        return invalidateablePromise(Promise.resolve(1));
      },
    });
    // Let the computed value be used within a reaction so that it becomes
    // cached
    autorun(() => {
      obj.comp; // tslint:disable-line:no-unused-expression
    });
    await obj.comp;
    expect(count).toBe(1);
    await obj.comp;
    await obj.comp;
    expect(count).toBe(1);
    obj.comp.invalidate();
    await obj.comp;
    expect(count).toBe(2);
    await obj.comp;
    expect(count).toBe(2);
  });

  it('supports naming the invalidate action of invalidateable promises', () => {
    let called = false;
    const INVALIDATE_NAME = 'TEST_INVALIDATE';
    const promise = invalidateablePromise(Promise.resolve(1), INVALIDATE_NAME);
    const disposeSpy = spy((evt) => {
      if (evt.type === 'action' && evt.name === INVALIDATE_NAME) {
        called = true;
      }
    });
    promise.invalidate();
    expect(called).toBe(true);
    disposeSpy();
  });
});
