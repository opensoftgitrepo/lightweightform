import { observable } from 'mobx';

import { childSchema, childState, childValue } from '../child-utils';
import { ARRAY_MAX_LENGTH } from '../path-utils';
import {
  listSchema,
  mapSchema,
  numberSchema,
  recordSchema,
  tupleSchema,
} from '../schema-creation';

// Example schemas
const numberSch = numberSchema();
const listSch = listSchema(numberSch);
const mapSch = mapSchema(numberSch);
const recordSch = recordSchema({ num: numberSch });
const tupleSch = tupleSchema([numberSch]);

// Example state of a number value
const createParentSt = (childrenStates) => ({
  props: observable.map(),
  childrenStates,
});
const numberSt = { props: observable.map() };

describe('Child utilities', () => {
  it('returns the children of list schemas', () => {
    expect(childSchema(listSch, '/', 0)).toBe(numberSch);
    expect(childSchema(listSch, '/', '0')).toBe(numberSch);
    expect(childSchema(listSch, '/', '?')).toBe(numberSch);
    expect(childSchema(listSch, '/', 1)).toBe(numberSch);
    expect(() => childSchema(listSch, '/', 'length')).toThrow();
    expect(() => childSchema(listSch, '/', -1)).toThrow();
    expect(() => childSchema(listSch, '/', ARRAY_MAX_LENGTH)).toThrow();
  });

  it('returns the children of map schemas', () => {
    expect(childSchema(mapSch, '/', 'x')).toBe(numberSch);
    expect(childSchema(mapSch, '/', '?')).toBe(numberSch);
    expect(childSchema(mapSch, '/', 0)).toBe(numberSch);
    expect(() => childSchema(mapSch, '/', 'a/b')).toThrow();
  });

  it('returns the children of record schemas', () => {
    expect(childSchema(recordSch, '/', 'num')).toBe(numberSch);
    expect(() => childSchema(recordSch, '/', 'x')).toThrow();
    expect(() => childSchema(recordSch, '/', '?')).toThrow();
  });

  it('returns the children of tuple schemas', () => {
    expect(childSchema(tupleSch, '/', 0)).toBe(numberSch);
    expect(childSchema(tupleSch, '/', '0')).toBe(numberSch);
    expect(() => childSchema(tupleSch, '/', 1)).toThrow();
    expect(() => childSchema(tupleSch, '/', '?')).toThrow();
    expect(() => childSchema(tupleSch, '/', 'length')).toThrow();
  });

  it('fails to return children of non-collection schemas', () => {
    expect(() => childSchema(numberSch, '/', 'x')).toThrow();
  });

  // NOTE: A pre-condition to calling `childValue` is that the `parentPath` +
  // `id` must correspond to a valid schema, we do not test cases where this
  // isn't true

  it('fails to return the children of `null` values', () => {
    expect(() => childValue(listSch, '/', null, 0)).toThrow();
    expect(() => childValue(mapSch, '/', null, 'x')).toThrow();
    expect(() => childValue(recordSch, '/', null, 'num')).toThrow();
    expect(() => childValue(tupleSch, '/', null, 0)).toThrow();
  });

  it('returns the children of list values', () => {
    const parentVal = [1];
    expect(childValue(listSch, '/', parentVal, 0)).toBe(1);
    expect(childValue(listSch, '/', parentVal, '0')).toBe(1);
    expect(() => childValue(listSch, '/', parentVal, 1)).toThrow();
    expect(() => childValue(listSch, '/', parentVal, '1')).toThrow();
    expect(() => childValue(listSch, '/', parentVal, '?')).toThrow();
  });

  it('returns the children of map values', () => {
    const parentVal = new Map([['x', 1]]);
    expect(childValue(mapSch, '/', parentVal, 'x')).toBe(1);
    expect(() => childValue(mapSch, '/', parentVal, 'y')).toThrow();
    expect(() => childValue(mapSch, '/', parentVal, '?')).toThrow();
  });

  it('returns the children of record values', () => {
    const parentVal = { num: 1 };
    expect(childValue(recordSch, '/', parentVal, 'num')).toBe(1);
  });

  it('returns the children of tuple values', () => {
    const parentVal = [1];
    expect(childValue(tupleSch, '/', parentVal, 0)).toBe(1);
    expect(childValue(tupleSch, '/', parentVal, '0')).toBe(1);
  });

  // NOTE: A pre-condition to calling `childState` is that the `parentPath` +
  // `id` must correspond to a valid schema, we do not test cases where this
  // isn't true

  it('returns the parent state when the parent value is computed', () => {
    const parentSt = createParentSt(null);
    expect(childState(listSch, '/', parentSt, true, 0)).toBe(parentSt);
    expect(childState(mapSch, '/', parentSt, true, 'x')).toBe(parentSt);
    expect(childState(recordSch, '/', parentSt, true, 'num')).toBe(parentSt);
    expect(childState(tupleSch, '/', parentSt, true, 0)).toBe(parentSt);
  });

  it('fails to return the child states of `null` values', () => {
    const parentSt = createParentSt(null);
    expect(() => childState(listSch, '/', parentSt, false, 0)).toThrow();
    expect(() => childState(mapSch, '/', parentSt, false, 'x')).toThrow();
    expect(() => childState(recordSch, '/', parentSt, false, 'num')).toThrow();
    expect(() => childState(tupleSch, '/', parentSt, false, 0)).toThrow();
  });

  it('returns the child states of list values', () => {
    const parentSt = createParentSt([numberSt]);
    expect(childState(listSch, '/', parentSt, false, 0)).toBe(numberSt);
    expect(childState(listSch, '/', parentSt, false, '0')).toBe(numberSt);
    expect(() => childState(listSch, '/', parentSt, false, 1)).toThrow();
    expect(() => childState(listSch, '/', parentSt, false, '1')).toThrow();
    expect(() => childState(listSch, '/', parentSt, false, '?')).toThrow();
  });

  it('returns the child states of map values', () => {
    const parentSt = createParentSt(new Map([['x', numberSt]]));
    expect(childState(mapSch, '/', parentSt, false, 'x')).toBe(numberSt);
    expect(() => childState(mapSch, '/', parentSt, false, 'y')).toThrow();
    expect(() => childState(mapSch, '/', parentSt, false, '?')).toThrow();
  });

  it('returns the child states of record values', () => {
    const parentSt = createParentSt({ num: numberSt });
    expect(childState(recordSch, '/', parentSt, false, 'num')).toBe(numberSt);
  });

  it('returns the child states of tuple values', () => {
    const parentSt = createParentSt([numberSt]);
    expect(childState(tupleSch, '/', parentSt, false, 0)).toBe(numberSt);
    expect(childState(tupleSch, '/', parentSt, false, '0')).toBe(numberSt);
  });
});
