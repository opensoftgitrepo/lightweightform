import { Schema } from '../../schema-types';
import { Storage } from '../../storage';
import { ValidationIssue } from '../error-utils';
import {
  booleanSchema,
  // dateSchema,
  // listSchema,
  // mapSchema,
  // numberSchema,
  // recordSchema,
  // stringSchema,
  // tupleSchema,
} from '../schema-creation';
import {
  // DATE_OUT_OF_BOUNDS_CODE,
  // DISALLOWED_VALUE_CODE,
  IS_REQUIRED_CODE,
  // LENGTH_OUT_OF_BOUNDS_CODE,
  // NUMBER_OUT_OF_BOUNDS_CODE,
  // SIZE_OUT_OF_BOUNDS_CODE,
  validateValue,
} from '../value-validation';

// Utility function used to check that a given value is valid according to a
// schema
async function expectValidValue(schema: Schema, value: any) {
  const storage = new Storage(schema);
  storage.set('/', value);
  const issues = await validateValue(
    schema,
    '/',
    storage.get(),
    storage.getState()
  );
  expect(Object.keys(issues).length).toBe(0);
}

// Utility function used to check that a given value is invalid according to a
// schema and that the first error of a given path (defaults to `'/'`) partially
// matches a given error
async function expectInvalidValue(
  schema: Schema,
  value: any,
  matchIssue: Partial<ValidationIssue>,
  errorPath: string = '/'
) {
  const storage = new Storage(schema);
  storage.set('/', value);
  const issues = await validateValue(
    schema,
    '/',
    storage.get(),
    storage.getState()
  );
  expect(issues[errorPath][0]).toMatchObject(matchIssue);
}

describe('Value validation', () => {
  it('validates nullable schemas with `isRequired`', async () => {
    const nullableSchema = booleanSchema({
      isNullable: true,
      isRequired: true,
    });
    await expectValidValue(nullableSchema, true);
    // Invalid value
    await expectInvalidValue(nullableSchema, null, { code: IS_REQUIRED_CODE });
  });
});
