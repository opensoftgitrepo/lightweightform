import {
  BooleanSchema,
  CollectionSchema,
  ComplexSchema,
  DateSchema,
  ListSchema,
  MapSchema,
  NumberSchema,
  RecordSchema,
  Schema,
  SimpleSchema,
  StringSchema,
  TableSchema,
  TupleSchema,
} from '../schema-types';

import { ParentContextFunction } from './context-utils';

/**
 * Returns whether a given schema is a boolean schema.
 * @param schema Schema with type to check.
 * @returns `true` when the provided schema is a boolean schema.
 */
export function isBooleanSchema(schema: Schema): schema is BooleanSchema {
  return schema.type === 'boolean';
}

/**
 * Returns whether a given schema is a date schema.
 * @param schema Schema with type to check.
 * @returns `true` when the provided schema is a date schema.
 */
export function isDateSchema(schema: Schema): schema is DateSchema {
  return schema.type === 'date';
}

/**
 * Returns whether a given schema is a list schema.
 * @param schema Schema with type to check.
 * @returns `true` when the provided schema is a list schema.
 */
export function isListSchema(schema: Schema): schema is ListSchema {
  return schema.type === 'list';
}

/**
 * Returns whether a given schema is a map schema.
 * @param schema Schema with type to check.
 * @returns `true` when the provided schema is a map schema.
 */
export function isMapSchema(schema: Schema): schema is MapSchema {
  return schema.type === 'map';
}

/**
 * Returns whether a given schema is a number schema.
 * @param schema Schema with type to check.
 * @returns `true` when the provided schema is a number schema.
 */
export function isNumberSchema(schema: Schema): schema is NumberSchema {
  return schema.type === 'number';
}

/**
 * Returns whether a given schema is a record schema.
 * @param schema Schema with type to check.
 * @returns `true` when the provided schema is a record schema.
 */
export function isRecordSchema(schema: Schema): schema is RecordSchema {
  return schema.type === 'record';
}

/**
 * Returns whether a given schema is a string schema.
 * @param schema Schema with type to check.
 * @returns `true` when the provided schema is a string schema.
 */
export function isStringSchema(schema: Schema): schema is StringSchema {
  return schema.type === 'string';
}

/**
 * Returns whether a given schema is a table schema.
 * @param schema Schema with type to check.
 * @returns `true` when the provided schema is a table schema.
 */
export function isTableSchema(schema: Schema): schema is TableSchema {
  return schema.type === 'table';
}

/**
 * Returns whether a given schema is a tuple schema.
 * @param schema Schema with type to check.
 * @returns `true` when the provided schema is a tuple schema.
 */
export function isTupleSchema(schema: Schema): schema is TupleSchema {
  return schema.type === 'tuple';
}

/**
 * Returns whether a given schema is one of the simple schemas (boolean, date,
 * number, or string).
 * @param schema Schema with type to check.
 * @returns `true` when the provided schema is one of the simple schemas.
 */
export function isSimpleSchema(schema: Schema): schema is SimpleSchema {
  return (
    isBooleanSchema(schema) ||
    isDateSchema(schema) ||
    isNumberSchema(schema) ||
    isStringSchema(schema)
  );
}

/**
 * Returns whether a given schema is one of the complex schemas (list, map,
 * record, table, or tuple).
 * @param schema Schema with type to check.
 * @returns `true` when the provided schema is one of the complex schemas.
 */
export function isComplexSchema(schema: Schema): schema is ComplexSchema {
  return (
    isListSchema(schema) ||
    isMapSchema(schema) ||
    isRecordSchema(schema) ||
    isTableSchema(schema) ||
    isTupleSchema(schema)
  );
}

/**
 * Returns whether a given schema is one of the collection schemas (lists, map,
 * or table).
 * @param schema Schema with type to check.
 * @returns `true` when the provided schema is one of the collection schemas.
 */
export function isCollectionSchema(schema: Schema): schema is CollectionSchema {
  return isListSchema(schema) || isMapSchema(schema) || isTableSchema(schema);
}

/**
 * Whether a given schema represents a computed value. Note that this function
 * does **not** know if the provided schema is computed because an ancestor is.
 * @param schema Schema to check if it represents a computed value.
 * @returns Whether the schema represents a computed value.
 */
export function isComputedSchema<T = any>(
  schema: Schema<T>
): schema is Schema<T> & { computedValue: ParentContextFunction<T> } {
  return schema.computedValue !== undefined;
}

/**
 * Whether a given schema is client-only (if the schema and values that it
 * represents are hidden from JSON schema/value representations). Note that this
 * function does **not** know if the provided schema is client-only because an
 * ancestor is.
 * @param schema Schema to check if is client-only.
 * @returns Whether the schema is client-only.
 */
export function isClientOnlySchema(schema: Schema): boolean {
  return schema.isClientOnly === undefined
    ? isComputedSchema(schema)
    : schema.isClientOnly;
}

/**
 * Whether a given schema allows `null` as a valid value type.
 * @param schema Schema to check if is nullable.
 * @returns Whether `null` is a valid value type for values of given schema.
 */
export function isNullableSchema(schema: Schema): boolean {
  return !!schema.isNullable;
}
