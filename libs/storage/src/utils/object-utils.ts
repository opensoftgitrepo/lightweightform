/**
 * Dynamically defines an enumerable and configurable getter in an object (same
 * as if the `get() {...}` syntax was used when the object was first created).
 * @param obj Object to define the getter in.
 * @param name Name of the getter.
 * @param getter Function to work as the getter.
 * @returns Passed object with the getter defined in it.
 */
export function defineGetter(obj: any, name: string, getter: () => any): any {
  Object.defineProperty(obj, name, {
    get: getter,
    enumerable: true,
    configurable: true,
  });
  return obj;
}
