/**
 * Type of the identifier used to access a schema from its parent schema.
 */
export type Identifier = number | string;

/**
 * String used as a placeholder when the actual identifier of a certain path is
 * unknown or irrelevant. For instance, if the user wants to know the schema of
 * the elements of a list schema, since all elements of a list have the same
 * schema it doesn't matter which index is used to access said schema. In such a
 * case, the placeholder may be used. Example: `storage.getSchema('/list/?/a')`
 * should return the schema of the field `'a'` of the list elements' schema.
 */
export const PATH_ID_PLACEHOLDER = '?';

/**
 * Maximum length of an array according to the ECMA-262 5th Edition spec. This
 * means that numbers >= than this are invalid indices.
 */
export const ARRAY_MAX_LENGTH = 2 ** 32 - 1;

/**
 * Internal function which normalizes an array of path parts which represent an
 * absolute path. This array may contain parts such as `'.'` or `'..'`. Example:
 * ```typescript
 * normalizeArray(['a', 'b', '..']); // Returns `['a']`
 * normalizeArray(['..', 'a']); // Returns `['a']`
 * normalizeArray(['', 'a', '.', 'b']); // Returns `['a', 'b']`
 * ```
 * @returns Array with the parts of the normalised path.
 */
function normalizeArray(pathArray: string[]): string[] {
  const resolved: string[] = [];
  for (const part of pathArray) {
    if (part === '..') {
      if (resolved.length > 0) {
        resolved.pop();
      }
    } else if (part !== '' && part !== '.') {
      resolved.push(part);
    }
  }
  return resolved;
}

/**
 * Whether a given posix-like path is absolute (if it starts with `'/'`).
 * Example:
 * ```typescript
 * pathIsAbsolute('/a/b'); // Returns `true`
 * pathIsAbsolute('a/b'); // Returns `false`
 * ```
 * @param path Path to check if it is absolute.
 * @returns `true` when the given path is absolute.
 */
export function isAbsolutePath(path: string): boolean {
  return path[0] === '/';
}

/**
 * Appends a given identifier to a provided path.
 * @param path Base path on which to append identifier.
 * @param id Identifier to append to base path.
 * @returns Provided path with the appended identifier.
 */
export function appendToPath(path: string, id: Identifier): string {
  if (
    process.env.NODE_ENV !== 'production' &&
    id.toString().indexOf('/') !== -1
  ) {
    throw new Error(`Invalid path part '${id}': Cannot contain '/'`);
  }
  return path + (path.slice(-1) === '/' ? '' : '/') + id;
}

/**
 * Function which resolves a given path against a "current working path" that be
 * absolute and returns the array of parts of the resolved path. This function
 * throws when the `currentPath` is not absolute. Example:
 * ```typescript
 * resolvePathToArray('/a', 'b/c'); // Returns `['a', 'b', 'c']`
 * resolvePathToArray('/a', './b/../c'); // Returns `['a', 'c']`
 * resolvePathToArray('/a', '/b'); // Returns `['b']`
 * resolvePathToArray('a', '/b'); // Throws because current path is not absolute
 * ```
 */
export function resolvePathToArray(
  currentPath: string,
  path: string
): string[] {
  if (process.env.NODE_ENV !== 'production' && !isAbsolutePath(currentPath)) {
    throw new Error('First argument must be an absolute path');
  }
  const fullPath = isAbsolutePath(path) ? path : currentPath + '/' + path;
  return normalizeArray(fullPath.split('/'));
}

/**
 * Function which resolves a given path against a "current working path" which
 * must be absolute. This function throws when the `currentPath` is not
 * absolute. Example:
 * ```typescript
 * resolvePath('/a', 'b/c'); // Returns `'/a/b/c'`
 * resolvePath('/a', './b/../c'); // Returns `'/a/c'`
 * resolvePath('/a', '/b'); // Returns `'/b'`
 * resolvePath('a', '/b'); // Throws because current path is not absolute
 * ```
 */
export function resolvePath(currentPath: string, path: string): string {
  return arrayToPath(resolvePathToArray(currentPath, path));
}

/**
 * Returns an absolute path given an array with each part of the path. Example:
 * ```typescript
 * arrayToPath([]); // Returns `'/'`
 * arrayToPath(['a', 'b', 'c']); // Returns `'/a/b/c'`
 * ```
 * @param pathArray Array with each part of a path.
 * @return Absolute path that consists of joining all array parts.
 */
export function arrayToPath(pathArray: string[]): string {
  return '/' + pathArray.join('/');
}

/**
 * Splits a given (non-root) absolute path into a tuple consisting of the parent
 * path of the provided path as the first element and the last id of the
 * provided path as the second element. Example:
 * ```typescript
 * splitLastId('/a'); // Returns `['/', 'a']`
 * splitLastId('/a/b/c'); // Returns `['/a/b', 'c']`
 * ```
 * @param path Path to split into parent path and last id. Must be a non-root
 * absolute path.
 * @returns Tuple consisting of the parent path and last id of the provided
 * path.
 */
export function splitLastId(path: string): [string, string | null] {
  if (
    process.env.NODE_ENV !== 'production' &&
    (!isAbsolutePath(path) || path === '/')
  ) {
    throw new Error('Argument must be an absolute path that is not the root');
  }
  const lastSlash = path.lastIndexOf('/');
  return [path.slice(0, lastSlash) || '/', path.slice(lastSlash + 1)];
}

/**
 * Whether a given index found in a path (e.g. for accessing a list element) is
 * valid. For example: `'2'` in the path `'/list/2/a'` is a valid index, but
 * `'07'` in `'/list/07/b'` is not. There is the option of accepting the
 * placeholder as valid. Example:
 * ```typescript
 * isValidPathIndex(2); // Returns `true`
 * isValidPathIndex('2'); // Returns `true`
 * isValidPathIndex('07'); // Returns `false`
 * isValidPathIndex('?'); // Returns `false`
 * isValidPathIndex('?', true); // Returns `true`
 * ```
 * @param id Index as found in the path.
 * @param acceptPlaceholder Whether the id placeholder is accepted as a valid
 * index.
 * @returns Whether the given string index is valid.
 */
export function isValidPathIndex(
  id: Identifier,
  acceptPlaceholder = false
): boolean {
  return (
    (acceptPlaceholder && id === PATH_ID_PLACEHOLDER) ||
    (/^(0|[1-9]\d*)$/.test(id.toString()) && +id < ARRAY_MAX_LENGTH)
  );
}

/**
 * Whether a given identifier can be used as part of a path. No identifiers may
 * contain a forward slash (since it conflicts with the path separator). They
 * can't also be `'.'` or `'..'`. Example:
 * ```typescript
 * isValidPathId(4); // Returns `true`
 * isValidPathId('a-b'); // Returns `true`
 * isValidPathId('..x'); // Returns `true`
 * isValidPathId('a/b'); // Returns `false`
 * isValidPathId('.'); // Returns `false`
 * isValidPathId('..'); // Returns `false`
 * ```
 * @param id Identifier to check if it is valid.
 * @returns Whether the provided identifier is a valid part of a path.
 */
export function isValidPathId(id: Identifier): boolean {
  return id !== '.' && id !== '..' && id.toString().indexOf('/') === -1;
}

/**
 * Whether `path2` is a subpath of `path1`. Example:
 * ```typescript
 * isSubpath('/', '/a'); // Returns `true`
 * isSubpath('/a', '/a/b'); // Returns `true`
 * isSubpath('/a', '/a'); // Returns `false`
 * isSubpath('/a', '/b'); // Returns `false`
 * ```
 * @param path1 First path.
 * @param path2 Second path.
 * @returns Whether `path2` is a child of `path1`.
 */
export function isSubpath(path1: string, path2: string): boolean {
  return (
    path1.length < path2.length &&
    (path1 === '/' || path2.indexOf(path1 + '/') === 0)
  );
}
