import { ValidationIssuesMap } from './error-utils';
import { isSubpath } from './path-utils';

/**
 * Given the map with all known issues, retrieves the issues relevant to a given
 * path.
 * @param path Path for which to retrieve issues.
 * @param issuesMap All known issues.
 * @returns Issues relevant to the provided path.
 */
export function getRelevantIssuesFromMap(
  path: string,
  issuesMap: ValidationIssuesMap
): ValidationIssuesMap {
  return Object.keys(issuesMap)
    .filter((issuesPath) => path === issuesPath || isSubpath(path, issuesPath))
    .reduce((map, issuesPath) => {
      map[issuesPath] = issuesMap[issuesPath];
      return map;
    }, {});
}
