import { isObservableMap } from 'mobx';

import { Schema } from '../schema-types';

import { isArrayLike } from './array-utils';
import { isDate } from './date-utils';
import {
  addValidationIssue,
  pathError,
  ValidationIssue,
  ValidationIssuesMap,
} from './error-utils';
import { appendToPath, isValidPathId } from './path-utils';
import {
  isBooleanSchema,
  isComputedSchema,
  isDateSchema,
  isListSchema,
  isMapSchema,
  isNullableSchema,
  isNumberSchema,
  isRecordSchema,
  isStringSchema,
  isTableSchema,
  isTupleSchema,
} from './schema-checker';
import { $lfRowId } from './table-utils';

/**
 * Maximum integer represented in JavaScript without losing precision.
 * `Number.MAX_SAFE_INTEGER` in modern JavaScript engines. Used by `typeIsValid`
 * to validate number values that must be integers.
 */
export const MAX_SAFE_INTEGER = 9007199254740991;

/**
 * Issue code representing that the value's type is invalid. E.g. when type
 * checking a number against a string schema. Validation issues with this `code`
 * also contain an `expected` data property containing the value's expected type
 * in TypeScript notation.
 */
export const INVALID_TYPE_CODE = 'LF_INVALID_TYPE';
/**
 * Issue code representing that the value is invalid because it is not
 * `undefined` and it is being type checked against a computed schema. This is a
 * validation issue only when the option `allowValuesOfComputedSchemas` is set
 * to `false`.
 */
export const SCHEMA_IS_COMPUTED_CODE = 'LF_SCHEMA_IS_COMPUTED';
/**
 * Issue code representing that the date value is invalid because it is
 * literally an `Invalid Date`. E.g. the date object that results from
 * `new Date('x')`.
 */
export const INVALID_DATE_CODE = 'LF_INVALID_DATE';
/**
 * Issue code representing that the map value is invalid because it contains an
 * invalid key. Keys may not contain a forward slash (`'/'`) or be `'.'`/`'..'`
 * due to how they are used in paths. Validation issues with this `code` also
 * contain a `key` data property containing the invalid key.
 */
export const INVALID_MAP_KEY_CODE = 'LF_INVALID_MAP_KEY';
/**
 * Issue code representing that the value is either `NaN` or (positive or
 * negative) `Infinity`: which are not supported number values (since they
 * cannot be properly serialised into JSON). Validation issues containing this
 * `code` also contain a `value` data property with the unsupported value.
 */
export const INVALID_NUMBER_CODE = 'LF_INVALID_NUMBER';
/**
 * Issue code representing that the number value is not a valid integer (which
 * is mandatory given the schema it is being type checked against).
 */
export const INVALID_INT_CODE = 'LF_INVALID_INT';
/**
 * Issue code representing that the number value is an integer that is not safe
 * (it is not between `Number.MIN_SAFE_INTEGER` and `Number.MAX_SAFE_INTEGER`).
 */
export const UNSAFE_INT_CODE = 'LF_UNSAFE_INT';
/**
 * Issue code representing that the record value contains a field that is not
 * part of its schema. This is a validation issue only when
 * `allowExtraRecordFields` is set to `false`. Validation issues containing this
 * `code` also contain a `field` data property with the invalid field.
 */
export const INVALID_RECORD_FIELD_CODE = 'LF_INVALID_RECORD_FIELD';
/**
 * Issue code representing that the record value is missing a field represented
 * by a non-computed schema. This is a validation issue only when
 * `allowUndefined` is set to `false`. Validation issues containing this `code`
 * also contain a `field` data property with the missing field.
 */
export const MISSING_RECORD_FIELD_CODE = 'LF_MISSING_RECORD_FIELD';

/**
 * Options used to validate the type of a value.
 */
export interface TypeValidationOptions {
  /**
   * Allows the value to be `undefined` or have `undefined` inner values. This
   * is useful when the value in question is going to be merged with the default
   * value anyway, so `undefined` values will be overwritten. It also means that
   * record values may omit some of their fields. Defaults to `true`.
   */
  allowUndefined?: boolean;
  /**
   * Whether the type checking should allow values represented by schemas that
   * are computed to not be `undefined`. Defaults to `true`. When set to `false`
   * the value's type is still type checked against its respective schema.
   */
  allowValuesOfComputedSchemas?: boolean;
  /**
   * Allows passed records to define extra fields (since, for instance, they
   * will end up being ignored). Defaults to `false`.
   */
  allowExtraRecordFields?: boolean;
  /**
   * Whether the type validation function should list all issues found in the
   * provided value or only the first one. Defaults to `false`, meaning that, by
   * default, the function returns as soon as an invalid type is found.
   */
  listAllValidationIssues?: boolean;
}

/**
 * Function that checks whether a given value has a valid type in respect to a
 * given schema.
 * @param value Value to check whether type is valid.
 * @param schema Schema against which to check value's type.
 * @param issuesMap Object provided by the callee that is filled by the function
 * with information depicting why a value has an invalid type. An empty object
 * should be provided unless the reason for an invalid type is not needed. This
 * object will be filled mapping invalid values to a list of their validation
 * issues.
 * @param path Absolute path of the value.
 * @param options Options used to validate the type of the value.
 * @param parentIsTable Whether the parent value is a table.
 * @returns Whether the value has a valid type according to the schema.
 */
export function typeIsValid(
  value: any,
  schema: Schema,
  issuesMap: ValidationIssuesMap = {},
  path: string,
  options: TypeValidationOptions = {},
  parentIsTable: boolean = false
): boolean {
  const {
    allowUndefined = true,
    allowValuesOfComputedSchemas = true,
    allowExtraRecordFields = false,
    listAllValidationIssues = false,
  } = options;
  // Helper function that works similarly to `Array.every` but that, depending
  // on `listAllValidationIssues` stops or continues after finding the first
  // `false`
  function customEvery(array, cb) {
    return listAllValidationIssues
      ? array.reduce((res, elem, i, arr) => cb(elem, i, arr) && res, true)
      : array.every(cb);
  }
  // Helper function used to call `typeIsValid` on a child given its value,
  // schema, and id
  function childIsValid(val, sch, id) {
    return typeIsValid(
      val,
      sch,
      issuesMap,
      appendToPath(path, id),
      options,
      isTableSchema(schema)
    );
  }

  const type = typeof value;
  // Allow `undefined` values
  if (allowUndefined && value === undefined) {
    return true;
  }
  // Validate nullable schemas
  if (isNullableSchema(schema) && value === null) {
    return true;
  }
  // Validate values represented by computed schemas
  if (isComputedSchema(schema)) {
    if (value === undefined) {
      return true;
    }
    if (!allowValuesOfComputedSchemas) {
      addValidationIssue(issuesMap, path, {
        code: SCHEMA_IS_COMPUTED_CODE,
        message: 'Schema is computed, value must be `undefined`',
      });
      return false;
    }
  }
  // Validate boolean
  if (isBooleanSchema(schema) && type === 'boolean') {
    return true;
  }
  // Validate date
  if (isDateSchema(schema) && isDate(value)) {
    // Refuse `Invalid Date` instances
    if (isNaN(+value)) {
      addValidationIssue(issuesMap, path, {
        code: INVALID_DATE_CODE,
        message: 'Date is invalid',
      });
      return false;
    }
    return true;
  }
  // Validate list/table
  if ((isListSchema(schema) || isTableSchema(schema)) && isArrayLike(value)) {
    const childrenSchema = isListSchema(schema)
      ? schema.elementsSchema
      : schema.rowsSchema;
    return customEvery(value, (el, i) => childIsValid(el, childrenSchema, i));
  }
  // Validate map
  if (isMapSchema(schema) && (value instanceof Map || isObservableMap(value))) {
    return customEvery(Array.from(value.entries()), ([key, val]) => {
      if (!isValidPathId(key)) {
        addValidationIssue(issuesMap, path, {
          code: INVALID_MAP_KEY_CODE,
          message: ({ key: k }) => `Invalid map key: "${k}"`,
          data: { key },
        });
        return false;
      }
      return childIsValid(val, schema.valuesSchema, key);
    });
  }
  // Validate number
  if (isNumberSchema(schema) && type === 'number') {
    // Refuse `NaN` and `Infinity` numbers
    if (!isFinite(value)) {
      addValidationIssue(issuesMap, path, {
        code: INVALID_NUMBER_CODE,
        message: ({ value: val }) => `Invalid number value: \`${val}\``,
        data: { value },
      });
      return false;
    }
    // Refuse non-integer numbers when schema requires an integer
    if (schema.isInteger && Math.floor(value) !== value) {
      addValidationIssue(issuesMap, path, {
        code: INVALID_INT_CODE,
        message: 'Provided number is not an integer',
      });
      return false;
    }
    // Refuse non-safe integers when schema requires an integer
    if (schema.isInteger && Math.abs(value) > MAX_SAFE_INTEGER) {
      addValidationIssue(issuesMap, path, {
        code: UNSAFE_INT_CODE,
        message:
          'Provided integer is not safe (must be between ' +
          `\`${-MAX_SAFE_INTEGER}\` and \`${MAX_SAFE_INTEGER}\`)`,
      });
      return false;
    }
    return true;
  }
  // Validate record
  if (isRecordSchema(schema) && typeof value === 'object' && value !== null) {
    return (
      // Validate the fields existent in the value
      customEvery(Object.keys(value), (field) => {
        if (!(field in schema.fieldsSchemas)) {
          // Allow table rows to have a row id (even if it may end up ignored)
          if (allowExtraRecordFields || (parentIsTable && field === $lfRowId)) {
            return true;
          }
          addValidationIssue(issuesMap, path, {
            code: INVALID_RECORD_FIELD_CODE,
            message: ({ field: f }) => `Invalid record field: "${f}"`,
            data: { field },
          });
          return false;
        }
        return childIsValid(value[field], schema.fieldsSchemas[field], field);
      }) &&
      // When missing fields aren't allowed, report them
      (allowUndefined ||
        customEvery(Object.keys(schema.fieldsSchemas), (field) => {
          const fieldSchema = schema.fieldsSchemas[field];
          // Fields with computed schemas never need to exist
          if (!isComputedSchema(fieldSchema) && !(field in value)) {
            addValidationIssue(issuesMap, path, {
              code: MISSING_RECORD_FIELD_CODE,
              message: ({ field: f }) => `Missing record field: "${f}"`,
              data: { field },
            });
            return false;
          }
          return true;
        }))
    );
  }
  // Validate string
  if (isStringSchema(schema) && type === 'string') {
    return true;
  }
  // Validate tuple
  if (
    isTupleSchema(schema) &&
    isArrayLike(value) &&
    value.length <= schema.elementsSchemas.length
  ) {
    return customEvery(schema.elementsSchemas, (elementSchema, i) =>
      childIsValid(value[i], elementSchema, i)
    );
  }
  // Type is invalid
  addValidationIssue(issuesMap, path, {
    code: INVALID_TYPE_CODE,
    message: ({ expected }) => `Expected: \`${expected}\``,
    data: { expected: expectedType(schema) },
  });
  return false;
}

/**
 * Function that returns a string depicting the type of value expected by a
 * given schema. The returned type string follows TypeScript's syntax.
 * @param schema Schema to check expected type for.
 * @returns String containing the type of value expected using TypeScript
 * notation.
 */
export function expectedType(schema: Schema): string {
  if (isNullableSchema(schema)) {
    // Get type of schema if it wasn't nullable
    return expectedType({ ...schema, isNullable: false }) + ' | null';
  }
  if (isBooleanSchema(schema)) {
    return 'boolean';
  }
  if (isDateSchema(schema)) {
    return 'Date';
  }
  if (isListSchema(schema) || isTableSchema(schema)) {
    return `Array<${expectedType(
      isListSchema(schema) ? schema.elementsSchema : schema.rowsSchema
    )}>`;
  }
  if (isMapSchema(schema)) {
    return `Map<string, ${expectedType(schema.valuesSchema)}>`;
  }
  if (isNumberSchema(schema)) {
    return 'number';
  }
  if (isRecordSchema(schema)) {
    return `{${Object.keys(schema.fieldsSchemas)
      .map((key) => `${key}: ${expectedType(schema.fieldsSchemas[key])}`)
      .join(', ')}}`;
  }
  if (isStringSchema(schema)) {
    return 'string';
  }
  if (isTupleSchema(schema)) {
    return `[${schema.elementsSchemas.map(expectedType).join(', ')}]`;
  }
  throw new Error('Invalid schema');
}

/**
 * Throws a type error given an "invalid type" object (as provided by the
 * `typeIsValid` function). The error message will contain the path information
 * and depending on the provided object either the expected value type or the
 * reason why the type is invalid.
 * @param path Path where the error occurred.
 * @param invalidType Invalid type object as obtained from `typeIsValid`
 */
export function typeError(
  path,
  { code, message, data }: ValidationIssue
): TypeError & {
  path: string;
  code: string;
  data?: Record<string, any>;
} {
  const msg = typeof message === 'function' ? message(data) : message;
  const error: any = pathError(path, `Invalid type: ${msg}`, TypeError);
  error.code = code;
  if (data) {
    error.data = data;
  }
  return error;
}

/**
 * Function that checks whether a given value has a valid type in respect to a
 * given schema and throws an error if it doesn't.
 * @param value Value to check whether type is valid.
 * @param schema Schema against which to check value's type.
 * @param path Absolute path of the value.
 * @param options Options used to validate the type of the value.
 */
export function validateType(
  value: any,
  schema: Schema,
  path: string,
  options: TypeValidationOptions = {}
): void {
  const issuesMap = {};
  if (!typeIsValid(value, schema, issuesMap, path, options)) {
    const firstInvalidPath = Object.keys(issuesMap)[0];
    throw typeError(firstInvalidPath, issuesMap[firstInvalidPath][0]);
  }
}
