import {
  $mobx,
  comparer,
  computed,
  extendObservable,
  IObservableValue,
  observable,
} from 'mobx';

import { Schema } from '../schema-types';
import { Storage } from '../storage';

import { parentContextProperty } from './context-utils';
import { pathError } from './error-utils';
import { defineGetter } from './object-utils';
import { appendToPath, Identifier } from './path-utils';
import {
  isBooleanSchema,
  isComplexSchema,
  isComputedSchema,
  isDateSchema,
  isListSchema,
  isMapSchema,
  isNullableSchema,
  isNumberSchema,
  isRecordSchema,
  isSimpleSchema,
  isStringSchema,
  isTableSchema,
  isTupleSchema,
} from './schema-checker';
import {
  initializeChildrenStates,
  initializeState,
  ValueState,
} from './state-creation';
import {
  $lfRowId,
  $lfTable,
  getRow,
  getRowId,
  getRowState,
  hasRow,
  newTable,
  nextRowId,
  setRowId,
} from './table-utils';
import { validateType } from './type-validation';

/**
 * Options used for setting a value. A value to be set is always merged against
 * the one in the storage; however, a few options can be used to specify the
 * merging behaviour.
 */
export interface MergeValueOptions {
  /**
   * How to handle the merging of collections (lists, maps, and tables). The
   * `'replace'` option replaces the existing collection with the collection
   * being set, whilst still merging elements with the same id. `'merge'` merges
   * the collection being set with the existing collection, without removing any
   * elements from the existing collection. `'append'` behaves similarly to
   * `'merge'` but, for lists and tables, elements being set are appended to the
   * existing collection (no elements are merged). Note that, for tables, rows
   * are only ever merged if they have the same row id and, unless in
   * `'replace'` mode, they will not be reordered. Defaults to `'replace'`.
   */
  handleCollections?: 'replace' | 'merge' | 'append';
  /**
   * How to handle `undefined` values (including records with missing fields).
   * The `'reset'` option resets the existing value to its initial value.
   * `'ignore'` ignores `undefined` values by not touching the existing value.
   * Defaults to `'ignore'`.
   */
  handleUndefined?: 'ignore' | 'reset';
}

/**
 * Merges a given value with the value in the storage. If there is no value in
 * the storage, it is created.
 * @param storage Storage instance where values are to be merged.
 * @param schema Schema of the values to merge.
 * @param path Path of the values to merge.
 * @param id Identifier of the values to merge.
 * @param currentValue Current value present in the storage (possibly
 * `undefined` if the value doesn't exist).
 * @param currentState Current state of the value in the storage (possible
 * `undefined` if the value doesn't exist).
 * @param newValue New value to merge with the existing value.
 * @param parentSchema Schema of the value's parent (`undefined` for the root
 * value). Optional if it is known that the value being merged is a non-`null`
 * collection.
 * @param parentValue Parent value (or observable box to contain the value for
 * the root). Optional if it is known that the value being merged is a
 * non-`null` collection.
 * @param parentState Parent state (or observable box to contain the state for
 * the root). Optional if it is known that the value being merged is a
 * non-`null` collection.
 * @param options Options on how to merge the values.
 */
export function mergeValues(
  storage: Storage,
  schema: Schema,
  path: string,
  id: Identifier | null,
  currentValue: any,
  currentState: ValueState,
  newValue: any,
  parentSchema?: Schema,
  parentValue?: any,
  parentState?: ValueState | IObservableValue<ValueState>,
  options: MergeValueOptions = {}
): void {
  const { handleCollections = 'replace', handleUndefined = 'ignore' } = options;
  // Helper function to merge two children
  function mergeChild(chSchema, chId, chVal, chState, newChVal) {
    mergeValues(
      storage,
      chSchema,
      appendToPath(path, chId),
      chId,
      chVal,
      chState,
      newChVal,
      schema,
      currentValue,
      currentState,
      options
    );
  }
  // Helper function to know when a child value should be ignored
  function shouldIgnoreChild(chValue: any): boolean {
    return (
      chValue === undefined &&
      handleCollections === 'replace' &&
      handleUndefined === 'ignore'
    );
  }
  // Handle `undefined` new value
  if (newValue === undefined) {
    if (handleUndefined === 'ignore') {
      return; // Do not merge (ignore new value)
    }
    newValue = initialValue(storage, schema, path);
  }
  // The value doesn't currently exist in the storage, so we have to create it
  if (currentValue === undefined) {
    return initializeValue(
      storage,
      schema,
      path,
      id,
      newValue,
      parentSchema,
      parentValue,
      parentState!
    );
  }
  // Merge simple value (if they differ)
  if (isSimpleSchema(schema)) {
    if (!comparer.structural(currentValue, newValue)) {
      setOnParent(id, newValue, parentSchema, parentValue);
    }
    return;
  }
  // Don't merge an object with itself (nothing to do), unless in append mode,
  // in which case the elements of a list/table need to be appended to the
  // list/table itself
  if (currentValue === newValue && handleCollections !== 'append') {
    return;
  }
  // When setting complex value to `null`
  if (newValue === null) {
    setOnParent(id, null, parentSchema, parentValue);
    currentState.childrenStates = null;
    return;
  }
  // When the current complex value is `null` (similar to `initializeValue` but
  // without creating the state from scratch)
  if (currentValue === null) {
    const val = createValue(storage, schema, path, newValue);
    setOnParent(id, val, parentSchema, parentValue);
    initializeChildrenStates(storage, schema, path, val, currentState);
    normalizeValue(storage, schema, path, val, currentState);
    return;
  }
  const { childrenStates } = currentState;
  // Merge lists
  if (isListSchema(schema)) {
    const elementsSchema = schema.elementsSchema;
    // Keep track of the nr. of `undefined` values that we have ignored when in
    // replace mode (to properly set the values and trim the collection)
    let ignoredElements = 0;
    // Merge/append new elements
    newValue.forEach((el, i) => {
      if (shouldIgnoreChild(el)) {
        return ignoredElements++;
      }
      const j = i - ignoredElements;
      const chId =
        handleCollections === 'append' || j > currentValue.length
          ? currentValue.length
          : j;
      // Don't access untracked `MobX` elements
      let chValue;
      let chState;
      if (chId !== currentValue.length) {
        chValue = currentValue[chId];
        chState = childrenStates[chId];
      }
      mergeChild(elementsSchema, chId, chValue, chState, el);
    });
    // Remove unwanted elements from the existing list
    if (handleCollections === 'replace') {
      const newLen = newValue.length - ignoredElements;
      if (currentValue.length > newLen) {
        currentValue.splice(newLen);
        childrenStates.splice(newLen);
      }
    }
  }
  // Merge maps
  else if (isMapSchema(schema)) {
    const valuesSchema = schema.valuesSchema;
    // Keep track of the nr. of `undefined` values that we have ignored when in
    // replace mode (to properly set the values and trim the collection)
    let ignoredValues = 0;
    // Merge/add new values
    Array.from(newValue.entries()).forEach(([k, v]) => {
      if (shouldIgnoreChild(v)) {
        return ignoredValues++;
      }
      const chValue = currentValue.get(k);
      const chState = childrenStates.get(k);
      // Delete and re-add the current value when in replace mode, so that the
      // insertion order is the same as the one in the new value
      if (handleCollections === 'replace' && chValue !== undefined) {
        currentValue.delete(k);
        currentValue.set(k, chValue);
        childrenStates.delete(k);
        childrenStates.set(k, chState);
      }
      mergeChild(valuesSchema, k, chValue, chState, v);
    });
    // Remove unwanted values from existing map
    if (handleCollections === 'replace') {
      // The first values are the unwanted ones (since new values have been
      // re-added)
      Array.from(currentValue.keys())
        .slice(0, currentValue.size - newValue.size + ignoredValues)
        .forEach((k) => {
          currentValue.delete(k);
          childrenStates.delete(k);
        });
    }
  }
  // Merge records/tuples
  else if (isRecordSchema(schema) || isTupleSchema(schema)) {
    const childrenSchemas = isRecordSchema(schema)
      ? schema.fieldsSchemas
      : schema.elementsSchemas;
    for (const k of Object.keys(childrenSchemas)) {
      const chSchema = childrenSchemas[k];
      // Ignore computed values
      if (!isComputedSchema(chSchema)) {
        const chValue = currentValue[k];
        const chState = childrenStates[k];
        mergeChild(chSchema, k, chValue, chState, newValue[k]);
      }
    }
  }
  // Merge tables
  else {
    const rowsSchema = schema.rowsSchema;
    // Merge/create rows
    const newRowsIndex: Map<Identifier, any> = new Map();
    const newChildrenStates: Map<Identifier, ValueState> = new Map();
    newValue.forEach((row) => {
      if (shouldIgnoreChild(row)) {
        return;
      }
      let chId = row && getRowId(row);
      let chValue;
      let chState;
      if (
        chId === undefined ||
        // Never merge in append mode
        handleCollections === 'append' ||
        !hasRow(currentValue, chId)
      ) {
        chId = nextRowId();
      } else {
        chValue = getRow(currentValue, chId);
        chState = getRowState(childrenStates, chId);
      }
      mergeChild(rowsSchema, chId, chValue, chState, row);
      if (handleCollections === 'replace') {
        newRowsIndex.set(chId, getRow(currentValue, chId));
        newChildrenStates.set(chId, getRowState(childrenStates, chId)!);
      }
    });
    // Replace table rows so that they're ordered similarly to the new value's
    // and remove unwanted rows (as well as their states)
    if (handleCollections === 'replace') {
      currentValue[$lfTable][$lfRowId].replace(newRowsIndex);
      currentValue.replace(Array.from(newRowsIndex.values()));
      childrenStates.replace(newChildrenStates);
    }
  }
}

/**
 * Initialises a value previously not present in the storage.
 * @param storage Storage instance where value should be initialised.
 * @param schema Schema of value to initialise.
 * @param path Path of value to initialise.
 * @param id Identifier of value to initialise (`null` when initialising the
 * root value).
 * @param value Value from which to initalise.
 * @param parentSchema Schema of the value's parent.
 * @param rowIndex Identifier of the parent value where this value is to be
 * initialised. Typically the same as `id`, except for table rows.
 * @param parentValue Parent value.
 * @param parentState State of the parent value.
 * @param rowIndex Used when setting rows within tables (if `undefined`, the new
 * row is appended to the table).
 * @returns Initialised value.
 */
export function initializeValue(
  storage: Storage,
  schema: Schema,
  path: string,
  id: Identifier | null,
  value: any,
  parentSchema: Schema | undefined,
  parentValue: any,
  parentState: ValueState | IObservableValue<ValueState>,
  rowIndex?: number
): any {
  const val = createValue(
    storage,
    schema,
    path,
    value === undefined ? initialValue(storage, schema, path) : value
  );
  setOnParent(id, val, parentSchema, parentValue, rowIndex);
  const st = initializeState(
    storage,
    schema,
    path,
    id,
    val,
    parentSchema,
    parentState
  );
  normalizeValue(storage, schema, path, val, st!);
  return val;
}

/**
 * Sets a given value on its parent value in the storage.
 * @param id Identifier of the value to set (`null` for the root value).
 * @param value Value to set in the storage.
 * @param parentSchema Schema of the value's parent (`undefined` for the root
 * value).
 * @param parentValue Parent value (or observable box to contain the value for
 * the root).
 * @param parentValue Parent value.
 * @param rowIndex Used when setting rows within tables (if `undefined`, the new
 * row is appended to the table).
 */
function setOnParent(
  id: Identifier | null,
  value: any,
  parentSchema: Schema | undefined,
  parentValue: any,
  rowIndex?: number
): void {
  if (id === null) {
    parentValue.set(value); // Observable box
  } else if (isMapSchema(parentSchema!)) {
    parentValue.set(id, value);
  } else if (isTableSchema(parentSchema!)) {
    setRowId(value, id);
    parentValue[$lfTable][$lfRowId].set(id, value);
    parentValue[rowIndex === undefined ? parentValue.length : rowIndex] = value;
  } else {
    parentValue[id!] = value;
  }
}

/**
 * Normalises an observable value already present in the storage by initialising
 * all of its `undefined` values within.
 * @param storage Storage instance with value to normalise.
 * @param schema Schema of value to normalise.
 * @param path Path of value to normalise.
 * @param value Value to normalise.
 * @param state State of the value.
 */
function normalizeValue(
  storage: Storage,
  schema: Schema,
  path: string,
  value: any,
  state: ValueState
): void {
  // Helper function to normalise/initialise a child
  function handleChild(chSch, chId, chV, chSt, rI?) {
    const chP = appendToPath(path, chId);
    if (chV === undefined) {
      initializeValue(storage, chSch, chP, chId, chV, schema, value, state, rI);
    } else {
      normalizeValue(storage, chSch, chP, chV, chSt);
    }
  }
  // No need to normalise `null` or non-complex values
  if (value === null || isSimpleSchema(schema)) {
    return;
  }
  const { childrenStates } = state;
  // Normalise list
  if (isListSchema(schema)) {
    const elementsSchema = schema.elementsSchema;
    value.forEach((el, i) =>
      handleChild(elementsSchema, i, el, childrenStates[i])
    );
  }
  // Normalise map
  else if (isMapSchema(schema)) {
    const valuesSchema = schema.valuesSchema;
    Array.from(value.entries()).forEach(([k, v]) =>
      handleChild(valuesSchema, k, v, childrenStates.get(k))
    );
  }
  // Normalise record/tuple
  else if (isRecordSchema(schema) || isTupleSchema(schema)) {
    const childrenSchemas = isRecordSchema(schema)
      ? schema.fieldsSchemas
      : schema.elementsSchemas;
    for (const k of Object.keys(childrenSchemas)) {
      const childSch = childrenSchemas[k];
      // Ignore computed values
      if (!isComputedSchema(childSch)) {
        handleChild(childSch, k, value[k], childrenStates[k]);
      }
    }
  }
  // Normalise table
  else {
    const rowsSchema = schema.rowsSchema;
    value.forEach((row, i) => {
      let rowId = row && getRowId(row);
      if (rowId === undefined) {
        rowId = nextRowId();
      }
      handleChild(rowsSchema, rowId, row, childrenStates.get(rowId), i);
    });
  }
}

/**
 * Create an observable value from a given value and its schema. `undefined`
 * values are ignored: records and tuples are created with the corresponding
 * properties with a value of `undefined`. By separating the creation of the
 * observable with the "initialisation" of undefined properties, we are able to
 * first set the observable in the storage and then "normalise" it by
 * initialising missing properties, thus allowing `initialValue`s to reference
 * other values initialised before.
 * @param storage Storage instance where the schema resides.
 * @param schema Schema for which to build the observable value.
 * @param path Absolute path for the schema from which to get the observable
 * value.
 * @param value Value to turn into an observable value.
 * @returns Observable value obtained from the provided value.
 */
function createValue(
  storage: Storage,
  schema: Schema,
  path: string,
  value?: any
): any {
  // Helper function to create a child
  function createChild(chSch, chId, chVal) {
    return createValue(storage, chSch, appendToPath(path, chId), chVal);
  }
  // Do nothing with `undefined` values
  if (value == null || isSimpleSchema(schema)) {
    return value;
  }
  const observableOptions = { name: path, deep: false };
  if (isListSchema(schema)) {
    const elementsSchema = schema.elementsSchema;
    return observable.array(
      value.map((el, i) => createChild(elementsSchema, i, el)),
      observableOptions
    );
  }
  if (isMapSchema(schema)) {
    const valuesSchema = schema.valuesSchema;
    return observable.map(
      Array.from(value.entries()).map(([k, v]) => [
        k,
        createChild(valuesSchema, k, v),
      ]),
      observableOptions
    );
  }
  if (isRecordSchema(schema) || isTupleSchema(schema)) {
    let recordOrTuple;
    let childrenSchemas;
    if (isRecordSchema(schema)) {
      recordOrTuple = observable.object({}, {}, observableOptions);
      childrenSchemas = schema.fieldsSchemas;
    } else {
      // Create an observable object by using an array as base; set the name by
      // accessing internal MobX state (the name doesn't seem to be set with
      // `extendObservable` and using `extendObservable` seems to be the only
      // way of creating an observable tuple, see:
      // https://github.com/mobxjs/mobx/issues/1391)
      recordOrTuple = extendObservable([], {}, {}, { deep: false });
      recordOrTuple[$mobx].name = observableOptions.name;
      childrenSchemas = schema.elementsSchemas;
    }
    const props = {};
    const decors = {};
    for (const k of Object.keys(childrenSchemas)) {
      const childSch = childrenSchemas[k];
      const childPath = appendToPath(path, k);
      // Computed values. `value` is completely ignored in case the schema is
      // computed; in an ideal world `value` should be `undefined`
      // TODO: Read computed value from state (allow async computed values)
      if (isComputedSchema(childSch)) {
        // Define a getter to act as a computed property
        defineGetter(props, k, () => {
          const comp = childSch.computedValue(
            storage.relativeStorage(path, true),
            k
          );
          if (process.env.NODE_ENV !== 'production') {
            validateType(comp, childSch, childPath);
          }
          return normalizeComputedValue(childSch, comp);
        });
        // Set the computed decorator
        decors[k] = computed({ equals: comparer.structural });
      } else {
        props[k] = createChild(childSch, k, value[k]);
      }
    }
    extendObservable(recordOrTuple, props, decors);
    return recordOrTuple;
  }
  if (isTableSchema(schema)) {
    const table = newTable(path);
    const rowIndex = table[$lfTable][$lfRowId];
    table.push(
      ...value.map((row) => {
        if (row !== undefined) {
          const rowId = nextRowId();
          const obsRow = createChild(schema.rowsSchema, rowId, row);
          setRowId(obsRow, rowId);
          rowIndex.set(rowId, obsRow);
          return obsRow;
        }
      })
    );
    return table;
  }
}

/**
 * Initial value of a given schema as specified in its `initialValue`. If no
 * `initialValue` was defined then its default value is returned. If the schema
 * is computed, an error is thrown.
 * @param storage Storage instance where the schema resides.
 * @param schema Schema from which to get initial value.
 * @param path Absolute path for the schema from which to get the initial value.
 * @returns Initial value of the provided schema.
 */
export function initialValue(
  storage: Storage,
  schema: Schema,
  path: string
): any {
  if (isComputedSchema(schema)) {
    throw pathError(path, 'Cannot get initial value of computed value');
  }
  const value =
    path === '/'
      ? schema.initialValue
      : parentContextProperty(storage, 'initialValue', schema, path);
  if (value === undefined) {
    return defaultValue(schema);
  }
  if (process.env.NODE_ENV !== 'production') {
    validateType(value, schema, path);
  }
  return value;
}

/**
 * Normalises a computed value by defaulting `undefined` values. This function
 * possibly mutates the passed value.
 * @param schema Schema of the computed value to normalise.
 * @param value Computed value to normalise.
 * @returns Normalised value.
 */
function normalizeComputedValue(schema: Schema, value: any): any {
  if (value === undefined) {
    value = defaultValue(schema);
  }
  if (value !== null && isComplexSchema(schema)) {
    if (isListSchema(schema)) {
      const elementsSchema = schema.elementsSchema;
      value.forEach((el, i) => {
        const normVal = normalizeComputedValue(elementsSchema, el);
        if (el === undefined) {
          value[i] = normVal;
        }
      });
    } else if (isMapSchema(schema)) {
      const valuesSchema = schema.valuesSchema;
      Array.from(value.entries()).forEach(([k, v]) => {
        const normVal = normalizeComputedValue(valuesSchema, v);
        if (v === undefined) {
          value.set(k, normVal);
        }
      });
    } else if (isRecordSchema(schema) || isTupleSchema(schema)) {
      const childrenSchemas = schema.fieldsSchemas || schema.elementsSchemas;
      Object.keys(childrenSchemas).forEach((k) => {
        const val = value[k];
        const normVal = normalizeComputedValue(childrenSchemas[k], val);
        if (val === undefined) {
          value[k] = normVal;
        }
      });
    }
  }
  return value;
}

/**
 * Default value used to set an initial value (in case the schema doesn't
 * specify an initial value). The default value is **not** observable and does
 * not initialise inner properties of records/tuples.
 * @param schema Schema for which to get a default value.
 * @returns Default value for a given schema.
 */
function defaultValue(schema: Schema) {
  if (isNullableSchema(schema)) {
    return null;
  }
  if (isBooleanSchema(schema)) {
    return false;
  }
  if (isDateSchema(schema)) {
    return new Date(0);
  }
  if (isNumberSchema(schema)) {
    return 0;
  }
  if (isStringSchema(schema)) {
    return '';
  }
  if (isListSchema(schema) || isTableSchema(schema) || isTupleSchema(schema)) {
    return [];
  }
  if (isMapSchema(schema)) {
    return new Map();
  }
  if (isRecordSchema(schema)) {
    return {};
  }
}
