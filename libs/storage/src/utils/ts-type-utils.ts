/**
 * Helper which lists the "known" keys of a given type (`keyof` loses
 * information when the object has index signature parameters). Adapted from:
 * https://github.com/Microsoft/TypeScript/issues/25987#issuecomment-408339599
 */
export type KnownKeys<T> = {
  [K in keyof T]: string extends K
    ? never
    : number extends K
    ? never
    : symbol extends K
    ? never
    : K;
} extends { [K in keyof T]: infer U }
  ? U
  : never;
