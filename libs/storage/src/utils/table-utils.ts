import {
  defineProperty,
  IObservableArray,
  observable,
  ObservableMap,
} from 'mobx';

import { Identifier, isValidPathIndex } from './path-utils';
import { ValueState } from './state-creation';

/**
 * Name of the property used to extend "lists" with the indices used to access
 * tables by key.
 */
export const $lfTable = '$lfTable';

/**
 * Name of the property used to keep track of a table row's key.
 */
export const $lfRowId = '$lfRowId';

/**
 * Type of a table value.
 */
export type Table<T extends Record<string, any> = Record<string, any>> =
  IObservableArray<Row<T>> & {
    $lfTable: Record<string, ObservableMap<any, T>>;
  };

/**
 * Type of a table's row.
 */
export type Row<T extends Record<string, any> = Record<string, any>> = T & {
  $lfRowId: Identifier;
};

/**
 * Counter used globally to set the id of a table row.
 */
// TODO: Possibly use timestamped UUIDs instead?
let globalRowCounter = 0;

/**
 * Next row id for the table at a given path.
 * @returns Next row id.
 */
export function nextRowId(): Identifier {
  return globalRowCounter++;
}

/**
 * Whether a given id is a valid row identifier.
 * @param id Row id as found in the path.
 * @param acceptPlaceholder Whether the id placeholder is accepted as a valid
 * index.
 * @returns Whether the provided id is a valid row identifier.
 */
export function isValidTableRowId(
  id: Identifier,
  acceptPlaceholder = false
): boolean {
  return isValidPathIndex(id, acceptPlaceholder);
}

/**
 * Creates a new empty table value.
 * @param path Path of the table value in the storage (used to name the
 * observable).
 * @returns New table value.
 */
export function newTable(path: string): Table {
  // TODO: Create other indices
  const rowIndex = observable.map(
    {},
    { name: `${$lfRowId}(${path})`, deep: false }
  );
  const table = observable.array([], {
    name: path,
    deep: false,
  });
  // Non-enumerable property
  Object.defineProperty(table, $lfTable, {
    value: { [$lfRowId]: rowIndex },
  });
  return table as any;
}

/**
 * Returns the row identifier of a given table row.
 * @param row Row from which to get the identifier.
 * @returns Identifier of the provided row.
 */
export function getRowId(row: Record<string, any>): Identifier {
  return row[$lfRowId];
}

/**
 * Defines the row id as a non-enumerable property of the row.
 * @param row Row where to set identifier.
 * @param id Identifier to set on row.
 */
export function setRowId(row: Record<string, any>, id: Identifier): void {
  defineProperty(row, $lfRowId, { value: id });
}

/**
 * Whether the table value has a row with a given id.
 * @param table Table to check for row.
 * @param id Row id to check.
 * @returns Whether the table has a row with the provided id.
 */
export function hasRow<T>(table: Table<T>, id: Identifier): boolean {
  return table[$lfTable][$lfRowId].has(+id);
}

/**
 * Gets a row with a given id from a table, or `undefined` if no such row
 * exists.
 * @param table Table from where to fetch the row.
 * @param id Id of the row to fetch.
 * @returns Row with provided id or `undefined` if no such row exists.
 */
export function getRow<T>(table: Table<T>, id: Identifier): T | undefined {
  return table[$lfTable][$lfRowId].get(+id);
}

/**
 * Deletes a row with a given id from the table's indices.
 * @param table Table from where to delete the row from the indices.
 * @param id Id of the row to delete from the indices.
 */
export function deleteRowFromIndices(table: Table, id: Identifier): void {
  table[$lfTable][$lfRowId].delete(+id);
}

/**
 * Gets the state of a row given the children states and the row identifier, or
 * `undefined` if no such state exists.
 * @param childrenStates Children states as found in the parent's value state.
 * @param id Row identifier for which to fetch state.
 * @returns State of the row with provided id or `undefined` if no such row
 * exists.
 */
export function getRowState(
  childrenStates: ObservableMap<Identifier, ValueState>,
  id: Identifier
): ValueState | undefined {
  return childrenStates.get(+id);
}

/**
 * Delete the state of a table row.
 * @param childrenStates Children states as found in the parent's value state.
 * @param id Row identifier for which to delete state.
 */
export function deleteRowState(
  childrenStates: ObservableMap<Identifier, ValueState>,
  id: Identifier
): void {
  childrenStates.delete(+id);
}
