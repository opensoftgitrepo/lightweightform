import { Schema } from '../schema-types';

import { pathError } from './error-utils';
import {
  Identifier,
  isValidPathId,
  isValidPathIndex,
  PATH_ID_PLACEHOLDER,
} from './path-utils';
import {
  isListSchema,
  isMapSchema,
  isRecordSchema,
  isTableSchema,
  isTupleSchema,
} from './schema-checker';
import { ValueState } from './state-creation';
import { getRow, getRowState, isValidTableRowId } from './table-utils';

/**
 * Returns the child schema of a given schema given its path and the identifier
 * of the child. Errors are thrown when the provided identifier isn't valid.
 * @param parentSchema Parent schema from which to fetch child schema.
 * @param id Identifier of the child schema on the parent.
 * @param parentPath Path of the parent schema.
 * @returns Schema of the child with provided identifier.
 */
export function childSchema(
  parentSchema: Schema,
  parentPath: string,
  id: Identifier
): Schema {
  if (isListSchema(parentSchema)) {
    // Only integers are valid identifiers for `ListSchema` elements (the
    // placeholder is also valid here)
    if (!isValidPathIndex(id, true)) {
      throw pathError(parentPath, `Invalid list index '${id}'`);
    }
    return parentSchema.elementsSchema;
  }
  if (isMapSchema(parentSchema)) {
    // A map key may not be `'.'` or `'..'` or contain a forward slash
    if (!isValidPathId(id)) {
      throw pathError(parentPath, `Invalid map key '${id}'`);
    }
    return parentSchema.valuesSchema;
  }
  if (isRecordSchema(parentSchema)) {
    // If there is no schema, then the field was invalid
    if (!(id in parentSchema.fieldsSchemas)) {
      throw pathError(parentPath, `Invalid record field '${id}'`);
    }
    return parentSchema.fieldsSchemas[id];
  }
  if (isTableSchema(parentSchema)) {
    // Only allow valid row identifiers (or a placeholder)
    if (!isValidTableRowId(id, true)) {
      throw pathError(parentPath, `Invalid table row '${id}'`);
    }
    return parentSchema.rowsSchema;
  }
  if (isTupleSchema(parentSchema)) {
    // Only valid integer indices are valid identifiers
    if (!isValidPathIndex(id) || +id >= parentSchema.elementsSchemas.length) {
      throw pathError(parentPath, `Invalid tuple element '${id}'`);
    }
    return parentSchema.elementsSchemas[id];
  }
  // Schema is not "complex", so it has no children schemasvalue-creation3
  throw pathError(parentPath, `Schema has no child '${id}'`);
}

/**
 * Returns the value of a child with a given id, given its parent schema, parent
 * path, and parent value. This method assumes that the provided path
 * (`parentPath` + `id`) corresponds to a valid schema, i.e. calling
 * `childSchema(parentSchema, parentPath, id)` should yield a valid schema.
 * @param parentSchema Schema of parent value from which to fetch child value.
 * @param parentPath Absolute path of the parent.
 * @param parentValue Parent value from which to fetch child value.
 * @param id Identifier of the child on the parent.
 * @param allowAccessToNewValue Whether to allow accessing a nonexisting child,
 * if the provided id would correspond to a new child.
 * @returns Value of the child with the provided identifier.
 */
export function childValue(
  parentSchema: Schema,
  parentPath: string,
  parentValue: any,
  id: Identifier,
  allowAccessToNewValue: boolean = false
): any {
  id = id.toString();
  if (parentValue === null) {
    throw pathError(parentPath, `Cannot access '${id}' of null`);
  }
  if (isMapSchema(parentSchema)) {
    const value = parentValue.get(id);
    if (!allowAccessToNewValue && value === undefined) {
      throw pathError(parentPath, `Map has no value with key '${id}'`);
    }
    return value;
  }
  if (isListSchema(parentSchema) || isTableSchema(parentSchema)) {
    if (id === PATH_ID_PLACEHOLDER) {
      throw pathError(parentPath, `Value paths may not contain placeholders`);
    }
    if (isListSchema(parentSchema)) {
      const maxId = parentValue.length - (allowAccessToNewValue ? 0 : 1);
      if (+id > maxId) {
        throw pathError(
          parentPath,
          `List index '${id}' is out of bounds (greater than '${maxId}')`
        );
      }
      // Don't access untracked `MobX` element
      return +id === parentValue.length ? undefined : parentValue[id];
    } else {
      const row = getRow(parentValue, id);
      if (row === undefined) {
        throw pathError(parentPath, `Table has no row with id '${id}'`);
      }
      return row;
    }
  }
  const ch = parentValue[id];
  if (ch === undefined) {
    throw pathError(
      parentPath,
      `Attempting to access '${id}' before it is defined`
    );
  }
  return ch;
}

/**
 * Returns the state of a child value with a given id, given its parent schema,
 * parent path, parent state, and whether the parent is computed. This method
 * assumes that the provided path corresponds to a valid schema. Children of
 * computed values all share the state of the parent computed value.
 * @param parentSchema Schema of parent value from which to fetch child state.
 * @param parentPath Absolute path of the parent.
 * @param parentState Parent state from which to fetch child state.
 * @param parentIsComputed Whether the parent value is computed.
 * @param id Identifier of the child on the parent.
 * @param allowAccessToNewValue Whether to allow accessing a nonexisting child,
 * if the provided id would correspond to a new child.
 * @returns State of the child with the provided identifier.
 */
export function childState(
  parentSchema: Schema,
  parentPath: string,
  parentState: ValueState,
  parentIsComputed: boolean,
  id: Identifier,
  allowAccessToNewValue: boolean = false
): ValueState {
  if (parentIsComputed) {
    return parentState;
  }
  const { childrenStates } = parentState;
  id = id.toString();
  if (childrenStates === null) {
    throw pathError(parentPath, `Cannot access '${id}' of null`);
  }
  if (isMapSchema(parentSchema)) {
    const valueState = childrenStates.get(id);
    if (!allowAccessToNewValue && valueState === undefined) {
      throw pathError(parentPath, `Map has no value with key '${id}'`);
    }
    return valueState;
  }
  if (isListSchema(parentSchema) || isTableSchema(parentSchema)) {
    if (id === PATH_ID_PLACEHOLDER) {
      throw pathError(parentPath, `Value paths may not contain placeholders`);
    }
    if (isListSchema(parentSchema)) {
      const maxId = childrenStates.length - (allowAccessToNewValue ? 0 : 1);
      if (+id > maxId) {
        throw pathError(
          parentPath,
          `List index '${id}' is out of bounds (greater than '${maxId}')`
        );
      }
      // Don't access untracked `MobX` element
      return +id === childrenStates.length ? undefined : childrenStates[id];
    } else {
      const rowState = getRowState(childrenStates, id);
      if (rowState === undefined) {
        throw pathError(parentPath, `Table has no row with id '${id}'`);
      }
      return rowState;
    }
  }
  const chState = childrenStates[id];
  if (chState === undefined) {
    throw pathError(
      parentPath,
      `Attempting to access '${id}' before it is defined`
    );
  }
  return chState;
}
