import { isObservableArray } from 'mobx';

/**
 * Whether the provided value is an array or a MobX observable array.
 * @param value Value to check if it is array-like.
 * @returns Whether the value is an array or a MobX observable array.
 */
export function isArrayLike(value: any): value is any[] {
  return Array.isArray(value) || isObservableArray(value);
}
