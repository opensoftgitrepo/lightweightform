import { action, observable, when } from 'mobx';
import {
  fromPromise,
  FULFILLED,
  IFulfilledPromise,
  IPromiseBasedObservable,
  isPromiseBasedObservable,
  PENDING,
} from 'mobx-utils';

/**
 * Observable promise which, when used within a MobX computed value, provides an
 * `invalidate` function that forces the recomputation of said computed value.
 */
export type InvalidateablePromise<T = any> = IPromiseBasedObservable<T> & {
  invalidate(): void;
};

/**
 * Whether a given value is (probably) a promise (if it is an object which
 * defines a `then` function).
 * @param val Value to check if it (likely) is a promise.
 * @returns Whether a given value is (probably) a promise.
 */
export function isPromise(val: any): val is Promise<any> {
  return (
    val !== null && typeof val === 'object' && typeof val.then === 'function'
  );
}

/**
 * Returns an observable promise from the provided value. If a promise is
 * passed, then an observable promise is created from the passed promise; if
 * another value is passed, then a resolved observable promise is created with
 * such value.
 * @param val Value to promisify.
 * @returns An observable promise from the provided value.
 */
export function promisify<T>(
  val: T | PromiseLike<T>
): IPromiseBasedObservable<T> {
  return isPromise(val) ? fromPromise(val) : fromPromise.resolve(val);
}

/**
 * Returns a promise which resolves whenever a certain MobX tracked function
 * evaluates to `true`.
 * @param fn Predicate that causes the promise to resolve when it evaluates to
 * `true`.
 * @param name Name to pass to MobX `when`.
 * @returns A promise that resolves once the passed predicate evaluates to
 * `true`.
 */
export function resolveWhen(fn: () => boolean, name?: string): Promise<void> {
  return new Promise((res, rej) => when(fn, res, { onError: rej, name }));
}

/**
 * Returns the passed value or, when an observable promise is passed, its
 * fulfilled value (or `pendingValue`/`rejectedValue` when the promise is in the
 * pending/rejected state).
 * @param val Value that may be a promise (from which to return fulfilled
 * value).
 * @param pendingValue Value to return when the promise is in the pending state.
 * @param rejectedValue Value to return when the promise is in the rejected
 * state.
 * @returns Either the passed value or the fulfilled value of a passed
 * observable promise.
 */
export function fulfilledValue(val: any, pendingValue?, rejectedValue?): any {
  return isPromiseBasedObservable(val)
    ? val.state === FULFILLED
      ? (val as IFulfilledPromise<any>).value
      : val.state === PENDING
      ? pendingValue
      : rejectedValue
    : val;
}

/**
 * Creates an observable promise which, when used within a MobX computed value
 * provides means of being invalidated (and thus force the recomputation of the
 * computed value).
 * @param promise Promise to transform into an observable invalidate-able
 * promise.
 * @param invalidateActionName Name of the action that invalidates the computed
 * value which accesses this promise.
 * @returns Observable invalidate-able promise.
 */
export function invalidateablePromise<T>(
  promise: Promise<T>,
  invalidateActionName: string = 'invalidate'
): InvalidateablePromise<T> {
  // Create a way of recomputing the promise by depending on a dummy "trigger"
  // observable which is used to invalidate the computed value
  const trigger = observable.box(true, {
    name: 'invalidateablePromiseTrigger',
  });
  trigger.get(); // Make the computed value depend on the trigger
  const obsPromise: any = fromPromise(promise);
  // Add a function to the promise that changes the value of the trigger, thus
  // invalidating the computed value
  obsPromise.invalidate = () =>
    action(invalidateActionName, () => trigger.set(!trigger.get()))();
  return obsPromise;
}
