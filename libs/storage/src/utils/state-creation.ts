import {
  $mobx,
  comparer,
  computed,
  extendObservable,
  IObservableValue,
  observable,
  set,
} from 'mobx';

import { Schema } from '../schema-types';
import { Storage } from '../storage';

import { isArrayLike } from './array-utils';
import { ContextFunction, parentContextProperty } from './context-utils';
import { isValidationIssue } from './error-utils';
import { appendToPath, Identifier } from './path-utils';
import { invalidateablePromise, isPromise } from './promise-utils';
import {
  isCollectionSchema,
  isComplexSchema,
  isComputedSchema,
  isDateSchema,
  isListSchema,
  isMapSchema,
  isNullableSchema,
  isNumberSchema,
  isRecordSchema,
  isStringSchema,
  isTableSchema,
} from './schema-checker';
import { requiresValidation } from './schema-utils';
import { $lfRowId } from './table-utils';
import { validateType } from './type-validation';
import { validateValue } from './value-validation';

/**
 * Type used to represent the state of a value of the storage.
 */
export interface ValueState {
  /**
   * State properties of the value.
   */
  props: Record<string, any>;
  /**
   * Only used for complex non-computed values. Stores the states on an object
   * of type similar to that of the value itself.
   */
  childrenStates?: any;
}

/**
 * Sets a given state on its parent state in the storage.
 * @param id Identifier of the state to set (`null` for the root value).
 * @param state State to set in the storage.
 * @param parentSchema Schema of the value's parent (`undefined` for the root
 * value).
 * @param parentState Parent state (or observable box to contain the state for
 * the root).
 */
function setOnParentState(
  id: Identifier | null,
  state: ValueState | undefined,
  parentSchema: Schema | undefined,
  parentState: ValueState | IObservableValue<ValueState>
): void {
  if (id === null) {
    // Observable box
    (parentState as IObservableValue<ValueState>).set(state!);
  } else if (isMapSchema(parentSchema!) || isTableSchema(parentSchema!)) {
    (parentState as ValueState).childrenStates.set(id, state);
  } else {
    (parentState as ValueState).childrenStates[id!] = state;
  }
}

/**
 * Initialises the state of a value previously not present in the storage.
 * Passed `undefined` children values that aren't associated to a computed
 * schema are set a state of `undefined`, since the state initialisation occurs
 * before the value normalisation and they'll be initialised after.
 * @param storage Storage instance where value state should be initialised.
 * @param schema Schema of value with state to initialise.
 * @param path Path of value state to initialise.
 * @param id Identifier of value with state to initialise.
 * @param value Value for which to initialise state.
 * @param parentSchema Schema of the value's parent.
 * @param parentState State of the value's parent.
 * @returns Initialised state (which may be `undefined` in the scenario
 * explained above).
 */
export function initializeState(
  storage: Storage,
  schema: Schema,
  path: string,
  id: Identifier | null,
  value: any,
  parentSchema: Schema | undefined,
  parentState: ValueState | IObservableValue<ValueState>
): ValueState | undefined {
  if (!isComputedSchema(schema) && value === undefined) {
    setOnParentState(id, undefined, parentSchema, parentState);
    return;
  }
  const hasChildrenSt = !isComputedSchema(schema) && isComplexSchema(schema);
  const state = observable.object(
    {
      props: initialStateProperties(storage, schema, path),
      ...(hasChildrenSt ? { childrenStates: undefined } : {}),
    },
    {},
    { name: `state(${path})`, deep: false }
  );
  setOnParentState(id, state, parentSchema, parentState);
  if (hasChildrenSt) {
    initializeChildrenStates(storage, schema, path, value, state);
  }
  return state;
}

/**
 * Creates the initial children states for a given value with a complex
 * non-computed schema.
 * @param storage Storage instance with the provided schema and value.
 * @param schema Complex non-computed schema of the value for which the children
 * states are to be initialised.
 * @param path Path of the provided value.
 * @param value Value for which the children states are to be initialised.
 * @param state State with children to initialise.
 */
export function initializeChildrenStates(
  storage: Storage,
  schema: Schema,
  path: string,
  value: any,
  state: ValueState
): any {
  // Helper function to create the initial state of a child given its schema,
  // value, and id
  function initChildSt(chiSch: Schema, id?: Identifier, chiVal?: any) {
    // `id` is `undefined` when the table row id isn't yet known
    if (id !== undefined) {
      initializeState(
        storage,
        chiSch,
        appendToPath(path, id),
        id,
        chiVal,
        schema,
        state
      );
    }
  }

  // Initial children states for `null` values
  if (value === null) {
    state.childrenStates = null;
  } else {
    // Options for the creation of observable children states
    const observableOptions = { name: `childrenStates(${path})`, deep: false };
    // Initialise children states for lists
    if (isListSchema(schema)) {
      state.childrenStates = observable.array([], observableOptions);
      value.forEach((el, i) => initChildSt(schema.elementsSchema, i, el));
    }
    // Initialise children states for maps
    else if (isMapSchema(schema)) {
      state.childrenStates = observable.map({}, observableOptions);
      Array.from(value.entries()).forEach(([k, v]) =>
        initChildSt(schema.valuesSchema, k, v)
      );
    }
    // Initialise children states for tables
    else if (isTableSchema(schema)) {
      state.childrenStates = observable.map({}, observableOptions);
      value.forEach((row) =>
        initChildSt(schema.rowsSchema, row && row[$lfRowId], row)
      );
    }
    // Initialise children states for records/tuples
    else {
      let childrenSchemas;
      if (isRecordSchema(schema)) {
        state.childrenStates = observable.object({}, {}, observableOptions);
        childrenSchemas = schema.fieldsSchemas;
      } else {
        // Done similarly to the value creation
        state.childrenStates = extendObservable([], {}, {}, { deep: false });
        state.childrenStates[$mobx].name = observableOptions.name;
        childrenSchemas = schema.elementsSchemas;
      }
      extendObservable(
        state.childrenStates,
        Object.keys(childrenSchemas).reduce((obj, k) => {
          obj[k] = undefined;
          return obj;
        }, {}),
        {}
      );
      Object.keys(childrenSchemas).forEach((k) => {
        const chSch = childrenSchemas[k];
        // Do not compute (access) a computed value
        const chVal = isComputedSchema(chSch) ? undefined : value[k];
        initChildSt(chSch, k, chVal);
      });
    }
  }
}

/**
 * Creates the initial state properties for a value with the provided schema.
 * @param storage Storage instance with the provided schema.
 * @param schema Schema of the value for which to create the state properties.
 * @param path Path of the provided schema.
 * @returns State properties for a value with the provided schema.
 */
function initialStateProperties(
  storage: Storage,
  schema: Schema,
  path: string
): Record<string, any> {
  // Initial schema-defined properties
  const initialProps = schema.initialState || {};
  const props = observable.object({}, {}, { name: `stateProps(${path})` });
  Object.keys(initialProps).forEach((prop) => {
    set(props, prop, statePropValue(storage, path, prop, initialProps[prop]));
  });

  // No need to add validation-related properties when values of the schema
  // don't need to be validated
  if (!requiresValidation(schema)) {
    return props;
  }

  // Helper function to add validation-related properties found in the schema as
  // computed state properties
  function addSchemaValidationProp(prop, schemaCheckFn, cb?) {
    // To consume less memory, we fetch the "state property" directly from the
    // schema when it isn't a function (since it is constant)
    if (
      (!schemaCheckFn || schemaCheckFn(schema)) &&
      typeof schema[prop] === 'function'
    ) {
      const schemaPropComp = computed(
        () => {
          const val = parentContextProperty(storage, prop, schema, path);
          // XXX: Set the `computed` as `keepAlive` only when handling promises
          (schemaPropComp as any).keepAlive = isPromise(val);
          return isPromise(val)
            ? invalidateablePromise(
                cb ? val.then((v) => cb(v)) : val,
                `stateProps(${path}).${prop}-invalidate`
              )
            : cb
            ? cb(val)
            : val;
        },
        // All validation props should be compared structurally
        { name: `stateProps(${path}).${prop}`, equals: comparer.structural }
      );
      set(props, prop, schemaPropComp);
    }
  }

  // Add `allowedValues` as a computed state property
  addSchemaValidationProp(
    'allowedValues',
    undefined,
    // Validate the type of each allowed value when in development mode
    process.env.NODE_ENV !== 'production'
      ? (vals) => {
          vals && vals.forEach((val) => validateType(val, schema, path));
          return vals;
        }
      : undefined
  );

  // Add "simple" validation-related properties as computed state properties
  for (const [simpleProp, schemaCheckFn] of [
    ['isRequired', isNullableSchema],
    ['minDate', isDateSchema],
    ['maxDate', isDateSchema],
    ['minSize', isCollectionSchema],
    ['maxSize', isCollectionSchema],
    ['min', isNumberSchema],
    ['max', isNumberSchema],
    ['minLength', isStringSchema],
    ['maxLength', isStringSchema],
  ]) {
    addSchemaValidationProp(simpleProp, schemaCheckFn);
  }

  // Add the result of calling the schema's `validate` function(s) as a computed
  // state property (when defined)
  if (schema.validate !== undefined) {
    const validateComp = computed(
      () => {
        const validate = schema.validate!;
        const fns: ContextFunction[] = isArrayLike(validate)
          ? validate
          : [validate];
        const relStorage = storage.relativeStorage(path);
        const fnsResults = fns.map((fn) => fn(relStorage));
        // Merge all issues from all validation functions into an array
        const mergeIssues = (results) =>
          results.reduce((issues, val) => {
            if (isArrayLike(val)) {
              issues.push(...val.filter(isValidationIssue));
            } else if (isValidationIssue(val)) {
              issues.push(val);
            }
            return issues;
          }, []);
        const handlingPromises = fnsResults.some((val) => isPromise(val));
        // XXX: Set the `computed` as `keepAlive` only when handling promises
        (validationIssuesComp as any).keepAlive = handlingPromises;
        // Wait for all async validation functions to complete before
        // returning the issues
        return handlingPromises
          ? invalidateablePromise(
              Promise.all(fnsResults).then(mergeIssues),
              `stateProps(${path}).validate-invalidate`
            )
          : mergeIssues(fnsResults);
      },
      { name: `stateProps(${path}).validate`, equals: comparer.structural }
    );
    set(props, 'validate', validateComp);
  }

  // Add `validationIssues` as a computed state property
  const validationIssuesComp = computed(
    () => {
      const [{ value: val, state }] = storage
        .pathInfo(path, { fetchValues: true, fetchStates: true })
        .slice(-1);
      const issuesMap = validateValue(schema, path, val, state!);
      // XXX: Set the `computed` as `keepAlive` only when handling promises
      (validationIssuesComp as any).keepAlive = isPromise(issuesMap);
      return isPromise(issuesMap)
        ? invalidateablePromise(
            issuesMap,
            `stateProps(${path}).validationIssues-invalidate`
          )
        : issuesMap;
    },
    {
      name: `stateProps(${path}).validationIssues`,
      equals: comparer.structural,
    }
  );
  set(props, 'validationIssues', validationIssuesComp);

  return props;
}

/**
 * Value to set as a state property of a storage value. When passed a function,
 * we return a computed value which executes and caches the result of calling
 * such function with the storage (relative to the path of the storage value) as
 * argument. Asynchronous functions are further made observable.
 * @param storage Storage instance.
 * @param path Path of the value with the state property.
 * @param prop Name of the state property.
 * @param value Value from which to create the value of the state property.
 * @returns Either the provided `value` (when not a function), or a computed
 * value, when `value` is a function.
 */
export function statePropValue(
  storage: Storage,
  path: string,
  prop: string,
  value: any
): any {
  if (typeof value === 'function') {
    const statePropComp = computed(
      () => {
        const val = value(storage.relativeStorage(path));
        // XXX: Set the `computed` as `keepAlive` only when handling promises
        (statePropComp as any).keepAlive = isPromise(val);
        return isPromise(val)
          ? invalidateablePromise(val, `stateProps(${path}).${prop}-invalidate`)
          : val;
      },
      { name: `stateProps(${path}).${prop}` }
    );
    return statePropComp;
  }
  return value;
}
