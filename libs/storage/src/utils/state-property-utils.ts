import { get, has, isComputed as isMobxComputed, set } from 'mobx';
import { REJECTED } from 'mobx-utils';

import { Schema, TupleSchema } from '../schema-types';
import { Storage } from '../storage';

import { pathError } from './error-utils';
import { appendToPath } from './path-utils';
import { isPromise } from './promise-utils';
import {
  isComputedSchema,
  isListSchema,
  isMapSchema,
  isRecordSchema,
  isTableSchema,
  isTupleSchema,
} from './schema-checker';
import { requiresValidation } from './schema-utils';
import { isReservedStateProp } from './schema-validation';
import { statePropValue, ValueState } from './state-creation';

/**
 * Options used to set a state property.
 */
export interface SetStatePropertyOptions {
  /**
   * Whether to propagate the state property upwards in the value chain.
   * Defaults to `false`.
   */
  propagateUp?: boolean;
  /**
   * Whether to propagate the state property to all children of the value.
   * Defaults to `false`.
   */
  propagateDown?: boolean;
  /**
   * Whether to set the state property if the value is computed. Defaults to
   * `true`. Useful for (user-side) state properties such as `isDirty`.
   */
  setOnComputed?: boolean;
  /**
   * Whether to stop propagating the state property upwards once the ancestor's
   * state property is equal to the value being set. Defaults to `false`. Useful
   * for (user-side) state properties such as `isDirty`.
   */
  stopPropagateUpOnEqual?: boolean;
}

/**
 * Whether a given state property exists in a value's state.
 * @param schema Schema of the value.
 * @param state Value state where to check for the existence of the property.
 * @param prop Property with existence to check.
 * @returns Whether a given property exists in a value's state.
 */
export function hasStateProperty(
  schema: Schema,
  state: ValueState,
  prop: string
): boolean {
  // Fake existence of reserved state properties (they may not exist to consume
  // less memory but it should seem like they do)
  return isReservedStateProp(schema, prop) ? true : has(state.props, prop);
}

/**
 * Gets the value of a state property given the value's state. If the state
 * property is computed, it requests the value of the computation.
 * @param schema Schema of the value.
 * @param state Value state from where to fetch the property.
 * @param prop Property to fetch.
 * @returns Value of the state property.
 */
export function getStateProperty(
  schema: Schema,
  state: ValueState,
  prop: string
): any {
  // Fake existence of reserved state properties (they may not exist to consume
  // less memory but it should seem like they do)
  if (isReservedStateProp(schema, prop)) {
    if (prop === 'validate' && schema.validate !== undefined) {
      return get(state.props, prop).get();
    }
    if (prop === 'validationIssues') {
      return requiresValidation(schema) ? get(state.props, prop).get() : {};
    }
    return typeof schema[prop] === 'function'
      ? get(state.props, prop).get()
      : schema[prop];
  }
  const val = get(state.props, prop);
  return isMobxComputed(val) ? val.get() : val;
}

/**
 * Sets the value of a value's state property. Throws an error when attempting
 * to set the value of a reserved state property. When passed a function, we
 * set, as state property, a computed value which executes and caches the result
 * of calling such function with the storage (relative to the path of the
 * storage value) as argument. Asynchronous functions are further made
 * observable. Supports options for propagating up or down the state property
 * (useful for implementing states such as `isDirty` in user applications).
 * @param storage Storage instance.
 * @param path Path on which to set the state property (possibly relative to the
 * storage's current path).
 * @param prop Property with value to set.
 * @param value Value to set on the state property.
 * @param options Options used for setting the state property.
 */
export function setStateProperty(
  storage: Storage,
  path: string,
  prop: string,
  value: any,
  {
    propagateUp = false,
    propagateDown = false,
    setOnComputed = true,
    stopPropagateUpOnEqual = false,
  }: SetStatePropertyOptions = {}
): void {
  function throwIfReserved(sc: Schema, pt: string) {
    if (isReservedStateProp(sc, prop)) {
      throw pathError(
        pt,
        `State property "${prop}" is reserved and cannot be overridden`
      );
    }
  }
  function setPropCascade(
    sc: Schema,
    pt: string,
    st: ValueState,
    isComp: boolean,
    cascade: boolean
  ) {
    if (setOnComputed || !isComp) {
      throwIfReserved(sc, pt);
      set(st.props, prop, statePropValue(storage, pt, prop, value));
    }
    if (st.childrenStates && cascade && !isComp) {
      if (isListSchema(sc)) {
        const elsSchema = sc.elementsSchema;
        st.childrenStates.forEach((s: ValueState, i) =>
          setPropCascade(elsSchema, appendToPath(pt, i), s, isComp, true)
        );
      } else if (isMapSchema(sc) || isTableSchema(sc)) {
        const chSchema = sc.valuesSchema || sc.rowsSchema;
        Array.from(st.childrenStates.entries()).forEach(([k, v]) =>
          setPropCascade(chSchema, appendToPath(pt, k), v, isComp, true)
        );
      } else {
        const chSchemas = isRecordSchema(sc)
          ? sc.fieldsSchemas
          : (sc as TupleSchema).elementsSchemas;
        Object.keys(chSchemas).forEach((k) => {
          const chiSchema = chSchemas[k];
          setPropCascade(
            chiSchema,
            appendToPath(pt, k),
            st.childrenStates[k],
            isComp || isComputedSchema(chiSchema),
            true
          );
        });
      }
    }
  }
  const pathInfo = storage.pathInfo(path, { fetchStates: true });
  const [{ schema, path: pat, state, isComputed }] = pathInfo.slice(-1);
  // Set state property and possibly cascade it to all descendants
  setPropCascade(schema, pat, state!, isComputed, propagateDown);
  // Propagate state to all ancestors
  if (propagateUp) {
    for (
      let i = pathInfo.length - 2;
      i >= 0 &&
      (!stopPropagateUpOnEqual ||
        getStateProperty(pathInfo[i].schema, pathInfo[i].state!, prop) !==
          value);
      --i
    ) {
      const {
        schema: sc,
        path: pt,
        state: st,
        isComputed: isComp,
      } = pathInfo[i];
      if (setOnComputed || !isComp) {
        throwIfReserved(sc, pt);
        set(st!.props, prop, statePropValue(storage, pt, prop, value));
      }
    }
  }
}

/**
 * Forces the recomputation of all asynchronous state properties whose
 * evaluation resulted in a rejected promise. If `recomputeAll` is `true`, then
 * it forces the recomputation of all asynchronous state properties (even if
 * they weren't rejected).
 * @param schema Schema of the value with state properties that need to be
 * recomputed.
 * @param state Path of the provided schema.
 * @param recomputeAll Whether to recompute all asynchronous state properties.
 * Defaults to `false` (only rejected asynchronous state properties are
 * recomputed).
 */
export function recomputeStateProperties(
  schema: Schema,
  state: ValueState,
  recomputeAll: boolean = false
): void {
  const { props: stateProps, childrenStates } = state;
  // Trigger the recomputation of all children
  if (childrenStates) {
    if (isListSchema(schema)) {
      for (const st of childrenStates) {
        recomputeStateProperties(schema.elementsSchema, st);
      }
    } else if (isMapSchema(schema)) {
      for (const st of Array.from(childrenStates.values())) {
        recomputeStateProperties(schema.valuesSchema, st as ValueState);
      }
    } else if (isRecordSchema(schema)) {
      for (const field of Object.keys(schema.fieldsSchemas)) {
        recomputeStateProperties(
          schema.fieldsSchemas[field],
          childrenStates[field]
        );
      }
    } else if (isTupleSchema(schema)) {
      schema.elementsSchemas.forEach((elSchema, i) =>
        recomputeStateProperties(elSchema, childrenStates[i])
      );
    }
  }

  // Trigger the recomputation of the relevant properties
  for (const prop of Array.from(Object.keys(stateProps))) {
    const val = getStateProperty(schema, state, prop);
    if (isPromise(val) && (recomputeAll || (val as any).state === REJECTED)) {
      (val as any).invalidate();
    }
  }
}
