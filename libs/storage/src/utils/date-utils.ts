/**
 * Whether a given value is a JavaScript `Date`.
 * @param value Value to check if it is a date.
 * @return `true` when the value is a JavaScript date; `false` otherwise.
 */
export function isDate(value: any): value is Date {
  return (
    typeof value === 'object' &&
    Object.prototype.toString.call(value) === '[object Date]'
  );
}

/**
 * Compares two instances of `Date` and returns a number `< 0` if
 * `date1 < date2`, `0` when `date1 = date2`, and a number `> 0` if
 * `date1 > date2`.
 * @param date1 First date to compare.
 * @param date2 Second date to compare.
 * @returns A negative number, `0` or a positive number depending on whether
 * `date1` is lesser than, equal, or greater that `date2` respectively.
 */
export function compareDates(date1: Date, date2: Date): number {
  return date1.getTime() - date2.getTime();
}
