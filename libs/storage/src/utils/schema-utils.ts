import { Schema } from '../schema-types';

import {
  isCollectionSchema,
  isComplexSchema,
  isComputedSchema,
  isDateSchema,
  isListSchema,
  isMapSchema,
  isNullableSchema,
  isNumberSchema,
  isRecordSchema,
  isStringSchema,
  isTableSchema,
  isTupleSchema,
} from './schema-checker';

/**
 * Whether values of a given schema require any type of validation (if the
 * schema or any of its children contains at least one validation property).
 * @param schema Schema from which to check if their values require validation.
 * @returns Whether values of a given schema require validation.
 */
export function requiresValidation(schema: Schema): boolean {
  if (
    schema.allowedValues !== undefined ||
    schema.validate !== undefined ||
    (isNullableSchema(schema) && schema.isRequired !== undefined) ||
    (isDateSchema(schema) &&
      (schema.minDate !== undefined || schema.maxDate !== undefined)) ||
    (isNumberSchema(schema) &&
      (schema.min !== undefined || schema.max !== undefined)) ||
    (isStringSchema(schema) &&
      (schema.minLength !== undefined || schema.maxLength !== undefined)) ||
    (isCollectionSchema(schema) &&
      (schema.minSize !== undefined || schema.maxSize !== undefined))
  ) {
    return true;
  }
  if (isComplexSchema(schema) && !isComputedSchema(schema)) {
    if (isListSchema(schema)) {
      return requiresValidation(schema.elementsSchema);
    }
    if (isMapSchema(schema)) {
      return requiresValidation(schema.valuesSchema);
    }
    if (isTableSchema(schema)) {
      return requiresValidation(schema.rowsSchema);
    }
    if (isRecordSchema(schema)) {
      return Object.keys(schema.fieldsSchemas).some((field) =>
        requiresValidation(schema.fieldsSchemas[field])
      );
    }
    if (isTupleSchema(schema)) {
      return schema.elementsSchemas.some((elSchema) =>
        requiresValidation(elSchema)
      );
    }
  }
  return false;
}
