import { comparer } from 'mobx';

import { Schema } from '../schema-types';

import { isArrayLike } from './array-utils';
import { compareDates } from './date-utils';
import { addValidationIssue, ValidationIssuesMap } from './error-utils';
import { isPromise } from './promise-utils';
import {
  isCollectionSchema,
  isComputedSchema,
  isDateSchema,
  isListSchema,
  isMapSchema,
  isNullableSchema,
  isNumberSchema,
  isRecordSchema,
  isStringSchema,
  isTupleSchema,
} from './schema-checker';
import { requiresValidation } from './schema-utils';
import { ValueState } from './state-creation';
import { getStateProperty } from './state-property-utils';

/**
 * Issue code representing that a value's validation function was rejected (a
 * promise failed to resolve).
 */
export const IS_REJECTED_CODE = 'LF_IS_REJECTED';
/**
 * Default issue code representing that the value is required and thus cannot be
 * `null`.
 */
export const IS_REQUIRED_CODE = 'LF_IS_REQUIRED';
/**
 * Default issue code representing that the value is not one of the allowed
 * values defined by its schema.
 */
export const DISALLOWED_VALUE_CODE = 'LF_DISALLOWED_VALUE';
/**
 * Default issue code representing that the date value does not obey its
 * schema's `minDate`/`maxDate` restrictions.
 */
export const DATE_OUT_OF_BOUNDS_CODE = 'LF_DATE_OUT_OF_BOUNDS';
/**
 * Default issue code representing that the collection value does not obey its
 * schema's `minSize`/`maxSize` restrictions.
 */
export const SIZE_OUT_OF_BOUNDS_CODE = 'LF_SIZE_OUT_OF_BOUNDS';
/**
 * Default issue code representing that the numeric value is invalid because it
 * does not obey its schema's `min`/`max` restrictions.
 */
export const NUMBER_OUT_OF_BOUNDS_CODE = 'LF_NUMBER_OUT_OF_BOUNDS';
/**
 * Default issue code representing that the string value is invalid because it
 * does not obey its schema's `minLength`/`maxLength` restrictions.
 */
export const LENGTH_OUT_OF_BOUNDS_CODE = 'LF_LENGTH_OUT_OF_BOUNDS';

/**
 * Type of an issue emitted due to a promise being rejected. Validation issues
 * of this type also contain a `reason` data property containing the error
 * returned by the promise.
 */
export const IS_REJECTED_TYPE = 'isRejected';
/**
 * Type of an issue emitted due to a required value being `null`.
 */
export const IS_REQUIRED_TYPE = 'isRequired';
/**
 * Type of an issue emitted due to a value not being one of its schema's allowed
 * values. Validation issues of this type also contain a `value` data property
 * containing the disallowed value and an `allowedValues` data property
 * containing the schema's allowed values.
 */
export const DISALLOWED_VALUE_TYPE = 'disallowedValue';
/**
 * Type of an issue emitted due to a date value not obeying its schema's
 * `minDate`/`maxDate` bounds. Validation issues of this type also contain a
 * `value` data property containing the date and `minDate` and/or `maxDate` data
 * properties with the schema's defined values.
 */
export const DATE_OUT_OF_BOUNDS_TYPE = 'dateOutOfBounds';
/**
 * Type of an issue emitted due to a collection's size not obeying its schema's
 * `minSize`/`maxSize` bounds. Validation issues of this type also contain a
 * `size` data property with the collection's size and `minSize` and/or
 * `maxSize` data properties with the schema's defined values.
 */
export const SIZE_OUT_OF_BOUNDS_TYPE = 'sizeOutOfBounds';
/**
 * Type of an issue emitted due to a numeric value not obeying its schema's
 * `min`/`max` bounds. Validation issues of this type also contain a `value`
 * data property with the number and `min` and/or `max` data properties with the
 * schema's defined values.
 */
export const NUMBER_OUT_OF_BOUNDS_TYPE = 'numberOutOfBounds';
/**
 * Type of an issue emitted due to a string value's length not obeying its
 * schema's `minLength`/`maxLength` bounds. Validation issues of this type also
 * contain a `length` data property with the value's length and `minLength`
 * and/or `maxLength` data properties with the schema's defined values.
 */
export const LENGTH_OUT_OF_BOUNDS_TYPE = 'lengthOutOfBounds';

/**
 * Validates a given value against its schema (possibly asynchronously).
 * @param schema Schema against which to validate the value.
 * @param path Absolute path of the value. If not provided it is assumed that
 * the value is the root value (path is set to '/').
 * @param value Value to validate.
 * @param state State of the value to validate.
 * @returns Map of validation issues (a map from the path of the value with
 * issues to the list of issues of that value). In case the validation involves
 * anything asynchronous then a promise is returned with said validation issues.
 */
export function validateValue(
  schema: Schema,
  path: string = '/',
  value: any,
  state: ValueState
): ValidationIssuesMap | Promise<ValidationIssuesMap> {
  const { childrenStates } = state;
  const issues = {}; // Issues associated to the value being validated

  // Helper function to push issues obtained from the children of a complex
  // value
  function addComplexIssues(issuesMaps: ValidationIssuesMap[]): void {
    for (const issuesMap of issuesMaps) {
      for (const p of Object.keys(issuesMap)) {
        issues[p] || (issues[p] = []);
        issues[p].push(...issuesMap[p]);
      }
    }
  }

  // Children of computed schemas shouldn't be validated
  const isComputed = isComputedSchema(schema);
  // Values to validate (obtained from the state properties); these values may
  // be promises; each element of the array should have a corresponding
  // validation; if one validation should validate more than one value at once,
  // then we add an array with all values to be passed to the validation
  const validationValues: Array<any | any[]> = [];
  // Validations to perform
  const validations: Array<(val: any) => any> = [];

  // Validate `null` values (no other validation is executed)
  if (isNullableSchema(schema) && value === null) {
    validationValues.push(getStateProperty(schema, state, 'isRequired'));
    validations.push((isRequired) => {
      if (isRequired) {
        addValidationIssue(issues, path, {
          code: schema.isRequiredCode ?? IS_REQUIRED_CODE,
          data: { type: IS_REQUIRED_TYPE },
        });
      }
    });
  } else {
    // Validate allowed values
    validationValues.push(getStateProperty(schema, state, 'allowedValues'));
    validations.push((allowedValues) => {
      if (
        allowedValues !== undefined &&
        !allowedValues.some((allowed) => comparer.structural(value, allowed))
      ) {
        addValidationIssue(issues, path, {
          code: schema.disallowedValueCode ?? DISALLOWED_VALUE_CODE,
          data: { type: DISALLOWED_VALUE_TYPE, value, allowedValues },
        });
      }
    });

    // Validate date
    if (isDateSchema(schema)) {
      validationValues.push([
        getStateProperty(schema, state, 'minDate'),
        getStateProperty(schema, state, 'maxDate'),
      ]);
      validations.push(([minDate, maxDate]) => {
        const code =
          minDate !== undefined && compareDates(value, minDate) < 0
            ? schema.minDateCode ?? DATE_OUT_OF_BOUNDS_CODE
            : maxDate !== undefined && compareDates(value, maxDate) > 0
            ? schema.maxDateCode ?? DATE_OUT_OF_BOUNDS_CODE
            : null;
        if (code != null) {
          addValidationIssue(issues, path, {
            code,
            data: { type: DATE_OUT_OF_BOUNDS_TYPE, value, minDate, maxDate },
          });
        }
      });
    }

    // Validate list or map
    else if (isCollectionSchema(schema)) {
      // Validate list/map/table sizes
      validationValues.push([
        getStateProperty(schema, state, 'minSize'),
        getStateProperty(schema, state, 'maxSize'),
      ]);
      validations.push(([minSize, maxSize]) => {
        const valueSize = isMapSchema(schema) ? value.size : value.length;
        const code =
          minSize !== undefined && valueSize < minSize
            ? schema.minSizeCode ?? SIZE_OUT_OF_BOUNDS_CODE
            : maxSize !== undefined && valueSize > maxSize
            ? schema.maxSizeCode ?? SIZE_OUT_OF_BOUNDS_CODE
            : null;
        if (code != null) {
          addValidationIssue(issues, path, {
            code,
            data: {
              type: SIZE_OUT_OF_BOUNDS_TYPE,
              size: valueSize,
              minSize,
              maxSize,
            },
          });
        }
      });

      if (!isComputed) {
        // Add list elements' issues
        if (isListSchema(schema)) {
          const elementsSchema = schema.elementsSchema;
          if (requiresValidation(elementsSchema)) {
            validationValues.push(
              childrenStates.map((st) =>
                getStateProperty(elementsSchema, st, 'validationIssues')
              )
            );
            validations.push((issuesMaps) => addComplexIssues(issuesMaps));
          }
        }

        // Add map/table values/rows' issues
        else {
          const childrenSchema = isMapSchema(schema)
            ? schema.valuesSchema
            : schema.rowsSchema;
          if (requiresValidation(childrenSchema)) {
            validationValues.push(
              Array.from(childrenStates.values()).map((st: ValueState) =>
                getStateProperty(childrenSchema, st, 'validationIssues')
              )
            );
            validations.push((issuesMaps) => addComplexIssues(issuesMaps));
          }
        }
      }
    }

    // Validate number
    else if (isNumberSchema(schema)) {
      validationValues.push([
        getStateProperty(schema, state, 'min'),
        getStateProperty(schema, state, 'max'),
      ]);
      validations.push(([min, max]) => {
        const code =
          min !== undefined && value < min
            ? schema.minCode ?? NUMBER_OUT_OF_BOUNDS_CODE
            : max !== undefined && value > max
            ? schema.maxCode ?? NUMBER_OUT_OF_BOUNDS_CODE
            : null;
        if (code != null) {
          addValidationIssue(issues, path, {
            code,
            data: { type: NUMBER_OUT_OF_BOUNDS_TYPE, value, min, max },
          });
        }
      });
    }

    // Validate record
    else if (isRecordSchema(schema)) {
      if (!isComputed) {
        validationValues.push(
          Object.keys(schema.fieldsSchemas).map((field) =>
            getStateProperty(
              schema.fieldsSchemas[field],
              childrenStates[field],
              'validationIssues'
            )
          )
        );
        validations.push((issuesMaps) => addComplexIssues(issuesMaps));
      }
    }

    // Validate string
    else if (isStringSchema(schema)) {
      validationValues.push([
        getStateProperty(schema, state, 'minLength'),
        getStateProperty(schema, state, 'maxLength'),
      ]);
      validations.push(([minLength, maxLength]) => {
        const valueLength = value.length;
        const code =
          minLength !== undefined && valueLength < minLength
            ? schema.minLengthCode ?? LENGTH_OUT_OF_BOUNDS_CODE
            : maxLength !== undefined && valueLength > maxLength
            ? schema.maxLengthCode ?? LENGTH_OUT_OF_BOUNDS_CODE
            : null;
        if (code != null) {
          addValidationIssue(issues, path, {
            code,
            data: {
              type: LENGTH_OUT_OF_BOUNDS_TYPE,
              length: valueLength,
              minLength,
              maxLength,
            },
          });
        }
      });
    }

    // Validate tuple
    else if (isTupleSchema(schema)) {
      if (!isComputed) {
        validationValues.push(
          schema.elementsSchemas.map((elSchema, i) =>
            getStateProperty(elSchema, childrenStates[i], 'validationIssues')
          )
        );
        validations.push((issuesMaps) => addComplexIssues(issuesMaps));
      }
    }

    // Add custom `validate` issues
    validationValues.push(getStateProperty(schema, state, 'validate'));
    validations.push((resIssues) => {
      if (resIssues !== undefined) {
        resIssues.forEach((err) => addValidationIssue(issues, path, err));
      }
    });
  }

  // Helper function which returns the validation issues after validating all
  // values
  function issuesFromValidations(values) {
    validations.forEach((validation, i) => validation(values[i]));
    return issues;
  }

  // Helper function to handle rejections from promises, adding the reason for
  // their rejection to the validation issues
  function wrapValidationValue(val: any) {
    return isPromise(val)
      ? val.catch((err) =>
          addValidationIssue(issues, path, {
            code: IS_REJECTED_CODE,
            data: { type: IS_REJECTED_TYPE, reason: err },
          })
        )
      : val;
  }

  // When there is at least one asynchronous validation, then we must return a
  // promise which resolves after all asynchronous validations finish
  if (
    validationValues.some(
      (val) =>
        isPromise(val) || (isArrayLike(val) && val.some((v) => isPromise(v)))
    )
  ) {
    // Wrap all promises so that if they reject, instead of rejecting all
    // promises, a validation issue is added with the reason for their rejection
    return Promise.all(
      validationValues.map((val) =>
        isArrayLike(val)
          ? Promise.all(val.map(wrapValidationValue))
          : wrapValidationValue(val)
      )
    ).then(issuesFromValidations);
  }

  // All validations are synchronous; we can simply return the validation issues
  return issuesFromValidations(validationValues);
}
