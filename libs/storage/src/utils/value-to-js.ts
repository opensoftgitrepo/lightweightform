import { Schema } from '../schema-types';

import {
  isClientOnlySchema,
  isDateSchema,
  isListSchema,
  isMapSchema,
  isRecordSchema,
  isTableSchema,
  isTupleSchema,
} from './schema-checker';

/**
 * Transforms a given value represented by a given schema into its JS
 * representation omitting cliend-side values in the process.
 * @param schema Schema of the value to transform into JSON.
 * @param value Value to transform in its JSON representation.
 * @returns JSON representation of given value.
 */
export function valueToJS(schema: Schema, value: any): any {
  if (value === null) {
    return null;
  }
  if (isDateSchema(schema)) {
    return new Date(value.getTime());
  }
  if (isListSchema(schema) || isTableSchema(schema)) {
    const childrenSchema = isListSchema(schema)
      ? schema.elementsSchema
      : schema.rowsSchema;
    return value.map((el) => valueToJS(childrenSchema, el));
  }
  if (isMapSchema(schema)) {
    return new Map(
      Array.from(value.entries()).map(
        ([key, val]) =>
          [key, valueToJS(schema.valuesSchema, val)] as [string, any]
      )
    );
  }
  if (isRecordSchema(schema)) {
    return Object.keys(schema.fieldsSchemas).reduce((record, field) => {
      const fieldSchema = schema.fieldsSchemas[field];
      if (!isClientOnlySchema(fieldSchema)) {
        record[field] = valueToJS(fieldSchema, value[field]);
      }
      return record;
    }, {});
  }
  if (isTupleSchema(schema)) {
    // Client-only elements become `undefined` in the resulting tuple
    return schema.elementsSchemas.map((elementSchema, i) => {
      if (!isClientOnlySchema(elementSchema)) {
        return valueToJS(elementSchema, value[i]);
      }
    });
  }
  return value;
}
