import { Schema } from '../schema-types';
import { Storage } from '../storage';

import { Identifier, splitLastId } from './path-utils';

/**
 * A function which receives a read-only storage with its current working path
 * set to the path of the schema to which this function belongs.
 * @param R Return type of the function.
 */
export type ContextFunction<R = any> = (
  ctx: Storage
) => R | undefined | PromiseLike<R | undefined>;

/**
 * A function which receives a read-only storage with its current working path
 * set to the path of the parent of the schema to which this function belongs.
 * This function further receives the identifier which identifies the schema in
 * its parent. Functions of this type are not allowed on root schemas.
 * @param R Return type of the function.
 */
export type ParentContextFunction<R = any> = (
  ctx: Storage,
  id: Identifier
) => R | undefined | PromiseLike<R | undefined>;

/**
 * Fetches the "actual" value of a given schema property which allows a "context
 * function" as its value.
 * @param storage Instance of the storage which contains the schema.
 * @param prop Property of the schema.
 * @param schema Schema from which to fetch the "actual" property value.
 * @param path Absolute path of the schema.
 * @returns "Actual" value of the schema's property.
 */
export function contextProperty(
  storage: Storage,
  prop: string,
  schema: Schema,
  path: string
): any {
  return typeof schema[prop] === 'function'
    ? schema[prop](storage.relativeStorage(path, true))
    : schema[prop];
}

/**
 * Fetches the "actual" value of a given schema property which allows a "parent
 * context function" as its value.
 * @param storage Instance of the storage which contains the schema.
 * @param prop Property of the schema.
 * @param schema Schema from which to fetch the "actual" property value.
 * @param path Absolute path of the schema.
 * @returns "Actual" value of the schema's property.
 */
export function parentContextProperty(
  storage: Storage,
  prop: string,
  schema: Schema,
  path: string
): any {
  if (typeof schema[prop] !== 'function') {
    return schema[prop];
  }
  const [parentPath, id] = splitLastId(path);
  return schema[prop](storage.relativeStorage(parentPath, true), id);
}
