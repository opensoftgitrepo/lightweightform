import {
  action,
  comparer,
  computed,
  IObservableValue,
  IValueDidChange,
  Lambda,
  makeObservable,
  observable,
  observe,
} from 'mobx';
import {
  FULFILLED,
  IPromiseBasedObservable,
  isPromiseBasedObservable,
  PENDING,
  PromiseState,
} from 'mobx-utils';

import { Schema } from './schema-types';
import { childSchema, childState, childValue } from './utils/child-utils';
import {
  invalidSchemaTypeError,
  pathError,
  ValidationIssues,
  ValidationIssuesMap,
} from './utils/error-utils';
import { getRelevantIssuesFromMap } from './utils/issues-map-utils';
import {
  appendToPath,
  Identifier,
  PATH_ID_PLACEHOLDER,
  resolvePathToArray,
} from './utils/path-utils';
import { fulfilledValue, promisify } from './utils/promise-utils';
import {
  isClientOnlySchema,
  isCollectionSchema,
  isComputedSchema,
  isListSchema,
  isMapSchema,
  isNullableSchema,
  isRecordSchema,
  isTableSchema,
  isTupleSchema,
} from './utils/schema-checker';
import { validateSchema } from './utils/schema-validation';
import { ValueState } from './utils/state-creation';
import {
  getStateProperty,
  hasStateProperty,
  recomputeStateProperties,
  setStateProperty,
  SetStatePropertyOptions,
} from './utils/state-property-utils';
import {
  $lfRowId,
  deleteRowFromIndices,
  deleteRowState,
  getRowId,
  nextRowId,
} from './utils/table-utils';
import {
  typeIsValid,
  TypeValidationOptions,
  validateType,
} from './utils/type-validation';
import {
  initializeValue,
  MergeValueOptions,
  mergeValues,
} from './utils/value-creation';
import { valueToJS } from './utils/value-to-js';
import { validateValue } from './utils/value-validation';

/**
 * Options to be passed when asking for `pathInfo`.
 */
export interface PathInfoOptions {
  /**
   * Whether to navigate through the value-tree.
   */
  fetchValues?: boolean;
  /**
   * Whether to navigate through the state-tree.
   */
  fetchStates?: boolean;
  /**
   * Whether to allow a path that would correspond to a new value.
   */
  allowAccessToNewValue?: boolean;
}

/**
 * Information about part of a path.
 */
export interface PathPartInfo {
  /**
   * Absolute path of the part.
   */
  path: string;
  /**
   * Identifier of the part (`null` when the `path` is root).
   */
  id: Identifier | null;
  /**
   * Schema of the part.
   */
  schema: Schema;
  /**
   * Whether the schema at this part represents a computed value.
   */
  isComputed: boolean;
  /**
   * Whether the schema at this part represents a client-only value.
   */
  isClientOnly: boolean;
  /**
   * Value of the part (provided only when `fetchValues` is set to `true` when
   * calling `pathInfo`).
   */
  value?: any;
  /**
   * State of the part (provided only when `fetchStates` is set to `true` when
   * calling `pathInfo`).
   */
  state?: ValueState;
}

/**
 * Mode of validation available to the storage.
 */
export enum ValidationMode {
  /**
   * Automatic validation: no call to `validate` is required; validation issues
   * are computed/tracked when `validationIssues` are requested.
   */
  AUTO = 'auto',
  /**
   * Manual validation: a call to `validate` is required; validation issues are
   * computed/tracked based on the result of `validate`.
   */
  MANUAL = 'manual',
}

/**
 * Type of the status of the validation of a value. It is never `'rejected'`
 * since rejected promises in validation properties end up being added as
 * validation issues with their reason for rejection.
 */
export type ValidationStatus = typeof PENDING | typeof FULFILLED;

/**
 * Class responsible for holding the schema, value, and value-related state of
 * an application. All value and state manipulations should pass through an
 * instance of this class in order to guarantee correctness in terms of types
 * and (MobX) observable behaviour.
 */
export class Storage {
  /**
   * Schema of the whole storage.
   */
  protected _schema: Schema;

  /**
   * Value of the whole storage. We make it a "boxed observable" in order to use
   * it across multiple (relative) storages (when the root value changes in one,
   * it should change across all others as well). This value is set when the
   * schema is set.
   */
  protected _value: IObservableValue<any>;

  /**
   * State of each value. We make it a "boxed observable" in order to use it
   * across multiple (relative) storages (when the root state changes in one, it
   * should change across all others as well). This value is set when the schema
   * is set.
   */
  protected _state: IObservableValue<ValueState>;

  /**
   * Path on which work is currently occurring (the current working path). All
   * paths will be relative to this path unless they're absolute. Set to `'/'`
   * for the "root" storage.
   */
  protected _currentPath: string;

  /**
   * Whether the storage instance is in read-only mode: in which case no data
   * manipulation is allowed. Set to `false` for the "root" storage.
   */
  protected _readOnly: boolean;

  /**
   * Currently active validation mode.
   */
  protected _validationMode: IObservableValue<ValidationMode>;

  /**
   * Validation issues set by a manual `validate` call.
   */
  private _manualValidationIssues: IObservableValue<
    IPromiseBasedObservable<ValidationIssuesMap> | undefined
  >;

  /**
   * Creates a new "root" storage instance given its schema.
   * @param schema Schema of the new instance. This schema is validated against
   * a set of rules.
   * @param validationMode Initial validation mode.
   */
  constructor(
    schema: Schema,
    validationMode: ValidationMode = ValidationMode.AUTO
  ) {
    makeObservable(this);

    // When called internally (via `relativeStorage`) `schema` will be
    // `undefined`
    if (schema !== undefined) {
      if (process.env.NODE_ENV !== 'production') {
        // Check for errors in the schema
        validateSchema(schema);
      }
      this._currentPath = '/';
      this._readOnly = false;
      action('initStorage', () => {
        this._schema = schema;
        this._value = observable.box(undefined, {
          name: 'valueBox',
          deep: false,
        });
        this._state = observable.box(undefined!, {
          name: 'stateBox',
          deep: false,
        });
        this._validationMode = observable.box(validationMode, {
          name: 'validationModeBox',
        });
        this._manualValidationIssues = observable.box(undefined, {
          name: 'manualValidationIssuesBox',
        });
        initializeValue(
          this,
          schema,
          '/',
          null,
          undefined,
          undefined,
          this._value,
          this._state
        );
      })();
    }
  }

  /**
   * Whether this storage instance is read-only, i.e. if it disallows data
   * manipulation.
   */
  public get isReadOnly(): boolean {
    return this._readOnly;
  }

  /**
   * Path on which work is currently occurring (the current working path). All
   * given paths will be relative to this path unless they're absolute.
   */
  public get currentPath(): string {
    return this._currentPath;
  }

  /**
   * Path on which work is currently occurring (the current working path) but in
   * array notation. E.g. if the current working path is `'/'` then this
   * property will be `[]`; if the current path is `'/a/b'`, then this property
   * will be `['a', 'b']`.
   */
  @computed
  public get currentPathAsArray(): Identifier[] {
    return this.resolvePathToArray('.');
  }

  /**
   * Identifier of the value of the current working path in relation to its
   * parent value. When the current working path is `'/'`, the current
   * identifier is `null`. E.g. if the current working path is `'/a/b/c'` then
   * the current identifier is `'c'`.
   */
  @computed
  public get currentId(): Identifier | null {
    return this.id();
  }

  /**
   * Validation mode currently in use by the storage.
   */
  @computed
  public get validationMode(): ValidationMode {
    return this._validationMode.get();
  }

  public set validationMode(validationMode: ValidationMode) {
    if (validationMode === this._validationMode.get()) {
      return;
    }
    this._validationMode.set(validationMode);

    // Clear unneeded manual validation issues when switching to automatic
    // validations
    if (validationMode === ValidationMode.AUTO) {
      this._manualValidationIssues.set(undefined);
    }
  }

  /**
   * Low-level method to get all available information about the given path in a
   * single pass over the schema/value/state tree. This includes, for each part
   * of a path: its schema; absolute path; identifier; whether it is computed;
   * or client-only; and, if requested, its value; and state. Throws an error if
   * the provided path is invalid or, when the values or state are requested,
   * when there exists no value/state for the given path.
   * @param relativePath Path from which to fetch information (relative to the
   * current working path). Defaults to `'.'`.
   * @param options Whether to pass through the value or value state trees.
   * @returns List of information about each part of the path, from the root to
   * the most specific part.
   */
  public pathInfo(
    relativePath: string = '.',
    options: PathInfoOptions = {}
  ): PathPartInfo[] {
    const {
      fetchValues = false,
      fetchStates = false,
      allowAccessToNewValue = false,
    } = options;
    const rootInfo: any = {};
    rootInfo.schema = this._schema;
    rootInfo.path = '/';
    rootInfo.id = null;
    rootInfo.isComputed = isComputedSchema(rootInfo.schema);
    rootInfo.isClientOnly = isClientOnlySchema(rootInfo.schema);
    if (fetchValues) {
      rootInfo.value = this._value.get();
    }
    if (fetchStates) {
      rootInfo.state = this._state.get();
    }
    return resolvePathToArray(this._currentPath, relativePath).reduce(
      (pathInfo, id, i, pathArray) => {
        const prev: PathPartInfo = pathInfo[i];
        const partInfo: any = {};
        partInfo.schema = childSchema(prev.schema, prev.path, id);
        partInfo.path = appendToPath(prev.path, id);
        partInfo.id =
          (isListSchema(prev.schema) ||
            isTableSchema(prev.schema) ||
            isTupleSchema(prev.schema)) &&
          id !== PATH_ID_PLACEHOLDER
            ? +id
            : id;
        partInfo.isComputed =
          prev.isComputed || isComputedSchema(partInfo.schema);
        partInfo.isClientOnly =
          prev.isClientOnly || isClientOnlySchema(partInfo.schema);
        if (fetchValues) {
          partInfo.value = childValue(
            prev.schema,
            prev.path,
            prev.value,
            id,
            allowAccessToNewValue && i === pathArray.length - 1
          );
        }
        if (fetchStates) {
          partInfo.state = childState(
            prev.schema,
            prev.path,
            prev.state!,
            prev.isComputed,
            id,
            allowAccessToNewValue && i === pathArray.length - 1
          );
        }
        pathInfo.push(partInfo);
        return pathInfo;
      },
      [rootInfo]
    );
  }

  /**
   * Throws an error if the provided path does not correspond to a valid schema
   * path. Idiomatic way of calling `pathInfo(relativePath)` and ignoring the
   * returned value.
   * @param relativePath Path to validate (relative to the current working
   * path). Defaults to `'.'`.
   */
  public validatePath(relativePath: string = '.'): void {
    this.pathInfo(relativePath);
  }

  /**
   * Resolves the given path relative to the current working path: returns an
   * absolute path with no references to `'.'` and `'..'`.
   * @param relativePath Path to resolve against the current working path.
   * @returns Normalised absolute path.
   */
  public resolvePath(relativePath: string): string {
    return this.lastPathPartInfo(relativePath).path;
  }

  /**
   * Resolves the given path relative to the current working path and returns
   * the resulting path in array notation. This methods resolves references to
   * `'.'` and `'..'`.
   * @param relativePath Path to resolve against the current working path.
   * @returns Normalised absolute path in array notation.
   */
  public resolvePathToArray(relativePath: string): Identifier[] {
    return this.pathInfo(relativePath)
      .slice(1)
      .map((partInfo) => partInfo.id!);
  }

  /**
   * Returns the identifier of the schema associated with a given path in
   * relation to its parent schema. When the provided path corresponds to `'/'`,
   * the identifier is `null`. E.g. the identifier for the path `'/a/b/c'` is
   * `'c'`.
   * @param relativePath Path for which to fetch the identifier (relative to the
   * current working path). Defaults to `'.'`.
   * @returns Identifier of the associated path.
   */
  public id(relativePath: string = '.'): Identifier | null {
    // `pathInfo` is used in order to get the appropriate type of identifier
    // from the path (e.g. children of lists/tuples have `number` ids)
    return this.lastPathPartInfo(relativePath).id;
  }

  /**
   * Checks whether the given path is valid according to the storage schema.
   * Allows paths with "placeholders" for list, map, and table schemas.
   * @param relativePath Path to check if valid (relative to the current working
   * path). Defaults to `'.'`.
   * @returns Whether the given path is valid according to the storage schema.
   */
  public hasPath(relativePath: string = '.'): boolean {
    try {
      this.validatePath(relativePath);
      return true;
    } catch (err) {
      return false;
    }
  }

  /**
   * Creates a new storage instance from the current one (with the same schema,
   * value, and state) but with a (possibly) different current working path.
   * This new instance may be made read-only.
   * @param relativePath Path (relative to the current working path) to be used
   * as the current working path of the new instance. Defaults to `'.'`.
   * @param readOnly Whether the new instance should disallow data manipulation.
   * It is not possible to create a new writable storage from a read-only one
   * (attempts to do so are ignored and the returned storage will be in
   * read-only mode).
   * @returns A new storage instance created from the current storage with a
   * possibly different current working path.
   */
  public relativeStorage(
    relativePath: string = '.',
    readOnly: boolean = false
  ): Storage {
    // Throws for invalid paths
    const resolvedPath = this.resolvePath(relativePath);
    // We use `this.constructor` instead of `Storage` so that this method may
    // still be called on classes extending `Storage`
    const relativeStorage = new (this.constructor as any)();
    relativeStorage._currentPath = resolvedPath;
    relativeStorage._schema = this._schema;
    relativeStorage._value = this._value;
    relativeStorage._state = this._state;
    relativeStorage._validationMode = this._validationMode;
    relativeStorage._manualValidationIssues = this._manualValidationIssues;
    // A writable storage cannot be created from a read-only one
    relativeStorage._readOnly = this._readOnly || readOnly;
    return relativeStorage;
  }

  // Operations on all schemas/values ==========================================

  /**
   * Gets the schema at the given path.
   * @param relativePath Path from which to fetch schema (relative to the
   * current working path). Defaults to `'.'`.
   * @returns Schema at the given path.
   */
  public schema(relativePath: string = '.'): Schema {
    return this.lastPathPartInfo(relativePath).schema;
  }

  /**
   * Whether the given path is associated to a schema that represents computed
   * values. A computed value is one whose schema or any of its parent schemas
   * represents a computed value (defines `computedValue`).
   * @param relativePath Path on which to check if the schema represents
   * computed values (relative to the current working path). Defaults to `'.'`.
   * @returns Whether the schema at the given path represents a computed value.
   */
  public isComputed(relativePath: string = '.'): boolean {
    return this.lastPathPartInfo(relativePath).isComputed;
  }

  /**
   * Whether the given path is associated to a schema that is client-only (if
   * the values that it represents are hidden from JS value representations).
   * @param relativePath Path on which to check if the schema is client-only
   * (relative to the current working path). Defaults to `'.'`.
   * @returns Whether the schema at the given path is client-only.
   */
  public isClientOnly(relativePath: string = '.'): boolean {
    return this.lastPathPartInfo(relativePath).isClientOnly;
  }

  /**
   * Whether the given path is associated to a schema that is nullable (i.e. a
   * schema where `null` is a valid type of value).
   * @param relativePath Path on which to check if the schema is nullable
   * (relative to the current working path). Defaults to `'.'`.
   * @returns Whether the schema at the given path is nullable.
   */
  public isNullable(relativePath: string = '.'): boolean {
    return isNullableSchema(this.schema(relativePath));
  }

  /**
   * Whether the schema associated to the given path represents a value that is
   * removable (i.e. which is a child of a non-computed collection). Note that
   * this method does not check whether a value exists at the given path, only
   * whether it would be removable if it did exist.
   * @param relativePath Path on which to check if the schema represents a
   * removable value (relative to the current working path). Defaults to `'.'`.
   * @returns Whether the schema at the provided path represents a removable
   * value.
   */
  public isRemovable(relativePath: string = '.'): boolean {
    const [{ path }, { schema: parentSchema, isComputed: parentIsComputed }] =
      this.lastPathPartAndParentInfo(relativePath);
    return (
      path !== '/' && !parentIsComputed && isCollectionSchema(parentSchema)
    );
  }

  /**
   * Checks whether there exists a value at the given path.
   * @param relativePath Path where to check for the existence of a value
   * (relative to the current working path). Defaults to `'.'`.
   * @returns Whether a value exists at the given path.
   */
  public has(relativePath: string = '.'): boolean {
    if (process.env.NODE_ENV !== 'production') {
      // Verify that the path corresponds to a valid schema (throw otherwise)
      this.validatePath(relativePath);
    }
    try {
      // Attempt to access a value at the given path, if an error is thrown, no
      // value exists
      this.pathInfo(relativePath, { fetchValues: true });
      return true;
    } catch (err) {
      return false;
    }
  }

  /**
   * Gets the value at the given path.
   * @param relativePath Path from which to fetch value (relative to the current
   * working path). Defaults to `'.'`.
   * @returns Value of given path.
   */
  public get(relativePath: string = '.'): any {
    return this.lastPathPartInfo(relativePath, { fetchValues: true }).value;
  }

  /**
   * Gets the value at the given path as a simple (non-MobX) JS value. This
   * method may be called on client-only values, however, client-only values
   * that are descendants of the requested value are omitted.
   * @param relativePath Path from which to fetch value as JS (relative to the
   * current working path). Defaults to `'.'`.
   * @returns Value of given path as a simple JS value.
   */
  public getAsJS(relativePath: string = '.'): any {
    const { schema, value } = this.lastPathPartInfo(relativePath, {
      fetchValues: true,
    });
    return valueToJS(schema, value);
  }

  /**
   * Sets the given value at the given path.
   * @param relativePath Path on which to set the value (relative to the current
   * working path). Defaults to `'.'`.
   * @param value Value to set at given path (will be typechecked against its
   * respective schema).
   * @param options Options used for setting the value.
   */
  @action
  public set(
    relativePath: string = '.',
    value: any,
    options?: MergeValueOptions
  ): void {
    this.throwIfReadOnly();
    const [
      { schema, path, id, isComputed, value: currentValue, state },
      { schema: parentSchema, value: parentValue, state: parentState },
    ] = this.lastPathPartAndParentInfo(relativePath, {
      fetchValues: true,
      fetchStates: true,
      allowAccessToNewValue: true,
    });
    this.throwIfComputed(path, isComputed);
    if (process.env.NODE_ENV !== 'production') {
      validateType(value, schema, path);
    }
    mergeValues(
      this,
      schema,
      path,
      id,
      currentValue,
      state!,
      value,
      parentSchema,
      parentValue,
      parentState!,
      options
    );
  }

  /**
   * Resets the value at the given path back to its initial value (equivalent to
   * setting the value to `undefined` with the `handleUndefined` option set to
   * `'reset'`).
   * @param relativePath Path on which to reset the value (relative to the
   * current working path). Defaults to `'.'`.
   */
  @action
  public reset(relativePath: string = '.'): void {
    this.set(relativePath, undefined, { handleUndefined: 'reset' });
  }

  /**
   * Low-level method to return the state of the value at the given path (it is
   * advisable to use `getStateProperty` and variants instead when appropriate).
   * @param relativePath Path of the value from which to fetch the state
   * (relative to the current working path). Defaults to `'.'`.
   * @returns State of the value at the provided path.
   */
  public getState(relativePath: string = '.'): ValueState {
    return this.lastPathPartInfo(relativePath, { fetchStates: true }).state!;
  }

  /**
   * Whether the value at the given path has a provided state property.
   * @param relativePath Path of the value on which to check for the existence
   * of a given state property (relative to the current working path). Defaults
   * to `'.'`.
   * @param prop Name of the property to check for existence.
   * @returns Whether the property exists in the value's state.
   */
  public hasStateProperty(relativePath: string = '.', prop: string): boolean {
    const { schema, state } = this.lastPathPartInfo(relativePath, {
      fetchStates: true,
    });
    return hasStateProperty(schema, state!, prop);
  }

  /**
   * Gets the value of a state property of the value at the given path. If the
   * state property is computed, it requests the value of the computation. If
   * the state property is asynchronous it returns the value returned by the
   * promise or `undefined` if the promise hasn't resolved.
   * @param relativePath Path from which to fetch state property (relative to
   * the current working path). Defaults to `'.'`.
   * @param prop State property to fetch.
   * @returns State property associated to value at the provided path.
   */
  public getStateProperty(relativePath: string = '.', prop: string): any {
    const { schema, state } = this.lastPathPartInfo(relativePath, {
      fetchStates: true,
    });
    return fulfilledValue(getStateProperty(schema, state!, prop));
  }

  /**
   * Sets the value of a state property for the value at the given path. When
   * passed a function, the state property becomes computed, with its value
   * being the result of calling such function with the storage (relative to the
   * provided path) as argument. Asynchronous functions are further made
   * observable. Supports options for propagating up or down the state property
   * (useful for implementing states such as `isDirty` in user applications).
   * @param relativePath Path on which to set state property (relative to the
   * current working path). Defaults to `'.'`.
   * @param prop State property to set.
   * @param value Value to set on the state property.
   * @param options Options used for setting the state property.
   */
  @action
  public setStateProperty(
    relativePath: string = '.',
    prop: string,
    value: any,
    options?: SetStatePropertyOptions
  ): void {
    this.throwIfReadOnly();
    setStateProperty(this, relativePath, prop, value, options);
  }

  /**
   * Status of a state property of the value at the given path: one of
   * `'pending'`, `'fulfilled'`, or `'rejected'`. The status will be
   * `'fulfilled'` when the state property is not asynchronous.
   * @param relativePath Path of the value from which to check the status of the
   * state property (relative to the current working path). Defaults to `'.'`.
   * @param prop State property with the status to check.
   * @returns The validation status of a given state property (`'pending'`,
   * `'fulfilled'`, or `'rejected'`).
   */
  public statePropertyStatus(
    relativePath: string = '.',
    prop: string
  ): PromiseState {
    const [{ schema, state }, { isComputed: parentIsComputed }] =
      this.lastPathPartAndParentInfo(relativePath, { fetchStates: true });
    if (parentIsComputed) {
      return FULFILLED;
    }
    const value = getStateProperty(schema, state!, prop);
    return isPromiseBasedObservable(value)
      ? (value.state as ValidationStatus)
      : FULFILLED;
  }

  /**
   * Forces the recomputation of all asynchronous state properties of the value
   * at the given path whose evaluation resulted in a rejected promise. If
   * `recomputeAll` is `true`, then it forces the recomputation of all
   * asynchronous state properties (even if they weren't rejected).
   * @param relativePath Path from which to recomputed asynchronous state
   * properties (relative to the current working path). Defaults to `'.'`.
   * @param recomputeAll Whether to recompute all asynchronous state properties.
   * Defaults to `false` (only rejected asynchronous state properties are
   * recomputed).
   */
  // TODO: Add `recomputeStateProperty` with options `cascade` and `force` for
  // more control; add same options to this method
  @action
  public recomputeStateProperties(
    relativePath: string = '.',
    recomputeAll: boolean = false
  ): void {
    this.throwIfReadOnly();
    const { schema, state } = this.lastPathPartInfo(relativePath, {
      fetchStates: true,
    });
    recomputeStateProperties(schema, state!, recomputeAll);
  }

  /**
   * Checks whether a given value has a valid type in respect to the schema at
   * the given path.
   * @param relativePath Path of value with type to check (relative to the
   * current working path). Defaults to `'.'`.
   * @param value Value to check whether type is valid.
   * @param issuesMap Object provided by the callee that is filled by the method
   * with information depicting why a value has an invalid type. An empty object
   * should be provided unless the reason for an invalid type is not needed.
   * This object will be filled mapping invalid values to a list of their
   * validation issues.
   * @param options Options used to validate the type of the value.
   * @returns Whether the value has a valid type according to the schema.
   */
  public typeIsValid(
    relativePath: string = '.',
    value: any,
    issuesMap?: ValidationIssuesMap,
    options?: TypeValidationOptions
  ): boolean {
    const { schema, path } = this.lastPathPartInfo(relativePath);
    return typeIsValid(value, schema, issuesMap, path, options);
  }

  /**
   * Manually triggers the validation of the value stored in the storage when
   * the validation mode is set to "manual". This method does nothing when the
   * validation mode is set to "auto".
   */
  @action
  public validate(): void {
    if (this.validationMode === ValidationMode.AUTO) {
      if (process.env.NODE_ENV !== 'production') {
        console.warn('Called `validate` with `validationMode` set to "auto".');
      }
      return;
    }

    const { schema, value, state } = this.lastPathPartInfo('/', {
      fetchValues: true,
      fetchStates: true,
    });
    this._manualValidationIssues.set(
      promisify(validateValue(schema, '/', value, state!))
    );
  }

  /**
   * Returns validation issues for the value at the given path. If the
   * validation is asynchronous and still pending, `undefined` is returned.
   * Children of computed values never have validation issues, even if their
   * parent computed value has issues and even though their state is shared with
   * their parent computed value.
   * @param relativePath Path of the value from which to fetch the validation
   * issues (relative to the current working path). Defaults to `'.'`.
   * @returns Validation issues associated with the given path or `undefined`
   * when the validation is in pending state.
   */
  public validationIssues(
    relativePath: string = '.'
  ): ValidationIssuesMap | undefined {
    if (this.validationMode === ValidationMode.AUTO) {
      const [{ schema, state }, { isComputed: parentIsComputed }] =
        this.lastPathPartAndParentInfo(relativePath, { fetchStates: true });
      return parentIsComputed
        ? {}
        : fulfilledValue(getStateProperty(schema, state!, 'validationIssues'));
    }

    const issuesMap = fulfilledValue(this._manualValidationIssues.get());
    return (
      issuesMap &&
      getRelevantIssuesFromMap(this.resolvePath(relativePath), issuesMap)
    );
  }

  /**
   * Whether the value at the given path has any validation issue.
   * @param relativePath Path on which to check if the value contains a
   * validation issue (relative to the current working path). Defaults to `'.'`.
   * @returns Whether the value at the given path contains at least one
   * validation issue.
   */
  public hasIssues(relativePath: string = '.'): boolean | undefined {
    const issuesMap = this.validationIssues(relativePath);
    return issuesMap && Object.keys(issuesMap).length !== 0;
  }

  /**
   * Status of the value validation at the given path: one of `'pending'` or
   * `'fulfilled'`. Note that the validation status is never "rejected"; in case
   * of rejected promises in validation properties, the rejection reason is
   * added as a validation issue. Children of computed values always have their
   * validation status set to `'fulfilled'`, even if their parent computed
   * value's status is in no such state. The status will be `'fulfilled'` when
   * no validation is asynchronous.
   * @param relativePath Path of the value from which to check the validation
   * status (relative to the current working path). Defaults to `'.'`.
   * @returns The validation status of a given path (`'pending'` or
   * `'fulfilled'`).
   */
  public validationStatus(relativePath: string = '.'): ValidationStatus {
    if (this.validationMode === ValidationMode.AUTO) {
      const [{ schema, state }, { isComputed: parentIsComputed }] =
        this.lastPathPartAndParentInfo(relativePath, { fetchStates: true });
      if (parentIsComputed) {
        return FULFILLED;
      }
      const validationIssues = getStateProperty(
        schema,
        state!,
        'validationIssues'
      );
      return isPromiseBasedObservable(validationIssues)
        ? (validationIssues.state as ValidationStatus)
        : FULFILLED;
    }

    const issuesMap = this._manualValidationIssues.get();
    return this.resolvePath(relativePath) === '/' &&
      isPromiseBasedObservable(issuesMap)
      ? (issuesMap.state as ValidationStatus)
      : FULFILLED;
  }

  /**
   * Returns local validation issues for the value at the given path (validation
   * issues that belong to the value at the given path and not to one of its
   * children). If the validation is asynchronous and still pending, `undefined`
   * is returned.
   * @param relativePath Path of the value from which to fetch the local
   * validation issues (relative to the current working path). Defaults to
   * `'.'`.
   * @returns Validation issues local to the given path (ignoring children
   * issues) or `undefined` when the validation is in pending state.
   */
  public localValidationIssues(
    relativePath: string = '.'
  ): ValidationIssues | undefined {
    const issuesMap = this.validationIssues(relativePath);
    return issuesMap && (issuesMap[this.resolvePath(relativePath)] || []);
  }

  /**
   * Observe the value associated to the given path. The observation is **not**
   * deep (if the path corresponds to a record value, changes to a field of said
   * record do **not** trigger the listener). The provided path must correspond
   * to a valid schema; however, the value may not exists at a given time, in
   * which case the `listener` will be fired with a `newValue` of `undefined`.
   * @param relativePath Path of the value to observe (relative to the current
   * working path). Defaults to `'.'`.
   * @param listener Listener function that is called whenever the observed
   * value changes. See [MobX Observe Event
   * Overview](https://mobx.js.org/refguide/observe.html#event-overview) for
   * more details.
   * @param listener.change Object representing the observed change.
   * @param fireImmediately Whether to fire the listener function immediately
   * (defaults to `false`, meaning that the listener will only be called when
   * the first change occurs).
   * @returns Disposer function that should be called to stop listening to the
   * value at the given path.
   */
  public observe(
    relativePath: string = '.',
    listener: (change: IValueDidChange<any>) => void,
    fireImmediately: boolean = false
  ): Lambda {
    if (process.env.NODE_ENV !== 'production') {
      // Verify that the path corresponds to a valid schema (throw otherwise)
      this.validatePath(relativePath);
    }
    return observe(
      computed(
        () => {
          // The path may not correspond to an existing value, in which case we
          // return `undefined`
          try {
            return this.get(relativePath);
          } catch (err) {
            return undefined;
          }
        },
        {
          name: `storageObserver(${this.resolvePath(relativePath)})`,
          equals: comparer.structural,
        }
      ),
      listener,
      fireImmediately
    );
  }

  // Operations on nullable schemas/values =====================================

  /**
   * Initialises the nullable value at the given path. The value is set to the
   * initial value of the schema as if said schema wasn't nullable.
   * @param relativePath Path on which to initialise the value (relative to the
   * current working path). Defaults to `'.'`.
   */
  @action
  public initialize(relativePath: string = '.'): void {
    this.throwIfReadOnly();
    const [
      { schema, path, id, value, state, isComputed },
      { schema: parentSchema, value: parentValue, state: parentState },
    ] = this.lastPathPartAndParentInfo(relativePath, {
      fetchValues: true,
      fetchStates: true,
    });
    this.throwIfComputed(path, isComputed);
    this.throwIfNotNullable(path, schema);
    if (value !== null) {
      throw pathError(path, 'Value is already initialised');
    }
    // Use the schema's initial value if the schema wasn't nullable
    mergeValues(
      this,
      { ...schema, isNullable: false },
      path,
      id,
      null,
      state!,
      undefined,
      parentSchema,
      parentValue,
      parentState!,
      { handleUndefined: 'reset' }
    );
  }

  // Operations on complex schemas/values ======================================

  /**
   * Returns the list of children identifiers of the complex value (list, map,
   * record, table, or tuple) at the given path.
   * @param relativePath Path of the value from which to fetch the children
   * identifiers. Defaults to `'.'`.
   * @returns List of children identifiers of the value at the given path.
   */
  public childrenIds(relativePath: string = '.'): Identifier[] {
    const { schema, path, value } = this.lastPathPartInfo(relativePath, {
      fetchValues: true,
    });
    this.throwIfInvalidSchemaType(path, schema, [
      'list',
      'map',
      'record',
      'table',
      'tuple',
    ]);
    this.throwIfNull(path, value);
    // Return list and tuple children ids as numbers; for records and tuples
    // fetch the ids from the schemas since the values may contains computed
    // properties which makes said properties non-enumerable
    return isListSchema(schema)
      ? value.map((_, i) => i) // `Object.keys` doesn't work on MobX arrays
      : isMapSchema(schema)
      ? Array.from(value.keys())
      : isRecordSchema(schema)
      ? Object.keys(schema.fieldsSchemas)
      : isTableSchema(schema)
      ? value.map((row) => getRowId(row))
      : Object.keys((schema as any).elementsSchemas).map((i) => +i);
  }

  // Operations on schemas/values that are part of collections =================

  /**
   * Removes the value at the given path from its parent list, map, or table.
   * @param relativePath Path of value to remove (relative to the current
   * working path). Defaults to `'.'`.
   */
  @action
  public remove(relativePath: string = '.'): void {
    this.throwIfReadOnly();
    const [
      { path, id, value },
      {
        schema: parentSchema,
        path: parentPath,
        value: parentValue,
        isComputed: parentIsComputed,
      },
    ] = this.lastPathPartAndParentInfo(relativePath, { fetchValues: true });
    if (path === '/' || parentIsComputed || !isCollectionSchema(parentSchema)) {
      throw pathError(path, 'Value is not removable');
    }
    // Remove the value from its parent collection
    if (isMapSchema(parentSchema)) {
      this.delete(parentPath, id as string);
    } else if (isListSchema(parentSchema)) {
      this.filter(parentPath, (_, i) => i !== id);
    } else {
      this.splice(parentPath, parentValue.indexOf(value), 1);
    }
  }

  // Operations on list/map/table schemas/values ===============================

  /**
   * Size of the list, map, or table at the given path.
   * @param relativePath Path on which to inspect the size of the list, map, or
   * table (relative to the current working path). Defaults to `'.'`.
   * @returns The size of the list, map, or table at the provided path.
   */
  public size(relativePath: string = '.'): number {
    const { schema, path, value } = this.lastPathPartInfo(relativePath, {
      fetchValues: true,
    });
    this.throwIfInvalidSchemaType(path, schema, ['list', 'map', 'table']);
    this.throwIfNull(path, value);
    return isMapSchema(schema) ? value.size : value.length;
  }

  /**
   * Clears the list, map, or table at the given path, i.e. removes all elements
   * from a list, all entries from a map, or all rows from a table.
   * @param relativePath Path of the list, map, or table to clean (relative to
   * the current working path). Defaults to `'.'`.
   */
  @action
  public clear(relativePath: string = '.'): void {
    this.throwIfReadOnly();
    const { schema, path, value, state, isComputed } = this.lastPathPartInfo(
      relativePath,
      {
        fetchValues: true,
        fetchStates: true,
      }
    );
    this.throwIfComputed(path, isComputed);
    this.throwIfInvalidSchemaType(path, schema, ['list', 'map', 'table']);
    this.throwIfNull(path, value);
    value.clear();
    state!.childrenStates.clear();
  }

  // Operations on list/table schemas/values ===================================

  /**
   * Pushes elements into the list or table at the given path. Pushed
   * `undefined` values are turned into their initial values.
   * @param relativePath Path of the list or table on which to push new elements
   * (relative to the current working path). Defaults to `'.'`.
   * @param toInsert Values to push.
   * @returns New size of the list or table.
   */
  @action
  public push(relativePath: string = '.', ...toInsert: any[]): number {
    this.throwIfReadOnly();
    const { schema, path, id, value, state, isComputed } =
      this.lastPathPartInfo(relativePath, {
        fetchValues: true,
        fetchStates: true,
      });
    this.throwIfComputed(path, isComputed);
    this.throwIfInvalidSchemaType(path, schema, ['list', 'table']);
    this.throwIfNull(path, value);
    // Validate type of elements to push
    const childrenSchema = isListSchema(schema)
      ? schema.elementsSchema
      : schema.rowsSchema;
    if (process.env.NODE_ENV !== 'production') {
      toInsert.forEach((el) =>
        validateType(el, childrenSchema, appendToPath(path, '?'))
      );
    }
    mergeValues(
      this,
      schema,
      path,
      id,
      value,
      state!,
      toInsert,
      undefined,
      undefined,
      undefined,
      { handleCollections: 'append', handleUndefined: 'reset' }
    );
    return value.length;
  }

  /**
   * Pops the last element from the list or table at the given path.
   * @param relativePath Path of the list or table from which to pop an element
   * (relative to the current working path). Defaults to `'.'`.
   * @returns Popped element.
   */
  @action
  public pop(relativePath: string = '.'): any {
    this.throwIfReadOnly();
    const { schema, path, value, state, isComputed } = this.lastPathPartInfo(
      relativePath,
      {
        fetchValues: true,
        fetchStates: true,
      }
    );
    this.throwIfComputed(path, isComputed);
    this.throwIfInvalidSchemaType(path, schema, ['list', 'table']);
    this.throwIfNull(path, value);
    const popped = value.pop();
    if (isListSchema(schema)) {
      state!.childrenStates.pop();
    } else {
      // Remove information of deleted row (indices and state)
      deleteRowFromIndices(value, popped[$lfRowId]);
      deleteRowState(state!.childrenStates, popped[$lfRowId]);
    }
    return popped;
  }

  /**
   * Transforms the list or table at the given path by filtering it using a
   * provided function.
   * @param relativePath Path of the list or table to filter (relative to the
   * current working path). Defaults to `'.'`.
   * @param fn Function used to filter the list or table.
   * @param fn.element Element being currently processed.
   * @param fn.index Index of the element being currently processed.
   * @param fn.collection List or table being filtered.
   */
  @action
  public filter(
    relativePath: string = '.',
    fn: (element: any, index: number, collection: any[]) => boolean
  ): void {
    this.throwIfReadOnly();
    const { schema, path, id, value, state, isComputed } =
      this.lastPathPartInfo(relativePath, {
        fetchValues: true,
        fetchStates: true,
      });
    this.throwIfComputed(path, isComputed);
    this.throwIfInvalidSchemaType(path, schema, ['list', 'table']);
    this.throwIfNull(path, value);
    mergeValues(this, schema, path, id, value, state!, value.filter(fn));
  }

  // Operations on table schemas/values ========================================

  /**
   * Splices the table at the given path. Similar to running
   * `Array.prototype.splice` on the table but taking into consideration the
   * table's indices.
   * @param relativePath Path of the table to splice (relative to the current
   * working path). Defaults to `'.'`.
   * @param start Index at which to start changing the table.
   * @param deleteCount Number of rows to remove from the table, starting at
   * `start`.
   * @param toInsert Rows to insert into the table, starting at `start`.
   * @returns Deleted elements.
   */
  @action
  public splice(
    relativePath: string = '.',
    start: number,
    deleteCount: number,
    ...toInsert: any[]
  ): any[] {
    this.throwIfReadOnly();
    const { schema, path, value, state, isComputed } = this.lastPathPartInfo(
      relativePath,
      {
        fetchValues: true,
        fetchStates: true,
      }
    );
    this.throwIfComputed(path, isComputed);
    this.throwIfInvalidSchemaType(path, schema, ['table']);
    this.throwIfNull(path, value);
    // Validate type of rows to insert
    const rowsSchema = schema.rowsSchema;
    if (process.env.NODE_ENV !== 'production') {
      toInsert.forEach((el) =>
        validateType(el, rowsSchema, appendToPath(path, '?'))
      );
    }
    // Insert `undefined` values into the table
    const deleted = value.splice(
      start,
      deleteCount,
      ...toInsert.map(() => undefined)
    );
    // Remove information of deleted rows (indices and state)
    for (const { [$lfRowId]: rowId } of deleted) {
      deleteRowFromIndices(value, rowId);
      deleteRowState(state!.childrenStates, rowId);
    }
    // Initialise rows one by one
    let i = start;
    for (const newRow of toInsert) {
      const rowId = nextRowId();
      initializeValue(
        this,
        schema.rowsSchema,
        appendToPath(path, rowId),
        rowId,
        newRow,
        schema,
        value,
        state!,
        i++
      );
    }
    return deleted;
  }

  // Operations on map schemas/values ==========================================

  /**
   * Deletes an entry referenced by `key` from the map at the given path.
   * @param relativePath Path of the map from which to remove the entry
   * (relative to the current working path). Defaults to `'.'`.
   * @param key Key of the map entry to delete.
   * @returns `true` when an element was successfully removed from the map,
   * `false` otherwise.
   */
  @action
  public delete(relativePath: string = '.', key: string): boolean {
    this.throwIfReadOnly();
    const { schema, path, value, state, isComputed } = this.lastPathPartInfo(
      relativePath,
      {
        fetchValues: true,
        fetchStates: true,
      }
    );
    this.throwIfComputed(path, isComputed);
    this.throwIfInvalidSchemaType(path, schema, ['map']);
    this.throwIfNull(path, value);
    state!.childrenStates.delete(key);
    return value.delete(key);
  }

  // Path part utilities =======================================================

  /**
   * Last part of the "path information" (the last element of the array returned
   * by `pathInfo`).
   * @param relativePath Path from which to fetch information (relative to the
   * current working path). Defaults to `'.'`.
   * @param options Whether to pass through the value or value state trees.
   * @returns Last part of the "path information".
   */
  protected lastPathPartInfo(
    relativePath: string = '.',
    options?: PathInfoOptions
  ): PathPartInfo {
    const pathInfo = this.pathInfo(relativePath, options);
    return pathInfo[pathInfo.length - 1];
  }

  /**
   * Last and before last parts of the "path information" (the last element
   * followed by the element before last of the array returned by `pathInfo`).
   * When `relativePath` represents the "root", then the second element of the
   * array is replaced by an object with `value` and `state` pointing to their
   * boxed observable counterparts.
   * @param relativePath Path from which to fetch information (relative to the
   * current working path). Defaults to `'.'`.
   * @param options Whether to pass through the value or value state trees.
   * @returns Last and before last parts of the "path information".
   */
  protected lastPathPartAndParentInfo(
    relativePath: string = '.',
    options?: PathInfoOptions
  ): [PathPartInfo, PathPartInfo] {
    const pathInfo = this.pathInfo(relativePath, options);
    if (pathInfo.length === 1) {
      return [pathInfo[0], { value: this._value, state: this._state } as any];
    }
    return [pathInfo[pathInfo.length - 1], pathInfo[pathInfo.length - 2]];
  }

  // Error utilities ===========================================================

  /**
   * Method that throw an error if the storage is in read-only mode.
   */
  protected throwIfReadOnly(): void {
    if (this.isReadOnly) {
      throw new Error('Storage is read-only, values cannot be modifed');
    }
  }

  /**
   * Throws an error if a given schema is not nullable.
   * @param path Path of the schema.
   * @param schema Schema to check if nullable.
   */
  protected throwIfNotNullable(path: string, schema: Schema): void {
    if (!isNullableSchema(schema)) {
      throw invalidSchemaTypeError(path, ['nullable']);
    }
  }

  /**
   * Throws an error if the schema in question is computed.
   * @param path Path of the schema.
   * @param isComputed Whether the schema is computed.
   */
  protected throwIfComputed(path: string, isComputed: boolean): void {
    if (isComputed) {
      throw pathError(path, 'Schema is computed, operation not allowed');
    }
  }

  /**
   * Throws an error if a given schema has an invalid type.
   * @param path Path of the schema.
   * @param schema Schema in question.
   * @param allowedSchemas Allowed schema types.
   */
  protected throwIfInvalidSchemaType(
    path: string,
    schema: Schema,
    allowedSchemas: string[]
  ): void {
    if (allowedSchemas.indexOf(schema.type) === -1) {
      throw invalidSchemaTypeError(path, allowedSchemas);
    }
  }

  /**
   * Throws an error if a given value is `null`.
   * @param path Path of the value.
   * @param value Value in question.
   */
  protected throwIfNull(path: string, value: any) {
    if (value === null) {
      throw pathError(path, 'Value is null');
    }
  }
}
