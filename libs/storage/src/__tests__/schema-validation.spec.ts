import { Schema } from '../schema-types';
import { Storage } from '../storage';
import {
  booleanSchema,
  dateSchema,
  listSchema,
  mapSchema,
  numberSchema,
  recordSchema,
  stringSchema,
  tableSchema,
  tupleSchema,
} from '../utils/schema-creation';
import {
  COMPUTED_CHILDREN_FORBIDDEN_PROPS,
  ROOT_FORBIDDEN_FUNCTIONS,
} from '../utils/schema-validation';
import { $lfRowId } from '../utils/table-utils';

// Creating a storage instance with an invalid schema throws an error
function schemaIsValid(schema: Schema): boolean {
  try {
    return !!new Storage(schema);
  } catch (err) {
    return false;
  }
}

describe('Schema validation', () => {
  it('invalidates non-nullable schemas that define `isRequired`', () => {
    expect(schemaIsValid(numberSchema({ isRequired: true }))).toBe(false);
  });

  it('invalidates root schemas that are computed', () => {
    expect(schemaIsValid(numberSchema({ computedValue: () => 1 }))).toBe(false);
  });

  it('invalidates root schemas that are client-only', () => {
    expect(schemaIsValid(numberSchema({ isClientOnly: true }))).toBe(false);
  });

  it('invalidates root schemas that define dynamic invalid properties', () => {
    for (const prop of ROOT_FORBIDDEN_FUNCTIONS) {
      expect(schemaIsValid(numberSchema({ [prop]: () => undefined }))).toBe(
        false
      );
    }
  });

  it('invalidates computed schemas inside of computed schemas', () => {
    expect(
      schemaIsValid(
        recordSchema({
          rec: recordSchema(
            { num: numberSchema({ computedValue: () => 1 }) },
            { computedValue: () => ({ num: 5 }) }
          ),
        })
      )
    ).toBe(false);
    // Even if not direct children
    expect(
      schemaIsValid(
        recordSchema({
          li: listSchema(
            recordSchema({ num: numberSchema({ computedValue: () => 1 }) }),
            { computedValue: () => [{ num: 5 }] }
          ),
        })
      )
    ).toBe(false);
  });

  it('invalidates schemas defining both initial and computed values', () => {
    expect(
      schemaIsValid(
        recordSchema({
          num: numberSchema({ initialValue: 4, computedValue: () => 1 }),
        })
      )
    ).toBe(false);
    expect(
      schemaIsValid(
        recordSchema({
          rec: recordSchema(
            { num: numberSchema({ initialValue: 4 }) },
            { computedValue: () => ({ num: 1 }) }
          ),
        })
      )
    ).toBe(false);
  });

  it('invalidates children of computed schemas using validation properties', () => {
    for (const prop of COMPUTED_CHILDREN_FORBIDDEN_PROPS) {
      expect(
        schemaIsValid(
          recordSchema({
            li: listSchema(numberSchema({ [prop]: null }), {
              computedValue: () => [1],
            }),
          })
        )
      ).toBe(false);
      expect(
        schemaIsValid(
          recordSchema({
            rec: recordSchema(
              { x: numberSchema({ [prop]: null }) },
              { computedValue: () => ({ x: 1 }) }
            ),
          })
        )
      ).toBe(false);
    }
  });

  it('invalidates computed table schemas', () => {
    expect(
      schemaIsValid(
        recordSchema({
          table: tableSchema(recordSchema({ num: numberSchema() }), {
            computedValue: () => [{ num: 1 }],
          }),
        })
      )
    ).toBe(false);
  });

  it('invalidates computed schemas as direct children of list, map, or table schemas', () => {
    expect(
      schemaIsValid(listSchema(numberSchema({ computedValue: () => 1 })))
    ).toBe(false);
    expect(
      schemaIsValid(mapSchema(numberSchema({ computedValue: () => 1 })))
    ).toBe(false);
    expect(
      schemaIsValid(
        tableSchema(recordSchema({}, { computedValue: () => ({}) }))
      )
    ).toBe(false);
  });

  it('invalidates client-only schemas as direct children of list, map, or table schemas', () => {
    expect(
      schemaIsValid(listSchema(numberSchema({ isClientOnly: true })))
    ).toBe(false);
    expect(schemaIsValid(mapSchema(numberSchema({ isClientOnly: true })))).toBe(
      false
    );
    expect(
      schemaIsValid(tableSchema(recordSchema({}, { isClientOnly: true })))
    ).toBe(false);
  });

  it('invalidates schemas using reserved state properties', () => {
    expect(
      schemaIsValid(booleanSchema({ initialState: { allowedValues: true } }))
    ).toBe(false);
    expect(
      schemaIsValid(booleanSchema({ initialState: { validate: true } }))
    ).toBe(false);
    expect(
      schemaIsValid(booleanSchema({ initialState: { validationIssues: true } }))
    ).toBe(false);
    expect(
      schemaIsValid(
        booleanSchema({ isNullable: true, initialState: { isRequired: true } })
      )
    ).toBe(false);
    expect(schemaIsValid(dateSchema({ initialState: { minDate: true } }))).toBe(
      false
    );
    expect(schemaIsValid(dateSchema({ initialState: { maxDate: true } }))).toBe(
      false
    );
    expect(
      schemaIsValid(
        listSchema(booleanSchema(), { initialState: { minSize: true } })
      )
    ).toBe(false);
    expect(
      schemaIsValid(
        mapSchema(booleanSchema(), { initialState: { maxSize: true } })
      )
    ).toBe(false);
    expect(schemaIsValid(numberSchema({ initialState: { min: true } }))).toBe(
      false
    );
    expect(schemaIsValid(numberSchema({ initialState: { max: true } }))).toBe(
      false
    );
    expect(
      schemaIsValid(stringSchema({ initialState: { minLength: true } }))
    ).toBe(false);
    expect(
      schemaIsValid(stringSchema({ initialState: { maxLength: true } }))
    ).toBe(false);
    // Properties are only reserved for their particular schema
    expect(
      schemaIsValid(
        booleanSchema({
          initialState: {
            isRequired: true,
            minDate: true,
            maxDate: true,
            minSize: true,
            maxSize: true,
            min: true,
            max: true,
            minLength: true,
            maxLength: true,
          },
        })
      )
    ).toBe(true);
  });

  it('invalidates record schemas with invalid field names', () => {
    expect(schemaIsValid(recordSchema({ 'a/b': numberSchema() }))).toBe(false);
    expect(schemaIsValid(recordSchema({ '.': numberSchema() }))).toBe(false);
    expect(schemaIsValid(recordSchema({ '..': numberSchema() }))).toBe(false);
  });

  it('invalidates table row schemas that are nullable', () => {
    expect(
      schemaIsValid(
        tableSchema(recordSchema({ num: numberSchema() }, { isNullable: true }))
      )
    ).toBe(false);
  });

  it('invalidates table row schemas that use reserved field names', () => {
    expect(
      schemaIsValid(tableSchema(recordSchema({ [$lfRowId]: numberSchema() })))
    ).toBe(false);
  });

  it('invalidates schemas nested deeply inside other schemas', () => {
    expect(
      schemaIsValid(
        recordSchema({
          listOfMapsToTuple: listSchema(
            mapSchema(tupleSchema([numberSchema({ isRequired: true })]))
          ),
        })
      )
    ).toBe(false);
  });

  it('does not check the validity of a schema in production', () => {
    const originalEnv = process.env.NODE_ENV;
    process.env.NODE_ENV = 'production';
    // Invalid schema, but check is not performed
    expect(schemaIsValid(numberSchema({ isRequired: true }))).toBe(true);
    process.env.NODE_ENV = originalEnv;
  });
});
