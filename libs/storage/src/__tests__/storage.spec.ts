import { Storage } from '../storage';
import { isArrayLike } from '../utils/array-utils';
import {
  booleanSchema,
  dateSchema,
  listSchema,
  mapSchema,
  numberSchema,
  recordSchema,
  stringSchema,
  tableSchema,
  tupleSchema,
} from '../utils/schema-creation';
import { getRowId } from '../utils/table-utils';

const exampleSchema = recordSchema({
  bool: booleanSchema(),
  date: dateSchema(),
  numList: listSchema(numberSchema()),
  numMap: mapSchema(numberSchema()),
  table: tableSchema(recordSchema({ num: numberSchema() })),
  maybeNum: numberSchema({ isNullable: true }),
  num: numberSchema(),
  compNum: numberSchema({ computedValue: (ctx) => ctx.get('num') + 1 }),
  rec: recordSchema({
    num: numberSchema(),
    compNum: numberSchema({
      computedValue: (ctx) => ctx.get('num') + ctx.get('/compNum'),
    }),
  }),
  str: stringSchema(),
  numStrTuple: tupleSchema([numberSchema(), stringSchema()]),
  clientStr: stringSchema({ isClientOnly: true }),
  compRec: recordSchema(
    { listSize: numberSchema(), mapSize: numberSchema() },
    {
      computedValue: (ctx) => ({
        listSize: ctx.size('numList'),
        mapSize: ctx.size('numMap'),
      }),
    }
  ),
  maybeRec: recordSchema({ num: numberSchema() }, { isNullable: true }),
});

describe('Storage', () => {
  let storage: Storage;
  beforeEach(() => (storage = new Storage(exampleSchema)));

  it('gets schemas', () => {
    expect(storage.schema()).toBe(exampleSchema);
    expect(storage.schema('bool')).toBe(exampleSchema.fieldsSchemas.bool);
    expect(storage.schema('../../rec/num/../num')).toBe(
      exampleSchema.fieldsSchemas.rec.fieldsSchemas.num
    );
    expect(storage.schema('numList/?')).toBe(
      exampleSchema.fieldsSchemas.numList.elementsSchema
    );
    expect(storage.schema('numList/10')).toBe(
      exampleSchema.fieldsSchemas.numList.elementsSchema
    );
    expect(storage.schema('numMap/?')).toBe(
      exampleSchema.fieldsSchemas.numMap.valuesSchema
    );
    expect(storage.schema('numMap/xx')).toBe(
      exampleSchema.fieldsSchemas.numMap.valuesSchema
    );
    expect(storage.schema('table/0')).toBe(
      exampleSchema.fieldsSchemas.table.rowsSchema
    );
    expect(storage.schema('table/?')).toBe(
      exampleSchema.fieldsSchemas.table.rowsSchema
    );
    // Invalid paths
    expect(() => storage.schema('x')).toThrow();
    expect(() => storage.schema('/numList/x')).toThrow();
    expect(() => storage.schema('/table/x')).toThrow();
  });

  it('determines which schemas are computed', () => {
    expect(storage.isComputed('compNum')).toBe(true);
    expect(storage.isComputed('rec/compNum')).toBe(true);
    expect(storage.isComputed('num')).toBe(false);
    expect(storage.isComputed('compRec')).toBe(true);
    expect(storage.isComputed('compRec/listSize')).toBe(true);
  });

  it('gets values', () => {
    expect(storage.get()).toBe(storage.get('/'));
    expect(storage.get('bool')).toBe(false);
    expect(isArrayLike(storage.get('numList'))).toBe(true);
    expect(isArrayLike(storage.get('table'))).toBe(true);
    expect(storage.get('num')).toBe(0);
    expect(storage.get('/num')).toBe(0);
    expect(storage.get('./date/../num')).toBe(0);
    expect(storage.get('compNum')).toBe(1);
    // `compNum` is not enumerable but is still there
    expect(storage.get('rec')).toEqual({ num: 0 });
    expect(storage.get('rec').compNum).toBe(1);
    expect(storage.get('rec/compNum')).toBe(1);
    expect(storage.get('numStrTuple/1')).toBe('');
    expect(storage.get('maybeNum')).toBeNull();
    expect(storage.get('compRec/listSize')).toBe(0);
    // Invalid paths
    expect(() => storage.get('x')).toThrow();
    expect(() => storage.get('rec/y')).toThrow();
    expect(() => storage.get('numList/0')).toThrow();
    expect(() => storage.get('numStrTuple/2')).toThrow();
  });

  it('sets values', () => {
    storage.set('num', 10);
    expect(storage.get('num')).toBe(10);
    expect(storage.get('compNum')).toBe(11);
    storage.set('rec/num', 20);
    expect(storage.get('rec/compNum')).toBe(31);
    storage.set('numList/0', 5);
    expect(storage.get('numList/0')).toBe(5);
    expect(storage.get('numList')[0]).toBe(5);
    expect(storage.get('compRec/listSize')).toBe(1);
    storage.set('numMap/xxx', 3);
    expect(storage.get('numMap/xxx')).toBe(3);
    expect(storage.get('numMap').get('xxx')).toBe(3);
    expect(storage.get('compRec/mapSize')).toBe(1);
    storage.set('table', [{ num: 0 }, { num: 1 }]);
    expect(storage.get('table')).toEqual([{ num: 0 }, { num: 1 }]);
    // Invalid types
    expect(() => storage.set('bool', 0)).toThrow();
    expect(() => storage.set('numList/0', 'x')).toThrow();
    // Out of bounds index
    expect(() => storage.set('numList/5', 10)).toThrow();
  });

  it('checks that values exist', () => {
    expect(storage.has()).toBe(true);
    expect(storage.has('bool')).toBe(true);
    expect(storage.has('/rec/num')).toBe(true);
    expect(storage.has('numList/0')).toBe(false);
    expect(storage.has('numMap/xxx')).toBe(false);
    storage.set('numMap/xxx', 20);
    expect(storage.has('numMap/xxx')).toBe(true);
    expect(storage.has('table/0')).toBe(false);
    expect(storage.has('maybeRec/num')).toBe(false);
    // Invalid path
    expect(() => storage.has('x')).toThrow();
  });

  it('lists children ids', () => {
    expect(storage.childrenIds('rec')).toEqual(['num', 'compNum']);
    expect(storage.childrenIds('numList')).toEqual([]);
    storage.push('numList', 1);
    storage.push('numList', 10);
    expect(storage.childrenIds('numList')).toEqual([0, 1]);
    expect(storage.childrenIds('numMap')).toEqual([]);
    storage.set('numMap/xxx', 2);
    storage.set('numMap/yyy', 22);
    expect(storage.childrenIds('numMap')).toEqual(['xxx', 'yyy']);
    storage.set('table', [{ num: 0 }, { num: 1 }]);
    expect(storage.childrenIds('table')).toEqual(
      storage.get('table').map((row) => getRowId(row))
    );
    expect(storage.childrenIds('numStrTuple')).toEqual([0, 1]);
    // Null value
    expect(() => storage.childrenIds('maybeRec')).toThrow();
    // Invalid path
    expect(() => storage.childrenIds('x')).toThrow();
  });

  it('observes paths', () => {
    let called = 0;
    let change;
    const listener = (changeObj) => {
      ++called;
      change = changeObj;
    };
    const dispose = storage.observe('maybeRec/num', listener);
    expect(called).toBe(0);
    storage.initialize('maybeRec');
    expect(called).toBe(1);
    expect(change).toMatchObject({
      type: 'update',
      newValue: 0,
    });
    storage.set('maybeRec/num', 1);
    expect(called).toBe(2);
    expect(change).toMatchObject({ type: 'update', oldValue: 0, newValue: 1 });
    storage.set('maybeRec', { num: 2 });
    expect(called).toBe(3);
    expect(change).toMatchObject({ type: 'update', oldValue: 1, newValue: 2 });
    storage.reset('maybeRec');
    expect(called).toBe(4);
    expect(change).toMatchObject({
      type: 'update',
      oldValue: 2,
      newValue: undefined,
    });
    dispose();
    storage.initialize('maybeRec');
    expect(called).toBe(4);
    // Invalid path
    expect(() => storage.observe('x', listener)).toThrow();
  });

  it('property maintains value state', () => {
    expect(storage.getStateProperty('num', 'x')).toBeUndefined();
    storage.setStateProperty('num', 'x', 10);
    expect(storage.getStateProperty('num', 'x')).toBe(10);
    storage.set('num', 10);
    expect(storage.getStateProperty('num', 'x')).toBe(10);
    storage.set('/', storage.get('/'));
    expect(storage.getStateProperty('num', 'x')).toBe(10);
  });
});
