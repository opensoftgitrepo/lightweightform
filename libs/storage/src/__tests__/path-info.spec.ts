import { observable } from 'mobx';

import { PathInfoOptions, Storage } from '../storage';
import {
  booleanSchema,
  listSchema,
  mapSchema,
  recordSchema,
  tupleSchema,
} from '../utils/schema-creation';

const exampleSchema = recordSchema({
  bool: booleanSchema({ isClientOnly: true }),
  list: listSchema(booleanSchema(), { computedValue: () => [true] }),
  map: mapSchema(booleanSchema()),
  rec: recordSchema({ bool: booleanSchema() }, { isNullable: true }),
  tupl: tupleSchema([booleanSchema({ initialState: { someProp: 0 } })]),
});

describe('Path information', () => {
  let storage: Storage;
  beforeEach(() => (storage = new Storage(exampleSchema)));

  it('throws errors when accessing invalid paths', () => {
    expect(() => storage.pathInfo('/404')).toThrow();
    expect(() => storage.pathInfo('/list/x')).toThrow();
    expect(() => storage.pathInfo('/list/-1')).toThrow();
    // Integer too big
    expect(() => storage.pathInfo('/list/9999999999999999999')).toThrow();
    expect(() => storage.pathInfo('/rec/404')).toThrow();
    expect(() => storage.pathInfo('/tupl/1')).toThrow();
    expect(() => storage.pathInfo('/tupl/-1')).toThrow();
  });

  it('resolves paths', () => {
    expect(storage.pathInfo('/')).toEqual(storage.pathInfo('/../..'));
    expect(storage.pathInfo('/list')).toEqual(storage.pathInfo('/list/?/..'));
    expect(storage.pathInfo('./rec')).toEqual(
      storage.pathInfo('/rec/../rec/.')
    );
  });

  it('fetches only schema by default', () => {
    const rootInfo = {
      path: '/',
      id: null,
      schema: exampleSchema,
      isComputed: false,
      isClientOnly: false,
    };
    expect(storage.pathInfo('/')).toEqual([rootInfo]);
    expect(storage.pathInfo('bool')).toEqual([
      rootInfo,
      {
        path: '/bool',
        id: 'bool',
        schema: booleanSchema({ isClientOnly: true }),
        isComputed: false,
        isClientOnly: true,
      },
    ]);
  });

  it('allows access to schema of non-existent values', () => {
    expect(storage.pathInfo('/list/10')[2]).toEqual({
      path: '/list/10',
      id: 10,
      schema: booleanSchema(),
      isComputed: true,
      isClientOnly: true,
    });
    expect(storage.pathInfo('/list/?')[2]).toEqual({
      path: '/list/?',
      id: '?',
      schema: booleanSchema(),
      isComputed: true,
      isClientOnly: true,
    });
    expect(storage.pathInfo('/map/x')[2]).toEqual({
      path: '/map/x',
      id: 'x',
      schema: booleanSchema(),
      isComputed: false,
      isClientOnly: false,
    });
    expect(storage.pathInfo('/map/?')[2]).toEqual({
      path: '/map/?',
      id: '?',
      schema: booleanSchema(),
      isComputed: false,
      isClientOnly: false,
    });
    expect(storage.pathInfo('/rec/bool')[2]).toEqual({
      path: '/rec/bool',
      id: 'bool',
      schema: booleanSchema(),
      isComputed: false,
      isClientOnly: false,
    });
    expect(storage.pathInfo('/tupl/0')[2]).toEqual({
      path: '/tupl/0',
      id: 0,
      schema: booleanSchema({ initialState: { someProp: 0 } }),
      isComputed: false,
      isClientOnly: false,
    });
  });

  it('fetches values when requested', () => {
    const opts: PathInfoOptions = { fetchValues: true };
    expect(storage.pathInfo('/bool', opts)[1].value).toBe(false);
    expect(storage.pathInfo('/list/0', opts)[2].value).toBe(true);
    expect(storage.pathInfo('/map', opts)[1].value).toEqual(observable.map());
    expect(storage.pathInfo('/rec', opts)[1].value).toBe(null);
    expect(storage.pathInfo('/tupl/0', opts)[2].value).toBe(false);
    storage.initialize('/rec');
    expect(storage.pathInfo('/rec/bool', opts)[2].value).toBe(false);
    storage.set('/map/x', true);
    expect(storage.pathInfo('/map/x', opts)[2].value).toBe(true);
  });

  it('throws error when accessing invalid values', () => {
    const opts: PathInfoOptions = { fetchValues: true };
    expect(() => storage.pathInfo('/list/1', opts)).toThrow();
    expect(() => storage.pathInfo('/list/?', opts)).toThrow();
    expect(() => storage.pathInfo('/map/x', opts)).toThrow();
    expect(() => storage.pathInfo('/rec/bool', opts)).toThrow();
    expect(() => storage.pathInfo('/tupl/1', opts)).toThrow();
  });

  it('fetches state when requested', () => {
    const opts: PathInfoOptions = { fetchStates: true };
    // State of computed values is same as parent (id doesn't matter)
    expect(storage.pathInfo('/list', opts)[1].state).toBe(
      storage.pathInfo('/list/?', opts)[2].state
    );
    expect(storage.pathInfo('/list', opts)[1].state).toBe(
      storage.pathInfo('/list/10', opts)[2].state
    );
    expect(storage.pathInfo('/tupl/0', opts)[2].state!.props.someProp).toBe(0);
  });

  it('throws error when accessing invalid states', () => {
    const opts: PathInfoOptions = { fetchStates: true };
    expect(() => storage.pathInfo('/map/x', opts)).toThrow();
    expect(() => storage.pathInfo('/rec/bool', opts)).toThrow();
    expect(() => storage.pathInfo('/tupl/1', opts)).toThrow();
  });
});
