import { Storage } from '../storage';
import { booleanSchema, recordSchema } from '../utils/schema-creation';

const exampleSchema = recordSchema({
  rec: recordSchema({ bool: booleanSchema() }),
});

describe('Relative storage', () => {
  let storage: Storage;
  beforeEach(() => (storage = new Storage(exampleSchema)));

  it('is created', () => {
    const relStorage = storage.relativeStorage('rec');
    expect(relStorage.currentPath).toBe('/rec');
    expect(relStorage.isReadOnly).toBe(false);
    // Relative storage of relative storage
    expect(relStorage.relativeStorage('bool').currentPath).toBe('/rec/bool');
  });

  it('is created in read-only mode', () => {
    const readStorage = storage.relativeStorage('rec', true);
    expect(readStorage.isReadOnly).toBe(true);
    // Setting in read-only mode should throw
    expect(() => readStorage.set('bool', true)).toThrow();
    // Not possible to create a writable storage from a read-only one
    expect(readStorage.relativeStorage('bool', false).isReadOnly).toBe(true);
  });

  it('is created from instance constructor', () => {
    class XStorage extends Storage {}
    expect(
      new XStorage(exampleSchema).relativeStorage('rec') instanceof XStorage
    ).toBe(true);
  });
});
