import { observable } from 'mobx';

import { Storage } from '../storage';
import {
  booleanSchema,
  dateSchema,
  listSchema,
  mapSchema,
  numberSchema,
  recordSchema,
  stringSchema,
  tableSchema,
  tupleSchema,
} from '../utils/schema-creation';
import { $lfRowId, getRowId } from '../utils/table-utils';

const exampleSchema = recordSchema({
  bool: booleanSchema(),
  date: dateSchema(),
  num: numberSchema(),
  str: stringSchema({ initialValue: 'x' }),
  maybeNum: numberSchema({ isNullable: true }),
  numList: listSchema(numberSchema()),
  maybeNumList: listSchema(numberSchema(), { isNullable: true }),
  numMap: mapSchema(numberSchema()),
  maybeNumMap: mapSchema(numberSchema(), { isNullable: true }),
  rec: recordSchema({ num: numberSchema(), str: stringSchema() }),
  maybeRec: recordSchema({ num: numberSchema() }, { isNullable: true }),
  table: tableSchema(recordSchema({ num: numberSchema() })),
  maybeTable: tableSchema(recordSchema({ num: numberSchema() }), {
    isNullable: true,
  }),
  numStrTuple: tupleSchema([numberSchema(), stringSchema()]),
  maybeNumTuple: tupleSchema([numberSchema()], { isNullable: true }),
});

describe('Set value', () => {
  let storage: Storage;
  beforeEach(() => (storage = new Storage(exampleSchema)));

  it('sets simple values', () => {
    storage.set('bool', true);
    expect(storage.get('bool')).toBe(true);
    storage.set('date', new Date(1));
    expect(storage.get('date')).toEqual(new Date(1));
    storage.set('maybeNum', 50);
    expect(storage.get('maybeNum')).toBe(50);
    storage.set('maybeNum', null);
    expect(storage.get('maybeNum')).toBe(null);
    storage.set('num', 10);
    expect(storage.get('num')).toBe(10);
    storage.set('str', 'hello');
    expect(storage.get('str')).toBe('hello');

    // Ignores `undefined` by default
    storage.set('str', undefined);
    expect(storage.get('str')).toBe('hello');

    // Resets `undefined`
    storage.set('num', undefined, { handleUndefined: 'reset' });
    expect(storage.get('num')).toBe(0);
    storage.set('str', undefined, { handleUndefined: 'reset' });
    expect(storage.get('str')).toBe('x');
  });

  it('handles nullable complex values', () => {
    storage.set('maybeNumList', [1, 2, 3]);
    expect(storage.get('maybeNumList')).toEqual([1, 2, 3]);
    storage.set('maybeNumList', null);
    expect(storage.get('maybeNumList')).toBe(null);

    storage.set('maybeNumMap', observable.map({ x: 1, y: 2 }));
    expect(storage.get('maybeNumMap')).toEqual(observable.map({ x: 1, y: 2 }));
    storage.set('maybeNumMap', null);
    expect(storage.get('maybeNumMap')).toBe(null);

    storage.set('maybeRec', { num: 33 });
    expect(storage.get('maybeRec')).toEqual({ num: 33 });
    storage.set('maybeRec', null);
    expect(storage.get('maybeRec')).toBe(null);

    storage.set('maybeTable', [{ num: 1 }, { num: 2 }]);
    expect(storage.get('maybeTable')).toEqual([{ num: 1 }, { num: 2 }]);
    storage.set('maybeTable', null);
    expect(storage.get('maybeTable')).toBe(null);

    storage.set('maybeNumTuple', [17]);
    expect(storage.get('maybeNumTuple')).toEqual([17]);
    storage.set('maybeNumTuple', null);
    expect(storage.get('maybeNumTuple')).toBe(null);
  });

  it('sets lists', () => {
    // Setting will always merge, so we can keep a ref
    const numList = storage.get('numList');

    // Replace mode (the default)
    storage.set('numList', [1, 2, 3]);
    expect(numList).toEqual([1, 2, 3]);
    storage.set('numList', [4, 17, 13, 99]);
    expect(numList).toEqual([4, 17, 13, 99]);
    storage.set('numList', [3, undefined, 15]);
    expect(numList).toEqual([3, 15]);
    storage.set('numList', [0, undefined, undefined]);
    expect(numList).toEqual([0]);
    storage.set('numList', [undefined, 4, 16], { handleUndefined: 'reset' });
    expect(numList).toEqual([0, 4, 16]);
    storage.set('numList', [0, 55], { handleCollections: 'replace' });
    expect(numList).toEqual([0, 55]);
    storage.set('numList', undefined, { handleUndefined: 'reset' });
    expect(numList).toEqual([]);

    // Merge mode
    storage.set('numList', [88, 99], { handleCollections: 'merge' });
    expect(numList).toEqual([88, 99]);
    storage.set('numList', [10], { handleCollections: 'merge' });
    expect(numList).toEqual([10, 99]);
    storage.set('numList', [undefined, 21], { handleCollections: 'merge' });
    expect(numList).toEqual([10, 21]);
    storage.set('numList', [15, undefined, 28], {
      handleCollections: 'merge',
      handleUndefined: 'reset',
    });
    expect(numList).toEqual([15, 0, 28]);

    // Append mode
    storage.set('numList', [21, undefined, 44], {
      handleCollections: 'append',
    });
    expect(numList).toEqual([15, 0, 28, 21, 44]);
    storage.set('numList', [undefined, 3], {
      handleCollections: 'append',
      handleUndefined: 'reset',
    });
    expect(numList).toEqual([15, 0, 28, 21, 44, 0, 3]);
  });

  it('sets maps', () => {
    // Setting will always merge, so we can keep a ref
    const numMap = storage.get('numMap');
    const { map } = observable; // Simple map creation
    // Compare entries instead of the map itself to also compare insertion order
    const entries = (mapTemp) => Array.from(mapTemp.entries());

    // Replace mode (the default)
    storage.set('numMap', map({ x: 0, y: 1 }));
    expect(entries(numMap)).toEqual(entries(map({ x: 0, y: 1 })));
    storage.set('numMap', map({ a: 4, b: 5, c: 6 }));
    expect(entries(numMap)).toEqual(entries(map({ a: 4, b: 5, c: 6 })));
    storage.set('numMap', map({ a: 6, b: undefined, c: 18 }));
    expect(entries(numMap)).toEqual(entries(map({ a: 6, c: 18 })));
    storage.set('numMap', map({ a: undefined, c: 15 }));
    expect(entries(numMap)).toEqual(entries(map({ c: 15 })));
    storage.set('numMap', map({ a: 12, b: undefined }), {
      handleUndefined: 'reset',
    });
    expect(entries(numMap)).toEqual(entries(map({ a: 12, b: 0 })));
    // Order should change
    storage.set('numMap', map({ b: 10, a: 4 }), {
      handleCollections: 'replace',
    });
    expect(entries(numMap)).toEqual(entries(map({ b: 10, a: 4 })));
    storage.set('numMap', undefined, { handleUndefined: 'reset' });
    expect(entries(numMap)).toEqual(entries(map({})));

    // Merge/append mode (should be the same for maps)
    storage.set('numMap', map({ b: 77, a: 66 }), {
      handleCollections: 'append',
    });
    expect(entries(numMap)).toEqual(entries(map({ b: 77, a: 66 })));
    storage.set('numMap', map({ a: 17, x: 21 }), {
      handleCollections: 'merge',
    });
    expect(entries(numMap)).toEqual(entries(map({ b: 77, a: 17, x: 21 })));
    storage.set('numMap', map({ x: 3, y: undefined, a: undefined }), {
      handleCollections: 'merge',
    });
    expect(entries(numMap)).toEqual(entries(map({ b: 77, a: 17, x: 3 })));
    storage.set('numMap', map({ y: undefined, a: undefined, b: 20 }), {
      handleCollections: 'append',
      handleUndefined: 'reset',
    });
    expect(entries(numMap)).toEqual(entries(map({ b: 20, a: 0, x: 3, y: 0 })));
  });

  it('sets records', () => {
    // Setting will always merge, so we can keep a ref
    const rec = storage.get('rec');

    storage.set('rec', { num: 4, str: 'hi' });
    expect(rec).toEqual({ num: 4, str: 'hi' });

    // Ignores `undefined` by default
    storage.set('rec', { num: 54 });
    expect(rec).toEqual({ num: 54, str: 'hi' });
    storage.set('rec', { str: 'xxx', num: undefined });
    expect(rec).toEqual({ num: 54, str: 'xxx' });

    // Reset `undefined`
    storage.set('rec', { str: 'bye' }, { handleUndefined: 'reset' });
    expect(rec).toEqual({ num: 0, str: 'bye' });
    storage.set(
      'rec',
      { num: 8, str: undefined },
      { handleUndefined: 'reset' }
    );
    expect(rec).toEqual({ num: 8, str: '' });
    storage.set('rec', undefined, { handleUndefined: 'reset' });
    expect(rec).toEqual({ num: 0, str: '' });
  });

  it('sets tables', () => {
    // Setting will always merge, so we can keep a ref
    const table = storage.get('table');

    // Replace mode (the default)
    storage.set('table', [{ num: 1 }, { num: 2 }]);
    expect(table).toEqual([{ num: 1 }, { num: 2 }]);
    storage.set('table', [{ num: 4 }, { num: 15 }, { num: 8 }]);
    expect(table).toEqual([{ num: 4 }, { num: 15 }, { num: 8 }]);
    storage.set('table', [{ num: 3 }, undefined, { num: 15 }]);
    expect(table).toEqual([{ num: 3 }, { num: 15 }]);
    storage.set('table', [{ num: 0 }, undefined, undefined]);
    expect(table).toEqual([{ num: 0 }]);
    storage.set('table', [undefined, { num: 22 }, { num: 52 }], {
      handleUndefined: 'reset',
    });
    expect(table).toEqual([{ num: 0 }, { num: 22 }, { num: 52 }]);
    // Rows with the same id are merged (same object is used)
    const tableClone = table.map((row) => row);
    storage.set('table', tableClone.filter((row) => row.num < 25).reverse());
    expect(table).toEqual([{ num: 22 }, { num: 0 }]);
    expect(table[0]).toBe(tableClone[1]);
    expect(table[1]).toBe(tableClone[0]);
    storage.set('table', [{ num: 3, [$lfRowId]: getRowId(table[1]) }], {
      handleCollections: 'replace',
    });
    expect(table).toEqual([{ num: 3 }]);
    expect(table[0]).toBe(tableClone[0]);
    // Ids in rows to set, if not present, are ignored and a new id is generated
    storage.set('table', [{ num: 555, [$lfRowId]: 9999999 }]);
    expect(table).toEqual([{ num: 555 }]);
    expect(getRowId(table[0])).not.toBe(9999999);
    storage.set('table', undefined, { handleUndefined: 'reset' });
    expect(table).toEqual([]);

    // Merge mode
    storage.set('table', [{ num: 88 }, { num: 99 }], {
      handleCollections: 'merge',
    });
    expect(table).toEqual([{ num: 88 }, { num: 99 }]);
    // Merging only happens when the row ids are the same
    storage.set('table', [{ num: 10 }], { handleCollections: 'merge' });
    expect(table).toEqual([{ num: 88 }, { num: 99 }, { num: 10 }]);
    storage.set(
      'table',
      [undefined, { num: 21, [$lfRowId]: getRowId(table[1]) }],
      { handleCollections: 'merge' }
    );
    expect(table).toEqual([{ num: 88 }, { num: 21 }, { num: 10 }]);
    storage.set(
      'table',
      [
        { num: 15, [$lfRowId]: getRowId(table[2]) },
        undefined, // New value created
        { num: 28, [$lfRowId]: getRowId(table[0]) },
      ],
      {
        handleCollections: 'merge',
        handleUndefined: 'reset',
      }
    );
    expect(table).toEqual([{ num: 28 }, { num: 21 }, { num: 15 }, { num: 0 }]);
    // Ids in rows to set, if not present, are ignored and a new id is generated
    storage.set('table', [{ num: 333, [$lfRowId]: 9999999 }], {
      handleCollections: 'merge',
    });
    expect(table).toEqual([
      { num: 28 },
      { num: 21 },
      { num: 15 },
      { num: 0 },
      { num: 333 },
    ]);
    expect(getRowId(table[4])).not.toBe(9999999);

    // Append mode
    // Rows are never merged, even when they have the same row id
    storage.set('table', [{ num: 112, [$lfRowId]: getRowId(table[0]) }], {
      handleCollections: 'append',
    });
    expect(table).toEqual([
      { num: 28 },
      { num: 21 },
      { num: 15 },
      { num: 0 },
      { num: 333 },
      { num: 112 },
    ]);
    expect(getRowId(table[5])).not.toBe(getRowId(table[0]));
    // In append mode, even though the table is being merged with itself, the
    // appending should still happen (bypasses strict object comparison when
    // merging)
    storage.set('table', table, { handleCollections: 'append' });
    expect(table.length).toBe(12);
  });

  it('sets tuples', () => {
    storage.set('numStrTuple', [4, 'hi']);
    expect(storage.get('numStrTuple')).toEqual([4, 'hi']);

    // Ignores `undefined` by default
    storage.set('numStrTuple', [54]);
    expect(storage.get('numStrTuple')).toEqual([54, 'hi']);
    storage.set('numStrTuple', [undefined, 'xxx']);
    expect(storage.get('numStrTuple')).toEqual([54, 'xxx']);

    // Reset `undefined`
    storage.set('numStrTuple', [undefined, 'bye'], {
      handleUndefined: 'reset',
    });
    expect(storage.get('numStrTuple')).toEqual([0, 'bye']);
    storage.set('numStrTuple', [8], { handleUndefined: 'reset' });
    expect(storage.get('numStrTuple')).toEqual([8, '']);
    storage.set('numStrTuple', undefined, { handleUndefined: 'reset' });
    expect(storage.get('numStrTuple')).toEqual([0, '']);
  });
});
