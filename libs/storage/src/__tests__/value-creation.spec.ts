import { observable } from 'mobx';

import { Schema } from '../schema-types';
import { Storage } from '../storage';
import {
  booleanSchema,
  dateSchema,
  listSchema,
  mapSchema,
  numberSchema,
  recordSchema,
  stringSchema,
  tableSchema,
  tupleSchema,
} from '../utils/schema-creation';

// The initial value for a value with a given schema (supports computed values)
function initialValue<T>(schema: Schema<T>): T {
  return new Storage(recordSchema({ val: schema })).get('/val');
}

describe('Value creation', () => {
  it('returns the default value for all schema types', () => {
    expect(initialValue(booleanSchema({ isNullable: true }))).toBeNull();
    expect(initialValue(booleanSchema())).toBe(false);
    expect(initialValue(dateSchema())).toEqual(new Date(0));
    expect(initialValue(numberSchema())).toBe(0);
    expect(initialValue(stringSchema())).toBe('');
    expect(initialValue(listSchema(numberSchema()))).toEqual([]);
    expect(initialValue(mapSchema(numberSchema()))).toEqual(observable.map());
    expect(
      initialValue(recordSchema({ num: numberSchema(), str: stringSchema() }))
    ).toEqual({ num: 0, str: '' });
    expect(initialValue(tableSchema(recordSchema({})))).toEqual([]);
    expect(initialValue(tupleSchema([numberSchema(), stringSchema()]))).toEqual(
      [0, '']
    );
  });

  it('returns the initial value for all schema types', () => {
    expect(
      initialValue(booleanSchema({ isNullable: true, initialValue: true }))
    ).toBe(true);
    expect(initialValue(booleanSchema({ initialValue: true }))).toBe(true);
    expect(initialValue(dateSchema({ initialValue: new Date(1) }))).toEqual(
      new Date(1)
    );
    expect(initialValue(numberSchema({ initialValue: 3 }))).toBe(3);
    expect(initialValue(stringSchema({ initialValue: 'x' }))).toBe('x');
    expect(
      initialValue(listSchema(numberSchema(), { initialValue: [undefined, 2] }))
    ).toEqual([0, 2]);
    expect(
      initialValue(
        mapSchema(numberSchema(), {
          initialValue: new Map([
            ['a', undefined],
            ['b', 2],
          ]),
        })
      )
    ).toEqual(observable.map({ a: 0, b: 2 }));
    expect(
      initialValue(
        recordSchema(
          {
            num: numberSchema(),
            str: stringSchema({ initialValue: 'x' }),
          },
          { initialValue: { num: 2 } }
        )
      )
    ).toEqual({ num: 2, str: 'x' });
    expect(
      initialValue(
        recordSchema(
          {
            num: numberSchema(),
            str: stringSchema({ initialValue: 'x' }),
          },
          { initialValue: { str: 'y' } }
        )
      )
    ).toEqual({ num: 0, str: 'y' });
    expect(
      initialValue(
        tableSchema(
          recordSchema({
            num: numberSchema(),
            str: stringSchema({ initialValue: 'x' }),
          }),
          { initialValue: [{ num: 4 }] }
        )
      )
    ).toEqual([{ num: 4, str: 'x' }]);
    expect(
      initialValue(
        tableSchema(
          recordSchema({
            num: numberSchema(),
            str: stringSchema({ initialValue: 'x' }),
          }),
          { initialValue: [undefined!, { str: 'y' }] }
        )
      )
    ).toEqual([
      { num: 0, str: 'x' },
      { num: 0, str: 'y' },
    ]);
    expect(
      initialValue(
        tupleSchema([numberSchema(), stringSchema({ initialValue: 'x' })], {
          initialValue: [2],
        })
      )
    ).toEqual([2, 'x']);
    expect(
      initialValue(
        tupleSchema([numberSchema(), stringSchema({ initialValue: 'x' })], {
          initialValue: [undefined, 'y'],
        })
      )
    ).toEqual([0, 'y']);
  });

  it('creates values with initial values that depend on other values', () => {
    expect(
      initialValue(
        recordSchema({
          num: numberSchema({ initialValue: 2 }),
          numPlus1: numberSchema({ initialValue: (ctx) => ctx.get('num') + 1 }),
        })
      )
    ).toEqual({ num: 2, numPlus1: 3 });
    // Accessing "future" properties should throw (this assumes an order in the
    // object properties which should be respected in a similar fashion in most
    // engines anyway)
    expect(() =>
      initialValue(
        recordSchema({
          numPlus1: numberSchema({ initialValue: (ctx) => ctx.get('num') + 1 }),
          num: numberSchema({ initialValue: 2 }),
        })
      )
    ).toThrow();
    expect(
      initialValue(
        // FIXME: `id` should be a number here but is a `string`
        listSchema(numberSchema({ initialValue: (_, id) => +id }), {
          initialValue: [undefined, undefined] as any,
        })
      )
    ).toEqual([0, 1]);
    expect(
      initialValue(
        tableSchema(
          recordSchema({
            num: numberSchema({
              initialValue: (ctx) => {
                const table = ctx.get('..');
                const row = ctx.get();
                const rowIdx = table.findIndex((r) => row === r);
                return rowIdx === 0 ? 0 : table[rowIdx - 1].num + 1;
              },
            }),
          }),
          { initialValue: [undefined, undefined] as any }
        )
      )
    ).toEqual([{ num: 0 }, { num: 1 }]);
  });

  it('returns the value of "simple" computed values', () => {
    expect(
      initialValue(
        booleanSchema({ isNullable: true, computedValue: () => null })
      )
    ).toBe(null);
    expect(initialValue(booleanSchema({ computedValue: () => true }))).toBe(
      true
    );
    expect(
      initialValue(dateSchema({ computedValue: () => new Date(1) }))
    ).toEqual(new Date(1));
    expect(initialValue(numberSchema({ computedValue: () => 3 }))).toBe(3);
    expect(initialValue(stringSchema({ computedValue: () => 'x' }))).toBe('x');

    // Normalise computed values
    expect(
      initialValue(
        listSchema(numberSchema(), { computedValue: () => [undefined, 2] })
      )
    ).toEqual([0, 2]);
    expect(
      initialValue(
        mapSchema(numberSchema(), {
          computedValue: () =>
            new Map([
              ['a', undefined],
              ['b', 2],
            ]),
        })
      )
    ).toEqual(
      new Map([
        ['a', 0],
        ['b', 2],
      ])
    );
    expect(
      initialValue(
        recordSchema(
          { num: numberSchema(), str: stringSchema() },
          { computedValue: () => ({ num: 2 }) }
        )
      )
    ).toEqual({ num: 2, str: '' });
    expect(
      initialValue(
        recordSchema(
          { num: numberSchema(), str: stringSchema() },
          { computedValue: () => ({ str: 'y' }) }
        )
      )
    ).toEqual({ num: 0, str: 'y' });
    expect(
      initialValue(
        tupleSchema([numberSchema(), stringSchema()], {
          computedValue: () => [2],
        })
      )
    ).toEqual([2, '']);
    expect(
      initialValue(
        tupleSchema([numberSchema(), stringSchema()], {
          computedValue: () => [undefined, 'y'],
        })
      )
    ).toEqual([0, 'y']);

    // Computed properties in MobX objects are not enumerable
    expect(
      initialValue(
        recordSchema({ num: numberSchema({ computedValue: () => 1 }) })
      )
    ).toEqual({});
    expect(
      initialValue(
        recordSchema({ num: numberSchema({ computedValue: () => 1 }) })
      ).num
    ).toEqual(1);
  });
});
