import { ContextFunction, ParentContextFunction } from './utils/context-utils';
import { ValidationIssue, ValidationIssues } from './utils/error-utils';

/**
 * Schema for simple values.
 */
export type SimpleSchema =
  | BooleanSchema
  | DateSchema
  | NumberSchema
  | StringSchema;

/**
 * Schema for complex values (a schema which contains child schemas).
 */
export type ComplexSchema =
  | ListSchema
  | MapSchema
  | RecordSchema
  | TableSchema
  | TupleSchema;

/**
 * Generic schema (properties available to all schemas).
 * @param T (Non-observable) type of the value the schema represents.
 */
export interface Schema<T = any> {
  /**
   * Type of the schema. All schema types should have a different `type` and all
   * instances of the same schema should use the same `type`.
   */
  type: string;
  /**
   * Initial value for values following the schema. Schemas with set computed
   * values cannot have set initial values.
   */
  initialValue?: T | ParentContextFunction<T | null> | null;
  /**
   * The schema represents a value that is the result of executing the passed
   * function. Schemas with set computed values cannot have set initial values.
   */
  computedValue?: ParentContextFunction<T | null>;
  /**
   * Initial state to be associated with the value represented by the schema.
   * Non-function properties will become "observable" state properties.
   * Optionally, initial state properties may be set as `ContextFunction`s which
   * will be computed and thus become "computed" state properties.
   */
  initialState?: Record<string, any>;
  /**
   * Schemas and values that are client-only are omitted when transformed into
   * their JSON counterpart. Schemas whose values are computed are client-only
   * by default. The JSON representation of schemas **must** be static,
   * therefore a function is not accepted here.
   */
  isClientOnly?: boolean;
  /**
   * Whether `null` is a valid type value. Defaults to `false`. If this is set
   * to `true`, then a `null` value will fail to type check on being set for
   * this schema.
   */
  isNullable?: boolean;
  /**
   * Whether the value is required (if `null` is a valid value). This may only
   * be set when `isNullable` is `true`.
   */
  isRequired?: boolean | ParentContextFunction<boolean>;
  /**
   * String to use as issue code when the value is `null` but it is required.
   * Defaults to `IS_REQUIRED_CODE`.
   */
  isRequiredCode?: string;
  /**
   * List of values that are allowed for the value represented by the schema.
   */
  allowedValues?: Array<T | null> | ParentContextFunction<Array<T | null>>;
  /**
   * String to use as issue code when the value is not one of those allowed.
   * Defaults to `DISALLOWED_VALUE_CODE`.
   */
  disallowedValueCode?: string;
  /**
   * Custom validation function(s) used to determine whether the value
   * represented by the schema has issues. Validation functions can return a
   * single validation issue or a list of them, possibly asynchronously. These
   * functions receives a storage instance with a current working path set to
   * the path of the schema. If the schema is nullable, these function will
   * never be called when the value associated with the schema is `null`.
   */
  validate?:
    | ContextFunction<ValidationIssue | ValidationIssues | void>
    | Array<ContextFunction<ValidationIssue | ValidationIssues | void>>;
  /**
   * Schemas may have additional arbitrary properties.
   */
  [extraProperties: string]: any;
}

/**
 * A schema which represents a collection.
 */
export interface CollectionSchema<T = any> extends Schema<T> {
  /**
   * Minimum size that the collection represented by this schema is allowed to
   * have.
   */
  minSize?: number | ParentContextFunction<number>;
  /**
   * String to use as issue code when the size of the value is less than the
   * minimum allowed size. Defaults to `SIZE_OUT_OF_BOUNDS_CODE`.
   */
  minSizeCode?: string;
  /**
   * Maximum size that the collection represented by this schema is allowed to
   * have.
   */
  maxSize?: number | ParentContextFunction<number>;
  /**
   * String to use as issue code when the size of the value is greater than the
   * maximum allowed size. Defaults to `SIZE_OUT_OF_BOUNDS_CODE`.
   */
  maxSizeCode?: string;
}

/**
 * Schema representing boolean values.
 */
export interface BooleanSchema extends Schema<boolean> {
  /**
   * Type for boolean schema instances.
   */
  type: 'boolean';
}

/**
 * Schema representing date values.
 */
export interface DateSchema extends Schema<Date> {
  /**
   * Type for date schema instances.
   */
  type: 'date';
  /**
   * Minimum date that the value represented by this schema is allowed to be.
   */
  minDate?: Date | ParentContextFunction<Date>;
  /**
   * String to use as issue code when the date is less than the minimum allowed
   * date. Defaults to `DATE_OUT_OF_BOUNDS_CODE`.
   */
  minDateCode?: string;
  /**
   * Maximum date that the value represented by this schema is allowed to be.
   */
  maxDate?: Date | ParentContextFunction<Date>;
  /**
   * String to use as issue code when the date is greater than the maximum
   * allowed date. Defaults to `DATE_OUT_OF_BOUNDS_CODE`.
   */
  maxDateCode?: string;
}

/**
 * Schema representing values that are lists of elements.
 * @param T Type of the list elements.
 * @param S Schema of the list elements.
 */
export interface ListSchema<T = any, S extends Schema<T> = Schema<T>>
  extends CollectionSchema<T[]> {
  /**
   * Type for list schema instances.
   */
  type: 'list';
  /**
   * Schema of the list's elements.
   */
  elementsSchema: S;
}

/**
 * Schema representing values that are maps of strings to values.
 * @param T Type of the map values.
 * @param S Schema of the map values.
 */
export interface MapSchema<T = any, S extends Schema<T> = Schema<T>>
  extends CollectionSchema<Map<string, T>> {
  /**
   * Type for map schema instances.
   */
  type: 'map';
  /**
   * Schema of the map's values.
   */
  valuesSchema: S;
}

/**
 * Schema representing numeric values.
 */
export interface NumberSchema extends Schema<number> {
  /**
   * Type for number schema instances.
   */
  type: 'number';
  /**
   * Minimum number that the value represented by this schema is allowed to be.
   * `-Number.MAX_VALUE` by default (since `-Infinity` is not JSON
   * serialisable).
   */
  min?: number | ParentContextFunction<number>;
  /**
   * String to use as issue code when the value is less than the minimum allowed
   * value. Defaults to `NUMBER_OUT_OF_BOUNDS_CODE`.
   */
  minCode?: string;
  /**
   * Maximum number that the value represented by this schema is allowed to be.
   * `Number.MAX_VALUE` by default (since `Infinity` is not JSON serialisable).
   */
  max?: number | ParentContextFunction<number>;
  /**
   * String to use as issue code when the value is greater than the maximum
   * allowed value. Defaults to `NUMBER_OUT_OF_BOUNDS_CODE`.
   */
  maxCode?: string;
  /**
   * Whether the schema represents a number which must be a safe integer (in
   * which case `min` and `max` automatically get the bounds of
   * `Number.MIN_SAFE_INTEGER` and `Number.MAX_SAFE_INTEGER` respectively). This
   * is `false` by default.
   */
  isInteger?: boolean;
}

/**
 * Schema representing record values where each field has a specified schema.
 * @param T Type of the record that this schema represents.
 * @param S Schemas of all record properties.
 */
export interface RecordSchema<
  T extends Record<keyof T, any> = Record<any, any>,
  S extends { [F in keyof T]: Schema<T[F]> } = { [F in keyof T]: Schema<T[F]> }
> extends Schema<T> {
  /**
   * Type for record schema instances.
   */
  type: 'record';
  /**
   * Schemas of each record field.
   */
  fieldsSchemas: S;
}

/**
 * Schema representing string values.
 */
export interface StringSchema extends Schema<string> {
  /**
   * Type for string schema instances.
   */
  type: 'string';
  /**
   * Minimum length that the value represented by this schema is allowed to
   * have.
   */
  minLength?: number | ParentContextFunction<number>;
  /**
   * String to use as issue code when the string length is less than the minimum
   * allowed length. Defaults to `LENGTH_OUT_OF_BOUNDS_CODE`.
   */
  minLengthCode?: string;
  /**
   * Maximum length that the value represented by this schema is allowed to
   * have.
   */
  maxLength?: number | ParentContextFunction<number>;
  /**
   * String to use as issue code when the string length is greater than the
   * maximum allowed length. Defaults to `LENGTH_OUT_OF_BOUNDS_CODE`.
   */
  maxLengthCode?: string;
}

/**
 * Schema representing values that are lists of records where each "row" has a
 * unique unchanging key.
 * @param T Type of the table rows.
 * @param S Schema of the list elements.
 */
export interface TableSchema<
  T extends Record<string, any> = Record<string, any>,
  S extends RecordSchema<T> = RecordSchema<T>
> extends CollectionSchema<T[]> {
  /**
   * Type for table schema instances.
   */
  type: 'table';
  /**
   * Schema of the table's rows.
   */
  rowsSchema: S;
  /**
   * TODO: Write docs and stabilise API.
   */
  keys?: Record<string, string | ContextFunction<T>>;
}

/**
 * Schema representing tuple values where each element has a specified schema.
 * @param T Type of the tuple that this schema represents.
 * @param S Schemas of all tuple elements.
 */
export interface TupleSchema<
  T extends any[] = any[],
  S extends Schema[] = Schema[]
> extends Schema<T> {
  /**
   * Type for tuple schema instances.
   */
  type: 'tuple';
  /**
   * Schemas of each tuple element.
   */
  elementsSchemas: S;
}
