import { getTestBed, TestBed } from '@angular/core/testing';
import { LfSerializer, LfStorage, LF_APP_SCHEMA } from '@lightweightform/core';
import {
  booleanSchema,
  dateSchema,
  isBooleanSchema,
  isListSchema,
  isNumberSchema,
  listSchema,
  mapSchema,
  numberSchema,
  recordSchema,
  Schema,
  stringSchema,
  tableSchema,
  tupleSchema,
} from '@lightweightform/storage';

import { libCollectionChildren } from '../../utils/public-utils';
import {
  LfXmlSerializer,
  LF_XML_SERIALIZER_OPTIONS,
} from '../xml-serializer.service';

// Remove left padding from a string template
function unpad(str: string): string {
  const initialSpace = str.slice(1).match(/^\s*/)![0];
  return str.replace(new RegExp('^' + initialSpace, 'gm'), '').trim();
}

describe('XML serialiser', () => {
  let lfStorage: LfStorage;
  let serializer: LfXmlSerializer;

  // Helper functions to set up the storage
  function setupWithSchema(schema: Schema): void {
    TestBed.configureTestingModule({
      providers: [
        { provide: LF_APP_SCHEMA, useValue: schema },
        { provide: LfSerializer, useClass: LfXmlSerializer },
        { provide: LF_XML_SERIALIZER_OPTIONS, useValue: { space: 2 } },
        LfStorage,
      ],
    });
    lfStorage = getTestBed().inject(LfStorage);
    serializer = getTestBed().inject(LfSerializer) as LfXmlSerializer;
  }
  // Helper function to check an equivalence between a JS value and an XML
  // string
  function expectToBeEquivalent(path: string, val: any, xml: string) {
    const schema = lfStorage.schema(path);
    if (!lfStorage.isComputed(path)) {
      lfStorage.set(path, val);
    }
    expect(serializer.serialize(lfStorage.getAsJS(path), schema, path)).toBe(
      unpad(xml)
    );
    expect(serializer.deserialize(xml, schema, path)).toEqual(
      lfStorage.getAsJS(path)
    );
  }
  // Helper function to check an unequivalence between a JS value and an XML
  // string (the XML string is built from the JS value, but a different JS value
  // is produced from the XML string)
  function expectNotToBeEquivalent(
    path: string,
    val: any,
    xml: string,
    resVal: any
  ) {
    const schema = lfStorage.schema(path);
    if (!lfStorage.isComputed(path)) {
      lfStorage.set(path, val);
    }
    expect(serializer.serialize(lfStorage.getAsJS(path), schema, path)).toBe(
      unpad(xml)
    );
    const deserialized = serializer.deserialize(xml, schema, path);
    expect(deserialized).not.toEqual(lfStorage.getAsJS(path));
    expect(deserialized).toEqual(resVal);
  }

  it('allows configuring the number of spaces', () => {
    setupWithSchema(recordSchema({ str: stringSchema() }));

    serializer.setOptions({ space: 4 });
    expectToBeEquivalent(
      '/',
      { str: 'abc' },
      `
      <?xml version="1.0" encoding="UTF-8"?>
      <app>
          <str>abc</str>
      </app>
      `
    );

    serializer.setOptions({ space: '\t' });
    expectToBeEquivalent(
      '/',
      { str: 'abc' },
      `
      <?xml version="1.0" encoding="UTF-8"?>
      <app>
      \t<str>abc</str>
      </app>
      `
    );

    serializer.setOptions({ space: 0 });
    expectToBeEquivalent(
      '/',
      { str: 'abc' },
      '<?xml version="1.0" encoding="UTF-8"?><app><str>abc</str></app>'
    );
  });

  it('allows configuring whether to trim XML text', () => {
    setupWithSchema(recordSchema({ str: stringSchema() }));

    expectToBeEquivalent(
      '/',
      { str: '  abc\t\n' },
      `
      <?xml version="1.0" encoding="UTF-8"?>
      <app>
        <str>  abc\t\n</str>
      </app>
      `
    );

    serializer.setOptions({ trim: true });
    expectNotToBeEquivalent(
      '/',
      { str: '  abc\t\n' },
      `
      <?xml version="1.0" encoding="UTF-8"?>
      <app>
        <str>  abc\t\n</str>
      </app>
      `,
      { str: 'abc' }
    );
  });

  it('allows configuring whether to self-close elements', () => {
    setupWithSchema(
      recordSchema({
        str: stringSchema(),
        list: listSchema(numberSchema()),
        map: mapSchema(numberSchema()),
        tab: tableSchema(recordSchema({ num: numberSchema() })),
      })
    );

    // Self-close by default
    expectToBeEquivalent(
      '/',
      { str: '', list: [], map: new Map(), tab: [] },
      `
      <?xml version="1.0" encoding="UTF-8"?>
      <app>
        <str/>
        <list/>
        <map/>
        <tab/>
      </app>
      `
    );

    serializer.setOptions({ selfCloseEmptyElements: false });
    expectToBeEquivalent(
      '/',
      { str: '', list: [], map: new Map(), tab: [] },
      `
      <?xml version="1.0" encoding="UTF-8"?>
      <app>
        <str></str>
        <list></list>
        <map></map>
        <tab></tab>
      </app>
      `
    );
  });

  it('serialises XML declarations', () => {
    setupWithSchema(recordSchema({}));

    expectToBeEquivalent(
      '/',
      {},
      `
      <?xml version="1.0" encoding="UTF-8"?>
      <app/>
      `
    );

    // Lowercase encoding supported by setting it manually (though it should be
    // case-insensitive, uppercase seems to be better supported)
    serializer.setOptions({
      declarationVersion: '1.1',
      declarationEncoding: 'iso-8859-1',
      declarationStandalone: 'yes',
    });
    // Setting the `declarationEncoding` shouldn't change the `charset`
    expect(serializer.charset).toBe('utf-8');
    expectToBeEquivalent(
      '/',
      {},
      `
      <?xml version="1.1" encoding="iso-8859-1" standalone="yes"?>
      <app/>
      `
    );

    // Setting the `charset` should set the `declarationEncoding` (in uppercase)
    // by default
    serializer.setOptions({ charset: 'ansi' });
    expectToBeEquivalent(
      '/',
      {},
      `
      <?xml version="1.1" encoding="ANSI" standalone="yes"?>
      <app/>
      `
    );

    // No declaration emitted
    serializer.setOptions({ emitDeclaration: false });
    expectToBeEquivalent('/', {}, '<app/>');
  });

  it('handles `null` values by setting an `xsi:nil` attribute', () => {
    setupWithSchema(recordSchema({ str: stringSchema({ isNullable: true }) }));

    expectToBeEquivalent(
      '/',
      { str: null },
      `
      <?xml version="1.0" encoding="UTF-8"?>
      <app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
        <str xsi:nil="true"/>
      </app>
      `
    );

    expectToBeEquivalent(
      '/',
      { str: '' },
      `
      <?xml version="1.0" encoding="UTF-8"?>
      <app>
        <str/>
      </app>
      `
    );

    // A new namespace prefix should be generated for the `nil` attribute
    serializer.setOptions({ namespaces: { someURI: 'xsi' } });
    expectToBeEquivalent(
      '/str',
      null,
      `
      <?xml version="1.0" encoding="UTF-8"?>
      <str xmlns:xsi="someURI" xmlns:xsi2="http://www.w3.org/2001/XMLSchema-instance" xsi2:nil="true"/>
      `
    );
  });

  it('handles `null` values by leaving elements empty', () => {
    setupWithSchema(
      recordSchema({
        boolNull: booleanSchema({ isNullable: true }),
        datNull: dateSchema({ isNullable: true }),
        numNull: numberSchema({ isNullable: true }),
        recNull: recordSchema({}, { isNullable: true }),
        tupNull: tupleSchema([], { isNullable: true }),
        str: stringSchema(),
        strNull: stringSchema({ isNullable: true }),
        list: listSchema(numberSchema()),
        listNull: listSchema(numberSchema(), { isNullable: true }),
        map: mapSchema(numberSchema()),
        mapNull: mapSchema(numberSchema(), { isNullable: true }),
        tab: tableSchema(recordSchema({ num: numberSchema() })),
        tabNull: tableSchema(recordSchema({ num: numberSchema() }), {
          isNullable: true,
        }),
      })
    );
    serializer.setOptions({ nullValueRepresentation: 'emptyElement' });

    expectToBeEquivalent(
      '/',
      {
        boolNull: null,
        datNull: null,
        numNull: null,
        recNull: null,
        tupNull: null,
        str: '',
        strNull: null,
        list: [],
        listNull: null,
        map: new Map(),
        mapNull: null,
        tab: [],
        tabNull: null,
      },
      `
      <?xml version="1.0" encoding="UTF-8"?>
      <app>
        <boolNull/>
        <datNull/>
        <numNull/>
        <recNull/>
        <tupNull/>
        <str/>
        <strNull/>
        <list/>
        <listNull/>
        <map/>
        <mapNull/>
        <tab/>
        <tabNull/>
      </app>
      `
    );

    // Can't distinguish empty string from `null` (deserialiser returns `null`)
    expectNotToBeEquivalent(
      '/strNull',
      '',
      `
      <?xml version="1.0" encoding="UTF-8"?>
      <strNull/>
      `,
      null
    );

    // Can't distinguish empty list from `null` (deserialiser returns `null`)
    expectNotToBeEquivalent(
      '/listNull',
      [],
      `
      <?xml version="1.0" encoding="UTF-8"?>
      <listNull/>
      `,
      null
    );

    // Can't distinguish empty map from `null` (deserialiser returns `null`)
    expectNotToBeEquivalent(
      '/mapNull',
      new Map(),
      `
      <?xml version="1.0" encoding="UTF-8"?>
      <mapNull/>
      `,
      null
    );

    // Can't distinguish empty table from `null` (deserialiser returns `null`)
    expectNotToBeEquivalent(
      '/tabNull',
      [],
      `
      <?xml version="1.0" encoding="UTF-8"?>
      <tabNull/>
      `,
      null
    );
  });

  it('supports custom element names', () => {
    setupWithSchema(
      recordSchema({
        list: listSchema(numberSchema(), { xmlElementName: 'nList' }),
        map: mapSchema(numberSchema()),
      })
    );
    serializer.setOptions({
      elementNames: {
        '/': 'rootName',
        '/list': 'thisShouldBeOverridden',
        '/list/?': 'number',
        '/map': 'nMap',
      },
    });

    const val = { list: [0], map: new Map([['x', 4]]) };

    expectToBeEquivalent(
      '/',
      val,
      `
      <?xml version="1.0" encoding="UTF-8"?>
      <rootName>
        <nList>
          <number>0</number>
        </nList>
        <nMap>
          <nMapValue key="x">4</nMapValue>
        </nMap>
      </rootName>
      `
    );

    expectToBeEquivalent(
      '/map/x',
      4,
      `
      <?xml version="1.0" encoding="UTF-8"?>
      <nMapValue>4</nMapValue>
      `
    );
  });

  it('supports custom element attributes', () => {
    setupWithSchema(
      recordSchema(
        {
          list: listSchema(
            numberSchema({ xmlElementAttributes: { z: (v) => v * 2 } })
          ),
          tab: tableSchema(
            recordSchema({ x: numberSchema(), y: numberSchema() })
          ),
        },
        { xmlElementAttributes: { version: '3.0', y: 'z' } }
      )
    );
    serializer.setOptions({
      elementAttributes: {
        '/': { version: '2.0', x: 'y' },
        '/list/?': { a: 'a' },
        '/tab/?': {
          x: {
            serialize: (value) => {
              const x = value.x;
              delete value.x;
              return x;
            },
            deserialize: (value, x) => {
              value.x = +x;
            },
          },
        },
      },
    });

    expectToBeEquivalent(
      '/',
      {
        list: [0, 1],
        tab: [
          { x: 2, y: 3 },
          { x: 4, y: 5 },
        ],
      },
      `
      <?xml version="1.0" encoding="UTF-8"?>
      <app version="3.0" x="y" y="z">
        <list>
          <listElement a="a" z="0">0</listElement>
          <listElement a="a" z="2">1</listElement>
        </list>
        <tab>
          <tabRow x="2">
            <y>3</y>
          </tabRow>
          <tabRow x="4">
            <y>5</y>
          </tabRow>
        </tab>
      </app>
      `
    );
  });

  it('supports setting namespaces in the root element', () => {
    setupWithSchema(recordSchema({}, { xmlElementAttributes: { v: '1' } }));
    serializer.setOptions({
      elementAttributes: {
        '/': { a: 'a' },
      },
      namespaces: {
        'http://default.ns': '',
        'http://some.uri': 'some',
        'http://another.uri': 'other',
      },
    });

    expectToBeEquivalent(
      '/',
      {},
      `
      <?xml version="1.0" encoding="UTF-8"?>
      <app xmlns="http://default.ns" xmlns:some="http://some.uri" xmlns:other="http://another.uri" a="a" v="1"/>
      `
    );
  });

  it('supports changing suffixes in collection elements', () => {
    setupWithSchema(
      recordSchema({
        l: listSchema(
          mapSchema(tableSchema(recordSchema({ num: numberSchema() })))
        ),
      })
    );

    const val = {
      l: [
        new Map([
          ['x', [{ num: 0 }, { num: 1 }]],
          ['y', [{ num: 2 }]],
        ]),
        new Map(),
        new Map([['z', []]]),
      ],
    };

    expectToBeEquivalent(
      '/',
      val,
      `
      <?xml version="1.0" encoding="UTF-8"?>
      <app>
        <l>
          <lElement>
            <lElementValue key="x">
              <lElementValueRow>
                <num>0</num>
              </lElementValueRow>
              <lElementValueRow>
                <num>1</num>
              </lElementValueRow>
            </lElementValue>
            <lElementValue key="y">
              <lElementValueRow>
                <num>2</num>
              </lElementValueRow>
            </lElementValue>
          </lElement>
          <lElement/>
          <lElement>
            <lElementValue key="z"/>
          </lElement>
        </l>
      </app>
      `
    );

    // Gets element names right when getting an element other that the root
    expectToBeEquivalent(
      '/l/2',
      val.l[2],
      `
      <?xml version="1.0" encoding="UTF-8"?>
      <lElement>
        <lElementValue key="z"/>
      </lElement>
      `
    );

    serializer.setOptions({
      listElementSuffix: 'E',
      mapValueSuffix: 'V',
      tableRowSuffix: 'R',
    });

    expectToBeEquivalent(
      '/',
      val,
      `
      <?xml version="1.0" encoding="UTF-8"?>
      <app>
        <l>
          <lE>
            <lEV key="x">
              <lEVR>
                <num>0</num>
              </lEVR>
              <lEVR>
                <num>1</num>
              </lEVR>
            </lEV>
            <lEV key="y">
              <lEVR>
                <num>2</num>
              </lEVR>
            </lEV>
          </lE>
          <lE/>
          <lE>
            <lEV key="z"/>
          </lE>
        </l>
      </app>
      `
    );

    // Gets element names right when getting an element other that the root
    expectToBeEquivalent(
      '/l/2',
      val.l[2],
      `
      <?xml version="1.0" encoding="UTF-8"?>
      <lE>
        <lEV key="z"/>
      </lE>
      `
    );
  });

  it('supports changing the "key" attribute name in map values', () => {
    setupWithSchema(
      recordSchema({
        map: mapSchema(numberSchema(), { xmlKeyAttributeName: 'k' }),
        map2: mapSchema(numberSchema()),
      })
    );
    serializer.setOptions({ mapValueKeyAttributeName: 'KK' });

    expectToBeEquivalent(
      '/',
      {
        map: new Map([['x', 1]]),
        map2: new Map([['y', 2]]),
      },
      `
      <?xml version="1.0" encoding="UTF-8"?>
      <app>
        <map>
          <mapValue k="x">1</mapValue>
        </map>
        <map2>
          <map2Value KK="y">2</map2Value>
        </map2>
      </app>
      `
    );
  });

  it('serialises dates properly', () => {
    setupWithSchema(recordSchema({ dat: dateSchema() }));

    expectToBeEquivalent(
      '/',
      { dat: new Date(1) },
      `
      <?xml version="1.0" encoding="UTF-8"?>
      <app>
        <dat>1970-01-01T00:00:00.001Z</dat>
      </app>
      `
    );

    expectToBeEquivalent(
      '/dat',
      new Date(7),
      `
      <?xml version="1.0" encoding="UTF-8"?>
      <dat>1970-01-01T00:00:00.007Z</dat>
      `
    );
  });

  it('serialises tuples with client-side elements', () => {
    setupWithSchema(
      recordSchema({
        tup: tupleSchema([
          numberSchema({ computedValue: () => 99 }),
          numberSchema(),
        ]),
      })
    );

    expectToBeEquivalent(
      '/',
      { tup: [undefined, 10] },
      `
      <?xml version="1.0" encoding="UTF-8"?>
      <app>
        <tup>
          <tup1>10</tup1>
        </tup>
      </app>
      `
    );

    expectToBeEquivalent(
      '/tup/0',
      99,
      `
      <?xml version="1.0" encoding="UTF-8"?>
      <tup0>99</tup0>
      `
    );
  });

  it('serialises structure with all schema types', () => {
    setupWithSchema(
      recordSchema({
        bool: booleanSchema(),
        dat: dateSchema(),
        str: stringSchema(),
        rec: recordSchema({
          list: listSchema(numberSchema()),
          map: mapSchema(numberSchema()),
          tab: tableSchema(recordSchema({ num: numberSchema() })),
          tup: tupleSchema([numberSchema()]),
        }),
        nil: recordSchema({}, { isNullable: true }),
      })
    );

    expectToBeEquivalent(
      '/',
      {
        bool: true,
        dat: new Date(55),
        str: 'hi',
        rec: {
          list: [1, 2],
          map: new Map([['x', 3]]),
          tab: [{ num: 4 }, { num: 5 }],
          tup: [6],
        },
        nil: null,
      },
      `
      <?xml version="1.0" encoding="UTF-8"?>
      <app xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
        <bool>true</bool>
        <dat>1970-01-01T00:00:00.055Z</dat>
        <str>hi</str>
        <rec>
          <list>
            <listElement>1</listElement>
            <listElement>2</listElement>
          </list>
          <map>
            <mapValue key="x">3</mapValue>
          </map>
          <tab>
            <tabRow>
              <num>4</num>
            </tabRow>
            <tabRow>
              <num>5</num>
            </tabRow>
          </tab>
          <tup>
            <tup0>6</tup0>
          </tup>
        </rec>
        <nil xsi:nil="true"/>
      </app>
      `
    );
  });

  it('supports custom simple transforms', () => {
    setupWithSchema(
      recordSchema({
        bool: booleanSchema({
          xmlSimpleTransform: {
            serialize: (value) => 'XXX' + value,
            deserialize: (value) => value.slice(3),
          },
        }),
        num: numberSchema(),
        dat: dateSchema(),
      })
    );
    serializer.setOptions({
      simpleTransform: [
        {
          serialize: (value, schema) =>
            isBooleanSchema(schema) ? !value : value,
          deserialize: (value, schema) =>
            isBooleanSchema(schema) ? value === 'false' : value,
        },
        {
          serialize: (value, schema) =>
            isNumberSchema(schema) ? value + 10 : undefined,
          deserialize: (value, schema) =>
            isNumberSchema(schema) ? +value - 10 : undefined,
        },
      ],
    });

    expectToBeEquivalent(
      '/',
      {
        bool: true,
        num: 10,
        dat: new Date(44),
      },
      `
      <?xml version="1.0" encoding="UTF-8"?>
      <app>
        <bool>XXXfalse</bool>
        <num>20</num>
        <dat>1970-01-01T00:00:00.044Z</dat>
      </app>
      `
    );
  });

  it('supports custom transforms', () => {
    setupWithSchema(
      recordSchema(
        { bool: booleanSchema(), list: listSchema(numberSchema()) },
        {
          xmlTransform: {
            serialize: (value) => {
              value.y = { '/text': value.x['/text'] + 10 };
              return value;
            },
            deserialize: (value) => {
              delete value.y;
              return value;
            },
          },
        }
      )
    );
    serializer.setOptions({
      transform: [
        {
          serialize: (value, schema) => {
            if (isBooleanSchema(schema)) {
              value['/text'] = !(value['/text'] === 'true').toString();
            }
            return value;
          },
          deserialize: (value, schema) => {
            if (isBooleanSchema(schema)) {
              value['/text'] = (value['/text'] === 'false').toString();
            }
            return value;
          },
        },
        {
          serialize: (value, _, path) => {
            if (path === '/') {
              value.x = { '/text': 5 };
            }
            return value;
          },
          deserialize: (value, _, path) => {
            if (path === '/') {
              delete value.x;
            }
            return value;
          },
        },
        {
          serialize: (value, schema) => {
            if (isListSchema(schema)) {
              libCollectionChildren(value).forEach(
                (el, i) => (el['/attributes'] = { i })
              );
            }
            return value;
          },
        },
      ],
    });

    expectToBeEquivalent(
      '/',
      { bool: true, list: [9, 8, 7] },
      `
      <?xml version="1.0" encoding="UTF-8"?>
      <app>
        <bool>false</bool>
        <list>
          <listElement i="0">9</listElement>
          <listElement i="1">8</listElement>
          <listElement i="2">7</listElement>
        </list>
        <x>5</x>
        <y>15</y>
      </app>
      `
    );
  });
});
