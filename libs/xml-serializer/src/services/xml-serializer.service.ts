import { Inject, Injectable, InjectionToken, Optional } from '@angular/core';
import { LfSerializer, LfStorage } from '@lightweightform/core';
import {
  appendToPath,
  isCollectionSchema,
  PATH_ID_PLACEHOLDER,
  Schema,
} from '@lightweightform/storage';

import { LfXmlSerializerSchemaProperties } from '../types/schema-properties';
import {
  LfXmlElementAttributes,
  LfXmlSerializerOptions,
  LfXmlTransformationFn,
  LfXmlTransformer,
} from '../types/xml-serializer-options';
import { jsToXml } from '../utils/js-to-xml';
import { childElementName, specifiedElementName } from '../utils/options-utils';
import { xmlToJs } from '../utils/xml-to-js';

/**
 * Injection token used to set the XML serialiser options.
 */
export const LF_XML_SERIALIZER_OPTIONS: InjectionToken<LfXmlSerializerOptions> =
  new InjectionToken('LF_XML_SERIALIZER_OPTIONS');

/**
 * Default name for the root (`'/'` path) XML element name.
 */
export const DEFAULT_ROOT_XML_ELEMENT_NAME = 'app';

/**
 * Serialiser used to save/load application values as XML. Has limited support
 * for namespaces (defined only on the root element), if further support is
 * required, please open an issue.
 */
// We don't provide this service in the root since it depends on the `LfStorage`
@Injectable()
export class LfXmlSerializer extends LfSerializer {
  public charset = 'utf-8';
  public mimeType = 'application/xml';
  // `.txt` and `text/plain` added for IE 9- support.
  public accept = '.xml,.txt,application/xml,text/plain';
  public extension = 'xml';
  public autoBOM = true;
  public encode?: (content: string) => BlobPart[];
  /**
   * Space used when printing the value as an XML string.
   */
  public space: number | string =
    process.env.NODE_ENV === 'production' ? 0 : '\t';
  /**
   * Whether to trim whitespace characters that may exist before and after text
   * when deserialising.
   */
  public trim = false;
  /**
   * Whether to emit an XML declaration.
   */
  public emitDeclaration = true;
  /**
   * Version to set in the XML declaration.
   */
  public declarationVersion = '1.0';
  /**
   * Encoding to set in the XML declaration.
   */
  public declarationEncoding: string = this.charset.toUpperCase();
  /**
   * Value for the "standalone" property in the XML declaration.
   */
  public declarationStandalone?: 'yes' | 'no';
  /**
   * Simple transformer(s).
   */
  public simpleTransform?:
    | LfXmlTransformer<LfXmlTransformationFn>
    | Array<LfXmlTransformer<LfXmlTransformationFn>>;
  /**
   * Transformer(s).
   */
  public transform?:
    | LfXmlTransformer<LfXmlTransformationFn>
    | Array<LfXmlTransformer<LfXmlTransformationFn>>;
  /**
   * How to represents `null` values.
   */
  public nullValueRepresentation: 'nilAttribute' | 'emptyElement' =
    'nilAttribute';
  /**
   * Mapping of element names.
   */
  public elementNames?: Record<string, string>;
  /**
   * Mapping of XML attributes to add to each element.
   */
  public elementAttributes?: Record<string, LfXmlElementAttributes>;
  /**
   * Namespaces to set in the root XML element.
   */
  public namespaces?: Record<string, string>;
  /**
   * Suffix used to name the XML elements that are children of a list.
   */
  public listElementSuffix = 'Element';
  /**
   * Suffix used to name the XML elements that are children of a map.
   */
  public mapValueSuffix = 'Value';
  /**
   * Suffix used to name the XML elements that are children of a table.
   */
  public tableRowSuffix = 'Row';
  /**
   * Name of the XML attribute on children elements of maps used to specify the
   * value's key.
   */
  public mapValueKeyAttributeName = 'key';
  /**
   * Whether to produce XML elements without sub-elements as self-closing.
   */
  public selfCloseEmptyElements = true;

  constructor(
    @Optional()
    @Inject(LF_XML_SERIALIZER_OPTIONS)
    options: LfXmlSerializerOptions | null,
    private lfStorage: LfStorage
  ) {
    super();
    if (options) {
      this.setOptions(options);
    }
  }

  /**
   * Sets the serialiser's options.
   * @param options Options to set.
   */
  public setOptions(options: LfXmlSerializerOptions): void {
    options.charset !== undefined && (this.charset = options.charset);
    options.autoBOM !== undefined && (this.autoBOM = options.autoBOM);
    options.encode !== undefined && (this.encode = options.encode);
    options.space !== undefined && (this.space = options.space);
    options.trim !== undefined && (this.trim = options.trim);
    options.emitDeclaration !== undefined &&
      (this.emitDeclaration = options.emitDeclaration);
    options.declarationVersion !== undefined &&
      (this.declarationVersion = options.declarationVersion);
    (options.charset !== undefined ||
      options.declarationEncoding !== undefined) &&
      (this.declarationEncoding =
        options.declarationEncoding === undefined
          ? options.charset!.toUpperCase()
          : options.declarationEncoding);
    options.declarationStandalone !== undefined &&
      (this.declarationStandalone = options.declarationStandalone);
    options.simpleTransform !== undefined &&
      (this.simpleTransform = options.simpleTransform);
    options.transform !== undefined && (this.transform = options.transform);
    options.nullValueRepresentation !== undefined &&
      (this.nullValueRepresentation = options.nullValueRepresentation);
    options.elementNames !== undefined &&
      (this.elementNames = options.elementNames);
    options.elementAttributes !== undefined &&
      (this.elementAttributes = options.elementAttributes);
    options.namespaces !== undefined && (this.namespaces = options.namespaces);
    options.listElementSuffix !== undefined &&
      (this.listElementSuffix = options.listElementSuffix);
    options.mapValueSuffix !== undefined &&
      (this.mapValueSuffix = options.mapValueSuffix);
    options.tableRowSuffix !== undefined &&
      (this.tableRowSuffix = options.tableRowSuffix);
    options.mapValueKeyAttributeName !== undefined &&
      (this.mapValueKeyAttributeName = options.mapValueKeyAttributeName);
    options.selfCloseEmptyElements !== undefined &&
      (this.selfCloseEmptyElements = options.selfCloseEmptyElements);
  }

  public serialize(
    js: any,
    schema: Schema & LfXmlSerializerSchemaProperties,
    path: string
  ): string {
    const pathP = this.pathWithPlaceholders(path);
    const name = this.elementName(schema, pathP);
    return jsToXml(js, name, schema, path, pathP, this);
  }

  public deserialize(
    xml: string,
    schema: Schema & LfXmlSerializerSchemaProperties,
    path: string
  ): any {
    const pathP = this.pathWithPlaceholders(path);
    const name = this.elementName(schema, pathP);
    return xmlToJs(xml, name, schema, path, pathP, this);
  }

  /**
   * Path with all collection element ids replaced by the path id placeholder
   * (to be able to correctly access the path in the
   * `elementNames`/`elementProperties` maps).
   * @param path Path to transform.
   * @returns Provided path with all collection element ids replaced by the path
   * id placeholder.
   */
  protected pathWithPlaceholders(path: string): string {
    const pathInfo = this.lfStorage.pathInfo(path);
    let curPath = '/';
    for (let i = 1, l = pathInfo.length; i < l; ++i) {
      curPath = appendToPath(
        curPath,
        isCollectionSchema(pathInfo[i - 1].schema)
          ? PATH_ID_PLACEHOLDER
          : pathInfo[i].id!
      );
    }
    return curPath;
  }

  /**
   * Name of the XML element associated with the given schema and path.
   * @param schema Schema of the value.
   * @param pathP Path of the value with collection element ids replaced by path
   * id placeholders.
   * @returns XML element name.
   */
  protected elementName(
    schema: Schema & LfXmlSerializerSchemaProperties,
    pathP: string
  ): string {
    const specifiedName = specifiedElementName(schema, pathP, this);
    if (specifiedName !== undefined) {
      return specifiedName;
    }
    const pathInfo = this.lfStorage.pathInfo(pathP);
    let curSchema = pathInfo[0].schema;
    const rootName = specifiedElementName(curSchema, '/', this);
    let curName =
      rootName === undefined ? DEFAULT_ROOT_XML_ELEMENT_NAME : rootName;
    for (let i = 1, l = pathInfo.length; i < l; ++i) {
      const { schema: nextSchema, id: nextId, path: nextPath } = pathInfo[i];
      curName = childElementName(
        curName,
        curSchema,
        nextSchema,
        nextPath,
        nextId!,
        this
      );
      curSchema = nextSchema;
    }
    return curName;
  }
}
