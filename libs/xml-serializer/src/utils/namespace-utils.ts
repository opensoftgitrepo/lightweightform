/**
 * Namespace defining the `nil` attribute (used to represent `null` values).
 */
export const XSI_NS = 'http://www.w3.org/2001/XMLSchema-instance';

/**
 * Namespace defining the `nil` attribute (used to represent `null` values).
 */
export const XSI_NS_PREFIX = 'xsi';

/**
 * Transforms a map of namespaces into an object representing the attributes
 * used to declare the namespaces in the XML.
 * @param namespaces Record of namespaces (mapping from the namespace to its
 * prefix).
 * @returns Attributes to set in an XML element (mapping from attribute name to
 * its value).
 */
export function namespacesToAttributes(
  namespaces: Record<string, string>
): Record<string, string> {
  return Object.keys(namespaces).reduce((attrs, ns) => {
    const attr = 'xmlns' + (namespaces[ns] ? `:${namespaces[ns]}` : '');
    attrs[attr] = ns;
    return attrs;
  }, {});
}

/**
 * Transforms an object representing the attributes of an XML element into a map
 * of namespaces.
 * @param attrs object representing the attributes of an XML element (mapping
 * from attribute name to its value).
 * @returns Map of namespaces (mapping from the namespace to its prefix).
 */
export function attributesToNamespaces(
  attrs: Record<string, string>
): Record<string, string> {
  return Object.keys(attrs).reduce((ns, attr) => {
    if (attr === 'xmlns') {
      ns[attrs[attr]] = '';
    } else if (attr.startsWith('xmlns:')) {
      ns[attrs[attr]] = attr.slice(6); // Length of "xmlns:"
    }
    return ns;
  }, {});
}

/**
 * Adds a namespace to the record of namespaces if not yet added given a
 * preferred prefix for the namespace. If the prefix is already taken, a new one
 * is generated.
 * @param namespaces Map of namespaces (mapping from the namespace to its
 * prefix).
 * @param namespace Namespace to add.
 * @param preferredPrefix Preferred prefix to be associated with the namespace.
 */
export function addNamespace(
  namespaces: Record<string, string>,
  namespace: string,
  preferredPrefix: string
): void {
  if (namespaces[namespace] === undefined) {
    // Create a reverse map (from prefix to namespace)
    const prefixes = Object.keys(namespaces).reduce((pref, ns) => {
      pref[namespaces[ns]] = ns;
      return pref;
    }, {});
    // Find an unused prefix
    if (prefixes[preferredPrefix] === undefined) {
      namespaces[namespace] = preferredPrefix;
    } else {
      for (let i = 2; true; ++i) {
        if (prefixes[preferredPrefix + i] === undefined) {
          namespaces[namespace] = preferredPrefix + i;
          break;
        }
      }
    }
  }
}

/**
 * Returns the provided string with a namespace prefix.
 * @param namespaces Record of namespaces (mapping from the namespace to its
 * prefix).
 * @param ns Namespace to prefix.
 * @param str String to prefix with the namespace.
 * @returns Prefixed string.
 */
export function withPrefix(
  namespaces: Record<string, string>,
  ns: string,
  str: string
): string {
  if (namespaces[ns] === undefined) {
    throw new Error(`Unknown namespace: ${ns}`);
  }
  const prefix = namespaces[ns];
  return prefix ? `${prefix}:${str}` : str;
}
