import {
  appendToPath,
  isClientOnlySchema,
  isDateSchema,
  isListSchema,
  isMapSchema,
  isRecordSchema,
  isSimpleSchema,
  isTableSchema,
  PATH_ID_PLACEHOLDER,
  Schema,
} from '@lightweightform/storage';
import { js2xml } from 'xml-js';

import { LfXmlSerializerSchemaProperties } from '../types/schema-properties';
import { LfXmlSerializerOptions } from '../types/xml-serializer-options';

import { libOptions } from './lib-options';
import {
  addNamespace,
  namespacesToAttributes,
  withPrefix,
  XSI_NS,
  XSI_NS_PREFIX,
} from './namespace-utils';
import {
  childElementName,
  mapValueKeyAttributeName,
  runSerializers,
  runSimpleSerializers,
  serializeAttributes,
} from './options-utils';

/**
 * Converts a given storage-compatible JS value with a given schema into an XML
 * string using `xml-js`.
 * @param js Storage-compatible JS value to convert to an XML string.
 * @param name XML element name of the root element being converted.
 * @param schema Schema of the value being converted.
 * @param path Path of value being converted.
 * @param pathP Path of value being converted with collection value ids replaced
 * with placeholders.
 * @param id Identifier of value being converted.
 * @param options Options used to convert the value.
 * @returns XML string representation of the value.
 */
export function jsToXml(
  js: any,
  name: string,
  schema: Schema & LfXmlSerializerSchemaProperties,
  path: string,
  pathP: string,
  options: Readonly<LfXmlSerializerOptions>
): string {
  // Build `xml-js` representation of the XML value
  const lib: any = {};
  // XML declaration
  if (options.emitDeclaration) {
    lib['/declaration'] = {
      '/attributes': {
        version: options.declarationVersion,
        encoding: options.declarationEncoding,
        standalone: options.declarationStandalone,
      },
    };
  }
  // `xml-js` representation of the storage-compatible value
  const ns = Object.assign({}, options.namespaces);
  const rootLib = jsToLib(js, name, schema, path, pathP, ns, options);
  if (rootLib !== undefined) {
    const rootAttrs = rootLib['/attributes'] || {};
    // Set namespaces before all other attributes
    rootLib['/attributes'] = Object.assign(
      namespacesToAttributes(ns),
      rootAttrs
    );
    lib[name] = rootLib;
  }
  return js2xml(lib, {
    ...libOptions,
    spaces: options.space,
    fullTagEmptyElement: !options.selfCloseEmptyElements,
  });
}

/**
 * Converts a storage-compatible JS value with a given schema into an
 * intermediate representation used by the `xml-js` library to convert the value
 * into an XML string.
 * @param js Storage-compatible JS value to convert to the intermediate
 * representation.
 * @param name XML element name of the value being converted.
 * @param schema Schema of the value being converted.
 * @param path Path of value being converted.
 * @param pathP Path of value being converted with collection value ids replaced
 * with placeholders.
 * @param id Identifier of value being converted.
 * @param ns Mapping of namespaces.
 * @param options Options used to convert the value.
 * @returns `xml-js` compatible representation of the value.
 */
function jsToLib(
  js: any,
  name: string,
  schema: Schema & LfXmlSerializerSchemaProperties,
  path: string,
  pathP: string,
  ns: Record<string, string>,
  options: Readonly<LfXmlSerializerOptions>
): any {
  // Helper functions to obtain a child's name and call `jsToLib` on a child
  function chElName(chSchema, chPathP, chId?) {
    return childElementName(name, schema, chSchema, chPathP, chId, options);
  }
  function chJsToLib(ch, chName, chSchema, chPathP, chId) {
    const chPath = appendToPath(path, chId);
    return jsToLib(ch, chName, chSchema, chPath, chPathP, ns, options);
  }

  // `xml-js` representation of the `js` value
  let lib: any;

  if (js !== undefined) {
    lib = {};

    // Serialise XML attributes
    const customAttrs = serializeAttributes(js, schema, path, pathP, options);
    customAttrs && (lib['/attributes'] = customAttrs);

    if (js === null) {
      // Represent `null` values with a `'nil'` attribute (or leave them empty)
      if (options.nullValueRepresentation === 'nilAttribute') {
        addNamespace(ns, XSI_NS, XSI_NS_PREFIX);
        const attrs = lib['/attributes'] || (lib['/attributes'] = {});
        attrs[withPrefix(ns, XSI_NS, 'nil')] = 'true';
      }
    } else if (isSimpleSchema(schema)) {
      // Run custom simple serialisers
      let serialized = runSimpleSerializers(js, schema, path, options);
      // Perform "standard" serialisation when no serialiser changed the value
      if (js === serialized) {
        serialized = isDateSchema(schema) ? js.toISOString() : js.toString();
      }
      // Force `xml-js` to self-close (by default) empty strings
      serialized && (lib['/text'] = serialized);
    } else if (isListSchema(schema) || isTableSchema(schema)) {
      const chSchema = isListSchema(schema)
        ? schema.elementsSchema
        : schema.rowsSchema;
      const chPathP = appendToPath(pathP, PATH_ID_PLACEHOLDER);
      const chName = chElName(chSchema, chPathP);
      const children = js.reduce((arr, val, i) => {
        const ch = chJsToLib(val, chName, chSchema, chPathP, i);
        ch !== undefined && arr.push(ch);
        return arr;
      }, []);
      // Force `xml-js` to self-close (by default) empty collections
      children.length > 0 && (lib[chName] = children);
    } else if (isMapSchema(schema)) {
      const chSchema = schema.valuesSchema;
      const chPathP = appendToPath(pathP, PATH_ID_PLACEHOLDER);
      const chName = chElName(chSchema, chPathP);
      const children = Array.from(js.entries()).reduce(
        (arr: any[], [key, val]) => {
          const ch = chJsToLib(val, chName, chSchema, chPathP, key);
          if (ch !== undefined) {
            const chAttrs = ch['/attributes'] || (ch['/attributes'] = {});
            chAttrs[mapValueKeyAttributeName(schema, options)] = key;
            arr.push(ch);
          }
          return arr;
        },
        []
      ) as any[];
      // Force `xml-js` to self-close (by default) empty collections
      children.length > 0 && (lib[chName] = children);
    }
    // Record or tuple
    else {
      const chSchemas = isRecordSchema(schema)
        ? schema.fieldsSchemas
        : schema.elementsSchemas;
      for (const id of Object.keys(chSchemas)) {
        const chSchema = chSchemas[id];
        const chPathP = appendToPath(pathP, id);
        const chName = chElName(chSchema, chPathP, id);
        // Omit elements in tuples with client-only schemas
        if (!isClientOnlySchema(chSchema)) {
          const ch = chJsToLib(js[id], chName, chSchema, chPathP, id);
          // Ignore `undefined` lib values
          ch !== undefined && (lib[chName] = ch);
        }
      }
    }
  }

  // Run custom serialisers
  lib = runSerializers(lib, schema, path, options);

  return lib;
}
