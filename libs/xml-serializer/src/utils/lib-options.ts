import { Options } from 'xml-js';

/**
 * Options used when calling `js2xml` and `xml2js` from the `xml-js` library. We
 * use forward slashes preceding all keys since they cannot conflict with LF
 * record fields.
 */
export const libOptions: Options.JS2XML & Options.XML2JS = {
  compact: true,
  declarationKey: '/declaration',
  instructionKey: '/instruction',
  attributesKey: '/attributes',
  textKey: '/text',
  cdataKey: '/cdata',
  doctypeKey: '/doctype',
  commentKey: '/comment',
  parentKey: '/parent',
};
