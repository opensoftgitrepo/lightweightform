import { arrayfy } from '@lightweightform/core';
import {
  Identifier,
  isListSchema,
  isMapSchema,
  isRecordSchema,
  isTableSchema,
  Schema,
} from '@lightweightform/storage';

import { LfXmlSerializerSchemaProperties } from '../types/schema-properties';
import { LfXmlSerializerOptions } from '../types/xml-serializer-options';

/**
 * XML element name according to provided schema and options.
 * @param schema Schema of the value.
 * @param path Path of the value.
 * @param options XML serialiser options.
 * @returns XML element name of the value or `undefined` if one wasn't specified
 * in the schema or options.
 */
export function specifiedElementName(
  schema: Schema & LfXmlSerializerSchemaProperties,
  path: string,
  options: Readonly<LfXmlSerializerOptions>
): string | undefined {
  if (schema.xmlElementName !== undefined) {
    return schema.xmlElementName;
  }
  if (options.elementNames && options.elementNames[path] !== undefined) {
    return options.elementNames[path];
  }
}

/**
 * XML element name of the child element.
 * @param name XML element name of the parent.
 * @param schema Schema of the parent.
 * @param childSchema Schema of the child.
 * @param childPathP Path of the child with collection value ids replaced with
 * placeholders.
 * @param childId Identifier of the child (for records/tuples, unneeded
 * otherwise).
 * @param options XML serialiser options.
 * @returns XML element name of the child.
 */
export function childElementName(
  name: string,
  schema: Schema & LfXmlSerializerSchemaProperties,
  childSchema: Schema & LfXmlSerializerSchemaProperties,
  childPathP: string,
  childId: Identifier | undefined,
  options: Readonly<LfXmlSerializerOptions>
): string {
  if (childSchema.xmlElementName !== undefined) {
    return childSchema.xmlElementName;
  }
  if (options.elementNames && options.elementNames[childPathP] !== undefined) {
    return options.elementNames[childPathP];
  }
  return isListSchema(schema)
    ? name + options.listElementSuffix
    : isMapSchema(schema)
    ? name + options.mapValueSuffix
    : isTableSchema(schema)
    ? name + options.tableRowSuffix
    : isRecordSchema(schema)
    ? `${childId}`
    : name + childId;
}

/**
 * Name of the XML property on children elements of maps used to specify the
 * value's key.
 * @param schema Schema of the map.
 * @param options XML serialiser options.
 * @returns Name of the property.
 */
export function mapValueKeyAttributeName(
  schema: Schema,
  options: Readonly<LfXmlSerializerOptions>
): string {
  return schema.xmlKeyAttributeName || options.mapValueKeyAttributeName;
}

/**
 * Serialises all attributes to set in a certain XML element.
 * @param js Value for which to set attributes.
 * @param schema Schema of value with attributes to serialise.
 * @param path Path of value with attributes to serialise.
 * @param pathP Path of value with attributes to serialise with collection value
 * ids replaced with placeholders.
 * @param option XML serialiser options.
 * @returns Serialised attributes to set in the XML element.
 */
export function serializeAttributes(
  js: any,
  schema: Schema & LfXmlSerializerSchemaProperties,
  path: string,
  pathP: string,
  options: Readonly<LfXmlSerializerOptions>
): Record<string, string> | undefined {
  let attrs: Record<string, string> | undefined;
  for (const maybeAttrs of [
    options.elementAttributes && options.elementAttributes[pathP],
    schema.xmlElementAttributes,
  ]) {
    for (const attr of Object.keys(maybeAttrs || {})) {
      const val = maybeAttrs![attr];
      if (typeof val === 'string') {
        (attrs || (attrs = {}))[attr] = val;
      } else if (typeof val === 'function' || val.serialize) {
        const serialize = typeof val === 'function' ? val : val.serialize!;
        (attrs || (attrs = {}))[attr] = serialize(js, schema, path);
      }
    }
  }
  return attrs;
}

/**
 * Deserialises all attributes from a certain XML element.
 * @param js Value from which to unset attributes.
 * @param libAttrs Attributes of the XML element.
 * @param schema Schema of value with attributes to deserialise.
 * @param path Path of value with attributes to deserialise.
 * @param pathP Path of value with attributes to deserialise with collection
 * value ids replaced with placeholders.
 * @param option XML serialiser options.
 */
export function deserializeAttributes(
  js: any,
  libAttrs: Record<string, string>,
  schema: Schema & LfXmlSerializerSchemaProperties,
  path: string,
  pathP: string,
  options: Readonly<LfXmlSerializerOptions>
): void {
  for (const maybeAttrs of [
    schema.xmlElementAttributes,
    options.elementAttributes && options.elementAttributes[pathP],
  ]) {
    for (const attr of Object.keys(maybeAttrs || {})) {
      const val = maybeAttrs![attr];
      if (typeof val === 'object' && val.deserialize) {
        val.deserialize(js, libAttrs[attr], schema, path);
      }
    }
  }
}

/**
 * Runs all simple serialisers for a value of a simple schema.
 * @param value Value on which to run simple serialisers.
 * @param schema Schema of value on which to run serialisers.
 * @param path Path of value on which to run serialisers.
 * @param options XML serialiser options.
 * @returns Serialised value.
 */
export function runSimpleSerializers(
  value: any,
  schema: Schema & LfXmlSerializerSchemaProperties,
  path: string,
  options: Readonly<LfXmlSerializerOptions>
): any {
  let serialized = value;
  for (const t of [
    ...arrayfy(options.simpleTransform),
    schema.xmlSimpleTransform,
  ]) {
    const res = t && t.serialize && t.serialize(serialized, schema, path);
    res !== undefined && (serialized = res);
  }
  return serialized;
}

/**
 * Runs all simple deserialisers for a value of a simple schema.
 * @param value Value on which to run simple deserialisers.
 * @param schema Schema of value on which to run deserialisers.
 * @param path Path of value on which to run deserialisers.
 * @param options XML serialiser options.
 * @returns Deserialised value.
 */
export function runSimpleDeserializers(
  value: any,
  schema: Schema & LfXmlSerializerSchemaProperties,
  path: string,
  options: Readonly<LfXmlSerializerOptions>
): any {
  let deserialised = value;
  for (const t of [
    schema.xmlSimpleTransform,
    ...arrayfy(options.simpleTransform).reverse(),
  ]) {
    const res = t && t.deserialize && t.deserialize(deserialised, schema, path);
    res !== undefined && (deserialised = res);
  }
  return deserialised;
}

/**
 * Runs all serialisers for a lib value of a given schema.
 * @param value Lib value on which to run serialisers.
 * @param schema Schema of value on which to run serialisers.
 * @param path Path of value on which to run serialisers.
 * @param options XML serialiser options.
 * @returns Serialised lib value.
 */
export function runSerializers(
  lib: any,
  schema: Schema & LfXmlSerializerSchemaProperties,
  path: string,
  options: Readonly<LfXmlSerializerOptions>
): any {
  for (const t of [...arrayfy(options.transform), schema.xmlTransform]) {
    const serializer = t && t.serialize;
    serializer && (lib = serializer(lib, schema, path));
  }
  return lib;
}

/**
 * Runs all deserialisers for a lib value of a given schema.
 * @param value Lib value on which to run deserialisers.
 * @param schema Schema of value on which to run deserialisers.
 * @param path Path of value on which to run deserialisers.
 * @param options XML serialiser options.
 * @returns Deserialised lib value.
 */
export function runDeserializers(
  lib: any,
  schema: Schema & LfXmlSerializerSchemaProperties,
  path: string,
  options: Readonly<LfXmlSerializerOptions>
): any {
  for (const t of [
    schema.xmlTransform,
    ...arrayfy(options.transform).reverse(),
  ]) {
    const deserializer = t && t.deserialize;
    deserializer && (lib = deserializer(lib, schema, path));
  }
  return lib;
}
