/**
 * Returns the children of a collection represented as an `xml-js` object.
 * @param lib `xml-js` object representing a collection.
 * @returns List of `xml-js` objects representing the collection's items.
 */
export function libCollectionChildren(lib: any): any[] {
  if (Array.isArray(lib)) {
    return lib;
  }
  const childrenProp = Object.keys(lib).find((prop) => prop[0] !== '/');
  return childrenProp ? lib[childrenProp] : [];
}
