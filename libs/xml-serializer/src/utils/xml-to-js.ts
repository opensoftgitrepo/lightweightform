import { arrayfy } from '@lightweightform/core';
import {
  appendToPath,
  isBooleanSchema,
  isClientOnlySchema,
  isDateSchema,
  isListSchema,
  isMapSchema,
  isNullableSchema,
  isNumberSchema,
  isRecordSchema,
  isSimpleSchema,
  isTableSchema,
  isTupleSchema,
  PATH_ID_PLACEHOLDER,
  Schema,
} from '@lightweightform/storage';
import { xml2js } from 'xml-js';

import { LfXmlSerializerSchemaProperties } from '../types/schema-properties';
import { LfXmlSerializerOptions } from '../types/xml-serializer-options';

import { libOptions } from './lib-options';
import { attributesToNamespaces, withPrefix, XSI_NS } from './namespace-utils';
import {
  childElementName,
  deserializeAttributes,
  mapValueKeyAttributeName,
  runDeserializers,
  runSimpleDeserializers,
} from './options-utils';

/**
 * Converts a given XML string into a storage-compatible JS value with a given
 * schema using `xml-js`.
 * @param xml XML string to convert to a storage-compatible JS value.
 * @param name XML element name of the root element being converted.
 * @param schema Schema of the value being converted.
 * @param path Path of value being converted.
 * @param pathP Path of value being converted with collection value ids replaced
 * with placeholders.
 * @param options Options used to convert the value.
 * @returns Storage-compatible JS value representation of the XML.
 */
export function xmlToJs(
  xml: string,
  name: string,
  schema: Schema & LfXmlSerializerSchemaProperties,
  path: string,
  pathP: string,
  options: Readonly<LfXmlSerializerOptions>
): any {
  const rootLib = xml2js(xml, { ...libOptions, trim: options.trim })[name];
  const ns =
    rootLib === undefined
      ? {}
      : attributesToNamespaces(rootLib['/attributes'] || {});
  return libToJs(rootLib, name, schema, path, pathP, ns, options);
}

/**
 * Converts the intermediate representation of a value used by the `xml-js`
 * library to convert a value from an XML string into a storage-compatible JS
 * value with a given schema.
 * @param lib `xml-js` compatible representation of the value to convert into a
 * storage-compatible JS value.
 * @param name XML element name of the value being converted.
 * @param schema Schema of the value being converted.
 * @param path Path of value being converted.
 * @param pathP Path of value being converted with collection value ids replaced
 * with placeholders.
 * @param ns Mapping of namespaces.
 * @param options Options used to convert the value.
 * @returns Storage-compatible JS value with a given schema.
 */
function libToJs(
  lib: any,
  name: string,
  schema: Schema & LfXmlSerializerSchemaProperties,
  path: string,
  pathP: string,
  ns: Record<string, string>,
  options: Readonly<LfXmlSerializerOptions>
): any {
  // Helper functions to obtain a child's name and call `libToJs` on a child
  function chElName(chSchema, chPathP, chId?) {
    return childElementName(name, schema, chSchema, chPathP, chId, options);
  }
  function chLibToJs(ch, chName, chSchema, chPathP, chId) {
    const chPath = appendToPath(path, chId);
    return libToJs(ch, chName, chSchema, chPath, chPathP, ns, options);
  }

  // Storage-compatible JS value
  let js: any;

  // Run custom deserialisers
  lib = runDeserializers(lib, schema, path, options);

  if (lib !== undefined) {
    // `null` values
    if (
      isNullableSchema(schema) &&
      ((options.nullValueRepresentation === 'nilAttribute' &&
        ns[XSI_NS] &&
        lib['/attributes'] &&
        lib['/attributes'][withPrefix(ns, XSI_NS, 'nil')] === 'true') ||
        (options.nullValueRepresentation === 'emptyElement' &&
          libElementIsEmpty(lib)))
    ) {
      js = null;
    } else if (isSimpleSchema(schema)) {
      const text = lib['/text'] || '';

      // Run custom simple deserialisers
      let deserialised = runSimpleDeserializers(text, schema, path, options);

      // Perform "standard" deserialisation when no deserialiser changed the
      // value
      if (text === deserialised) {
        deserialised = isBooleanSchema(schema)
          ? text === 'true' || text === '1' // Comply with `xs:boolean` type
          : isDateSchema(schema)
          ? new Date(text)
          : isNumberSchema(schema)
          ? +text
          : text;
      }
      js = deserialised;
    } else if (isListSchema(schema) || isTableSchema(schema)) {
      const chSchema = isListSchema(schema)
        ? schema.elementsSchema
        : schema.rowsSchema;
      const chPathP = appendToPath(pathP, PATH_ID_PLACEHOLDER);
      const chName = chElName(chSchema, chPathP);
      const children = arrayfy(lib[chName]);
      for (let i = 0; i < children.length; ++i) {
        children[i] = chLibToJs(children[i], chName, chSchema, chPathP, i);
      }
      js = children;
    } else if (isMapSchema(schema)) {
      const chSchema = schema.valuesSchema;
      const chPathP = appendToPath(pathP, PATH_ID_PLACEHOLDER);
      const chName = chElName(chSchema, chPathP);
      js = new Map(
        arrayfy(lib[chName]).map((ch) => {
          const k =
            ch['/attributes'][mapValueKeyAttributeName(schema, options)];
          return [k, chLibToJs(ch, chName, chSchema, chPathP, k)];
        }) as Array<[string, any]>
      );
    }
    // Record or tuple
    else {
      const chSchemas = isRecordSchema(schema)
        ? schema.fieldsSchemas
        : schema.elementsSchemas;
      js = Object.keys(chSchemas).reduce(
        (obj, id) => {
          const chSchema = chSchemas[id];
          const chPathP = appendToPath(pathP, id);
          const chName = chElName(chSchema, chPathP, id);
          // Set `undefined` in tuples with client-only schemas
          if (isClientOnlySchema(chSchema)) {
            isTupleSchema(schema) && (obj[id] = undefined);
          } else {
            obj[id] = chLibToJs(lib[chName], chName, chSchema, chPathP, id);
          }
          return obj;
        },
        isRecordSchema(schema) ? {} : []
      );
    }

    // Deserialise XML attributes
    const libAttrs = lib['/attributes'] || {};
    deserializeAttributes(js, libAttrs, schema, path, pathP, options);
  }

  return js;
}

/**
 * Whether a given lib object represents an empty XML element.
 * @param lib `xml-js` representation of an XML element.
 * @returns Whether the lib object represents an empty XML element.
 */
function libElementIsEmpty(lib: any): boolean {
  return !lib['/text'] && Object.keys(lib).every((prop) => prop[0] === '/');
}
