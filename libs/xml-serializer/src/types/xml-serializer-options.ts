import { Schema } from '@lightweightform/storage';

/**
 * Type of object used to apply transformations during a (de)serialisation.
 * @param S Type of the serialisation function.
 * @param D Type of the deserialisation function.
 */
export interface LfXmlTransformer<S, D = S> {
  /**
   * Serialisation function.
   */
  serialize?: S;
  /**
   * Deserialisation function.
   */
  deserialize?: D;
}

/**
 * Function used to (de)serialise a value.
 * @param value Value to (de)serialise.
 * @param schema Schema of the value being (de)serialised.
 * @param path Path of the value being (de)serialised.
 * @returns (De)serialised value (not necessarily the last form since the value
 * will be passed to the next (de)serialiser).
 */
export type LfXmlTransformationFn = (
  value: any,
  schema: Schema,
  path: string
) => any;

/**
 * Function used to set a custom attribute of an XML element.
 * @param js Storage-compatible JS value on which to set XML attribute.
 * @param schema Schema of the value on which to set XML attribute.
 * @param path Path of the value on which to set XML attribute.
 * @returns Value of attribute to set.
 */
export type LfXmlElementAttributeSerializerFn = (
  js: any,
  schema: Schema,
  path: string
) => string;

/**
 * Function used to unset a custom attribute of an XML element.
 * @param js Storage-compatible JS value from which to unset XML attribute.
 * @param value Value of the attribute to unset.
 * @param schema Schema of the value from which to unset XML attribute.
 * @param path Path of the value from which to unset XML attribute.
 */
export type LfXmlElementAttributeDeserializerFn = (
  js: any,
  value: string,
  schema: Schema,
  path: string
) => void;

/**
 * Record used to provide attributes to an XML element.
 */
export type LfXmlElementAttributes = Record<
  string,
  | string
  | LfXmlElementAttributeSerializerFn
  | LfXmlTransformer<
      LfXmlElementAttributeSerializerFn,
      LfXmlElementAttributeDeserializerFn
    >
>;

/**
 * Options used to configure the XML serialiser.
 */
export interface LfXmlSerializerOptions {
  /**
   * Character encoding used to save the file or load from the file.
   */
  charset?: string;
  /**
   * Whether to automatically add an UTF BOM (not applied to IE9-). Defaults to
   * `true`.
   */
  autoBOM?: boolean;
  /**
   * A function used to encode the content according to the intended charset.
   * This is something that JavaScript cannot natively do (if not provided, the
   * provided charset will be set as file metadata but the content will be
   * encoded as `utf-8`).
   */
  encode?: (content: string) => BlobPart[];
  /**
   * Space used when printing the value as an XML string. Defaults to `0` (no
   * spaces) or `'\t'` (use tabs as spaces) depending on whether the app is
   * running in production mode.
   */
  space?: number | string;
  /**
   * Whether to trim whitespace characters that may exist before and after text
   * when deserialising. Defaults to `false`.
   */
  trim?: boolean;
  /**
   * Whether to emit an XML declaration (`<?xml ...?>`) when serialising a
   * value. Defaults to `true`.
   */
  emitDeclaration?: boolean;
  /**
   * Version to set in the XML declaration (only relevant when
   * `emitDeclaration` is `true`). Defaults to `'1.0'`.
   */
  declarationVersion?: string;
  /**
   * Encoding to set in the XML declaration (only relevant when
   * `emitDeclaration` is `true`). Defaults to the value of the `charset` in
   * upper case.
   */
  declarationEncoding?: string;
  /**
   * Value for the "standalone" property in the XML declaration (only relevant
   * when `emitDeclaration` is `true`). Defaults to `undefined` (i.e. the
   * property isn't set).
   */
  declarationStandalone?: 'yes' | 'no';
  /**
   * Simple transformer or list of simple transformers: a simple transformer is
   * an object which provides functions to (de)serialise non-`null` values of
   * simple schemas (boolean, date, number, and string schemas).
   *
   * When a list of simple transformers is provided, they run sequentially (in
   * reverse order when deserialising). These \[serialisers|deserialisers] will
   * run \[before|after] the \[serialiser|deserialiser] defined in a schema's
   * `xmlSimpleTransform` respectively.
   *
   * Return `undefined` or the provided value to perform no transformation. The
   * XML serialiser service will perform its standard transformation when the
   * value remains the same after all simple transformations run.
   *
   * The following example serialises booleans as "yes/no":
   * ```typescript
   * {
   *   simpleTransform: {
   *     serialize: (val, schema, path) => {
   *       if (isBooleanSchema(schema)) {
   *         return val ? 'yes' : 'no';
   *       }
   *     },
   *     deserialize: (val, schema, path) => {
   *       if (isBooleanSchema(schema)) {
   *         return val === 'yes';
   *       }
   *     },
   *   }
   * }
   * ```
   */
  simpleTransform?:
    | LfXmlTransformer<LfXmlTransformationFn>
    | Array<LfXmlTransformer<LfXmlTransformationFn>>;
  /**
   * Transformer or list of transformers: a transformer is an object providing
   * functions to (de)serialise values. Serialisers transform values after they
   * have been serialised by the XML serialiser service but before they are
   * serialised by the `xml-js` library. Deserialisers transform values after
   * they have been deserialised by the `xml-js` library but before they are
   * deserialised by the XML serialiser service.
   *
   * When a list of transformers are provided, they will run sequentially (in
   * reverse order when deserialising). These serialisers/deserialisers will run
   * before/after the serialiser/deserialiser defined in a schema's
   * `xmlTransform` respectively. To perform no transformation, functions should
   * return the provided value untouched.
   *
   * Using a transformer requires understanding the format used by the
   * underlying library [`xml-js`](https://github.com/nashwaan/xml-js). We use
   * `xml-js` in "compact" mode where each "key name" is prefixed with a forward
   * slash. E.g. to set an element's attributes we use the `'/attributes'`
   * property, or `'/text'` when specifying an element's text content.
   */
  transform?:
    | LfXmlTransformer<LfXmlTransformationFn>
    | Array<LfXmlTransformer<LfXmlTransformationFn>>;
  /**
   * How to represents `null` values: by setting an `xsi:nil` attribute or by
   * simply leaving the element empty. Note that selecting `'emptyElement'` will
   * cause the deserialiser to not be able to distinguish values with empty
   * strings or empty collections from `null` elements (in which case the value
   * will become `null` when the schema is nullable). Defaults to
   * `'nilAttribute'`.
   */
  nullValueRepresentation?: 'nilAttribute' | 'emptyElement';
  /**
   * Mapping of element names: map from the element's path to its XML element
   * name. Children of collections must use placeholders (`?`) within their
   * paths. Example naming the root element `rootElement` and elements of a list
   * of "people" `person`:
   * ```typescript
   * {
   *   elementNames: {
   *     '/': 'rootElement',
   *     '/people/?': 'person'
   *   }
   * }
   * ```
   * Note that defining an `xmlElementName` property in a schema has priority
   * over these mappings.
   */
  elementNames?: Record<string, string>;
  /**
   * Mapping of XML attributes to add to each element: map from the element's
   * path to its map of attributes (from the attribute to its value). Children
   * of collections must use placeholders (`?`) within their paths.
   *
   * It is possible to provide a function as the value of an attribute or a
   * transformer to serialise/deserialise the attribute. This allows the value
   * of the attribute to depend on the value the XML element represents.
   * Provided serialisers may manipulate the passed value, removing fields that
   * should no longer be serialised (since they are to be serialised as
   * attributes).
   *
   * The following example sets a "version" attribute in the root element and
   * uses a person's passport id as an attribute instead of an element.
   * ```typescript
   * {
   *   elementAttributes: {
   *     '/': {version: '2.0'},
   *     '/people/?': {
   *       passportId: {
   *         serialize: person => {
   *           const id = person.passportId;
   *           delete person.passportId;
   *           return id;
   *         },
   *         deserialize: (person, id) => {
   *           person.passportId = id;
   *         }
   *       }
   *     }
   *   }
   * }
   * ```
   * Note that defining an `xmlElementAttributes` property in a schema has
   * priority over these attributes when they are merged.
   */
  elementAttributes?: Record<string, LfXmlElementAttributes>;
  /**
   * Namespaces to set in the root XML element. Mapping of the namespace URI to
   * its prefix. Use the empty string as the prefix to set the default
   * namespace. Example:
   * ```typescript
   * {
   *   namespaces: {
   *     'http://some.uri': '',
   *     'http://another.uri': 'other'
   *   }
   * }
   * ```
   * The above namespaces would set the following in the root element:
   * ```xml
   * <app xmlns="http://some.uri" xmlns:other="http://another.uri">
   * ```
   */
  namespaces?: Record<string, string>;
  /**
   * Suffix used to name the XML elements that are children of a list (when they
   * do not specify an XML element name themselves). The child element's XML
   * name will be a concatenation of the list's element name with the value of
   * this property. Defaults to `Element`.
   */
  listElementSuffix?: string;
  /**
   * Suffix used to name the XML elements that are children of a map (when they
   * do not specify an XML element name themselves). The child element's XML
   * name will be a concatenation of the map's element name with the value of
   * this property. Defaults to `Value`.
   */
  mapValueSuffix?: string;
  /**
   * Suffix used to name the XML elements that are children of a table (when
   * they do not specify an XML element name themselves). The child element's
   * XML name will be a concatenation of the table's element name with the value
   * of this property. Defaults to `Row`.
   */
  tableRowSuffix?: string;
  /**
   * Name of the XML attribute on children elements of maps used to specify the
   * value's key (when the map schema doesn't define `xmlKeyAttributeName`).
   * Defaults to `key`.
   */
  mapValueKeyAttributeName?: string;
  /**
   * Whether to produce XML elements without sub-elements as self-closing. I.e.
   * produce `<a/>` instead of `<a></a>`. Defaults to `true`.
   */
  selfCloseEmptyElements?: boolean;
}
