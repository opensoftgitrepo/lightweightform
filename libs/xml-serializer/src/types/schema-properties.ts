import {
  LfXmlElementAttributes,
  LfXmlTransformationFn,
  LfXmlTransformer,
} from './xml-serializer-options';

/**
 * Extra properties that can be added to any schema to configure how its values
 * should be serialised by the XML serialiser.
 */
export interface LfXmlSerializerSchemaProperties {
  /**
   * Name of the XML element associated to values of this schema.
   */
  xmlElementName?: string;
  /**
   * XML element attributes to set on elements associated to values of this
   * schema. Mapping from XML attribute to its value.
   *
   * It is possible to provide a function as the value of an attribute or a
   * transformer to serialise/deserialise the attribute. This allows the value
   * of the attribute to depend on the value the XML element represents.
   * Provided serialisers may manipulate the passed value, removing fields that
   * should no longer be serialised (since they are to be serialised as
   * attributes).
   */
  xmlElementAttributes?: LfXmlElementAttributes;
  /**
   * Applicable only to map schemas: this property determines the name of the
   * XML property on child XML elements used to specify the value's key.
   */
  xmlKeyAttributeName?: string;
  /**
   * Simple transformer: an object which provides functions to (de)serialise
   * non-`null` values of simple schemas (boolean, date, number, and string
   * schemas).
   *
   * This \[serialiser|deserialiser] will run \[after|before] the
   * \[serialisers|deserialisers] defined in the XML serialiser service
   * `simpleTransform` option respectively.
   */
  xmlSimpleTransform?: LfXmlTransformer<LfXmlTransformationFn>;
  /**
   * Transformer: an object providing functions to (de)serialise values of this
   * schema. The serialiser transforms values after they have been serialised by
   * the XML serialiser service but before they are serialised by the `xml-js`
   * library. The deserialiser transforms values after they have been
   * deserialised by the `xml-js` library but before they are deserialised by
   * the XML serialiser service.
   *
   * This \[serialiser|deserialiser] will run \[after|before] the
   * \[serialisers|deserialisers] defined in the XML serialiser service
   * `transform` option respectively. To perform no transformation, functions
   * should return the provided value untouched.
   *
   * Using a transformer requires understanding the format used by the
   * underlying library [`xml-js`](https://github.com/nashwaan/xml-js). We use
   * `xml-js` in "compact" mode where each "key name" is prefixed with a forward
   * slash. E.g. to set an element's attributes we use the `'/attributes'`
   * property, or `'/text'` when specifying an element's text content.
   */
  xmlTransform?: LfXmlTransformer<LfXmlTransformationFn>;
  /**
   * Low-level deserialisation function to apply to values of this schema after
   * they have been deserialised by the `xml-js` library but before they are
   * deserialised by the XML serialiser service. Using this function requires
   * understanding the format used by the underlying library used by the XML
   * serialiser service: [`xml-js`](https://github.com/nashwaan/xml-js). We use
   * `xml-js` in "non-compact" mode with default key names.
   *
   * This function receives a "non-compact" JS value as understood by `xml-js`
   * (which represents a value of this schema) and should return a value of the
   * same kind which will then be further deserialised to produce a
   * storage-compatible JS value. This function should "undo" whatever the
   * `xmlSerialize` function does.
   *
   * This function runs before all the custom deserialisation functions provided
   * to the XML serialiser service.
   */
  xmlDeserialize?: LfXmlTransformationFn;
}
