# Lightweightform/xml-serializer

Lightweightform serialiser for XML using the `xml-js` library.

Supports:

- Configuring declarations
- Custom (de)serialisers (specified via options or directly in the schema)
- Multiple representations of the `null` value
- Custom element names (specified via options or directly in the schema)
- Custom element attributes (specified via options or directly in the schema)
- Limited support for namespaces

## Documentation

All Lightweightform documentation is available at:
https://docs.lightweightform.io.

The `@lightweightform/xml-serializer` API is available at:
https://docs.lightweightform.io/api/xml-serializer.

## Browser support

- **Internet Explorer**: 9+ (requires `Map` and `Array.from` polyfills)
- **Other browsers**: Latest 2 versions
