# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.13.1"></a>

## [4.13.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.13.0&sourceBranch=refs%2Ftags%2Fv4.13.1) (2022-02-03)

**Note:** Version bump only for package @lightweightform/theme-common

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.13.0"></a>

# [4.13.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.12.2&sourceBranch=refs%2Ftags%2Fv4.13.0) (2022-01-27)

### Bug Fixes

- **theme-common:** type error in schematics
  ([21636df](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/21636df))

### Features

- update dependencies
  ([f921338](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/f921338))

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.10.2"></a>

## [4.10.2](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.10.1&sourceBranch=refs%2Ftags%2Fv4.10.2) (2021-09-13)

### Bug Fixes

- **theme-common:** fix build issue
  ([83651be](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/83651be))
- **theme-common:** table column insertion order
  ([a7352e3](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/a7352e3))

<a name="4.10.0"></a>

# [4.10.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.9.2&sourceBranch=refs%2Ftags%2Fv4.10.0) (2021-07-14)

### Features

- **theme-common:** support `minWidth` array
  ([d1c9b13](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/d1c9b13))

<a name="4.9.0"></a>

# [4.9.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.8.3&sourceBranch=refs%2Ftags%2Fv4.9.0) (2021-07-05)

### Features

- **theme-common:** colspan support on columns
  ([e9b131b](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/e9b131b))

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.7.5"></a>

## [4.7.5](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.7.4&sourceBranch=refs%2Ftags%2Fv4.7.5) (2021-03-10)

### Bug Fixes

- **theme-common:** make addNgModuleImport receive opt importExpression
  ([589f54e](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/589f54e)),
  closes #52

<a name="4.7.0"></a>

# [4.7.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.6.1&sourceBranch=refs%2Ftags%2Fv4.7.0) (2020-12-15)

**Note:** Version bump only for package @lightweightform/theme-common

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.5.4"></a>

## [4.5.4](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.5.3&sourceBranch=refs%2Ftags%2Fv4.5.4) (2020-10-28)

**Note:** Version bump only for package @lightweightform/theme-common

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.5.2"></a>

## [4.5.2](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.5.1&sourceBranch=refs%2Ftags%2Fv4.5.2) (2020-02-19)

**Note:** Version bump only for package @lightweightform/theme-common

<a name="4.5.1"></a>

## [4.5.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.5.0&sourceBranch=refs%2Ftags%2Fv4.5.1) (2020-02-19)

### Bug Fixes

- peer dependencies
  ([b212247](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/b212247))

<a name="4.5.0"></a>

# [4.5.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.4.6&sourceBranch=refs%2Ftags%2Fv4.5.0) (2020-02-19)

### Features

- support Angular 9
  ([54a7154](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/54a7154))

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.4.5"></a>

## [4.4.5](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.4.4&sourceBranch=refs%2Ftags%2Fv4.4.5) (2020-01-23)

**Note:** Version bump only for package @lightweightform/theme-common

<a name="4.4.4"></a>

## [4.4.4](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.4.3&sourceBranch=refs%2Ftags%2Fv4.4.4) (2020-01-22)

**Note:** Version bump only for package @lightweightform/theme-common

<a name="4.4.3"></a>

## [4.4.3](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.4.2&sourceBranch=refs%2Ftags%2Fv4.4.3) (2020-01-19)

**Note:** Version bump only for package @lightweightform/theme-common

<a name="4.4.2"></a>

## [4.4.2](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.4.1&sourceBranch=refs%2Ftags%2Fv4.4.2) (2020-01-18)

**Note:** Version bump only for package @lightweightform/theme-common

<a name="4.4.1"></a>

## [4.4.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.4.0&sourceBranch=refs%2Ftags%2Fv4.4.1) (2020-01-14)

### Bug Fixes

- repo-wide spellchecks + small improvements
  ([90545fb](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/90545fb))

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.4.0"></a>

# [4.4.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.3.0&sourceBranch=refs%2Ftags%2Fv4.4.0) (2020-01-06)

### Features

- **theme-common:** schematic utils improvements
  ([789b49c](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/789b49c))

<a name="4.3.0"></a>

# [4.3.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.2.0&sourceBranch=refs%2Ftags%2Fv4.3.0) (2019-12-26)

### Features

- **numeric-input:** introduce numeric-input
  ([7ee28ff](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/7ee28ff)),
  closes #44

<a name="4.2.0"></a>

# [4.2.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.1.0&sourceBranch=refs%2Ftags%2Fv4.2.0) (2019-12-08)

### Features

- support AoT compilation
  ([bd9e886](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/bd9e886)),
  closes #43

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.1.0"></a>

# [4.1.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.0.1&sourceBranch=refs%2Ftags%2Fv4.1.0) (2019-11-10)

### Bug Fixes

- **bootstrap-theme:** fix schematics on Windows
  ([c7f3575](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/c7f3575))
- **bootstrap-theme:** form schematic on Windows
  ([a6c611a](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/a6c611a))

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.0.0"></a>

# [4.0.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.0.0-beta.19&sourceBranch=refs%2Ftags%2Fv4.0.0) (2019-10-19)

Initial LF 4 release.
