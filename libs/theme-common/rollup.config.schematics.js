import nodeResolve from '@rollup/plugin-node-resolve';
import typescript from 'rollup-plugin-typescript2';

// Catch inner packages as well, e.g. `@schematics/angular/utility/config`
const external = (id) => {
  const externals = [
    '@angular-devkit/core',
    '@angular-devkit/schematics',
    '@schematics/angular',
  ];
  return (
    externals.find(
      (e) => id.startsWith(e) && (!id[e.length] || id[e.length] === '/')
    ) !== undefined
  );
};

const plugins = [
  nodeResolve(),
  typescript({
    tsconfig: '../../tsconfig.schematics.json',
    tsconfigOverride: { include: [`${__dirname}/src/schematics/**/*`] },
  }),
];

export default {
  input: 'src/schematics/index.ts',
  external,
  plugins,
  output: {
    file: 'dist/schematics/index.js',
    format: 'cjs',
    sourcemap: true,
  },
};
