import { ScrollbarUtils } from '../scrollbar-utils.service';

describe('Scrollbar utilities', () => {
  const scrollbarUtils = new ScrollbarUtils();

  it('allows getting scrollbar sizes', () => {
    // Not worth it to actually mock these
    expect(typeof scrollbarUtils.horizontalScrollbarHeight).toBe('number');
    expect(typeof scrollbarUtils.verticalScrollbarWidth).toBe('number');
  });

  it('checks if an element is overflowing', () => {
    const el1: any = {
      scrollHeight: 200,
      scrollWidth: 200,
      clientHeight: 100,
      clientWidth: 100,
    };
    const el2: any = {
      scrollHeight: 100,
      scrollWidth: 100,
      clientHeight: 100,
      clientWidth: 100,
    };
    expect(scrollbarUtils.hasVerticalOverflow(el1)).toBe(true);
    expect(scrollbarUtils.hasHorizontalOverflow(el1)).toBe(true);
    expect(scrollbarUtils.hasVerticalOverflow(el2)).toBe(false);
    expect(scrollbarUtils.hasHorizontalOverflow(el2)).toBe(false);
  });
});
