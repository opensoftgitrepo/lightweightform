import { Injectable } from '@angular/core';

/**
 * Service providing scrolling-related utilities.
 */
@Injectable({ providedIn: 'root' })
export class ScrollbarUtils {
  // Only compute sizes once (this assumes that browser scrollbar sizes don't
  // change over time, which seems like a valid assumption)
  private static scrollbarWidth?: number = undefined;
  private static scrollbarHeight?: number = undefined;

  /**
   * Computes the width/height (in px) of vertical/horizontal scrollbars in the
   * user's browser and caches the results. This method should only ever be
   * called once.
   */
  private static computeScrollbarSizes(): void {
    const div = document.createElement('div');
    div.innerHTML = `<div style="overflow:auto; position:absolute; top:0;
                                 width:100px; height:100px;">
                       <div style="width:200px; height:200px;"></div>
                     </div>`;
    const child = div.firstChild as HTMLDivElement;
    document.body.appendChild(div);
    ScrollbarUtils.scrollbarWidth = child.offsetWidth - child.clientWidth;
    ScrollbarUtils.scrollbarHeight = child.offsetHeight - child.clientHeight;
    document.body.removeChild(div);
  }

  /**
   * Width (in px) of a vertical scrollbar in the user's browser. This value is
   * only computed once and cached for subsequent calls.
   * @returns Width of the vertical scrollbar in the user's browser.
   */
  public get verticalScrollbarWidth(): number {
    if (ScrollbarUtils.scrollbarWidth === undefined) {
      ScrollbarUtils.computeScrollbarSizes();
    }
    return ScrollbarUtils.scrollbarWidth!;
  }

  /**
   * Height (in px) of a horizontal scrollbar in the user's browser. This value
   * is only computed once and cached for subsequent calls.
   * @returns Height of the horizontal scrollbar in the user's browser.
   */
  public get horizontalScrollbarHeight(): number {
    if (ScrollbarUtils.scrollbarHeight === undefined) {
      ScrollbarUtils.computeScrollbarSizes();
    }
    return ScrollbarUtils.scrollbarHeight!;
  }

  /**
   * Whether a given DOM element has a vertical scrollbar.
   * @param element Element to check for vertical scrollbar.
   * @returns Whether the provided element has a vertical scrollbar.
   */
  // FIXME: Unreliable in IE/Edge
  public hasVerticalOverflow(element: HTMLElement): boolean {
    return element.scrollHeight > element.clientHeight;
  }

  /**
   * Whether a given DOM element has a horizontal scrollbar.
   * @param element Element to check for horizontal scrollbar.
   * @returns Whether the provided element has a horizontal scrollbar.
   */
  // FIXME: Unreliable in IE/Edge
  public hasHorizontalOverflow(element: HTMLElement): boolean {
    return element.scrollWidth > element.clientWidth;
  }
}
