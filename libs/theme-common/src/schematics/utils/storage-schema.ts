import { dirname, join, Path, resolve } from '@angular-devkit/core';
import { SchematicsException, Tree } from '@angular-devkit/schematics';
import ts from '@schematics/angular/third_party/github.com/Microsoft/TypeScript/lib/typescript';

import { applyChanges } from './changes';
import { SchemaExplorer } from './schema-explorer';
import {
  insertInNodeArray,
  insertTsImport,
  propertyAsString,
  relativeTsImportPath,
  stringAsProperty,
  TsImport,
} from './ts-common';

/**
 * Suffix used within schema file names.
 */
const schemaFileRegExp = /\.schema\.ts$/;

/**
 * Returns the root schema for a given form.
 * @param host Source tree.
 * @param moduleDir Path of the module directory where the root schema should be
 * found.
 * @returns Path of the root schema.
 */
export function getRootSchemaPath(host: Tree, moduleDir: Path): Path {
  const schemaFile = host
    .getDir(moduleDir)
    .subfiles.find((file) => schemaFileRegExp.test(file));
  if (!schemaFile) {
    throw new SchematicsException(
      `Could not find the root schema file at: "${moduleDir.slice(1)}"`
    );
  }
  return join(moduleDir, schemaFile);
}

/**
 * Gets the parent storage path of a form's schema given its file path.
 * @param schemaExplorer `SchemaExplorer` instance.
 * @param schemaPath File path of the schema for which to find the parent
 * storage path.
 * @returns Storage path of the parent schema.
 */
export function getParentStoragePath(
  schemaExplorer: SchemaExplorer,
  schemaPath: Path
): string {
  for (
    let dir = dirname(schemaPath);
    dir !== resolve(dir, '..' as Path);
    dir = resolve(dir, '..' as Path)
  ) {
    const schemaFiles = schemaExplorer.getSchemaFilePathsFromDir(dir);
    if (schemaFiles.length > 0) {
      return schemaExplorer.getSchemaInfoFromFilePath(schemaFiles[0])[0]
        .storagePath;
    }
  }
  throw new SchematicsException(
    'Could not determine the storage path for the schema at: ' +
      `"${schemaPath.slice(1)}".`
  );
}

/**
 * Adds a schema as a field of its parent record schema.
 * @param host Source tree.
 * @param schemaExplorer `SchemaExplorer` instance.
 * @param schemaPath File path of the schema being added.
 * @param tsSchemaName Name of the identifier being imported in the parent
 * schema.
 * @param storagePathId Identifier to use for access in the storage.
 * @param parentStoragePath Storage path of the parent record schema.
 */
export function addSchemaToParentSchema(
  host: Tree,
  schemaExplorer: SchemaExplorer,
  schemaPath: Path,
  tsSchemaName: string,
  storagePathId: string,
  parentStoragePath: string
): void {
  const schemaInfo =
    schemaExplorer.getSchemaInfoFromStoragePath(parentStoragePath);
  if (!schemaInfo || schemaInfo.type !== 'record') {
    throw new SchematicsException(
      `No record schema found with the storage path: "${parentStoragePath}".`
    );
  }
  const parentSchemaPath = schemaInfo.fileInfo.filePath;
  if (!schemaInfo.recordObject) {
    throw new SchematicsException(
      'Could not access the object record of the record schema at: ' +
        `"${parentSchemaPath}".`
    );
  }

  // Don't add an already added property
  const props = schemaInfo.recordObject.properties;
  if (
    props.find(
      (p) =>
        ts.isPropertyAssignment(p) &&
        propertyAsString(p.name.getText()) === storagePathId
    )
  ) {
    throw new SchematicsException(
      `Parent schema at "${parentSchemaPath.slice(1)}" already contains a ` +
        `child schema with id "${storagePathId}".`
    );
  }

  const changes = [
    insertTsImport(
      schemaInfo.fileInfo.source,
      parentSchemaPath,
      new TsImport(
        tsSchemaName,
        relativeTsImportPath(parentSchemaPath, schemaPath)
      )
    ),
    insertInNodeArray(
      parentSchemaPath,
      props,
      `${stringAsProperty(storagePathId)}: ${tsSchemaName}`
    ),
  ];

  applyChanges(host, parentSchemaPath, changes);
}
