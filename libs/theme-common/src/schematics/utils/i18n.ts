import { join, Path } from '@angular-devkit/core';
import { SchematicsException, Tree } from '@angular-devkit/schematics';
import ts from '@schematics/angular/third_party/github.com/Microsoft/TypeScript/lib/typescript';
import { Change } from '@schematics/angular/utility/change';

import { applyChanges } from './changes';
import {
  getTsSourceFile,
  insertInNodeArray,
  insertTsImport,
  propertyAsString,
  relativeTsImportPath,
  stringAsProperty,
  TsImport,
} from './ts-common';

/**
 * Suffix used for the root i18n file.
 */
const i18nFileRegExp = /\.i18n\.ts$/;

/**
 * Returns the root i18n for a given form.
 * @param host Source tree.
 * @param moduleDir Path of the module directory where the root i18n should be
 * found.
 * @returns Path of the root i18n.
 */
export function getRootI18nPath(host: Tree, moduleDir: Path): Path {
  const i18nFile = host
    .getDir(moduleDir)
    .subfiles.find((file) => i18nFileRegExp.test(file));
  if (!i18nFile) {
    throw new SchematicsException(
      `Could not find the root i18n file at: "${moduleDir.slice(1)}"`
    );
  }
  return join(moduleDir, i18nFile);
}

/**
 * Adds a form's locale to the root i18n file.
 * @param host Source tree.
 * @param rootI18nPath Path of the root i18n.
 * @param locale Locale being added.
 * @param localePath Path of the form's locale.
 * @param localeTsName Exported named of the form's locale.
 */
export function addFormLocaleToRootI18n(
  host: Tree,
  rootI18nPath: Path,
  locale: string,
  localePath: Path,
  localeTsName: string
): void {
  const source = getTsSourceFile(host, rootI18nPath);
  const i18nObj = getExportedI18nObject(source);
  if (!i18nObj) {
    throw new SchematicsException(
      `Could not find an exported i18n object in "${rootI18nPath.slice(1)}".`
    );
  }

  const relativeLocaleLocation = relativeTsImportPath(rootI18nPath, localePath);
  const changes: Change[] = [
    insertTsImport(
      source,
      rootI18nPath,
      new TsImport(localeTsName, relativeLocaleLocation)
    ),
  ];

  // Property for the provided locale
  const props = i18nObj.properties;
  const relevantProp = props.find(
    (p) =>
      ts.isPropertyAssignment(p) &&
      propertyAsString(p.name.getText()) === locale
  ) as ts.PropertyAssignment | undefined;
  if (relevantProp) {
    // Locale found
    const propInit = relevantProp.initializer;
    const mergeArgs = ts.isCallExpression(propInit) && propInit.arguments;
    if (!mergeArgs) {
      return console.error(
        `Unable to add the i18n object exported by "${localePath.slice(1)}" ` +
          `to "${rootI18nPath.slice(1)}". Please make sure to add the object ` +
          'manually.'
      );
    }
    changes.push(insertInNodeArray(rootI18nPath, mergeArgs, localeTsName));
  } else {
    // Locale not found (create it)
    const localeProp = stringAsProperty(locale);
    changes.push(
      insertInNodeArray(
        rootI18nPath,
        props,
        `${localeProp}: LfI18n.mergeTranslations(${localeTsName})`
      ),
      insertTsImport(
        source,
        rootI18nPath,
        new TsImport('LfI18n', '@lightweightform/core')
      )
    );
  }

  applyChanges(host, rootI18nPath, changes);
}

/**
 * Returns the exported i18n object.
 * @param source TS source code.
 * @returns Exported i18n object.
 */
function getExportedI18nObject(
  source: ts.SourceFile
): ts.ObjectLiteralExpression | undefined {
  // Exported declarations
  const decls: ts.VariableDeclaration[] = source.statements
    .filter(
      (s: ts.Statement) =>
        ts.isVariableStatement(s) &&
        s.modifiers &&
        s.modifiers.find((m) => m.kind === ts.SyntaxKind.ExportKeyword)
    )
    .flatMap((v: ts.VariableStatement) => v.declarationList.declarations);
  // Get the first declaration exporting an object.
  const decl = decls.find(
    (d: ts.VariableDeclaration) =>
      !!d.initializer && ts.isObjectLiteralExpression(d.initializer)
  );
  return decl && (decl.initializer as ts.ObjectLiteralExpression);
}
