import { dirname, Path, relative } from '@angular-devkit/core';
import { SchematicsException, Tree } from '@angular-devkit/schematics';
import ts from '@schematics/angular/third_party/github.com/Microsoft/TypeScript/lib/typescript';
import {
  insertAfterLastOccurrence,
  insertImport,
} from '@schematics/angular/utility/ast-utils';
import {
  Change,
  NoopChange,
  RemoveChange,
} from '@schematics/angular/utility/change';

import { applyChanges } from './changes';
import { getIndentation } from './indentantion';

/**
 * RegExp used to determine whether a string is a valid TS identifier.
 */
export const validTsIdRegExp = /^[a-z_$][\w$]*$/i;

/**
 * Symbol representing a "default binding" (import or export).
 */
export const defaultBinding = Symbol('Default binding');

/**
 * Class used to insert a new import in a TS file.
 */
export class TsImport {
  /**
   * Creates a new TS import.
   * @param name Name to import (or the default import symbol). `null` to import
   * no name (e.g. `import 'module';`).
   * @param location Import location.
   * @param as Identifier to import as (use also when importing a default).
   */
  constructor(
    public name: string | typeof defaultBinding,
    public location: string,
    public as?: string
  ) {}
}

/**
 * Source of a given TypeScript file given its path.
 * @param host Source tree.
 * @param path File path for which to fetch source.
 * @returns TypeScript source file.
 */
export function getTsSourceFile(host: Tree, path: Path): ts.SourceFile {
  const buffer = host.read(path);
  if (!buffer) {
    throw new SchematicsException(`File not found: "${path.slice(1)}".`);
  }
  return ts.createSourceFile(
    path,
    buffer.toString('utf-8'),
    ts.ScriptTarget.Latest,
    true
  );
}

/**
 * Location used to import a file relatively to another file in a TS import.
 * @param sourcePath File path where the import is to be inserted.
 * @param importPath File being imported.
 * @returns Relative file path from the source file to the one being imported.
 */
export function relativeTsImportPath(
  sourcePath: Path,
  importPath: Path
): string {
  const rel = relative(dirname(sourcePath), importPath).replace(/\.ts$/, '');
  return rel.startsWith('../') ? rel : `./${rel}`;
}

/**
 * Creates a change that inserts a new TS import.
 * @param source TS source.
 * @param path Path of the TS file on which to import.
 * @param toImport Import object representing import to add.
 * @returns Change to apply.
 */
export function insertTsImport(
  source: ts.SourceFile,
  path: Path,
  toImport: TsImport
): Change {
  const isDefault = toImport.name === defaultBinding;
  let name = isDefault ? toImport.as! : (toImport.name as string);
  // XXX: `insertImport` is not prepared to deal with import aliases so we fake
  // an import without the alias to see if there would be a change, and if so we
  // set the symbol as a string with the alias
  if (!isDefault && toImport.as && name !== toImport.as) {
    const mockImportChange = insertImport(
      source,
      path,
      name,
      toImport.location
    );
    if (mockImportChange instanceof NoopChange) {
      return mockImportChange;
    }
    name = `${toImport.name as string} as ${toImport.as}`;
  }
  return insertImport(source, path, name, toImport.location, isDefault);
}

/**
 * Creates a change that removes a TS import.
 * @param source TS source.
 * @param path Path of the TS file on which to remove the import.
 * @param name Import name (or the default import symbol).
 * @param location Import location.
 */
export function removeTsImport(
  source: ts.SourceFile,
  path: Path,
  name: string | typeof defaultBinding,
  location: string
): Change {
  const importStats = getLocationImports(source, location);
  for (const importStat of importStats) {
    const importClause = importStat.importClause;
    if (!importClause) {
      continue;
    }
    const importClauseStr = importClause.getText();
    if (name === defaultBinding) {
      // Remove default import
      const match = matchDefaultImport(importClauseStr);
      if (match) {
        if (match[0] === importClauseStr) {
          // Remove whole statement
          return new RemoveChange(path, importStat.pos, importStat.getText());
        } else {
          // Remove just the default import
          return new RemoveChange(
            path,
            importClause.pos + importClauseStr.indexOf(match[0]) + 1,
            match[0]
          );
        }
      }
    } else {
      // Remove named binding
      const match = matchNamedBindingImport(importClauseStr, name);
      if (match) {
        if (
          (importClause.namedBindings as ts.NamedImports).elements.length === 1
        ) {
          if (match[0] === importClauseStr) {
            // Remove whole statement
            return new RemoveChange(path, importStat.pos, importClauseStr);
          } else {
            // Remove just the named imports group
            return new RemoveChange(
              path,
              importClause.pos + importClauseStr.indexOf(match[0]) + 1,
              match[0]
            );
          }
        } else {
          // Remove just the named import
          return new RemoveChange(
            path,
            importClause.pos + importClauseStr.indexOf(match[1]) + 1,
            match[1]
          );
        }
      }
    }
  }
  return new NoopChange();
}

/**
 * Obtains the id with which a certain name was imported in a given file,
 * `undefined` if the given name hasn't been imported. E.g., let `source`
 * contain:
 * ```ts
 * import {w, x as y} from 'location';
 * ```
 * Then:
 * ```ts
 * getImportId(source, 'w', 'location'); // Returns `'w'`
 * getImportId(source, 'x', 'location'); // Returns `'y'`
 * getImportId(source, 'z', 'location'); // Returns `undefined`
 * ```
 * @param source TS source.
 * @param name Name of the import (or the default import symbol).
 * @param location Import location.
 * @returns Id of the imported name or `undefined` if the name hasn't been
 * imported.
 */
export function getTsImportId(
  source: ts.SourceFile,
  name: string | typeof defaultBinding,
  location: string
): string | undefined {
  const importStats = getLocationImports(source, location);
  for (const importStat of importStats) {
    const importClause = importStat.importClause;
    if (!importClause) {
      continue;
    }
    const importClauseStr = importClause.getText();
    if (name === defaultBinding) {
      // Get default import name
      const match = matchDefaultImport(importClauseStr);
      if (match) {
        return match[1];
      }
    } else {
      // Get named binding
      const match = matchNamedBindingImport(importClauseStr, name);
      if (match) {
        return match[2] || name;
      }
    }
  }
}

/**
 * Obtains the declaration exporting the provided name (or default export).
 * @param source TS source.
 * @param name Name of the exported declaration (or default export name).
 * @returns Declaration exported with the provided name (or default export).
 */
export function getTsExportedDeclaration(
  source: ts.SourceFile,
  name: string | typeof defaultBinding
):
  | ts.FunctionDeclaration
  | ts.ClassDeclaration
  | ts.VariableDeclaration
  | undefined {
  const isDefault = name === defaultBinding;

  // Find matching function or class declaration
  const fn = source.statements.find(
    (s: ts.Statement) =>
      (ts.isFunctionDeclaration(s) || ts.isClassDeclaration(s)) &&
      !!s.modifiers &&
      !!s.modifiers.find((m) => m.kind === ts.SyntaxKind.ExportKeyword) &&
      (!isDefault ||
        !!s.modifiers.find((m) => m.kind === ts.SyntaxKind.DefaultKeyword)) &&
      !!s.name &&
      s.name.getText() === name
  ) as ts.FunctionDeclaration | ts.ClassDeclaration | undefined;
  if (fn) {
    return fn;
  }

  // Find matching variable declaration
  const decl: ts.VariableDeclaration | undefined = source.statements
    .filter(
      (s: ts.Statement) =>
        ts.isVariableStatement(s) &&
        !!s.modifiers &&
        !!s.modifiers.find((m) => m.kind === ts.SyntaxKind.ExportKeyword) &&
        (!isDefault ||
          !!s.modifiers.find((m) => m.kind === ts.SyntaxKind.DefaultKeyword))
    )
    .flatMap((v: ts.VariableStatement) => v.declarationList.declarations)
    .find((d: ts.VariableDeclaration) => d.name.getText() === name);
  return decl;
}

/**
 * Get all imports from a given location.
 * @param source TS source.
 * @param location Import location.
 * @returns List of imports from the given location.
 */
function getLocationImports(
  source: ts.SourceFile,
  location: string
): ts.ImportDeclaration[] {
  return source.statements.filter(
    (stat) =>
      ts.isImportDeclaration(stat) &&
      ts.isStringLiteral(stat.moduleSpecifier) &&
      stat.moduleSpecifier.text === location
  ) as ts.ImportDeclaration[];
}

/**
 * Match the default import section of an import statement's clause. Returns a
 * `RegExp` match where the whole match contains the portion to remove (when the
 * default import is to be removed). The first capturing groups contains the
 * name of the imported default id.
 * @param importClauseStr String representing the import statement's clause.
 * @returns `RegExp` match or `null` when the import doesn't match.
 */
function matchDefaultImport(importClauseStr: string): RegExpMatchArray | null {
  return importClauseStr.match(/^([^\s,]+)(?:\s*,\s*)?/);
}

/**
 * Match an import named binding from an import statement's clause. Returns a
 * `RegExp` match where the full match matches the whole named bindings group to
 * remove (when appropriate); the first capturing group is the portion of the
 * named binding to remove (when the import name is to be removed); and the
 * second capturing group contains the "alias" if it exists, e.g. `N` in
 * `name as N`.
 * @param importClauseStr String representing the import statement's clause.
 * @param name Name to match in the import statement.
 * @returns `RegExp` match or `null` when the import doesn't match.
 */
function matchNamedBindingImport(
  importClauseStr: string,
  name: string
): RegExpMatchArray | null {
  return importClauseStr.match(
    new RegExp(
      `(?:,\\s*)?{[^]*?(${name}(?:\\s+as\\s+([^\\s,}]+))?(?:\\s*,\\s*)?)[^]*}`
    )
  );
}

/**
 * Given a TS node, attempts to find the value represented by said node. I.e.,
 * if the given node **is not** an identifier, the given node is returned back;
 * if the node **is** an identifier, we try to find a declaration within the
 * same file matching said identifier and return the declared value; if no such
 * declaration is found, we check if the identifier has been imported from
 * somewhere, in which case we return a `TsImport` with the name of the imported
 * identifier and its location.
 * @param source Source code where the node has been found.
 * @param node Node for which to get a non-identifier node.
 */
export function getTsValue(
  source: ts.SourceFile,
  node?: ts.Node
): ts.Node | TsImport | undefined {
  if (node && ts.isIdentifier(node)) {
    const name = node.getText();

    // Find matching function or class declaration
    const fn = source.statements.find(
      (s: ts.Statement) =>
        (ts.isFunctionDeclaration(s) || ts.isClassDeclaration(s)) &&
        !!s.name &&
        s.name.getText() === name
    ) as ts.FunctionDeclaration | ts.ClassDeclaration | undefined;
    if (fn) {
      return fn;
    }

    // Find matching variable declaration
    const decl: ts.VariableDeclaration | undefined = source.statements
      .filter((s: ts.Statement) => ts.isVariableStatement(s))
      .flatMap((v: ts.VariableStatement) => v.declarationList.declarations)
      .find((d: ts.VariableDeclaration) => d.name.getText() === name);
    if (decl) {
      return getTsValue(source, decl.initializer);
    }

    // Find matching import
    const imports = source.statements.filter(
      (stat) => ts.isImportDeclaration(stat) && stat.importClause
    ) as ts.ImportDeclaration[];
    for (const imp of imports) {
      const location = (imp.moduleSpecifier as ts.StringLiteral).text;
      // Default import
      if (imp.importClause!.name && imp.importClause!.name.getText() === name) {
        return new TsImport(defaultBinding, location);
      }
      // Named import
      if (
        imp.importClause!.namedBindings &&
        ts.isNamedImports(imp.importClause!.namedBindings)
      ) {
        const bindings = imp.importClause!.namedBindings.elements;
        for (const el of bindings) {
          if (el.name.getText() === name) {
            return new TsImport(
              el.propertyName ? el.propertyName.getText() : name,
              location
            );
          }
        }
      }
    }
  } else {
    return node;
  }
}

/**
 * Transforms a given string in a string that can be used as the property of
 * an object. E.g.:
 * ```ts
 * stringAsProperty('aaaa'); // Returns `'aaaa'`
 * stringAsProperty('1xx'); // Returns `'\'1xx\''`
 * ```
 * @param string String to transform into a string that can be used as property.
 * @returns String that can be used as an object property.
 */
export function stringAsProperty(id: string): string {
  return validTsIdRegExp.test(id) ? id : `'${id.replace(/'/g, "\\'")}'`;
}

/**
 * Transforms a given string that represents an object property into the actual
 * name of the property (removing extra delimiters, etc.). E.g.:
 * ```ts
 * propertyAsString('aaaa'); // Returns `'aaaa'`
 * propertyAsString('\'1xx\''); // Returns `'1xx'`
 * ```
 * @param property Property (as a string) from which to get the name.
 * @returns Name of the property.
 */
export function propertyAsString(property: string): string {
  return /^('|").*?\1$/.test(property)
    ? property.slice(1, property.length - 1).replace(/\\'/g, "'")
    : property;
}

/**
 * Creates a change that inserts a given element (as a string) in a given array
 * of nodes.
 * @param filePath Path of file being edited.
 * @param nodeArr Array of nodes where to insert the new element.
 * @param nodeStr New node to insert (as a string).
 * @param separator Separator used between nodes.
 * @returns Change to apply.
 */
export function insertInNodeArray(
  filePath: Path,
  nodeArr: ts.NodeArray<any>,
  nodeStr: string,
  separator: string = ','
): Change {
  const indent = nodeArr[0] && getIndentation(nodeArr[0].getFullText(), '');
  const spacing = indent ? `\n${indent}` : ' ';
  const toInsert =
    nodeArr.length > 0 ? `${separator}${spacing}${nodeStr}` : nodeStr;

  return insertAfterLastOccurrence(
    nodeArr as any,
    toInsert,
    filePath,
    nodeArr.pos
  );
}
