import { Path } from '@angular-devkit/core';
import { Tree } from '@angular-devkit/schematics';
import ts from '@schematics/angular/third_party/github.com/Microsoft/TypeScript/lib/typescript';
import {
  addRouteDeclarationToModule,
  findNodes,
  getRouterModuleDeclaration,
} from '@schematics/angular/utility/ast-utils';
import { Change, ReplaceChange } from '@schematics/angular/utility/change';

import { applyChanges } from './changes';
import {
  getTsImportId,
  getTsSourceFile,
  getTsValue,
  insertTsImport,
  propertyAsString,
  relativeTsImportPath,
  removeTsImport,
  TsImport,
} from './ts-common';

/**
 * Interface used to create a new LF route.
 */
export interface NewLfRoute {
  /**
   * Route's path.
   */
  routePath: string;
  /**
   * Route's associated LF path.
   */
  storagePath: string;
  /**
   * Name of the component to route to.
   */
  componentTsName?: string;
  /**
   * File path of the component to route to.
   */
  componentFilePath?: Path;
  /**
   * When redirecting, route path to which to redirect to.
   */
  redirectTo?: string;
  /**
   * Used to set the `pathMatch` route attribute.
   */
  pathMatch?: string;
}

/**
 * Information about an LF route.
 */
export interface LfRouteInfo {
  /**
   * Route.
   */
  route?: string;
  /**
   * Route parameters.
   */
  params: string[];
}

/**
 * Changes the usage of `Routes` in a given routing module to `LfRoutes`.
 * @param host Source tree.
 * @param modulePath Path for the routing module on which to make the change.
 */
export function changeRoutesToLfRoutes(host: Tree, modulePath: Path): void {
  const source = getTsSourceFile(host, modulePath);

  // Find the routes declaration from the argument passed to the
  // `RouterModule`'s method
  const routerModuleImport = getRouterModuleDeclaration(source);
  if (!routerModuleImport) {
    return console.error(
      `Could not find the router module import in "${modulePath}".`
    );
  }
  const routesArg = (routerModuleImport as ts.CallExpression).arguments[0];
  if (!routesArg) {
    const { line } = source.getLineAndCharacterOfPosition(
      routerModuleImport.getStart()
    );
    return console.error(
      `The router module method call has no arguments at line ${line + 1} in ` +
        `"${modulePath}".`
    );
  }

  // The routes have been added inline into the router module method call, so we
  // do nothing
  if (!ts.isIdentifier(routesArg)) {
    return;
  }

  // Find the declaration matching the identifier used in the router module
  // method call
  const routesVarName = routesArg.getText();
  const routesDecl = source.statements
    .filter((stat: ts.Statement) => ts.isVariableStatement(stat))
    .flatMap((v: ts.VariableStatement) => v.declarationList.declarations)
    .find(
      (d: ts.VariableDeclaration) =>
        d.name && d.name.getText() === routesVarName
    );
  if (!routesDecl) {
    const { line } = source.getLineAndCharacterOfPosition(routesArg.getStart());
    return console.error(
      'No declaration of routes found that corresponds to the argument ' +
        `passed to the router module method at line ${line + 1} in ` +
        `"${modulePath}".`
    );
  }

  const routesId = getTsImportId(source, 'Routes', '@angular/router');
  const routesType = routesDecl.type;
  // Do nothing if the routes declaration isn't of type `Routes`
  if (!routesId || !routesType || routesType.getText() !== routesId) {
    return;
  }

  // Replace `Routes` by `LfRoutes` and import `LfRoutes`
  const changes: Change[] = [
    insertTsImport(
      source,
      modulePath,
      new TsImport('LfRoutes', '@lightweightform/core')
    ),
    new ReplaceChange(modulePath, routesType.getStart(), routesId, 'LfRoutes'),
  ];

  // Remove `Routes` import if it is no longer used
  const routesRefs = findNodes(
    source,
    ts.SyntaxKind.TypeReference,
    Infinity,
    true
  ).filter((t) => t.getText() === routesId);
  if (routesRefs.length === 1 && routesRefs[0] === routesType) {
    changes.push(
      removeTsImport(source, modulePath, 'Routes', '@angular/router')
    );
  }

  applyChanges(host, modulePath, changes);
}

/**
 * Adds a new LF route to the router module.
 * @param host Source tree.
 * @param modulePath Path for the routing module on which to add the route.
 * @param route Route to create.
 */
// TODO: This may not be accurate in case the route being added is a child of
// another route, in which case something like a `RouteExplorer` may be
// necessary
export function addLfRoute(
  host: Tree,
  modulePath: Path,
  {
    routePath,
    storagePath,
    componentTsName,
    componentFilePath,
    redirectTo,
    pathMatch,
  }: NewLfRoute
): void {
  const source = getTsSourceFile(host, modulePath);
  const componentStr = componentTsName ? `, component: ${componentTsName}` : '';
  const redirectToStr = redirectTo ? `, redirectTo: '${redirectTo}'` : '';
  const pathMatchStr = pathMatch ? `, pathMatch: '${pathMatch}'` : '';
  const lfPathStr = routeMatchesStoragePath('/', routePath, storagePath)
    ? ''
    : `, lfPath: '${storagePath}'`;
  const toInsert = `{ path: '${routePath}'${componentStr}${redirectToStr}${pathMatchStr}${lfPathStr} }`;

  const changes: Change[] = [
    addRouteDeclarationToModule(source, modulePath, toInsert),
  ];
  if (componentTsName && componentFilePath) {
    const importPath = relativeTsImportPath(modulePath, componentFilePath);
    changes.push(
      insertTsImport(
        source,
        modulePath,
        new TsImport(componentTsName, importPath)
      )
    );
  }

  applyChanges(host, modulePath, changes);
}

/**
 * Returns whether the routing module at the given path has routes.
 * @param host Source tree.
 * @param modulePath Path for the routing module.
 * @returns Whether the module has any routes.
 */
export function hasRoutes(host: Tree, modulePath: Path): boolean {
  const source = getTsSourceFile(host, modulePath);
  const routes = getRoutesValue(source);
  return !!routes && routes.elements.length > 0;
}

/**
 * Gets the route information of the route associated with the given storage
 * path.
 * @param host Source tree.
 * @param modulePath Path for the routing module.
 * @param storagePath Storage path for which to fetch matching route.
 * @returns Information about the route matching the provided storage path or
 * `undefined` when no match was found.
 */
// TODO: This may not be accurate in case the route being added is a child of
// another route, in which case something like a `RouteExplorer` may be
// necessary
export function getLfRouteInfo(
  host: Tree,
  modulePath: Path,
  storagePath: string
): LfRouteInfo | undefined {
  const source = getTsSourceFile(host, modulePath);
  const routes = getRoutesValue(source);
  if (!routes) {
    return;
  }

  for (const route of routes.elements) {
    // Route value
    const routeVal = getTsValue(source, route);
    if (
      !routeVal ||
      routeVal instanceof TsImport ||
      !ts.isObjectLiteralExpression(routeVal)
    ) {
      continue;
    }

    // Check that the route doesn't have `isLfRoute: false`
    if (
      routeVal.properties.find((p) => {
        const isLfRouteInit =
          ts.isPropertyAssignment(p) &&
          propertyAsString(p.name.getText()) === 'isLfRoute' &&
          p.initializer;
        const isLfRouteVal = isLfRouteInit && getTsValue(source, isLfRouteInit);
        return (
          !!isLfRouteVal &&
          !(isLfRouteVal instanceof TsImport) &&
          isLfRouteVal.kind === ts.SyntaxKind.FalseKeyword
        );
      })
    ) {
      continue;
    }

    // Route `path`
    const routePath = routeVal.properties.find(
      (p) =>
        ts.isPropertyAssignment(p) &&
        propertyAsString(p.name.getText()) === 'path'
    ) as ts.PropertyAssignment | undefined;
    // Route `lfPath`
    const routeLfPath = routeVal.properties.find(
      (p) =>
        ts.isPropertyAssignment(p) &&
        propertyAsString(p.name.getText()) === 'lfPath'
    ) as ts.PropertyAssignment | undefined;

    if (routeLfPath) {
      const routeLfPathVal = getTsValue(source, routeLfPath.initializer);
      if (
        routeLfPathVal &&
        !(routeLfPathVal instanceof TsImport) &&
        ts.isStringLiteral(routeLfPathVal) &&
        routeMatchesStoragePath('', routeLfPathVal.text, storagePath)
      ) {
        const routePathVal =
          routePath && getTsValue(source, routePath.initializer);
        const routeStr =
          routePathVal &&
          (!(routePathVal instanceof TsImport) &&
          ts.isStringLiteral(routePathVal)
            ? routePathVal.text
            : undefined);
        return {
          route: routeStr,
          params: routeParams(routeLfPathVal.text),
        };
      }
    } else if (routePath) {
      const routePathVal = getTsValue(source, routePath.initializer);
      if (
        routePathVal &&
        !(routePathVal instanceof TsImport) &&
        ts.isStringLiteral(routePathVal) &&
        routeMatchesStoragePath('/', routePathVal.text, storagePath)
      ) {
        return {
          route: routePathVal.text,
          params: routeParams(routePathVal.text),
        };
      }
    }
  }
}

/**
 * Whether a given route matches with a given storage path.
 * @param routePrefix Part of the route before the provided route.
 * @param routePath Route.
 * @param storagePath Storage path.
 * @returns Whether the route matches with a given storage path.
 */
export function routeMatchesStoragePath(
  routePrefix: string,
  routePath: string,
  storagePath: string
): boolean {
  const routeWithoutParams = `${routePrefix}${routePath
    .split('/')
    .map((id) => (id.startsWith(':') ? '?' : id))
    .join('/')}`;
  return routeWithoutParams === storagePath;
}

/**
 * Gets the routes array from the source file corresponding to a routing module.
 * @param source TS source of the routing module.
 * @returns Routes.
 */
function getRoutesValue(
  source: ts.SourceFile
): ts.ArrayLiteralExpression | undefined {
  // Find the routes declaration from the argument passed to the
  // `RouterModule`'s method
  const routerModuleImport = getRouterModuleDeclaration(source);
  if (!routerModuleImport) {
    return;
  }
  const routesArg = (routerModuleImport as ts.CallExpression).arguments[0];
  if (!routesArg) {
    return;
  }

  const routes = getTsValue(source, routesArg);
  if (
    !routes ||
    routes instanceof TsImport ||
    !ts.isArrayLiteralExpression(routes)
  ) {
    return;
  }
  return routes;
}

/**
 * Parameters of a certain LF route.
 * @param path Path from which to fetch params.
 * @returns Route parameters.
 */
function routeParams(path: string): string[] {
  return path
    .split('/')
    .filter((id) => id.startsWith(':'))
    .map((id) => id.slice(1));
}
