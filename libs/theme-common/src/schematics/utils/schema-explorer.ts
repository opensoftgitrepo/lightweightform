import { dirname, join, Path, resolve } from '@angular-devkit/core';
import { SchematicsException, Tree } from '@angular-devkit/schematics';
import ts from '@schematics/angular/third_party/github.com/Microsoft/TypeScript/lib/typescript';

import { getRootSchemaPath } from './storage-schema';
import {
  defaultBinding,
  getTsExportedDeclaration,
  getTsImportId,
  getTsSourceFile,
  getTsValue,
  propertyAsString,
  TsImport,
} from './ts-common';

/**
 * Information about a given schema.
 */
export interface SchemaInfo {
  /**
   * Information about the schema's file.
   */
  fileInfo: SchemaFileInfo;
  /**
   * Storage path representing the schema.
   */
  storagePath: string;
  /**
   * Type of schema (record or table).
   */
  type: 'record' | 'table';
  /**
   * TS node expression where the schema is declared.
   */
  node: ts.CallExpression;
  /**
   * Object used to set the fields of the schema (for record schemas only).
   */
  recordObject?: ts.ObjectLiteralExpression;
}

/**
 * Information about a given schema file.
 */
export interface SchemaFileInfo {
  /**
   * Schema's source file.
   */
  source: ts.SourceFile;
  /**
   * File path where the schema is declared.
   */
  filePath: Path;
  /**
   * Name with which `recordSchema` is exported in the source. `undefined` if it
   * isn't exported.
   */
  recordSchemaName?: string;
  /**
   * Name with which `tableSchema` is exported in the source. `undefined` if it
   * isn't exported.
   */
  tableSchemaName?: string;
}

/**
 * Class used to create a map of how schemas are distributed across files within
 * a user's project, in order to properly determine the relationship between a
 * newly created form in a schematic and its parent record. It allows users to
 * follow a directory-based hierarchical approach or a more liberal approach
 * when creating new forms or collections of forms. Note that this class builds
 * a "static" map of the schemas and will not be updated when schematics change
 * said schemas.
 */
export class SchemaExplorer {
  /**
   * Name with which the root schema has been exported (or the default export
   * symbol).
   */
  public rootSchemaName: string | typeof defaultBinding;

  /**
   * Map from the storage path to the schema's info.
   */
  private storagePathToSchemaInfo: Map<string, SchemaInfo> = new Map();
  /**
   * Map from a directory to the list of schema file paths in said directory
   * (sorted by those closest to the root schema).
   */
  private dirToSchemaFilePaths: Map<Path, Set<Path>> = new Map();
  /**
   * Map from a file path to the list of schema informations in that file
   * (sorted by those closest to the root schema).
   */
  private filePathToSchemaInfos: Map<Path, SchemaInfo[]> = new Map();

  /**
   * Map from file path to its schema file information.
   */
  private filePathToFileInfo: Map<Path, SchemaFileInfo> = new Map();

  /**
   * Creates a new schema explorer.
   * @param host Source tree.
   * @param moduleDir Module directory (where the root schema file should be
   * found).
   */
  constructor(private host: Tree, moduleDir: Path) {
    const rootSchemaPath = getRootSchemaPath(this.host, moduleDir);
    const rootSchemaInfo = this.getRootSchemaInfo(rootSchemaPath);
    this.buildAllSchemaInfo(rootSchemaInfo);
  }

  /**
   * Returns the schema info for a given storage path.
   * @param storagePath Storage path for which to fetch schema information.
   * @returns Schema info for the given storage path.
   */
  public getSchemaInfoFromStoragePath(
    storagePath: string
  ): SchemaInfo | undefined {
    return this.storagePathToSchemaInfo.get(storagePath);
  }

  /**
   * Returns the list of schema file paths in the given directory, sorted by
   * those with schemas closest to the root schema.
   * @param dir Directory for which to fetch schema file paths.
   * @returns List of schema file paths in the given directory.
   */
  public getSchemaFilePathsFromDir(dir: Path): Path[] {
    const schemaFilePaths = this.dirToSchemaFilePaths.get(dir);
    return schemaFilePaths ? Array.from(schemaFilePaths.values()) : [];
  }

  /**
   * Returns all schema infos associated with the given file path, sorted by
   * those closest to the root schema.
   * @param filePath File path from which to fetch associated schemas.
   * @returns List of schema infos associated with the given file path.
   */
  public getSchemaInfoFromFilePath(filePath: Path): SchemaInfo[] {
    return this.filePathToSchemaInfos.get(filePath) || [];
  }

  /**
   * Recursively builds info on all schemas from a given schema's info.
   * @param schemaInfo Info of the schema from which to build all schema's info.
   */
  private buildAllSchemaInfo(schemaInfo: SchemaInfo): void {
    const fileInfo = schemaInfo.fileInfo;
    const filePath = fileInfo.filePath;
    const dir = dirname(filePath);

    // Save schema info in the instance maps
    this.storagePathToSchemaInfo.set(schemaInfo.storagePath, schemaInfo);

    let dirSchemaFilePaths = this.dirToSchemaFilePaths.get(dir);
    if (!dirSchemaFilePaths) {
      this.dirToSchemaFilePaths.set(dir, (dirSchemaFilePaths = new Set()));
    }
    dirSchemaFilePaths.add(filePath);

    let filePathSchemaInfos = this.filePathToSchemaInfos.get(filePath);
    if (!filePathSchemaInfos) {
      this.filePathToSchemaInfos.set(filePath, (filePathSchemaInfos = []));
    }
    filePathSchemaInfos.push(schemaInfo);

    // Build children schema infos
    if (schemaInfo.node.arguments.length > 0) {
      if (schemaInfo.type === 'table') {
        const rowsInfo = this.getSchemaInfo(
          fileInfo,
          join(schemaInfo.storagePath as Path, '?'),
          schemaInfo.node.arguments[0]
        );
        if (rowsInfo) {
          this.buildAllSchemaInfo(rowsInfo);
        }
      } else if (schemaInfo.type === 'record' && schemaInfo.recordObject) {
        const props = schemaInfo.recordObject.properties;
        for (const prop of props) {
          if (ts.isPropertyAssignment(prop) && prop.initializer) {
            const propInfo = this.getSchemaInfo(
              fileInfo,
              join(
                schemaInfo.storagePath as Path,
                propertyAsString(prop.name.getText())
              ),
              prop.initializer
            );
            if (propInfo) {
              this.buildAllSchemaInfo(propInfo);
            }
          }
        }
      }
    }
  }

  /**
   * Obtains the schema information of the given node (which may be an
   * identifier [possibly imported] or the schema definition itself).
   * @param fileInfo File information of the node in question.
   * @param storagePath Storage path for the node in question.
   * @param node Node for which to fetch schema information.
   * @returns Schema information of the given node, `undefined` when the node
   * does not represent a record or table schema.
   */
  private getSchemaInfo(
    fileInfo: SchemaFileInfo,
    storagePath: string,
    node: ts.Node
  ): SchemaInfo | undefined {
    const val = getTsValue(fileInfo.source, node);
    if (val instanceof TsImport) {
      const loc = val.location;
      if (loc.startsWith('./') || loc.startsWith('../')) {
        const valFilePath = (resolve(dirname(fileInfo.filePath), loc as Path) +
          '.ts') as Path;
        const importInfo = this.getSchemaFileInfo(valFilePath);
        const decl = getTsExportedDeclaration(importInfo.source, val.name);
        if (decl && ts.isVariableDeclaration(decl) && decl.initializer) {
          return this.getSchemaInfo(importInfo, storagePath, decl.initializer);
        }
      }
    } else if (val && ts.isCallExpression(val)) {
      const exp = val.expression.getText();
      if (fileInfo.recordSchemaName && exp === fileInfo.recordSchemaName) {
        let recordObject;
        // NOTE: The record schema's argument cannot currently have been
        // imported from a different file.
        const obj = getTsValue(fileInfo.source, val.arguments[0]);
        if (
          obj &&
          !(obj instanceof TsImport) &&
          ts.isObjectLiteralExpression(obj)
        ) {
          recordObject = obj;
        }
        return {
          fileInfo,
          storagePath,
          type: 'record',
          node: val,
          recordObject,
        };
      }
      if (fileInfo.tableSchemaName && exp === fileInfo.tableSchemaName) {
        return { fileInfo, storagePath, type: 'table', node: val };
      }
    }
  }

  /**
   * Returns the declaration exporting a file's schema.
   * @param source TS source code.
   * @returns Exported schema declaration.
   */
  private getRootSchemaInfo(filePath: Path): SchemaInfo {
    const fileInfo = this.getSchemaFileInfo(filePath);
    const stats = fileInfo.source.statements.filter(
      (s: ts.Statement) =>
        ts.isVariableStatement(s) &&
        s.modifiers &&
        s.modifiers.find((m) => m.kind === ts.SyntaxKind.ExportKeyword)
    ) as ts.VariableStatement[];
    for (const stat of stats) {
      for (const decl of stat.declarationList.declarations) {
        const info =
          decl.initializer &&
          this.getSchemaInfo(fileInfo, '/', decl.initializer);
        if (info) {
          this.rootSchemaName = stat.modifiers!.find(
            (m) => m.kind === ts.SyntaxKind.DefaultKeyword
          )
            ? defaultBinding
            : decl.name.getText();
          return info;
        }
      }
    }
    throw new SchematicsException(
      `Could not find the root schema info at "${filePath}".`
    );
  }

  /**
   * Get the schema file information of a given file path (caching the obtained
   * objects).
   * @param filePath File path for which to get the information.
   * @returns Schema file information of a given path.
   */
  private getSchemaFileInfo(filePath: Path): SchemaFileInfo {
    let info = this.filePathToFileInfo.get(filePath);
    if (!info) {
      const source = getTsSourceFile(this.host, filePath);
      this.filePathToFileInfo.set(
        filePath,
        (info = {
          source,
          filePath,
          recordSchemaName: getTsImportId(
            source,
            'recordSchema',
            '@lightweightform/storage'
          ),
          tableSchemaName: getTsImportId(
            source,
            'tableSchema',
            '@lightweightform/storage'
          ),
        })
      );
    }
    return info;
  }
}
