import { Path } from '@angular-devkit/core';
import { SchematicsException, Tree } from '@angular-devkit/schematics';
import ts from '@schematics/angular/third_party/github.com/Microsoft/TypeScript/lib/typescript';
import {
  getDecoratorMetadata,
  insertAfterLastOccurrence,
} from '@schematics/angular/utility/ast-utils';
import {
  Change,
  InsertChange,
  ReplaceChange,
} from '@schematics/angular/utility/change';

import { applyChanges } from './changes';
import { getIndentation, indentNewLines } from './indentantion';
import {
  getTsSourceFile,
  insertInNodeArray,
  insertTsImport,
  TsImport,
} from './ts-common';

/**
 * Gets the first `@Component` class of a given TS source file.
 * @param source TS source file.
 * @returns Component's class declaration.
 */
function getFirstComponent(source: ts.SourceFile): ts.ClassDeclaration | null {
  // Obtain the parent class declaration of a node
  function getParentClassDeclaration(nd: ts.Node): ts.ClassDeclaration | null {
    if (ts.isClassDeclaration(nd)) {
      return nd;
    }
    return nd.parent ? getParentClassDeclaration(nd.parent) : null;
  }
  // Find `@Component`s
  const components = getDecoratorMetadata(source, 'Component', '@angular/core');
  return components.length === 0
    ? null
    : getParentClassDeclaration(components[0]);
}

/**
 * Adds an interface to the first component in a given file.
 * @param host Source tree.
 * @param componentPath Path on which to find the component.
 * @param interfaceName Name of the interface to implement.
 * @param toImport Identifier of the interface to import.
 */
export function addTsComponentInterface(
  host: Tree,
  componentPath: Path,
  interfaceName: string,
  toImport?: TsImport
): void {
  const source = getTsSourceFile(host, componentPath);
  const component = getFirstComponent(source);
  if (!component) {
    return console.error(`No \`@Component\` found in "${componentPath}".`);
  }

  const interfacesClause =
    component.heritageClauses &&
    (component.heritageClauses.find(
      (h) => ts.tokenToString(h.token) === 'implements'
    ) as ts.HeritageClause | null);

  const changes: Change[] = [];
  if (interfacesClause) {
    changes.push(
      insertInNodeArray(componentPath, interfacesClause.types, interfaceName)
    );
  } else {
    const insertAfter = component.heritageClauses
      ? component.heritageClauses[component.heritageClauses.length - 1].end
      : component.name!.end;
    const toInsert = ` implements ${interfaceName}`;
    changes.push(new InsertChange(componentPath, insertAfter, toInsert));
  }

  if (toImport) {
    changes.push(insertTsImport(source, componentPath, toImport));
  }

  applyChanges(host, componentPath, changes);
}

/**
 * Adds a member to the first component in a given file.
 * @param host Source tree.
 * @param componentPath Path on which to find the component.
 * @param memberName Name of the member to add.
 * @param toInsert Member content to insert.
 * @param memberKind Kind of member to insert.
 * @param modifiers Member modifiers, e.g. `['private', 'static']`.
 * @param toImport Identifiers to import.
 */
export function addTsComponentMember(
  host: Tree,
  componentPath: Path,
  memberName: string,
  toInsert: string,
  memberKind:
    | ts.SyntaxKind.PropertyDeclaration
    | ts.SyntaxKind.Constructor
    | ts.SyntaxKind.GetAccessor
    | ts.SyntaxKind.SetAccessor
    | ts.SyntaxKind.MethodDeclaration,
  modifiers: string[] = [],
  toImport: TsImport[] = []
): void {
  const source = getTsSourceFile(host, componentPath);
  const component = getFirstComponent(source);
  if (!component) {
    return console.error(`No \`@Component\` found in "${componentPath}".`);
  }
  const members = component.members;
  // Do nothing if member already exists
  if (
    members &&
    members.find((m) => !!m.name && (m.name as any).escapedText === memberName)
  ) {
    return;
  }

  // Add modifiers and indent new member
  const modifiersStr = modifiers.length ? modifiers.join(' ') + ' ' : '';
  const indent = getIndentation(component.getFullText());
  toInsert = `\n\n${indent}${modifiersStr}${indentNewLines(toInsert, indent)}`;

  const ordering = {
    [ts.SyntaxKind.PropertyDeclaration]: 0,
    [ts.SyntaxKind.Constructor]: 1,
    [ts.SyntaxKind.GetAccessor]: 2,
    [ts.SyntaxKind.SetAccessor]: 2,
    [ts.SyntaxKind.MethodDeclaration]: 3,
  };
  const memberOrder = ordering[memberKind];

  const changes = [
    insertAfterLastOccurrence(
      members.filter((m) => ordering[m.kind] <= memberOrder),
      toInsert,
      componentPath,
      members.pos
    ),
  ];
  toImport.forEach((t) =>
    changes.push(insertTsImport(source, componentPath, t))
  );

  applyChanges(host, componentPath, changes);
}

/**
 * Adds a property to the first component in a given file.
 * @param host Source tree.
 * @param componentPath Path on which to find the component.
 * @param propertyName Name of the property to add.
 * @param propertyValue Value of the property to add.
 * @param propertyType Type of the property to add.
 * @param modifiers Property modifiers, e.g. `['private', 'static']`.
 * @param toImport Identifiers to import.
 */
export function addTsComponentProperty(
  host: Tree,
  componentPath: Path,
  propertyName: string,
  propertyType?: string,
  propertyValue?: string,
  modifiers?: string[],
  toImport?: TsImport[]
): void {
  const typ = propertyType ? `: ${propertyType}` : '';
  const assign = propertyValue ? ` = ${propertyValue}` : '';
  const toInsert = `${propertyName}${typ}${assign};`;
  addTsComponentMember(
    host,
    componentPath,
    propertyName,
    toInsert,
    ts.SyntaxKind.PropertyDeclaration,
    modifiers,
    toImport
  );
}

/**
 * Adds a getter to the first component in a given file.
 * @param host Source tree.
 * @param componentPath Path on which to find the component.
 * @param getterName Name of the getter to add.
 * @param getterType Type of the getter to add.
 * @param getterContent Content of the getter to add, including the opening and
 * closing brackets, e.g.: `'{ return this.prop; }'`.
 * @param modifiers Getter modifiers, e.g. `['private']`.
 * @param toImport Identifiers to import.
 */
export function addTsComponentGetter(
  host: Tree,
  componentPath: Path,
  getterName: string,
  getterType?: string,
  getterContent?: string,
  modifiers?: string[],
  toImport?: TsImport[]
): void {
  const typ = getterType ? `: ${getterType}` : '';
  const toInsert = `get ${getterName}()${typ} ${getterContent}`;
  addTsComponentMember(
    host,
    componentPath,
    getterName,
    toInsert,
    ts.SyntaxKind.GetAccessor,
    modifiers,
    toImport
  );
}

/**
 * Adds a setter to the first component in a given file.
 * @param host Source tree.
 * @param componentPath Path on which to find the component.
 * @param setterName Name of the setter to add.
 * @param setterArgName Name of the setter's argument.
 * @param setterArgType Type of the setter's argument.
 * @param setterContent Content of the setter to add, , including the opening
 * and closing brackets, e.g.: `'{ this.prop = arg; }'`.
 * @param modifiers Setter modifiers, e.g. `['private']`.
 * @param toImport Identifiers to import.
 */
export function addTsComponentSetter(
  host: Tree,
  componentPath: Path,
  setterName: string,
  setterArgName: string,
  setterArgType?: string,
  setterContent?: string,
  modifiers?: string[],
  toImport?: TsImport[]
): void {
  const typ = setterArgType ? `: ${setterArgType}` : '';
  const toInsert = `set ${setterName}(${setterArgName}${typ}) ${setterContent}`;
  addTsComponentMember(
    host,
    componentPath,
    setterName,
    toInsert,
    ts.SyntaxKind.SetAccessor,
    modifiers,
    toImport
  );
}

/**
 * Adds a method to the first component in a given file.
 * @param host Source tree.
 * @param componentPath Path on which to find the component.
 * @param methodName Name of the method to add.
 * @param methodContent Content of the method to add, including the method's
 * signature, but excluding its name, e.g.:
 * `'(arg1: number): string { return arg1.toString(); }'`
 * @param modifiers Property modifiers, e.g. `['private', 'static']`.
 * @param toImport Identifiers to import.
 */
export function addTsComponentMethod(
  host: Tree,
  componentPath: Path,
  methodName: string,
  methodContent: string,
  modifiers?: string[],
  toImport?: TsImport[]
): void {
  const toInsert = `${methodName}${methodContent}`;
  addTsComponentMember(
    host,
    componentPath,
    methodName,
    toInsert,
    ts.SyntaxKind.MethodDeclaration,
    modifiers,
    toImport
  );
}

/**
 * Adds an argument to the first constructor of the first component in a given
 * file.
 * @param host Source tree.
 * @param componentPath Path on which to find the component.
 * @param paramName Name of the parameter to add.
 * @param paramType Type of the parameter to add.
 * @param modifiers Property modifiers, e.g. `['private']`.
 * @param toImport Identifiers to import.
 */
export function addTsComponentConstructorParameter(
  host: Tree,
  componentPath: Path,
  paramName: string,
  paramType: string,
  modifiers: string[] = [],
  toImport: TsImport[] = []
) {
  const source = getTsSourceFile(host, componentPath);
  const component = getFirstComponent(source);
  if (!component) {
    return console.error(`No \`@Component\` found in "${componentPath}".`);
  }

  const members = component.members;
  const constructor =
    component.members &&
    (component.members.find((m) => ts.isConstructorDeclaration(m)) as
      | ts.ConstructorDeclaration
      | undefined);
  const modifiersStr = modifiers.length ? modifiers.join(' ') + ' ' : '';
  const paramStr = `${modifiersStr}${paramName}: ${paramType}`;
  const changes: Change[] = [];

  if (constructor) {
    // Constructor already exists, simply add param if it doesn't exist
    const params = (constructor as any).parameters;
    if (
      params &&
      params.find((p) => ts.isParameter(p) && p.name.getText() === paramName)
    ) {
      return;
    }
    changes.push(insertInNodeArray(componentPath, params, paramStr));
  } else {
    // Constructor doesn't exist, create it after all properties
    const indent = getIndentation(component.getFullText());
    const toInsert = `\n\n${indent}constructor(${paramStr}) {}`;
    changes.push(
      insertAfterLastOccurrence(
        members.filter((m) => ts.isPropertyDeclaration(m)),
        toInsert,
        componentPath,
        members.pos,
        ts.SyntaxKind.PropertyDeclaration
      )
    );
  }
  toImport.forEach((t) =>
    changes.push(insertTsImport(source, componentPath, t))
  );

  applyChanges(host, componentPath, changes);
}

/**
 * Set the `inlineTemplate` of the first component of a given file.
 * @param host Source tree.
 * @param componentPath Path on which to find the component.
 * @param template String to add as inline template.
 */
export function setTsComponentInlineTemplate(
  host: Tree,
  componentPath: Path,
  template: string
): void {
  const source = getTsSourceFile(host, componentPath);
  const metadatas = getDecoratorMetadata(source, 'Component', '@angular/core');
  if (metadatas.length === 0) {
    return console.error(`No \`@Component\` found in "${componentPath}".`);
  }
  const metadata = metadatas[0];
  if (!ts.isObjectLiteralExpression(metadata)) {
    throw new SchematicsException(
      `Invalid \`@Component\` metadata in "${componentPath}".`
    );
  }

  const props = metadata.properties;
  const templateProp = props.find(
    (p) => ts.isPropertyAssignment(p) && p.name.getText() === 'template'
  ) as ts.PropertyAssignment | undefined;

  const indent = getIndentation(metadata.getText());
  const templateStr = `\`${indentNewLines(
    `\n${template}`,
    indent + indent
  )}\n${indent}\``;

  let change: Change;
  if (templateProp) {
    // `template` already exists
    change = new ReplaceChange(
      componentPath,
      templateProp.initializer.pos,
      templateProp.initializer.getFullText(),
      templateStr
    );
  } else {
    // `template` doesn't exist (create new property)
    change = insertInNodeArray(
      componentPath,
      props,
      `template: ${templateStr}`
    );
  }

  applyChanges(host, componentPath, [change]);
}
