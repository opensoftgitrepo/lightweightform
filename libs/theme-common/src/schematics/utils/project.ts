import { workspaces } from '@angular-devkit/core';
import { SchematicsException, Tree } from '@angular-devkit/schematics';
import { getWorkspace } from '@schematics/angular/utility/workspace';

/**
 * Returns project configuration of a project with a given name (or the default
 * project).
 * @param host Source tree.
 * @param projectName Name of the project to get.
 * @returns Project configuration.
 */
export async function getProject(
  host: Tree,
  projectName?: string
): Promise<workspaces.ProjectDefinition> {
  const workspace = await getWorkspace(host);
  const project =
    workspace.projects[
      projectName || (workspace.extensions.defaultProject as string)
    ];
  if (!project) {
    throw new SchematicsException(
      `Could not find project in workspace: ${projectName}`
    );
  }
  return project;
}

/**
 * Updates the project configuration of a project with a given name (or the
 * default project).
 * @param host Source tree.
 * @param projectConfig Updated project configuration.
 * @param projectName Name of the project to update.
 */
export function updateProject(
  host: Tree,
  projectConfig: workspaces.ProjectDefinition,
  projectName?: string
): void {
  const workspacePath = ['angular.json', '.angular.json'].find((p) =>
    host.exists(p)
  )!;
  const workspaceStr = host.read(workspacePath)!.toString('utf-8');
  const workspace = JSON.parse(workspaceStr);
  const projectToUpdate = projectName || workspace.defaultProject!;
  const newWorkspace = {
    ...workspace,
    projects: { ...workspace.projects, [projectToUpdate]: projectConfig },
  };
  const newWorkspaceStr = JSON.stringify(newWorkspace, null, 2);
  if (workspaceStr !== newWorkspaceStr) {
    host.overwrite(workspacePath, newWorkspaceStr);
  }
}

/**
 * Looks for the main TypeScript file in the given project and returns its path.
 * @param project Project for which to fetch the main file path.
 * @returns Main file path.
 */
export function getProjectMainFilePath(
  project: workspaces.ProjectDefinition
): string {
  const buildOptions = getProjectTargetOptions(project, 'build');
  if (!buildOptions.main) {
    throw new SchematicsException(
      `Could not find the project main file inside of the workspace config ` +
        `(${project.sourceRoot})`
    );
  }
  return buildOptions.main;
}

/**
 * Resolves the target options for the build target of the given project.
 * @param project Project for which to fetch target options.
 * @param buildTarget Target for which to fetch options.
 * @returns Target options.
 */
export function getProjectTargetOptions(
  project: workspaces.ProjectDefinition,
  buildTarget: string
): Record<string, any> {
  const targets = project.targets || project.extensions.architect;
  if (targets && targets[buildTarget] && targets[buildTarget].options) {
    return targets[buildTarget].options;
  }
  throw new SchematicsException(
    `Cannot determine project target configuration for: ${buildTarget}.`
  );
}

/**
 * Returns the options of a given schematic in a given project.
 * @param host Source tree.
 * @param projectName Name of the project from where to get the schematic
 * options.
 * @param schematic Schematic name, e.g. `package-name:schematic-name`.
 * @returns Schematic options or `undefined` if no options have been defined for
 * the specified schematic.
 */
export async function getProjectSchematicOptions(
  host: Tree,
  projectName: string,
  schematic: string
): Promise<Record<string, any> | undefined> {
  const project = await getProject(host, projectName);
  return (project['schematics'] || {})[schematic];
}

/**
 * Sets the options for a given schematic for a given project.
 * @param host Source tree.
 * @param projectName Name of the project on which to set the schematic options.
 * @param schematic Schematic name, e.g. `package-name:schematic-name`.
 * @param options Options to set.
 * @param overwrite Whether to overwrite schematic options (the default
 * behaviour is to merge them with the existing options).
 */
export async function setProjectSchematicOptions(
  host: Tree,
  projectName: string,
  schematic: string,
  options: Record<string, any>,
  overwrite = false
): Promise<void> {
  const project = await getProject(host, projectName);
  const schematics = project['schematics'] || (project['schematics'] = {});
  schematics[schematic] = overwrite
    ? options
    : { ...schematics[schematic], ...options };
  updateProject(host, project, projectName);
}
