import { normalize, Path, workspaces } from '@angular-devkit/core';
import { Tree } from '@angular-devkit/schematics';

import { getProject, getProjectTargetOptions, updateProject } from './project';

/**
 * Regular expression that matches all possible Angular CLI default style files.
 */
const defaultStyleFileRegex = /styles\.((c|le|sa|sc)ss)|styl$/;

/**
 * Regular expression that matches all files that have a proper stylesheet
 * extension.
 */
const validStyleFileRegex = /\.((c|le|sa|sc)ss)|styl$/;

/**
 * Gets a style file with the given extension in a project and returns its path.
 * If no extension is specified, any style file with a valid extension will be
 * returned.
 * @param project Project for which to fetch the style file.
 * @param extension Expected style file extension.
 * @returns Path of the project's style file.
 */
export function getProjectStyleFilePath(
  project: workspaces.ProjectDefinition,
  extension?: string
): Path | null {
  const buildOptions = getProjectTargetOptions(project, 'build');

  if (buildOptions.styles && buildOptions.styles.length) {
    const styles = buildOptions.styles.map((s) =>
      typeof s === 'string' ? s : s.input
    );

    // Look for the default style file that is generated for new projects by the
    // Angular CLI. This default style file is usually called `styles.[ext]`
    // unless it has been changed explicitly.
    const defaultMainStylePath = styles.find((file) =>
      extension
        ? file === `styles.${extension}`
        : defaultStyleFileRegex.test(file)
    );

    if (defaultMainStylePath) {
      return normalize(defaultMainStylePath);
    }

    // If no default style file could be found, use the first style file that
    // matches the given extension. If no extension specified explicitly, we
    // look for any file with a valid style file extension.
    const fallbackStylePath = styles.find((file) =>
      extension
        ? file.endsWith(`.${extension}`)
        : validStyleFileRegex.test(file)
    );

    if (fallbackStylePath) {
      return normalize(fallbackStylePath);
    }
  }

  return null;
}

/**
 * Adds a style path to a project's build or test target.
 * @param host Source tree.
 * @param projectName Name of the project where to add the style path.
 * @param targetName Target on which to add the style path.
 * @param stylePath Path of the style file to add.
 */
export async function addStylePathToProjectTarget(
  host: Tree,
  projectName: string,
  targetName: 'build' | 'test',
  stylePath: Path
): Promise<void> {
  const defaultBuilders = {
    build: '@angular-devkit/build-angular:browser',
    test: '@angular-devkit/build-angular:karma',
  };
  const project = await getProject(host, projectName);
  const targets = project.targets || project.extensions.architect;
  const targetConfig = targets && targets[targetName];
  const usesDefaultBuilder =
    targetConfig && targetConfig.builder === defaultBuilders[targetName];
  if (!usesDefaultBuilder) {
    console.warn(
      `The project is not using the default builder for "${targetName}". ` +
        `This means that we could not add "${stylePath}" to the ` +
        `"${targetName}" target.`
    );
    return;
  }

  const targetOptions = getProjectTargetOptions(project, targetName);
  if (!targetOptions.styles) {
    targetOptions.styles = [stylePath];
  } else {
    const existingStyles = targetOptions.styles.map((s) =>
      typeof s === 'string' ? s : s.input
    );
    if (existingStyles.indexOf(stylePath) !== -1) {
      return;
    }
    targetOptions.styles.unshift(stylePath);
  }

  updateProject(host, project, projectName);
}
