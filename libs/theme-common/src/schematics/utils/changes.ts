import { Path } from '@angular-devkit/core';
import { Tree } from '@angular-devkit/schematics';
import {
  Change,
  InsertChange,
  NoopChange,
  RemoveChange,
  ReplaceChange,
} from '@schematics/angular/utility/change';

/**
 * Apply changes to a file.
 * @param host Source tree.
 * @param path Path on where to apply changes.
 * @param changes List of changes to apply.
 */
export function applyChanges(host: Tree, path: Path, changes: Change[]): void {
  changes = changes.filter((change) => !(change instanceof NoopChange));
  if (changes.length > 0) {
    const recorder = host.beginUpdate(path);
    for (const change of changes) {
      // FIXME: `pos`, `toRemove`, `oldText`, and `newText` are private in
      // `@schematics/angular` for some reason, we should implement `Change` and
      // derivatives ourselves
      if (change instanceof InsertChange) {
        recorder.insertLeft(change.pos, change.toAdd);
      } else if (change instanceof RemoveChange) {
        recorder.remove((change as any).pos, (change as any).toRemove.length);
      } else if (change instanceof ReplaceChange) {
        recorder.remove((change as any).pos, (change as any).oldText.length);
        recorder.insertLeft((change as any).pos, (change as any).newText);
      }
    }
    host.commitUpdate(recorder);
  }
}
