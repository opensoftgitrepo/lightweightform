import { basename, Path, workspaces } from '@angular-devkit/core';
import { Tree } from '@angular-devkit/schematics';
import { addSymbolToNgModuleMetadata } from '@schematics/angular/utility/ast-utils';
import { findModuleFromOptions } from '@schematics/angular/utility/find-module';
import { buildDefaultPath } from '@schematics/angular/utility/workspace';

import { applyChanges } from './changes';
import { getTsSourceFile, insertTsImport, TsImport } from './ts-common';

/**
 * Regex matching the suffix of a module path.
 */
export const moduleSuffixRegExp = /\.module\.ts$/;
/**
 * Regex matching the suffix of a routing module path.
 */
export const routingModuleSuffixRegExp = /-routing\.module\.ts$/;

/**
 * Returns the path of the Angular module file for the given module name.
 * @param host Source tree.
 * @param project Project for which to fetch the module path.
 * @param moduleName Name of the module for which to fetch the module path.
 * @param routing Whether to fetch the routing module.
 * @param name Possibly a name provided by a schematic where a component has
 * been generated for which we want to find its declaring module.
 * @returns Angular module path.
 */
export function getModulePath(
  host: Tree,
  project: workspaces.ProjectDefinition,
  moduleName?: string,
  routing: boolean = false,
  name: string = ''
): Path {
  return findModuleFromOptions(host, {
    path: buildDefaultPath(project),
    name,
    module: moduleName || 'app',
    moduleExt: routing ? '-routing.module.ts' : '.module.ts',
  })!;
}

/**
 * Returns the name of a module given its Angular module path.
 * @param modulePath Angular module path.
 * @param routing Whether the module path belongs to a routing module.
 * @returns Module name.
 */
export function getModuleNameFromPath(
  modulePath: Path,
  routing: boolean = false
) {
  return basename(modulePath).replace(
    routing ? routingModuleSuffixRegExp : moduleSuffixRegExp,
    ''
  );
}

/**
 * Adds something to the metadata of an `NgModule`.
 * @param host Source tree.
 * @param modulePath Path of the Angular module on which to add the import.
 * @param metadataField Metadata field on which to add something.
 * @param expression Expression to add.
 * @param toImport Identifiers to import.
 */
export function addNgModuleMetadata(
  host: Tree,
  modulePath: Path,
  metadataField: string,
  expression: string,
  toImport: TsImport[] = []
) {
  const source = getTsSourceFile(host, modulePath);
  const changes = addSymbolToNgModuleMetadata(
    source,
    modulePath,
    metadataField,
    expression
  );
  toImport.forEach((t) => changes.push(insertTsImport(source, modulePath, t)));
  applyChanges(host, modulePath, changes);
}

/**
 * Adds a declaration into an `NgModule`.
 * @param host Source tree.
 * @param modulePath Path of the Angular module on which to add the declaration.
 * @param declarationName Name of the declaration.
 * @param declarationLocation Location of declaration (where to import it from).
 */
export function addNgModuleDeclaration(
  host: Tree,
  modulePath: Path,
  declarationName: string,
  declarationLocation: string
): void {
  addNgModuleMetadata(host, modulePath, 'declarations', declarationName, [
    new TsImport(declarationName, declarationLocation),
  ]);
}

/**
 * Adds an import into an `NgModule`.
 * @param host Source tree.
 * @param modulePath Path of the Angular module on which to add the import.
 * @param importName Name of the import.
 * @param importLocation Location of import (where to import it from).
 * @param importExpression Expression to use in the module imports array (defaults to `importName`).
 */
export function addNgModuleImport(
  host: Tree,
  modulePath: Path,
  importName: string,
  importLocation: string,
  importExpression: string = importName
): void {
  addNgModuleMetadata(host, modulePath, 'imports', importExpression, [
    new TsImport(importName, importLocation),
  ]);
}

/**
 * Adds a simple provider into an `NgModule`.
 * @param host Source tree.
 * @param modulePath Path of the Angular module on which to add the provider.
 * @param providerName Name of the provider.
 * @param providerLocation Location of provider (where to import it from).
 */
export function addNgModuleProvider(
  host: Tree,
  modulePath: Path,
  providerName: string,
  providerLocation: string
): void {
  addNgModuleMetadata(host, modulePath, 'providers', providerName, [
    new TsImport(providerName, providerLocation),
  ]);
}

/**
 * Adds a provider of type `{provide: providerName, useValue: valueName}` into
 * an `NgModule`.
 * @param host Source tree.
 * @param modulePath Path of the Angular module on which to add the provider.
 * @param providerName Name of the provider.
 * @param providerLocation Location of provider (where to import it from).
 * @param valueName Name of the value.
 * @param valueLocation Location of value (where to import it from).
 */
export function addNgModuleValueProvider(
  host: Tree,
  modulePath: Path,
  providerName: string,
  providerLocation: string,
  valueName: string,
  valueLocation?: string
): void {
  const toImport = [new TsImport(providerName, providerLocation)];
  if (valueLocation) {
    toImport.push(new TsImport(valueName, valueLocation));
  }
  addNgModuleMetadata(
    host,
    modulePath,
    'providers',
    `{ provide: ${providerName}, useValue: ${valueName} }`,
    toImport
  );
}

/**
 * Adds a provider of type `{provide: providerName, useClass: className}` into
 * an `NgModule`.
 * @param host Source tree.
 * @param modulePath Path of the Angular module on which to add the provider.
 * @param providerName Name of the provider.
 * @param providerLocation Location of provider (where to import it from).
 * @param className Name of the class.
 * @param classLocation Location of class (where to import it from).
 */
export function addNgModuleClassProvider(
  host: Tree,
  modulePath: Path,
  providerName: string,
  providerLocation: string,
  className: string,
  classLocation?: string
): void {
  const toImport = [new TsImport(providerName, providerLocation)];
  if (classLocation) {
    toImport.push(new TsImport(className, classLocation));
  }
  addNgModuleMetadata(
    host,
    modulePath,
    'providers',
    `{ provide: ${providerName}, useClass: ${className} }`,
    toImport
  );
}

/**
 * Adds an export into an `NgModule`.
 * @param host Source tree.
 * @param modulePath Path of the Angular module on which to add the export.
 * @param exportName Name of the export.
 * @param exportLocation Location of export (where to import it from).
 */
export function addNgModuleExport(
  host: Tree,
  modulePath: Path,
  exportName: string,
  exportLocation: string
): void {
  addNgModuleMetadata(host, modulePath, 'exports', exportName, [
    new TsImport(exportName, exportLocation),
  ]);
}

/**
 * Adds a component to be bootstrap in an `NgModule`.
 * @param host Source tree.
 * @param modulePath Path of the Angular module on which to add the component to
 * bootstrap.
 * @param componentName Name of the component to bootstrap.
 * @param componentLocation Location of component to bootstrap (where to import
 * it from).
 */
export function addNgModuleBootstrap(
  host: Tree,
  modulePath: Path,
  componentName: string,
  componentLocation: string
): void {
  addNgModuleMetadata(host, modulePath, 'bootstrap', componentName, [
    new TsImport(componentName, componentLocation),
  ]);
}
