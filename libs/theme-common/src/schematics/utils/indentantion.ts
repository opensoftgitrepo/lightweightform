/**
 * Gets the indentation used in the given content by returning the indentation
 * of the first indented line.
 * @param content File content on which to get indentation.
 * @param dft Default indentation when no indentation was found.
 * @returns Indentation used in the file.
 */
export function getIndentation(content: string, dft = '  '): string {
  // The `[^*]` prevents obtaining indentation from within a JSDoc
  return (content.match(/^(( |\t)+)[^*]/m) || [, dft])[1]!;
}

/**
 * Indents new lines in the given content.
 * @param content Content with new lines to indend.
 * @param indent Indentation to use on new lines.
 * @returns Content with indented new lines.
 */
export function indentNewLines(content: string, indent = '  '): string {
  return content.replace(/(\r?\n)(.)/g, `$1${indent}$2`);
}
