/**
 * Removes all comments from an HTML file.
 * @param content HTML file content.
 * @returns HTML file content with no comments.
 */
export function htmlRemoveComments(content: string): string {
  const commentsRegExp = /<!--[^]*?-->/g;
  return content.replace(commentsRegExp, '');
}

/**
 * Whether a given HTML file contains an HTML element with a given tag.
 * @param content HTML file content.
 * @param tag Tag of element to check (inserted in a `RegExp`).
 * @returns Whether the given HTML file content contains an HTML element with
 * the given tag.
 */
export function htmlContainsTag(content: string, tag: string): boolean {
  const tagRegExp = new RegExp(`<${tag}\\b[^>]*>`);
  return htmlRemoveComments(content).match(tagRegExp) !== null;
}
