/**
 * Executes a task as soon as possible.
 * @param task Function or any object that implements `call()`.
 */
// There doesn't seem to be a way of importing this module using ES6 syntax that
// both TypeScript and Rollup accept
export const asap = require('asap'); // tslint:disable-line:no-var-requires
