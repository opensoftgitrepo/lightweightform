import { Directive, ElementRef, Input, SkipSelf } from '@angular/core';
import { makeObservable } from 'mobx';
import { computed, observable } from 'mobx-angular';

import { TableColumnContainer } from './table-column-container';

/**
 * Directive representing a table column (which may have sub-columns). Leaf
 * columns (columns with no sub-columns) may specify `minWidth` and `fixed` to
 * impose sizing restrictions on their respective table columns.
 */
@Directive({
  selector: 'lf-table-column, [lfTableColumn]',
  exportAs: 'lfTableColumn',
  providers: [
    { provide: TableColumnContainer, useExisting: TableColumnDirective },
  ],
})
export class TableColumnDirective extends TableColumnContainer {
  /**
   * Identifier of this column (used, for example, to set a label on the
   * respective table-header cell).
   */
  @observable
  @Input()
  public id: string;

  /**
   * Colspan of the column; this binding only has an effect if the column is a
   * leaf column.
   */
  @observable
  @Input()
  public colspan?: number | string;

  /**
   * Minimum width of a column (in pixels); this binding is only relevant when
   * applied to leaf columns. Note that the actual width of a column will be
   * relative the minimum width of all other columns; as an example a column
   * with a `minWidth` of `200` should always be twice as large as a column with
   * a `minWidth` of `100`.
   *
   * An array may be provided (with a length matching `colspan`) to specify the
   * `minWidth` of each column that this column spans over.
   */
  @observable
  @Input()
  public minWidth?: number | number[];

  /**
   * `fixed` columns have their width set to `minWidth` and their width won't
   * ever vary.
   */
  @observable
  @Input()
  public fixed = false;

  constructor(
    @SkipSelf() parentTableColumnContainer: TableColumnContainer | null = null,
    protected elementRef: ElementRef
  ) {
    super(parentTableColumnContainer, elementRef);
    makeObservable(this);
  }

  /**
   * `rowSpan` that should be set on the header table cell that represents this
   * column.
   * @returns`rowSpan` of the cell representing this column.
   */
  @computed
  public get rowSpan(): number {
    // Iterate until `columnContainer` is a `TableHeaderDirective` and
    // `rowIndex` represents the index of the header row (`<tr>`) to which the
    // cell (`<th>`) representing this column belongs
    let columnContainer;
    let rowIndex = 0;
    for (
      columnContainer = this.parentTableColumnContainer;
      columnContainer.parentTableColumnContainer;
      columnContainer = columnContainer.parentTableColumnContainer
    ) {
      ++rowIndex;
    }
    return this.height > 1 ? 1 : columnContainer.numberOfRows - rowIndex;
  }

  /**
   * `colSpan` that should be set on the header table cell that represents this
   * column.colSpan
   * @returns `colSpan` of the cell representing this column.
   */
  @computed
  public get colSpan(): number {
    return this.width;
  }
}
