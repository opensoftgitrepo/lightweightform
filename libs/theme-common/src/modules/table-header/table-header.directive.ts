import { Directive, ElementRef, Input } from '@angular/core';
import { isArrayLike } from '@lightweightform/storage';
import { makeObservable } from 'mobx';
import { computed, observable } from 'mobx-angular';

import { TableColumnContainer } from './table-column-container';
import { TableColumnDirective } from './table-column.directive';

/**
 * Directive used to simplify the creation of table headers and manage the size
 * of a table's max width, min width, and all columns. Tables using these
 * functionalities should have their `table-layout` CSS property set to `fixed`.
 *
 * This directive provides utilities to transform the following:
 * ```html
 * <lf-table-header>
 *   <lf-table-column id="id" [fixed]="true" [minWidth]="40"></lf-table-column>
 *   <lf-table-column id="name">
 *     <lf-table-column id="first"></lf-table-column>
 *     <lf-table-column id="last"></lf-table-column>
 *   </lf-table-column>
 *   <lf-table-column id="age"></lf-table-column>
 * </lf-table-header>
 * ```
 * Into a table header similar to the following diagram, where the `id` column
 * has a fixed size and the remaining columns expand (with similar width) to the
 * table's width.
 * ```
 * |    |         name        |          |
 * | id |---------------------|    age   |
 * |    |   first  |   last   |          |
 * ```
 *
 * The directive provides utilities to help a table set its minimum and maximum
 * widths, as well as column sizes (using `<colgroup>`) when necessary. The
 * following example shows the creation of a table that uses an instance of this
 * directive (named `header`) to manage width and column sizes. The example
 * assumes that the table's `table-layout` CSS property is set to `fixed`:
 * ```html
 * <table
 *   [style.minWidth]="header.tableMinWidth + 'px'"
 *   [style.maxWidth]="header.tableMaxWidth + 'px'"
 * >
 *   <colgroup>
 *     <col *ngFor="let width of header.columnWidths" [attr.width]="width"/>
 *   </colgroup>
 *
 *   <thead>
 *     <tr *ngFor="let row of header.rows">
 *       <th
 *         *ngFor="let column of row"
 *         [attr.rowSpan]="column.rowSpan"
 *         [attr.colSpan]="column.colSpan"
 *       >
 *         {{ column.id }} <!-- Can be used to fetch a label, for example -->
 *       </th>
 *     </tr>
 *   </thead>
 *
 *   <tbody><!-- ... --></tbody>
 * </table>
 * ```
 */
@Directive({
  selector: 'lf-table-header, [lfTableHeader]',
  exportAs: 'lfTableHeader',
  providers: [
    { provide: TableColumnContainer, useExisting: TableHeaderDirective },
  ],
})
export class TableHeaderDirective extends TableColumnContainer {
  /**
   * Default min width (in `px`) of columns.
   */
  @observable
  @Input()
  public defaultColumnsMinWidth = 100; // Sane default

  constructor(protected elementRef: ElementRef) {
    super(null, elementRef);
    makeObservable(this);
  }

  /**
   * Total number of rows of the header (should map to the number of `<tr>` of
   * the actual header).
   * @returns Number of rows of the header.
   */
  @computed
  public get numberOfRows(): number {
    return this.height - 1;
  }

  /**
   * Number of (leaf) columns of the header (number of columns actually
   * representing table cells/that don't have sub-columns).
   * @returns Number of leaf columns of the header.
   */
  @computed
  public get numberOfColumns(): number {
    return this.width;
  }

  /**
   * List of rows of the header, where each row contains a list of its columns.
   * This should directly map each row to a `<tr>` of the header, and each
   * column to a `<th>`. For the example depicted in the class description, this
   * method would return: `[[id, name, age], [first, last]]`.
   * @returns List of rows, where each row is a list of columns.
   */
  @computed
  public get rows(): TableColumnDirective[][] {
    const rows: TableColumnDirective[][] = [];
    for (let i = 0; i < this.numberOfRows; ++i) {
      rows.push(this.columnsOfRow(i));
    }
    return rows;
  }

  /**
   * Minimum width (in pixels) that should be set on the table that contains
   * this header. If the table does not have a min width, then the columns will
   * collapse indefinitely, we thus provide a minimum width that amounts to the
   * sum of the minimum width of all columns.
   * @returns Minimum width (in pixels) that should be set on the table.
   */
  @computed
  public get tableMinWidth(): number {
    return this.leafColumns().reduce(
      (sum, column) => sum + this.columnMinWidth(column),
      0
    );
  }

  /**
   * Maximum width (in pixels) that should be set on the table that contains
   * this header. Tables where all the columns have a fixed width cannot ever
   * expand, for that we must set a max width on the table, otherwise this
   * method returns `Infinity`.
   * @returns Maximum width (in pixels) that should be set on the table.
   */
  @computed
  public get tableMaxWidth(): number {
    return this.leafColumns().every((column) => column.fixed)
      ? this.tableMinWidth
      : Infinity;
  }

  /**
   * Width for all table columns, as they should be set in CSS (strings ending
   * in `'px'` or `'%'` depending on whether the column is fixed). Each of these
   * widths should be put on a `<col>` element inside a `<colgroup>` before the
   * definition of the `<thead>` within a `<table>` with the `table-layout` CSS
   * property set to `fixed`
   * @returns List of widths of each leaf column.
   */
  @computed
  public get columnWidths(): string[] {
    const leaves = this.leafColumns();
    // Columns not set as `fixed` should expand as necessary, as such we provide
    // them with widths that together add up to `100%`
    const dynamicMinWidthSum = leaves.reduce(
      (sum, column) => sum + (column.fixed ? 0 : this.columnMinWidth(column)),
      0
    );
    return leaves.reduce((array: string[], column) => {
      const colSpan = column.colSpan ?? 1;
      // Determine the widths of each column being spanned by `column`
      const colMinWidthArray = isArrayLike(column.minWidth)
        ? column.minWidth
        : // Distribute the width amongst the columns being spanned
          [...Array(colSpan)].map(() => this.columnMinWidth(column) / colSpan);
      for (let i = 0; i < colSpan; ++i) {
        // Columns will end up with a width of `0` when a `minWidth` array is
        // provided with length smaller than `colSpan`
        const colMinWidth = colMinWidthArray[i] ?? 0;
        array.push(
          column.fixed
            ? `${colMinWidth}px`
            : `${(colMinWidth / dynamicMinWidthSum) * 100}%`
        );
      }
      return array;
    }, []);
  }

  /**
   * Method that returns the list of leaf columns: columns whose label directly
   * represents a table cell/columns that contain no sub-columns. For the
   * example depicted in the class description, this method would return:
   * `[id, first, last, age]`.
   * @param columnContainer For internal use only (to recurse over columns).
   * @returns List of columns that are leaves.
   */
  private leafColumns(
    columnContainer: TableColumnContainer = this
  ): TableColumnDirective[] {
    return columnContainer.childrenColumns.reduce(
      (leaves: any, column) =>
        leaves.concat(
          column.childrenColumns.length === 0
            ? [column]
            : this.leafColumns(column)
        ),
      []
    );
  }

  /**
   * Method that returns the list of columns of a given row (`<tr>`) of the
   * table header. For the example depicted in the class description, for row
   * `0` this method would return `[id, name, age]`; for row `1` it would return
   * `[first, last]`.
   * @param index Index of row from which to fetch columns.
   * @param columnContainer For internal use only (to recurse over columns).
   * @returns List of columns of row with index `index`.
   */
  private columnsOfRow(
    index: number,
    columnContainer: TableColumnContainer = this
  ): TableColumnDirective[] {
    return index === 0
      ? columnContainer.childrenColumns
      : columnContainer.childrenColumns.reduce(
          (rowColumns: any, column) =>
            rowColumns.concat(this.columnsOfRow(index - 1, column)),
          []
        );
  }

  /**
   * Minimum width of a column (or a default minimum width when none has been
   * created).
   * @param column Column whose minimum width we are interested in.
   * @returns Minimum width of the column.
   */
  private columnMinWidth(column: TableColumnDirective): number {
    return isArrayLike(column.minWidth)
      ? column.minWidth.reduce((sum, width) => sum + width, 0)
      : column.minWidth ?? this.defaultColumnsMinWidth * column.colSpan;
  }
}
