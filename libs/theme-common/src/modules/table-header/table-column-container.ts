import { Directive, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { makeObservable } from 'mobx';
import { computed, observable } from 'mobx-angular';

import { TableColumnDirective } from './table-column.directive';

/**
 * Class that represents a container of columns: super class of both the table
 * header and of the column (which may have sub-columns as children).
 */
@Directive()
// tslint:disable-next-line: directive-class-suffix
export abstract class TableColumnContainer implements OnInit, OnDestroy {
  private _childrenColumns: TableColumnContainer[] = observable.array();

  constructor(
    protected parentTableColumnContainer: TableColumnContainer | null,
    protected elementRef: ElementRef
  ) {
    makeObservable(this);
  }

  /**
   * List of column controllers that are direct children of this table column
   * container.
   * @returns List of column children.
   */
  @computed
  public get childrenColumns(): TableColumnContainer[] {
    return this._childrenColumns.slice();
  }

  /**
   * Height of a column container (height of the tree represented by the column
   * container as root, and its columns as children).
   * @returns Height of the column container.
   */
  @computed
  protected get height(): number {
    return (
      this._childrenColumns.reduce(
        (height, column) => Math.max(height, column.height),
        0
      ) + 1
    );
  }

  /**
   * Width of a column container (width of the tree represented by the column
   * container as root, and its columns as children).
   * @returns Width of the column container.
   */
  @computed
  protected get width(): number {
    // Use the `colspan` of a leaf column as its width
    return this instanceof TableColumnDirective &&
      this.childrenColumns.length === 0
      ? this.colspan != null
        ? +this.colspan
        : 1
      : this._childrenColumns.reduce((sum, column) => sum + column.width, 0);
  }

  public ngOnInit(): void {
    // Register column in parent column container in same order as declared
    if (this.parentTableColumnContainer) {
      const childrenColumns = this.parentTableColumnContainer._childrenColumns;

      // Find the index of the column which has an index in the parent container
      // greater than this column and insert this column before it; if no such
      // column is found, insert this column at the end
      const indexInParentEl = this.columnIndexInParentEl();
      let insertionIndex = -1;
      for (let i = 0, l = childrenColumns.length; i < l; ++i) {
        if (this.columnIndexInParentEl(childrenColumns[i]) > indexInParentEl) {
          insertionIndex = i;
          break;
        }
      }
      if (insertionIndex === -1) {
        childrenColumns.push(this);
      } else {
        childrenColumns.splice(insertionIndex, 0, this);
      }
    }
  }

  public ngOnDestroy(): void {
    // Unregister column
    if (this.parentTableColumnContainer) {
      this.parentTableColumnContainer._childrenColumns.splice(
        this.parentTableColumnContainer._childrenColumns.indexOf(this),
        1
      );
    }
  }

  /**
   * Index of the column's element in the parent container's element. Used to
   * make sure that we're inserting columns in the order declared in the HTML
   * file (needed when conditionally rendering columns).
   * @param column Column for which to get index.
   * @returns Index of column in parent container's element.
   */
  private columnIndexInParentEl(column: TableColumnContainer = this): number {
    return Array.from(
      column.parentTableColumnContainer!!.elementRef.nativeElement.children
    ).indexOf(column.elementRef.nativeElement);
  }
}
