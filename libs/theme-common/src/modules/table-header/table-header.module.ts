import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { TableColumnDirective } from './table-column.directive';
import { TableHeaderDirective } from './table-header.directive';

/**
 * Exported/declared components and directives.
 */
const DECLARATIONS = [TableColumnDirective, TableHeaderDirective];

/**
 * LF table header module.
 */
@NgModule({
  imports: [CommonModule],
  declarations: DECLARATIONS,
  exports: DECLARATIONS,
  providers: [],
})
export class LfTableHeaderModule {}
