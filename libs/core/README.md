# Lightweightform/core

This package provides the core functionality for Lightweightform, barren of all
theme-related logic. This package may be used on its own but its mostly useful
when used together with a Lightweightform theme package such as the officially
provided
[Bootstrap theme](https://www.npmjs.com/package/@lightweightform/bootstrap-theme).

In particular, this package provides:

- Angular services for interacting with a Lightweightform application:
  - A storage service to interact with the application's schema and value (an
    extended instance of the storage provided by
    [`@lightweightform/storage`](https://www.npmjs.com/package/@lightweightform/storage)).
  - A router service built on top of the Angular router: used to navigate to
    routes that can be associated with storage values.
  - A file storage service to allow saving the application's current value to a
    file, or loading the application's value given a file.
  - An internationalisation service that interacts well with the application's
    storage.
- Utility directives/classes used to help build Lightweightform themes.

## Example setup

```typescript
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {
  I18nLanguages,
  LF_APP_I18N,
  LF_APP_SCHEMA,
  LF_I18N_LANGUAGE_QUERY_PARAM,
  LF_ROUTER_BASE_PATH,
  LfFileStorage,
  LfI18n,
  LfRouter,
  LfRoutes,
  LfSerializer,
  LfStorage,
  LfUnloadAlert,
} from '@lightweightform/core';
import { recordSchema, RecordSchema } from '@lightweightform/storage';

// Application schema
export const schema: RecordSchema = recordSchema({
  /*...*/
});
// Application routes
export const routes: LfRoutes = {
  /*...*/
};
// Application i18n
export const i18n: I18nLanguages = {
  /*...*/
};

@NgModule({
  imports: [
    /*...*/
    RouterModule.forRoot(routes),
  ],
  providers: [
    LfStorage,
    LfI18n,
    LfFileStorage,
    LfUnloadAlert,
    LfRouter,
    { provide: LF_APP_SCHEMA, useValue: schema },
    { provide: LF_ROUTER_BASE_PATH, useValue: '/' },
    { provide: LF_APP_I18N, useValue: i18n },
    { provide: LF_I18N_LANGUAGE_QUERY_PARAM, useValue: 'l' },
    { provide: LF_ROUTER_BASE_PATH, useValue: '/' },
  ],
})
export class AppModule {}
```

## Documentation

All Lightweightform documentation is available at:
https://docs.lightweightform.io.

The `@lightweightform/core` API is available at:
https://docs.lightweightform.io/api/core.

## Browser support

- **Internet Explorer**: 9+ (requires at least `Map` and `Array.from` polyfills)
- **Other browsers**: Latest 2 versions
