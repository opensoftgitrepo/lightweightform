import { isArrayLike } from '@lightweightform/storage';

/**
 * Force a value to be an array (do nothing if already an array, wrap in an
 * array if the value isn't `undefined`, return an empty array otherwise).
 * @param val Value to force to be an array.
 * @returns Array.
 */
export function arrayfy<T>(val?: T | T[]): T[] {
  return val === undefined ? [] : isArrayLike(val) ? val : [val];
}
