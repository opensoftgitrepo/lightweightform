import { ValidationIssuesMap } from '@lightweightform/storage';

/**
 * Type error thrown by when attempting to load a value with invalid type.
 */
export class LoadTypeError extends TypeError {
  constructor(
    message,
    /**
     * Map of type issues found when typechecking the value.
     */
    public issuesMap: ValidationIssuesMap
  ) {
    super(message);
    this.name = this.constructor.name;
  }
}
