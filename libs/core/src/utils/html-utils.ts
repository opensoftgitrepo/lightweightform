/**
 * Encodes a string so that it can be safely inserted as HTML text (for example
 * using `innerHTML`). Adapted from [mustache.js](
 * https://github.com/janl/mustache.js/blob/master/mustache.js#L73).
 * @param text Text to encode.
 * @returns Encoded text which can be safely inserted as HTML text.
 */
export function encodeHTML(text: string): string {
  const charMap = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;',
    '/': '&#x2F;',
    '`': '&#x60;',
    '=': '&#x3D;',
  };
  return String(text).replace(/[&<>"'`=\/]/g, (c) => charMap[c]);
}
