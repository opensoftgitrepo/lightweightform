import { InjectionToken } from '@angular/core';
import { configure } from 'mobx';

type MobXConfigureOptions = Parameters<typeof configure>[0];

/**
 * Injection token used to specify the MobX configuration. The provided config
 * will be merged with the default configuration `DEFAULT_MOBX_CONFIG` when
 * applied.
 */
export const MOBX_CONFIG = new InjectionToken<MobXConfigureOptions>(
  'MOBX_CONFIG'
);

/**
 * Default MobX configuration.
 */
export const DEFAULT_MOBX_CONFIG: MobXConfigureOptions = {
  enforceActions: 'never',
  useProxies: 'ifavailable',
};

/**
 * Service responsible by globally configuring MobX. Users may provide a custom
 * configuration via `MOBX_CONFIG` which will be merged with the default
 * configuration `DEFAULT_MOBX_CONFIG`.
 */
export function configureMobX(config: MobXConfigureOptions | null) {
  configure({ ...DEFAULT_MOBX_CONFIG, ...config });
}
