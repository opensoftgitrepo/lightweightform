/**
 * Allow spying on MobX when not in production mode.
 */
import { configure, Lambda, spy } from 'mobx';

/**
 * Name of the function added to the global scope to enable MobX spying from the
 * browser dev tools.
 */
const GLOBAL_SPYING_FN = 'spyOnMobX';
/**
 * Name of the function added to the global scope to enable changing MobX's
 * configuration on the fly.
 */
const GLOBAL_CONFIGURE_FN = 'configureMobX';
/**
 * Name of the key used in session storage to keep track of whether the
 * developer is spying on MobX.
 */
const STORAGE_SPYING_KEY = 'LF_SPY_ON_MOBX';
/**
 * Prefix used on messages output by `spyOnMobX`.
 */
const SPY_ON_MOBX_MSG_PREFIX = `[${GLOBAL_SPYING_FN}]`;

// tslint:disable:no-console

/**
 * Function to spy on all MobX events. When not in production, a function of the
 * same name is exposed in the `window` object with the following signature:
 * `spyOnMobX(shouldSpy: boolean, persist?: boolean): void`.
 * @returns Disposer function to stop spying on MobX.
 */
export function spyOnMobX(): Lambda {
  // Helper functions
  const print = (method, msg, ...params) =>
    console[method](
      `%c${SPY_ON_MOBX_MSG_PREFIX}%c ${msg}`,
      'color: dimgray',
      '',
      ...params
    );
  const log = (msg, ...params) => print('log', msg, ...params);
  const group = (msg, ...params) => print('groupCollapsed', msg, ...params);
  const groupEnd = console.groupEnd;

  log('Started spying on MobX');
  const disposer = spy((evt) => {
    let msg = `${evt.type}: `;
    const params: any[] = [];
    const extraMsgs: Array<{ msg: string; params: any[] }> = [];
    if (evt.type) {
      switch (evt.type) {
        case 'action':
          msg += `${evt.name}(${evt.arguments.map((_) => '%o').join(', ')})`;
          params.push(...evt.arguments.map((arg) => arg));
          break;
        case 'scheduled-reaction':
        case 'reaction':
          msg += evt.name;
          break;
        case 'error':
          msg += evt.message;
          break;
        case 'add':
        case 'create':
        case 'remove':
        case 'delete':
          msg += evt.debugObjectName;
          msg += ': %o';
          params.push(
            evt.type === 'remove' || evt.type === 'delete'
              ? evt.oldValue
              : evt.newValue
          );
          break;
        case 'update':
          msg += evt.debugObjectName;
          msg += ': %o → %o';
          params.push(evt.oldValue, evt.newValue);
          break;
        case 'splice':
          msg +=
            `${evt.debugObjectName}: @${evt.index} +${evt.addedCount} ` +
            `-${evt.removedCount}`;
          extraMsgs.push(
            { msg: 'added: %o', params: [evt.added] },
            { msg: 'removed: %o', params: [evt.removed] }
          );
          break;
      }
      // Create a group for spy events that are part of a group or those that
      // have extra messages to display
      if (evt.spyReportStart || extraMsgs.length > 0) {
        group(msg, ...params);
        extraMsgs.forEach(({ msg: extraMsg, params: extraParams }) =>
          log(extraMsg, ...extraParams)
        );
      } else {
        log(msg, ...params);
      }
    }
    // Log time when available
    if (evt.type === 'report-end' && evt.time) {
      log(`%c(${evt.time} ms)`, 'color: dimgray');
    }
    // Close spy event groups or groups created to display the extra messages
    if (
      evt.type === 'report-end' ||
      (!evt.spyReportStart && extraMsgs.length > 0)
    ) {
      groupEnd();
    }
  });

  return () => {
    disposer();
    log('Stopped spying on MobX');
  };
}

/**
 * When in development mode, this functions adds a function to the global scope
 * which allows developers to spy on MobX. The state of the spying may persist
 * (being kept in session storage) if specified.
 *
 * Furthermore, adds a global function to allow changing the MobX's
 * configuration on the fly.
 */
export function setUpSpyOnMobX() {
  if (
    process.env.NODE_ENV !== 'production' &&
    process.env.NODE_ENV !== 'test'
  ) {
    let disposeSpy: Lambda | null = null;
    window[GLOBAL_SPYING_FN] = (shouldSpy: boolean, persist?: boolean) => {
      if (typeof shouldSpy !== 'boolean') {
        console.error(
          `Call ${GLOBAL_SPYING_FN}(shouldSpy: boolean, persist?: boolean) to` +
            'start/stop spying on MobX'
        );
      } else {
        if (typeof sessionStorage !== 'undefined' && persist != null) {
          sessionStorage.setItem(STORAGE_SPYING_KEY, JSON.stringify(shouldSpy));
        }
        if (disposeSpy === null && shouldSpy) {
          disposeSpy = spyOnMobX();
        } else if (disposeSpy !== null && !shouldSpy) {
          disposeSpy();
          disposeSpy = null;
        }
      }
    };
    window[GLOBAL_CONFIGURE_FN] = configure;

    // Start spying if the storage state is `true`
    if (
      typeof sessionStorage !== 'undefined' &&
      JSON.parse(sessionStorage.getItem(STORAGE_SPYING_KEY) || 'false')
    ) {
      disposeSpy = spyOnMobX();
    } else {
      console.log(
        'Lightweightform is running in development mode. You can spy on all ' +
          'MobX related events by calling ' +
          `${GLOBAL_SPYING_FN}(shouldSpy: boolean, persist?: boolean).`
      );
    }
  }
}
