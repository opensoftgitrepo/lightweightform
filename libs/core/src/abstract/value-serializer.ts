import { Schema } from '@lightweightform/storage';

/**
 * Abstract class which specifies the API used by serialisers to transform
 * application values into strings to be saved into files and vice-versa.
 */
export abstract class LfSerializer {
  /**
   * Character encoding to set as metadata when saving the file and used to
   * decode files.
   */
  public abstract charset: string;
  /**
   * Mime type of the saved file.
   */
  public abstract mimeType: string;
  /**
   * Accepted type of files (used by the `<input>` element when choosing a file
   * to load).
   */
  public abstract accept: string;
  /**
   * Extension of file to save.
   */
  public abstract extension: string;
  /**
   * Whether to automatically add an UTF BOM (not applied to IE9-).
   */
  public abstract autoBOM: boolean;
  /**
   * A function used to encode the content according to the intended charset.
   */
  public abstract encode?: (content: string) => BlobPart[];

  /**
   * Function which serializes the JS value provided by the storage into a
   * string to be saved into a file. The provided object is a copy of the actual
   * storage value (obtained via the storage's `getAsJS` method), so the
   * serialiser is allowed to manipulate the passed value.
   * @param js JS value to serialise.
   * @param schema Schema of the value to serialise.
   * @param path Storage path to the value to serialise.
   * @returns Serialised string to save into a file.
   */
  public abstract serialize(js: any, schema: Schema, path: string): string;

  /**
   * Function which deserialises a string obtained from a file into a JS value
   * to load into the storage.
   * @param text String to deserialise.
   * @param schema Schema of the value being deserialised.
   * @param path Storage path to the value being deserialised.
   * @returns JS value to load into the storage.
   */
  public abstract deserialize(text: string, schema: Schema, path: string): any;
}
