import { Directive, Input, OnDestroy, OnInit } from '@angular/core';
import { resolvePath } from '@lightweightform/storage';
import { makeObservable, observe } from 'mobx';
import { computed, observable } from 'mobx-angular';
import { PromiseState } from 'mobx-utils';

import { GetTranslationOptions, LfI18n } from '../services/i18n.service';
import { LfStorage } from '../services/storage.service';

/**
 * Abstract class that directives which are reliant on a "path" should extend.
 *
 * Components expecting children "path-based" directives should `provide` this
 * class to their children so that their children can use relative paths.
 * Example:
 * ```typescript
 * @Component({
 *   selector: 'some-component',
 *   changeDetection: ChangeDetectionStrategy.OnPush,
 *   providers: [{provide: PathBased, useExisting: SomeComponent}],
 *   templateUrl: './some-component.component.html',
 * })
 * export class SomeComponent extends PathBased {
 *   constructor(
 *     lfStorage: LfStorage,
 *     lfI18n: LfI18n,
 *     @Optional()
 *     @SkipSelf()
 *     parentPathBasedComponent: PathBased,
 *   ) {
 *     super(lfStorage, lfI18n, parentPathBasedComponent);
 *   }
 * }
 * ```
 *
 * This class implements Angular's `OnInit` and `OnDestroy` interfaces, meaning
 * that directives that extend it must run
 * `super.ngOnInit()`/`super.ngOnDestroy()` on their own
 * `ngOnInit`/`ngOnDestroy` methods if they require them.
 *
 * Directives extending this class may optionally define a method named
 * `validatePath` which will be called whenever the directive's path changes;
 * `this.path` may be accessed within to make sure that the path is "valid"
 * according to the directive (e.g. by making sure that the path corresponds to
 * a schema of a certain type).
 */
@Directive()
// tslint:disable-next-line: directive-class-suffix
export abstract class PathBased implements OnInit, OnDestroy {
  /**
   * Path in the schema. It may be an absolute path or a relative path (in which
   * case the directive will append the path to it's parent component's path).
   * Defaults to `'.'`.
   */
  // tslint:disable-next-line:no-input-rename
  @observable
  @Input('path')
  public _path = '.';

  /**
   * Disposer for the observer which listens to changes in the path.
   */
  private disposePathObserve: () => void;

  constructor(
    protected parentPathBasedComponent: PathBased | null,
    protected lfStorage: LfStorage,
    protected lfI18n: LfI18n
  ) {
    makeObservable(this);
  }

  /**
   * Absolute path associated with the directive.
   */
  @computed
  public get path(): string {
    return resolvePath(
      (this.parentPathBasedComponent && this.parentPathBasedComponent.path) ||
        '/',
      this._path
    );
  }

  /**
   * Storage instance with its "current path" set to this directive's path (all
   * methods will be called relative to this directive's path).
   */
  @computed
  public get relativeStorage(): LfStorage {
    return this.lfStorage.relativeStorage(this.path);
  }

  public ngOnInit() {
    // Observe the directive's path and validate it
    this.disposePathObserve = observe(
      this,
      'path',
      () => {
        if (process.env.NODE_ENV !== 'production') {
          // Verify that the path corresponds to a valid schema
          this.lfStorage.validatePath(this.path);
        }
        this.validatePath && this.validatePath();
      },
      true
    );
  }

  public ngOnDestroy() {
    this.disposePathObserve && this.disposePathObserve();
  }

  /**
   * Obtain the translation for a given key associated to the directive's path.
   * @param key Key of the translation.
   * @param options Options for fetching the translation.
   * @returns Translation for the provided key in the current language.
   */
  public translate(key: string, options?: GetTranslationOptions): any {
    return this.lfI18n.getFromPath(this.path, key, options);
  }

  /**
   * Status of a given translation if it is asynchronous and `useState` was used
   * to fetch it, or `FULFILLED` in all other cases.
   * @param key Key of the translation from which to fetch its status.
   * @returns The status of the translation when possible, `FULFILLED` in all
   * other cases.
   */
  public translationStatus(key: string): PromiseState {
    return this.lfI18n.translationStatus(this.path, key);
  }

  /**
   * Method that directives which are "path-based" should implement when they
   * want to restrict the paths they accept. This method is automatically called
   * whenever the directive's path changes. Its implementation is **not**
   * mandatory for directives which accept any path.
   */
  protected validatePath?(): void;
}
