import { Directive, Inject, Optional, SkipSelf } from '@angular/core';

import { PathBased } from '../abstract/path-based';
import { LfI18n } from '../services/i18n.service';
import { LfStorage } from '../services/storage.service';

@Directive({
  selector: 'lf-value, [lfValue]',
  providers: [{ provide: PathBased, useExisting: ValueDirective }],
  exportAs: 'lfValue',
})
export class ValueDirective extends PathBased {
  constructor(
    @Optional()
    @SkipSelf()
    @Inject(PathBased)
    parentPathBasedComponent: PathBased | null,
    lfStorage: LfStorage,
    lfI18n: LfI18n
  ) {
    super(parentPathBasedComponent, lfStorage, lfI18n);
  }
}
