import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewChild,
} from '@angular/core';
import {
  ComponentFixture,
  ComponentFixtureAutoDetect,
  TestBed,
} from '@angular/core/testing';
import { numberSchema, recordSchema } from '@lightweightform/storage';
import { makeObservable, runInAction } from 'mobx';
import { MobxAngularModule, observable } from 'mobx-angular';

import { LfI18n, LF_APP_I18N } from '../../services/i18n.service';
import { LfStorage, LF_APP_SCHEMA } from '../../services/storage.service';
import { ValueDirective } from '../value.directive';

// Schema used to test the value directive
const appSchema = recordSchema({
  rec: recordSchema({ num: numberSchema() }),
});
// I18n used to test translations
const i18n = {
  en: { '/rec': { text: 'Record', path: (ctx) => ctx.currentPath } },
};

// Example component using value directives
@Component({
  selector: 'lf-values-container',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <ng-container *mobxAutorun>
      <span lfValue [path]="outerPath" #outer>
        <span lfValue [path]="innerPath" #inner></span>
      </span>
    </ng-container>
  `,
})
class ValuesContainerComponent {
  @observable @Input() public outerPath = '/';
  @observable @Input() public innerPath = 'rec';
  @ViewChild('outer', { read: ValueDirective })
  public outer: ValueDirective;
  @ViewChild('inner', { read: ValueDirective })
  public inner: ValueDirective;

  constructor() {
    makeObservable(this);
  }
}

describe('(Path-based) value directive', () => {
  let fixture: ComponentFixture<ValuesContainerComponent>;
  let component: ValuesContainerComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MobxAngularModule],
      declarations: [ValueDirective, ValuesContainerComponent],
      providers: [
        { provide: ComponentFixtureAutoDetect, useValue: true },
        { provide: LF_APP_SCHEMA, useValue: appSchema },
        { provide: LF_APP_I18N, useValue: i18n },
        LfStorage,
        LfI18n,
      ],
    });
    fixture = TestBed.createComponent(ValuesContainerComponent);
    component = fixture.componentInstance;
  });

  it('allows mutation of path', () => {
    expect(component.outer.path).toBe('/');
    expect(component.inner.path).toBe('/rec');
    runInAction(() => {
      component.outerPath = 'rec';
      component.innerPath = 'num';
    });
    expect(component.outer.path).toBe('/rec');
    expect(component.inner.path).toBe('/rec/num');
  });

  it('supports `relativeStorage`', () => {
    expect(component.outer.relativeStorage.currentPath).toBe('/');
    expect(component.inner.relativeStorage.currentPath).toBe('/rec');
  });

  it('supports translations', () => {
    expect(component.inner.translate('text')).toBe('Record');
    expect(component.inner.translate('path')).toBe('/rec');
    // TODO: Test translations returning promises
  });
});
