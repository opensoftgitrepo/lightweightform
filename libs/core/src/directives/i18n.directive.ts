import {
  Directive,
  HostBinding,
  Inject,
  InjectionToken,
  Input,
  Optional,
} from '@angular/core';
import { makeObservable } from 'mobx';
import { computed, observable } from 'mobx-angular';
import { expr } from 'mobx-utils';

import { PathBased } from '../abstract/path-based';
import { LfI18n } from '../services/i18n.service';
import { LfStorage } from '../services/storage.service';
import { encodeHTML } from '../utils/html-utils';

/**
 * Injection token used to specify whether the i18n directive should render its
 * text as HTML (by default it renders it as plain text).
 */
export const LF_I18N_RENDER_AS_HTML = new InjectionToken<boolean>(
  'LF_I18N_RENDER_AS_HTML'
);

/**
 * Directive used to render a translation within the element on which it is
 * applied. This directive uses `innerHTML` to render the translation text,
 * encoding the HTML by default. It is possible to render HTML by providing the
 * option.
 */
@Directive({
  selector: 'lf-i18n, [lfI18n]',
  exportAs: 'lfI18n',
})
export class I18nDirective extends PathBased {
  /**
   * Key used to access the i18n value.
   */
  @observable
  @Input()
  public key: string;
  /**
   * Whether to use the state to cache the value of the translation.
   */
  @Input()
  public useState = false;
  /**
   * Transformation function to apply to the i18n value.
   */
  @observable
  @Input()
  public transform?: (value: any) => string;

  constructor(
    @Optional()
    @Inject(PathBased)
    parentPathBasedComponent: PathBased | null,
    lfStorage: LfStorage,
    lfI18n: LfI18n,
    @Optional()
    @Inject(LF_I18N_RENDER_AS_HTML)
    private i18nRenderAsHtml: boolean = false
  ) {
    super(parentPathBasedComponent, lfStorage, lfI18n);
    makeObservable(this);
  }

  /**
   * Translation rendered by the directive.
   */
  public get translation(): any {
    return this.useState
      ? this._translationFromState
      : this._translationNotFromState;
  }

  @HostBinding('innerHTML')
  public get _hostInnerHTML(): string {
    let value = this.translation;
    value === undefined && (value = '');
    return this.i18nRenderAsHtml ? value.toString() : encodeHTML(value);
  }

  private get _translationFromState(): any {
    const value = this.translate(this.key, { useState: true });
    return expr(() => (this.transform ? this.transform(value) : value));
  }

  @computed
  private get _translationNotFromState(): any {
    const value = this.translate(this.key);
    return this.transform ? this.transform(value) : value;
  }
}
