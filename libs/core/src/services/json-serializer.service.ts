import { Inject, Injectable, InjectionToken, Optional } from '@angular/core';
import {
  appendToPath,
  isClientOnlySchema,
  isDateSchema,
  isListSchema,
  isMapSchema,
  isRecordSchema,
  isSimpleSchema,
  isTableSchema,
  isTupleSchema,
  Schema,
} from '@lightweightform/storage';

import { LfSerializer } from '../abstract/value-serializer';
import { arrayfy } from '../utils/common';

/**
 * Function used to (de)serialise a value.
 * @param value Value to (de)serialise.
 * @param schema Schema of the value being (de)serialised.
 * @param path Path of the value being (de)serialised.
 * @returns (De)serialised value (not necessarily the last form since the value
 * will be passed to the next (de)serialiser).
 */
export type LfJsonTransformationFn = (
  value: any,
  schema: Schema,
  path: string
) => any;

/**
 * Type of object used to apply transformations during a (de)serialisation.
 */
export interface LfJsonTransformer {
  /**
   * Serialisation function.
   */
  serialize?: LfJsonTransformationFn;
  /**
   * Deserialisation function.
   */
  deserialize?: LfJsonTransformationFn;
}

/**
 * Options used to configure the JSON serialiser.
 */
export interface LfJsonSerializerOptions {
  /**
   * Character encoding to set as metadata when saving the file and used to
   * decode files. Defaults to `utf-8`.
   */
  charset?: string;
  /**
   * Whether to automatically add an UTF BOM (not applied to IE9-). Defaults to
   * `true`.
   */
  autoBOM?: boolean;
  /**
   * A function used to encode the content according to the intended charset.
   * This is something that JavaScript cannot natively do (if not provided, the
   * provided charset will be set as file metadata but the content will be
   * encoded as `utf-8`).
   */
  encode?: (content: string) => BlobPart[];
  /**
   * Space used when printing the value as a JSON string. Defaults to `0` (no
   * spaces) or `'\t'` (use tabs as spaces) depending on whether the app is
   * running in production mode.
   */
  space?: number | string;
  /**
   * Simple transformer or list of simple transformers: a simple transformer is
   * an object which provides functions to (de)serialise non-`null` values of
   * simple schemas (boolean, date, number, and string schemas).
   *
   * When a list of simple transformers is provided, they run sequentially (in
   * reverse order when deserialising). These \[serialisers|deserialisers] will
   * run \[before|after] the \[serialiser|deserialiser] defined in a schema's
   * `jsonSimpleTransform` respectively.
   *
   * Return `undefined` or the provided value to perform no transformation. The
   * JSON serialiser service will perform its standard transformation when the
   * value remains the same after all simple transformations run.
   *
   * The following example serialises booleans as "yes/no":
   * ```typescript
   * {
   *   simpleTransform: {
   *     serialize: (val, schema, path) => {
   *       if (isBooleanSchema(schema)) {
   *         return val ? 'yes' : 'no';
   *       }
   *     },
   *     deserialize: (val, schema, path) => {
   *       if (isBooleanSchema(schema)) {
   *         return val === 'yes';
   *       }
   *     },
   *   }
   * }
   * ```
   */
  simpleTransform?: LfJsonTransformer | LfJsonTransformer[];
  /**
   * Transformer or list of transformers: a transformer is an object providing
   * functions to (de)serialise values. Serialisers transform values after they
   * have been serialised by the JSON serialiser service but before they are
   * `JSON.stringify`ed. Deserialisers transform values after they have been
   * `JSON.parse`d but before they are deserialised by the JSON serialiser
   * service.
   *
   * When a list of transformers are provided, they will run sequentially (in
   * reverse order when deserialising). These serialisers/deserialisers will run
   * before/after the serialiser/deserialiser defined in a schema's
   * `jsonTransform` respectively. To perform no transformation, functions
   * should return the provided value untouched.
   */
  transform?: LfJsonTransformer | LfJsonTransformer[];
}

/**
 * Extra properties that can be added to any schema to configure how its values
 * should be serialised by the JSON serialiser.
 */
export interface LfJsonSerializerSchemaProperties {
  /**
   * Simple transformer: an object which provides functions to (de)serialise
   * non-`null` values of simple schemas (boolean, date, number, and string
   * schemas).
   *
   * This \[serialiser|deserialiser] will run \[after|before] the
   * \[serialisers|deserialisers] defined in the JSON serialiser service
   * `simpleTransform` option respectively.
   */
  jsonSimpleTransform?: LfJsonTransformer;
  /**
   * Transformer: an object providing functions to (de)serialise values of this
   * schema. The serialiser transforms values after they have been serialised by
   * the JSON serialiser service but before they are `JSON.stringify`ed. The
   * deserialiser transforms values after they have been `JSON.parse`d but
   * before they are deserialised by the JSON serialiser service.
   *
   * This \[serialiser|deserialiser] will run \[after|before] the
   * \[serialisers|deserialisers] defined in the JSON serialiser service
   * `transform` option respectively. To perform no transformation, functions
   * should return the provided value untouched.
   */
  jsonTransform?: LfJsonTransformer;
}

/**
 * Injector used to set the JSON serialiser options.
 */
export const LF_JSON_SERIALIZER_OPTIONS: InjectionToken<LfJsonSerializerOptions> =
  new InjectionToken('LF_JSON_SERIALIZER_OPTIONS');

/**
 * Transformer used to save/load application values as JSON.
 */
@Injectable({ providedIn: 'root' })
export class LfJsonSerializer extends LfSerializer {
  public charset = 'utf-8';
  public mimeType = 'application/json';
  // `.txt` and `text/plain` added for IE 9- support.
  public accept = '.json,.txt,application/json,text/plain';
  public extension = 'json';
  public autoBOM = true;
  public encode?: (content: string) => BlobPart[];
  /**
   * Space used when printing the value as a JSON string.
   */
  public space: number | string =
    process.env.NODE_ENV === 'production' ? 0 : '\t';
  /**
   * Simple transform(s).
   */
  public simpleTransform?: LfJsonTransformer | LfJsonTransformer[];
  /**
   * Transform(s).
   */
  public transform?: LfJsonTransformer | LfJsonTransformer[];

  constructor(
    @Optional()
    @Inject(LF_JSON_SERIALIZER_OPTIONS)
    options: LfJsonSerializerOptions | null
  ) {
    super();
    if (options) {
      this.setOptions(options);
    }
  }

  /**
   * Sets the serialiser's options.
   * @param options Options to set.
   */
  public setOptions(options: LfJsonSerializerOptions): void {
    options.charset !== undefined && (this.charset = options.charset);
    options.autoBOM !== undefined && (this.autoBOM = options.autoBOM);
    options.encode !== undefined && (this.encode = options.encode);
    options.space !== undefined && (this.space = options.space);
    options.simpleTransform !== undefined &&
      (this.simpleTransform = options.simpleTransform);
    options.transform !== undefined && (this.transform = options.transform);
  }

  public serialize(
    js: any,
    schema: Schema & LfJsonSerializerSchemaProperties,
    path: string
  ): string {
    const json = this.jsToJson(js, schema, path);
    return json === undefined
      ? ''
      : JSON.stringify(this.jsToJson(js, schema, path), null, this.space);
  }

  public deserialize(
    text: string,
    schema: Schema & LfJsonSerializerSchemaProperties,
    path: string
  ): any {
    return text === ''
      ? undefined
      : this.jsonToJs(JSON.parse(text), schema, path);
  }

  /**
   * Convert a storage-compatible JS value into a JSON-compatible value (handle
   * dates and maps, especially).
   * @param js Storage-compatible JS value to convert into a JSON-compatible
   * value.
   * @param schema Schema of the value to convert.
   * @param path Path of the value to convert.
   * @returns JSON-compatible version of provided value.
   */
  public jsToJson(
    js: any,
    schema: Schema & LfJsonSerializerSchemaProperties,
    path: string
  ) {
    // JSON-compatible value
    let json: any;

    if (js !== undefined) {
      if (js === null) {
        json = null;
      } else if (isSimpleSchema(schema)) {
        // Run custom simple serialisers
        let serialized = this.runSimpleSerializers(js, schema, path);

        // Perform "standard" serialisation when no serialiser changed the value
        if (js === serialized) {
          serialized = isDateSchema(schema) ? js.toISOString() : js;
        }
        json = serialized;
      } else if (isListSchema(schema) || isTableSchema(schema)) {
        const chSchema = isListSchema(schema)
          ? schema.elementsSchema
          : schema.rowsSchema;
        json = js.reduce((arr, val, i) => {
          const ch = this.jsToJson(val, chSchema, appendToPath(path, i));
          ch !== undefined && arr.push(ch);
          return arr;
        }, []);
      } else if (isMapSchema(schema)) {
        // Transform maps into objects
        const chSchema = schema.valuesSchema;
        json = Array.from(js.entries()).reduce(
          (obj: Record<string, any>, [k, val]) => {
            const ch = this.jsToJson(val, chSchema, appendToPath(path, k));
            ch !== undefined && (obj[k] = ch);
            return obj;
          },
          {}
        );
      } else if (isRecordSchema(schema) || isTupleSchema(schema)) {
        const chSchemas = isRecordSchema(schema)
          ? schema.fieldsSchemas
          : schema.elementsSchemas;
        json = isRecordSchema(schema) ? {} : [];
        for (const id of Object.keys(chSchemas)) {
          const chSchema = chSchemas[id];
          // Map `undefined` to `null` in tuples with client-only schemas
          if (isClientOnlySchema(chSchema)) {
            isTupleSchema(schema) && (json[id] = null);
          } else {
            const ch = this.jsToJson(js[id], chSchema, appendToPath(path, id));
            ch !== undefined && (json[id] = ch);
          }
        }
      }
    }

    // Run custom serialisers
    json = this.runSerializers(json, schema, path);

    return json;
  }

  /**
   * Convert a JSON-compatible value into a storage-compatible JS value (handle
   * dates and maps, especially).
   * @param json JSON-compatible value to convert into a storage-compatible JS
   * value.
   * @param schema Schema of the value to convert.
   * @param path Path of the value to convert.
   * @returns Storage-compatible JS value.
   */
  public jsonToJs(
    json: any,
    schema: Schema & LfJsonSerializerSchemaProperties,
    path: string
  ) {
    // Storage compatible JS value
    let js: any;

    // Run custom deserialisers
    json = this.runDeserializers(json, schema, path);

    if (json !== undefined) {
      if (json === null) {
        js = null;
      } else if (isSimpleSchema(schema)) {
        // Run custom simple deserialisers
        let deserialised = this.runSimpleDeserializers(json, schema, path);

        // Perform "standard" deserialisation when no deserialiser changed the
        // value (handle dates)
        if (json === deserialised) {
          deserialised = isDateSchema(schema) ? new Date(json) : json;
        }
        js = deserialised;
      } else if (isListSchema(schema) || isTableSchema(schema)) {
        const chSchemas = isListSchema(schema)
          ? schema.elementsSchema
          : schema.rowsSchema;
        for (let i = 0, l = json.length; i < l; ++i) {
          json[i] = this.jsonToJs(json[i], chSchemas, appendToPath(path, i));
        }
        js = json;
      } else if (isMapSchema(schema)) {
        // Transform objects into maps
        const chSchema = schema.valuesSchema;
        js = new Map(
          Object.keys(json).map(
            (k) =>
              [k, this.jsonToJs(json[k], chSchema, appendToPath(path, k))] as [
                string,
                any
              ]
          )
        );
      } else if (isRecordSchema(schema) || isTupleSchema(schema)) {
        const chSchemas = isRecordSchema(schema)
          ? schema.fieldsSchemas
          : schema.elementsSchemas;
        js = isRecordSchema(schema) ? {} : [];
        for (const id of Object.keys(chSchemas)) {
          const chSchema = chSchemas[id];
          // Map `null` to `undefined` in tuples with client-only schemas
          if (isClientOnlySchema(chSchema)) {
            isTupleSchema(schema) && (js[id] = undefined);
          } else {
            js[id] = this.jsonToJs(json[id], chSchema, appendToPath(path, id));
          }
        }
      }
    }

    return js;
  }

  /**
   * Runs all simple serialisers for a value of a simple schema.
   * @param value Value on which to run simple serialisers.
   * @param schema Schema of value on which to run serialisers.
   * @param path Path of value on which to run serialisers.
   * @returns Serialised value.
   */
  private runSimpleSerializers(
    value: any,
    schema: Schema & LfJsonSerializerSchemaProperties,
    path: string
  ) {
    let serialized = value;
    for (const t of [
      ...arrayfy(this.simpleTransform),
      schema.jsonSimpleTransform,
    ]) {
      const res = t && t.serialize && t.serialize(serialized, schema, path);
      res !== undefined && (serialized = res);
    }
    return serialized;
  }

  /**
   * Runs all simple deserialisers for a value of a simple schema.
   * @param value Value on which to run simple deserialisers.
   * @param schema Schema of value on which to run deserialisers.
   * @param path Path of value on which to run deserialisers.
   * @returns Deserialised value.
   */
  private runSimpleDeserializers(
    value: any,
    schema: Schema & LfJsonSerializerSchemaProperties,
    path: string
  ) {
    let deserialised = value;
    for (const t of [
      schema.jsonSimpleTransform,
      ...arrayfy(this.simpleTransform).reverse(),
    ]) {
      const res =
        t && t.deserialize && t.deserialize(deserialised, schema, path);
      res !== undefined && (deserialised = res);
    }
    return deserialised;
  }

  /**
   * Runs all serialisers for a JSON value of a given schema.
   * @param value JSON value on which to run serialisers.
   * @param schema Schema of value on which to run serialisers.
   * @param path Path of value on which to run serialisers.
   * @returns Serialised JSON value.
   */
  private runSerializers(
    json: any,
    schema: Schema & LfJsonSerializerSchemaProperties,
    path: string
  ) {
    for (const t of [...arrayfy(this.transform), schema.jsonTransform]) {
      const serializer = t && t.serialize;
      serializer && (json = serializer(json, schema, path));
    }
    return json;
  }

  /**
   * Runs all deserialisers for a JSON value of a given schema.
   * @param value JSON value on which to run deserialisers.
   * @param schema Schema of value on which to run deserialisers.
   * @param path Path of value on which to run deserialisers.
   * @returns Deserialised JSON value.
   */
  private runDeserializers(
    json: any,
    schema: Schema & LfJsonSerializerSchemaProperties,
    path: string
  ) {
    for (const t of [
      schema.jsonTransform,
      ...arrayfy(this.transform).reverse(),
    ]) {
      const deserializer = t && t.deserialize;
      deserializer && (json = deserializer(json, schema, path));
    }
    return json;
  }
}
