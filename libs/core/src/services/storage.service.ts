import { Inject, Injectable, InjectionToken, Optional } from '@angular/core';
import {
  IS_REJECTED_CODE,
  Schema,
  Storage,
  ValidationIssues,
  ValidationIssuesMap,
  ValidationMode,
} from '@lightweightform/storage';
import { action, makeObservable } from 'mobx';
import { PENDING } from 'mobx-utils';

/**
 * Injection token used to specify the schema of an LF application.
 */
export const LF_APP_SCHEMA = new InjectionToken<Schema>('LF_APP_SCHEMA');

/**
 * Injection token used to specify the initial validation mode of the
 * `LfStorage`.
 */
export const LF_APP_INITIAL_VALIDATION_MODE =
  new InjectionToken<ValidationMode>('LF_INITIAL_VALIDATION_MODE');

/**
 * Injectable storage to be used as the LF application's storage. It extends the
 * base storage class by providing utility methods to manage the "touched",
 * "untouched", "dirty", and "pristine" state properties of values as well as
 * knowing when to show values as [valid|invalid|with warnings|pending|needing
 * recomputation of their validations] based on such state properties.
 */
@Injectable()
export class LfStorage extends Storage {
  constructor(
    @Inject(LF_APP_SCHEMA) schema: Schema,
    @Optional()
    @Inject(LF_APP_INITIAL_VALIDATION_MODE)
    validationMode: ValidationMode | null
  ) {
    super(schema, validationMode ?? ValidationMode.AUTO);
    makeObservable(this);
  }

  // Correct return type
  public relativeStorage(
    relativePath: string,
    readOnly: boolean = false
  ): LfStorage {
    return super.relativeStorage(relativePath, readOnly) as LfStorage;
  }

  /**
   * Returns validation errors for the value at the given path (validation
   * issues without `isWarning` set). If the validation is asynchronous and
   * still pending, `undefined` is returned.
   * @param relativePath Path of the value from which to fetch the validation
   * errors (relative to the current working path). Defaults to `'.'`.
   * @returns Validation errors associated with the given path or `undefined`
   * when the validation is in pending state.
   */
  public validationErrors(
    relativePath: string = '.'
  ): ValidationIssuesMap | undefined {
    const issuesMap = this.validationIssues(relativePath);
    return (
      issuesMap &&
      Object.keys(issuesMap).reduce((errorsMap, path) => {
        const errors = issuesMap[path].filter((issue) => !issue.isWarning);
        if (errors.length > 0) {
          errorsMap[path] = errors;
        }
        return errorsMap;
      }, {})
    );
  }

  /**
   * Returns local validation errors for the value at the given path (validation
   * issues without `isWarning` set that belong to the value at the given path
   * and not to one of its children). If the validation is asynchronous and
   * still pending, `undefined` is returned.
   * @param relativePath Path of the value from which to fetch the local
   * validation errors (relative to the current working path). Defaults to
   * `'.'`.
   * @returns Validation errors local to the given path (ignoring children
   * errors) or `undefined` when the validation is in pending state.
   */
  public localValidationErrors(
    relativePath: string = '.'
  ): ValidationIssues | undefined {
    const errorsMap = this.validationErrors(relativePath);
    return errorsMap && (errorsMap[this.resolvePath(relativePath)] || []);
  }

  /**
   * Whether the value at the given path has at least one validation error (a
   * validation issue without `isWarning` set).
   * @param relativePath Path on which to check if the value contains a
   * validation error (relative to the current working path). Defaults to `'.'`.
   * @returns Whether the value at the given path contains at least one
   * validation error.
   */
  public hasErrors(relativePath: string = '.'): boolean | undefined {
    const errorsMap = this.validationErrors(relativePath);
    return errorsMap && Object.keys(errorsMap).length > 0;
  }

  /**
   * Returns validation warnings for the value at the given path (validation
   * issues with `isWarning` set). If the validation is asynchronous and still
   * pending, `undefined` is returned.
   * @param relativePath Path of the value from which to fetch the validation
   * warnings (relative to the current working path). Defaults to `'.'`.
   * @returns Validation warnings associated with the given path or `undefined`
   * when the validation is in pending state.
   */
  public validationWarnings(
    relativePath: string = '.'
  ): ValidationIssuesMap | undefined {
    const issuesMap = this.validationIssues(relativePath);
    return (
      issuesMap &&
      Object.keys(issuesMap).reduce((warningsMap, path) => {
        const warnings = issuesMap[path].filter((issue) => issue.isWarning);
        if (warnings.length > 0) {
          warningsMap[path] = warnings;
        }
        return warningsMap;
      }, {})
    );
  }

  /**
   * Returns local validation warnings for the value at the given path
   * (validation issues with `isWarning` set that belong to the value at the
   * given path and not to one of its children). If the validation is
   * asynchronous and still pending, `undefined` is returned.
   * @param relativePath Path of the value from which to fetch the local
   * validation warnings (relative to the current working path). Defaults to
   * `'.'`.
   * @returns Validation warnings local to the given path (ignoring children
   * warnings) or `undefined` when the validation is in pending state.
   */
  public localValidationWarnings(
    relativePath: string = '.'
  ): ValidationIssues | undefined {
    const warningsMap = this.validationWarnings(relativePath);
    return warningsMap && (warningsMap[this.resolvePath(relativePath)] || []);
  }

  /**
   * Whether the value at the given path has at least one validation warning (a
   * validation issue with `isWarning` set).
   * @param relativePath Path on which to check if the value contains a
   * validation warning (relative to the current working path). Defaults to
   * `'.'`.
   * @returns Whether the value at the given path contains at least one
   * validation warning.
   */
  public hasWarnings(relativePath: string = '.'): boolean | undefined {
    const warningsMap = this.validationWarnings(relativePath);
    return warningsMap && Object.keys(warningsMap).length > 0;
  }

  /**
   * Whether a given value has been "touched" (somehow unfocused by the user).
   * @param relativePath Path on which to check if the value has been touched
   * (relative to the current working path). Defaults to `'.'`.
   * @returns Whether the value at the given path has been touched.
   */
  public isTouched(relativePath: string = '.'): boolean {
    return !!this.getStateProperty(relativePath, 'isTouched');
  }

  /**
   * Whether a given value is "untouched" (hasn't been unfocused by the user).
   * @param relativePath Path on which to check if the value is untouched
   * (relative to the current working path). Defaults to `'.'`.
   * @returns Whether the value at the given path is untouched.
   */
  public isUntouched(relativePath: string = '.'): boolean {
    return !this.isTouched(relativePath);
  }

  /**
   * Whether a given value is "dirty" (if it has somehow been changed by user
   * interaction).
   * @param relativePath Path on which to check if the value is dirty (relative
   * to the current working path). Defaults to `'.'`.
   * @returns Whether the value at the given path is dirty.
   */
  public isDirty(relativePath: string = '.'): boolean {
    return !!this.getStateProperty(relativePath, 'isDirty');
  }

  /**
   * Whether a given value is "pristine" (if it hasn't been changed by user
   * interaction).
   * @param relativePath Path on which to check if the value is pristine
   * (relative to the current working path). Defaults to `'.'`.
   * @returns Whether the value at the given path is pristine.
   */
  public isPristine(relativePath: string = '.'): boolean {
    return !this.isDirty(relativePath);
  }

  /**
   * Set the value at the given path and all of its ancestors as "touched".
   * Possibly cascades the "touched" state to all descendants.
   * @param relativePath Path on which to set the value as touched (relative to
   * the current working path). Defaults to `'.'`.
   * @param cascade Whether to cascade the touched state to all descendants of
   * the value.
   */
  @action
  public setTouched(
    relativePath: string = '.',
    cascade: boolean = false
  ): void {
    this.setStateProperty(relativePath, 'isTouched', true, {
      propagateUp: true,
      propagateDown: cascade,
      setOnComputed: false,
      stopPropagateUpOnEqual: true,
    });
  }

  /**
   * Set the value at the given path and all of its descendants as "untouched".
   * @param relativePath Path on which to set the value as untouched (relative
   * to the current working path). Defaults to `'.'`.
   */
  @action
  public setUntouched(relativePath: string = '.'): void {
    this.setStateProperty(relativePath, 'isTouched', false, {
      propagateDown: true,
      setOnComputed: false,
    });
  }

  /**
   * Set the value at the given path and all of its ancestors as "dirty".
   * Possibly cascades the "dirty" state to all descendants.
   * @param relativePath Path on which to set the value as dirty (relative to
   * the current working path). Defaults to `'.'`.
   * @param cascade Whether to cascade the dirty state to all descendants of the
   * value.
   */
  @action
  public setDirty(relativePath: string = '.', cascade: boolean = false): void {
    this.setStateProperty(relativePath, 'isDirty', true, {
      propagateUp: true,
      propagateDown: cascade,
      setOnComputed: false,
      stopPropagateUpOnEqual: true,
    });
  }

  /**
   * Set the value at the given path and all of its descendants as "pristine".
   * @param relativePath Path on which to set the value as pristine (relative to
   * the current working path). Defaults to `'.'`.
   */
  @action
  public setPristine(relativePath: string = '.'): void {
    this.setStateProperty(relativePath, 'isDirty', false, {
      propagateDown: true,
      setOnComputed: false,
    });
  }

  /**
   * Whether a given value should show as valid: that is, if its validation
   * issues are still being computed or if all of its issues come from
   * non-computed "untouched" values.
   * @param relativePath Path on which to check if the value should show as
   * valid (relative to the current working path). Defaults to `'.'`.
   * @returns Whether the value should show as valid.
   */
  public shouldShowValid(relativePath: string = '.'): boolean {
    const issuesMap = this.validationIssues(relativePath);
    const manualValidations = this.validationMode === ValidationMode.MANUAL;
    return (
      issuesMap === undefined ||
      Object.keys(issuesMap).every(
        (path) =>
          (manualValidations && !this.has(path)) ||
          (!this.isComputed(path) && this.isUntouched(path))
      )
    );
  }

  /**
   * Whether a given value should show as having an error: that is, if its
   * validations issues have been computed and it has at least one issue that
   * isn't a warning and comes from a computed or "touched" value.
   * @param relativePath Path on which to check if the value should show as
   * having an error (relative to the current working path). Defaults to `'.'`.
   * @returns Whether the value should show as having an error.
   */
  public shouldShowError(relativePath: string = '.'): boolean {
    const issuesMap = this.validationIssues(relativePath);
    const autoValidations = this.validationMode === ValidationMode.AUTO;
    return (
      issuesMap !== undefined &&
      Object.keys(issuesMap).some(
        (path) =>
          (autoValidations || this.has(path)) &&
          (this.isComputed(path) || this.isTouched(path)) &&
          issuesMap[path].some((issue) => !issue.isWarning)
      )
    );
  }

  /**
   * Whether a given value should show as having a warning: that is, if its
   * validations issues have been computed and it has issues coming from
   * computed or "touched" values, all of them being warnings.
   * @param relativePath Path on which to check if the value should show as
   * having a warning (relative to the current working path). Defaults to `'.'`.
   * @returns Whether the value should show as having a warning.
   */
  public shouldShowWarning(relativePath: string = '.'): boolean {
    const issuesMap = this.validationIssues(relativePath);
    const autoValidations = this.validationMode === ValidationMode.AUTO;
    const relevantIssuesPaths =
      issuesMap &&
      Object.keys(issuesMap).filter(
        (path) =>
          (autoValidations || this.has(path)) &&
          (this.isComputed(path) || this.isTouched(path))
      );
    return (
      issuesMap !== undefined &&
      relevantIssuesPaths!.length > 0 &&
      relevantIssuesPaths!.every((path) =>
        issuesMap[path].every((issue) => issue.isWarning)
      )
    );
  }

  /**
   * Whether a given value should show indication that its validations are being
   * computed: that is, if its validation issues are still being computed and
   * the value is computed or has been "touched".
   * @param relativePath Path on which to check if the value should show
   * indication that its validations are being computed. Defaults to `'.'`.
   * @returns Whether the value should show indication that its validations are
   * being computed.
   */
  public shouldShowPending(relativePath: string = '.'): boolean {
    return (
      (this.validationMode === ValidationMode.AUTO || this.has(relativePath)) &&
      (this.isComputed(relativePath) || this.isTouched(relativePath)) &&
      this.validationStatus(relativePath) === PENDING
    );
  }

  /**
   * Whether a given value should show indication that its validation has been
   * rejected: that is, if its validations issues have been computed and it has
   * issues caused by rejected promises coming from computed or "touched"
   * values.
   * @param relativePath Path on which to check if the value should show
   * indication that its validation needs to be recomputed (relative to the
   * current working path). Defaults to `'.'`.
   * @returns Whether the value should show indication that its validation needs
   * to be recomputed.
   */
  public shouldShowRejected(relativePath: string = '.'): boolean {
    const issuesMap = this.validationIssues(relativePath);
    const autoValidations = this.validationMode === ValidationMode.AUTO;
    return (
      issuesMap !== undefined &&
      Object.keys(issuesMap).some(
        (path) =>
          (autoValidations || this.has(path)) &&
          (this.isComputed(path) || this.isTouched(path)) &&
          issuesMap[path].some((issue) => issue.code === IS_REJECTED_CODE)
      )
    );
  }
}
