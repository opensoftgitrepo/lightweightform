import { Inject, Injectable, InjectionToken, Optional } from '@angular/core';
import { PATH_ID_PLACEHOLDER } from '@lightweightform/storage';
import {
  action,
  computed,
  isObservable,
  keys,
  makeObservable,
  observable,
  set,
} from 'mobx';
import { FULFILLED, PromiseState } from 'mobx-utils';

import { LfRouter } from './router.service';
import { LfStorage } from './storage.service';

/**
 * Options used for fetching a translation.
 */
export interface GetTranslationOptions {
  /**
   * Whether functions in translations should be resolved (by calling them with
   * a storage instance with the path relative to the translation's path).
   * Defaults to `true`.
   */
  callFunctionWithContext?: boolean;
  /**
   * Whether to cache the translation as a state property of the path in
   * question. `callFunctionWithContext` is assumed `true` if this setting is
   * set. This setting makes it possible to use asynchronous functions as
   * translations. The first time the translation is fetched, a state property
   * is created in the storage: as such, a MobX `action` is performed, meaning
   * that fetching translations with this setting set to `true` should not be
   * done from within a MobX computed value.
   */
  useState?: boolean;
}

/**
 * Map of i18n languages to their map of paths.
 */
export interface I18nLanguages {
  [lang: string]: I18nPaths;
}

/**
 * Map of paths to their map of translations.
 */
export interface I18nPaths {
  [path: string]: Record<string, any>;
}

/**
 * Injection token used to specify the initial i18n object.
 */
export const LF_APP_I18N: InjectionToken<I18nLanguages> =
  new InjectionToken<I18nLanguages>('LF_APP_I18N');

/**
 * Injection token used to specify the name of the URL query parameter with
 * which to manage the i18n service's current language. If none is provided,
 * then the i18n service won't control the current language via query parameter.
 */
export const LF_I18N_LANGUAGE_QUERY_PARAM = new InjectionToken<string>(
  'LF_I18N_LANGUAGE_QUERY_PARAM'
);

/**
 * Separator used in between translation record properties to form a unique key.
 * E.g. if the translation record has something like: `{a: {b: 'string'}}` then
 * we can use `'a.b'` to access the respective translation.
 */
export const I18N_KEY_SEPARATOR = '.';

/**
 * Translations can be specified by "path"; however, the `'*'` pseudo-path works
 * as a "catch-all" when no path should be associated or if the translation
 * should be available to all paths.
 */
export const I18N_CATCH_ALL_PATH = '*';

/**
 * Prefix used in state properties to hold i18n strings.
 */
export const I18N_STATE_PROPERTY_PREFIX = 'i18n';

/**
 * Service used to deal with the internationalisation of applications.
 * Translations may be added dynamically and be associated with paths if
 * desired.
 */
@Injectable()
export class LfI18n {
  @observable private _defaultLanguage: string;
  /**
   * Used only when `lfLanguageQueryParam` is `null`.
   */
  @observable private _currentLanguage?: string;
  @observable private translations: Record<string, any> = {};
  /**
   * Map generic paths found in translations to a RegExp that matches them
   * against given paths. E.g. the generic path `/a/?` should match against
   * `/a/b` or `a/c`.
   */
  @observable.shallow private genericPaths: Map<string, RegExp> = new Map();

  constructor(
    private lfStorage: LfStorage,
    @Optional()
    @Inject(LF_APP_I18N)
    translations: I18nLanguages | null,
    @Optional()
    @Inject(LfRouter)
    private lfRouter: LfRouter | null,
    @Optional()
    @Inject(LF_I18N_LANGUAGE_QUERY_PARAM)
    private lfLanguageQueryParam: string | null
  ) {
    makeObservable(this);

    if (translations !== null) {
      this.addTranslations(translations);
    }
  }

  /**
   * Static method used to merge translation objects. Merges all translations
   * into the first one.
   * @param translations Translations to merge into the first object. A minimum
   * of one object is required.
   * @returns Merged translations.
   */
  public static mergeTranslations(
    ...translations: Array<Record<string, any>>
  ): Record<string, any> {
    if (translations.length === 0) {
      throw new Error('At least 1 translation object must be provided');
    }
    const obj = translations[0];
    for (let i = 1; i < translations.length; ++i) {
      LfI18n._mergeTranslations(obj, translations[i]);
    }
    return obj;
  }

  private static _mergeTranslations(t1: any, t2: any): any {
    // Merge objects (merge `t2` into `t1`)
    if (
      t1 !== null &&
      t2 !== null &&
      typeof t1 === 'object' &&
      typeof t2 === 'object'
    ) {
      const t2Keys = Object.keys(t2);
      for (const key of [...Object.keys(t1), ...t2Keys]) {
        // FIXME: Should allow key separators in paths
        if (key.indexOf(I18N_KEY_SEPARATOR) !== -1) {
          throw new Error(
            `At key "${key}": i18n translation keys cannot contain ` +
              `"${I18N_KEY_SEPARATOR}"`
          );
        }
      }
      for (const key of t2Keys) {
        if (isObservable(t1)) {
          set(t1, key, LfI18n._mergeTranslations(t1[key], t2[key]));
        } else {
          t1[key] = LfI18n._mergeTranslations(t1[key], t2[key]);
        }
      }
      return t1;
    }
    // "Merge" other types (return `t2` unless `undefined`)
    return t2 === undefined ? t1 : t2;
  }

  /**
   * Available languages.
   */
  @computed
  public get languages(): string[] {
    return keys(this.translations) as string[];
  }

  /**
   * Default language from which to fetch translations when one wasn't found in
   * the current language.
   */
  @computed
  public get defaultLanguage(): string {
    return this._defaultLanguage;
  }

  public set defaultLanguage(lang: string) {
    this._defaultLanguage = lang;
  }

  /**
   * Language being currently used for translations.
   */
  @computed
  public get currentLanguage(): string {
    const currentLang =
      this.lfRouter === null || this.lfLanguageQueryParam === null
        ? this._currentLanguage
        : this.lfRouter.queryParams[this.lfLanguageQueryParam];
    return currentLang != null ? currentLang : this.defaultLanguage;
  }

  // FIXME: See: https://github.com/Microsoft/TypeScript/issues/2521 (getters
  // and setters unable to have different types), we want to allow setting
  // `undefined`, whilst the getter should only return strings. For now, one
  // must set the `currentLanguage` to `undefined` using `undefined as any` (or
  // using `setCurrentLanguage`).
  public set currentLanguage(lang: string) {
    this.setCurrentLanguage(lang);
  }

  /**
   * Sets the current language, possibly asynchronously when using a query
   * parameter to keep track of the current language.
   * @param lang Language to set as current language. `undefined` means that the
   * i18n service will use the default language.
   * @returns Promise that resolves to `true`/`false` when the navigation
   * succeeds/fails or is rejected when an error happens (when using a query
   * parameter to keep track of the current language, otherwise this method
   * returns `undefined`).
   */
  @action
  public setCurrentLanguage(lang?: string): void | Promise<boolean> {
    if (this.lfRouter === null || this.lfLanguageQueryParam === null) {
      this._currentLanguage = lang;
    } else {
      return this.lfRouter.setQueryParams(
        {
          [this.lfLanguageQueryParam]:
            lang === this.defaultLanguage || lang == null ? null : lang,
        },
        'merge'
      );
    }
  }

  /**
   * Gets a given translation from the "catch-all" path (tries the current
   * language, if no translation was found for the current language, tries the
   * default language).
   * @param key Key of the translation to fetch.
   * @param options Options for fetching the translation.
   * @returns Translation for provided key (typically a string, but can
   * theoretically be anything).
   */
  public get(
    key: string,
    {
      callFunctionWithContext = true,
      useState = false,
    }: GetTranslationOptions = {}
  ): any {
    return useState
      ? this.getFromState(I18N_CATCH_ALL_PATH, key)
      : this.getFromTranslations(
          I18N_CATCH_ALL_PATH,
          key,
          callFunctionWithContext
        );
  }

  /**
   * Gets a given translation for a given path (tries the provided path with the
   * current language, if no translation was found then tries the "catch-all"
   * path for the current language; if a translation still hasn't been found the
   * process is repeated for the default language).
   * @param path Path from which to fetch translation.
   * @param key Key of the translation to fetch.
   * @param options Options for fetching the translation.
   * @returns Translation for provided key (typically a string or a function,
   * but can theoretically be anything).
   */
  public getFromPath(
    path: string,
    key: string,
    {
      callFunctionWithContext = true,
      useState = false,
    }: GetTranslationOptions = {}
  ): any {
    if (process.env.NODE_ENV !== 'production') {
      this.lfStorage.validatePath(path);
    }
    return useState
      ? this.getFromState(path, key)
      : this.getFromTranslations(path, key, callFunctionWithContext);
  }

  /**
   * Status of a translation when it is an asynchronous context function and
   * `useState` was used to fetch it, or `'fulfilled'` in all other cases.
   * @param path Path from which to fetch the translation status.
   * @param key Key of the translation from which to fetch the status.
   * @returns The status of the translation when possible, `'fulfilled'` in all
   * other cases.
   */
  public translationStatus(path: string, key: string): PromiseState {
    const storagePath = path === I18N_CATCH_ALL_PATH ? '/' : path;
    if (this.lfStorage.has(storagePath)) {
      return this.lfStorage.statePropertyStatus(
        storagePath,
        this.statePropName(path, key)
      );
    }
    return FULFILLED;
  }

  /**
   * Adds translations to the application. Translations may be added for
   * multiple languages at once or for a single language.
   * @param translations Translations to add to the application. If `lang` is
   * defined then its translations should be an object mapping paths to
   * translation keys:
   * ```typescript
   * {
   *   '*': {
   *     randomKey: 'Some translation'
   *   },
   *   '/some/path': {
   *     anotherKey: 'Another translation'
   *   }
   * }
   * ```
   * Otherwise (if `lang` is not defined), the object should map languages to
   * objects similar to the example:
   * ```typescript
   * {
   *   'en-US': {
   *     '*': {
   *       randomKey: 'Some translation'
   *     },
   *     '/some/path': {
   *       anotherKey: 'Another translation'
   *     }
   *   },
   *   'pt-PT': {
   *     '*': {
   *       randomKey: 'Alguma tradução'
   *     }
   *   }
   * }
   * ```
   * @param lang Language of the translation object if the object only contains
   * translations for a single language.
   */
  @action
  public addTranslations(
    translations: Record<string, any>,
    lang?: string
  ): void {
    if (lang !== undefined) {
      translations = { [lang]: translations };
    }
    const langs = Object.keys(translations);
    for (const curLang of langs) {
      this.addGenericPaths(Object.keys(translations[curLang]));
    }
    if (!this.defaultLanguage && langs.length > 0) {
      this.defaultLanguage = langs[0];
    }
    LfI18n.mergeTranslations(this.translations, translations);
  }

  @action
  private addGenericPaths(paths: string[]): void {
    // From: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions#special-unicode-escape-es6
    function escapeRegExp(str: string): string {
      return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
    }
    for (const path of paths) {
      if (!this.genericPaths.has(path)) {
        const parts = path.split('/');
        const isGeneric = parts.some((part) => part === PATH_ID_PLACEHOLDER);
        if (isGeneric) {
          const regExpParts = parts.map((part) =>
            part === PATH_ID_PLACEHOLDER ? '[^/]+' : escapeRegExp(part)
          );
          this.genericPaths.set(path, new RegExp(`^${regExpParts.join('/')}$`));
        }
      }
    }
  }

  private getFromState(path: string, key: string): any {
    const storagePath = path === I18N_CATCH_ALL_PATH ? '/' : path;
    if (this.lfStorage.has(storagePath)) {
      const statePropName = this.statePropName(path, key);
      if (!this.lfStorage.hasStateProperty(storagePath, statePropName)) {
        this.lfStorage.setStateProperty(storagePath, statePropName, (ctx) => {
          const value = this.getFromTranslations(path, key, false);
          return typeof value === 'function' ? value(ctx, this) : value;
        });
      }
      return this.lfStorage.getStateProperty(storagePath, statePropName);
    }
  }

  private getFromTranslations(
    path: string,
    key: string,
    callFunctionWithContext: boolean
  ): any {
    if (this.currentLanguage === undefined) {
      return;
    }
    // Languages from which to try to fetch a translation (current and default)
    const langs = [this.currentLanguage];
    if (this.currentLanguage !== this.defaultLanguage) {
      langs.push(this.defaultLanguage);
    }
    // Paths from which to try to fetch a translation
    const paths = [path];
    if (path !== I18N_CATCH_ALL_PATH) {
      paths.push(I18N_CATCH_ALL_PATH);
    }
    // Properties from which to try to fetch a translation
    const keyProps = key.split(I18N_KEY_SEPARATOR);
    for (const curLang of langs) {
      const langTranslations = this.translations[curLang];
      if (langTranslations === undefined) {
        continue;
      }
      for (const curPath of paths) {
        // Current path + generic paths which match the current path
        const possiblePaths =
          curPath === I18N_CATCH_ALL_PATH
            ? [curPath]
            : [
                curPath,
                ...Array.from(this.genericPaths.keys()).filter((genericPath) =>
                  this.genericPaths.get(genericPath)!.test(curPath)
                ),
              ];
        for (const curPossiblePath of possiblePaths) {
          const pathTranslations = langTranslations[curPossiblePath];
          if (pathTranslations === undefined) {
            continue;
          }
          let value = pathTranslations;
          for (const prop of keyProps) {
            value = value[prop];
            if (value === undefined) {
              break;
            }
          }
          if (value !== undefined) {
            return callFunctionWithContext && typeof value === 'function'
              ? value(
                  this.lfStorage.relativeStorage(
                    path === I18N_CATCH_ALL_PATH ? '/' : path
                  ),
                  this
                )
              : value;
          }
        }
      }
    }
  }

  private statePropName(path: string, key: string): string {
    // The state property name should have the path present in it when the
    // parent value is computed (since children share the parent state) or when
    // the path is the "catch-all", since the state is then shared within the
    // "root" value
    return path === I18N_CATCH_ALL_PATH ||
      this.lfStorage.isComputed(`${path}/..`)
      ? `${I18N_STATE_PROPERTY_PREFIX}(${key})[${path}]`
      : `${I18N_STATE_PROPERTY_PREFIX}(${key})`;
  }
}
