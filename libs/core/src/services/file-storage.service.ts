import { Inject, Injectable, InjectionToken, Optional } from '@angular/core';
import {
  MergeValueOptions,
  Schema,
  TypeValidationOptions,
} from '@lightweightform/storage';
import { computed, makeObservable, observable } from 'mobx';

import { LfSerializer } from '../abstract/value-serializer';
import { LoadTypeError } from '../utils/load-type-error';

import { FileSystemService } from './file-system.service';
import { LfStorage } from './storage.service';

/**
 * Interface of a class that can be used to intercept the loading of a file into
 * the storage.
 */
export interface LfFileStorageLoadInterceptor {
  /**
   * When an interceptor is provided, this method is called after the content of
   * a file has been deserialised but before the value is typechecked or
   * inserted into the storage.
   *
   * Among other uses, it can be used to check that the value to be loaded isn't
   * "too big" (since inserting a value into the storage may cause JavaScript's
   * heap size to explode due to it being converted into a MobX value), for
   * instance, in which case a warning or error may be raised.
   *
   * If a value other than `undefined` is returned, the returned value is the
   * one that will be typechecked and inserted into the storage.
   * @param deserializedValue File content after being deserialised and before
   * being inserted into the storage.
   * @param schema Schema of the deserialised value.
   * @param path Path of the deserialised value.
   * @returns The value with which to proceed the loading or `undefined` to
   * proceed with `deserializedValue`.
   */
  beforeLoad(deserializedValue: any, schema: Schema, path: string): any;
}

/**
 * Injection token used to add a "load interceptor" that runs before a value is
 * loaded.
 */
export const LF_FILE_STORAGE_LOAD_INTERCEPTOR: InjectionToken<LfFileStorageLoadInterceptor> =
  new InjectionToken<LfFileStorageLoadInterceptor>(
    'LF_FILE_STORAGE_LOAD_INTERCEPTOR'
  );

/**
 * Service used to save/load storage values to/from files in the user's file
 * system. Using this service requires providing an LF serialiser.
 */
@Injectable()
export class LfFileStorage {
  /**
   * Whether a load operation is currently happening.
   */
  @observable private _isLoading = false;

  constructor(
    protected lfStorage: LfStorage,
    protected fileSystemService: FileSystemService,
    protected lfSerializer: LfSerializer,
    @Optional()
    @Inject(LF_FILE_STORAGE_LOAD_INTERCEPTOR)
    protected loadInterceptor: LfFileStorageLoadInterceptor
  ) {
    makeObservable(this);
  }

  /**
   * Whether the current browser supports loading from a file.
   */
  // Property added so that users don't have to interact directly with the
  // `FileSystemService`.
  public get loadIsSupported(): boolean {
    return this.fileSystemService.loadIsSupported;
  }

  /**
   * Whether a load operation is currently happening.
   */
  @computed
  public get isLoading(): boolean {
    return this._isLoading;
  }

  /**
   * Saves the value at the given path to a file in the user's file system given
   * the name of the file to save (minus extension).
   * @param path Storage path of the value to save. Defaults to `'/'`.
   * @param fileName Name of file to save (minus extension, which is dictated by
   * the serialiser). Defaults to `'app'` when saving the root value or `path`
   * (with `'/'` replaced by `'_'`) when saving another value.
   * @returns A promise which resolves once the file has been saved.
   */
  public saveToFile(path: string = '/', fileName?: string): Promise<void> {
    if (fileName === undefined) {
      fileName =
        path === '/' ? 'app' : path.replace(/^\//, '').replace('/', '_');
    }
    const serializedValue = this.lfSerializer.serialize(
      this.lfStorage.getAsJS(path),
      this.lfStorage.schema(path),
      path
    );
    return this.fileSystemService.save(
      this.lfSerializer.encode
        ? this.lfSerializer.encode(serializedValue)
        : [serializedValue],
      `${fileName}.${this.lfSerializer.extension}`,
      {
        charset: this.lfSerializer.charset,
        mimeType: this.lfSerializer.mimeType,
        autoBOM: this.lfSerializer.autoBOM,
      }
    );
  }

  /**
   * Loads a value from a file in the user's file system and sets it as the
   * value in the provided path.
   * @param path Storage path on which to set the value from the file. Defaults
   * to `'/'`.
   * @param mergeValueOptions Options used for setting the value in the storage.
   * @param typeValidationOptions Options used when validating the type of the
   * value.
   * @returns A promise which resolves with whether a value was loaded (`false`
   * when the user cancels the action).
   */
  public async loadFromFile(
    path: string = '/',
    mergeValueOptions?: MergeValueOptions,
    typeValidationOptions?: TypeValidationOptions
  ): Promise<boolean> {
    const fileContent = await this.fileSystemService.load({
      charset: this.lfSerializer.charset,
      accept: this.lfSerializer.accept,
    });
    if (fileContent == null) {
      return false;
    }
    this._isLoading = true;
    // XXX: Give time to do something with `isLoading` (e.g. show an icon)
    return await new Promise((res) => setTimeout(res, 0))
      .then(() => {
        const schema = this.lfStorage.schema(path);
        let value = this.lfSerializer.deserialize(fileContent, schema, path);

        if (this.loadInterceptor) {
          const interceptedValue = this.loadInterceptor.beforeLoad(
            value,
            schema,
            path
          );
          if (interceptedValue !== undefined) {
            value = interceptedValue;
          }
        }

        // Check that the type of the deserialised value is valid
        const issuesMap = {};
        if (
          !this.lfStorage.typeIsValid(
            path,
            value,
            issuesMap,
            typeValidationOptions
          )
        ) {
          throw new LoadTypeError('Type of loaded value is invalid', issuesMap);
        }

        this.lfStorage.set(path, value, mergeValueOptions);
        return true;
      })
      .finally(() => {
        this._isLoading = false;
      });
  }
}
