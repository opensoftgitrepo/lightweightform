import { Injectable, Optional } from '@angular/core';
import { action, computed, makeObservable, observable } from 'mobx';

import { LfI18n } from './i18n.service';
import { LfStorage } from './storage.service';

/**
 * I18n key used to set the unload alert message.
 */
export const I18N_UNLOAD_ALERT_MESSAGE_KEY = 'unloadAlertMessage';

/**
 * Service used to enable/disable a browser alert when the user reloads/leaves
 * the page. The user will be reminded that they might lose filled-in form data,
 * and they will be able to stay in the current page or confirm their leaving of
 * the page.
 */
@Injectable()
export class LfUnloadAlert {
  /**
   * Event handler function to run `beforeunload`. It makes the browser display
   * an alert when the application's value is dirty.
   */
  private handler: (evt: BeforeUnloadEvent) => string | undefined;

  /**
   * Variable that keeps track of the existence of an event listener for the
   * `beforeunload` event.
   */
  @observable
  private _isEnabled = false;

  constructor(lfStorage: LfStorage, @Optional() lfI18n: LfI18n) {
    makeObservable(this);
    this.handler = (evt: BeforeUnloadEvent) => {
      // Show alert only when the application's value is dirty
      if (lfStorage.isDirty('/')) {
        let message = '';
        if (lfI18n) {
          message = lfI18n.get(I18N_UNLOAD_ALERT_MESSAGE_KEY) || '';
        }
        evt.preventDefault();
        evt.returnValue = message;
        return message;
      }
    };
  }

  /**
   * Whether the browser alert for when the user reloads/leaves the page is
   * enabled.
   */
  @computed
  public get isEnabled() {
    return this._isEnabled;
  }

  /**
   * Enable a browser alert for when the user reloads/leaves the page. A custom
   * message may be passed, although most browsers will ignore it (in favour of
   * a default browser-specific message). Consecutive calls to this method
   * simply change the alert's message.
   * @param message Message to display on the alert (ignored by most browsers
   * except IE).
   */
  @action
  public enable(): void {
    if (!this._isEnabled) {
      window.addEventListener('beforeunload', this.handler, false);
      this._isEnabled = true;
    }
  }

  /**
   * Disable the browser alert for when the user reloads/leaves the page.
   * Consecutive calls to this method have no effect.
   */
  @action
  public disable(): void {
    if (this._isEnabled) {
      window.removeEventListener('beforeunload', this.handler, false);
      this._isEnabled = false;
    }
  }
}
