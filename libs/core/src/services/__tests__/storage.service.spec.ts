import { getTestBed, TestBed } from '@angular/core/testing';
import {
  booleanSchema,
  listSchema,
  mapSchema,
  numberSchema,
  recordSchema,
  stringSchema,
  tupleSchema,
} from '@lightweightform/storage';
import { when } from 'mobx';
import { PENDING } from 'mobx-utils';

import { LfStorage, LF_APP_SCHEMA } from '../storage.service';

// Schema used to test the LF storage
const appSchema = recordSchema({
  rec: recordSchema({
    bool: booleanSchema(),
    boolList: listSchema(booleanSchema(), { initialValue: [false] }),
    boolMap: mapSchema(booleanSchema(), {
      initialValue: new Map([['x', false]]),
    }),
    boolTup: tupleSchema([booleanSchema()]),
  }),
  num: numberSchema({ min: 5 }),
  strRec: recordSchema(
    {
      str: stringSchema({
        validate: [
          (ctx) => {
            if (ctx.get().indexOf('error') !== -1) {
              return { code: 'STR_ERROR' };
            }
          },
          (ctx) => {
            if (ctx.get().indexOf('warning') !== -1) {
              return { code: 'STR_WARNING', isWarning: true };
            }
          },
        ],
      }),
    },
    {
      validate: [
        (ctx) => {
          if (ctx.get('str').indexOf('parentError') !== -1) {
            return { code: 'CHILD_STR_ERROR' };
          }
        },
        (ctx) => {
          if (ctx.get('str').indexOf('parentWarning') !== -1) {
            return { code: 'CHILD_STR_WARNING', isWarning: true };
          }
        },
      ],
    }
  ),
  numComp: numberSchema({
    computedValue: (ctx) => ctx.get('num') + 1,
    max: 10,
  }),
  numAsync: numberSchema({
    max: (ctx) => {
      // Create dependency before the async action
      const numIsValid = !ctx.hasIssues('num');
      return new Promise((res, rej) =>
        setTimeout(() => (numIsValid ? res(3) : rej('Some error')))
      );
    },
  }),
});

describe('LF storage', () => {
  let lfStorage: LfStorage;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: LF_APP_SCHEMA, useValue: appSchema }, LfStorage],
    });
    lfStorage = getTestBed().inject(LfStorage);
  });

  it('creates relative storages that are instances of `LfStorage`', () => {
    expect(lfStorage.relativeStorage('num')).toBeInstanceOf(LfStorage);
  });

  it("detects validation errors (issues that aren't warnings)", () => {
    expect(lfStorage.localValidationErrors('strRec')!.length).toBe(0);
    expect(lfStorage.localValidationErrors('strRec/str')!.length).toBe(0);
    expect(lfStorage.hasErrors('strRec')).toBe(false);
    expect(lfStorage.hasErrors('strRec/str')).toBe(false);

    expect(lfStorage.set('strRec/str', 'error'));
    expect(lfStorage.localValidationErrors('strRec')!.length).toBe(0);
    expect(lfStorage.localValidationErrors('strRec/str')!.length).toBe(1);
    expect(lfStorage.hasErrors('strRec')).toBe(true);
    expect(lfStorage.hasErrors('strRec/str')).toBe(true);

    expect(lfStorage.set('strRec/str', 'warning'));
    expect(lfStorage.localValidationErrors('strRec')!.length).toBe(0);
    expect(lfStorage.localValidationErrors('strRec/str')!.length).toBe(0);
    expect(lfStorage.hasErrors('strRec')).toBe(false);
    expect(lfStorage.hasErrors('strRec/str')).toBe(false);

    expect(lfStorage.set('strRec/str', 'error warning'));
    expect(lfStorage.localValidationIssues('strRec')!.length).toBe(0);
    expect(lfStorage.localValidationIssues('strRec/str')!.length).toBe(2);
    expect(lfStorage.localValidationErrors('strRec')!.length).toBe(0);
    expect(lfStorage.localValidationErrors('strRec/str')!.length).toBe(1);
    expect(lfStorage.hasErrors('strRec')).toBe(true);
    expect(lfStorage.hasErrors('strRec/str')).toBe(true);

    expect(
      lfStorage.set('strRec/str', 'error warning parentError parentWarning')
    );
    expect(Object.keys(lfStorage.validationIssues('strRec')!).length).toBe(2);
    expect(Object.keys(lfStorage.validationErrors('strRec')!).length).toBe(2);
    expect(lfStorage.localValidationIssues('strRec')!.length).toBe(2);
    expect(lfStorage.localValidationIssues('strRec/str')!.length).toBe(2);
    expect(lfStorage.localValidationErrors('strRec')!.length).toBe(1);
    expect(lfStorage.localValidationErrors('strRec/str')!.length).toBe(1);
    expect(lfStorage.hasErrors('strRec')).toBe(true);
    expect(lfStorage.hasErrors('strRec/str')).toBe(true);
  });

  it('detects validation warnings', () => {
    expect(lfStorage.localValidationWarnings('strRec')!.length).toBe(0);
    expect(lfStorage.localValidationWarnings('strRec/str')!.length).toBe(0);
    expect(lfStorage.hasWarnings('strRec')).toBe(false);
    expect(lfStorage.hasWarnings('strRec/str')).toBe(false);

    expect(lfStorage.set('strRec/str', 'warning'));
    expect(lfStorage.localValidationWarnings('strRec')!.length).toBe(0);
    expect(lfStorage.localValidationWarnings('strRec/str')!.length).toBe(1);
    expect(lfStorage.hasWarnings('strRec')).toBe(true);
    expect(lfStorage.hasWarnings('strRec/str')).toBe(true);

    expect(lfStorage.set('strRec/str', 'error'));
    expect(lfStorage.localValidationWarnings('strRec')!.length).toBe(0);
    expect(lfStorage.localValidationWarnings('strRec/str')!.length).toBe(0);
    expect(lfStorage.hasWarnings('strRec')).toBe(false);
    expect(lfStorage.hasWarnings('strRec/str')).toBe(false);

    expect(lfStorage.set('strRec/str', 'error warning'));
    expect(lfStorage.localValidationIssues('strRec')!.length).toBe(0);
    expect(lfStorage.localValidationIssues('strRec/str')!.length).toBe(2);
    expect(lfStorage.localValidationWarnings('strRec')!.length).toBe(0);
    expect(lfStorage.localValidationWarnings('strRec/str')!.length).toBe(1);
    expect(lfStorage.hasWarnings('strRec')).toBe(true);
    expect(lfStorage.hasWarnings('strRec/str')).toBe(true);

    expect(
      lfStorage.set('strRec/str', 'error warning parentError parentWarning')
    );
    expect(Object.keys(lfStorage.validationIssues('strRec')!).length).toBe(2);
    expect(Object.keys(lfStorage.validationWarnings('strRec')!).length).toBe(2);
    expect(lfStorage.localValidationIssues('strRec')!.length).toBe(2);
    expect(lfStorage.localValidationIssues('strRec/str')!.length).toBe(2);
    expect(lfStorage.localValidationWarnings('strRec')!.length).toBe(1);
    expect(lfStorage.localValidationWarnings('strRec/str')!.length).toBe(1);
    expect(lfStorage.hasWarnings('strRec')).toBe(true);
    expect(lfStorage.hasWarnings('strRec/str')).toBe(true);
  });

  it('allows setting values as "touched"', () => {
    lfStorage.setTouched('rec/bool');
    expect(lfStorage.isTouched('rec/bool')).toBe(true);
    expect(lfStorage.isTouched('rec')).toBe(true);
    expect(lfStorage.isTouched('/')).toBe(true);

    // Doesn't affect unrelated values
    expect(lfStorage.isTouched('rec/boolList')).toBe(false);
    expect(lfStorage.isTouched('num')).toBe(false);

    // `isUntouched` is the opposite of `isTouched`
    expect(lfStorage.isUntouched('rec/bool')).toBe(false);
    expect(lfStorage.isUntouched('rec')).toBe(false);
    expect(lfStorage.isUntouched('/')).toBe(false);
    expect(lfStorage.isUntouched('rec/boolList')).toBe(true);
    expect(lfStorage.isUntouched('num')).toBe(true);
  });

  it('allows setting values as "touched" while cascading the setting', () => {
    lfStorage.setTouched('rec', true);
    expect(lfStorage.isTouched('rec')).toBe(true);
    expect(lfStorage.isTouched('/')).toBe(true);
    expect(lfStorage.isTouched('rec/boolList')).toBe(true);
    expect(lfStorage.isTouched('rec/boolList/0')).toBe(true);
    expect(lfStorage.isTouched('rec/boolMap')).toBe(true);
    expect(lfStorage.isTouched('rec/boolMap/x')).toBe(true);
    expect(lfStorage.isTouched('rec/boolTup')).toBe(true);
    expect(lfStorage.isTouched('rec/boolTup/0')).toBe(true);

    // New children aren't touched
    lfStorage.push('rec/boolList', false);
    lfStorage.set('rec/boolMap/y', false);
    expect(lfStorage.isUntouched('rec/boolList/1')).toBe(true);
    expect(lfStorage.isUntouched('rec/boolMap/y')).toBe(true);

    // Doesn't affect unrelated values
    expect(lfStorage.isUntouched('num')).toBe(true);
  });

  it('does not set computed values as touched', () => {
    lfStorage.setTouched('/', true);
    expect(lfStorage.isTouched('numComp')).toBe(false);
    lfStorage.setTouched('numComp');
    expect(lfStorage.isTouched('numComp')).toBe(false);
  });

  it('allows setting values as "untouched"', () => {
    lfStorage.setTouched('rec', true);
    lfStorage.setUntouched('rec');

    // `setUntouched` cascades down
    expect(lfStorage.isTouched('/')).toBe(true);
    expect(lfStorage.isUntouched('rec')).toBe(true);
    expect(lfStorage.isUntouched('rec/bool')).toBe(true);
    expect(lfStorage.isUntouched('rec/boolList')).toBe(true);
    expect(lfStorage.isUntouched('rec/boolList/0')).toBe(true);
    expect(lfStorage.isUntouched('rec/boolMap')).toBe(true);
    expect(lfStorage.isUntouched('rec/boolMap/x')).toBe(true);
    expect(lfStorage.isUntouched('rec/boolTup')).toBe(true);
    expect(lfStorage.isUntouched('rec/boolTup/0')).toBe(true);
  });

  it('allows setting values as "dirty"', () => {
    lfStorage.setDirty('rec/bool');
    expect(lfStorage.isDirty('rec/bool')).toBe(true);
    expect(lfStorage.isDirty('rec')).toBe(true);
    expect(lfStorage.isDirty('/')).toBe(true);

    // Doesn't affect unrelated values
    expect(lfStorage.isDirty('rec/boolList')).toBe(false);
    expect(lfStorage.isDirty('num')).toBe(false);

    // `isPristine` is the opposite of `isDirty`
    expect(lfStorage.isPristine('rec/bool')).toBe(false);
    expect(lfStorage.isPristine('rec')).toBe(false);
    expect(lfStorage.isPristine('/')).toBe(false);
    expect(lfStorage.isPristine('rec/boolList')).toBe(true);
    expect(lfStorage.isPristine('num')).toBe(true);
  });

  it('allows setting values as "dirty" while cascading the setting', () => {
    lfStorage.setDirty('rec', true);
    expect(lfStorage.isDirty('rec')).toBe(true);
    expect(lfStorage.isDirty('/')).toBe(true);
    expect(lfStorage.isDirty('rec/boolList')).toBe(true);
    expect(lfStorage.isDirty('rec/boolList/0')).toBe(true);
    expect(lfStorage.isDirty('rec/boolMap')).toBe(true);
    expect(lfStorage.isDirty('rec/boolMap/x')).toBe(true);
    expect(lfStorage.isDirty('rec/boolTup')).toBe(true);
    expect(lfStorage.isDirty('rec/boolTup/0')).toBe(true);

    // New children aren't dirty
    lfStorage.push('rec/boolList', false);
    lfStorage.set('rec/boolMap/y', false);
    expect(lfStorage.isPristine('rec/boolList/1')).toBe(true);
    expect(lfStorage.isPristine('rec/boolMap/y')).toBe(true);

    // Doesn't affect unrelated values
    expect(lfStorage.isPristine('num')).toBe(true);
  });

  it('does not set computed values as dirty', () => {
    lfStorage.setDirty('/', true);
    expect(lfStorage.isDirty('numComp')).toBe(false);
    lfStorage.setDirty('numComp');
    expect(lfStorage.isDirty('numComp')).toBe(false);
  });

  it('allows setting values as "pristine"', () => {
    lfStorage.setDirty('rec', true);
    lfStorage.setPristine('rec');

    // `setPristine` cascades down
    expect(lfStorage.isDirty('/')).toBe(true);
    expect(lfStorage.isPristine('rec')).toBe(true);
    expect(lfStorage.isPristine('rec/bool')).toBe(true);
    expect(lfStorage.isPristine('rec/boolList')).toBe(true);
    expect(lfStorage.isPristine('rec/boolList/0')).toBe(true);
    expect(lfStorage.isPristine('rec/boolMap')).toBe(true);
    expect(lfStorage.isPristine('rec/boolMap/x')).toBe(true);
    expect(lfStorage.isPristine('rec/boolTup')).toBe(true);
    expect(lfStorage.isPristine('rec/boolTup/0')).toBe(true);
  });

  it('determines when values should show as valid', () => {
    expect(lfStorage.shouldShowValid('num')).toBe(true);
    lfStorage.setTouched('num');
    expect(lfStorage.shouldShowValid('num')).toBe(false);
    lfStorage.set('num', 5);
    expect(lfStorage.shouldShowValid('num')).toBe(true);
    // `numComp` is computed, so the value should not display as valid even
    // though it is "untouched"
    lfStorage.set('num', 10);
    expect(lfStorage.hasIssues('numComp')).toBe(true);
    expect(lfStorage.shouldShowValid('numComp')).toBe(false);
  });

  it('determines when values should show with errors', () => {
    expect(lfStorage.shouldShowError('num')).toBe(false);
    lfStorage.setTouched('num');
    expect(lfStorage.shouldShowError('num')).toBe(true);
    lfStorage.set('num', 5);
    expect(lfStorage.shouldShowError('num')).toBe(false);
    // `numComp` is computed, so the value should show with error even though it
    // is "untouched"
    lfStorage.set('num', 10);
    expect(lfStorage.hasIssues('numComp')).toBe(true);
    expect(lfStorage.shouldShowError('numComp')).toBe(true);
    // When the only error is a warning, the value shouldn't show with error
    lfStorage.setTouched('strRec/str');
    lfStorage.set('strRec/str', 'warning');
    expect(lfStorage.shouldShowError('strRec/str')).toBe(false);
    // But if at least one error isn't a warning, the value should show with
    // error
    lfStorage.set('strRec/str', 'error warning');
    expect(lfStorage.shouldShowError('strRec/str')).toBe(true);
  });

  it('determines when values should show with warnings', () => {
    // When all errors are warnings, the value should show with warning
    lfStorage.setTouched('strRec/str');
    lfStorage.set('strRec/str', 'warning');
    expect(lfStorage.shouldShowWarning('strRec/str')).toBe(true);
    // However, as soon as a "real" error appears, the value shouldn't show with
    // warning
    lfStorage.set('strRec/str', 'error warning');
    expect(lfStorage.shouldShowWarning('strRec/str')).toBe(false);
  });

  it('determines when values should show as pending', () => {
    expect(lfStorage.shouldShowPending('numAsync')).toBe(false);
    lfStorage.setTouched('numAsync');
    expect(lfStorage.shouldShowPending('numAsync')).toBe(true);
    return new Promise<void>((res) => {
      when(
        () => lfStorage.validationStatus('numAsync') !== PENDING,
        () => {
          expect(lfStorage.shouldShowPending('numAsync')).toBe(false);
          res();
        }
      );
    });
  });

  it('determines when values should show as rejected', () => {
    return new Promise<void>((res) => {
      when(
        () => lfStorage.validationStatus('numAsync') !== PENDING,
        () => {
          expect(lfStorage.hasIssues('numAsync')).toBe(true);
          expect(lfStorage.shouldShowRejected('numAsync')).toBe(false);
          lfStorage.setTouched('numAsync');
          expect(lfStorage.shouldShowRejected('numAsync')).toBe(true);

          lfStorage.set('num', 5);
          when(
            () => lfStorage.validationStatus('numAsync') !== PENDING,
            () => {
              expect(lfStorage.hasIssues('numAsync')).toBe(false);
              expect(lfStorage.shouldShowRejected('numAsync')).toBe(false);
              res();
            }
          );
        }
      );
    });
  });
});
