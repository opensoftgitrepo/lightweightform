import { getTestBed, TestBed } from '@angular/core/testing';
import {
  booleanSchema,
  dateSchema,
  isBooleanSchema,
  isNumberSchema,
  listSchema,
  mapSchema,
  numberSchema,
  recordSchema,
  Schema,
  stringSchema,
  tableSchema,
  tupleSchema,
} from '@lightweightform/storage';

import { LfSerializer } from '../../abstract/value-serializer';
import {
  LfJsonSerializer,
  LF_JSON_SERIALIZER_OPTIONS,
} from '../json-serializer.service';
import { LfStorage, LF_APP_SCHEMA } from '../storage.service';

// Remove left padding from a string template
function unpad(str: string): string {
  const initialSpace = str.slice(1).match(/^\s*/)![0];
  return str.replace(new RegExp('^' + initialSpace, 'gm'), '').trim();
}

describe('JSON serialiser', () => {
  let lfStorage: LfStorage;
  let serializer: LfJsonSerializer;

  // Helper functions to set up the storage
  function setupWithSchema(schema: Schema): void {
    TestBed.configureTestingModule({
      providers: [
        { provide: LF_APP_SCHEMA, useValue: schema },
        { provide: LfSerializer, useClass: LfJsonSerializer },
        { provide: LF_JSON_SERIALIZER_OPTIONS, useValue: { space: 2 } },
        LfStorage,
      ],
    });
    lfStorage = getTestBed().inject(LfStorage);
    serializer = getTestBed().inject(LfSerializer) as LfJsonSerializer;
  }
  // Helper function to check an equivalence between a JS value and a JSON
  // string
  function expectToBeEquivalent(path: string, val: any, json: string) {
    const schema = lfStorage.schema(path);
    if (!lfStorage.isComputed(path)) {
      lfStorage.set(path, val);
    }
    expect(serializer.serialize(lfStorage.getAsJS(path), schema, path)).toBe(
      unpad(json)
    );
    expect(serializer.deserialize(json, schema, path)).toEqual(
      lfStorage.getAsJS(path)
    );
  }

  it('allows configuring the number of spaces', () => {
    setupWithSchema(recordSchema({ str: stringSchema() }));

    serializer.setOptions({ space: 4 });
    expectToBeEquivalent(
      '/',
      { str: 'abc' },
      `
      {
          "str": "abc"
      }
      `
    );

    serializer.setOptions({ space: '\t' });
    expectToBeEquivalent(
      '/',
      { str: 'abc' },
      `
      {
      \t"str": "abc"
      }
      `
    );

    serializer.setOptions({ space: 0 });
    expectToBeEquivalent('/', { str: 'abc' }, '{"str":"abc"}');
  });

  it('serialises dates as strings', () => {
    setupWithSchema(recordSchema({ dat: dateSchema() }));

    expectToBeEquivalent(
      '/',
      { dat: new Date(1) },
      `
      {
        "dat": "1970-01-01T00:00:00.001Z"
      }
      `
    );

    expectToBeEquivalent(
      '/dat',
      new Date(7),
      `
      "1970-01-01T00:00:00.007Z"
      `
    );
  });

  it('serialises maps as records', () => {
    setupWithSchema(recordSchema({ map: mapSchema(numberSchema()) }));

    expectToBeEquivalent(
      '/',
      {
        map: new Map([
          ['x', 1],
          ['y', 2],
        ]),
      },
      `
      {
        "map": {
          "x": 1,
          "y": 2
        }
      }
      `
    );

    expectToBeEquivalent('/map', new Map([]), '{}');
  });

  it('serialises tuples with client-side elements', () => {
    setupWithSchema(
      recordSchema({
        tup: tupleSchema([
          numberSchema({ computedValue: () => 99 }),
          numberSchema(),
        ]),
      })
    );

    expectToBeEquivalent(
      '/',
      { tup: [undefined, 10] },
      `
      {
        "tup": [
          null,
          10
        ]
      }
      `
    );

    expectToBeEquivalent('/tup/0', 99, '99');
  });

  it('serialises structure with all schema types', () => {
    setupWithSchema(
      recordSchema({
        bool: booleanSchema(),
        dat: dateSchema(),
        str: stringSchema(),
        rec: recordSchema({
          list: listSchema(numberSchema()),
          map: mapSchema(numberSchema()),
          tab: tableSchema(recordSchema({ num: numberSchema() })),
          tup: tupleSchema([numberSchema()]),
        }),
        nil: recordSchema({}, { isNullable: true }),
      })
    );

    expectToBeEquivalent(
      '/',
      {
        bool: true,
        dat: new Date(55),
        str: 'hi',
        rec: {
          list: [1, 2],
          map: new Map([['x', 3]]),
          tab: [{ num: 4 }, { num: 5 }],
          tup: [6],
        },
        nil: null,
      },
      `
      {
        "bool": true,
        "dat": "1970-01-01T00:00:00.055Z",
        "str": "hi",
        "rec": {
          "list": [
            1,
            2
          ],
          "map": {
            "x": 3
          },
          "tab": [
            {
              "num": 4
            },
            {
              "num": 5
            }
          ],
          "tup": [
            6
          ]
        },
        "nil": null
      }
      `
    );
  });

  it('supports custom simple serialisers', () => {
    setupWithSchema(
      recordSchema({
        bool: booleanSchema({
          jsonSimpleTransform: {
            serialize: (value) => 'XXX' + value,
            deserialize: (value) => value.slice(3),
          },
        }),
        num: numberSchema(),
        dat: dateSchema(),
      })
    );
    serializer.setOptions({
      simpleTransform: [
        {
          serialize: (value, schema) =>
            isBooleanSchema(schema) ? value.toString() : value,
          deserialize: (value, schema) =>
            isBooleanSchema(schema) ? value === 'true' : value,
        },
        {
          serialize: (value, schema) =>
            isNumberSchema(schema) ? value + 10 : undefined,
          deserialize: (value, schema) =>
            isNumberSchema(schema) ? value - 10 : undefined,
        },
      ],
    });

    expectToBeEquivalent(
      '/',
      {
        bool: true,
        num: 10,
        dat: new Date(44),
      },
      `
      {
        "bool": "XXXtrue",
        "num": 20,
        "dat": "1970-01-01T00:00:00.044Z"
      }
      `
    );
  });

  it('supports custom serialisers', () => {
    setupWithSchema(
      recordSchema(
        { bool: booleanSchema() },
        {
          jsonTransform: {
            serialize: (value) => ((value.y = value.x + 10), value),
            deserialize: (value) => (delete value.y, value),
          },
        }
      )
    );
    serializer.setOptions({
      transform: [
        {
          serialize: (value, schema) =>
            isBooleanSchema(schema) ? value.toString() : value,
          deserialize: (value, schema) =>
            isBooleanSchema(schema) ? value === 'true' : value,
        },
        {
          serialize: (value, _, path) =>
            path === '/' ? ((value.x = 5), value) : value,
          deserialize: (value, _, path) =>
            path === '/' ? (delete value.x, value) : value,
        },
      ],
    });

    expectToBeEquivalent(
      '/',
      { bool: true },
      `
      {
        "bool": "true",
        "x": 5,
        "y": 15
      }
      `
    );
  });
});
