import { getTestBed, TestBed } from '@angular/core/testing';
import { recordSchema } from '@lightweightform/storage';

import { LfStorage, LF_APP_SCHEMA } from '../storage.service';
import { LfUnloadAlert } from '../unload-alert.service';

describe('LF unload alert', () => {
  let lfUnloadAlert: LfUnloadAlert;
  let lfStorage: LfStorage;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: LF_APP_SCHEMA, useValue: recordSchema({}) },
        LfStorage,
        LfUnloadAlert,
      ],
    });
    lfUnloadAlert = getTestBed().inject(LfUnloadAlert);
    lfStorage = getTestBed().inject(LfStorage);
  });

  it('should be disabled by default', () => {
    expect(lfUnloadAlert.isEnabled).toBe(false);
  });

  it('cancels unload when appropriate', () => {
    lfUnloadAlert.enable();

    // Unload not cancelled because app isn't dirty
    let evt = new Event('beforeunload', { cancelable: true });
    window.dispatchEvent(evt);
    expect(evt.defaultPrevented).toBe(false);

    lfStorage.setDirty();

    // Unload is cancelled
    evt = new Event('beforeunload', { cancelable: true });
    window.dispatchEvent(evt);
    expect(evt.defaultPrevented).toBe(true);

    lfUnloadAlert.disable();

    // Unload not cancelled because the unload service is disabled
    evt = new Event('beforeunload', { cancelable: true });
    window.dispatchEvent(evt);
    expect(evt.defaultPrevented).toBe(false);
  });
});
