import { Component } from '@angular/core';
import { getTestBed, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { listSchema, recordSchema } from '@lightweightform/storage';

import { LfRouter, LfRoutes, LF_ROUTER_BASE_PATH } from '../router.service';
import { LfStorage, LF_APP_SCHEMA } from '../storage.service';

// Dummy component to add to routes to test the LF router
@Component({ selector: 'lf-dummy', template: '' })
class DummyComponent {}

// Schema used to test the LF router
const appSchema = recordSchema({
  a: recordSchema({
    aa: recordSchema({}),
    ac: recordSchema({}),
  }),
  b: recordSchema({}),
  c: recordSchema({}), // No route
  l: listSchema(recordSchema({})),
});

// Routes used to test the LF router without a base path
const routes: LfRoutes = [
  { path: '', redirectTo: 'a', pathMatch: 'full' },
  {
    path: 'a',
    children: [
      { path: '', redirectTo: 'aa', pathMatch: 'full' },
      { path: 'aa', component: DummyComponent },
      { path: 'ab', component: DummyComponent, lfPath: 'ac' },
    ],
  },
  { path: 'b', component: DummyComponent },
  { path: 'l/:index', component: DummyComponent },
  { path: 'nonLf', component: DummyComponent, isLfRoute: false },
  { path: '**', component: DummyComponent },
];

// Routes used to test the LF router with a base path
const routesWithBase: LfRoutes = [
  { path: '', redirectTo: 'path/to/lf', pathMatch: 'full' },
  {
    path: 'path/to/lf',
    children: [
      { path: '', redirectTo: 'a', pathMatch: 'full' },
      {
        path: 'a',
        children: [
          { path: '', redirectTo: 'aa', pathMatch: 'full' },
          { path: 'aa', component: DummyComponent },
          { path: 'ab', component: DummyComponent, lfPath: 'ac' },
        ],
      },
      { path: 'b', component: DummyComponent },
      { path: 'l/:index', component: DummyComponent },
      { path: 'nonLf', component: DummyComponent, isLfRoute: false },
    ],
  },
  { path: 'foo', component: DummyComponent },
  { path: 'bar', children: [{ path: 'quux', component: DummyComponent }] },
  { path: '**', component: DummyComponent },
];

describe('LF router without base path', () => {
  // let lfStorage: LfStorage;
  let lfRouter: LfRouter;
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes(routes)],
      declarations: [DummyComponent],
      providers: [
        { provide: LF_APP_SCHEMA, useValue: appSchema },
        LfStorage,
        LfRouter,
      ],
    });
    // lfStorage = getTestBed().inject(LfStorage);
    lfRouter = getTestBed().inject(LfRouter);
    router = getTestBed().inject(Router);
  });

  it('transforms routes into LF routes', () => {
    expect(router.config).toEqual([
      {
        path: '',
        redirectTo: 'a',
        pathMatch: 'full',
        isLfRoute: true,
        lfPath: '/',
      },
      {
        path: 'a',
        children: [
          {
            path: '',
            redirectTo: 'aa',
            pathMatch: 'full',
            isLfRoute: true,
            lfPath: '/a',
          },
          {
            path: 'aa',
            component: DummyComponent,
            isLfRoute: true,
            lfPath: '/a/aa',
          },
          {
            path: 'ab',
            component: DummyComponent,
            isLfRoute: true,
            lfPath: '/a/ac',
          },
        ],
        isLfRoute: true,
        lfPath: '/a',
      },
      { path: 'b', component: DummyComponent, isLfRoute: true, lfPath: '/b' },
      {
        path: 'l/:index',
        component: DummyComponent,
        isLfRoute: true,
        lfPath: '/l/:index',
      },
      { path: 'nonLf', component: DummyComponent, isLfRoute: false },
      { path: '**', component: DummyComponent, isLfRoute: false },
    ]);
  });

  it('finds number of matching routes with default options', () => {
    // All routes except the non-LF routes
    expect(lfRouter.matchingRoutes('/').length).toBe(7);
    // Root route, the route itself (2 of them because of the redirect), and all
    // children
    expect(lfRouter.matchingRoutes('/a').length).toBe(5);
    // Parents and the route itself
    expect(lfRouter.matchingRoutes('/a/aa').length).toBe(4);
    expect(lfRouter.matchingRoutes('/a/ac').length).toBe(4);
    // Root and the route itself
    expect(lfRouter.matchingRoutes('/b').length).toBe(2);
    // Root route, `/c` has no matching route
    expect(lfRouter.matchingRoutes('/c').length).toBe(1);
    // Root and the `/l/:index` route
    expect(lfRouter.matchingRoutes('/l').length).toBe(2);
    expect(lfRouter.matchingRoutes('/l/0').length).toBe(2);

    // Invalid path, `/a/b` is the route, the path is `/a/c`
    expect(() => lfRouter.matchingRoutes('/a/b')).toThrow();
  });

  it('finds number of matching routes without `navigateToParent`', () => {
    const opts = { navigateToParent: false };
    // All routes except the non-LF routes
    expect(lfRouter.matchingRoutes('/', opts).length).toBe(7);
    // The route itself (2 of them because of the redirect), and all children
    expect(lfRouter.matchingRoutes('/a', opts).length).toBe(4);
    // Route itself
    expect(lfRouter.matchingRoutes('/a/aa', opts).length).toBe(1);
    expect(lfRouter.matchingRoutes('/a/ac', opts).length).toBe(1);
    expect(lfRouter.matchingRoutes('/b', opts).length).toBe(1);
    // No matching routes
    expect(lfRouter.matchingRoutes('/c', opts).length).toBe(0);
    // `/l/:index` route
    expect(lfRouter.matchingRoutes('/l', opts).length).toBe(1);
    expect(lfRouter.matchingRoutes('/l/0', opts).length).toBe(1);
  });

  it('finds number of matching routes without `navigateToChild`', () => {
    const opts = { navigateToChild: false };
    // Root route
    expect(lfRouter.matchingRoutes('/', opts).length).toBe(1);
    // Root route + the route itself (2 of them because of the redirect)
    expect(lfRouter.matchingRoutes('/a', opts).length).toBe(3);
    // Parents and the route itself
    expect(lfRouter.matchingRoutes('/a/aa', opts).length).toBe(4);
    expect(lfRouter.matchingRoutes('/a/ac', opts).length).toBe(4);
    // Root and the route itself
    expect(lfRouter.matchingRoutes('/b', opts).length).toBe(2);
    // Root route, `/c` and `/l` have no matching routes
    expect(lfRouter.matchingRoutes('/c', opts).length).toBe(1);
    expect(lfRouter.matchingRoutes('/l', opts).length).toBe(1);
    // Root and the `/l/:index` route
    expect(lfRouter.matchingRoutes('/l/0', opts).length).toBe(2);
  });

  it(
    'finds number of matching routes without `navigateToParent` or ' +
      '`navigateToChild`',
    () => {
      const opts = { navigateToParent: false, navigateToChild: false };
      // Root route
      expect(lfRouter.matchingRoutes('/', opts).length).toBe(1);
      // The route itself (2 of them because of the redirect)
      expect(lfRouter.matchingRoutes('/a', opts).length).toBe(2);
      // The route itself
      expect(lfRouter.matchingRoutes('/a/aa', opts).length).toBe(1);
      expect(lfRouter.matchingRoutes('/a/ac', opts).length).toBe(1);
      expect(lfRouter.matchingRoutes('/b', opts).length).toBe(1);
      // `/c` and `/l` have no matching routes
      expect(lfRouter.matchingRoutes('/c', opts).length).toBe(0);
      expect(lfRouter.matchingRoutes('/l', opts).length).toBe(0);
      // `/l/:index` route
      expect(lfRouter.matchingRoutes('/l/0', opts).length).toBe(1);
    }
  );
});

describe('LF router with base path', () => {
  // let lfStorage: LfStorage;
  let lfRouter: LfRouter;
  let router: Router;

  function setupTestbedWithBasePath(routerBasePath: string) {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes(routesWithBase)],
      declarations: [DummyComponent],
      providers: [
        { provide: LF_APP_SCHEMA, useValue: appSchema },
        { provide: LF_ROUTER_BASE_PATH, useValue: routerBasePath },
        LfStorage,
        LfRouter,
      ],
    });
    // lfStorage = getTestBed().inject(LfStorage);
    lfRouter = getTestBed().inject(LfRouter);
    router = getTestBed().inject(Router);
  }

  it('transforms routes only within base path "/path/to/lf"', () => {
    setupTestbedWithBasePath('/path/to/lf');
    expect(router.config).toEqual([
      {
        path: '',
        redirectTo: 'path/to/lf',
        pathMatch: 'full',
      },
      {
        path: 'path/to/lf',
        isLfRoute: true,
        lfPath: '/',
        children: [
          {
            path: '',
            redirectTo: 'a',
            pathMatch: 'full',
            isLfRoute: true,
            lfPath: '/',
          },
          {
            path: 'a',
            children: [
              {
                path: '',
                redirectTo: 'aa',
                pathMatch: 'full',
                isLfRoute: true,
                lfPath: '/a',
              },
              {
                path: 'aa',
                component: DummyComponent,
                isLfRoute: true,
                lfPath: '/a/aa',
              },
              {
                path: 'ab',
                component: DummyComponent,
                isLfRoute: true,
                lfPath: '/a/ac',
              },
            ],
            isLfRoute: true,
            lfPath: '/a',
          },
          {
            path: 'b',
            component: DummyComponent,
            isLfRoute: true,
            lfPath: '/b',
          },
          {
            path: 'l/:index',
            component: DummyComponent,
            isLfRoute: true,
            lfPath: '/l/:index',
          },
          { path: 'nonLf', component: DummyComponent, isLfRoute: false },
        ],
      },
      { path: 'foo', component: DummyComponent },
      { path: 'bar', children: [{ path: 'quux', component: DummyComponent }] },
      { path: '**', component: DummyComponent },
    ]);
  });

  it('transforms routes only within base path "/path/to"', () => {
    setupTestbedWithBasePath('/path/to');

    expect(router.config).toEqual([
      {
        path: '',
        redirectTo: 'path/to/lf',
        pathMatch: 'full',
      },
      {
        path: 'path/to/lf',
        isLfRoute: true,
        lfPath: '/lf',
        children: [
          {
            path: '',
            redirectTo: 'a',
            pathMatch: 'full',
            isLfRoute: true,
            lfPath: '/lf',
          },
          {
            path: 'a',
            children: [
              {
                path: '',
                redirectTo: 'aa',
                pathMatch: 'full',
                isLfRoute: true,
                lfPath: '/lf/a',
              },
              {
                path: 'aa',
                component: DummyComponent,
                isLfRoute: true,
                lfPath: '/lf/a/aa',
              },
              {
                path: 'ab',
                component: DummyComponent,
                isLfRoute: true,
                lfPath: '/lf/a/ac',
              },
            ],
            isLfRoute: true,
            lfPath: '/lf/a',
          },
          {
            path: 'b',
            component: DummyComponent,
            isLfRoute: true,
            lfPath: '/lf/b',
          },
          {
            path: 'l/:index',
            component: DummyComponent,
            isLfRoute: true,
            lfPath: '/lf/l/:index',
          },
          { path: 'nonLf', component: DummyComponent, isLfRoute: false },
        ],
      },
      { path: 'foo', component: DummyComponent },
      { path: 'bar', children: [{ path: 'quux', component: DummyComponent }] },
      { path: '**', component: DummyComponent },
    ]);
  });
});
