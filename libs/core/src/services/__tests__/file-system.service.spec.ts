import { saveAs } from 'file-saver';

import { FileSystemService } from '../file-system.service';

// Returns the content of a Blob as a string (asynchronously)
function blobAsString(blob: Blob): Promise<string> {
  return new Promise((resolve) => {
    const fileReader = new FileReader();
    fileReader.onload = () => resolve(fileReader.result!.toString());
    fileReader.readAsText(blob);
  });
}

describe('File system service', () => {
  let fileSystem: FileSystemService;
  beforeEach(() => (fileSystem = new FileSystemService()));

  it('correctly determines whether loading is supported', () => {
    expect(fileSystem.loadIsSupported).toBe(true);
    // Remove the `FileReader` to check that the loading stops being supported
    // and add it again after
    const fileReader = FileReader;
    FileReader = undefined as any;
    expect(fileSystem.loadIsSupported).toBe(false);
    FileReader = fileReader;
  });

  it('fails loading when loading is not supported', async () => {
    // Remove the `FileReader` to force loading to fail
    const fileReader = FileReader;
    FileReader = undefined as any;
    try {
      await fileSystem.load();
    } catch (err) {
      expect(err).toBeInstanceOf(Error);
    }
    FileReader = fileReader;
  });

  it('saves a string into a file with default options', async () => {
    await fileSystem.save(['some text'], 'file.txt');
    expect(saveAs).toHaveBeenCalled();
    const [blob, fileName, disableAutoBOM] = (saveAs as any).mock.calls[0];
    expect(await blobAsString(blob)).toBe('some text');
    expect(blob.type).toBe('text/plain;charset=utf-8');
    expect(fileName).toBe('file.txt');
    expect(disableAutoBOM).toBe(false);
  });

  it('saves a string into a file with custom options', async () => {
    await fileSystem.save(['some text'], 'file.txt', {
      mimeType: 'application/json',
      charset: 'iso-8859-1',
      autoBOM: false,
    });
    expect(saveAs).toHaveBeenCalled();
    const [blob, fileName, disableAutoBOM] = (saveAs as any).mock.calls[0];
    expect(await blobAsString(blob)).toBe('some text');
    expect(blob.type).toBe('application/json;charset=iso-8859-1');
    expect(fileName).toBe('file.txt');
    expect(disableAutoBOM).toBe(true);
  });

  it('loads previously saved file', async () => {
    await fileSystem.save(['some text'], 'file.txt');
    const content = await fileSystem.load();
    expect(content).toBe('some text');
  });
});
