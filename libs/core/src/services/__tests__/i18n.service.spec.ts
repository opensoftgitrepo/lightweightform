import { getTestBed, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import {
  listSchema,
  numberSchema,
  recordSchema,
} from '@lightweightform/storage';
import { action, makeObservable, observable } from 'mobx';

import {
  LfI18n,
  LF_APP_I18N,
  LF_I18N_LANGUAGE_QUERY_PARAM,
} from '../i18n.service';
import { LfRouter } from '../router.service';
import { LfStorage, LF_APP_SCHEMA } from '../storage.service';

const appSchema = recordSchema({
  num: numberSchema(),
  list: listSchema(numberSchema()),
});

const appI18n = {
  en: {
    '*': {
      appName: 'Test',
      appDescription: '<3',
      object: {
        prop: 'Prop',
      },
    },
    '/num': {
      label: 'Number',
      currency: '€',
      object: {
        prop: 'Number prop',
      },
    },
    '/list/?': {
      label: 'Element',
      object: {
        prop: 'Element prop',
      },
    },
  },
  pt: {
    '*': {
      appName: 'Teste',
    },
    '/num': {
      label: 'Número',
    },
    '/list/?': {
      label: 'Elemento',
    },
  },
};

describe('LF i18n', () => {
  let lfI18n: LfI18n;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: LF_APP_SCHEMA, useValue: appSchema },
        { provide: LF_APP_I18N, useValue: appI18n },
        LfStorage,
        LfI18n,
      ],
    });
    lfI18n = getTestBed().inject(LfI18n);
  });

  it('allows merging translation objects', () => {
    const obj1 = { en: { '*': { a: 'a', c: 'b' } } };
    const obj2 = { en: { '*': { b: 'b', c: 'c' } } };
    const res = LfI18n.mergeTranslations(obj1, obj2);
    expect(res).toEqual({ en: { '*': { a: 'a', b: 'b', c: 'c' } } });
    expect(res).toBe(obj1); // Merging happens in place
  });

  it('rejects merging an empty list of translations', () => {
    expect(() => LfI18n.mergeTranslations()).toThrow();
  });

  it('lists all languages', () => {
    expect(lfI18n.languages).toEqual(['en', 'pt']);
  });

  it('sets the default language as the first language found', () => {
    expect(lfI18n.defaultLanguage).toBe('en');
  });

  it('allows changing the default language', () => {
    lfI18n.defaultLanguage = 'pt';
    expect(lfI18n.defaultLanguage).toBe('pt');
  });

  it('uses the default language as the current language', () => {
    expect(lfI18n.currentLanguage).toBe('en');
    lfI18n.defaultLanguage = 'pt';
    expect(lfI18n.currentLanguage).toBe('pt');
  });

  it('allows changing the current language', () => {
    lfI18n.currentLanguage = 'pt';
    expect(lfI18n.currentLanguage).toBe('pt');
    // Allows languages that don't exist on the i18n object
    lfI18n.currentLanguage = 'es';
    expect(lfI18n.currentLanguage).toBe('es');
    // Allows setting the current language as `undefined` to use the default
    // language instead
    lfI18n.currentLanguage = undefined as any; // TS limitation
    expect(lfI18n.currentLanguage).toBe(lfI18n.defaultLanguage);
  });

  it('gets translations from the "generic" path', () => {
    expect(lfI18n.get('appName')).toBe('Test');
    expect(lfI18n.get('appDescription')).toBe('<3');
    expect(lfI18n.get('object.prop')).toBe('Prop');
    lfI18n.currentLanguage = 'pt';
    expect(lfI18n.get('appName')).toBe('Teste');
    expect(lfI18n.get('appDescription')).toBe('<3'); // Default lang
    // Non existent key
    expect(lfI18n.get('nonExistent')).toBeUndefined();
    // Will fetch from default lang
    lfI18n.currentLanguage = 'es';
    expect(lfI18n.get('appName')).toBe('Test');
    // Non existent langs (settings `defaultLanguage` to `undefined` shouldn't
    // be allowed, but on start-up of the service it may happen)
    lfI18n.defaultLanguage = lfI18n.currentLanguage = undefined as any;
    expect(lfI18n.get('appName')).toBe(undefined);
  });

  it('gets translations from a path', () => {
    expect(lfI18n.getFromPath('/num', 'label')).toBe('Number');
    expect(lfI18n.getFromPath('/num', 'currency')).toBe('€');
    expect(lfI18n.getFromPath('/num', 'object.prop')).toBe('Number prop');
    lfI18n.currentLanguage = 'pt';
    expect(lfI18n.getFromPath('/num', 'label')).toBe('Número');
    expect(lfI18n.getFromPath('/num', 'currency')).toBe('€'); // Default lang
    // Non existent key
    expect(lfI18n.getFromPath('/num', 'nonExistent')).toBeUndefined();
    // Will fetch from default lang
    lfI18n.currentLanguage = 'es';
    expect(lfI18n.getFromPath('/num', 'label')).toBe('Number');
    // Invalid path
    expect(() => lfI18n.getFromPath('/x', 'label')).toThrow();
  });

  it('gets translations from a generic path', () => {
    expect(lfI18n.getFromPath('/list/0', 'label')).toBe('Element');
    expect(lfI18n.getFromPath('/list/99', 'label')).toBe('Element');
    expect(lfI18n.getFromPath('/list/0', 'object.prop')).toBe('Element prop');
    lfI18n.currentLanguage = 'pt';
    expect(lfI18n.getFromPath('/list/0', 'label')).toBe('Elemento');
    expect(lfI18n.getFromPath('/list/99', 'label')).toBe('Elemento');
    // Non existent key
    expect(lfI18n.getFromPath('/list/99', 'nonExistent')).toBeUndefined();
    // Will fetch from default lang
    lfI18n.currentLanguage = 'es';
    expect(lfI18n.getFromPath('/list/0', 'label')).toBe('Element');
    // Invalid path
    expect(() => lfI18n.getFromPath('/list/x', 'label')).toThrow();
  });

  it('allows adding new translations dynamically', () => {
    lfI18n.addTranslations({
      en: {
        '*': {
          appName: 'Test 2',
          object: {
            otherProp: 'Other prop',
          },
        },
      },
    });
    expect(lfI18n.get('appName')).toBe('Test 2');
    expect(lfI18n.get('object.otherProp')).toBe('Other prop');
    expect(lfI18n.get('object.prop')).toBe('Prop'); // Not overridden

    lfI18n.addTranslations({ '*': { appName: 'Prueba' } }, 'es');
    lfI18n.currentLanguage = 'es';
    expect(lfI18n.get('appName')).toBe('Prueba');
    expect(lfI18n.get('appDescription')).toBe('<3');

    // Invalid translation key
    expect(() =>
      lfI18n.addTranslations({ '/num': { 'x.y': 'z' } }, 'en')
    ).toThrow();
  });
});

// Mock router which allows manipulating query parameters
class MockRouter {
  @observable public queryParams: Record<string, string> = {};

  constructor() {
    makeObservable(this);
  }

  @action
  public setQueryParams(params: Record<string, string>): Promise<boolean> {
    this.queryParams = { ...this.queryParams, ...params }; // Merge
    return Promise.resolve(true); // Fake `async`
  }
}

describe('LF i18n with router integration', () => {
  let lfRouter: LfRouter;
  let lfI18n: LfI18n;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([])],
      providers: [
        { provide: LF_APP_SCHEMA, useValue: appSchema },
        { provide: LF_APP_I18N, useValue: appI18n },
        { provide: LF_I18N_LANGUAGE_QUERY_PARAM, useValue: 'lang' },
        { provide: LfRouter, useClass: MockRouter },
        LfStorage,
        LfI18n,
      ],
    });
    lfRouter = getTestBed().inject(LfRouter);
    lfI18n = getTestBed().inject(LfI18n);
  });

  it('uses a query parameter to set the current language', async () => {
    expect(lfI18n.defaultLanguage).toBe('en');
    expect(lfI18n.currentLanguage).toBe('en');
    await lfRouter.setQueryParams({ lang: 'pt' });
    expect(lfI18n.currentLanguage).toBe('pt');
    await lfI18n.setCurrentLanguage('fr');
    expect(lfRouter.queryParams.lang).toBe('fr');
    await lfRouter.setQueryParams({ lang: null });
    expect(lfI18n.currentLanguage).toBe('en');
  });
});
