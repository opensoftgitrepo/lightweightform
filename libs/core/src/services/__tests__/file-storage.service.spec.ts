import { Injectable } from '@angular/core';
import { getTestBed, TestBed } from '@angular/core/testing';
import { numberSchema, recordSchema } from '@lightweightform/storage';
import { saveAs } from 'file-saver';

import { LfSerializer } from '../../abstract/value-serializer';
import {
  LfFileStorage,
  LfFileStorageLoadInterceptor,
  LF_FILE_STORAGE_LOAD_INTERCEPTOR,
} from '../file-storage.service';
import { FileSystemService } from '../file-system.service';
import {
  LfJsonSerializer,
  LF_JSON_SERIALIZER_OPTIONS,
} from '../json-serializer.service';
import { LfStorage, LF_APP_SCHEMA } from '../storage.service';

// Schema used to test the LF file storage
const appSchema = recordSchema({
  num: numberSchema({ initialValue: 10 }),
});

@Injectable()
class LoadInterceptor implements LfFileStorageLoadInterceptor {
  private _beforeLoad;

  public setBeforeLoad(fn): void {
    this._beforeLoad = fn;
  }

  public beforeLoad(value, schema, path): void {
    this._beforeLoad && this._beforeLoad(value, schema, path);
  }
}

describe('LF file storage with JSON serialiser', () => {
  let lfStorage: LfStorage;
  let lfFileStorage: LfFileStorage;
  let fileSystem: FileSystemService;
  let serializer: LfJsonSerializer;
  let loadInterceptor: LoadInterceptor;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: LF_APP_SCHEMA, useValue: appSchema },
        { provide: LfSerializer, useClass: LfJsonSerializer },
        {
          provide: LF_FILE_STORAGE_LOAD_INTERCEPTOR,
          useClass: LoadInterceptor,
        },
        // Don't use spaces when saving
        { provide: LF_JSON_SERIALIZER_OPTIONS, useValue: { space: 0 } },
        LfStorage,
        FileSystemService,
        LfFileStorage,
      ],
    });
    lfStorage = getTestBed().inject(LfStorage);
    lfFileStorage = getTestBed().inject(LfFileStorage);
    fileSystem = getTestBed().inject(FileSystemService);
    serializer = getTestBed().inject(LfSerializer) as LfJsonSerializer;
    loadInterceptor = getTestBed().inject(
      LF_FILE_STORAGE_LOAD_INTERCEPTOR
    ) as LoadInterceptor;
  });

  // NOTE: File saving/loading is being mocked; the file names don't matter in
  // these tests; the loaded file is always the one that was last saved

  it('saves/loads values using the JSON serialiser', async () => {
    serializer.setOptions({ charset: 'ansi', autoBOM: false });
    // Save to file
    await lfFileStorage.saveToFile('/', 'app');
    expect(await fileSystem.load()).toBe('{"num":10}');

    // The options set in the serialiser were used
    expect(saveAs).toHaveBeenCalled();
    const [blob, fileName, disableAutoBOM] = (saveAs as any).mock.calls[0];
    expect(blob.type).toBe('application/json;charset=ansi');
    expect(fileName).toBe('app.json');
    expect(disableAutoBOM).toBe(true);

    // Load from file
    await fileSystem.save(['20'], 'num.json');
    await lfFileStorage.loadFromFile('/num');
    expect(lfStorage.get('/num')).toBe(20);
  });

  it('supports custom encodings', async () => {
    serializer.setOptions({ encode: (str) => [str + 'xxx'] });
    // Save to file
    await lfFileStorage.saveToFile('/', 'app');
    expect(await fileSystem.load()).toBe('{"num":10}xxx');
  });

  it('allows passing merge options when loading', async () => {
    lfStorage.set('/num', 99);

    // Load from file
    await fileSystem.save(['{}'], 'app.json');
    await lfFileStorage.loadFromFile('/', { handleUndefined: 'reset' });
    expect(lfStorage.get('/num')).toBe(10); // Initial value
  });

  it('allows intercepting a load', async () => {
    const beforeLoad = jest.fn(() => {
      throw new Error();
    });
    loadInterceptor.setBeforeLoad(beforeLoad);

    expect.assertions(3);
    await fileSystem.save(['{}'], 'app.json');
    try {
      await lfFileStorage.loadFromFile('/');
    } catch (err) {
      expect(err).toBeInstanceOf(Error);
    }
    expect(beforeLoad).toHaveBeenCalled();
    expect(beforeLoad.mock.calls[0]).toEqual([{}, appSchema, '/']);
  });
});
