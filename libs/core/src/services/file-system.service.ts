import { Inject, Injectable, InjectionToken, Optional } from '@angular/core';
import { saveAs } from 'file-saver';

/**
 * Optional options available when saving to a file.
 */
export interface SaveOptions {
  /**
   * Mime type of the saved file.
   */
  mimeType?: string;
  /**
   * Character encoding to set as metadata when saving the file. Defaults to
   * `utf-8`.
   */
  charset?: string;
  /**
   * Whether to automatically add an UTF BOM.
   */
  autoBOM?: boolean;
}

/**
 * Optional options available when loading from a file.
 */
export interface LoadOptions {
  /**
   * Accepted type of files (used by the `<input>` element).
   */
  accept?: string;
  /**
   * Character encoding used by the file to load.
   */
  charset?: string;
}

/**
 * Injection token to specify the amount of milliseconds after a window focus
 * that we wait for a change event on the file input to occur before we consider
 * that the user has cancelled the loading.
 */
export const LF_WAIT_BEFORE_DETECT_CANCEL = new InjectionToken<number>(
  'LF_WAIT_BEFORE_DETECT_CANCEL'
);

/**
 * Service used to interact with the user's file system in order to save or load
 * files. This service does not depend on anything LF related but is used by LF
 * to save/load application state.
 */
@Injectable({ providedIn: 'root' })
export class FileSystemService {
  /**
   * Default amount of milliseconds after a window focus that we wait for a
   * change event on the file input to occur before we consider that the user
   * has cancelled the loading.
   */
  static DEFAULT_WAIT_BEFORE_DETECT_CANCEL = 1000;

  protected _fileInput: HTMLInputElement;
  protected _reader: FileReader;

  // Settings for the current load, note that only one load may run at a time
  protected _loadCharset: string;
  protected _loadResolve?: (fileContent: string | null) => void;
  protected _loadReject?: (error: Error) => void;

  // Timeout until we detect a cancel
  private _cancelTimeout?: any;

  constructor(
    @Optional()
    @Inject(LF_WAIT_BEFORE_DETECT_CANCEL)
    private waitBeforeDetectCancel: number | null = null
  ) {
    if (!this.loadIsSupported) {
      return;
    }

    // Create a new `<input type="file">` element
    this._fileInput = document.createElement('input');
    this._fileInput.type = 'file';

    // Create a file reader that on load calls the callback function
    this._reader = new FileReader();

    // File reader events
    this._reader.onload = () => {
      this._loadResolve?.(this._reader.result!.toString());
      this._loadResolve = undefined;

      // Fix for IE
      ignoreChange = true;
      this._fileInput.value = '';
      ignoreChange = false;
    };
    this._reader.onerror = () => {
      this._loadReject?.(this._reader.error!);
      this._loadReject = undefined;
    };

    // Read the file and set the model whenever a file is selected; IE triggers
    // a `change` event when emptying the value, so a flag to ignore a change is
    // required
    let ignoreChange = false;
    this._fileInput.addEventListener('change', () => {
      if (ignoreChange) {
        return;
      }

      if (this._cancelTimeout != null) {
        clearTimeout(this._cancelTimeout);
        this._cancelTimeout = undefined;
      }

      if ((this._fileInput.files || []).length > 0) {
        this._reader.readAsText(this._fileInput.files![0], this._loadCharset);
      }
    });
  }

  /**
   * Whether the current browser supports saving to a file (IE<=9 is not
   * supported).
   */
  public get saveIsSupported(): boolean {
    return window.Blob !== undefined;
  }

  /**
   * Whether the current browser supports loading from a file.
   */
  public get loadIsSupported(): boolean {
    return (window as any).FileReader !== undefined;
  }

  /**
   * Saves the provided content into a file. Does not support IE<=9.
   * @param content Content to save to file.
   * @param fileName Name of the file to save.
   * @param options Options used to save the file.
   * @returns A promise which resolves once the file has been saved.
   */
  public save(
    content: BlobPart[],
    fileName: string,
    {
      mimeType = 'text/plain',
      charset = 'utf-8',
      autoBOM = true,
    }: SaveOptions = {}
  ): Promise<void> {
    if (!this.saveIsSupported) {
      return Promise.reject(new Error('Save is not supported in this browser'));
    }

    return new Promise((resolve, reject) => {
      const blob = new Blob(content, {
        type: `${mimeType};charset=${charset}`,
      });
      const saver: any = saveAs(blob, fileName, !autoBOM);
      // `FileSaver` uses IE10+ and Edge's built-in `msSaveOrOpenBlob` when
      // available, which returns a boolean to represent success; for other
      // browsers, an object is returned on which we may define callbacks
      if (saver !== null && typeof saver === 'object') {
        saver.onwrite = resolve;
        // From reading the `FileSaver` source, this seems to never be
        // called; however, it may one day be implemented
        saver.onerror = () => reject(saver.error);
      } else if (saver) {
        resolve();
      } else {
        reject(new Error('Error saving file'));
      }
    });
  }

  /**
   * Triggers a file picker to choose a file to be loaded.
   * @param options Options to load from a file.
   * @returns Promise which resolves with the content of the file once the file
   * has finished loading or `null` if the user cancelled the action.
   */
  public load({
    accept = '.txt,text/plain',
    charset = 'utf-8',
  }: LoadOptions = {}): Promise<string | null> {
    if (!this.loadIsSupported) {
      return Promise.reject(new Error('Load is not supported in this browser'));
    }

    // Use the file system access API on browsers that support it as it provides
    // a proper API that detects a user pressing "cancel".
    if (typeof (window as any).showOpenFilePicker !== 'undefined') {
      return new Promise(async (resolve, reject) => {
        this._loadResolve = resolve;
        this._loadReject = reject;

        try {
          // Convert the `accept` string into a "type" understood by the file
          // system access API
          // TODO: Change our own API to match the file system access one as it
          //  is more flexible and can be converted into the legacy one
          const accepts = accept.split(',');
          const mimeTypes = accepts.filter((str) => str[0] !== '.');
          const extensions = accepts.filter((str) => str[0] === '.');
          const [fileHandle] = await (window as any).showOpenFilePicker({
            types: [
              {
                description: extensions.map((ext) => `*${ext}`).join(', '),
                accept: mimeTypes.reduce((obj, mimeType) => {
                  obj[mimeType] = extensions;
                  return obj;
                }, {}),
              },
            ],
          });
          const file = await fileHandle.getFile();
          this._reader.readAsText(file, charset);
        } catch (err) {
          // User cancelled the file load
          if (err instanceof DOMException) {
            resolve(null);
          } else {
            reject(err);
          }
        }
      });
    }

    return new Promise((resolve, reject) => {
      // Set settings for loading
      this._loadResolve = resolve;
      this._loadReject = reject;
      this._loadCharset = charset;

      // Set the `<input>` element's `accept` attribute
      this._fileInput.accept = accept;

      // Trigger the file picker
      this._fileInput.click();

      // XXX: Attempt to resolve the promise with `null` when the user cancels
      // the loading of the file. Explanation: there is no browser event emitted
      // when the user presses "cancel" on the file picker; to work around this
      // we wait for `WAIT_BEFORE_DETECT_CANCEL`ms after the first event of the
      // types below and consider that the user cancelled if no `change` event
      // was emitted by the browser in this time.
      const eventTypes = ['focus', 'mousemove', 'touchstart'];
      const cancelAfterTimeout = () => {
        eventTypes.forEach((type) =>
          window.removeEventListener(type, cancelAfterTimeout)
        );
        this._cancelTimeout = setTimeout(() => {
          this._cancelTimeout = undefined;
          if ((this._fileInput.files || []).length === 0) {
            resolve(null);
          }
        }, this.waitBeforeDetectCancel ?? FileSystemService.DEFAULT_WAIT_BEFORE_DETECT_CANCEL);
      };
      eventTypes.forEach((type) =>
        window.addEventListener(type, cancelAfterTimeout)
      );
    });
  }
}
