import { Inject, Injectable, InjectionToken, Optional } from '@angular/core';
import {
  ActivationEnd,
  NavigationCancel,
  NavigationEnd,
  NavigationError,
  NavigationExtras,
  Params,
  Route,
  RouteConfigLoadEnd,
  Router,
  UrlTree,
} from '@angular/router';
import {
  appendToPath,
  arrayToPath,
  isSubpath,
  pathError,
  resolvePath,
  resolvePathToArray,
} from '@lightweightform/storage';
import { action, autorun, computed, makeObservable, observable } from 'mobx';

import { LfStorage } from './storage.service';

/**
 * List of LF routes.
 */
export type LfRoutes = LfRoute[];

/**
 * Function to indicate whether an LF route can be activated.
 */
export type LfRouteCanActivate = (ctx: LfStorage) => boolean;

/**
 * An Angular Router route extended with LF-specific options.
 */
export interface LfRoute extends Route {
  /**
   * Flag that indicates whether the route is an LF route.
   */
  isLfRoute?: boolean;
  /**
   * Schema path that this route represents. If not absolute, this path is
   * relative to their parent route's LF path. These paths support the syntax
   * used for defining parameters within Angular routes (e.g. `:param`).
   */
  lfPath?: string;
  /**
   * Function used to determine whether the route can be activated for a given
   * path. By default the route can be activated for existing non-`null` paths.
   */
  // TODO: Possibly support asynchronous conditions; tricky to have an
  // observable over them for automatically redirecting on deactivate.
  lfCanActivate?: LfRouteCanActivate;
  children?: LfRoutes;
}

/**
 * Extra options used during LF navigation.
 */
export interface LfNavigationExtras extends NavigationExtras {
  /**
   * If, when attempting to navigate to a path which has no associated route,
   * the router should instead navigate to its first child that has an
   * associated route. Defaults to `true`.
   *
   * Example: if attempting to navigate to `/a` but there is no route associated
   * with `/a`, then the router will check if there exists a child path (e.g.
   * `/a/b`) with an associated route and navigate to the first such path
   * instead. When both `navigateToChild` and `navigateToParent` are `true`, the
   * router will first attempt to navigate to a child before attempting to
   * navigate to the parent.
   */
  navigateToChild?: boolean;
  /**
   * Whether, when attempting to navigate to a path which has no associated
   * route, the router should instead navigate to its closest parent path with
   * an associated route. Defaults to `true`.
   *
   * Example: if attempting to navigate to `/a/b/c` but a route only exists for
   * `/a`, then the router should navigate to `/a`. When both `navigateToChild`
   * and `navigateToParent` are `true`, the router will first attempt to
   * navigate to a child before attempting to navigate to the parent.
   */
  navigateToParent?: boolean;
  /**
   * When `navigateToParent` is `true`, this setting sets the part of the path
   * that didn't match the route as the URL's fragment. Defaults to `true`.
   *
   * Example: if attempting to navigate to `/a/b/c` but a route only exists for
   * `/a`, then the router should navigate to `/a#b/c`.
   */
  extraPathAsFragment?: boolean;
  /**
   * Extra route parameters to use when navigating to an LF path. This is useful
   * if there is an LF route with, for example, `lfPath` set to `/a` but whose
   * Angular router path is `:param/a`. Unless `param` is specified, the LF
   * router will not know what path to navigate to when navigating to `/a`.
   */
  extraParams?: Params;
}

/**
 * Result of matching a provided LF path with the `lfPath` of a route.
 */
export interface MatchingPath {
  /**
   * Parts of the provided path that match the route's path.
   */
  matching: string[];
  /**
   * Parameters used when matching the provided path with the route's path.
   */
  params: Params;
  /**
   * Parts of the route's path missing in the provided path.
   */
  missing: string[];
  /**
   * Parts of the provided path that are children of the route's path.
   */
  extra: string[];
}

/**
 * Information on a route that matches a provided LF path.
 */
export interface LfMatchingRoute {
  /**
   * List of routes that match the provided path.
   */
  routes: LfRoutes;
  /**
   * Information on the matching of the provided path with the route's `lfPath`.
   * `null` when the route isn't an LF route (because it is a parent of an LF
   * route).
   */
  matchingPath: MatchingPath;
}

/**
 * Injection token used to set the base Angular path on which to find the LF
 * routes configuration. Routes not within the provided path are ignored by the
 * service. This makes it possible to have multiple LF apps within an Angular
 * app without them conflicting with each other or to simply have an LF app
 * within a regular Angular app without the need to add `isLfRoute: false` to
 * all non-LF related routes.
 */
export const LF_ROUTER_BASE_PATH = new InjectionToken<string>(
  'LF_ROUTER_BASE_PATH'
);

/**
 * LF router options.
 */
export interface LfRouterOptions {
  /**
   * Default function that determines whether a certain LF route can activate.
   * An `lfCanActivate` can still be set on each LF route. By default, a given
   * route can activate if there exists a value in the storage for the route's
   * `lfPath` that isn't `null`.
   */
  defaultCanActivate: LfRouteCanActivate;
}

/**
 * Injection token used to set the LF router options.
 */
export const LF_ROUTER_OPTIONS = new InjectionToken<LfRouterOptions>(
  'LF_ROUTER_OPTIONS'
);

/**
 * Default function to check whether a given LF route can activate. It returns
 * `true` when the LF path of the route to activate corresponds to an existing
 * non-`null` value in the storage.
 */
export function defaultLfCanActivate(ctx: LfStorage): boolean {
  // Same as `ctx.has() && ctx.get() !== null` but with a single storage call
  try {
    return ctx.get() !== null;
  } catch (err) {
    return false;
  }
}

/**
 * Service used to navigate throughout the application.
 */
@Injectable()
export class LfRouter {
  /**
   * Configuration of the LfRouter (copy of the Angular router's config but with
   * only LF-relevant routes and normalised access to a route's `children`). The
   * observable is updated whenever a new lazy-loaded route is loaded.
   */
  @observable.ref private lfRouterConfig: LfRoutes;

  /**
   * LF route currently activated.
   */
  @observable.ref private activatedLfRoute: LfRoute | null = null;

  /**
   * Parameters of the Angular route currently activated.
   */
  @observable.struct private _params: Params = {};

  /**
   * URL query parameters in object notation of the Angular route currently
   * activated.
   */
  @observable.struct private _queryParams: Params = {};

  /**
   * Fragment of the Angular route currently activated.
   */
  @observable private _fragment: string | null = null;

  /**
   * LF path currently activated.
   */
  @observable private _path: string | null = null;

  /**
   * Index of the id of the current active LF path in the list of children ids
   * of its parent path, or `-1` when the id doesn't exist.
   */
  private idIndex = -1;

  constructor(
    private lfStorage: LfStorage,
    private router: Router,
    @Optional()
    @Inject(LF_ROUTER_BASE_PATH)
    private lfRouterBasePath: string,
    @Optional()
    @Inject(LF_ROUTER_OPTIONS)
    private options: LfRouterOptions | null
  ) {
    makeObservable(this);

    // Normalise the service's base path
    this.lfRouterBasePath =
      this.lfRouterBasePath === null
        ? '/'
        : resolvePath('/', this.lfRouterBasePath);

    this.updateConfig(); // Transform Angular routes into LF routes

    // Listen to Angular router events and update the LF router state as
    // appropriate
    let activatedParams: Params | null = null;
    let activatedQueryParams: Params | null = null;
    let activatedFragment: string | null = null;
    let activatedLfRoute: LfRoute | null = null;
    let routeConfigLoaded = false;
    this.router.events.subscribe((evt) => {
      // TODO: Handle multiple outlets
      if (evt instanceof RouteConfigLoadEnd) {
        routeConfigLoaded = true;
      } else if (evt instanceof ActivationEnd) {
        // Save the params and query params of the first activation
        if (activatedParams === null) {
          activatedParams = evt.snapshot.params;
          activatedQueryParams = evt.snapshot.queryParams;
          activatedFragment = evt.snapshot.fragment;
        }
        // Save the first activation whose route is an LF one
        if (activatedLfRoute === null) {
          const route: LfRoute | null = evt.snapshot.routeConfig;
          if (route !== null && route.isLfRoute) {
            activatedLfRoute = route;
          }
        }
      } else if (evt instanceof NavigationEnd) {
        // Update router state
        action('lfRouterUpdateState', () => {
          this._params = activatedParams || {};
          this._queryParams = activatedQueryParams || {};
          this._fragment = activatedFragment;
          this.activatedLfRoute = activatedLfRoute;
          this._path =
            activatedLfRoute &&
            this.replaceParams(activatedLfRoute.lfPath!, this._params);
          if (this._path) {
            this.saveIdIndex();
          }
          // Update router config with new lazy-loaded route
          if (routeConfigLoaded) {
            this.updateConfig();
          }
        })();
      }
      // Reset variables when the navigation finishes
      if (
        evt instanceof NavigationEnd ||
        evt instanceof NavigationCancel ||
        evt instanceof NavigationError
      ) {
        activatedParams =
          activatedQueryParams =
          activatedFragment =
          activatedLfRoute =
            null;
        routeConfigLoaded = false;
      }
    });

    // Navigate to the previous route whenever the current one becomes invalid
    // (probably because its value was removed)
    autorun(
      () => {
        if (
          this.activatedLfRoute !== null &&
          !this.routeCanActivate(this.activatedLfRoute, this.path!)
        ) {
          this.navigateFromNavigateableRoute(
            this.previousNavigateablePath() ||
              // FIXME: If no route exists, what should we do?
              this.navigateableRoute('/')!,
            {
              replaceUrl: true,
              extraPathAsFragment: false,
              queryParamsHandling: 'merge',
            }
          );
        }
      },
      { name: 'lfRouterAutorunNavigateOnInvalidPath' }
    );
  }

  /**
   * Currently activated LF path (`null` if no LF path is activated).
   */
  @computed
  public get path(): string | null {
    return this._path;
  }

  /**
   * Currently activated LF path with the URL's fragment resolved onto it. If
   * there is no fragment, or if the resolved path is not valid, it returns the
   * activated path itself.
   */
  @computed
  public get pathWithFragment(): string | null {
    if (this._path && this._fragment) {
      const pathWithFragment = resolvePath(this._path, this._fragment);
      if (this.lfStorage.hasPath(pathWithFragment)) {
        return pathWithFragment;
      }
    }
    return this._path;
  }

  /**
   * Parameters of the activated route.
   */
  @computed
  public get params(): Readonly<Params> {
    return this._params;
  }

  /**
   * Query parameters of the activated route.
   */
  @computed
  public get queryParams(): Readonly<Params> {
    return this._queryParams;
  }

  /**
   * Fragment of the activated route (what comes after the `#`).
   */
  @computed
  public get fragment(): string | null {
    return this._fragment;
  }

  /**
   * Helper function to set query parameters in the URL.
   * @param queryParams Query parameters to set.
   * @param queryParamsHandling Strategy on how to handle the parameters.
   * @returns Promise that resolves to `true`/`false` when the navigation
   * succeeds/fails or is rejected when an error happens.
   */
  public setQueryParams(
    queryParams: Params,
    queryParamsHandling?: '' | 'merge' | 'preserve' | null
  ): Promise<boolean> {
    return this.router.navigate([], { queryParams, queryParamsHandling });
  }

  /**
   * Returns the LF routes that match a given path, ordered from most specific
   * to least specific.
   * @param path Path to match against the routes (relative to the current
   * path).
   * @param extras Navigation extras.
   * @returns Routes matching the provided path.
   */
  public matchingRoutes(
    path: string,
    extras?: LfNavigationExtras
  ): LfMatchingRoute[] {
    path = resolvePath(this.path || '/', path);
    if (process.env.NODE_ENV !== 'production') {
      // Verify that the path corresponds to a valid schema (throw otherwise)
      this.lfStorage.validatePath(path);
    }
    return this._matchingRoutes(path, extras);
  }

  /**
   * Given a list of matching routes, returns the first route that is
   * navigateable and, if necessary, adds missing parameters to the match.
   * Returns `null` when no matching route is navigateable.
   * @param matchingRoutes List of matching routes.
   * @returns The first navigateable route or `null` when none was found.
   */
  public navigateableRouteFromMatchingRoutes(
    matchingRoutes: LfMatchingRoute[]
  ): LfMatchingRoute | null {
    for (const { routes, matchingPath } of matchingRoutes) {
      const navigateablePath = this.navigateablePathOfMatchingPath(
        matchingPath,
        routes[routes.length - 1]
      );
      if (navigateablePath !== null) {
        return { routes, matchingPath: navigateablePath };
      }
    }
    return null;
  }

  /**
   * Given a path, returns the most specific route of its matching routes that
   * is navigateable. Returns `null` when no matching route is navigateable.
   * @param path Path for which to find a matching navigateable route (relative
   * to the current path).
   * @param extras Navigation extras.
   * @returns The most specific navigateable route or `null` when none was
   * found.
   */
  public navigateableRoute(
    path: string,
    extras: LfNavigationExtras = {}
  ): LfMatchingRoute | null {
    return this.navigateableRouteFromMatchingRoutes(
      this.matchingRoutes(path, extras)
    );
  }

  /**
   * Given a navigateable `LfMatchingRoute`, creates a URL tree for the matched
   * Angular path.
   * @param navigateableRoute Information on a navigateable LF route that
   * matched against some path.
   * @param extras Navigation extras.
   * @returns URL tree associated with the provided match.
   */
  public createUrlTreeFromNavigateableRoute(
    navigateableRoute: LfMatchingRoute,
    extras?: LfNavigationExtras
  ): UrlTree {
    const { extraPathAsFragment = true, extraParams = {} } = extras || {};
    const {
      routes,
      matchingPath: { params: matchingParams, extra },
    } = navigateableRoute;
    // Parameters to use when building the URL from the routes' paths
    const params = { ...this.params, ...extraParams, ...matchingParams };
    return this.router.createUrlTree(
      routes.reduce(
        (urlParts, route) => {
          // TODO: Handle routes with no `path` (with a `matcher`, for example)
          // TODO: Handle outlets
          // Ignore empty string paths
          if (route.path) {
            urlParts.push(...this.replaceParams(route.path, params).split('/'));
          }
          return urlParts;
        },
        ['/']
      ),
      {
        ...extras,
        fragment:
          extraPathAsFragment && extra.length > 0 ? extra.join('/') : undefined,
      }
    );
  }

  /**
   * Given a navigateable `LfMatchingRoute`, creates a URL for the matched
   * Angular path.
   * @param navigateableRoute Information on a navigateable LF route that
   * matched against some path.
   * @param extras Navigation extras.
   * @returns URL associated with the provided match.
   */
  public createUrlFromNavigateableRoute(
    navigateableRoute: LfMatchingRoute,
    extras?: LfNavigationExtras
  ): string {
    return this.router.serializeUrl(
      this.createUrlTreeFromNavigateableRoute(navigateableRoute, extras)
    );
  }

  /**
   * Navigates to the URL associated to the provided navigateable
   * `LfMatchingRoute`.
   * @param navigateableRoute Information on a navigateable LF route that
   * matched against some path.
   * @param extras Navigation extras.
   * @returns Promise that resolves to `true`/`false` when the navigation
   * succeeds/fails or is rejected when an error happens.
   */
  public navigateFromNavigateableRoute(
    navigateableRoute: LfMatchingRoute,
    extras?: LfNavigationExtras
  ): Promise<boolean> {
    return this.router.navigateByUrl(
      this.createUrlTreeFromNavigateableRoute(navigateableRoute, extras),
      extras
    );
  }

  /**
   * URL tree associated with a given schema path. Throws an error if no
   * matching navigateable route was found.
   * @param path Schema path for which to fetch the associated URL tree.
   * @param extras Navigation extras.
   * @returns URL tree for the provided schema path.
   */
  public createUrlTree(path: string, extras?: LfNavigationExtras): UrlTree {
    const navigateableRoute = this.navigateableRoute(path, extras);
    if (navigateableRoute === null) {
      throw pathError(path, 'No matching navigateable route found');
    }
    return this.createUrlTreeFromNavigateableRoute(navigateableRoute, extras);
  }

  /**
   * URL associated with a given schema path. Throws an error if no matching
   * navigateable route was found.
   * @param path Schema path for which to fetch associated URL.
   * @param extras Navigation extras.
   * @returns URL for the provided schema path.
   */
  public createUrl(path: string, extras?: LfNavigationExtras): string {
    return this.router.serializeUrl(this.createUrlTree(path, extras));
  }

  /**
   * Navigates to the URL associated to the provided schema path. Throws an
   * error if no matching navigateable route was found.
   * @param path Path whose associated URL we will navigate to.
   * @param extras Navigation extras.
   * @returns Promise that resolves to `true`/`false` when the navigation
   * succeeds/fails or is rejected when an error happens.
   */
  public navigate(path: string, extras?: LfNavigationExtras): Promise<boolean> {
    return this.router.navigateByUrl(this.createUrlTree(path, extras), extras);
  }

  /**
   * Whether the given path is currently active (if it is the currently
   * activated path or a parent of it).
   * @param path Path to check if is active.
   * @param exact Whether to check if the activated path is exactly the one
   * provided (and not simply a child of it).
   * @returns Whether the given path is active.
   */
  public isActive(path: string, exact: boolean = false): boolean {
    if (process.env.NODE_ENV !== 'production') {
      // Verify that the path corresponds to a valid schema (throw otherwise)
      this.lfStorage.validatePath(path);
    }
    return (
      this.path !== null &&
      (exact || path.length === this.path.length
        ? path === this.path
        : isSubpath(path, this.path))
    );
  }

  /**
   * Whether the given path is currently active in respect with the activated
   * path with fragment (if it is the currently activated path with fragment or
   * a parent of it).
   * @param path Path to check if is active.
   * @param exact Whether to check if the activated path with fragment is
   * exactly the one provided (and not simply a child of it).
   * @returns Whether the given path is active in respect with the activated
   * path with fragment.
   */
  public isActiveWithFragment(path: string, exact: boolean = false): boolean {
    if (process.env.NODE_ENV !== 'production') {
      // Verify that the path corresponds to a valid schema (throw otherwise)
      this.lfStorage.validatePath(path);
    }
    return (
      this.pathWithFragment !== null &&
      (exact || path.length === this.pathWithFragment.length
        ? path === this.pathWithFragment
        : isSubpath(path, this.pathWithFragment))
    );
  }

  /**
   * Transforms the Angular router's routes into LF routes and updates the
   * service's inner "relevant" config object. `updateConfig` should be called
   * manually if the Angular router's config is being changed dynamically via
   * `resetConfig`.
   */
  @action
  public updateConfig(): void {
    this.toLfRoutes(this.router.config);
    this.lfRouterConfig = this.relevantRoutes(this.router.config);
    if (process.env.NODE_ENV !== 'production') {
      this.validateRoutes();
    }
  }

  /**
   * Check for LF specific errors in the configured routes (for example, verify
   * that all LF routes have valid LF paths).
   */
  private validateRoutes(routes: LfRoutes = this.lfRouterConfig): void {
    for (const route of routes) {
      if (route.isLfRoute) {
        if (route.lfPath === undefined) {
          throw new Error('LF routes need to define a corresponding `lfPath`');
        }
        // TODO: Verify that the `lfPath` corresponds to a valid schema path. If
        // the path contains parameters, depending on the type of schema, either
        // convert them to a placeholder, or attempt to match with all possible
        // children ids.
      }
      // Validate children (we allow LF routes inside of non-LF routes)
      if (route.children !== undefined) {
        this.validateRoutes(route.children);
      }
    }
  }

  /**
   * Convert a series of Angular routes into routes that the `LfRouter` can
   * understand (LF routes). This is done by setting `isLfRoute` to `true` on
   * each relevant route (a route that is or is a child of the base path) and
   * adding an `lfPath` attribute to each route which maps a given Angular route
   * to the LF schema path it represents. Routes with `isLfRoute` set to `false`
   * and their children are ignored. Wildcard paths (`'**'`) will have
   * `isLfRoute` set to `false` by default. Routes which define their `lfPath`
   * are changed to make their `lfPath` absolute. Example (with the base path
   * set to `'/'`):
   * ```typescript
   * toLfRoutes([
   *   {path: '', redirectTo: 'a', pathMatch: 'full'},
   *   {
   *     path: 'a',
   *     children: [
   *       {path: '', redirectTo: 'aa', pathMatch: 'full'},
   *       {path: 'aa', component: AA},
   *       {path: 'ab', component: AC, lfPath: 'ac'}
   *     ],
   *   },
   *   {path: 'b', component: B},
   *   {path: 'l/:index', component: L},
   *   {path: 'nonLf', component: NonLF, isLfRoute: false},
   *   {path: '**', component: NotFound}
   * ])
   * ```
   * Returns the provided routes, but transformed to the following:
   * ```typescript
   * [
   *   {path: '', redirectTo: 'a', pathMatch: 'full', isLfRoute: true,
   *    lfPath: '/'},
   *   {
   *     path: 'a',
   *     children: [
   *       {path: '', redirectTo: 'aa', pathMatch: 'full', isLfRoute: true,
   *        lfPath: '/a'},
   *       {path: 'aa', component: AA, isLfRoute: true, lfPath: '/a/aa'},
   *       {path: 'ab', component: AC, isLfRoute: true, lfPath: '/a/ac'}
   *     ],
   *     isLfRoute: true,
   *     lfPath: '/a'
   *   },
   *   {path: 'b', component: B, isLfRoute: true, lfPath: '/b'},
   *   {path: 'l/:index', component: F, isLfRoute: true, lfPath: '/l/:index'},
   *   {path: 'nonLf', component: NonLF, isLfRoute: false},
   *   {path: '**', component: NotFound, isLfRoute: false}
   * ]
   * ```
   * @param routes Angular routes to transform into routes that the LF router
   * service understands.
   * @param parentLfPath LF schema path of the parent route of the routes being
   * transformed.
   * @returns Provided routes mutated with LF routing information.
   */
  private toLfRoutes(
    routes: LfRoutes,
    parentPath: string = '/',
    parentLfPath: string = '/'
  ): LfRoutes {
    for (const route of routes) {
      // TODO: Support `matcher` somehow?
      const routePath =
        route.path !== undefined
          ? resolvePath(parentPath, route.path)
          : undefined;
      // Whether this route is relevant (if it is a child of the base path)
      const isRelevant =
        routePath !== undefined &&
        (routePath === this.lfRouterBasePath ||
          isSubpath(this.lfRouterBasePath, routePath));
      // Transform relevant non-LF routes into LF routes
      if (isRelevant) {
        // Allow users to have non-LF routes together with the LF routes by
        // manually setting `isLfRoute` to `false`
        if (route.isLfRoute === undefined) {
          route.isLfRoute = route.path !== '**';
        }
        if (route.isLfRoute) {
          if (route.lfPath === undefined && route.path === undefined) {
            throw new Error(
              'Routes without a `path` must have their ' +
                '`lfPath` set manually'
            );
          }
          // Force the LF path to be absolute
          if (route.lfPath !== undefined) {
            route.lfPath = resolvePath(parentLfPath, route.lfPath);
          } else {
            // Prevent the LF path from including the base path
            const routePathRelativeToBase =
              this.lfRouterBasePath === '/'
                ? routePath!.slice(1)
                : routePath!.slice(this.lfRouterBasePath.length + 1);
            const smallestPath =
              routePathRelativeToBase.length < route.path!.length
                ? routePathRelativeToBase
                : route.path!;
            route.lfPath = resolvePath(parentLfPath, smallestPath);
          }
        }
      }
      // Don't handle children of relevant routes that aren't LF routes
      if (!isRelevant || (isRelevant && route.isLfRoute)) {
        // FIXME: see https://github.com/angular/angular/issues/24069, we must
        // use `_loadedConfig` to access the config of lazy loaded children,
        // which is a non-public Angular API
        const children =
          route.children || ((route as any)._loadedConfig || {}).routes;
        // Handle children
        if (children !== undefined) {
          this.toLfRoutes(children, routePath, route.lfPath);
        }
      }
    }
    return routes;
  }

  /**
   * Routes of the router configuration which are relevant to the `LfRouter`.
   * Ignores non-LF routes with no children that are LF routes. Further
   * simplifies lazy-loaded routes by setting their `children` property with the
   * routes that were lazily fetched.
   * @param routes Routes from which to remove non-LF related routes.
   * @param parentPath Path of the route that precedes the `routes` being
   * handled.
   * @returns Copy of passed routes with non-LF routes removed and lazy-loaded
   * routes simplified.
   */
  private relevantRoutes(routes: LfRoutes, parentPath: string = '/'): LfRoutes {
    return routes.reduce((relevantRoutes: LfRoutes, route: LfRoute) => {
      // TODO: Support `matcher` somehow?
      const routePath =
        route.path !== undefined
          ? resolvePath(parentPath, route.path)
          : undefined;
      // FIXME: see https://github.com/angular/angular/issues/24069, we must
      // use `_loadedConfig` to access the config of lazy loaded children,
      // which is a non-public Angular API
      let children: LfRoutes | undefined =
        route.children || ((route as any)._loadedConfig || {}).routes;
      children && (children = this.relevantRoutes(children, routePath));
      // A relevant route is either a leaf LF route whose Angular router path
      // equals the LF router base path or is a child of it, or a route which
      // has one such child
      if (
        (route.isLfRoute &&
          !children &&
          routePath !== undefined &&
          (this.lfRouterBasePath === routePath ||
            isSubpath(this.lfRouterBasePath, routePath))) ||
        (children && children.length > 0)
      ) {
        // Use a copy of the route with only relevant children (we make
        // lazy-loaded routes similar to regular routes by using the
        // `children` attribute)
        relevantRoutes.push({ ...route, children });
      }
      return relevantRoutes;
    }, [] as LfRoutes);
  }

  /**
   * Whether a given route can activate for a matching path depending on its
   * `lfCanActivate` function.
   * @param route Route to check if can be activated.
   * @param path Path that matches the given route.
   * @returns Whether the given route can be activated.
   */
  private routeCanActivate(route: LfRoute, path: string): boolean {
    const canActivateFn =
      route.lfCanActivate ||
      (this.options && this.options.defaultCanActivate) ||
      defaultLfCanActivate;
    return canActivateFn(this.lfStorage.relativeStorage(path));
  }

  /**
   * Returns the LF routes that match a given path, ordered from most specific
   * to least specific.
   * @param path Path to match against the routes.
   * @param extras Navigation extras.
   * @param routes Routes from which to fetch a matching route (router
   * configuration by default).
   * @returns Routes matching the provided path.
   */
  private _matchingRoutes(
    path: string,
    extras: LfNavigationExtras = {},
    routes: LfRoutes = this.lfRouterConfig
  ): LfMatchingRoute[] {
    const { navigateToChild = true, navigateToParent = true } = extras;
    // Find all routes whose `lfPath` matches the provided path (depending on
    // `navigateToChild`/`navigateToParent`).
    const matchingRoutes: LfMatchingRoute[] = [];
    for (const route of routes) {
      // Handle the route's children first (children matches should come before
      // parent matches in the `matchingRoutes` array)
      if (route.children !== undefined) {
        const matchingChildren = this._matchingRoutes(
          path,
          extras,
          route.children
        );
        for (const matchingChild of matchingChildren) {
          matchingChild.routes = [route, ...matchingChild.routes];
        }
        matchingRoutes.push(...matchingChildren);
      }
      // Add the route as a match when appropriate
      if (route.isLfRoute) {
        const matchingPath = this.matchPaths(route.lfPath!, path);
        if (
          matchingPath !== null &&
          (navigateToChild || matchingPath.missing.length === 0) &&
          (navigateToParent || matchingPath.extra.length === 0)
        ) {
          matchingRoutes.push({ routes: [route], matchingPath });
        }
      }
    }
    // Sort matching routes from most specific to least specific, in case of
    // equal specificity, opt for the order in which they appear in the router
    // configuration; NOTE: this assumes that the sorting algorithm is stable,
    // which should be the case for all recent browsers for arrays with less
    // than 512 elements. See: https://mathiasbynens.be/demo/sort-stability. If
    // the need arises, it should be easy to force stability by adding an index
    // to each matching route
    return matchingRoutes.sort((match1, match2) => {
      const {
        matching: ma1,
        missing: miss1,
        extra: ext1,
      } = match1.matchingPath!;
      const {
        matching: ma2,
        missing: miss2,
        extra: ext2,
      } = match2.matchingPath!;
      return (
        ma2.length - ma1.length ||
        miss1.length - miss2.length ||
        ext1.length - ext2.length
      );
    });
  }

  /**
   * Returns whether a given path partially matches against an LF route path.
   * Returns `null` when the two paths don't match or, when the two paths do
   * match, an object containing the matching parts, matching parameters,
   * missing parts of the LF route path that didn't match, or extra parts of the
   * given path which didn't match.
   * @param routePath Path of the LF route.
   * @param path Path to check if it matches against the LF route path.
   * @returns Matching between an LF route path and a provided path.
   */
  private matchPaths(routePath: string, path: string): MatchingPath | null {
    const routePathArray = resolvePathToArray('/', routePath);
    const pathArray = resolvePathToArray('/', path);
    const commonLength = Math.min(routePathArray.length, pathArray.length);
    const params: Params = {};
    // Verify that all common parts match and build `params`
    for (let i = 0; i < commonLength; ++i) {
      const routePathPart = routePathArray[i];
      const pathPart = pathArray[i];
      if (routePathPart[0] === ':') {
        params[routePathPart.slice(1)] = pathPart;
      } else if (routePathPart !== pathPart) {
        return null;
      }
    }
    // Build result
    return {
      matching: pathArray.slice(0, commonLength),
      params,
      missing: routePathArray.slice(commonLength),
      extra: pathArray.slice(commonLength),
    };
  }

  /**
   * Given a matching of paths, returns a version of the match where the
   * matching path is navigateable and without missing parts or `null` when no
   * such match can be found.
   * @param matchingPath Matching between two paths, possibly with `missing`
   * parts.
   * @param route Route where such matching occurred which may possibly define
   * an `lfCanActivate`.
   * @returns Matching of paths where the matching path is navigateable and with
   * missing parts, or `null` when no such matching exists.
   */
  private navigateablePathOfMatchingPath(
    matchingPath: MatchingPath,
    route: LfRoute
  ): MatchingPath | null {
    const { matching, params, missing, extra } = matchingPath;
    if (missing.length === 0) {
      return this.routeCanActivate(route, arrayToPath(matching))
        ? matchingPath
        : null;
    }
    // Resolve missing path
    const currentMatching = [...matching];
    let i = 0;
    for (; i < missing.length; ++i) {
      let missingPart = missing[i];
      // Attempt to resolve a param using the existing params, if impossible,
      // break the loop
      if (missingPart[0] === ':') {
        const param = params[missingPart.slice(1)];
        if (param === undefined) {
          break;
        }
        missingPart = param;
      }
      currentMatching.push(missingPart);
    }
    if (i === missing.length) {
      return this.navigateablePathOfMatchingPath(
        { matching: currentMatching, params, missing: [], extra },
        route
      );
    }
    // If a parameter that couldn't be resolved was found, then we must attempt
    // to resolve it using the known children of its parent path
    const missingParam = missing[i].slice(1);
    try {
      const childrenIds = this.lfStorage.childrenIds(
        arrayToPath(currentMatching)
      );
      for (const id of childrenIds) {
        const childMatchingPath = this.navigateablePathOfMatchingPath(
          {
            matching: [...currentMatching, id.toString()],
            params: { ...params, [missingParam]: id.toString() },
            missing: missing.slice(i + 1),
            extra,
          },
          route
        );
        if (childMatchingPath !== null) {
          return childMatchingPath;
        }
      }
    } catch (err) {}
    return null;
  }

  /**
   * Replaces the parameters of a given path with whatever value they have
   * within the `params` record. Throws an error in the event of a missing
   * parameter.
   * @param path Path with parameters to replace.
   * @param params Params to replace on the path.
   * @returns Path with parameters replaced with the respective values.
   */
  private replaceParams(path: string, params: Params): string {
    return path
      .split('/')
      .map((part) => {
        if (part[0] === ':') {
          const param = part.slice(1);
          const value = params[param];
          if (value === undefined) {
            throw new Error(`Missing parameter: ${param}`);
          }
          return value;
        }
        return part;
      })
      .join('/');
  }

  /**
   * Saves the index of the current path's id in respect to the list of children
   * ids of its parent.
   */
  private saveIdIndex(): void {
    if (this.path === '/') {
      this.idIndex = -1;
    } else {
      try {
        this.idIndex = this.lfStorage
          .childrenIds(`${this.path}/..`) // May throw when parent doesn't exist
          .indexOf(this.lfStorage.id(this.path!)!);
      } catch (err) {
        this.idIndex = -1;
      }
    }
  }

  /**
   * Finds the previous navigateable route in respect to the current route
   * (which has become invalid).
   * @returns Previous navigateable route.
   */
  private previousNavigateablePath(): LfMatchingRoute | null {
    const parentPath = this.lfStorage.resolvePath(`${this.path}/..`);
    if (this.idIndex !== -1) {
      try {
        const childrenIds = this.lfStorage.childrenIds(parentPath); // May throw
        for (
          let i = Math.min(this.idIndex, childrenIds.length - 1);
          i >= 0;
          --i
        ) {
          const childId = childrenIds[i];
          const nextPath = appendToPath(parentPath, childId);
          const route = this.navigateableRoute(nextPath);
          if (route !== null) {
            return route;
          }
        }
      } catch (err) {}
    }
    return this.path === '/'
      ? null
      : this.navigateableRoute(parentPath, { navigateToChild: false });
  }
}
