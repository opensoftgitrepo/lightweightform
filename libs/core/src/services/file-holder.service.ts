import { Injectable } from '@angular/core';

/**
 * Service used to hold files associated with storage paths. A typical scenario
 * would have a string in the storage represent the name of a file to upload
 * (selected via a file input). This service is used to "hold" the file to later
 * upload. On upload, this service can then be queried to create a `FormData`
 * instance to be sent to a server.
 */
@Injectable()
export class LfFileHolder implements Map<string, File> {
  /**
   * Files being held.
   */
  private files: Map<string, File> = new Map();

  public [Symbol.toStringTag] = 'LfFileHolder';

  public get size(): number {
    return this.files.size;
  }

  public get(path: string): File | undefined {
    return this.files.get(path);
  }

  public set(path: string, file: File): this {
    this.files.set(path, file);
    return this;
  }

  public has(path: string): boolean {
    return this.files.has(path);
  }

  public delete(path: string): boolean {
    return this.files.delete(path);
  }

  public clear(): void {
    this.files.clear();
  }

  public forEach(
    callbackfn: (file: File, path: string, map: Map<string, File>) => void,
    thisArg?: any
  ): void {
    this.files.forEach((file, path) => callbackfn(file, path, this), thisArg);
  }

  public [Symbol.iterator](): IterableIterator<[string, File]> {
    return this.entries();
  }

  public entries(): IterableIterator<[string, File]> {
    return this.files.entries();
  }

  public keys(): IterableIterator<string> {
    return this.files.keys();
  }

  public values(): IterableIterator<File> {
    return this.files.values();
  }
}
