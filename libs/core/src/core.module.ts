import { CommonModule } from '@angular/common';
import { APP_INITIALIZER, NgModule, Optional, Provider } from '@angular/core';

import { I18nDirective } from './directives/i18n.directive';
import { ValueDirective } from './directives/value.directive';
import { LfFileHolder } from './services/file-holder.service';
import { LfFileStorage } from './services/file-storage.service';
import { LfI18n } from './services/i18n.service';
import { LfRouter } from './services/router.service';
import { LfStorage } from './services/storage.service';
import { LfUnloadAlert } from './services/unload-alert.service';
import { configureMobX, MOBX_CONFIG } from './utils/configure-mobx';
import { setUpSpyOnMobX } from './utils/spy-on-mobx';

/**
 * Exported/declared components and directives.
 */
const DECLARATIONS = [I18nDirective, ValueDirective];

/**
 * LF core module.
 */
@NgModule({
  imports: [CommonModule],
  declarations: DECLARATIONS,
  exports: DECLARATIONS,
  providers: [
    // Configure MobX on app initialisation
    {
      provide: APP_INITIALIZER,
      multi: true,
      deps: [[new Optional(), MOBX_CONFIG]],
      useFactory: (config) => () => configureMobX(config),
    },
    // Set up MobX spying when in development
    {
      provide: APP_INITIALIZER,
      multi: true,
      deps: [],
      useFactory: () => () => setUpSpyOnMobX(),
    },
  ],
})
export class LfCoreModule {}

/**
 * Core services that should be provided for each provided `LF_APP_SCHEMA`. They
 * provide, among others, a storage instance based on the provided application
 * schema.
 */
export const LF_CORE_SERVICES: Provider = [
  LfStorage,
  LfI18n,
  LfRouter,
  LfFileHolder,
  LfFileStorage,
  LfUnloadAlert,
];
