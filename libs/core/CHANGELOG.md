# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.13.4"></a>

## [4.13.4](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.13.3&sourceBranch=refs%2Ftags%2Fv4.13.4) (2022-05-12)

### Bug Fixes

- **core:** typecheck loaded values
  ([01de469](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/01de469))

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.13.3"></a>

## [4.13.3](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.13.2&sourceBranch=refs%2Ftags%2Fv4.13.3) (2022-03-16)

### Bug Fixes

- **core:** access languages in a mobx friendly way
  ([e1e339c](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/e1e339c))

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.13.2"></a>

## [4.13.2](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.13.1&sourceBranch=refs%2Ftags%2Fv4.13.2) (2022-02-14)

### Bug Fixes

- **core:** provide way of changing MobX config
  ([ba70a94](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/ba70a94))

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.13.1"></a>

## [4.13.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.13.0&sourceBranch=refs%2Ftags%2Fv4.13.1) (2022-02-03)

### Bug Fixes

- default to "ifavailable" MobX proxy config
  ([fc16532](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/fc16532))

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.13.0"></a>

# [4.13.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.12.2&sourceBranch=refs%2Ftags%2Fv4.13.0) (2022-01-27)

### Features

- update dependencies
  ([f921338](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/f921338))

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.12.2"></a>

## [4.12.2](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.12.1&sourceBranch=refs%2Ftags%2Fv4.12.2) (2021-10-27)

**Note:** Version bump only for package @lightweightform/core

<a name="4.12.1"></a>

## [4.12.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.12.0&sourceBranch=refs%2Ftags%2Fv4.12.1) (2021-10-27)

**Note:** Version bump only for package @lightweightform/core

<a name="4.12.0"></a>

# [4.12.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.11.2&sourceBranch=refs%2Ftags%2Fv4.12.0) (2021-10-26)

### Features

- **storage:** support custom codes in standard issues
  ([09c6abf](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/09c6abf))

<a name="4.11.2"></a>

## [4.11.2](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.11.1&sourceBranch=refs%2Ftags%2Fv4.11.2) (2021-10-25)

### Bug Fixes

- **core:** fix load sometimes not working
  ([a4c9d0b](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/a4c9d0b))

<a name="4.11.1"></a>

## [4.11.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.11.0&sourceBranch=refs%2Ftags%2Fv4.11.1) (2021-10-22)

**Note:** Version bump only for package @lightweightform/core

<a name="4.11.0"></a>

# [4.11.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.10.2&sourceBranch=refs%2Ftags%2Fv4.11.0) (2021-10-20)

### Features

- support "manual" validation
  ([d2e1aff](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/d2e1aff))

<a name="4.10.1"></a>

## [4.10.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.10.0&sourceBranch=refs%2Ftags%2Fv4.10.1) (2021-08-22)

### Bug Fixes

- **bootstrap-theme:** fix null refs
  ([774af0f](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/774af0f))
- **core:** attempt at fixing failures loading
  ([6c6e7c5](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/6c6e7c5))

<a name="4.9.1"></a>

## [4.9.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.9.0&sourceBranch=refs%2Ftags%2Fv4.9.1) (2021-07-11)

**Note:** Version bump only for package @lightweightform/core

<a name="4.8.3"></a>

## [4.8.3](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.8.2&sourceBranch=refs%2Ftags%2Fv4.8.3) (2021-06-22)

### Bug Fixes

- **core:** more fixes/improvements to file loading
  ([e2be461](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/e2be461))

<a name="4.8.2"></a>

## [4.8.2](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.8.1&sourceBranch=refs%2Ftags%2Fv4.8.2) (2021-06-22)

### Bug Fixes

- **core:** hotfix for loading not working: inc timeout to 1s
  ([d2fbb0f](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/d2fbb0f))

<a name="4.8.1"></a>

## [4.8.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.8.0&sourceBranch=refs%2Ftags%2Fv4.8.1) (2021-06-21)

### Bug Fixes

- **core:** loading not working on some platforms
  ([d736183](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/d736183))

<a name="4.8.0"></a>

# [4.8.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.7.5&sourceBranch=refs%2Ftags%2Fv4.8.0) (2021-06-19)

### Features

- async actions + detect load cancel
  ([34998c6](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/34998c6))

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.7.0"></a>

# [4.7.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.6.1&sourceBranch=refs%2Ftags%2Fv4.7.0) (2020-12-15)

### Features

- **bootstrap-theme:** introduce lf-file
  ([dde03e1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/dde03e1))

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.5.4"></a>

## [4.5.4](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.5.3&sourceBranch=refs%2Ftags%2Fv4.5.4) (2020-10-28)

### Bug Fixes

- **core:** prevent base path in LF route path
  ([f950425](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/f950425)),
  closes #51

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.5.2"></a>

## [4.5.2](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.5.1&sourceBranch=refs%2Ftags%2Fv4.5.2) (2020-02-19)

**Note:** Version bump only for package @lightweightform/core

<a name="4.5.1"></a>

## [4.5.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.5.0&sourceBranch=refs%2Ftags%2Fv4.5.1) (2020-02-19)

### Bug Fixes

- peer dependencies
  ([b212247](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/b212247))

<a name="4.5.0"></a>

# [4.5.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.4.6&sourceBranch=refs%2Ftags%2Fv4.5.0) (2020-02-19)

### Features

- support Angular 9
  ([54a7154](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/54a7154))

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.4.5"></a>

## [4.4.5](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.4.4&sourceBranch=refs%2Ftags%2Fv4.4.5) (2020-01-23)

**Note:** Version bump only for package @lightweightform/core

<a name="4.4.4"></a>

## [4.4.4](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.4.3&sourceBranch=refs%2Ftags%2Fv4.4.4) (2020-01-22)

**Note:** Version bump only for package @lightweightform/core

<a name="4.4.3"></a>

## [4.4.3](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.4.2&sourceBranch=refs%2Ftags%2Fv4.4.3) (2020-01-19)

**Note:** Version bump only for package @lightweightform/core

<a name="4.4.2"></a>

## [4.4.2](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.4.1&sourceBranch=refs%2Ftags%2Fv4.4.2) (2020-01-18)

**Note:** Version bump only for package @lightweightform/core

<a name="4.4.1"></a>

## [4.4.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.4.0&sourceBranch=refs%2Ftags%2Fv4.4.1) (2020-01-14)

### Bug Fixes

- repo-wide spellchecks + small improvements
  ([90545fb](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/90545fb))

<a name="4.2.0"></a>

# [4.2.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.1.0&sourceBranch=refs%2Ftags%2Fv4.2.0) (2019-12-08)

### Features

- support AoT compilation
  ([bd9e886](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/bd9e886)),
  closes #43

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.1.0"></a>

# [4.1.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.0.1&sourceBranch=refs%2Ftags%2Fv4.1.0) (2019-11-10)

### Features

- **core:** support custom encoding when saving
  ([3c95169](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/3c95169))

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.0.1"></a>

## [4.0.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.0.0&sourceBranch=refs%2Ftags%2Fv4.0.1) (2019-10-22)

### Bug Fixes

- **core:** query params should not disappear
  ([cef41ea](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/cef41ea))

<a name="4.0.0"></a>

# [4.0.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.0.0-beta.19&sourceBranch=refs%2Ftags%2Fv4.0.0) (2019-10-19)

Initial LF 4 release.
