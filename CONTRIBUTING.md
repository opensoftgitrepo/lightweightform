# Contributing

Contributions are always welcome. Feel free to create a new issue in
Lightweightform's
[issue tracker](https://bitbucket.org/opensoftgitrepo/lightweightform/issues?status=new&status=open)
or to submit a pull request against the `develop` branch.

## Project structure

Lightweightform follows a monorepo approach, all officially maintained modules
are kept in the same repository. We use the
[Angular CLI](https://cli.angular.io/) and
[Lerna](https://github.com/lerna/lerna) to manage the monorepo.

## Developing

### Prerequisites:

- [Node.js](https://nodejs.org/)

### Setup

On the repository's root, run:

```bash
npm install
npm run build
```

For building automatically after changes and launching the example applications
in local servers, run:

```bash
npm run dev
```

- The documentation (itself a Lightweightform application) may then be accessed
  at: https://localhost:3000.
- The Ireland Census example application may then be accessed at:
  https://localhost:3001.

### Code formatting and testing

All code should be formatted via [Prettier](https://prettier.io/) and properly
linted. Git hooks should run prettier and lint changed files after each commit;
however it is still possible to run the commands manually.

Format the codebase and attempt to fix linting issues with:

```bash
npm run format
```

Simply lint the codebase by running:

```bash
npm run lint
```

To run all tests, execute:

```bash
npm test
```
