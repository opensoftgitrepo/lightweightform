# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.13.4"></a>

## [4.13.4](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.13.3&sourceBranch=refs%2Ftags%2Fv4.13.4) (2022-05-12)

### Bug Fixes

- **core:** typecheck loaded values
  ([01de469](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/01de469))

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.13.3"></a>

## [4.13.3](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.13.2&sourceBranch=refs%2Ftags%2Fv4.13.3) (2022-03-16)

### Bug Fixes

- **core:** access languages in a mobx friendly way
  ([e1e339c](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/e1e339c))
- **xml-serializer:** comply with `xs:boolean` when deserialising booleans
  ([e1cb373](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/e1cb373))

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.13.2"></a>

## [4.13.2](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.13.1&sourceBranch=refs%2Ftags%2Fv4.13.2) (2022-02-14)

### Bug Fixes

- **core:** provide way of changing MobX config
  ([ba70a94](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/ba70a94))

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.13.1"></a>

## [4.13.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.13.0&sourceBranch=refs%2Ftags%2Fv4.13.1) (2022-02-03)

### Bug Fixes

- **bootstrap-theme:** fix validation issue links
  ([b4981ac](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/b4981ac))
- **bootstrap-theme:** spacing between data-range calendars
  ([0a2e576](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/0a2e576))
- default to "ifavailable" MobX proxy config
  ([fc16532](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/fc16532))
- **docs:** fix too eager replace
  ([6586a0a](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/6586a0a))
- **docs:** remove accidental margin on tabs
  ([7f2d7ce](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/7f2d7ce))

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.13.0"></a>

# [4.13.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.12.2&sourceBranch=refs%2Ftags%2Fv4.13.0) (2022-01-27)

### Bug Fixes

- **theme-common:** type error in schematics
  ([21636df](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/21636df))
- **theme-common:** type error in schematics
  ([1dad5f4](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/1dad5f4))

### Features

- update dependencies
  ([f921338](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/f921338))

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.12.2"></a>

## [4.12.2](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.12.1&sourceBranch=refs%2Ftags%2Fv4.12.2) (2021-10-27)

### Bug Fixes

- **storage:** data type of 'is required' issue
  ([37e704e](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/37e704e))

<a name="4.12.1"></a>

## [4.12.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.12.0&sourceBranch=refs%2Ftags%2Fv4.12.1) (2021-10-27)

### Bug Fixes

- **bootstrap-theme:** small improvement to date-range validation
  ([9239be5](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/9239be5))
- issue types + date-range
  ([cbab3a2](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/cbab3a2))

<a name="4.12.0"></a>

# [4.12.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.11.2&sourceBranch=refs%2Ftags%2Fv4.12.0) (2021-10-26)

### Features

- **storage:** support custom codes in standard issues
  ([09c6abf](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/09c6abf))

<a name="4.11.2"></a>

## [4.11.2](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.11.1&sourceBranch=refs%2Ftags%2Fv4.11.2) (2021-10-25)

### Bug Fixes

- **core:** fix load sometimes not working
  ([a4c9d0b](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/a4c9d0b))

<a name="4.11.1"></a>

## [4.11.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.11.0&sourceBranch=refs%2Ftags%2Fv4.11.1) (2021-10-22)

### Bug Fixes

- **bootstrap-theme:** prevent access to state props in manual mode
  ([64588a8](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/64588a8))

<a name="4.11.0"></a>

# [4.11.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.10.2&sourceBranch=refs%2Ftags%2Fv4.11.0) (2021-10-20)

### Bug Fixes

- **bootstrap-theme:** fix lf:// links in issue messages
  ([d1b389a](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/d1b389a))

### Features

- support "manual" validation
  ([d2e1aff](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/d2e1aff))

<a name="4.10.2"></a>

## [4.10.2](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.10.1&sourceBranch=refs%2Ftags%2Fv4.10.2) (2021-09-13)

### Bug Fixes

- **theme-common:** fix build issue
  ([83651be](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/83651be))
- **theme-common:** table column insertion order
  ([a7352e3](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/a7352e3))

<a name="4.10.1"></a>

## [4.10.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.10.0&sourceBranch=refs%2Ftags%2Fv4.10.1) (2021-08-22)

### Bug Fixes

- **bootstrap-theme:** fix null refs
  ([774af0f](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/774af0f))
- **core:** attempt at fixing failures loading
  ([6c6e7c5](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/6c6e7c5))

<a name="4.10.0"></a>

# [4.10.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.9.2&sourceBranch=refs%2Ftags%2Fv4.10.0) (2021-07-14)

### Features

- **theme-common:** support `minWidth` array
  ([d1c9b13](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/d1c9b13))

<a name="4.9.2"></a>

## [4.9.2](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.9.1&sourceBranch=refs%2Ftags%2Fv4.9.2) (2021-07-11)

### Bug Fixes

- **bootstrap-theme:** fix incorrect import
  ([a6651b8](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/a6651b8))

<a name="4.9.1"></a>

## [4.9.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.9.0&sourceBranch=refs%2Ftags%2Fv4.9.1) (2021-07-11)

### Bug Fixes

- **storage:** relax type checking to allow any object-like
  ([ea558c0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/ea558c0))

<a name="4.9.0"></a>

# [4.9.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.8.3&sourceBranch=refs%2Ftags%2Fv4.9.0) (2021-07-05)

### Bug Fixes

- **bootstrap-theme:** touch inner date-range values
  ([d1d8757](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/d1d8757))

### Features

- **theme-common:** colspan support on columns
  ([e9b131b](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/e9b131b))

<a name="4.8.3"></a>

## [4.8.3](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.8.2&sourceBranch=refs%2Ftags%2Fv4.8.3) (2021-06-22)

### Bug Fixes

- **core:** more fixes/improvements to file loading
  ([e2be461](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/e2be461))

<a name="4.8.2"></a>

## [4.8.2](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.8.1&sourceBranch=refs%2Ftags%2Fv4.8.2) (2021-06-22)

### Bug Fixes

- **core:** hotfix for loading not working: inc timeout to 1s
  ([d2fbb0f](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/d2fbb0f))

<a name="4.8.1"></a>

## [4.8.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.8.0&sourceBranch=refs%2Ftags%2Fv4.8.1) (2021-06-21)

### Bug Fixes

- **core:** loading not working on some platforms
  ([d736183](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/d736183))

<a name="4.8.0"></a>

# [4.8.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.7.5&sourceBranch=refs%2Ftags%2Fv4.8.0) (2021-06-19)

### Features

- async actions + detect load cancel
  ([34998c6](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/34998c6))

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.7.5"></a>

## [4.7.5](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.7.4&sourceBranch=refs%2Ftags%2Fv4.7.5) (2021-03-10)

### Bug Fixes

- **theme-common:** make addNgModuleImport receive opt importExpression
  ([589f54e](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/589f54e)),
  closes #52

<a name="4.7.4"></a>

## [4.7.4](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.7.3&sourceBranch=refs%2Ftags%2Fv4.7.4) (2021-02-06)

### Bug Fixes

- **bootstrap-theme:** file across multiple forms
  ([9d06459](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/9d06459))
- **bootstrap-theme:** file across multiple forms
  ([4ac1f19](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/4ac1f19))

<a name="4.7.3"></a>

## [4.7.3](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.7.2&sourceBranch=refs%2Ftags%2Fv4.7.3) (2020-12-16)

### Bug Fixes

- **bootstrap-theme:** lf-file with long filenames
  ([4f90adb](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/4f90adb))
- **bootstrap-theme:** reset file on invalid size
  ([0ebf5cb](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/0ebf5cb))

<a name="4.7.2"></a>

## [4.7.2](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.7.1&sourceBranch=refs%2Ftags%2Fv4.7.2) (2020-12-15)

### Bug Fixes

- **bootstrap-theme:** file input title
  ([2df0302](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/2df0302))

<a name="4.7.1"></a>

## [4.7.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.7.0&sourceBranch=refs%2Ftags%2Fv4.7.1) (2020-12-15)

### Bug Fixes

- **bootstrap-theme:** export FileComponent
  ([a17d476](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/a17d476))
- **bootstrap-theme:** fix initial file
  ([bca8465](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/bca8465))

<a name="4.7.0"></a>

# [4.7.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.6.1&sourceBranch=refs%2Ftags%2Fv4.7.0) (2020-12-15)

### Features

- **bootstrap-theme:** introduce lf-file
  ([dde03e1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/dde03e1))

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.6.1"></a>

## [4.6.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.6.0&sourceBranch=refs%2Ftags%2Fv4.6.1) (2020-11-09)

### Bug Fixes

- **bootstrap-theme:** export es locale
  ([49c971d](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/49c971d))

<a name="4.6.0"></a>

# [4.6.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.5.4&sourceBranch=refs%2Ftags%2Fv4.6.0) (2020-11-09)

### Features

- **bootstrap-theme:** add spanish locale
  ([429836b](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/429836b))

<a name="4.5.4"></a>

## [4.5.4](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.5.3&sourceBranch=refs%2Ftags%2Fv4.5.4) (2020-10-28)

### Bug Fixes

- **core:** prevent base path in LF route path
  ([f950425](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/f950425)),
  closes #51

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.5.3"></a>

## [4.5.3](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.5.2&sourceBranch=refs%2Ftags%2Fv4.5.3) (2020-06-09)

### Bug Fixes

- **bootstrap-theme:** lf-number in virtual scroll
  ([80aa6ce](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/80aa6ce))

<a name="4.5.2"></a>

## [4.5.2](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.5.1&sourceBranch=refs%2Ftags%2Fv4.5.2) (2020-02-19)

### Bug Fixes

- **bootstrap-theme:** don't install optional deps
  ([a3550ce](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/a3550ce))

<a name="4.5.1"></a>

## [4.5.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.5.0&sourceBranch=refs%2Ftags%2Fv4.5.1) (2020-02-19)

### Bug Fixes

- **bootstrap-theme:** update schematics for ng 9
  ([6c6c4bc](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/6c6c4bc))
- peer dependencies
  ([b212247](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/b212247))

<a name="4.5.0"></a>

# [4.5.0](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.4.6&sourceBranch=refs%2Ftags%2Fv4.5.0) (2020-02-19)

### Bug Fixes

- **bootstrap-theme:** radio styles and name
  ([ef8f8c4](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/ef8f8c4))

### Features

- support Angular 9
  ([54a7154](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/54a7154))

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.4.6"></a>

## [4.4.6](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.4.5&sourceBranch=refs%2Ftags%2Fv4.4.6) (2020-02-13)

### Bug Fixes

- **bootstrap-theme:** date timezone issues
  ([0474a0a](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/0474a0a))
- **bootstrap-theme:** dates within tables
  ([0f0ae74](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/0f0ae74)),
  closes #49

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.4.5"></a>

## [4.4.5](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.4.4&sourceBranch=refs%2Ftags%2Fv4.4.5) (2020-01-23)

### Bug Fixes

- **bootstrap-theme:** date improvements
  ([59134d8](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/59134d8))
- **bootstrap-theme:** don't show required icon when read-only
  ([14712c9](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/14712c9))
- **bootstrap-theme:** fix typings in table
  ([c78f822](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/c78f822))
- **bootstrap-theme:** read-only checkbox style
  ([1d7b2c2](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/1d7b2c2))

<a name="4.4.4"></a>

## [4.4.4](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.4.3&sourceBranch=refs%2Ftags%2Fv4.4.4) (2020-01-22)

### Bug Fixes

- fix lint on apps
  ([f48ddd3](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/f48ddd3))
- **bootstrap-theme:** don't set when number is computed
  ([616fb2d](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/616fb2d))
- **docs:** example code reference
  ([26edd0f](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/26edd0f))

<a name="4.4.3"></a>

## [4.4.3](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.4.2&sourceBranch=refs%2Ftags%2Fv4.4.3) (2020-01-19)

### Bug Fixes

- **bootstrap-theme:** properly support min/maxDate
  ([8ad077f](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/8ad077f))
- **bootstrap-theme:** schematics collection columnLabels
  ([eda9070](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/eda9070))
- **bootstrap-theme:** schematics import computed from mobx-angular
  ([4781a43](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/4781a43))

<a name="4.4.2"></a>

## [4.4.2](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.4.1&sourceBranch=refs%2Ftags%2Fv4.4.2) (2020-01-18)

### Bug Fixes

- **bootstrap-theme:** date-range touched on component start
  ([6821784](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/6821784))
- **bootstrap-theme:** readd Popper.js as peerDep
  ([65b64f1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/65b64f1))

<a name="4.4.1"></a>

## [4.4.1](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/compare/diff?targetBranch=refs%2Ftags%2Fv4.4.0&sourceBranch=refs%2Ftags%2Fv4.4.1) (2020-01-14)

### Bug Fixes

- repo-wide spellchecks + small improvements
  ([90545fb](https://bitbucket.org/projects/opensoftgitrepo/repos/lightweightform/commits/90545fb))

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.4.0"></a>

# [4.4.0](/compare/diff?targetBranch=refs%2Ftags%2Fv4.3.0&sourceBranch=refs%2Ftags%2Fv4.4.0) (2020-01-06)

### Bug Fixes

- **bootstrap-theme:** limit lf-number int min/max (395d4a9)

### Features

- **theme-common:** schematic utils improvements (789b49c)

<a name="4.3.0"></a>

# [4.3.0](/compare/diff?targetBranch=refs%2Ftags%2Fv4.2.0&sourceBranch=refs%2Ftags%2Fv4.3.0) (2019-12-26)

### Features

- **numeric-input:** introduce numeric-input (7ee28ff), closes #44

<a name="4.2.0"></a>

# [4.2.0](/compare/diff?targetBranch=refs%2Ftags%2Fv4.1.0&sourceBranch=refs%2Ftags%2Fv4.2.0) (2019-12-08)

### Bug Fixes

- force MobX version 5.14.2 (68817b5)

### Features

- support AoT compilation (bd9e886), closes #43

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.1.0"></a>

# [4.1.0](/compare/diff?targetBranch=refs%2Ftags%2Fv4.0.1&sourceBranch=refs%2Ftags%2Fv4.1.0) (2019-11-10)

### Bug Fixes

- **bootstrap-theme:** fix schematics on Windows (c7f3575)
- **bootstrap-theme:** form schematic on Windows (a6c611a)
- **bootstrap-theme:** remove unused import (a37be25)
- **docs:** API JSON import (ad08ebc)

### Features

- **core:** support custom encoding when saving (3c95169)

# Change Log

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="4.0.1"></a>

## [4.0.1](/compare/diff?targetBranch=refs%2Ftags%2Fv4.0.0&sourceBranch=refs%2Ftags%2Fv4.0.1) (2019-10-22)

### Bug Fixes

- **core:** query params should not disappear (cef41ea)

<a name="4.0.0"></a>

# [4.0.0](/compare/diff?targetBranch=refs%2Ftags%2Fv4.0.0-beta.19&sourceBranch=refs%2Ftags%2Fv4.0.0) (2019-10-19)

Initial LF 4 release.
