# Lightweightform

[![npm version](https://img.shields.io/npm/v/@lightweightform/bootstrap-theme.svg)](https://www.npmjs.com/package/@lightweightform/bootstrap-theme)
[![lerna](https://img.shields.io/badge/Maintained%20with-Lerna-cc00ff.svg)](https://lernajs.io/)
[![License: Apache-2.0](https://img.shields.io/badge/License-Apache--2.0-yellowgreen.svg)](./LICENSE)

[Lightweightform](https://lightweightform.io) (or LF for short) is an
Angular-based open-source library that simplifies the development of demanding
web forms. This repository contains the source code, documentation, and examples
powering Lightweightform.

## Why was Lightweightform created?

It all started with a few challenges:

- How to build demanding web forms (possibly with more than a thousand fields
  each) as single page applications?
- How to handle and validate data provided by millions of users?
- How to effectively maintain and evolve multiple of these web forms, possibly
  all under a single design language?

With these challenges in mind, we started to work on a library that attempts to
deal with most of the low-level aspects of building web forms whilst allowing
developers to spend their time on what matters most: the business logic (or
form-specific logic). The result was a powerful solution to build demanding
forms in a quick and simple way: Lightweightform.

## Goals of Lightweightform

### Extensibility and configurability

Lightweightform works under the pretext of "themes". To LF, a theme is a package
which provides a set of Angular components that developers can use to build
their web form applications. Themes should be built on top of
[LF's core](https://www.npmjs.com/package/@lightweightform/core).

We provide an "official" theme under the name of Bootstrap theme, available as
[`@lightweightform/bootstrap-theme`](https://www.npmjs.com/package/@lightweightform/bootstrap-theme).
Developers who are happy with only changing the style of the Bootstrap theme may
do so by overriding the provided SCSS.

Developers may further create new components directly on top of LF's core or, if
they do in fact require a whole different look for their web form applications,
they are encouraged to build a new theme on top of the core using the Bootstrap
theme as reference.

Being able to maintain a set of applications under a single theme is one of LF's
goals in order to ease the maintenance and evolution of said applications.

### Don't repeat yourself

Arguably one of the most important aspects of form development is the validation
of their data. Typically this validation should occur both on the client and
server side of an application in order to leverage from both efficiency and
security.

It's an important goal of LF that these validations should not be written more
than once, especially so in cases where there are thousands of fields to
validate.

## Documentation

All documentation can be found at: https://docs.lightweightform.io.

To get started, take a look at our
[tutorial](https://docs.lightweightform.io/tutorial).

## Examples

Example applications built with Lightweightform may be found at:
https://lightweightform.io/examples.

## Contributing

Contributions are always welcome. Please read our
[contributing guide](./CONTRIBUTING.md) on how to contribute.

## License

Lightweightform is released under the [Apache License 2.0](./LICENSE).
